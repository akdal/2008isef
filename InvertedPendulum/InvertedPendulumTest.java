import java.util.*;
import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import static java.lang.Math.*;

public class InvertedPendulumTest extends JPanel implements KeyListener, Runnable{
	public final static int STATE_GAMEOVER = 0;
	public final static int STATE_PLAYING = 1;
	
	public final static int CART_WIDTH = 6;
	public final static int CART_HEIGHT = 3;
	public final static int BOX_WIDTH = 90;
	
	public final static int TIME_INTERVAL = 10;
	
	public final static String SZ_GAMEOVER = "Game Over!";
	public final static Color REC_LINECOLOR = new Color(130, 130, 130, 130);
	public final static Color REC_FILLCOLOR = new Color(255, 255, 255, 130);
	
	public final static double SCALE = 10;
	public final static double PEND_LEN = 20;
	public final static double G = 9.8;
	
	
	
	
	private int state;
	private double loc;
	private double vel;
	private double accl;
	private double angle;
	private double angvel;
	private double angaccl;
	
	private int elapsedTime = 0;
	private int totalcnt = 0;
	private int terminalcnt = 0;
	private StringBuffer log = new StringBuffer();
	private String fileName = "";
	
	
	
	
	// 현재 수행하고 있는 policy
	private ML_Policy currPolicy;
	// 현재 state, 취하고 있는 action
	private int currState;
	private int currActTime;
	private byte currAction;
	private ML_TestCase currTestCase;
	private ML_Utility currUtil;
	private ML_ApprxModel currModel;
	private ML_RewardFunc rewardFunc;
	
	
	
	public InvertedPendulumTest (double point, double demerit, int termcnt, String fileName){
		super.setBackground(Color.white);
		super.setOpaque(true);
		
		this.rewardFunc = new ML_RewardFunc(point, demerit);
		terminalcnt = termcnt;
		this.fileName = fileName;
		//min = new double[SEG_LOC][SEG_ANG][SEG_VEL][SEG_AVEL];
		//max = new double[SEG_LOC][SEG_ANG][SEG_VEL][SEG_AVEL];
	}
	
	
	
	
	public void paint (Graphics g){
		super.paint(g);
		if(g == null) return;
		
		int wid = super.getWidth(), hei = super.getHeight();
		
		g.setColor(Color.black);
		g.drawString(String.valueOf(elapsedTime), 30, 30);
		g.drawString(String.valueOf(totalcnt) + "번", 200, 30);
		_paintPendulum(g, wid, hei);
		_paintCart(g, wid, hei);
		_paintBox(g, wid, hei);
		if(state == STATE_GAMEOVER){
			g.setColor(REC_LINECOLOR);
			g.fillRect(0, 0, wid, hei);
			g.setColor(REC_FILLCOLOR);
			g.fillRect(wid / 2 - 50, hei / 2 - 10, 100, 20);
			g.setColor(Color.black);
			g.drawString("Game Over!", wid / 2 - 35, hei / 2 - 1);
		}
	}// end paint(Graphics)
	
	private void _paintPendulum (Graphics g, int wid, int hei){
		int startx = wid / 2 + (int)(loc * SCALE);
		int starty = (int)(CART_HEIGHT * SCALE) + 30;
		int endx = startx + (int)(SCALE * PEND_LEN * sin(angle));
		int endy = (int)(CART_HEIGHT * SCALE) + 30 + (int)(SCALE * PEND_LEN * cos(angle));
		g.drawLine(startx, hei - starty, endx, hei - endy);
	}// end _paintPen
	private void _paintCart (Graphics g, int wid, int hei){
		int cw = (int)(CART_WIDTH * SCALE),
			ch = (int)(CART_HEIGHT * SCALE);
		int startx = wid / 2 + (int)(SCALE * loc) - cw / 2;
		int starty = ch + 30;
		g.drawRect(startx, hei - starty, cw, ch);
	}// end _paintCart
	private void _paintBox (Graphics g, int wid, int hei){
		g.drawRect(wid / 2 - (int)(BOX_WIDTH * SCALE) / 2 - 10, hei - (int)(CART_HEIGHT * SCALE) - 30, 10, (int)(CART_HEIGHT * SCALE));
		g.drawRect(wid / 2 + (int)(BOX_WIDTH * SCALE) / 2, hei - (int)(CART_HEIGHT * SCALE) - 30, 10, (int)(CART_HEIGHT * SCALE));
	}
	
	
	
	
	
	
	
	/***************************************
	* Physical Simulation IMPLEMENTATIONS
	****************************************/
	public void run (){
		try{
			while(true){
				long time_start = System.currentTimeMillis();
				
				if(rewardFunc.termination(currState)){
					this.state = STATE_GAMEOVER;
					end();
					repaint();
					break;
				}
				updatePhysData();
				updateCurrentState();
				checkAction();
				repaint();
				long time_end = System.currentTimeMillis();
				Thread.sleep(time_end - time_start > TIME_INTERVAL ? 
				    0 : TIME_INTERVAL - (time_end - time_start));
				elapsedTime += TIME_INTERVAL;
			}
		}catch(Exception e){
			e.printStackTrace();
			System.exit(-1);
		}
	}// end run()
	private void updatePhysData (){
		// inetial force to the pendulum
		// F_n = -ma hat x, (a = accl of the cart)
		// gravity
		// F_g = -mg hat y
		// Torque t = 1/2 l(sin theta hat x + cos theta hat y) cross (F_n + F_g)
		//          = I alpha
		//          = 1/3 m l^2 alpha
		// Therefore
		// 1/2 l(sin theta hat x + cos theta hat y) cross (-a hat x - g hat y) = 1/3 l^2 alpha,
		// alpha = 3/(2l) (-g sin theta + a cos theta)
		double dt = (double)TIME_INTERVAL / 1000.0;
		
		angaccl = -3.0 / (2.0 * PEND_LEN) * (-G * sin(angle) + accl * cos(angle));
		angvel = angvel + angaccl * dt;
		angle = angle + angvel * dt - 0.5 * dt * dt * angaccl;
		vel = vel + accl * dt;
		loc = loc + vel * dt - 0.5 * accl * dt * dt;
	}// end updatePhysData()
	
	
	
	
	
	/* *****************************************
	* KeyListener IMPLEMENTATIONS
	****************************************/
	
	public void keyPressed (KeyEvent ke){
		if(ke.getKeyCode() == KeyEvent.VK_ENTER && this.state == STATE_GAMEOVER){
			start();
		}/*else if(ke.getKeyCode() == KeyEvent.VK_LEFT)
			accl = -G;
		else if(ke.getKeyCode() == KeyEvent.VK_RIGHT)
			accl = G;*/
	}// end keyPressed(KeyEvent)(
	public void keyReleased (KeyEvent ke){
		accl = 0;
	}// end keyreleased
	public void keyTyped (KeyEvent ke){}
	
	
	
	
	/* *******************************************
	 *  LEARNING ENGINE PART
	 *****************************************/
	
	private void updateCurrentState (){
		//System.out.printf("updateCurrentState() : x : %f, xdot : %f, theta : %f, thtdot : %f\n", loc, vel, angle, angvel);
		currState = ML_State.getState(loc, vel, angle, angvel);
		//System.out.printf("updateCurrentSt() : currState : %s\n", currState);
		System.out.printf("updateCurrentSt() : U(s) : %f\n", currUtil.value(currState));
	}// end updateCurrentState
	private void checkAction (){
		if((elapsedTime - currActTime) == ML_Action.RESPINTV){
			currTestCase.append(currAction, currState);
			currActTime = elapsedTime;
			doAction(currPolicy.getAction(currState));
		}
	}// end checkAction
	private void doAction (byte ac){
		System.out.printf("--- doAction : %s\n", ac);
		currAction = ac;
		if(ac == ML_Action.DIR_LEFT){
			accl = -G;
		}else if(ac == ML_Action.DIR_RIGHT){
			accl = G;
		}
	}


	private void start (){
		System.out.println("start()");
		vel = accl = angaccl = angvel = loc = elapsedTime = 0;
		angle = random() * PI / 9.0 - PI / 18.0;
		loc = random() * 30.0 - 15;

		if(currPolicy == null){
			currPolicy = new ML_Policy();
		}
		if(currModel == null)
			currModel = new ML_ApprxModel();
		if(currUtil == null)
			currUtil = new ML_Utility();
		if(currTestCase == null)
			currTestCase = new ML_TestCase();

		currActTime = 0;
		updateCurrentState();
		doAction(currPolicy.getAction(currState));
		currTestCase.s0 = currState;
		
		totalcnt++;
		Thread t = new Thread(this);
		this.state = STATE_PLAYING;
		t.start();
	}// end start()	
	private void end(){
		System.out.println("end()");
		
		currTestCase.append(currAction, currState);
		System.out.println("currTestCase : ");
		currTestCase.print();
		
		log();
		log.append(totalcnt).append(" ").append(elapsedTime).append("\n");
		generatePolicy();
		currTestCase.clear();
	
		if(totalcnt == terminalcnt)
			System.exit(0);
				
		start();
	}// end end()
	
	private void generatePolicy (){
		// adp
		updateTransitionModel();
		policyIteration();
	}// end generate
	
	private void updateTransitionModel (){
		this.currModel.apply(currTestCase);
	}// end updateTransitionModel()
	private void policyIteration (){
		System.out.println("policyIteration()");
		boolean changed = true;
		int loop = 0;
		currUtil.clear();
		
		while(changed && loop++ < 30){
			changed = false;
			currUtil = calcUtility(currUtil);
			byte maxa = -1;
			double maxaval, paval;
			
			for(int i = 0; i < ML_State.ALL_STATES_ITR.length; i++){
				maxaval = -1000;
				
				for(byte eacha = 0; eacha <= 2; eacha++){
					if(maxa == -1)
						maxa = eacha;
					double tmpv = 0;
					for(int k = 0; k < ML_State.ALL_STATES_ITR.length; k++){
						//eachsa = ML_State.ALL_STATES_ITR[k];
						tmpv += currModel.valueAt(i, eacha, k) * currUtil.valueAt(k);
					}
					
					if(tmpv > maxaval){
						maxaval = tmpv;
						maxa = eacha;
					}
				}
				paval = 0;
				byte pa = currPolicy.getAction(ML_State.ALL_STATES_ITR[i]);
				for(int k = 0; k < ML_State.ALL_STATES_ITR.length; k++)
					paval += currModel.valueAt(i, pa, k) * currUtil.valueAt(k);
				
				if(maxaval - paval > 0.001){
					System.out.printf("policyIteration : %s : %s->%s\n", ML_State.ALL_STATES_ITR[i], pa, maxa);
					changed = true;
					currPolicy.putAction(ML_State.ALL_STATES_ITR[i], maxa);
				}
			}
			System.out.printf("next loop! loop var : %d\n", loop);
		}
		System.out.println("policyIteration end");
	}// end policyIteration
	private ML_Utility calcUtility(ML_Utility util){
		System.out.println("calcUtility start");
		// transition model을 통해 utility 계산.
		// 행렬계산 같은거 쓰지 말고 그냥 근사값만 추출해낸다.
		ML_Utility nextutil = new ML_Utility();
		double maxchange = Double.POSITIVE_INFINITY;
		double gamma = 0.8;
		
		while(maxchange > 0.03){
			maxchange = 0;
			double val = 0, befval = 0;
			
			for(int i = 0; i < ML_State.ALL_STATES_ITR.length; i++){
				val = rewardFunc.reward(ML_State.ALL_STATES_ITR[i]);
				befval = util.valueAt(i);
				byte action = currPolicy.getAction(ML_State.ALL_STATES_ITR[i]);
				
				for(int k = 0; k < ML_State.ALL_STATES_ITR.length; k++){
					val += gamma * util.valueAt(k) * currModel.valueAt(i, action, k);
				}
				if(Math.abs(val - befval) > maxchange)
					maxchange = Math.abs(val - befval);
				nextutil.putValueAt(i, val);
			}
			System.out.printf("maxchange : %g\n", maxchange);
			
			// swap
			ML_Utility tmp = nextutil;
			nextutil = util;
			util = tmp;
		}
		//util.print();
		System.out.println("calcUtility end");
		return util;
	}// end calcUtility
	
	
	
	
	/*
	LOG
	*/
	public void log() {
		try{
			FileWriter fw = new FileWriter(fileName);
			BufferedWriter bw = new BufferedWriter(fw);
			
			bw.write(log.toString(), 0, log.length());
			
			bw.close();
			fw.close();
		}catch(Exception e){
			e.printStackTrace();
			System.exit(0);
		}
	}// end log()
	
	
	
	
	
	
	
	private static void testGetIndex(){
		for(int i = 0; i < ML_State.ALL_STATES_ITR.length; i++)
			System.out.printf("%d : %d\n", i, ML_State.getIndex(ML_State.ALL_STATES_ITR[i]));
		System.out.println("---------------------------");
	}// end testGetIndex()
	public static void main (String[] argv){
		//testGetIndex();
				
		// argv[0] : point, argv[1] : demerit, argv[2] : loop cnt
		JFrame f = new JFrame("Inverted Pendulum Test");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setLayout(new BorderLayout());
		InvertedPendulumTest ipt = new InvertedPendulumTest(Double.parseDouble(argv[0]),
				Double.parseDouble(argv[1]),
				Integer.parseInt(argv[2]),
				argv[3]);
		f.addKeyListener(ipt);
		f.add(ipt, BorderLayout.CENTER);
		f.setSize(1000, 400);
		f.setVisible(true);
		ipt.start();
	}// end main(String [])
}




class ML_State implements java.io.Serializable{
	final static int TH_CNT = 8;
	final static int THDOT_CNT = 7;
	final static int X_CNT = 7;
	final static int XDOT_CNT = 7;
	final static int TOTAL_CNT = TH_CNT * THDOT_CNT * X_CNT * XDOT_CNT;
	final static int[][][][] ALL_STATES = _generateAllStates();
	final static int[] ALL_STATES_ITR = _generateAllStatesItr();
	
	final static int[][][][] _generateAllStates(){
		int[][][][] ss = new int[X_CNT][TH_CNT][XDOT_CNT][THDOT_CNT];
		
		for(int i = 0; i < X_CNT; i++){
			for(int j = 0; j < TH_CNT; j++){
				for(int k = 0; k < XDOT_CNT; k++){
					for(int l = 0; l < THDOT_CNT; l++){
						ss[i][j][k][l] = (i << 24) | (j << 16) | (k << 8) | l;
						// xcnt, thcnt, xdotcnt, thdotcnt
					}
				}
			}
		}
		return ss;
	}// end _generateAllStates
	final static int[] _generateAllStatesItr(){
		int[] ss = new int[TOTAL_CNT];
		
		for(int i = 0; i < X_CNT; i++){
			for(int j = 0; j < TH_CNT; j++){
				for(int k = 0; k < XDOT_CNT; k++){
					for(int l = 0; l < THDOT_CNT; l++){
						ss[i  * THDOT_CNT * XDOT_CNT * TH_CNT
						 + j * THDOT_CNT * XDOT_CNT
						 + k * THDOT_CNT
						 + l] = ALL_STATES[i][j][k][l];
					}
				}
			}
		}
		return ss;
	}// end _generateAllStatesItr
	public static final int getIndex(int st){
		
		return ((st & 0xFF000000) >> 24) * TH_CNT * THDOT_CNT * XDOT_CNT
				+ ((st & 0x00FF0000) >> 16) * THDOT_CNT * XDOT_CNT
				+ ((st & 0x0000FF00) >> 8) * THDOT_CNT
				+ (st & 0x000000FF);
	}// end getIndex
	public static final int getState(double x, double xdot, double theta, double thetadot){
		int i = 0, j = 0, k = 0, l = 0;
		
		double x_stp = -InvertedPendulumTest.BOX_WIDTH / 2.0 + InvertedPendulumTest.CART_WIDTH / 2.0;
		double x_edp = InvertedPendulumTest.BOX_WIDTH / 2.0 - InvertedPendulumTest.CART_WIDTH / 2.0;
		if(x <= x_stp){
			i = 0;
		}else if(x >= x_edp){
			i = X_CNT - 1;
		}else{
			i = (int)((x - x_stp) / ((x_edp - x_stp) / (double)(X_CNT - 2))) + 1;
		}
		
		if(theta <= -PI / 2.0){
			j = 0;
		}else if(theta >= PI / 2.0){
			j = TH_CNT - 1;
		}else{
		    double seg = PI / (TH_CNT - 2);
		    j = (int)((theta + PI / 2.0) / seg) + 1;
		}
		
		if(xdot < -13)
			k = 0;
		else if(13.0 < xdot)
			k = XDOT_CNT - 1;
		else{
			double seg = 26.0 / (double)(XDOT_CNT - 2);
			k = (int)((xdot + 13.0) / seg) + 1;
		}
		
		if(thetadot < -2.0)
			l = 0;
		else if(2.0 < thetadot)
			l = THDOT_CNT - 1;
		else{
			double seg = 4.0 / (double)(THDOT_CNT - 2);
			l = (int)((thetadot + 2.0) / seg) + 1;
		}
		System.out.printf("x : %d, xdot : %d, th : %d, thd : %d\n", i, k, j, l);
		
		return ALL_STATES[i][j][k][l];
	}
	
	public static String toString(int state){
		byte x_state = (byte)((state & (0xFF000000)) >> 24);
		byte theta_state = (byte)((state & (0x00FF0000)) >> 16);
		byte xdot_state = (byte)((state & (0x0000FF00)) >> 8);
		byte thetadot_state = (byte)(state & (0x000000FF));
		return "[S : x:" + x_state + ", xdot:" + xdot_state + " theta:" + theta_state + ", thdot:" + thetadot_state + "]";
	}// end toString
}

class ML_RewardFunc{
	private double p = 0;
	private double demerit = 0;
	
	public ML_RewardFunc(double p, double demerit)
	{ this.p = p; this.demerit = demerit; }
	
	boolean termination(int st){
		if((st & 0xFF000000) == 0 || ((st & 0xFF000000) >> 24) == ML_State.X_CNT - 1)
			return true;
		else if((st & 0x00FF0000) == 0 || ((st & 0x00FF0000) >> 16) == ML_State.TH_CNT - 1)
			return true;
		return false;
	}// end termination
	double reward(int st){
		if(termination(st))
			return demerit;
		return p;
	}// end reward
}

class ML_Action implements java.io.Serializable{
	public final static byte DIR_LEFT = 0;
	public final static byte DIR_NONE = 1;
	public final static byte DIR_RIGHT = 2;
	public final static int RESPINTV = 200;
	
	public final static byte randomAction(){
		return (byte)(System.nanoTime() % 3L);
	}// end randomAction

	private ML_Action(){}
	
	public static String toString(byte action){
		return "[A : dir:" + action + "]";
	}// end toString()
}

class ML_Policy implements java.io.Serializable{
	private byte[] table = new byte[ML_State.TOTAL_CNT];
	
	public ML_Policy(){
        for(int i= 0; i < table.length; i++)
            table[i] = -1;
    }
	
	byte getAction(int state){
		int idx = ML_State.getIndex(state);
		if(table[idx] == -1){
			table[idx] = ML_Action.randomAction();
		}
		return table[idx];
	}// end getAction
	void putAction(int state, byte a)
	{ table[ML_State.getIndex(state)] = a; }
}

class ML_TestCase implements java.io.Serializable{
	int s0;
	int[] sSet = new int[0];
	byte[] aSet = new byte[0];
	
	void append(byte a, int s){
		int[] newsSet = new int[sSet.length + 1];
		System.arraycopy(sSet, 0, newsSet, 0, sSet.length);
		newsSet[sSet.length] = s;
		byte[] newaSet = new byte[aSet.length + 1];
		System.arraycopy(aSet, 0, newaSet, 0, aSet.length);
		newaSet[aSet.length] = a;
		
		aSet = newaSet;
		sSet = newsSet;
	}// end append
	
	void clear(){
		sSet = new int[0];
		aSet = new byte[0];
	}
	
	void print(){
		System.out.println(s0);
		for(int i = 0; i < sSet.length; i++){
			System.out.println("---" + ML_Action.toString(aSet[i]) + "---> " + ML_State.toString(sSet[i]));
		}
	}
}
// Approximated Transition Model
class ML_ApprxModel implements java.io.Serializable{
	private short[][][] data_trial = new short[ML_State.ALL_STATES_ITR.length][3][ML_State.ALL_STATES_ITR.length];
	private short[][][] data_appl = new short[ML_State.ALL_STATES_ITR.length][3][ML_State.ALL_STATES_ITR.length];
	
	
	double value(int s, byte a, int sa){
		return valueAt(ML_State.getIndex(s), a, ML_State.getIndex(sa));
	}// end value
	double valueAt(int sidx, byte aidx, int saidx){
		if(data_trial[sidx][aidx][saidx] == 0)
			return 0;
		return (double)data_appl[sidx][aidx][saidx] / (double)data_trial[sidx][aidx][saidx];
	}// end valueAt
	
	public void apply(ML_TestCase ts){
		int bef = ts.s0;
		for(int i = 0; i < ts.sSet.length; i++){
			byte a = ts.aSet[i];
			int sa = ts.sSet[i];
			
			int sidx = ML_State.getIndex(bef),
				aidx = a,
				saidx = ML_State.getIndex(sa);
				
			for(int j = 0; j < data_trial[sidx][aidx].length; j++){
			    if(data_trial[sidx][aidx][j] == Short.MAX_VALUE)
			        continue;
				data_trial[sidx][aidx][j]++;
			}
			if(data_trial[sidx][aidx][saidx] != Short.MAX_VALUE)
			    data_appl[sidx][aidx][saidx]++;
			bef = sa;
		}
	}// end apply
}

class ML_Utility implements java.io.Serializable{
    double[] values = new double[ML_State.ALL_STATES_ITR.length];
	
	double value(int st){
		return values[ML_State.getIndex(st)];
	}
	double valueAt(int stidx)
	{ return values[stidx]; }
	void putValue(int st, double v)
	{ values[ML_State.getIndex(st)] = v; }
	void putValueAt(int stidx, double v)
	{ values[stidx] = v; }
	void clear(){
	    for(int i = 0; i < values.length; i++)
	        values[i] = 0;
	}
	/*
	void print(){
		Set<Integer> keySet = table.keySet();
		ArrayList<Integer> array = new ArrayList<Integer>(keySet);
		Collections.sort(array, );
		for(ML_State m : array){
			if(m.x_state != 3) continue;
			System.out.printf("%s : %f\n", m, table.get(m));
		}
	}*/
}
