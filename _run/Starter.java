import java.io.*;
import javax.swing.*;

public class Starter{
	private final static String FILESEP = System.getProperty("file.separator");
	
	public static void main(String[] argv){
		try{
			String version = System.getProperty("java.vm.version").substring(0,
					"1.5.0".length());
			version = version.charAt(0) + "" + version.charAt(2) + "" + version.charAt(4);
			if(Integer.parseInt(version) < 150){
				JOptionPane.showMessageDialog(null, "JVM version is too low : at least 5.0");
				return;
			}
			
			
			String[] checkfiles = new String[]{
				"robodari.jar", "aglib.jar"
			};
			for(int i = 0; i < checkfiles.length; i++){
				File f = new File(checkfiles[i]);
				if(!f.exists()){
					JOptionPane.showMessageDialog(null, "Essential library not exists : " + checkfiles[i]);
					System.exit(0);
				}
			}
			
			String[] clibs = new File("." + FILESEP + "comps").list();
			String[] elibs = new File("." + FILESEP + "editors").list();
			String totalclspath = ".;." + FILESEP + "robodari.jar";
			for(int i = 0; i < clibs.length; i++){
				String nextstr = "." + FILESEP + "comps" + FILESEP + clibs[i];
				totalclspath += ";" + nextstr;
			}
			for(int i = 0; i < elibs.length; i++){
				String nextstr = "." + FILESEP + "editors" + FILESEP + elibs[i];
				totalclspath += ";" + nextstr;
			}
			
			
			String command = "java -cp " + totalclspath + " org.agdr.robodari.gui.Splash";
			System.out.println(command);
			Runtime.getRuntime().exec(command);
		}catch(Exception excp){
			JOptionPane.showMessageDialog(null, "Bug in starter.. Refer to stdout");
			excp.printStackTrace();
		}
	}// end main
}
