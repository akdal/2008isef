echo
echo "lib.jar : "
echo
del lib.jar
jar cvf lib.jar -C "../lib/bin" agdr
echo
echo "core.jar : "
echo
del core.jar
jar cvf core.jar -C "../core/bin" agdr

echo
echo "defcomps.jar : "
echo
del defcomps.jar
jar cvf defcomps.jar -C "../plugin_defaultcomp/bin" agdr
echo
echo "hitec.jar : "
echo
del hitec.jar
jar cvf hitec.jar -C "../plugin_hitec/bin" agdr
echo
echo "minirobot.jar : "
echo
del minirobot.jar
jar cvf minirobot.jar -C "../plugin_minirobot/bin" agdr

echo
echo "rmeditor.jar : "
echo
del rmeditor.jar
jar cvf rmeditor.jar -C "../plugin_modeleditor/bin" agdr
echo
echo "raeditor.jar : "
echo
del raeditor.jar
jar cvf raeditor.jar -C "../plugin_appleditor/bin" agdr

