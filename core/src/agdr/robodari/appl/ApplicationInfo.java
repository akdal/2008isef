package agdr.robodari.appl;

import agdr.library.lang.*;

public class ApplicationInfo {
	public final static VersionNumber VERSION = new VersionNumber(1, 0, 0, 0, 0);
	public final static String VERSIONNAME = "";
	public final static String NAME = "APPLICATION";
	public final static String AUTHOR = "JuneYoung Lee";
	public final static String ENCODING = "euc-kr";
	
	public static String getFullName()
	{ return NAME; }
}
