package agdr.robodari.appl;

import java.util.*;
import java.io.*;
import java.awt.*;
import javax.swing.*;

public class BugManager {
	public final static boolean DEBUG = true;
	
	public final static String PREFIXTURE = "Fatal error occured in this application : ";
	
	private static JTextArea excpTextArea = new JTextArea();
	private static JScrollPane excpScrPane;
	private static JLabel excpPrfxLabel;
	private static Object[] excpMessages;
	
	static{
		excpTextArea.setEditable(false);
		excpTextArea.setOpaque(false);
		
		excpScrPane = new JScrollPane(excpTextArea);
		excpScrPane.setPreferredSize(new Dimension(450, 300));
		
		excpPrfxLabel = new JLabel();
		excpMessages = new Object[]{
			excpPrfxLabel,
			excpTextArea
		};
	}
	
	public static String createLogFileName(){
		Calendar c = Calendar.getInstance();
		StringBuffer buf = new StringBuffer();
		buf.append(c.get(Calendar.YEAR) - 1900).append(c.get(Calendar.MONTH) + 1)
			.append(c.get(Calendar.DATE)).append("_bugreport");
		return buf.toString();
	}// end createLogFileName()
	
	public static void log(Throwable thrw, boolean showDialog){
		log(thrw, showDialog, true);
	}// end log(Throwable)
	public static void log(Throwable thrw, boolean showDialog, boolean exit){
		if(thrw == null) return;
		
		
		File f = null;
		PrintStream ps = null;
		try{
			f = new File(createLogFileName());
			if(f.exists())
				f.createNewFile();
			
			ps = new PrintStream(f);
			
			ps.println();
			ps.println("RoboDari BUG REPORT - " + ApplicationInfo.getFullName());
			
			thrw.printStackTrace(ps);
			if(showDialog)
				show(thrw);
		}catch(Exception excp){
			JOptionPane.showMessageDialog(null, "Error occured while recording error.");
			excp.printStackTrace();
		}finally{
			if(ps != null)
				ps.close();
			f = null;
			if(exit) System.exit(-1);
		}
	}// end log(Throwable)
	
	public static String getBugReportPrefixture()
	{ return PREFIXTURE; }
	
	public static void show(Throwable excp){
		StringWriter sw = new StringWriter();
		excp.printStackTrace(new PrintWriter(sw));
		
		excpTextArea.setText(sw.getBuffer().toString());
		excpPrfxLabel.setText(getBugReportPrefixture());
		
		JOptionPane.showMessageDialog(null, excpMessages,
				ApplicationInfo.getFullName(), JOptionPane.OK_OPTION);
	}// end shwo(String)
	
	
	
	public static void printf(String str, Object... items)
	{ if(DEBUG) System.out.printf(str, items); }
	public static void println(String str)
	{ if(DEBUG) System.out.println(str); }
	public static void print(Object obj)
	{ if(DEBUG) System.out.print(obj); }
	public static void println()
	{ if(DEBUG) System.out.println(); }
	public static void println(Object o)
	{ if(DEBUG) System.out.println(o); }
}
