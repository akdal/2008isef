package agdr.robodari.appl;

import java.util.*;
import javax.swing.*;
import org.xml.sax.*;

import agdr.library.lang.*;
import agdr.robodari.comp.*;
import agdr.robodari.comp.info.*;

public class ExceptionHandler {
	public final static String ONLYFILEALLOWED = "Only file allowed.";
	public final static String CANNOTCOPYFILE = "Cannot copy file.";
	public final static String CANNOTEXPORTFILE = "Cannot export file.";
	public final static String CANNOTSAVEPROJECT = "Cannot save project";
	public final static String CANNOTCREATEFILE = "Cannot create file.";
	public final static String FILEALREADYEXISTS = "File already exists.";
	public final static String READONLYFILE = "File has read-only property.";
	public final static String WRONGFILE = "Wrong file.";
	public final static String WRONGFILENAME = "Wrong file name : ";
	public final static String INCOMPATIBLECONNECTION = "Incompatible connection :" +
			" should follow this : ";
	public final static String STARTERNOTDECIDED = "Start controller is not decided.";
	public final static String REALIZATIONSTARTED = "Realization already started.";
	public final static String REALIZATIONNOTSTARTED = "Realization didn't start.";
	public final static String WRONGPROJECT = "Given project file is damaged : ";
	public final static String WRONGPROJECTNAME = "Wrong project name : ";
	public final static String WRONGPROJECTPATH  = "Wrong project path : ";
	
	
	
	
	
	
	public static void onlyFileAllowed(){
		JOptionPane.showMessageDialog(null, ONLYFILEALLOWED,
				ApplicationInfo.getFullName(),
				JOptionPane.OK_OPTION);
	}// end onlyFileAllowed
	
	

	public static void cannotCopyFile(String from, String to){
		Object[] messages = new Object[]{
				CANNOTCOPYFILE,
				from + " 에서 " + to + "로"
		};
		JOptionPane.showMessageDialog(null, messages,
				ApplicationInfo.getFullName(),
				JOptionPane.OK_OPTION);
	}// end cannotCreateComponet()

	public static void cannotExportFile(String file){
		Object[] messages = new Object[]{
				CANNOTEXPORTFILE,
				file
		};
		JOptionPane.showMessageDialog(null, messages,
				ApplicationInfo.getFullName(),
				JOptionPane.OK_OPTION);
	}// end cannotCreateComponet()
	
	public static void readOnlyFile(String file){
		Object[] messages = new Object[]{
				READONLYFILE,
				file
		};
		JOptionPane.showMessageDialog(null, messages,
				ApplicationInfo.getFullName(),
				JOptionPane.OK_OPTION);
	}// end cannotCreateComponet()
	
	public static void wrongFile(String file){
		Object[] messages = new Object[]{
				WRONGFILE,
				file
		};
		JOptionPane.showMessageDialog(null, messages,
				ApplicationInfo.getFullName(),
				JOptionPane.OK_OPTION);
	}// end cannotCreateComponet()
	public static void wrongFileName(String name){
		Object[] msgs = new Object[2];
		msgs[0] = WRONGPROJECTNAME;
		msgs[1] = name;
		JOptionPane.showMessageDialog(null, msgs, ApplicationInfo.getFullName(),
				JOptionPane.OK_OPTION);
	}// end wrongProjectName(String)
	
	public static void cannotSaveProject()
	{ JOptionPane.showMessageDialog(null, CANNOTSAVEPROJECT,
				ApplicationInfo.getFullName(),
				JOptionPane.OK_OPTION); }

	public static void cannotCreateFile(String file){
		Object[] messages = new Object[]{
				CANNOTCREATEFILE,
				file
		};
		JOptionPane.showMessageDialog(null, messages,
				ApplicationInfo.getFullName(),
				JOptionPane.OK_OPTION);
	}// end cannotCreateComponet()
	
	
	public static void fileAlreadyExists(String file){
		Object[] messages = new Object[]{
				FILEALREADYEXISTS,
				file
		};
		JOptionPane.showMessageDialog(null, messages,
				ApplicationInfo.getFullName(),
				JOptionPane.OK_OPTION);
	}// end fileAlreadyExists(String)
	
	
	public static void incompatibleConnection(String msg){
		Object[] messages = new Object[]{
				INCOMPATIBLECONNECTION,
				msg
		};
		JOptionPane.showMessageDialog(null, messages,
				ApplicationInfo.getFullName(),
				JOptionPane.OK_OPTION);
	}// end incompatibleConnection(String)
	
	
	public static void starterNotDecided(){
		JOptionPane.showMessageDialog(null, STARTERNOTDECIDED,
				ApplicationInfo.getFullName(),
				JOptionPane.OK_OPTION);
	}// end sketchNotOpened()

	public static void realizationStarted(){
		JOptionPane.showMessageDialog(null, REALIZATIONSTARTED,
				ApplicationInfo.getFullName(),
				JOptionPane.OK_OPTION);
	}// end realizationStarted()
	
	public static void realizationNotStarted(){
		JOptionPane.showMessageDialog(null, REALIZATIONNOTSTARTED,
				ApplicationInfo.getFullName(),
				JOptionPane.OK_OPTION);
	}// end realizationStarted()
	
	
	
	public static void wrongProject(ArrayList<SAXParseException> excps){
		Object[] msgs = new Object[excps.size() + 1];
		msgs[0] = WRONGPROJECT;
		for(int i = 0; i < excps.size(); i++)
			msgs[i + 1] = excps.get(i);
		JOptionPane.showMessageDialog(null, msgs, ApplicationInfo.getFullName(),
				JOptionPane.OK_OPTION);
	}// end wrongProject(ArrayList<SAXParseException>_)
	public static void wrongProjectName(String name){
		Object[] msgs = new Object[2];
		msgs[0] = WRONGPROJECTNAME;
		msgs[1] = name;
		JOptionPane.showMessageDialog(null, msgs, ApplicationInfo.getFullName(),
				JOptionPane.OK_OPTION);
	}// end wrongProjectName(String)
	public static void wrongProjectPath(String p){
		Object[] msgs = new Object[2];
		msgs[0] = WRONGPROJECTPATH;
		msgs[1] = p;
		JOptionPane.showMessageDialog(null, msgs, ApplicationInfo.getFullName(),
				JOptionPane.OK_OPTION);
	}// end wrongProjectName(String)
}
