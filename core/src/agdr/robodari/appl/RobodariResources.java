package agdr.robodari.appl;

import java.awt.*;

import agdr.robodari.gui.*;
import agdr.robodari.gui.dialog.*;

public class RobodariResources {/*
	public static final Image ERRTABLE_FERR_IMAGE = Toolkit.getDefaultToolkit()
			.createImage(ErrorTable.class
					.getResource("icons/fatalerror.jpg"));
	public static final Image ERRTABLE_WRN_IMAGE = Toolkit.getDefaultToolkit()
			.createImage(ErrorTable.class
					.getResource("icons/warning.jpg"));
	
	public final static Image SEP_PRJCFG_IMAGE = Toolkit.getDefaultToolkit()
			.createImage(SourceEditorPane.class
					.getResource("icons/srcprjcfg_small.jpg"));
	public final static Image SEP_NEWPRJ_IMAGE = Toolkit.getDefaultToolkit()
			.createImage(SourceEditorPane.class
					.getResource("icons/newsrcprj_small.jpg"));
	public final static Image SEP_NEWSRCPRJITM_IMAGE = Toolkit.getDefaultToolkit()
			.createImage(SourceEditorPane.class
					.getResource("icons/newsrcprjitm_small.jpg"));

	public final static Image SEP_SOURCEICON = Toolkit.getDefaultToolkit()
			.createImage(SourceEditorPane.class
					.getResource("icons/sep_sourceicon.jpg"));
	public final static Image SEP_ERRSOURCEICON = Toolkit.getDefaultToolkit()
			.createImage(SourceEditorPane.class
					.getResource("icons/sep_errsourceicon.jpg"));
	public final static Image SEP_REMOVEICON = Toolkit.getDefaultToolkit()
			.createImage(SourceEditorPane.class
					.getResource("icons/sep_removeicon.jpg"));
	public final static Image SEP_SOURCESAVEICON = Toolkit.getDefaultToolkit()
			.createImage(SourceEditorPane.class
					.getResource("icons/sep_saveicon.jpg"));
	public final static Image SEP_EXPORTICON = Toolkit.getDefaultToolkit()
			.createImage(SourceEditorPane.class
					.getResource("icons/sep_export.jpg"));
	public final static Image SEP_COMPILEICON = Toolkit.getDefaultToolkit()
	.createImage(SourceEditorPane.class
			.getResource("icons/sep_compileicon.jpg"));
	
	*/
	public final static Image NEWDLG_NEWPRJ_IMAGE = Toolkit.getDefaultToolkit()
			.createImage(NewDialog.class
					.getResource("images/newproject.jpg"));
	public final static Image NEWDLG_NEWITM_IMAGE = Toolkit.getDefaultToolkit()
			.createImage(NewDialog.class
					.getResource("images/newitem.jpg"));
	public final static Image NEWDLG_NEWPRJ_ROLLOVER_IMAGE =
			Toolkit.getDefaultToolkit().createImage(NewDialog.class
					.getResource("images/newproject_rollover.jpg"));
	public final static Image NEWDLG_NEWITM_ROLLOVER_IMAGE =
			Toolkit.getDefaultToolkit().createImage(NewDialog.class
					.getResource("images/newitem_rollover.jpg"));
	
	public final static Image SEQDLG_BGIMAGE = Toolkit.getDefaultToolkit()
			.createImage(SequenceDialog.class
					.getResource("images/catenary.jpg"));
	
/*
	public final static Image SPCDLG_NEWPRJ_IMAGE = Toolkit
			.getDefaultToolkit().createImage(SourceProjectConfigDialog
					.class.getResource("images/newsrcprj.jpg"));
	public final static Image SPCDLG_NEWPRJ_ROVER_IMAGE = Toolkit
			.getDefaultToolkit().createImage(SourceProjectConfigDialog
					.class.getResource("images/newsrcprj_rover.jpg"));
	public final static Image SPCDLG_NEWITM_IMAGE = Toolkit
			.getDefaultToolkit().createImage(SourceProjectConfigDialog
					.class.getResource("images/newsrcprjitm.jpg"));
	public final static Image SPCDLG_NEWITM_ROVER_IMAGE = Toolkit
			.getDefaultToolkit().createImage(SourceProjectConfigDialog
					.class.getResource("images/newsrcprjitm_rover.jpg"));
	public final static Image SPCDLG_PRJCFG_IMAGE = Toolkit
			.getDefaultToolkit().createImage(SourceProjectConfigDialog
					.class.getResource("images/srcprjcfg.jpg"));
	public final static Image SPCDLG_PRJCFG_ROVER_IMAGE = Toolkit
			.getDefaultToolkit().createImage(SourceProjectConfigDialog
					.class.getResource("images/srcprjcfg_rover.jpg"));
	*/
	
	
	
	public static void loadImages(Component spl) throws InterruptedException{
		MediaTracker tracker = new MediaTracker(spl);/*
		tracker.addImage(ERRTABLE_FERR_IMAGE, 0);
		tracker.addImage(ERRTABLE_WRN_IMAGE, 1);
		tracker.addImage(SEP_PRJCFG_IMAGE, 2);
		tracker.addImage(SEP_NEWPRJ_IMAGE, 3);
		tracker.addImage(SEP_NEWSRCPRJITM_IMAGE, 4);*/
		tracker.addImage(NEWDLG_NEWPRJ_IMAGE, 5);
		tracker.addImage(NEWDLG_NEWITM_IMAGE, 6);
		tracker.addImage(NEWDLG_NEWPRJ_ROLLOVER_IMAGE, 7);
		tracker.addImage(NEWDLG_NEWITM_ROLLOVER_IMAGE, 8);
		tracker.addImage(SEQDLG_BGIMAGE, 9);/*
		tracker.addImage(SPCDLG_NEWPRJ_IMAGE, 10);
		tracker.addImage(SPCDLG_NEWPRJ_ROVER_IMAGE, 11);
		tracker.addImage(SPCDLG_NEWITM_ROVER_IMAGE, 12);
		tracker.addImage(SPCDLG_NEWITM_IMAGE, 13);
		tracker.addImage(SPCDLG_PRJCFG_IMAGE, 14);
		tracker.addImage(SPCDLG_PRJCFG_ROVER_IMAGE, 15);
		tracker.addImage(SEP_SOURCEICON, 16);
		tracker.addImage(SEP_ERRSOURCEICON, 17);
		tracker.addImage(SEP_SOURCESAVEICON, 18);
		tracker.addImage(SEP_COMPILEICON, 19);
		tracker.addImage(SEP_EXPORTICON, 20);
		tracker.addImage(SEP_REMOVEICON, 21);*/
		
		tracker.waitForAll();
	}// end loadImages()
}
