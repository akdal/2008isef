package agdr.robodari.comp;

public interface Antenna extends RobotComponent{
	public boolean isSimplex();
	public void receive(Object obj);
}
