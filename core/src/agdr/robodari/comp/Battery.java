package agdr.robodari.comp;

public interface Battery extends RobotComponent, Sender{
	public float getVoltage();
}
