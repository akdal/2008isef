package agdr.robodari.comp;

import java.util.*;
import javax.swing.*;

public abstract class Initializer {
	private final static ArrayList<Initializer> initializers = 
		new ArrayList<Initializer>();
	
	public static void register(Initializer init)
	{ if(!containsInitializer(init)) initializers.add(init); }
	public static Initializer[] getInitializers()
	{ return initializers.toArray(new Initializer[]{}); }
	public static boolean containsInitializer(Initializer i)
	{ return initializers.contains(i); }
	
	
	
	
	
	public abstract void initialize() throws Exception;
}
