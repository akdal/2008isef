package agdr.robodari.comp;

public interface LED extends RobotComponent{
	public java.awt.Color getColor();
	public double getRadius();
	public double getPlusRodLength();
	public double getMinusRodLength();
}
