package agdr.robodari.comp;

import java.net.*;
import java.util.*;

public class Note implements java.io.Serializable{
	private String description;
	private ArrayList<String> references = new ArrayList<String>();
	
	public Note()
	{ setDescription(""); }
	public Note(String description)
	{ setDescription(description); }
	public Note(String desc, String[] references){
		setDescription(desc); 
		setReferences(references);
	}// end Constructor(String, String[])
	
	public String[] getReferences()
	{ return references.toArray(new String[]{}); }
	public void addReference(String url)
	{ references.add(url); }
	public int sizeOfReferences()
	{ return references.size(); }
	public String getReference(int idx)
	{ return references.get(idx); }
	public void setReferences(String[] url)
	{ references.addAll(Arrays.asList(url)); }
	
	public String getDescription()
	{ return description; }
	public void setDescription(String desc)
	{ this.description = desc; }
	
	public boolean equals(Object obj){
		if(obj == null) return false;
		else if(obj == this) return true;
		else if(obj instanceof Note){
			Note note = (Note)obj;
			return note.description.equals(description) && 
					note.references.equals(references);
		}
		return false;
	}// end equals(Object)
}
