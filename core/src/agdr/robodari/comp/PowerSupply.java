package agdr.robodari.comp;

public interface PowerSupply {
	public float getMinVoltage();
	public float getMaxVoltage();
}
