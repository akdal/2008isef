package agdr.robodari.comp;

public interface Receiver extends RobotComponent {
	public String getName();
	public javax.swing.Icon getSimpleIcon();
	public boolean isEnabled();

	public boolean isCompatible(Sender sd);
	public String getCondition();
}
