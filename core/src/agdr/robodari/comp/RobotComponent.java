package agdr.robodari.comp;

/**
 * A class for robot components.<br>
 * Constructors of the classes inheriting RobotComponent must have no arguments
 */
public interface RobotComponent extends java.io.Serializable{
	public Note getNote();
	public void setNote(Note note); 
	public String getName();
	public void setName(String name);
	
	public boolean hasReceiver();
	public Receiver[] getReceivers();
	public boolean hasSender();
	public Sender[] getSenders();
	public boolean hasSubComponent();
	
	public RobotComponent[] getSubComponents();
	
	public RobotComponent getParent();
	public void setParent(RobotComponent rc); 
}