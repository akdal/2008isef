package agdr.robodari.comp;

public interface Sender extends RobotComponent {
	public boolean isCompatible(Receiver rcv);
	public String getCondition();
	
	public String getName();
	public javax.swing.Icon getSimpleIcon();
	public boolean isEnabled();
}