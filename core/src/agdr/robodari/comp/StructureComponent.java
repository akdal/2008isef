package agdr.robodari.comp;

public abstract class StructureComponent implements RobotComponent{
	public final static long serialVersionUID = -1969451047686518724L;
	
	private RobotComponent parent;
	private Note note;
	private String name; 
	private boolean hasPhysicalBound = true;
	
	public StructureComponent(){
		this.note = new Note();
	}// ende Constructor()
	
	
	public boolean hasReceiver()
	{ return false; }
	public boolean hasSender()
	{ return false; }
	public Receiver[] getReceivers()
	{ return null; }
	public Sender[] getSenders()
	{ return null; }
	
	public RobotComponent getParent() {
		return parent;
	}
	public void setParent(RobotComponent parent) {
		this.parent = parent;
	}
	public Note getNote() {
		return note;
	}
	public void setNote(Note note) {
		this.note = note;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public boolean hasBound()
	{ return this.hasPhysicalBound; }
	public void setBound(boolean b)
	{ this.hasPhysicalBound = b; }
	
	public String toString()
	{ return this.name; }
}
