package agdr.robodari.comp;

import java.io.*;

public abstract class VirtualComponent implements RobotComponent, Externalizable{
	private RobotComponent parent;
	private Note note;
	private String name; 
	
	public VirtualComponent(){
		this.note = new Note();
	}// ende Constructor()
	
	
	public boolean hasReceiver()
	{ return false; }
	public boolean hasSender()
	{ return false; }
	public Receiver[] getReceivers()
	{ return null; }
	public Sender[] getSenders()
	{ return null; }
	
	public boolean hasSubComponent()
	{ return false; }
	public RobotComponent[] getSubComponents()
	{ return null; }
	public RobotComponent getParent() {
		return parent;
	}
	public void setParent(RobotComponent parent) {
		this.parent = parent;
	}
	public Note getNote() {
		return note;
	}
	public void setNote(Note note) {
		this.note = note;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String toString()
	{ return name; }
	
	
	public void writeExternal(ObjectOutput oo) throws IOException{
		oo.writeObject(this.name);
		oo.writeObject(this.note);
		oo.writeObject(this.parent);
	}// end writeExternal
	public void readExternal(ObjectInput oi) throws IOException, ClassNotFoundException{
		this.name = (String)oi.readObject();
		this.note = (Note)oi.readObject();
		this.parent = (RobotComponent)oi.readObject();
	}// end readExternal
}
