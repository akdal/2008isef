package agdr.robodari.comp.info;

import static agdr.robodari.comp.info.ExponentNumber.*;

public abstract class BasicUnit extends Unit{
	private static int[] gentype(int type){
		int[] types = new int[6];
		types[type] = 1;
		return types;
	}// end gentype
	public BasicUnit(int type, ExponentNumber number, Class standard){
		super(gentype(type), number, standard);
	}// end Constructor
	
	/*
	 * type[0] = 
	 * type[1] = ̽ð
	 * type[2] = 
	 * type[3] = չ
	 * type[4] = ԰
	 * type[5] = µ
	 */
	public static abstract class Length extends BasicUnit{
		public Length(ExponentNumber number)
		{ super(0, number, Meter.class); }
	}// end class
	public static class Meter extends Length{
		public final static String EXPRESSION = "m";

		public Meter()
		{ super(STANDARD); }
		
		public String expression(){ return EXPRESSION; }
	}// end class
	public static class KiloMeter extends Length{
		public final static String EXPRESSION = "km";

		public KiloMeter()
		{ super(KILO); }
		
		public String expression(){ return EXPRESSION; }
	}// end class
	public static class MilliMeter extends Length{
		public final static String EXPRESSION = "mm";

		public MilliMeter()
		{ super(MILLI); }
		
		public String expression(){ return EXPRESSION; }
	}// end class
	public static class CentiMeter extends Length{
		public final static String EXPRESSION = "cm";

		public CentiMeter()
		{ super(CENTI); }
		
		public String expression(){ return EXPRESSION; }
	}// end class
	public static class MicroMeter extends Length{
		public final static String EXPRESSION = "m";

		public MicroMeter()
		{ super(MICRO); }
		
		public String expression(){ return EXPRESSION; }
	}// end class
	public static class NanoMeter extends Length{
		public final static String EXPRESSION = "nm";

		public NanoMeter()
		{ super(NANO); }
		
		public String expression(){ return EXPRESSION; }
	}// end class
	
	
	// mass.
	public static abstract class Mass extends BasicUnit{
		public Mass(ExponentNumber num)
		{ super(3, num, KiloGram.class); }
	}// end class
	public static class Gram extends Mass{
		public final static String EXPRESSION = "g";

		public Gram()
		{ super(STANDARD); }
		
		public String expression(){ return EXPRESSION; }
	}// end class
	public static class MilliGram extends Mass{
		public final static int[] TYPE = new int[]{ 0, 0, 0, 1, 0, 0 };
		public final static String EXPRESSION = "mg";

		public MilliGram()
		{ super(MILLI); }
		
		public String expression(){ return EXPRESSION; }
	}// end class
	public static class KiloGram extends Mass{
		public final static String EXPRESSION = "kg";

		public KiloGram()
		{ super(KILO); }
		
		public String expression(){ return EXPRESSION; }
	}// end class
	public static class MicroGram extends Mass{
		public final static String EXPRESSION = "g";

		public MicroGram()
		{ super(MICRO); }
		
		public String expression(){ return EXPRESSION; }
	}// end class
	public static class NanoGram extends Mass{
		public final static String EXPRESSION = "g";

		public NanoGram()
		{ super(NANO); }
		
		public String expression(){ return EXPRESSION; }
	}// end class
	
	
	// ð
	public static abstract class Time extends BasicUnit{
		public Time(ExponentNumber num)
		{ super(1, num, Second.class); }
	}// end class
	public static class Second extends Time{
		public final static String EXPRESSION = "s";

		public Second()
		{ super(STANDARD); }
		
		public String expression(){ return EXPRESSION; }
	}// end class
	public static class Hour extends Time{
		public final static String EXPRESSION = "h";

		public Hour()
		{ super(HOUR); }
		
		public String expression(){ return EXPRESSION; }
	}// end class
	public static class MilliSecond extends Time{
		public final static String EXPRESSION = "ms";

		public MilliSecond()
		{ super(MILLI); }
		
		public String expression(){ return EXPRESSION; }
	}// end class
	public static class MicroSecond extends Time{
		public final static String EXPRESSION = "s";

		public MicroSecond()
		{ super(MICRO); }
		
		public String expression(){ return EXPRESSION; }
	}// end class
	public static class Minute extends Time{
		public final static String EXPRESSION = "min";

		public Minute()
		{ super(MINUTE); }
		
		public String expression(){ return EXPRESSION; }
	}// end class
	

	// Ϸ.
	
	public abstract static class Electricity extends BasicUnit{
		public Electricity(ExponentNumber num)
		{ super(2, num, Coulomb.class); }
	}// end class Electricity
	public static class Coulomb extends Electricity{
		public final static String EXPRESSION = "C";
		public Coulomb()
		{ super(STANDARD); }
		
		public String expression(){ return EXPRESSION; }
	}// end class
	public static class MilliCoulomb extends Electricity{
		public final static String EXPRESSION = "mC";
		public MilliCoulomb()
		{ super(MILLI); }
		
		public String expression(){ return EXPRESSION; }
	}// end class

	
	//  '
	public static abstract class Angle extends BasicUnit{
		public Angle(ExponentNumber num)
		{ super(4, num, Degrees.class); }
	}// end class
	public static class Radian extends Angle{
		public final static String EXPRESSION = "rad";
		
		public Radian()
		{ super(RADIAN); }

		public String expression(){ return EXPRESSION; }
	}// end class
	public static class Degrees extends Angle{
		public final static String EXPRESSION = "";

		public Degrees()
		{ super(STANDARD); }

		public String expression(){ return EXPRESSION; }
	}// end class
	public static class AngleMinute extends Angle{
		public final static String EXPRESSION = "\"";

		public AngleMinute()
		{ super(MINUTE.reciprocal()); }

		public String expression(){ return EXPRESSION; }
	}// end class
	public static class AngleSecond extends Angle{
		public final static String EXPRESSION = "\'";

		public AngleSecond()
		{ super(HOUR.reciprocal()); }

		public String expression(){ return EXPRESSION; }
	}// end class
	
	
	// ƿµ.
	public abstract static class Temperature extends BasicUnit{
		public Temperature(ExponentNumber expn)
		{ super(5, expn, Celcius.class); }
	}// end class
	public static class Celcius extends Temperature{
		public final static String EXPRESSION = "C";

		public Celcius()
		{ super(STANDARD); }

		public String expression(){ return EXPRESSION; }
	}// end class
	public static class Kelvin extends Temperature{
		public final static String EXPRESSION = "K";

		public Kelvin()
		{ super(STANDARD); }

		public String expression(){ return EXPRESSION; }
	}// end class
}
