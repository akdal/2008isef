package agdr.robodari.comp.info;


public enum CPUInfoKeys implements InfoKey{
	MAXPROPERVOLTAGE("maxProperVoltage", TYPE_QUANTITY,
			new BasicUnit.CentiMeter()),
	MINPROPERVOLTAGE("maxProperVoltage", TYPE_QUANTITY,
			new BasicUnit.CentiMeter());
	
	public final String name;
	public final int type;
	public final Unit unit;
	CPUInfoKeys(String name, int type, Unit unit)
	{ this.name = name; this.type = type; this.unit = unit; }
	
	public String getName()
	{ return name; }
	public int getType()
	{ return type; }
	public Unit getUnit()
	{ return unit; }
	
	public String toString()
	{ return getName(); }
}
