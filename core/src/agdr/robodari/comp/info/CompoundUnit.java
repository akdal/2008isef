package agdr.robodari.comp.info;

import static agdr.robodari.comp.info.BasicUnit.*;
import static agdr.robodari.comp.info.ExponentNumber.*;

public class CompoundUnit{
	// .
	public abstract static class Current extends Unit{
		public final static int[] TYPE = new int[]{ 0, -1, 1, 0, 0, 0 };
		
		public Current(ExponentNumber num)
		{ super(TYPE, num, Ampere.class); }
	}// end class Current
	public static class Ampere extends Current{
		public final static String EXPRESSION = "A";

		public Ampere()
		{ super(STANDARD); }
		
		public String expression(){ return EXPRESSION; }
	}// end class
	public static class MilliAmpere extends Current{
		public final static String EXPRESSION = "mA";

		public MilliAmpere()
		{ super(MILLI); }
		
		public String expression(){ return EXPRESSION; }
	}// end class
	
	
	// Volt
	public abstract static class Voltage extends Unit{
		public final static int[] TYPE = new int[]{ 2, -2, -1, 1, 0, 0 };
		
		public Voltage(ExponentNumber num)
		{ super(TYPE, num, Volt.class); }
	}// end class
	public static class Volt extends Voltage{
		public final static String EXPRESSION = "V";
		
		public Volt(){
			super(STANDARD);
		}// end Constructor

		public String expression(){ return EXPRESSION; }
	}// end class
	public static class MilliVolt extends Voltage{
		public final static String EXPRESSION = "mV";
		
		public MilliVolt(){
			super(MILLI);
		}// end Constructor

		public String expression(){ return EXPRESSION; }
	}// end class
	public static class MicroVolt extends Voltage{
		public final static String EXPRESSION = "V";
		
		public MicroVolt(){
			super(MICRO);
		}// end Constructor

		public String expression(){ return EXPRESSION; }
	}// end class
	

	// Jule, Calorie
	public abstract static class Energy extends Unit{
		public final static int[] TYPE = new int[]{ 2, -2, 0, 1, 0, 0 };
		public Energy(ExponentNumber num)
		{ super(TYPE, num, Jule.class); }
	}// end class
	public static class Jule extends Energy{
		public final static String EXPRESSION = "J";
		
		public Jule(){
			super(STANDARD);
		}// end Constructor

		public String expression(){ return EXPRESSION; }
	}// end class
	public static class Calorie extends Energy{
		public final static String EXPRESSION = "cal";
		
		public Calorie(){
			super(JULETOCAL);
		}// end Constructor

		public String expression(){ return EXPRESSION; }
	}// end class
	public static class KiloCalorie extends Energy{
		public final static String EXPRESSION = "kcal";

		public KiloCalorie(){
			super(JULETOKCAL);
		}// end Constructor

		public String expression(){ return EXPRESSION; }
	}// end class
	

	// 𵿼, ֱ
	public abstract static class Frequency extends Unit{
		public final static int[] TYPE = new int[]{ 0, -1, 0, 0, 0, 0 };
		public Frequency(ExponentNumber num)
		{ super(TYPE, num, Hertz.class); }
	}// end class Frequency
	public static class Hertz extends Frequency{
		public final static String EXPRESSION = "Hz";
		
		public Hertz(){
			super(STANDARD);
		}// end Constructor

		public String expression(){ return EXPRESSION; }
	}// end class
	public static class KiloHertz extends Frequency{
		public final static String EXPRESSION = "KHz";
		
		public KiloHertz(){
			super(KILO);
		}// end Constructor

		public String expression(){ return EXPRESSION; }
	}// end class
	public static class MegaHertz extends Frequency{
		public final static String EXPRESSION = "MHz";
		
		public MegaHertz(){
			super(MEGA);
		}// end Constructor

		public String expression(){ return EXPRESSION; }
	}// end class
	public static class GigaHertz extends Frequency{
		public final static String EXPRESSION = "GHz";
		
		public GigaHertz(){
			super(GIGA);
		}// end Constructor

		public String expression(){ return EXPRESSION; }
	}// end class
	
	

	
	// ӵ Ͱӵ.( )
	public static class Velocity extends Unit{
		public final static int[] TYPE = new int[]{ 1, -1, 0, 0, 0, 0 };
		
		public final Length length;
		public final Time time;
		public final String expression;
		
		public Velocity(Length length, Time time){
			super(TYPE, length.getScale().divide(time.getScale()), Velocity.class);
			this.length = length;
			this.time = time;
			this.expression = length.expression() + "/" + time.expression();
		}// end Constructor

		public String expression(){ return expression; }
	}// end class
	public static class Accel extends Unit{
		public final static int[] TYPE = new int[]{ 1, -2, 0, 0, 0, 0 };
		
		public final Length length;
		public final Time time;
		public final String expression;
		
		public Accel(Length length, Time time){
			super(TYPE, length.getScale().divide(time.getScale().square()), Accel.class);
			this.length = length;
			this.time = time;
			this.expression = length.expression() + "/" + time.expression() + "^2";
		}// end Constructor

		public String expression(){ return expression; }
	}// end class
	public static class AngularVelocity extends Unit{
		public final static int[] TYPE = new int[]{ 0, -1, 0, 0, 1, 0 };
		
		public final Angle angle;
		public final Time time;
		public final String expression;
		
		// lower, upper Լ?; ؾ8 ȵȴ.
		public AngularVelocity(Angle angle, Time time){
			super(TYPE, angle.getScale().divide(time.getScale()), AngularVelocity.class);
			this.angle = angle;
			this.time = time;
			this.expression = angle.expression() + "/" + time.expression();
		}// end Constructor

		public String expression(){ return expression; }
	}// end class
	public static class AngularAccel extends Unit{
		public final static int[] TYPE = new int[]{ 0, -2, 0, 0, 1, 0 };
		
		public final Angle angle;
		public final Time time;
		public final String expression;
		
		public AngularAccel(Angle angle, Time time){
			super(TYPE, angle.getScale().divide(time.getScale().square()), AngularAccel.class);
			this.angle = angle;
			this.time = time;
			this.expression = angle.expression() + "/" + time.expression() + "^2";
		}// end Constructor

		public String expression(){ return expression; }
	}// end class
	
	// Farad
	// CV = Qٿ.
	public abstract static class Capacity extends Unit{
		public final static int[] TYPE = new int[]{ -2, 2, 2, -1, 0, 0 };
		
		public Capacity(ExponentNumber num){
			super(TYPE, num, Farad.class);
		}// end Constructor
	}// end class
	public static class Farad extends Capacity{
		public final static String EXPRESSION = "F";
		
		public Farad(){
			super(STANDARD);
		}// end Constructor

		public String expression(){ return EXPRESSION; }
	}// end class
	public static class MilliFarad extends Capacity{
		public final static String EXPRESSION = "mF";
		
		public MilliFarad(){
			super(MILLI);
		}// end Constructor

		public String expression(){ return EXPRESSION; }
	}// end class
	public static class MicroFarad extends Capacity{
		public final static String EXPRESSION = "F";
		
		public MicroFarad(){
			super(MICRO);
		}// end Constructor

		public String expression(){ return EXPRESSION; }
	}// end class
	
	
	// Orm
	public abstract static class Resistance extends Unit{
		public final static int[] TYPE = new int[]{ 2, -1, -2, 1, 0, 0 };
		public Resistance(ExponentNumber num){
			super(TYPE, num, Orm.class);
		}// end Orm()
	}// end class
	public static class Orm extends Resistance{
		public final static String EXPRESSION = "";
		
		public Orm(){
			super(STANDARD);
		}// end Orm()
		
		public String expression(){ return EXPRESSION; }
	}// end class
	public static class MilliOrm extends Resistance{
		public final static String EXPRESSION = "m٧";
		
		public MilliOrm(){
			super(MILLI);
		}// end Orm()
		
		public String expression(){ return EXPRESSION; }
	}// end class
	public static class KiloOrm extends Resistance{
		public final static String EXPRESSION = "m٧";
		
		public KiloOrm(){
			super(KILO);
		}// end Orm()
		
		public String expression(){ return EXPRESSION; }
	}// end class
	
	
	// Force
	public static class Newton extends Unit{
		public final static int[] TYPE = new int[]{ 1, -2, 0, 1, 0, 0 };
		public final static String EXPRESSION = "N";
		
		public Newton(){
			super(TYPE, STANDARD, Newton.class);
		}// end Orm()
		
		public String expression(){ return EXPRESSION; }
	}// end class Newton
	
	public static class Torque extends Unit{
		public final static int[] TYPE = new int[]{ 2, -2, 0, 1, 0, 0 };
		public final static String EXPRESSION = "N m";
		
		public Torque(){
			super(TYPE, STANDARD, Torque.class);
		}// end Orm()
		
		public String expression(){ return EXPRESSION; }
	}// end class Torque
}
