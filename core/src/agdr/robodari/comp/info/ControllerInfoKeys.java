package agdr.robodari.comp.info;

public enum ControllerInfoKeys implements InfoKey{
	WIDTH("width", TYPE_QUANTITY, new BasicUnit.CentiMeter()),
	HEIGHT("height", TYPE_QUANTITY, new BasicUnit.CentiMeter()),
	LENGTH("length", TYPE_QUANTITY, new BasicUnit.CentiMeter()),
	MINPROPERVOLTAGE("minProperVoltage", TYPE_QUANTITY,new CompoundUnit.Volt()),
	MAXPROPERVOLTAGE("maxProperVoltage", TYPE_QUANTITY, new CompoundUnit.Volt());
	
	public final String name;
	public final int type;
	public final Unit unit;
	ControllerInfoKeys(String name, int type, Unit unit)
	{ this.name = name; this.type = type; this.unit = unit; }
	

	public String getName()
	{ return name; }
	public int getType()
	{ return type; }
	public Unit getUnit()
	{ return unit; }
	
	public String toString()
	{ return getName(); }
}
