package agdr.robodari.comp.info;

public class ExponentNumber extends Number{
	public final static ExponentNumber STANDARD = new ExponentNumber(1, 1);
	public final static ExponentNumber MEGA = new ExponentNumber(6, 1);
	public final static ExponentNumber KILO = new ExponentNumber(3, 1);
	public final static ExponentNumber GIGA = new ExponentNumber(9, 1);
	public final static ExponentNumber TERA = new ExponentNumber(12, 1);
	public final static ExponentNumber CENTI = new ExponentNumber(-2, 1);
	public final static ExponentNumber MILLI = new ExponentNumber(-3, 1);
	public final static ExponentNumber NANO = new ExponentNumber(-9, 1);
	public final static ExponentNumber MICRO = new ExponentNumber(-6, 1);
	
	public final static ExponentNumber HOUR = new ExponentNumber(3, 3.6);
	public final static ExponentNumber MINUTE = new ExponentNumber(2, 6);

	public final static ExponentNumber RADIAN = new ExponentNumber(-2, Math.PI / 1.8);

	public final static ExponentNumber JULETOCAL = new ExponentNumber(0, 4.2);
	public final static ExponentNumber JULETOKCAL = new ExponentNumber(3, 4.2);
	
	public final int exponent;
	public final double number;
	
	public ExponentNumber(int exponent, double number){
		this.exponent = exponent;
		this.number = number;
	}// end Constructor
	
	double gen(){
		double num = 1;
		if(exponent > 0)
			for(int i = 0; i < exponent; i++)
				num *= 10;
		else
			for(int i = 0; i < exponent; i++)
				num *= 0.1;
		return num;
	}// end gen
	
	public float floatValue()
	{ return (float)gen() * (float)number; }
	public double doubleValue()
	{ return gen() * number; }
	public int intValue()
	{ return (int)floatValue(); }
	public long longValue()
	{ return (long)doubleValue(); }

	public ExponentNumber multiple(ExponentNumber expnum){
		int exp = this.exponent + expnum.exponent;
		double num = this.number * expnum.number;
		if(num >= 10){
			while(num >= 10){
				num /= 10.0;
				exp++;
			}
		}else{
			while(num < 1){
				num *= 10.0;
				exp--;
			}
		}
		return new ExponentNumber(exp, num);
	}// end multiple
	public ExponentNumber divide(ExponentNumber expnum){
		int exp = this.exponent - expnum.exponent;
		double num = this.number / expnum.number;
		if(num >= 10){
			while(num >= 10){
				num /= 10.0;
				exp++;
			}
		}else{
			while(num < 1){
				num *= 10.0;
				exp--;
			}
		}
		return new ExponentNumber(exp, num);
	}// end divide
	
	public ExponentNumber negative()
	{ return new ExponentNumber(exponent, -1.0 * number); }
	public ExponentNumber reciprocal()
	{ return new ExponentNumber(-exponent, 1.0 / number); }
	public ExponentNumber square(){
		int exp = this.exponent;
		double num = this.number;
		
		exp *= 2;
		num *= num;
		
		if(num > 10){
			num /= 10.0;
			exp++;
		}
		return new ExponentNumber(exp, num);
	}// end square()
	
	public ExponentNumber plus(ExponentNumber expnum){
		int exp = this.exponent;
		double num = this.number;
		if(exp > expnum.exponent){
			while(exp != expnum.exponent){
				exp--;
				num *= 10.0;
			}
		}else if(exp < expnum.exponent){
			while(exp != expnum.exponent){
				exp++;
				num /= 10.0;
			}
		}
		return new ExponentNumber(exp, num + expnum.number);
	}// end plus(ExponentNumber)
	
	
	public boolean equals(Object obj){
		if(obj == null) return false;
		else if(!(obj instanceof ExponentNumber))
			return false;
		ExponentNumber en = (ExponentNumber)obj;
		return en.exponent == exponent && en.number == number;
	}// end equals
}
