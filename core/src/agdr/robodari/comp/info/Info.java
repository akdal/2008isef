package agdr.robodari.comp.info;

import java.util.*;

import agdr.robodari.comp.*;

public class Info implements java.io.Serializable{
	private Hashtable<InfoKey, Object> data = new Hashtable<InfoKey, Object>();
	private RobotComponent target;
	
	public Info(RobotComponent t)
	{ this.target = t; }
	
	public RobotComponent getTarget()
	{ return target; }
	
	public InfoKey getInfoKey(String key){
		Enumeration<InfoKey> enumr = data.keys();
		for(InfoKey ik = enumr.nextElement(); enumr.hasMoreElements();)
			if(ik.getName().equals(key))
				return ik;
		return null;
	}// end getInfoKey
	
	public Object get(InfoKey ik)
	{ return data.get(ik); }
	public Enumeration<InfoKey> keys()
	{ return data.keys(); }
	public void put(InfoKey ik, Object d)
	{ data.put(ik, d); }
}
