package agdr.robodari.comp.info;

import agdr.robodari.comp.*;

public interface InfoGenerator<T extends RobotComponent>{
	public Info generateInfo(T rc);
}
