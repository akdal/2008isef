package agdr.robodari.comp.info;

public interface InfoKey {
	public final static int TYPE_STRING = 0;
	public final static int TYPE_BOOLEAN = 1;
	public final static int TYPE_REALNUMBER = 4;
	public final static int TYPE_QUANTITY = 2;
	public final static int TYPE_OBJECT = 3;
	
	public String getName();
	public int getType();
	public Unit getUnit();
}
