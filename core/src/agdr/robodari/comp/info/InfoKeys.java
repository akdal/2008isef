package agdr.robodari.comp.info;

import agdr.robodari.comp.info.*;

public enum InfoKeys implements InfoKey{
	NAME("name", TYPE_STRING, null),
	BUSINESS("name", TYPE_STRING, null),
	ISOUTEROBJECT("isOuterObject", TYPE_BOOLEAN, null),
	WEIGHT("weight", TYPE_QUANTITY, new BasicUnit.Gram());
	
	public final String name;
	public final int type;
	public final Unit unit;
	InfoKeys(String v, int type, Unit unit){
		this.name = v;
		this.type = type;
		this.unit = unit;
	}// end Constructor(String, int, Unit)
	
	public String getName()
	{ return name; }
	public int getType()
	{ return type; }
	public Unit getUnit()
	{ return unit; }
	
	public String toString()
	{ return name; }
}
