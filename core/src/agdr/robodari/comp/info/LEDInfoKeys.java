package agdr.robodari.comp.info;

public enum LEDInfoKeys implements InfoKey{
	//WIDTH("width", TYPE_QUANTITY, new BasicUnit.CentiMeter()),
	//HEIGHT("height", TYPE_QUANTITY, new BasicUnit.CentiMeter()),
	//LENGTH("length", TYPE_QUANTITY, new BasicUnit.CentiMeter()),
	RADIUS("radius", TYPE_QUANTITY, new BasicUnit.CentiMeter()),
	COLOR("color", TYPE_OBJECT, null),
	PLUSRODLENGTH("plusRodLength", TYPE_QUANTITY, new BasicUnit.CentiMeter()),
	MINUSRODLENGTH("minusRodLength", TYPE_QUANTITY, new BasicUnit.CentiMeter());
	

	public final String name;
	public final int type;
	public final Unit unit;
	LEDInfoKeys(String v, int type, Unit unit){
		this.name = v;
		this.type = type;
		this.unit = unit;
	}// end Constructor(String, int, Unit)
	
	public String getName()
	{ return name; }
	public int getType()
	{ return type; }
	public Unit getUnit()
	{ return unit; }
}
