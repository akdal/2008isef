package agdr.robodari.comp.info;

import agdr.robodari.comp.*;

public enum MemoryInfoKeys implements InfoKey{
	CAPACITY("capacity", TYPE_REALNUMBER, null),
	MINPROPERVOLTAGE("minProperVoltage", TYPE_QUANTITY, new CompoundUnit.Volt()),
	MAXPROPERVOLTAGE("minProperVoltage", TYPE_QUANTITY, new CompoundUnit.Volt());
	
	public final String name;
	public final int type;
	public final Unit unit;
	MemoryInfoKeys(String name, int type, Unit unit){
		this.name = name;
		this.type = type;
		this.unit = unit;
	}// end Constructor(String, int, Unit)
	
	public String getName()
	{ return name; }
	public int getType()
	{ return type; }
	public Unit getUnit()
	{ return unit; }
}
