package agdr.robodari.comp.info;

import java.util.*;

public enum ServoMotorInfoKeys implements InfoKey{
	TORQUE("torque", TYPE_QUANTITY, new CompoundUnit.Torque()),
	WIDTH("width", TYPE_QUANTITY, new BasicUnit.CentiMeter()),
	HEIGHT("height", TYPE_QUANTITY, new BasicUnit.CentiMeter()),
	LENGTH("width", TYPE_QUANTITY, new BasicUnit.CentiMeter()),
	MAXANGLE("maxAngle", TYPE_QUANTITY, new BasicUnit.Degrees()),
	MINANGLE("minAngle", TYPE_QUANTITY, new BasicUnit.Degrees()),
	MAXPROPERVOLTAGE("maxProperVoltage", TYPE_QUANTITY,
			new BasicUnit.CentiMeter()),
	MINPROPERVOLTAGE("maxProperVoltage", TYPE_QUANTITY,
			new BasicUnit.CentiMeter()),
	SPEED("speed", TYPE_QUANTITY, new CompoundUnit.AngularVelocity(
			new BasicUnit.Degrees(),
			new BasicUnit.Second()));
	
	public final String name;
	public final Unit unit;
	public final int type;
	ServoMotorInfoKeys(String name, int type, Unit unit)
	{ this.name = name; this.unit = unit; this.type = type; }
	
	public String getName()
	{ return name; }
	public Unit getUnit()
	{ return unit; }
	public int getType()
	{ return type; }
}
