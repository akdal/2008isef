package agdr.robodari.comp.info;

import java.util.*;

public abstract class Unit {

	private final static ArrayList<Unit> unitBundle = new ArrayList<Unit>();
	
	int[] type;
	ExponentNumber scale;
	Class standard;
	
	protected Unit(int[] type, ExponentNumber scale, Class standard){
		this.type = type;
		this.scale = scale;
		this.standard = standard;
		if(!unitBundle.contains(this))
			unitBundle.add(this);
	}// end Constructor
	
	/**
	 * 실제 사용할 때 쓰는 표현을 반환합니다.<br>
	 * 예) 킬로미터 : km
	 */
	public abstract String expression();
	/**
	 * 이 유닛의 타잎을 반환합니다.<br><br>
	 * 반환하는 타잎의 배열 길이는 5이며, 각 번지마다 지수를 의미하는 숫자가
	 * 들어 있습니다.<br><br>
	 * type[0] = Length<br>
	 * type[1] = Time<br>
	 * type[2] = Coulomb<br>
	 * type[3] = Weight<br>
	 * type[4] = Angle<br>
	 * type[5] = Temperature<br><br>
	 * ex 1) 뉴턴(단위 N : kg * m / s^2)의 경우 타잎은 다음과 같이 됩니다.<br>
	 * type[0] = 1<br>
	 * type[1] = -2<br>
	 * type[2] = 0<br>
	 * type[3] = 1<br>
	 * type[4] = 0<br>
	 * type[5] = 0<br><br>
	 * ex 2) 암페어(단위 A : C / t)의 경우 타잎은 다음과 같이 됩니다.<br>
	 * type[0] = 0<br>
	 * type[1] = -1<br>
	 * type[2] = 1<br>
	 * type[3] = 0<br>
	 * type[4] = 0<br>
	 * type[5] = 0<br><br>
	 * @return
	 */
	public int[] getTypes(){
		return type;
	}// end getUpper()
	
	public ExponentNumber getScale()
	{ return scale; }
	
	public Class getStandard()
	{ return standard; }
	
	
	//FARAD("F"), MILLIFARAD("mF"), MICROFARAD("μF"),
	//ORM("Ω"), MILLIORM("mΩ"),
	//TESLA("T"), MILLITESLA("mT"), MICROTESLA("μT"),
	//liter, deciliter, milliliter
	//hertz
	
	
	
	public static Unit findUnit(Class unitClass){
		for(Unit eachUnit : unitBundle)
			if(eachUnit.getClass() == unitClass)
				return eachUnit;
		return null;
	}// end findUnit(Class)
	
	
	
	
	public static class None extends Unit{
		public final static String EXPRESSION = "";
		
		public None(){
			super(new int[]{0, 0, 0, 0, 0, 0}, ExponentNumber.STANDARD, None.class);
		}// end Constructor
		
		public String expression(){ return EXPRESSION; }
	}// end class None
}
