package agdr.robodari.comp.model;

import java.awt.*;

/**
 * A class for appearance.<br>
 * This class will contain data about colors, textures,
 *  and reflection informations which will be used in graphics.
 */
public class AppearanceData {
	private Color color;
	
	public Color getColor()
	{ return color; }
	public void setColor(Color c)
	{ this.color = c; }
	
	public AppearanceData clone(){
		AppearanceData ad = new AppearanceData();
		if(color != null)
			ad.color = new Color(this.color.getRed(), this.color.getGreen(), this.color.getBlue(), this.color.getAlpha());
		return ad;
	}// end clone()
	
	public void clear(){
		color = null;
	}// end clear()
}
