package agdr.robodari.comp.model;

import java.util.*;
import javax.vecmath.*;

import agdr.library.math.MathUtility;
import agdr.robodari.appl.*;
import static agdr.library.math.MathUtility.*;
import static java.lang.Math.*;
import static agdr.robodari.appl.BugManager.*;


/*
 * Matrix4f는 field중에 없다, 반드시!!!!!!! except CompoundBounds
 */ 

public abstract class BoundObject {
	private final static boolean DEBUG = BugManager.DEBUG && true;
	
	public final static double DIST_EPSILON = 4.0E-5;
	
	
	private BoundObject(){}
	
	// intersection이 있는지 확인하고, 만약 교차하지 않을 경우 가장 가까운 점 추가. ClosestPosInfoPool에 추가함.
	public abstract boolean intersects(BoundObject bobj, Matrix4d thistrans,
			Matrix4d bobjtrans, ArrayList<ClosestPosInfo> pool);
	public abstract boolean onFace(Matrix4d thistrans, double x, double y, double z, Vector3d output_normal);
	// output vector is normal vector
	public abstract boolean onEdge(Matrix4d thistrans, double x, double y, double z, Vector3d output_edgeDir);
	public abstract boolean isVertex(Matrix4d thistrans, double x, double y, double z);
	
	public abstract BoundObject clone();
	public abstract void translate(double x, double y, double z);
	
	/*
	 * LIST : 
	 * Sphere, Cylinder, Polyhedron, CompoundBounds, Plane
	 * 
	 * Calculation priority : 
	 * CompoundBounds, Cylinder, Polyhedron, Plane, Sphere
	 */
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static class CompoundBounds extends BoundObject{
		private ArrayList<BoundObject> objects = new ArrayList<BoundObject>();
		private ArrayList<Matrix4d> translist = new ArrayList<Matrix4d>();
		
		public void add(BoundObject bobj)
		{ objects.add(bobj); translist.add(null); }
		public void add(BoundObject bobj, Matrix4d trans)
		{ objects.add(bobj); translist.add(trans); }
		
		public void translate(double x, double y, double z){
			for(int i = 0; i < translist.size(); i++){
				Matrix4d m4d = translist.get(i);
				if(m4d == null){
					m4d = new Matrix4d();
					m4d.m00 = m4d.m11 = m4d.m22 = m4d.m33 = 1;
					translist.set(i, m4d);
				}
				m4d.m03 += x;
				m4d.m13 += y;
				m4d.m23 += z;
			}
		}// end trnsl
		
		
		public boolean intersects(BoundObject bobj, Matrix4d thistrans, Matrix4d bobjtrans, ArrayList<ClosestPosInfo> cpip){
			boolean flag = false;
			Matrix4d m4d = new Matrix4d();
			ClosestPosInfo icpi;
			BoundObject iobj;
			Matrix4d itrans;
			
			int initsize = 0;
			if(cpip != null)
				initsize = cpip.size();
			double mindis = 999999999999.0;
			
			for(int i = 0; i < objects.size(); i++){
				iobj = objects.get(i);
				if(iobj != null){
					itrans = translist.get(i);
					
					if(itrans != null){
						MathUtility.fastMul(thistrans, itrans, m4d);
						
						if(iobj.intersects(bobj, m4d, bobjtrans, cpip))
							flag = true;
					}else{
						if(iobj.intersects(bobj, thistrans, bobjtrans, cpip))
							flag = true;
					}
					
					if(cpip != null){
						if(BugManager.DEBUG && DEBUG)
							printf("Bound : Compound : removing not closest infos.. cpipsize : %d, mindis : %g, flag : %b\n", cpip.size(),
									mindis, flag);
						if(!flag){
							for(int j = cpip.size() - 1; j >= initsize; j--){
								icpi = cpip.get(j);
								
								if(icpi.distance - mindis > DIST_EPSILON){
									if(BugManager.DEBUG && DEBUG)
										printf("Bound : Compound : j : %d : removed : icpi.dis : %g\n", j, icpi.distance);
									cpip.remove(j);
									continue;
								}else if(mindis - icpi.distance > DIST_EPSILON){
									for(int k = cpip.size() - 1; k >= j + 1; k--)
										cpip.remove(k);
									mindis = icpi.distance;
								}
								
								if(icpi != null){
									if(icpi.aBoundsObj == iobj)
										icpi.aBoundsObj = this;
									else if(icpi.bBoundsObj == iobj)
										icpi.bBoundsObj = this;
									else{
										if(icpi.aBoundsObj != this && icpi.bBoundsObj != this){
											if(BugManager.DEBUG && DEBUG){
												printf("BUG in BoundObject : UNREACHABLE CODE!\n" +
													"  in CompoundBounds : \n" +
													"  aBoundsObj : %s\n" +
													"  bBoundsobj : %s\n" + 
													"  this : %s\n" + 
													"  cpip size : %d, j : %d\n",
													icpi.aBoundsObj, icpi.bBoundsObj, this, cpip.size(), j);
												System.exit(-1);
											}
										}
									}
								}
							}
						}
					}else{
						if(flag){
							if(BugManager.DEBUG && DEBUG)
								printf("Bound : Compound : intersects : %d\n", i);
							return true;
						}
					}
				}
			}
			return flag;
		}// end intersects(BoundsObjuect, Matrix4d, Matrix4d)

		
		
		public boolean onFace(Matrix4d trans, double px, double py, double pz, Vector3d input_normal){
			Vector3d tmpnormal = new Vector3d();
			Matrix4d m4d = new Matrix4d();
			for(int i = 0; i < objects.size(); i++){
				if(translist.get(i) == null){
					if(objects.get(i).onFace(trans, px, py, pz, tmpnormal)){
						input_normal.set(tmpnormal);
						return true;
					}
				}else{
					MathUtility.fastMul(trans, translist.get(i), m4d);
					//printf("BObj : Plane : onFace : %d : trans : \n%s", i, m4d);
					if(objects.get(i).onFace(m4d, px, py, pz, tmpnormal)){
						input_normal.set(tmpnormal);
						return true;
					}
				}
			}
			return false;
		}// end onFace(
		
		public boolean onEdge(Matrix4d trans, double px, double py, double pz, Vector3d output_edgedir){
			Vector3d tmpnormal = new Vector3d();
			Matrix4d m4d = new Matrix4d();
			for(int i = 0; i < objects.size(); i++){
				if(translist.get(i) == null){
					if(objects.get(i).onEdge(trans, px, py, pz, tmpnormal)){
						output_edgedir.set(tmpnormal);
						return true;
					}
				}else{
					MathUtility.fastMul(trans, translist.get(i), m4d);
					if(objects.get(i).onEdge(m4d, px, py, pz, tmpnormal)){
						output_edgedir.set(tmpnormal);
						return true;
					}
				}
			}
			return false;
		}// end onEdge(Matrix4d, double, double, double, Vectro3d)

		public boolean isVertex(Matrix4d trans, double px, double py, double pz){
			Matrix4d m4d = new Matrix4d();
			for(int i = 0; i < objects.size(); i++){
				if(translist.get(i) == null){
					if(objects.get(i).isVertex(trans, px, py, pz))
						return true;
				}else{
					MathUtility.fastMul(trans, translist.get(i), m4d);
					if(objects.get(i).isVertex(m4d, px, py, pz))
						return true;
				}
			}
			return false;
		}// end onEdge(Matrix4d, double, double, double, Vectro3d)
		
		public CompoundBounds clone(){
			CompoundBounds cb = new CompoundBounds();
			for(int i = 0; i < this.objects.size(); i++){
				if(this.objects.get(i)!= null){
					cb.objects.add(this.objects.get(i).clone());
					if(this.translist.get(i) != null)
						cb.translist.add((Matrix4d)this.translist.get(i).clone());
					else
						cb.translist.add(null);
				}
			}
			return cb;
		}
		public String toString(){
			StringBuffer buf = new StringBuffer("CompoundBounds(items : \n");
			for(int i = 0; i < this.objects.size(); i++)
				buf.append(this.objects.get(i)).append("\n");
			buf.append(")");
			return buf.toString();
		}// end toString()
	}// end class CompoundBounds
	
	
	
	
	
	
	
	
	
	
	public static class Plane extends BoundObject{
		private double normalX, normalY, normalZ;
		private Point3d arbitPOnPlane;
		
		public void set(double Nx, double Ny, double Nz, Point3f arbitraryPOnPlane){
			this.normalX = Nx;
			this.normalY = Ny;
			this.normalZ = Nz;
			this.arbitPOnPlane = new Point3d(arbitraryPOnPlane);
		}// end set(double, double, double, double)
		
		public void translate(double x, double y, double z){
			this.arbitPOnPlane.x += x;
			this.arbitPOnPlane.y += y;
			this.arbitPOnPlane.z += z;
		}// end translate
		
		public boolean intersects(BoundObject bobj, Matrix4d thistrans, Matrix4d bobjtrans, ArrayList<ClosestPosInfo> cpip){
			if(bobj instanceof CompoundBounds
					|| bobj instanceof ConvexPolyhedron
					|| bobj instanceof Cylinder){
				return bobj.intersects(this, bobjtrans, thistrans, cpip);
			}
			ClosestPosInfo tmpcpi = null;
			if(cpip != null){
				cpip.add(tmpcpi = new ClosestPosInfo());
				tmpcpi.clear();
				tmpcpi.aBoundsObj = this;
				tmpcpi.bBoundsObj = bobj;
			}

			double nx = normalX, ny = normalY, nz = normalZ;
			double apopx = arbitPOnPlane.x, apopy = arbitPOnPlane.y, apopz = arbitPOnPlane.z;
			if(thistrans != null){
				nx = thistrans.m00 * normalX + thistrans.m01 * normalY + thistrans.m02 * normalZ;
				ny = thistrans.m10 * normalX + thistrans.m11 * normalY + thistrans.m12 * normalZ;
				nz = thistrans.m20 * normalX + thistrans.m21 * normalY + thistrans.m22 * normalZ;
				
				apopx = thistrans.m00 * arbitPOnPlane.x + thistrans.m01 * arbitPOnPlane.y + thistrans.m02 * arbitPOnPlane.z + thistrans.m03;
				apopy = thistrans.m10 * arbitPOnPlane.x + thistrans.m11 * arbitPOnPlane.y + thistrans.m12 * arbitPOnPlane.z + thistrans.m13;
				apopz = thistrans.m20 * arbitPOnPlane.x + thistrans.m21 * arbitPOnPlane.y + thistrans.m22 * arbitPOnPlane.z + thistrans.m23;
			}
			
			if(bobj instanceof Plane){
				Plane pl = (Plane)bobj;
				double pnx = pl.normalX,
						pny = pl.normalY,
						pnz = pl.normalZ;
				double papopx = pl.arbitPOnPlane.x,
						papopy = pl.arbitPOnPlane.y,
						papopz = pl.arbitPOnPlane.z;
				if(bobjtrans != null){
					pnx = bobjtrans.m00 * pl.normalX + bobjtrans.m01 * pl.normalY + bobjtrans.m02 * pl.normalZ;
					pny = bobjtrans.m10 * pl.normalX + bobjtrans.m11 * pl.normalY + bobjtrans.m12 * pl.normalZ;
					pnz = bobjtrans.m20 * pl.normalX + bobjtrans.m21 * pl.normalY + bobjtrans.m22 * pl.normalZ;
					
					papopx = thistrans.m00 * pl.arbitPOnPlane.x + thistrans.m01 * pl.arbitPOnPlane.y + thistrans.m02 * pl.arbitPOnPlane.z + thistrans.m03;
					papopy = thistrans.m10 * pl.arbitPOnPlane.x + thistrans.m11 * pl.arbitPOnPlane.y + thistrans.m12 * pl.arbitPOnPlane.z + thistrans.m13;
					papopz = thistrans.m20 * pl.arbitPOnPlane.x + thistrans.m21 * pl.arbitPOnPlane.y + thistrans.m22 * pl.arbitPOnPlane.z + thistrans.m23;
				}
				double val1 = nx * pnx + ny * pny + nz * pnz;
				double val = 1.0 - Math.abs(val1);
				if(Math.abs(val) < BoundObject.DIST_EPSILON){
					// N dot P + n0 = 0
					// -> n0 = -N dot P
					double n0 = -(nx * apopx + ny * apopy + nz * apopz);
					double pn0 = -(pnx * papopx + pny * papopy + pnz * papopz);
					
					if(val1 > 0.0 && Math.abs(pn0 - n0) < BoundObject.DIST_EPSILON){
						// intersects through whole plane.
						if(tmpcpi == null)
							return true;
						tmpcpi.intersects = ClosestPosInfo.ITSC_YES;
						tmpcpi.distance = -1;
					}else if(val1 < 0.0 && Math.abs(pn0 + n0) < BoundObject.DIST_EPSILON){
						if(tmpcpi == null)
							return true;
						tmpcpi.intersects = ClosestPosInfo.ITSC_YES;
						tmpcpi.distance = -1;
					}else{
						if(tmpcpi == null)
							return false;
						double a_apopx = this.arbitPOnPlane.x,
								a_apopy = this.arbitPOnPlane.y,
								a_apopz = this.arbitPOnPlane.z;
						double b_apopx = pl.arbitPOnPlane.x,
								b_apopy = pl.arbitPOnPlane.y,
								b_apopz = pl.arbitPOnPlane.z;
						
						if(thistrans != null){
							a_apopx = this.arbitPOnPlane.x * thistrans.m00
									+ this.arbitPOnPlane.y * thistrans.m01
									+ this.arbitPOnPlane.z * thistrans.m02
									+ thistrans.m03;
							a_apopx = this.arbitPOnPlane.x * thistrans.m10
									+ this.arbitPOnPlane.y * thistrans.m11
									+ this.arbitPOnPlane.z * thistrans.m12
									+ thistrans.m13;
							a_apopx = this.arbitPOnPlane.x * thistrans.m20
									+ this.arbitPOnPlane.y * thistrans.m21
									+ this.arbitPOnPlane.z * thistrans.m22
									+ thistrans.m23;
						}
						if(bobjtrans != null){
							b_apopx = pl.arbitPOnPlane.x * bobjtrans.m00
									+ pl.arbitPOnPlane.y * bobjtrans.m01
									+ pl.arbitPOnPlane.z * bobjtrans.m02
									+ bobjtrans.m03;
							b_apopx = pl.arbitPOnPlane.x * bobjtrans.m10
									+ pl.arbitPOnPlane.y * bobjtrans.m11
									+ pl.arbitPOnPlane.z * bobjtrans.m12
									+ bobjtrans.m13;
							b_apopx = pl.arbitPOnPlane.x * bobjtrans.m20
									+ pl.arbitPOnPlane.y * bobjtrans.m21
									+ pl.arbitPOnPlane.z * bobjtrans.m22
									+ bobjtrans.m23;
						}
						
						tmpcpi.intersects = ClosestPosInfo.ITSC_NO;
						tmpcpi.aType = tmpcpi.bType = ClosestPosInfo.TYPE_PLANE;
						tmpcpi.distance = Math.abs(
								  (a_apopx - b_apopx) * nx
								+ (a_apopy - b_apopy) * ny
								+ (a_apopz - b_apopz) * nz);
						tmpcpi.aPoints = new double[]{
							a_apopx,
							a_apopy,
							a_apopz
						};
						tmpcpi.bPoints = new double[]{
							b_apopx,
							b_apopy,
							b_apopz
						};
					}
				}else{
					if(tmpcpi == null)
						return true;
					tmpcpi.intersects = ClosestPosInfo.ITSC_YES;
					tmpcpi.distance = -1;
				}
			}else if(bobj instanceof Sphere){
				Sphere sph = (Sphere)bobj;
				double scx = sph.centerX,
						scy = sph.centerY,
						scz = sph.centerZ;
				if(bobjtrans != null){
					scx = bobjtrans.m00 * sph.centerX + bobjtrans.m01 * sph.centerY
							+ bobjtrans.m02 * sph.centerZ + bobjtrans.m03;
					scy = bobjtrans.m10 * sph.centerX + bobjtrans.m11 * sph.centerY
							+ bobjtrans.m12 * sph.centerZ + bobjtrans.m13;
					scz = bobjtrans.m20 * sph.centerX + bobjtrans.m21 * sph.centerY
							+ bobjtrans.m22 * sph.centerZ + bobjtrans.m23;
				}
				double dist = nx * (scx - apopx) + ny * (scy - apopy) + nz * (scz - apopz);
				//printf("Bobj : Plane : getDis : Sph : dis : %f\n", dist);
				
				if(dist < 0.0 || Math.abs(dist) <= sph.radius){
					if(tmpcpi == null)
						return true;
					tmpcpi.intersects = ClosestPosInfo.ITSC_YES;
					//printf("      INTERSECTS! sph.rad : %g\n", sph.radius);
					tmpcpi.distance = -1;
				}else{
					if(tmpcpi == null)
						return false;
					tmpcpi.distance = dist - sph.radius;
					tmpcpi.aType = tmpcpi.bType = ClosestPosInfo.TYPE_VERTEX;
					tmpcpi.aPoints = new double[]{
							scx + dist * nx,
							scy + dist * ny,
							scz + dist * nz
					};
					tmpcpi.bPoints = new double[]{
							// on the sphere
							scx - sph.radius * nx,
							scy - sph.radius * ny,
							scz - sph.radius * nz
					};
					tmpcpi.intersects = ClosestPosInfo.ITSC_NO;
				}
			}else{
				if(tmpcpi == null)
					return true;
				tmpcpi.intersects = ClosestPosInfo.ITSC_YES;
				tmpcpi.distance = -1;
			}
			return tmpcpi.intersects == ClosestPosInfo.ITSC_YES;
		}// end getDistance(BoundObject, Matrix4d, Matrix4d)
		
		
		
		
		public boolean onEdge(Matrix4d trans, double x, double y, double z, Vector3d edgdir)
		{ return false; }
		public boolean isVertex(Matrix4d trans, double x, double y, double z)
		{ return false; }
		public boolean onFace(Matrix4d thistrans, double x, double y, double z, Vector3d output_Ndir){
			// STABLE CODE
			double nx = normalX, ny = normalY, nz = normalZ;
			double papopx = arbitPOnPlane.x, papopy = arbitPOnPlane.y, papopz = arbitPOnPlane.z;
			if(thistrans != null){
				nx = thistrans.m00 * normalX + thistrans.m01 * normalY + thistrans.m02 * normalZ;
				ny = thistrans.m10 * normalX + thistrans.m11 * normalY + thistrans.m12 * normalZ;
				nz = thistrans.m20 * normalX + thistrans.m21 * normalY + thistrans.m22 * normalZ;
				
				papopx = thistrans.m00 * arbitPOnPlane.x + thistrans.m01 * arbitPOnPlane.y + thistrans.m02 * arbitPOnPlane.z + thistrans.m03;
				papopy = thistrans.m10 * arbitPOnPlane.x + thistrans.m11 * arbitPOnPlane.y + thistrans.m12 * arbitPOnPlane.z + thistrans.m13;
				papopz = thistrans.m20 * arbitPOnPlane.x + thistrans.m21 * arbitPOnPlane.y + thistrans.m22 * arbitPOnPlane.z + thistrans.m23;
			}
			double dis = Math.abs(nx * (x - papopx) + ny * (y - papopy) + nz * (z - papopz));
			//printf("BObj : Plane : onFace : dis : %g, x : %g %g %g\n    nx : %g %g %g\n    papop : %g %g %g\n",
			//			dis, x, y, z, nx, ny, nz, papopx, papopy, papopz);
			if(dis <= BoundObject.DIST_EPSILON){
				if(output_Ndir != null){
					output_Ndir.x = nx;
					output_Ndir.y = ny;
					output_Ndir.z = nz;
				}
				return true;
			}
			return false;
		}// end onFace
		
		
		
		public Plane clone(){
			Plane p = new Plane();
			p.normalX = normalX;
			p.normalY = normalY;
			p.normalZ = normalZ;
			p.arbitPOnPlane = new Point3d(arbitPOnPlane);
			return p;
		}// end clone()
		public String toString(){
			return "Plane(normal : " + normalX + " , " + normalY + " , " + normalZ +", arbP : " + arbitPOnPlane + ")";
		}// end toString()
	}// end class Plane
	
	
	
	
	
	
	
	
	
	
	public static class Sphere extends BoundObject{
		private double centerX, centerY, centerZ;
		private double radius;
		private double radiusSqre;
		
		public void setCenter(double x, double y,double z){
			this.centerX = x;
			this.centerY = y;
			this.centerZ = z;
		}// end setCenter(double,double, double)
		public double getCenterX()
		{ return centerX; }
		public double getCenterY()
		{ return centerY; }
		public double getCenterZ()
		{ return centerZ; }
		
		public void translate(double x, double y, double z){
			centerX += x;
			centerY += y;
			centerZ += z;
		}// end translate(double, double, double)
		
		
		public double getRadius()
		{ return radius; }
		public void setRadius(double r){
			this.radius = r;
			this.radiusSqre = this.radius * radius;
		}
		
		public boolean intersects(BoundObject bobj, Matrix4d thistrans, Matrix4d bobjtrans, ArrayList<ClosestPosInfo> cpip){
			if(bobj instanceof CompoundBounds
					|| bobj instanceof ConvexPolyhedron
					|| bobj instanceof Cylinder
					|| bobj instanceof Plane)
				return bobj.intersects(this, bobjtrans, thistrans, cpip);
			
			ClosestPosInfo cpi = null;
			if(cpip != null){
				cpip.add(cpi = new ClosestPosInfo());
				cpi.clear();
				cpi.aBoundsObj = this;
				cpi.bBoundsObj = bobj;
			}
			
			if(bobj instanceof Sphere){
				Sphere sph = (Sphere)bobj;

				double cx = this.centerX, cy = this.centerY, cz = this.centerZ;
				double scx = sph.centerX, scy = sph.centerY, scz = sph.centerZ;
				
				if(thistrans != null){
					cx = centerX * thistrans.m00 + centerY * thistrans.m01 + centerZ * thistrans.m02 + thistrans.m03;
					cy = centerX * thistrans.m10 + centerY * thistrans.m11 + centerZ * thistrans.m12 + thistrans.m13;
					cz = centerX * thistrans.m20 + centerY * thistrans.m21 + centerZ * thistrans.m22 + thistrans.m23;
				}
				if(bobjtrans != null){
					scx = sph.centerX * bobjtrans.m00 + sph.centerY * bobjtrans.m01
							+ sph.centerZ * bobjtrans.m02 + bobjtrans.m03;
					scy = sph.centerX * bobjtrans.m10 + sph.centerY * bobjtrans.m11
							+ sph.centerZ * bobjtrans.m12 + bobjtrans.m13;
					scz = sph.centerX * bobjtrans.m20 + sph.centerY * bobjtrans.m21
							+ sph.centerZ * bobjtrans.m22 + bobjtrans.m23;
				}
				
				double len = (double)Math.sqrt((cx - scx) * (cx - scx) + (cy - scy) * (cy - scy) + (cz - scz) * (cz - scz));
				if(len < this.radius + sph.radius){
					if(cpi == null)
						return true;
					cpi.distance = -1;
					cpi.intersects = ClosestPosInfo.ITSC_YES;
				}else{
					if(cpi == null)
						return false;
					
					double dirx = (scx - cx) / len;
					double diry = (scy - cy) / len;
					double dirz = (scz - cz) / len;
					cpi.aPoints = new double[]{
						cx + dirx * radius, cy + diry * radius, cz + dirz * radius
					};
					cpi.bPoints = new double[]{
						scx - dirx * sph.radius, scy - diry * sph.radius, scz - dirz * sph.radius
					};
					cpi.aType = cpi.bType = ClosestPosInfo.TYPE_VERTEX;
					cpi.distance = len - radius - sph.radius;
					cpi.intersects = ClosestPosInfo.ITSC_NO;
				}
			}
			return cpi.intersects == ClosestPosInfo.ITSC_YES;
		}// end getDistance
		

		public boolean onEdge(Matrix4d trans, double px, double py, double pz, Vector3d edgdir)
		{ return false; }
		public boolean isVertex(Matrix4d trans, double px, double py, double pz)
		{ return false; }
		public boolean onFace(Matrix4d trans, double px, double py, double pz, Vector3d output_normal){
			double cx = this.centerX, cy = this.centerY, cz = this.centerZ;
			if(trans != null){
				cx = trans.m00 * centerX + trans.m01 * centerY
					+ trans.m02 * centerZ + trans.m03;
				cy = trans.m10 * centerX + trans.m11 * centerY
					+ trans.m12 * centerZ + trans.m13;
				cz = trans.m20 * centerX + trans.m21 * centerY
					+ trans.m22 * centerZ + trans.m23;
			}
			/*
			printf("Bobj : Sph : onFace : p : %f, %f, %f\n", px, py, pz);
			printf("Bobj : Sph : onFace : c : %f, %f, %f\n", cx, cy, cz);
			double dis = (double)Math.sqrt((px - cx) * (px - cx) + (py - cy) * (py - cy) + (pz - cz) * (pz - cz));
			printf("Bobj : Sph : onFace : (p-c)^2 : %f, radiusSqr : %f\n", dis, radiusSqre);
			printf("Bobj : Sph : onFace : flag : %g\n", dis / radius - 1.0);
			if(dis / radius - 1.0 < BoundObject.DIST_EPSILON){
			*/

				if(BugManager.DEBUG && DEBUG)
					printf("Bobj : Sph : onFace : p : %.3f, %.3f, %.3f, c : %.3f, %.3f, %.3f\n", px, py, pz, cx, cy, cz);
				output_normal.x = px - cx;
				output_normal.y = py - cy;
				output_normal.z = pz - cz;
				output_normal.normalize();
				return true;
			/*}
			return false;*/
		}// end onFace(Matrix4d, double, double, double, Vector3d)
		
		
		public Sphere clone(){
			Sphere sp = new Sphere();
			sp.centerX = centerX;
			sp.centerY = centerY;
			sp.centerZ = centerZ;
			sp.radius = radius;
			sp.radiusSqre = radiusSqre;
			return sp;
		}// end clone)
		
		public String toString(){
			return "Sphere(rad : " + radius + ",center : " + centerX + "," + centerY + "," + centerZ + ")";
		}// end toString
	}// end class Sphere
	
	
	
	
	
	
	
	
	
	
	public static class Cylinder extends ConvexPolyhedron{
		private double baseCx, baseCy, baseCz;
		private double topCx, topCy, topCz;
		private double radius;
		private double cylinderheight;
		
		
		public Cylinder(double basecx, double basecy, double basecz,
				double topcx, double topcy,double topcz, double radius){
			this.baseCx = basecx;
			this.baseCy = basecy;
			this.baseCz = basecz;
			this.topCx = topcx;
			this.topCy = topcy;
			this.topCz = topcz;
			this.radius = radius;
			this.cylinderheight = sqrt((baseCx - topCx) * (baseCx - topCx)
					+ (baseCy - topCy) * (baseCy - topCy)
					+ (baseCz - topCz) * (baseCz - topCz));
			
			// 원기둥을 감쌀 수 있는 가장 작은 convex polyhedron을 구현함.
			// 만약 반지름이 1보다 작을 경우 6각형, 더 클 경우 [(r - 1) * 2] + 6각형으로
			int n;
			if(radius < 1.0){
				n = 6;
			}else
				n = (int)((radius - 1) * 2.0) + 6;

			Point3f[] convex_vertices;
			int[][] convex_faces;
			
			double phi = Math.PI / (double)n;
			convex_vertices = new Point3f[2 * n];
			convex_faces = new int[n + 2][];
			
			double convex_r = radius / Math.cos(phi);
			double tmpx, tmpy;
			
			for(int i = 0; i < n; i++){
				tmpx = convex_r * cos((2 * i + 1) * phi);
				tmpy = convex_r * sin((2 * i + 1) * phi);
				convex_vertices[i] = new Point3f((float)tmpx, (float)tmpy, (float)cylinderheight / -2.0f);
				convex_vertices[i + n] = new Point3f((float)tmpx, (float)tmpy, (float)cylinderheight / 2.0f);
			}
			convex_faces[0] = new int[n];
			convex_faces[1] = new int[n];
			for(int i = 0; i < n; i++){
				convex_faces[0][i] = n - i - 1;
				convex_faces[1][i] = i + n;
			}
			for(int i = 0; i < n - 1; i++){
				convex_faces[i + 2] = new int[4];
				convex_faces[i + 2][0] = i;
				convex_faces[i + 2][1] = i + 1;
				convex_faces[i + 2][2] = i + n + 1;
				convex_faces[i + 2][3] = i + n;
			}
			convex_faces[n + 1] = new int[4];
			convex_faces[n + 1][0] = n - 1;
			convex_faces[n + 1][1] = 0;
			convex_faces[n + 1][2] = n;
			convex_faces[n + 1][3] = n - 1 + n;
			
			// conex_normal을 채운다.
			super.set(convex_vertices, convex_faces);
			
			convex_vertices = null;
			convex_faces = null;
		}// end Constructor()
		
		
		public void translate(double x, double y, double z){
			baseCx += x;
			baseCy += y;
			baseCz += z;
			topCx += x;
			topCy += y;
			topCz += z;
		}// end translate
		
		
		
		public strictfp boolean intersects(BoundObject bobj, Matrix4d thistrans, Matrix4d bobjtrans, ArrayList<ClosestPosInfo> cpip){
			if(bobj == null)
				return false;
			
			if(bobj instanceof CompoundBounds)
				return bobj.intersects(this, bobjtrans, thistrans, cpip);

			double bcx = baseCx, bcy = baseCy, bcz = baseCz;
			double tcx = topCx, tcy = topCy, tcz = topCz;
			double axisx = 0, axisy = 0, axisz = 0;
			if(thistrans != null){
				bcx = thistrans.m00 * baseCx + thistrans.m01 * baseCy
					+ thistrans.m02 * baseCz + thistrans.m03;
				bcy = thistrans.m10 * baseCx + thistrans.m11 * baseCy
					+ thistrans.m12 * baseCz + thistrans.m13;
				bcz = thistrans.m20 * baseCx + thistrans.m21 * baseCy
					+ thistrans.m22 * baseCz + thistrans.m23;

				tcx = thistrans.m00 * topCx + thistrans.m01 * topCy
					+ thistrans.m02 * topCz + thistrans.m03;
				tcy = thistrans.m10 * topCx + thistrans.m11 * topCy
					+ thistrans.m12 * topCz + thistrans.m13;
				tcz = thistrans.m20 * topCx + thistrans.m21 * topCy
					+ thistrans.m22 * topCz + thistrans.m23;
				
			}
			ClosestPosInfo cpi = null;
			if(cpip != null){
				cpip.add(cpi = new ClosestPosInfo());
				cpi.aBoundsObj = this;
				cpi.bBoundsObj = bobj;
			}
			
			axisx = tcx - bcx;
			axisy = tcy - bcy;
			axisz = tcz - bcz;
			axisx /= cylinderheight;
			axisy /= cylinderheight;
			axisz /= cylinderheight;
			
			
			
			if(bobj instanceof Cylinder){
				Cylinder cyl = (Cylinder)bobj;
				double cylbcx = cyl.baseCx, cylbcy = cyl.baseCy, cylbcz = cyl.baseCz;
				double cyltcx = cyl.topCx, cyltcy = cyl.topCy, cyltcz = cyl.topCz;
				
				if(bobjtrans != null){
					cylbcx = bobjtrans.m00 * cyl.baseCx + bobjtrans.m01 * cyl.baseCy
						+ bobjtrans.m02 * cyl.baseCz + bobjtrans.m03;
					cylbcy = bobjtrans.m10 * cyl.baseCx + bobjtrans.m11 * cyl.baseCy
						+ bobjtrans.m12 * cyl.baseCz + bobjtrans.m13;
					cylbcz = bobjtrans.m20 * cyl.baseCx + bobjtrans.m21 * cyl.baseCy
						+ bobjtrans.m22 * cyl.baseCz + bobjtrans.m23;

					cyltcx = bobjtrans.m00 * cyl.topCx + bobjtrans.m01 * cyl.topCy
						+ bobjtrans.m02 * cyl.topCz + bobjtrans.m03;
					cyltcy = bobjtrans.m10 * cyl.topCx + bobjtrans.m11 * cyl.topCy
						+ bobjtrans.m12 * cyl.topCz + bobjtrans.m13;
					cyltcz = bobjtrans.m20 * cyl.topCx + bobjtrans.m21 * cyl.topCy
						+ bobjtrans.m22 * cyl.topCz + bobjtrans.m23;
					
				}
				
				double cylaxisx = cyltcx - cylbcx,
						cylaxisy = cyltcy - cylbcy,
						cylaxisz = cyltcz - cylbcz;
				cylaxisx /= cyl.cylinderheight;
				cylaxisy /= cyl.cylinderheight;
				cylaxisz /= cyl.cylinderheight;
				
				double axisdot = axisx * cylaxisx + axisy * cylaxisy + axisz * cylaxisz;
				BugManager.printf("Bobj : cyl : thisaxis : %f %f %f bobjaxis : %f %f %f\n", axisx, axisy, axisz,
						cylaxisx, cylaxisy, cylaxisz);
				if(abs(axisx - cylaxisx) < DIST_EPSILON && abs(axisy - cylaxisy) < DIST_EPSILON){
					if(BoundObject.DEBUG)
						BugManager.printf("BObj : Cyl : parallel axis\n");
					// 나란함
					// tctcdis = this의 tc와 cyl의 tc사이 거리
					// tcaxisdis = this의 tc와 cyl의 tc사이의 축에 나란한 방향 거리 = vec Axis dot (vec Tc2 - vec Tc1)
					// axisdis ^ 2 = tcdis ^ 2 - tcaxisdis ^ 2
					
					// if axisdot < 0 then cyl.tc <=> cyl.bc
					double tmp;
					if(axisdot < 0){
						tmp = cyltcx;
						cyltcx = cylbcx;
						cylbcx = tmp;
						
						tmp = cyltcy;
						cyltcy = cylbcy;
						cylbcy = tmp;
						
						tmp = cyltcz;
						cyltcz = cylbcz;
						cylbcz = tmp;
					}
					
					double tctcaxisdis = axisx * (cyltcx - tcx) + axisy * (cyltcy - tcy) + axisz * (cyltcz - tcz);
					double axisdissqrt = (cyltcx - tcx) * (cyltcx - tcx)
							+ (cyltcy - tcy) * (cyltcy - tcy)
							+ (cyltcz - tcz) * (cyltcz - tcz)
							- tctcaxisdis * tctcaxisdis;
					double axisdis = sqrt(axisdissqrt);
					double tcbcaxisdis = axisx * (cylbcx - tcx) + axisy * (cylbcy - tcy) + axisz * (cylbcz - tcz);
					double bctcaxisdis = axisx * (cyltcx - bcx) + axisy * (cyltcy - bcy) + axisz * (cyltcz - bcz);
					double bcbcaxisdis = axisx * (cylbcx - bcx) + axisy * (cylbcy - bcy) + axisz * (cylbcz - bcz);
					
					double axisdisx = (cyltcx - tcx) - axisx * axisdis;
					double axisdisy = (cyltcy - tcy) - axisy * axisdis;
					double axisdisz = (cyltcz - tcz) - axisz * axisdis;
					axisdisx /= axisdis;
					axisdisy /= axisdis;
					axisdisz /= axisdis;
					
					if(axisdis > cyl.radius + this.radius){
						// 절대 안 겹친다.
						if(cpi == null)
							return false;
						
						if(tctcaxisdis >= 0.0 && tcbcaxisdis >= 0.0){
							// cyl is 'upper' of this cylinder
							// this의 tc와 cyl의 bc connect
							cpi.aType = cpi.bType = ClosestPosInfo.TYPE_VERTEX;
							cpi.aPoints = new double[]{
								tcx + axisdisx * this.radius,
								tcy + axisdisy * this.radius,
								tcz + axisdisz * this.radius
							};
							cpi.bPoints = new double[]{
								cylbcx - axisdisx * cyl.radius,
								cylbcy - axisdisy * cyl.radius,
								cylbcz - axisdisz * cyl.radius
							};
							cpi.distance = sqrt((axisdis - this.radius - cyl.radius)
									* (axisdis - this.radius - cyl.radius) - tcbcaxisdis * tcbcaxisdis);
							cpi.intersects = ClosestPosInfo.ITSC_NO;
						}else if(bctcaxisdis <= 0.0 && bcbcaxisdis <= 0.0){
							// cyl is under this cylinder
							// this의 tc와 cyl의 bc connect
							cpi.aType = cpi.bType = ClosestPosInfo.TYPE_VERTEX;
							cpi.aPoints = new double[]{
								bcx + axisdisx * this.radius,
								bcy + axisdisy * this.radius,
								bcz + axisdisz * this.radius
							};
							cpi.bPoints = new double[]{
								cyltcx - axisdisx * cyl.radius,
								cyltcy - axisdisy * cyl.radius,
								cyltcz - axisdisz * cyl.radius
							};
							cpi.distance = sqrt((axisdis - this.radius - cyl.radius)
									* (axisdis - this.radius - cyl.radius) - bctcaxisdis * bctcaxisdis);
							cpi.intersects = ClosestPosInfo.ITSC_NO;
						}else{
							// this.top face에 걸쳐져 있다
							cpi.aType = cpi.bType = ClosestPosInfo.TYPE_EDGE;
							cpi.aPoints = new double[]{
								(tctcaxisdis >= 0.0 ? tcx : cyltcx) + axisdisx * this.radius,
								(tctcaxisdis >= 0.0 ? tcy : cyltcy) + axisdisy * this.radius,
								(tctcaxisdis >= 0.0 ? tcz : cyltcz) + axisdisz * this.radius,
								(bcbcaxisdis >= 0.0 ? cylbcx : bcx) - axisdisx * (axisdis - this.radius),
								(bcbcaxisdis >= 0.0 ? cylbcy : bcy) - axisdisy * (axisdis - this.radius),
								(bcbcaxisdis >= 0.0 ? cylbcz : bcz) - axisdisz * (axisdis - this.radius)
							};
							cpi.bPoints = new double[]{
								(tctcaxisdis >= 0.0 ? tcx : cyltcx) + axisdisx * (axisdis - cyl.radius),
								(tctcaxisdis >= 0.0 ? tcy : cyltcy) + axisdisy * (axisdis - cyl.radius),
								(tctcaxisdis >= 0.0 ? tcz : cyltcz) + axisdisz * (axisdis - cyl.radius),
								(bcbcaxisdis >= 0.0 ? cylbcx : bcx) - axisdisx * cyl.radius,
								(bcbcaxisdis >= 0.0 ? cylbcy : bcy) - axisdisy * cyl.radius,
								(bcbcaxisdis >= 0.0 ? cylbcz : bcz) - axisdisz * cyl.radius
							};
							cpi.distance = axisdis - cyl.radius - this.radius;
							cpi.intersects = ClosestPosInfo.ITSC_NO;
						}
					}else{
						if(!(tcbcaxisdis >= 0.0 || bctcaxisdis <= 0.0)){
							// overlay
							if(cpi == null)
								return true;
							cpi.distance = -1;
							cpi.intersects = ClosestPosInfo.ITSC_YES;
						}else{
							if(cpi == null)
								return false;

							super._distance(cyl.vertices, cyl.faces, cyl.normals, thistrans, bobjtrans, cpi);
							
							return false;
						}
					}
				}else{
					if(cpi == null)
						cpi = new ClosestPosInfo();
					super._distance(cyl.vertices, cyl.faces, cyl.normals, thistrans, bobjtrans, cpi);
					
					BugManager.printf("BoundObject : Cyl : _distance : dis : %f\n", cpi.distance);
					
					return cpi.intersects == ClosestPosInfo.ITSC_YES;
				}
			}else if(bobj instanceof ConvexPolyhedron){
				return super.intersects(bobj, thistrans, bobjtrans, cpip);
			}else if(bobj instanceof Sphere){
				Sphere sph = (Sphere)bobj;
				
				double scx = sph.centerX, scy = sph.centerY, scz = sph.centerZ;
				if(bobjtrans != null){
					scx = bobjtrans.m00 * sph.centerX + bobjtrans.m01 * sph.centerY
						+ bobjtrans.m02 * sph.centerZ + bobjtrans.m03;
					scy = bobjtrans.m10 * sph.centerX + bobjtrans.m11 * sph.centerY
						+ bobjtrans.m12 * sph.centerZ + bobjtrans.m13;
					scz = bobjtrans.m20 * sph.centerX + bobjtrans.m21 * sph.centerY
						+ bobjtrans.m22 * sph.centerZ + bobjtrans.m23;
				}
				//printf("Bobj : Cyl : getDist : Sphere : sc : %f %f %f\n", scx,scy, scz);
				double sctcx = scx - tcx,
						sctcy = scy - tcy,
						sctcz = scz - tcz;// the center of the top of the cylinder -> the center of the sphere
				double scbcx = scx - bcx,
						scbcy = scy - bcy,
						scbcz = scz - bcz;
				double tcmlen = sctcx * axisx + sctcy * axisy + sctcz * axisz;
				// if tcmlen < 0 : sc-tc is not forward to axis
				double bcmlen = scbcx * axisx + scbcy * axisy + scbcz * axisz;
				// m : The closest point on the axis from sc
				double mx = tcx + tcmlen * axisx,
						my = tcy + tcmlen * axisy,
						mz = tcz + tcmlen * axisz;
				
				// distance btwn sphere center and m (shortest dist from axis)
				double distsqre = (mx - scx) * (mx - scx) + (my - scy) * (my - scy) + (mz - scz) * (mz - scz);
				double dist = Math.sqrt(distsqre);
				// NOT does sphere meet infinite cylinder? 
				// when intersected or sphere is under cylinder this is false.
				boolean isCylAxisOutOfSph = dist > (radius + sph.radius);
				
				//printf("Bobj : Cyl : getDist : Sphere : \n\tthis.rad : %f,\n\tdis btwn axis and sph : %f\n", this.radius, Math.sqrt(distsqre));
				if(cpi != null)
					cpi.aType = cpi.bType = ClosestPosInfo.TYPE_VERTEX;

				if(tcmlen * bcmlen < 0.0){
					if(isCylAxisOutOfSph){
						if(cpi == null)
							return false;
						// m to sphere center(perpendicular to axis) normal vector
						double scmx = (scx - mx) / dist,
								scmy = (scy - my) / dist,
								scmz = (scz - mz) / dist;
						
						// the closest pos is on the surface(side face) of cylinder
						cpi.intersects = ClosestPosInfo.ITSC_NO;
						cpi.distance = dist - this.radius - sph.radius;
						cpi.aPoints = new double[]{
							// on the cylinder
							mx + scmx * this.radius,
							my + scmy * this.radius,
							mz + scmz * this.radius
						};
						cpi.bPoints = new double[]{
							// on the sphere
							scx - scmx * sph.radius,
							scy - scmy * sph.radius,
							scz - scmz * sph.radius
						};
					}else{
						if(cpi == null)
							return true;
						cpi.intersects = ClosestPosInfo.ITSC_YES;
						cpi.distance = -1;
					}
				}else{
					double len = Math.min(Math.abs(tcmlen), Math.abs(bcmlen));
					
					if(dist > this.radius){
						if((len * len + (distsqre + this.radius * this.radius
									- 2.0 * this.radius * dist)) < sph.radiusSqre){
							if(cpi == null)
								return true;
							cpi.distance = -1;
							cpi.intersects = ClosestPosInfo.ITSC_YES;
						}else{
							if(cpi == null)
								return false;
							
							// m to sphere center(perpendicular to axis) normal vector
							double scmx = (scx - mx) / dist,
									scmy = (scy - my) / dist,
									scmz = (scz - mz) / dist;

							cpi.intersects = ClosestPosInfo.ITSC_NO;
							if(tcmlen > 0){
								cpi.aPoints = new double[]{
									// on the cylinder
									scmx * this.radius + tcx,
									scmy * this.radius + tcy,
									scmz * this.radius + tcz,
								};
							}else{
								cpi.aPoints = new double[]{
									// on the cylinder
									scmx * this.radius + bcx,
									scmy * this.radius + bcy,
									scmz * this.radius + bcz,
								};
							}
							double tmpx = cpi.aPoints[0] - scx,
									tmpy = cpi.aPoints[1] - scy,
									tmpz = cpi.aPoints[2] - scz;
							double tmplen = (double)Math.sqrt(tmpx * tmpx + tmpy * tmpy + tmpz * tmpz);
							cpi.distance = tmplen - sph.radius;
							tmpx /= tmplen;
							tmpy /= tmplen;
							tmpz /= tmplen;
							cpi.bPoints = new double[]{
								// on the sphere
								scx + sph.radius * tmpx,
								scy + sph.radius * tmpy,
								scz + sph.radius * tmpz,
							};
						}
					}else{
						// The shortest dist point is on the top(or bottom) of cylinder
						if(len > sph.radius){
							if(cpi == null)
								return false;
							
							cpi.intersects = ClosestPosInfo.ITSC_NO;
							cpi.distance = len - sph.radius;
							cpi.aType = ClosestPosInfo.TYPE_VERTEX;
							cpi.bType = ClosestPosInfo.TYPE_VERTEX;
							if(tcmlen > 0){
								cpi.aPoints = new double[]{
									// on the cylinder
									(scx - mx) + tcx,
									(scy - my) + tcy,
									(scz - mz) + tcz,
								};
								cpi.bPoints = new double[]{
									// on the sphere
									scx - axisx * sph.radius,
									scy - axisy * sph.radius,
									scz - axisz * sph.radius
								};
							}else{
								cpi.aPoints = new double[]{
									// on the cylinder
									(scx - mx) + bcx,
									(scy - my) + bcy,
									(scz - mz) + bcz,
								};
								cpi.bPoints = new double[]{
									// on the sphere
									scx + axisx * sph.radius,
									scy + axisy * sph.radius,
									scz + axisz * sph.radius
								};
							}
						}else{
							if(cpi == null)
								return true;
							// len < sph rad
							cpi.intersects = ClosestPosInfo.ITSC_YES;
							cpi.distance = -1;
						}
					}
				}
			}else if(bobj instanceof Plane){
				Plane pl = (Plane)bobj;
				double pnx = pl.normalX,
						pny = pl.normalY,
						pnz = pl.normalZ;
				double papopx = pl.arbitPOnPlane.x,
						papopy = pl.arbitPOnPlane.y,
						papopz = pl.arbitPOnPlane.z;
				
				if(bobjtrans != null){
					pnx = bobjtrans.m00 * pl.normalX + bobjtrans.m01 * pl.normalY + bobjtrans.m02 * pl.normalZ;
					pny = bobjtrans.m10 * pl.normalX + bobjtrans.m11 * pl.normalY + bobjtrans.m12 * pl.normalZ;
					pnz = bobjtrans.m20 * pl.normalX + bobjtrans.m21 * pl.normalY + bobjtrans.m22 * pl.normalZ;

					papopx = pl.arbitPOnPlane.x * bobjtrans.m00
							+ pl.arbitPOnPlane.y * bobjtrans.m01
							+ pl.arbitPOnPlane.z * bobjtrans.m02
							+ bobjtrans.m03;
					papopy = pl.arbitPOnPlane.x * bobjtrans.m10
							+ pl.arbitPOnPlane.y * bobjtrans.m11
							+ pl.arbitPOnPlane.z * bobjtrans.m12
							+ bobjtrans.m13;
					papopz = pl.arbitPOnPlane.x * bobjtrans.m20
							+ pl.arbitPOnPlane.y * bobjtrans.m21
							+ pl.arbitPOnPlane.z * bobjtrans.m22
							+ bobjtrans.m23;
				}
				
				double pnaxdot = axisx * pnx
							+ axisy * pny
							+ axisz * pnz;

				if(BugManager.DEBUG && DEBUG)
					printf("BObj : Cyl : getDist : Plane : sintheta : %g\n", pnaxdot);
				if(Math.abs(pnaxdot) < BoundObject.DIST_EPSILON){
					// parallel
					// check distance btwn tc and plane : 
					double dist = Math.abs(
							  pnx * (tcx - papopx)
							+ pny * (tcy - papopy)
							+ pnz * (tcz - papopz));

					if(BugManager.DEBUG && DEBUG)
						printf("Bobj : Cyl : getDist : Pl : dist : %f\n\tnormal : %f %f %f\n\tpapop : %f %f %f\n",
							dist, pnx, pny, pnz, papopx, papopy, papopz);
					if(dist <= this.radius){
						if(cpi == null)
							return true;
						
						cpi.intersects = ClosestPosInfo.ITSC_YES;
						cpi.distance = -1;
					}else{
						if(cpi == null)
							return false;
						
						cpi.aType = cpi.bType = ClosestPosInfo.TYPE_EDGE;
						// STABLE CODE
						cpi.bPoints = new double[]{
								tcx - dist * pnx,
								tcy - dist * pny,
								tcz - dist * pnz,
								bcx - dist * pnx,
								bcy - dist * pny,
								bcz - dist * pnz
						};
						cpi.aPoints = new double[]{
								tcx - this.radius * pnx,
								tcy - this.radius * pny,
								tcz - this.radius * pnz,
								bcx - this.radius * pnx,
								bcy - this.radius * pny,
								bcz - this.radius * pnz
						};

						if(BugManager.DEBUG && DEBUG){
							printf("Bobj : Cyl : getDist : Plane : onFace check : \n");
							printf("   plane.onFace(cpi.bPoints) : %b\n", bobj.onFace(bobjtrans, cpi.bPoints[0], cpi.bPoints[1], cpi.bPoints[2], null));
						}
						cpi.distance = dist - this.radius;
						cpi.intersects = ClosestPosInfo.ITSC_NO;
					}
				}else if(Math.abs(pnaxdot) < 0.999){
					// perpendicularSW
					double dis1 = pnx * (tcx - papopx) 
								+ pny * (tcy - papopy)
								+ pnz * (tcz - papopz);
					double dis2 = pnx * (bcx - papopx)
								+ pny * (bcy - papopy)
								+ pnz * (bcz - papopz);
					if(dis1 * dis2 <= 0.0){
						if(cpi == null)
							return true;
						
						cpi.distance = -1;
						cpi.intersects = ClosestPosInfo.ITSC_YES;
					}else{
						if(cpi == null)
							return false;
						
						double absdis1 = Math.abs(dis1);
						double absdis2 = Math.abs(dis2);
						if(absdis1 < absdis2){
							cpi.distance = Math.min(Math.abs(dis1), Math.abs(dis2));
							// FIXME : type must be a filled circle
							cpi.aType = cpi.bType = ClosestPosInfo.TYPE_VERTEX;
							cpi.aPoints = new double[]{
								tcx, tcy, tcz	
							};
							cpi.bPoints = new double[]{
								tcx - pnx * dis1,
								tcy - pny * dis1,
								tcz - pnz * dis1
							};
							cpi.intersects = ClosestPosInfo.ITSC_NO;
						}else{
							cpi.distance = Math.min(Math.abs(dis1), Math.abs(dis2));
							// FIXME : type must be filled circle
							cpi.aType = cpi.bType = ClosestPosInfo.TYPE_VERTEX;
							cpi.aPoints = new double[]{
								bcx, bcy, bcz	
							};
							cpi.bPoints = new double[]{
								bcx - pnx * dis2,
								bcy - pny * dis2,
								bcz - pnz * dis2
							};
							cpi.intersects = ClosestPosInfo.ITSC_NO;
						}
					}
				}else{
					// valid coordinate : 
					double vcx = 0, vcy = 0, vcz = 0;
					double vflag = (axisx * pnx + axisy * pny + axisz * pnz);
					if(vflag < 0.0){
						// top
						vcx = tcx;
						vcy = tcy;
						vcz = tcz;
					}else{
						vcx = bcx;
						vcy = bcy;
						vcz = bcz;
					}
					
					double sintheta = Math.abs(vflag), costheta = (double)Math.sqrt(1.0 - vflag * vflag);
					
					// (distance btwn bc and an intersected point of the extension of the axis and the plane) * sintheta
					double d = Math.abs(pnx * (vcx - papopx) + pny * (vcy - papopy) + pnz * (vcz - papopz));
					double d2 = radius * costheta;
					if(d2 > d){
						if(cpi == null)
							return true;
						
						cpi.distance = -1;
						cpi.intersects = ClosestPosInfo.ITSC_YES;
					}else{
						if(cpi == null)
							return false;
						
						// projection of vc
						double mx = vcx - d * pnx,
								my = vcy - d * pny,
								mz = vcz - d * pnz;

						if(BugManager.DEBUG && DEBUG)
							printf("Bobj : Cyl : getDist : m : %f %f %f\n\td2 : %f\n", mx, my, mz, d2);
						// projection of axis
						double maxisx = pnx * pnaxdot - axisx,
								maxisy = pny * pnaxdot - axisy,
								maxisz = pnz * pnaxdot - axisz;
						
						double c2rate = this.radius * sintheta / costheta;
						cpi.bPoints = new double[]{
								// coord name : C2,
								// the point on the plane, which is closest from the cylinder
								mx + maxisx * c2rate,
								my + maxisy * c2rate,
								mz + maxisz * c2rate
						};
						// distance = |C2 - C1| = |Tc - M| - |Tc - M'|
						//          = dsin theta - rcos theta
						cpi.distance = d - d2;
						cpi.aPoints = new double[]{
								// coord name : c1,
								// on the cylinder
								cpi.bPoints[0] + cpi.distance * pnx,
								cpi.bPoints[1] + cpi.distance * pny,
								cpi.bPoints[2] + cpi.distance * pnz
						};
						cpi.aType = cpi.bType = ClosestPosInfo.TYPE_VERTEX;
						cpi.intersects = ClosestPosInfo.ITSC_NO;
					}
				}
			}
			return cpi.intersects == ClosestPosInfo.ITSC_YES;
		}// end getDistance
		
		
		
		public boolean isVertex(Matrix4d trans, double x, double y, double z)
		{ return false; }
		public boolean onEdge(Matrix4d trans, double px, double py, double pz, Vector3d output_edgdir){
			double bcx = baseCx, bcy = baseCy, bcz = baseCz;
			double tcx = topCx, tcy = topCy, tcz = topCz;
			if(trans != null){
				bcx = trans.m00 * baseCx + trans.m01 * baseCy + trans.m02 * baseCz + trans.m03;
				bcy = trans.m10 * baseCx + trans.m11 * baseCy + trans.m12 * baseCz + trans.m13;
				bcz = trans.m20 * baseCx + trans.m21 * baseCy + trans.m22 * baseCz + trans.m23;

				tcx = trans.m00 * topCx + trans.m01 * topCy + trans.m02 * topCz + trans.m03;
				tcy = trans.m10 * topCx + trans.m11 * topCy + trans.m12 * topCz + trans.m13;
				tcz = trans.m20 * topCx + trans.m21 * topCy + trans.m22 * topCz + trans.m23;
			}
			double mx = tcx - bcx, my = tcy - bcy, mz = tcz - bcz;
			double mlen = (double)Math.sqrt(mx * mx + my * my + mz * mz);
			mx /= mlen;
			my /= mlen;
			mz /= mlen;
			double ptcdotm = (px - tcx) * mx + (py - tcy) * my + (pz - tcz) * mz;
			if((		(px - tcx) * (px - tcx)
						+ (py - tcy) * (py - tcy)
						+ (pz - tcz) * (pz - tcz)
						- (ptcdotm * ptcdotm))
					- radius * radius
						> BoundObject.DIST_EPSILON)
				// Distance from the axis must be R
				return false;

			if(Math.abs((px - tcx) * mx + (py - tcy) * my + (pz - tcz) * mz) < BoundObject.DIST_EPSILON){
				// p is on the edge of the circle which center is tc
				output_edgdir.x =   my * (pz - tcz) - mz * (py - tcy);
				output_edgdir.y = -(mx * (pz - tcz) - mz * (px - tcx));
				output_edgdir.z =   mx * (py - tcy) - my * (px - tcx);
				double len = output_edgdir.length();
				output_edgdir.x /= len;
				output_edgdir.y /= len;
				output_edgdir.z /= len;
				return true;
			}else if(Math.abs((px - bcx) * mx + (py - bcy) * my + (pz - bcz) * mz) < BoundObject.DIST_EPSILON){
				output_edgdir.x = -(my * (pz - bcz) - mz * (py - bcy));
				output_edgdir.y =   mx * (pz - bcz) - mz * (px - bcx);
				output_edgdir.z = -(mx * (py - bcy) - my * (px - bcx));
				double len = output_edgdir.length();
				output_edgdir.x /= len;
				output_edgdir.y /= len;
				output_edgdir.z /= len;
				return true;
			}
			return false;
		}// end onEdge
		
		@StableCode public boolean onFace(Matrix4d trans, double px, double py, double pz, Vector3d output_normal){
			double bcx = baseCx, bcy = baseCy, bcz = baseCz;
			double tcx = topCx, tcy = topCy, tcz = topCz;
			if(trans != null){
				bcx = trans.m00 * baseCx + trans.m01 * baseCy + trans.m02 * baseCz + trans.m03;
				bcy = trans.m10 * baseCx + trans.m11 * baseCy + trans.m12 * baseCz + trans.m13;
				bcz = trans.m20 * baseCx + trans.m21 * baseCy + trans.m22 * baseCz + trans.m23;

				tcx = trans.m00 * topCx + trans.m01 * topCy + trans.m02 * topCz + trans.m03;
				tcy = trans.m10 * topCx + trans.m11 * topCy + trans.m12 * topCz + trans.m13;
				tcz = trans.m20 * topCx + trans.m21 * topCy + trans.m22 * topCz + trans.m23;
			}
			
			double bpx = px - bcx, bpy = py - bcy, bpz = pz - bcz;
			double tpx = px - tcx, tpy = py - tcy, tpz = pz - tcz;
			double mx = tcx - bcx, my = tcy - bcy, mz = tcz - bcz;
				double _tmpmlen = (double)Math.sqrt(mx * mx + my * my + mz * mz);
				mx /= _tmpmlen;
				my /= _tmpmlen;
				mz /= _tmpmlen;
			
			double bpmdot = bpx * mx + bpy * my + bpz * mz;
			double tpmdot = tpx * mx + tpy * my + tpz * mz;
			if(BugManager.DEBUG && DEBUG)
				printf("Bobj : Cylinder : onFace : " +
						"\n\tp : %f, %f, %f" + 
						"\n\tbc : %.3f, %.3f, %.3f" + 
						"\n\ttc : %.3f, %.3f, %.3f" +
						"\n\tbpmdot : %f, tpmdot : %f\n", px, py, pz, bcx, bcy, bcz, tcx, tcy, tcz, bpmdot, tpmdot);
			if(Math.abs(bpmdot) <= BoundObject.DIST_EPSILON){
				double rho = bpx * bpx + bpy * bpy + bpz * bpz;
				if(BugManager.DEBUG && DEBUG)
					printf("Bobj : Cylinder : onFace : a pnt on edge of bc,\n   rho : %f, radius * rad : %f\n", rho, this.radius * radius);
				if(rho < this.radius * radius - BoundObject.DIST_EPSILON){
					output_normal.x = -mx;
					output_normal.y = -my;
					output_normal.z = -mz;
					return true;
				}else
					return false;
			}else if(Math.abs(tpmdot) <= BoundObject.DIST_EPSILON){
				double rho = tpx * tpx + tpy * tpy + tpz * tpz;
				if(BugManager.DEBUG && DEBUG)
					printf("Bobj : Cylinder : onFace : a pnt on edge of tc,\n   rho : %f, radius * rad : %f\n", rho, this.radius * radius);
				if(rho < this.radius * radius - BoundObject.DIST_EPSILON){
					output_normal.x = mx;
					output_normal.y = my;
					output_normal.z = mz;
					return true;
				}else
					return false;
			}else if(bpmdot * tpmdot < 0.0){
				if(Math.abs((bpx * bpx + bpy * bpy + bpz * bpz) - bpmdot * bpmdot - this.radius * this.radius) < BoundObject.DIST_EPSILON){
					// direction = hat(p - m)
					output_normal.x = px - bcx - mx * bpmdot;
					output_normal.y = py - bcy - my * bpmdot;
					output_normal.z = pz - bcz - mz * bpmdot;
					output_normal.normalize();
					return true;
				}else
					return false;
			}
			return false;
		}// end onFace(Matrix4d, double, double, double, Vector3d)
		
		
		public Cylinder clone(){
			Cylinder c = new Cylinder(baseCx, baseCy, baseCz, topCx, topCy, topCz, radius);
			return c;
		}// end clone()
		
		public String toString(){
			return "Cylinder(base:" + baseCx + "," + baseCy + "," + baseCz + ", top:" + topCx + "," + topCy + "," + topCz + ", rad:" + radius + ")";
		}// end toStr
	}// end class Cylinder
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static class ConvexPolyhedron extends BoundObject{
		private final static double[][] _S6x6 = new double[][]{
			{2, 0, 0, /*|*/ -2, 0, 0},
			{0, 2, 0, /*|*/ 0, -2, 0},
			{0, 0, 2, /*|*/ 0, 0, -2},
			
			{-2, 0, 0, /*|*/ 2, 0, 0},
			{0, -2, 0, /*|*/ 0, 2, 0},
			{0, 0, -2, /*|*/ 0, 0, 2},
		};
		private final static double[][] _S3x3 = new double[][]{
			{2, 0, 0},
			{0, 2, 0},
			{0, 0, 2}
		};
		
		protected Point3d[] vertices;
		protected int[][] faces;
		protected Vector3d[] normals;
		
		private Vector3d opt_mv;
		
		
		public void set(Point3f[] vertices, int[][] faces){
			this.vertices = new Point3d[vertices.length];
			for(int i = 0; i < vertices.length; i++)
				this.vertices[i] = new Point3d(vertices[i]);
			
			this.faces = new int[faces.length][];
			for(int i = 0; i < faces.length; i++)
				this.faces[i] = faces[i].clone();
			
			normals = new Vector3d[faces.length];
			MathUtility.generateNormals(this.vertices, faces, normals);
			_optimize();
		}// end set(Point3f[], int[][])
		public void set(Point3f[] vertices, int[][] faces, Vector3d[] normals){
			this.vertices = new Point3d[vertices.length];
			for(int i = 0; i < vertices.length; i++)
				this.vertices[i] = new Point3d(vertices[i]);
			
			this.faces = new int[faces.length][];
			for(int i = 0; i < faces.length; i++)
				this.faces[i] = faces[i].clone();
		
			this.normals = normals;
			_optimize();
		}// end set
		public int[][] getFaces()
		{ return faces; }
		public Point3d[] getVertices()
		{ return vertices; }
		public Vector3d[] getNormals()
		{ return normals; }
		
		private void _optimize(){
			if(opt_mv == null)
				opt_mv = new Vector3d();
			else
				opt_mv.set(0, 0, 0);
			
			for(Point3d eachv : vertices){
				opt_mv.x = eachv.x < opt_mv.x ? eachv.x : opt_mv.x;
				opt_mv.y = eachv.y < opt_mv.y ? eachv.y : opt_mv.y;
				opt_mv.z = eachv.z < opt_mv.z ? eachv.z : opt_mv.z;
			}
		}// end _optimize()
		
		
		
		public void translate(double x, double y, double z){
			for(int i = 0; i < vertices.length; i++){
				vertices[i].x += x;
				vertices[i].y += y;
				vertices[i].z += z;
			}
			_optimize();
		}// end translate
		
		
		public boolean intersects(BoundObject bobj, Matrix4d thistrans, Matrix4d bobjtrans, ArrayList<ClosestPosInfo> cpip){
			if(bobj instanceof CompoundBounds){
				return bobj.intersects(this, bobjtrans, thistrans, cpip);
			}

			ClosestPosInfo cpi = null;
			if(cpip != null){
				cpip.add(cpi = new ClosestPosInfo());
				cpi.aBoundsObj = this;
				cpi.bBoundsObj = bobj;
			}

			
			if(bobj instanceof ConvexPolyhedron){
				ConvexPolyhedron bobjcp = (ConvexPolyhedron)bobj;
				if(cpi == null)
					cpi = new ClosestPosInfo();
				this._distance(bobjcp.vertices, bobjcp.faces, bobjcp.normals, thistrans, bobjtrans, cpi);
				
				System.out.printf("ConvexPh : dist : %f, closest p on thisobj : %f %f %f, bobj : %f %f %f\n",
						cpi.distance,
						cpi.aPoints[0], cpi.aPoints[1], cpi.aPoints[2],
						cpi.bPoints[0], cpi.bPoints[1], cpi.bPoints[2]);
				return cpi.intersects == ClosestPosInfo.ITSC_YES;
			}
			else if(bobj instanceof Sphere){
				Sphere sph = (Sphere)bobj;

				double sx = sph.getCenterX() * bobjtrans.m00 + sph.getCenterY() * bobjtrans.m01 + sph.getCenterZ() * bobjtrans.m02 + bobjtrans.m03;
				double sy = sph.getCenterX() * bobjtrans.m10 + sph.getCenterY() * bobjtrans.m11 + sph.getCenterZ() * bobjtrans.m12 + bobjtrans.m13;
				double sz = sph.getCenterX() * bobjtrans.m20 + sph.getCenterY() * bobjtrans.m21 + sph.getCenterZ() * bobjtrans.m22 + bobjtrans.m23;
				
				if(cpi == null)
					cpi = new ClosestPosInfo();
				
				this._distance(sx, sy, sz, thistrans, cpi);
				if(cpi.distance < sph.getRadius()){
					//cpi.distance = -1;
					cpi.intersects = ClosestPosInfo.ITSC_YES;
				}else{
					double tmpx = cpi.aPoints[0] - sx,
							tmpy = cpi.aPoints[1] - sy,
							tmpz = cpi.aPoints[2] - sz;
					double tmplen = (double)Math.sqrt(tmpx * tmpx + tmpy * tmpy + tmpz * tmpz);
					// normalizing
					tmpx /= tmplen;
					tmpy /= tmplen;
					tmpz /= tmplen;

					cpi.bPoints[0] = cpi.bPoints[0] + tmpx * sph.getRadius();
					cpi.bPoints[1] = cpi.bPoints[1] + tmpy * sph.getRadius();
					cpi.bPoints[2] = cpi.bPoints[2] + tmpz * sph.getRadius();
					cpi.distance -= sph.getRadius();
					cpi.aType = cpi.bType = ClosestPosInfo.TYPE_VERTEX;
					cpi.intersects = ClosestPosInfo.ITSC_NO;
				}
				if(BugManager.DEBUG && DEBUG)
					printf("Bobj : CvxPlydr : getDistance : cpi.dist : %f,\n" +
						"\tcpi.aPoints : %.3f, %.3f, %.3f\n" + 
						"\tcpi.bPoints : %.3f, %.3f, %.3f\n", cpi.distance, cpi.aPoints[0], cpi.aPoints[1], cpi.aPoints[2], cpi.bPoints[0], cpi.bPoints[1], cpi.bPoints[2]);
				
				return cpi.intersects == ClosestPosInfo.ITSC_YES;
			}else if(bobj instanceof Plane){
				Plane plane = (Plane)bobj;
				
				Matrix4d planetrans = new Matrix4d();
				if(thistrans != null){
					MathUtility.fastInvert(thistrans, planetrans);
				}else{
					planetrans.m00 = 1; planetrans.m01 = 0; planetrans.m02 = 0; planetrans.m03 = 0;
					planetrans.m10 = 0; planetrans.m11 = 1; planetrans.m12 = 0; planetrans.m13 = 0;
					planetrans.m20 = 0; planetrans.m21 = 0; planetrans.m22 = 1; planetrans.m23 = 0;
					planetrans.m30 = 0; planetrans.m31 = 0; planetrans.m32 = 0; planetrans.m33 = 1;
				}
				if(bobjtrans != null)
					planetrans.mul(bobjtrans);

				if(BugManager.DEBUG && DEBUG){
					printf("Bobj : CvPoly : getDis : getPanel: \n\tthisloc : %f %f %f\n", thistrans.m03, thistrans.m13, thistrans.m23);
					printf("\tbobjloc : %f %f %f\n", bobjtrans.m03, bobjtrans.m13, bobjtrans.m23);
				}
				
				double nx = plane.normalX * planetrans.m00 + plane.normalY * planetrans.m01 + plane.normalZ * planetrans.m02,
					ny = plane.normalX * planetrans.m10 + plane.normalY * planetrans.m11 + plane.normalZ * planetrans.m12,
					nz = plane.normalX * planetrans.m20 + plane.normalY * planetrans.m21 + plane.normalZ * planetrans.m22;
				
				double px = plane.arbitPOnPlane.x * planetrans.m00
							+ plane.arbitPOnPlane.y * planetrans.m01
							+ plane.arbitPOnPlane.z * planetrans.m02
							+ planetrans.m03,
					py = plane.arbitPOnPlane.x * planetrans.m10
							+ plane.arbitPOnPlane.y * planetrans.m11
							+ plane.arbitPOnPlane.z * planetrans.m12
							+ planetrans.m13,
					pz = plane.arbitPOnPlane.x * planetrans.m20
							+ plane.arbitPOnPlane.y * planetrans.m21
							+ plane.arbitPOnPlane.z * planetrans.m22
							+ planetrans.m23;
				// 위의 nx, ny, nz의 경우는 상대좌표계에서의 계산을 위해 bobjtrans가 아닌 planetrans를 곱한다.
				// 이것을 절대좌표계로 되돌일 일이 나중에 생기는데, 이 때 thistrans를 곱해주면 오차가 매우 크다.
				// 따라서 아래에 미리 bobjtrans를 곱한 normal값을 계산해 두는 것이다.(absolute coordinate)
				double absnx = plane.normalX,
						absny = plane.normalY,
						absnz = plane.normalZ;
				double abpx = plane.arbitPOnPlane.x,
						abpy = plane.arbitPOnPlane.y,
						abpz = plane.arbitPOnPlane.z;
				if(bobjtrans != null){
					absnx = plane.normalX * bobjtrans.m00 + plane.normalY * bobjtrans.m01 + plane.normalZ * bobjtrans.m02;
					absny = plane.normalX * bobjtrans.m10 + plane.normalY * bobjtrans.m11 + plane.normalZ * bobjtrans.m12;
					absnz = plane.normalX * bobjtrans.m20 + plane.normalY * bobjtrans.m21 + plane.normalZ * bobjtrans.m22;

					abpx = plane.arbitPOnPlane.x * bobjtrans.m00
							+ plane.arbitPOnPlane.y * bobjtrans.m01
							+ plane.arbitPOnPlane.z * bobjtrans.m02
							+ bobjtrans.m03;
					abpy = plane.arbitPOnPlane.x * bobjtrans.m10
							+ plane.arbitPOnPlane.y * bobjtrans.m11
							+ plane.arbitPOnPlane.z * bobjtrans.m12
							+ bobjtrans.m13;
					abpz = plane.arbitPOnPlane.x * bobjtrans.m20
							+ plane.arbitPOnPlane.y * bobjtrans.m21
							+ plane.arbitPOnPlane.z * bobjtrans.m22
							+ bobjtrans.m23;
				}

				if(BugManager.DEBUG && DEBUG){
					printf("Bobj : CvPol : Panel : absn : %g %g %g\n", absnx, absny, absnz);
					printf("Bobj : CvPol : Panel : abp : %g %g %g\n", abpx, abpy, abpz);
				}
				
				if(cpi != null){
					cpi.distance = Float.POSITIVE_INFINITY;
					cpi.intersects = ClosestPosInfo.ITSC_UNKNOWN;
					cpi.aType = cpi.bType = ClosestPosInfo.TYPE_VERTEX;
				}
				
				/*
				 * process : 
				 * find the closest vertex
				 *    - if sign is not same : the vertex go through plane
				 *    		dis = -1, intersects = true
				 *    		stop vertex-finding
				 *    - if two vertices found : make type edge
				 *    - if three vertices found : 
				 *    		find the face that contains the three vertices(face normal vec == -plane normal vec)
				 *    		stop vertex-finding
				*/
				
				double dis;
				int closesti = 0;
				
				for(int i = 0; i < this.vertices.length; i++){
					dis = (vertices[i].x - px) * nx
						+ (vertices[i].y - py) * ny
						+ (vertices[i].z - pz) * nz;
					if(dis < 0.0){
						if(cpi == null)
							return true;
						cpi.distance = -1;
						cpi.intersects = ClosestPosInfo.ITSC_YES;
						break;
					}else if(i == 0){
						if(cpi != null){
							cpi.distance = dis; // no abs(dis) : for deriving closest-point-on-plane later
							cpi.intersects = ClosestPosInfo.ITSC_NO;
						}
						closesti = 0;
						continue;
					} 
					
					if(cpi == null)
						continue;
					else if(cpi.aType == ClosestPosInfo.TYPE_FACE)
						// for using upper condition : positivedis(cross btwn polyhedron & plain)
						continue;
					
					if(abs(dis - cpi.distance) < DIST_EPSILON){
						if(cpi.aType == ClosestPosInfo.TYPE_EDGE){
							// make TYPE_FACE
							double val;
							boolean facefound = false;
							for(int j = 0; j < faces.length; j++){
								val = (normals[j].x * nx + normals[j].y * ny + normals[j].z * nz);
								if(1.0 + val <= DIST_EPSILON){
									// found!
									BugManager.printf("ConvexPOly : Plane : making face contact..\n");
									facefound = true;
									cpi.aType = cpi.bType = ClosestPosInfo.TYPE_FACE;
									cpi.aPoints = new double[faces[j].length * 3];
									cpi.bPoints = new double[faces[j].length * 3];
									
									cpi.onFaceCase_normalx = absnx;
									cpi.onFaceCase_normaly = absny;
									cpi.onFaceCase_normalz = absnz;
									
									for(int k = 0; k < faces[j].length; k++){
										cpi.aPoints[3 * k] = vertices[faces[j][k]].x * thistrans.m00
												+ vertices[faces[j][k]].y * thistrans.m01
												+ vertices[faces[j][k]].z * thistrans.m02
												+ thistrans.m03;
												
										cpi.aPoints[3 * k + 1] = vertices[faces[j][k]].x * thistrans.m10
												+ vertices[faces[j][k]].y * thistrans.m11
												+ vertices[faces[j][k]].z * thistrans.m12
												+ thistrans.m13;
										
										cpi.aPoints[3 * k + 2] = vertices[faces[j][k]].x * thistrans.m20
												+ vertices[faces[j][k]].y * thistrans.m21
												+ vertices[faces[j][k]].z * thistrans.m22
												+ thistrans.m23;

										cpi.bPoints[3 * k] = cpi.aPoints[3 * k] - cpi.distance * absnx;
										cpi.bPoints[3 * k + 1] = cpi.aPoints[3 * k + 1] - cpi.distance * absny;
										cpi.bPoints[3 * k + 2] = cpi.aPoints[3 * k + 2] - cpi.distance * absnz;
									}
									break;
								}
							}
							
							if(!facefound){
								BugManager.printf("BUG!!! in BoundObject.ConvexPolyhedron : \n\tn : %f %f %f absn : %f %f %f", nx, ny, nz, absnx, absny, absnz);
								System.exit(-1);
							}
							continue;
						}else{
							BugManager.printf("Bobj : ConvexPly : Plane : making edge..\n");
							// apoint : this, bpoint : plane
							cpi.aType = ClosestPosInfo.TYPE_EDGE;
							cpi.bType = ClosestPosInfo.TYPE_EDGE;
							cpi.aPoints = new double[]{
								vertices[closesti].x * thistrans.m00
										+ vertices[closesti].y * thistrans.m01
										+ vertices[closesti].z * thistrans.m02
										+ thistrans.m03,
								vertices[closesti].x * thistrans.m10
										+ vertices[closesti].y * thistrans.m11
										+ vertices[closesti].z * thistrans.m12
										+ thistrans.m13,
								vertices[closesti].x * thistrans.m20
										+ vertices[closesti].y * thistrans.m21
										+ vertices[closesti].z * thistrans.m22
										+ thistrans.m23,
	
								vertices[i].x * thistrans.m00
										+ vertices[i].y * thistrans.m01
										+ vertices[i].z * thistrans.m02
										+ thistrans.m03,
								vertices[i].x * thistrans.m10
										+ vertices[i].y * thistrans.m11
										+ vertices[i].z * thistrans.m12
										+ thistrans.m13,
								vertices[i].x * thistrans.m20
										+ vertices[i].y * thistrans.m21
										+ vertices[i].z * thistrans.m22
										+ thistrans.m23
							};
							cpi.bPoints = new double[]{
								cpi.aPoints[0] - cpi.distance * absnx,
								cpi.aPoints[1] - cpi.distance * absny,
								cpi.aPoints[2] - cpi.distance * absnz,
								
								cpi.aPoints[3] - dis * absnx,
								cpi.aPoints[4] - dis * absny,
								cpi.aPoints[5] - dis * absnz
							};
							BugManager.printf("\tap1 : %f %f %f bp1 : %f %f %f\n", cpi.aPoints[0], cpi.aPoints[1], cpi.aPoints[2],
									cpi.bPoints[0], cpi.bPoints[1], cpi.bPoints[2]);
							BugManager.printf("\tap2 : %f %f %f bp2 : %f %f %f\n", cpi.aPoints[3], cpi.aPoints[4], cpi.aPoints[5],
									cpi.bPoints[3], cpi.bPoints[4], cpi.bPoints[5]);
							cpi.distance = (dis + cpi.distance) / 2.0;
						}
					}else if(abs(dis) < abs(cpi.distance)){
						cpi.distance = dis;
						closesti = i;
					}
				}
				
				if(cpi == null)
					return false;
				if(cpi.intersects != ClosestPosInfo.ITSC_YES && cpi.aType == ClosestPosInfo.TYPE_VERTEX){
					cpi.aPoints = new double[]{
							vertices[closesti].x * thistrans.m00
									+ vertices[closesti].y * thistrans.m01
									+ vertices[closesti].z * thistrans.m02
									+ thistrans.m03,
							vertices[closesti].x * thistrans.m10
									+ vertices[closesti].y * thistrans.m11
									+ vertices[closesti].z * thistrans.m12
									+ thistrans.m13,
							vertices[closesti].x * thistrans.m20
									+ vertices[closesti].y * thistrans.m21
									+ vertices[closesti].z * thistrans.m22
									+ thistrans.m23
					};
					cpi.bPoints = new double[]{
							cpi.aPoints[0] - cpi.distance * absnx,
							cpi.aPoints[1] - cpi.distance * absny,
							cpi.aPoints[2] - cpi.distance * absnz
					};
				}

				if(BugManager.DEBUG && DEBUG)
					printf("Bobj : CnvxPlyhdr : Plane : getDistance : final distance : %f\n", cpi.distance);
			}
			return cpi.intersects == ClosestPosInfo.ITSC_YES;
		}// end getDistance(BoundObject, Matrix4d, Matrix4d, ClosestPosInfo)
		
		
		
		
		
		
		
		

		protected double _distance(Point3d[] cp_vertices, int[][] cp_faces, Vector3d[] cp_normals,
				Matrix4d thistrans, Matrix4d cptrans, ClosestPosInfo cpi){
			// This source is derived from Game Physics Bible. p. 442
			int n0 = faces.length,
				n1 = cp_faces.length;
			double[][] A = new double[n0 + n1][6];
			double[] b = new double[n0 + n1 + 6];
			double[][] vts0 = new double[vertices.length][3],
					vts1 = new double[cp_vertices.length][3];
			
			double xtrans = 0, ytrans = 0, ztrans = 0;
			
			for(int i = 0; i < n0; i++){
				A[i][0] = thistrans.m00 * normals[i].x + thistrans.m01 * normals[i].y + thistrans.m02 * normals[i].z;
				A[i][1] = thistrans.m10 * normals[i].x + thistrans.m11 * normals[i].y + thistrans.m12 * normals[i].z;
				A[i][2] = thistrans.m20 * normals[i].x + thistrans.m21 * normals[i].y + thistrans.m22 * normals[i].z;
			}
			for(int i = 0; i < vts0.length; i++){
				vts0[i][0] = 
					thistrans.m00 * vertices[i].x + 
					thistrans.m01 * vertices[i].y + 
					thistrans.m02 * vertices[i].z + 
					thistrans.m03;
				vts0[i][1] = 
					thistrans.m10 * vertices[i].x + 
					thistrans.m11 * vertices[i].y + 
					thistrans.m12 * vertices[i].z + 
					thistrans.m13;
				vts0[i][2] = 
					thistrans.m20 * vertices[i].x + 
					thistrans.m21 * vertices[i].y + 
					thistrans.m22 * vertices[i].z + 
					thistrans.m23;

				if(xtrans > vts0[i][0])
					xtrans = vts0[i][0];
				if(ytrans > vts0[i][1])
					ytrans = vts0[i][1];
				if(ztrans > vts0[i][2])
					ztrans = vts0[i][2];
			}

			for(int i = n0; i < n0 + n1; i++){
				A[i][3] = cptrans.m00 * cp_normals[i - n0].x + cptrans.m01 * cp_normals[i - n0].y + cptrans.m02 * cp_normals[i - n0].z;
				A[i][4] = cptrans.m10 * cp_normals[i - n0].x + cptrans.m11 * cp_normals[i - n0].y + cptrans.m12 * cp_normals[i - n0].z;
				A[i][5] = cptrans.m20 * cp_normals[i - n0].x + cptrans.m21 * cp_normals[i - n0].y + cptrans.m22 * cp_normals[i - n0].z;
			}
			for(int i = 0; i < vts1.length; i++){
				vts1[i][0] = 
							cptrans.m00 * cp_vertices[i].x + 
							cptrans.m01 * cp_vertices[i].y + 
							cptrans.m02 * cp_vertices[i].z + 
							cptrans.m03;
				vts1[i][1] = 
							cptrans.m10 * cp_vertices[i].x + 
							cptrans.m11 * cp_vertices[i].y + 
							cptrans.m12 * cp_vertices[i].z + 
							cptrans.m13;
				vts1[i][2] = 
							cptrans.m20 * cp_vertices[i].x + 
							cptrans.m21 * cp_vertices[i].y + 
							cptrans.m22 * cp_vertices[i].z + 
							cptrans.m23;
				
				if(xtrans > vts1[i][0])
					xtrans = vts1[i][0];
				if(ytrans > vts1[i][1])
					ytrans = vts1[i][1];
				if(ztrans > vts1[i][2])
					ztrans = vts1[i][2];
			}
			
			for(int i = 0; i < n0; i++)
				b[i + 6] = A[i][0] * (vts0[faces[i][0]][0] - xtrans)
						+ A[i][1] * (vts0[faces[i][0]][1] - ytrans)
						+ A[i][2] * (vts0[faces[i][0]][2] - ztrans);

			for(int i = n0; i < n0 + n1; i++)
				b[i + 6] = A[i][3] * (vts1[cp_faces[i - n0][0]][0] - xtrans)
						+ A[i][4] * (vts1[cp_faces[i - n0][0]][1] - ytrans)
						+ A[i][5] * (vts1[cp_faces[i - n0][0]][2] - ztrans);
			
			
			double[][] M;
			double[][] A_t = new double[A[0].length][A.length];
			MathUtility.transpose(A, A_t);
			MathUtility.mul(A, -1, A);
			M = MathUtility.block_quad(_S6x6, A_t, A, null);
			double[] w = new double[n0 + n1 + 6],
					z = new double[n0 + n1 + 6];
			int res = MathUtility.LCPSolve(n0 + n1 + 6, M, b, z, w);
			
			z[0] += xtrans; z[3] += xtrans;
			z[1] += ytrans; z[4] += ytrans;
			z[2] += ztrans; z[5] += ztrans;
			double dist = 2147483647;
			if(res != MathUtility.LCP_CANNOT_REMOVE_COMPLEMENTARY){
				dist = (z[3] - z[0]) * (z[3] - z[0])
						+ (z[4] - z[1]) * (z[4] - z[1])
						+ (z[5] - z[2]) * (z[5] - z[2]);
				dist = (double)Math.sqrt(dist);
				
				if(dist < DIST_EPSILON * DIST_EPSILON){
					dist = -1;
					if(cpi != null){
						cpi.intersects = ClosestPosInfo.ITSC_YES;
						cpi.distance = -1;
					}
				}else if(cpi != null){
					cpi.intersects = ClosestPosInfo.ITSC_NO;
					// find normal parallel with (z[0], z[1], z[2]) -> (z[3], z[4], z[5]) from a_object(this)
					// if found : 
					//    find normal parallen with (z[3], z[4], z[5]) -> (z[0], z[1], z[2]) from bobj(Always exist)
					//    -> AND operation
					cpi.distance = dist;
					/*
					boolean facetoface_this = false;
					boolean facetoface_bobj = false;
					int facetoface_thisidx = -1;
					int facetoface_bobjidx = -1;
					double disvecx = (z[3] - z[0]) / dist,
							disvecy = (z[4] - z[1]) / dist,
							disvecz = (z[5] - z[2]) / dist;
					
					for(int i = 0; i < n0; i++){
						// A[i]에 this의 transformed normal이 모두 저장되있음
						if(abs(A[i][0] - disvecx) < DIST_EPSILON
								&& abs(A[i][1] - disvecy) < DIST_EPSILON){
							facetoface_this = true;
							facetoface_thisidx = i;
							break;
						}
					}
					for(int i = 0; i < n1; i++){
						if(abs(A[n0 + i][0] + disvecx) < DIST_EPSILON
								&& abs(A[n0 + i][1] + disvecy) < DIST_EPSILON){
							facetoface_bobjidx = i;
							facetoface_bobj = true;
							break;
						}
					}
					
					if(facetoface_this && facetoface_bobj){
						// FOUND!! facetoface_thisface -> facetoface_bobjface
						cpi.aType = ClosestPosInfo.TYPE_FACE;
						cpi.bType = ClosestPosInfo.TYPE_FACE;
						cpi.onFaceCase_normalx = disvecx;
						cpi.onFaceCase_normaly = disvecy;
						cpi.onFaceCase_normalz = disvecz;
						
					}else if(!facetoface_this && !facetoface_bobj){
						// edge to edge
						cpi.aType = ClosestPosInfo.TYPE_VERTEX;
						cpi.bType = ClosestPosInfo.TYPE_VERTEX;
					}else awefawfeaw
					
					if(cpi.aType == ClosestPosInfo.TYPE_VERTEX){
					}
					*/
					// FIXME...
					cpi.aType = ClosestPosInfo.TYPE_VERTEX;
					cpi.bType = ClosestPosInfo.TYPE_VERTEX;
					cpi.aPoints = new double[]{
						z[0], z[1], z[2]
					};
					cpi.bPoints = new double[]{
						z[3], z[4], z[5]
					};
				}
			}else{
				if(cpi != null){
					cpi.intersects = ClosestPosInfo.ITSC_YES;
					cpi.distance = -1;
					dist = -1;
				}
			}
			return dist;
		}// end _distance
		
		protected double _distance(double px, double py, double pz, Matrix4d thistrans, ClosestPosInfo cpi){
			// The code is generated from Game Physics Bible p.438
			double xtrans = px, ytrans = py, ztrans = pz;
			
			int n = faces.length;
			double[][] A = new double[n][3];
			double[] q = new double[n + 3];
			double[][] vts = new double[vertices.length][3];
			for(int i = 0; i < n; i++){
				A[i][0] = thistrans.m00 * normals[i].x + thistrans.m01 * normals[i].y + thistrans.m02 * normals[i].z;
				A[i][1] = thistrans.m10 * normals[i].x + thistrans.m11 * normals[i].y + thistrans.m12 * normals[i].z;
				A[i][2] = thistrans.m20 * normals[i].x + thistrans.m21 * normals[i].y + thistrans.m22 * normals[i].z;
			}
			for(int i = 0; i < vts.length; i++){
				vts[i][0] = 
					thistrans.m00 * vertices[i].x + 
					thistrans.m01 * vertices[i].y + 
					thistrans.m02 * vertices[i].z + 
					thistrans.m03;
				vts[i][1] = 
					thistrans.m10 * vertices[i].x + 
					thistrans.m11 * vertices[i].y + 
					thistrans.m12 * vertices[i].z + 
					thistrans.m13;
				vts[i][2] = 
					thistrans.m20 * vertices[i].x + 
					thistrans.m21 * vertices[i].y + 
					thistrans.m22 * vertices[i].z + 
					thistrans.m23;
				if(xtrans > vts[i][0])
					xtrans = vts[i][0];
				if(ytrans > vts[i][1])
					ytrans = vts[i][1];
				if(ztrans > vts[i][2])
					ztrans = vts[i][2];
			}
			for(int i = 0; i < n; i++){
				q[i + 3] = A[i][0] * (vts[faces[i][0]][0] - xtrans)
						+ A[i][1] * (vts[faces[i][0]][1] - ytrans)
						+ A[i][2] * (vts[faces[i][0]][2] - ztrans);
			}
			q[0] = -2.0 * (px - xtrans);
			q[1] = -2.0 * (py - ytrans);
			q[2] = -2.0 * (pz - ztrans);
			double[][] A_T = MathUtility.transpose(A, null);
			double[][] M = MathUtility.block_quad(_S3x3, A_T, MathUtility.mul(A, -1, null), null);
			
			/*println("Bobj : M : ");
			for(int i = 0; i < M.length; i++){
				for(int j = 0; j < M[i].length; j++)
					printf("%.3f ", M[i][j]);
				println();
			}
			println("Bobj : Q :");
			for(int i = 0; i < q.length; i++)
				printf("%.3f ", q[i]);
			println();
			*/
			double[] w = new double[n + 3], z = new double[n + 3];
			int res = MathUtility.LCPSolve(n + 3, M, q, z, w);

			if(BugManager.DEBUG && DEBUG)
				printf("BObj : CnvxPlyhdr : _distance(point) : res : %d\n", res);
			
			z[0] += xtrans;
			z[1] += ytrans;
			z[2] += ztrans;

			if(BugManager.DEBUG && DEBUG)
				printf("Bobj : CnvxPlyhdr : z : %f %f %f\n", z[0], z[1], z[2]);
			
			double dis = 2147483647;
			if(res != MathUtility.LCP_CANNOT_REMOVE_COMPLEMENTARY){
				dis = Math.sqrt((z[0] - px) * (z[0] - px)
					+ (z[1] - py) * (z[1] - py)
					+ (z[2] - pz) * (z[2] - pz));
				
				if(cpi != null){
					cpi.intersects = ClosestPosInfo.ITSC_NO;
					cpi.distance = dis;
					cpi.aPoints = new double[]{
							// this : ConvexPolyh
							z[0], z[1], z[2]
					};
					cpi.bPoints = new double[]{
							px, py, pz
					};
					cpi.aType = cpi.bType = ClosestPosInfo.TYPE_VERTEX;
				}
			}else{
				if(cpi != null){
					cpi.intersects = ClosestPosInfo.ITSC_YES;
					cpi.distance = -1;
					dis = -1;
				}
			}
			return dis;
		}// end distance
		
		
		
		
		
		
		
		
		
		
		public boolean isVertex(Matrix4d trans, double px, double py, double pz){
			Matrix4d inv = new Matrix4d();
			fastInvert(trans, inv);
			double x = px * inv.m00 + py * inv.m01 + pz * inv.m02 + inv.m03;
			double y = px * inv.m10 + py * inv.m11 + pz * inv.m12 + inv.m13;
			double z = px * inv.m20 + py * inv.m21 + pz * inv.m22 + inv.m23;
			
			for(int i = 0; i < vertices.length; i++){
				if(Math.abs(fastPow(x - vertices[i].x, 2) + fastPow(y - vertices[i].y, 2) + fastPow(z - vertices[i].z, 2))
						< BoundObject.DIST_EPSILON * BoundObject.DIST_EPSILON)
					return true;
			}
			return false;
		}// end isVertex
		
		public boolean onFace(Matrix4d thistrans, double px, double py, double pz, Vector3d output_normal){
			Matrix3d m3 = new Matrix3d();
			m3.m00 = thistrans.m00; m3.m01 = thistrans.m01; m3.m02 = thistrans.m02;
			m3.m10 = thistrans.m10; m3.m11 = thistrans.m11; m3.m12 = thistrans.m12;
			m3.m20 = thistrans.m20; m3.m21 = thistrans.m21; m3.m22 = thistrans.m22;
			int len = 0;
			Matrix4d inv = new Matrix4d();
			MathUtility.fastInvert(thistrans, inv);
			double x = px * inv.m00 + py * inv.m01 + pz * inv.m02 + inv.m03,
				y = px * inv.m10 + py * inv.m11 + pz * inv.m12 + inv.m13,
				z = px * inv.m20 + py * inv.m21 + pz * inv.m22 + inv.m23;
			
			double[] array = null;
			int edg_a, edg_b;
			double edg_len, edg_a_df, edg_b_df;
			for(int i = 0; i < faces.length; i++){
				if(array == null || array.length != faces[i].length)
					array = new double[faces[i].length * 3];
				
				len = faces[i].length;
				for(int j = 0; j < len; j++){
					edg_a = faces[i][j];
					edg_b = faces[i][(j + 1) % len];
					edg_len = fastPow(vertices[edg_a].x - vertices[edg_b].x, 2)
							+ fastPow(vertices[edg_a].y - vertices[edg_b].y, 2)
							+ fastPow(vertices[edg_a].z - vertices[edg_b].z, 2);
					edg_a_df = fastPow(x - vertices[edg_a].x, 2)
							+ fastPow(y - vertices[edg_a].y, 2)
							+ fastPow(z - vertices[edg_a].z, 2);
					edg_b_df = fastPow(x - vertices[edg_b].x, 2)
							+ fastPow(y - vertices[edg_b].y, 2)
							+ fastPow(z - vertices[edg_b].z, 2);
					if(Math.abs(edg_a_df + edg_b_df + 2.0 * Math.sqrt(edg_a_df * edg_b_df) - edg_len)
							< BoundObject.DIST_EPSILON){
						// on edge...

						if(BugManager.DEBUG && DEBUG)
							printf("Bobj : CnvPly : onFace : is on edge...\n");
						return false;
					}
					
					array[j * 3] = vertices[faces[i][j]].x;
					array[j * 3 + 1] = vertices[faces[i][j]].y;
					array[j * 3 + 2] = vertices[faces[i][j]].z;
				}
					
				if(MathUtility.fastContains(x, y, z, array, normals[i].x, normals[i].y, normals[i].z)){
					output_normal.x = normals[i].x * thistrans.m00 + normals[i].y * thistrans.m01 + normals[i].z * thistrans.m02;
					output_normal.y = normals[i].x * thistrans.m10 + normals[i].y * thistrans.m11 + normals[i].z * thistrans.m12;
					output_normal.z = normals[i].x * thistrans.m20 + normals[i].y * thistrans.m21 + normals[i].z * thistrans.m22;

					if(BugManager.DEBUG && DEBUG)
						printf("Bobj : CnvPly : onFace : result : \n\toutput_normal : %s,\n\toriginal normal : %s\n", output_normal, normals[i]);
					return true;
				}
			}
			return false;
		}// end onFace()
		public boolean onEdge(Matrix4d trans, double px, double py, double pz, Vector3d output_edgdir){
			Matrix4d inv = new Matrix4d();
			MathUtility.fastInvert(trans, inv);
			double x = px * inv.m00 + py * inv.m01 + pz * inv.m02 + inv.m03,
				y = px * inv.m10 + py * inv.m11 + pz * inv.m12 + inv.m13,
				z = px * inv.m20 + py * inv.m21 + pz * inv.m22 + inv.m23;
			
			int a, b;
			double edg_len, edg_a_df, edg_b_df;
			for(int i = 0; i < faces.length; i++){
				for(int j = 0; j < faces[i].length; j++){
					a = faces[i][j];
					b = faces[i][(j + 1) % faces[i].length];
					edg_len = fastPow(vertices[a].x - vertices[b].x, 2)
								+ fastPow(vertices[a].y - vertices[b].y, 2)
								+ fastPow(vertices[a].z - vertices[b].z, 2);
					edg_a_df = fastPow(x - vertices[a].x, 2)
								+ fastPow(y - vertices[a].y, 2)
								+ fastPow(z - vertices[a].z, 2);
					edg_b_df = fastPow(x - vertices[b].x, 2)
								+ fastPow(y - vertices[b].y, 2)
								+ fastPow(z - vertices[b].z, 2);

					if(BugManager.DEBUG && DEBUG)
						printf("BObj : CnvxPlyhdr : onEdge : %d,%d(total %d,%d) : \n" +
							"  edg_len : %g edg_a_df : %g edg_b_df : %g\n" +
							"  check : %g\n",
							i, j, faces.length, faces[i].length, edg_len, edg_a_df, edg_b_df,
							(Math.abs(edg_a_df + edg_b_df + 2.0 * Math.sqrt(edg_a_df * edg_b_df) - edg_len)));
					if(Math.abs(edg_a_df + edg_b_df + 2.0 * Math.sqrt(edg_a_df * edg_b_df) - edg_len)
							< BoundObject.DIST_EPSILON){
						if(output_edgdir != null){
							x = vertices[b].x - vertices[a].x;
							y = vertices[b].y - vertices[a].y;
							z = vertices[b].z - vertices[a].z;

							if(BugManager.DEBUG && DEBUG)
								printf("Bobj : CnvPlyh : onEdge : \n\ttrans : \n%s\tvertices[%d] : %s,\n\tvertices[%d] : %s\n", trans, b, vertices[b], a, vertices[a]);
							output_edgdir.x = x * trans.m00 + y * trans.m01 + z * trans.m02;
							output_edgdir.y = x * trans.m10 + y * trans.m11 + z * trans.m12;
							output_edgdir.z = x * trans.m20 + y * trans.m21 + z * trans.m22;
							output_edgdir.normalize();
						}
						return true;
					}
				}
			}
			return false;
		}// end onEdge
		
		
		public ConvexPolyhedron clone(){
			ConvexPolyhedron cp = new ConvexPolyhedron();
			
			cp.vertices = new Point3d[vertices.length];
			for(int i = 0; i < vertices.length; i++)
				cp.vertices[i] = new Point3d(vertices[i]);
			
			cp.faces = new int[faces.length][];
			for(int i = 0; i < faces.length; i++)
				cp.faces[i] = faces[i].clone();
			
			cp.normals = new Vector3d[normals.length];
			for(int i = 0; i < normals.length; i++)
				cp.normals[i] = new Vector3d(normals[i]);
			
			if(opt_mv != null)
				cp.opt_mv = new Vector3d(opt_mv);

			if(BugManager.DEBUG && DEBUG)
				printf("Bobj : Convex : clone() : this.opt_mv : %s\n", opt_mv);
			return cp;
		}// end clone
	}// end class ConvexPolyhedron
}
