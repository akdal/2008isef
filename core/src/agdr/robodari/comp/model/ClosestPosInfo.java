package agdr.robodari.comp.model;


public class ClosestPosInfo{
	public final static int TYPE_VERTEX = 0;
	public final static int TYPE_EDGE = 1;
	public final static int TYPE_FACE = 2;
	public final static int TYPE_PLANE = 3;
	
	public final static int ITSC_YES = 0;
	public final static int ITSC_NO = 1;
	public final static int ITSC_UNKNOWN = 2;
	
	public double distance;
	public int aType;
	public int bType;
	
	public BoundObject aBoundsObj;
	public BoundObject bBoundsObj;

	public double[] aPoints;
	public double[] bPoints;
	
	public double onFaceCase_normalx;
	public double onFaceCase_normaly;
	public double onFaceCase_normalz;
	
	// intersection을 가질 것인가?
	public int intersects = ITSC_UNKNOWN;
	
	
	
	public void clear(){
		distance = 0;
		aType = bType = 0;
		intersects = ITSC_UNKNOWN;
		aBoundsObj = bBoundsObj = null;
		aPoints = bPoints = null;
		onFaceCase_normalx = onFaceCase_normaly = onFaceCase_normalz = 0;
	}// end clear()
	public void set(ClosestPosInfo cpi){
		distance = cpi.distance;
		aType = cpi.aType;
		bType = cpi.bType;
		
		aPoints = cpi.aPoints;
		bPoints = cpi.bPoints;
		intersects = cpi.intersects;
		
		aBoundsObj = cpi.aBoundsObj;
		bBoundsObj = cpi.bBoundsObj;
		
		onFaceCase_normalx = cpi.onFaceCase_normalx;
		onFaceCase_normaly = cpi.onFaceCase_normaly;
		onFaceCase_normalz = cpi.onFaceCase_normalz;
	}// end set(ClosestPosInfo)
}