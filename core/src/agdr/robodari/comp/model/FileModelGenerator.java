package agdr.robodari.comp.model;

import java.awt.Color;
import java.awt.Font;
import java.io.Reader;
import java.io.StringReader;
import java.io.IOException;

import agdr.robodari.comp.RobotComponent;


public class FileModelGenerator implements ModelGenerator{
	/*private final static SAXParser xmlParser = createXMLParser();
	
	private static final SAXParser createXMLParser(){
		try{
			return SAXParserFactory.newInstance().newSAXParser();
		}catch(Exception expc){
			System.err.println("XML Parser not initialized.");
			System.exit(-1);
		}
		return null;
	}// createXMLParser
	*/
	
	
	public final static String SHORTCUT_RBDR_MODELFOLDER = "%ROBODARI_MODELFOLDER%";
	
	
	private Reader reader;
	private String content;
	
	
	public FileModelGenerator(Reader reader) throws IOException{
		this.reader = reader;
		
		StringBuffer buf = new StringBuffer();
		int len = 0;
		char[] chrs = new char[256];
		while((len = reader.read(chrs)) != -1)
			buf.append(new String(chrs, 0, len));
		
		content = buf.toString();
	}// end Constructor(String)
	
	
	
	public RobotCompModel generate(RobotComponent robotcmp) throws Exception{
		RobotCompModel str = new RobotCompModel();
		str.load(new StringReader(content));
		return str;
		/*ModelParser generator = new ModelParser();
		
		xmlParser.parse(inputStream, generator);
		while(generator.isGenerating());
		
		return generator.getGeneratedModel();*/
	}// end generateModel(Object[][])
	
	
/*
	private static class ModelParser extends DefaultHandler{
		private final static int ACTION_NOTHING = -1;
		private final static int ACTION_COMP = 1;
		private final static int ACTION_TEXT = 2;
		
		private BranchGroup rootGroup;
		private TransformGroup currentGroup;
		
		private ArrayList<String> grammarErrors;
		private ArrayList<String> contentErrors;
		private int action;
		private boolean isGenerating = false;
		
		
		
		public ModelParser(){
			grammarErrors = new ArrayList<String>();
			contentErrors = new ArrayList<String>();
		}// end Constructor()
		
		
		public ArrayList<String> getGrammarErrors()
		{ return grammarErrors; }
		public ArrayList<String> getContentErrors()
		{ return contentErrors; }
		public BranchGroup getGeneratedModel(){
			if(grammarErrors.size() != 0 || contentErrors.size() != 0)
				return null;
			return rootGroup;
		}// end getGeneratedModel()
		public boolean isGenerating()
		{ return isGenerating; }
		
		
		
		//*
		 *  ̾Ʒʹ org.xml.sax.DefaultHandler overriding.
		// */
		/*
		public void startDocument(){
			rootGroup = new BranchGroup();
			
			isGenerating = true;
			grammarErrors.clear();
			contentErrors.clear();
			action = ACTION_NOTHING;
			currentGroup = null;
		}// end startDocument()
		public void endDocument()
		{ isGenerating = false; }
		
		
		
		public void startElement(String uri, String localname, String qname, Attributes attrs){
			/* FIXME localname qname dȮ 𸣰ڵ -_-; **/
		/*	localname = localname.toLowerCase();
			
			if(localname.equals("object"))
				// root
				;
			else if(localname.equals("comp")){
				action = ACTION_COMP;
				currentGroup = new TransformGroup();
				
				// type == 0 ϶ : geom, type == 1 ϶ : text
				int type = 0;
				String name = null;
				Vector3d positionvec = new Vector3d(0, 0, 0);
				Vector3d transfvec = new Vector3d(1.0, 1.0, 1.0);
				String objectfile = "";
				
				String textcnt = "";
				String fontFamily = "";
				Color textForeground = null;
				int textsize = 0;
				
				
				for(int i = 0; i < attrs.getLength(); i++){
					String attrName = attrs.getLocalName(i).toLowerCase();
					
					if(attrName.equals("type")){
						if(attrs.getValue(i).toLowerCase().equals("geom"))
							type = 1;
					}else if(attrName.equals("name")){
						name = attrs.getValue(i);
					}else if(attrName.equals("x") || attrName.equals("y")
							|| attrName.equals("z") || attrName.equals("transx") ||
							attrName.equals("transy") || attrName.equals("transz")){
						double val = 0.0;
						try{
							val = Double.parseDouble(attrs.getValue(i));
						}catch(Exception excp){}
						
						if(attrName.equals("x"))
							positionvec.setX(val);
						else if(attrName.equals("y"))
							positionvec.setY(val);
						else if(attrName.equals("z"))
							positionvec.setZ(val);
						else if(attrName.equals("transx"))
							transfvec.setX(val);
						else if(attrName.equals("transy"))
							transfvec.setY(val);
						else if(attrName.equals("transz"))
							transfvec.setZ(val);
					}else if(attrName.equals("objectfile")){
						objectfile = attrs.getValue(i);
					}else if(attrName.equals("textfont")){
						fontFamily = attrs.getValue(i);
					}else if(attrName.equals("text")){
						textcnt = attrs.getValue(i);
					}else if(attrName.equals("textcolor")){
						String val = attrs.getValue(i).toLowerCase();
						if(val.startsWith("#")){
							val = val.substring(1, val.length());
							if(val.length() < 6) return;
							
							try{
								String r = val.substring(0, 2);
								String g = val.substring(2, 4);
								String b = val.substring(4, 6);
									textForeground = new Color(Integer.parseInt(r, 16),
											Integer.parseInt(g, 16), Integer.parseInt(b, 16));
							}catch(Exception excp){
							}
						}else if(val.equals("blue")){
							textForeground = Color.blue;
						}else if(val.equals("red")){
							textForeground = Color.red;
						}else if(val.equals("green")){
							textForeground = Color.green;
						}else if(val.equals("cyan")){
							textForeground = Color.cyan;
						}else if(val.equals("yellow")){
							textForeground = Color.yellow;
						}else if(val.equals("white")){
							textForeground = Color.white;
						}else if(val.equals("black")){
							textForeground = Color.black;
						}else if(val.equals("orange")){
							textForeground = Color.orange;
						}else if(val.equals("pink")){
							textForeground = Color.pink;
						}else if(val.equals("magenta")){
							textForeground = Color.magenta;
						}else if(val.equals("gray")){
							textForeground = Color.gray;
						}else if(val.equals("lightgray")){
							textForeground = Color.lightGray;
						}else{
							if(val.length() < 6) return;
							
							try{
								String r = val.substring(0, 2);
								String g = val.substring(2, 4);
								String b = val.substring(4, 6);
								textForeground = new Color(Integer.parseInt(r, 16),
										Integer.parseInt(g, 16), Integer.parseInt(b, 16));
							}catch(Exception excp){
							}
						}
						
					}else if(attrName.equals("textsize")){
						try{
							textsize = Integer.parseInt(attrs.getValue(i));
						}catch(Exception excp){}
					}
				}
				
				
				if(name != null){
					currentGroup.setName(name);
				}

				Transform3D transf = new Transform3D();
				transf.setTranslation(positionvec);
				transf.setScale(transfvec);
				currentGroup.setTransform(transf);
				if(type == 0){
					// geom.
					ObjectFile objfile = new ObjectFile(ObjectFile.TRIANGULATE);
					try{
						BranchGroup bgroup = objfile.load(objectfile).getSceneGroup();
						currentGroup.addChild(bgroup);
					}catch(Exception excp){
						contentErrors.add("x ʴ obj Դϴ : " + objectfile);
					}
				}else{
					action = ACTION_TEXT;
					Text3D textobject = new Text3D(new Font3D(new Font(fontFamily, textsize, 0),
							new FontExtrusion()), textcnt);
					SkObject appr = new SkObject();
					appr.setColoringAttributes(new ColoringAttributes(new Color3f(textForeground),
							ColoringAttributes.SHADE_GOURAUD));
					Shape3D shape = new Shape3D(textobject, appr);
					currentGroup.addChild(shape);
				}
				rootGroup.addChild(currentGroup);
				currentGroup = null;
			}else if(localname.equals("referto")){
				currentGroup = new TransformGroup();
				
				String cls = null;
				Transform3D transf = new Transform3D();
				Vector3d transvec = new Vector3d(1.0, 1.0, 1.0);
				Vector3d scalevec = new Vector3d(1.0, 1.0, 1.0);
				
				transf.setTranslation(transvec);
				transf.setScale(scalevec);
				
				for(int i = 0; i < attrs.getLength(); i++){
					String attrName = attrs.getLocalName(i).toLowerCase();
					
					if(attrName.equals("class")){
						cls = attrs.getValue(i);
					}else{
						double val = 0.0;
						try{
							val = Double.parseDouble(attrs.getValue(i));
						}catch(Exception excp){
							continue;
						}
						
						if(attrName.equals("x")){
							transvec.setX(val);
						}else if(attrName.equals("y")){
							transvec.setY(val);
						}else if(attrName.equals("z")){
							transvec.setZ(val);
						}else if(attrName.equals("rotxangle")){
							transf.rotX(val);
						}else if(attrName.equals("rotyangle")){
							transf.rotY(val);
						}else if(attrName.equals("rotzangle")){
							transf.rotZ(val);
						}else if(attrName.equals("xtransf")){
							scalevec.setX(val);
						}else if(attrName.equals("ytransf")){
							scalevec.setY(val);
						}else if(attrName.equals("ztransf")){
							scalevec.setZ(val);
						}
					}
				}
				currentGroup.setTransform(transf);
				
				Class referingClass = null;
				try{
					referingClass = Class.forName(cls);
				}catch(Exception excp){
					contentErrors.add("x ʴ classԴϴ : " + cls);
					return;
				}
				
				RobotModel bg = RobotModelStore.getModel(referingClass);
				if(bg == null)
					return;
				currentGroup.addChild(bg);

				rootGroup.addChild(currentGroup);
				currentGroup = null;
			}
		}// end startElement(String, STring, STring, Attributes)
		public void endElement(String uri, String localname, String qname){
			action = ACTION_NOTHING;
		}// end endElement(String, String, String)
		
		

		public void error(SAXParseException excp)
		{ grammarErrors.add(excp.getMessage()); }
		public void fatalError(SAXParseException excp)
		{ grammarErrors.add(excp.getMessage()); }
	}// end class ModelParser */
}
