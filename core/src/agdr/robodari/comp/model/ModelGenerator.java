package agdr.robodari.comp.model;

import agdr.robodari.comp.RobotComponent;

public interface ModelGenerator<T extends RobotComponent> {
	public RobotCompModel generate(T comp) throws Exception;
}
