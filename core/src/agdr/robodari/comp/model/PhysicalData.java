package agdr.robodari.comp.model;

import javax.vecmath.*;


public class PhysicalData {
	public float mass;
	public float volume;
	public Matrix3f inertia;
	public Point3f centerMassPoint;
	public BoundObject bounds;
	
	
	public void clear(){
		mass = volume = 0;
		if(inertia != null){
			inertia.setColumn(0, 0, 0, 0);
			inertia.setColumn(1, 0, 0, 0);
			inertia.setColumn(2, 0, 0, 0);
		}
		if(centerMassPoint != null)
			centerMassPoint.set(0, 0, 0);
	}// end clear()_
	
	public PhysicalData clone(){
		PhysicalData pd = new PhysicalData();
		pd.mass = mass;
		pd.volume = volume;
		pd.centerMassPoint = new Point3f(this.centerMassPoint);
		pd.inertia = new Matrix3f(this.inertia);
		pd.bounds = this.bounds.clone();
		return pd;
	}// end clone()
}
