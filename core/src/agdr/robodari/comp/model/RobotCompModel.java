package agdr.robodari.comp.model;

import java.io.*;
import java.util.*;
import javax.vecmath.*;

import agdr.library.math.*;
import agdr.robodari.appl.*;


public class RobotCompModel/* implements Serializable : MUST NOT BE STORED!!!!*/{
	private transient final static Point3f[] NULLPARRAY = new Point3f[]{};
	//private transient final static int[][] NULLIARRAY = new int[][]{{}};
	//private transient final static Structure[] NULLSARRAY = new Structure[]{};
	
	private ArrayList<Point3f> points = new ArrayList<Point3f>();
	private ArrayList<int[]> faces = new ArrayList<int[]>();
	private PhysicalData phyData = new PhysicalData();
	private AppearanceData apprData = new AppearanceData();

	private PhysicalData groupPhyData = null;
	private ArrayList<Pair> subModels = new ArrayList<Pair>();

	private boolean isGroup = false;
	
	
	public RobotCompModel() {}
	
	public Point3f getPoint(int idx){
		if(!isGroup)
			return points.get(idx);
		return null;
	}
	public Point3f[] getPoints(){
		if(!isGroup)
			return points.toArray(NULLPARRAY); 
		return null;
	}
	public int getPointCount(){
		if(isGroup)
			return 0;
		return points.size();
	}// end getPC
	
	public int[] getFace(int idx){
		if(isGroup)
			return null;
		return faces.get(idx);
	}
	public int[][] getFaces(){
		if(isGroup)
			return null;
		int[][] newarray = new int[faces.size()][];
		
		for(int i = 0; i < newarray.length; i++){
			newarray[i] = new int[faces.get(i).length];
			System.arraycopy(faces.get(i), 0, newarray[i], 0, newarray[i].length);
		}
		return newarray;
	}
	public int getLineCount(){
		if(isGroup)
			return 0;
		return faces.size();
	}// end getLineC
	
	
	
	public PhysicalData getPhysicalData(){
		if(!isGroup)
			return phyData;
		else
			return null;
	}// end getPhysicalData
	public void setPhysicalData(PhysicalData pd)
	{ this.phyData = pd; }
	
	public AppearanceData getApprData()
	{ return apprData; }
	public void setApprData(AppearanceData ad)
	{ this.apprData = ad; }
	
	
	
	public boolean isGroup()
	{ return isGroup; }
	public void setGroup(boolean b)
	{ this.isGroup = b; }
	
	
	
	
	

	private Point3f[] _getGroupPoints(RobotCompModel str, Matrix4f trans){
		if(str == null) return new Point3f[]{};
		
		ArrayList<Point3f> points = new ArrayList<Point3f>();
		if(!str.isGroup()){
			Point3f[] ps = str.getPoints();
			if(trans == null)
				Collections.addAll(points, ps);
			else{
				for(int i = 0; i < ps.length; i++){
					Point3f p = new Point3f(ps[i]);
					trans.transform(p);
					points.add(p);
				}
			}
		}
		
		int cnt = str.getSubModelCount();
		for(int i = 0; i < cnt; i++){
			RobotCompModel ss = str.getSubModel(i);
			
			Matrix4f strtrans = null;
			if(trans == null)
				strtrans = new Matrix4f(str.getTransform(ss));
			else
				strtrans = MathUtility.fastMul(trans, str.getTransform(ss), null);
			
			Collections.addAll(points, _getGroupPoints(ss, strtrans));
		}
		return points.toArray(new Point3f[]{});
	}// end _getGroupPoints(Structure)
	public Point3f[] getGroupPoints(){
		if(isGroup)
			return _getGroupPoints(this, null);
		return null;
	}// end getAllPoints()
	
	
	public RobotCompModel[] getSubModels(){
		RobotCompModel[] strs = new RobotCompModel[subModels.size()];
		for(int i = 0; i < strs.length; i++)
			strs[i] = subModels.get(i).str;
		return strs;
	}// end getSubModels()
	public Matrix4f[] getTransforms(){
		Matrix4f[] transs = new Matrix4f[subModels.size()];
		for(int i = 0; i < transs.length; i++)
			transs[i] = subModels.get(i).transform;
		return transs;
	}// end getTransforms()
	
	public int getSubModelCount(){ 
		if(!isGroup())
			return 0;
		return subModels.size();
	}// end
	public void addSubModel(RobotCompModel str, Matrix4f trans){
		subModels.add(new Pair(str, trans));
	}// end addSubModel(Structure, Matrix4f)
	public void removeSubModel(RobotCompModel str){ 
		subModels.remove(str);
	}// end removeSubModel(Structure)
	public boolean containsSubModel(RobotCompModel str)
	{ return subModels.contains(str); }
	public RobotCompModel getSubModel(int idx){
		if(!isGroup())
			return null;
		return subModels.get(idx).str;
	}// end getSubModel
	
	/**
	 * 좌표계는 무게중심 좌표계가 아닌 ModelGenerator에서 정의한 좌표계입니다.
	 */
	public Matrix4f getTransform(RobotCompModel str){
		if(!isGroup())
			return null;
		for(int i = 0; i < subModels.size(); i++)
			if(subModels.get(i).str == str)
				return subModels.get(i).transform;
		return null;
	}// end getTransform(Structure)
	
	public PhysicalData getGroupPhysData(){
		if(isGroup)
			return this.groupPhyData;
		return null;
	}// end getGroupPhysData
	public void setGroupPhysData(PhysicalData pd)
	{ this.groupPhyData = pd; }
	// 이 메소드는 속도는 포기함 -_-
	public PhysicalData calcGroupPhysData(){
		if(!isGroup)
			return null;
		
		// intergrated physical data를 구한다.
		// 좌표계 : 절대좌표계
		
		
		PhysicalData gpd = new PhysicalData();
		gpd.centerMassPoint = new Point3f();
		BoundObject.CompoundBounds gbound = new BoundObject.CompoundBounds();
		gpd.bounds = gbound;
		gpd.inertia = new Matrix3f();
		
		PhysicalData[] subpdata = new PhysicalData[subModels.size()];
		Matrix4f[] subtransfs = new Matrix4f[subpdata.length];
		
		// mass는 값이 있는데 cmp가 없는 경우가 있다
		// 이 때는 cmp를 구할 때 잘못 구해지므로 아래와 같은 항을 따로 만들어야함 
		float massForCMP = 0;
		boolean hasInfMass = false;
		int infMassCnt = 0;
		
		
		for(int i = 0; i < subModels.size(); i++){
			RobotCompModel target = subModels.get(i).str;
			
			if(target.isGroup()){
				subpdata[i] = target.getGroupPhysData();
				if(subpdata[i] == null)
					subpdata[i] = target.calcGroupPhysData();
			}else{
				subpdata[i] = target.getPhysicalData();
			}

			subtransfs[i] = new Matrix4f(this.getTransform(target));
			// mass가 infinite일 경우 처리가 필요함.
			if(subpdata[i] == null)
				continue;
			
			gbound.add(subpdata[i].bounds, new Matrix4d(subtransfs[i]));
			gpd.mass += subpdata[i].mass;
			gpd.volume += subpdata[i].volume;
			if(subpdata[i].centerMassPoint != null)
				massForCMP += subpdata[i].mass;
			
			if(Float.isInfinite(subpdata[i].mass)){
				hasInfMass = true;
				if(subpdata[i].centerMassPoint != null)
					infMassCnt++;
			}
		}
		
		
		Matrix3f tmpinrt = new Matrix3f();
		// cmp, inrt 계산
		if(hasInfMass){
			// inrt는 무조건 무한
			// cmp는 질량이 inf인것만 찾아서 더함.
			if(gpd.inertia == null)
				gpd.inertia = new Matrix3f();
			gpd.inertia.m00 = Float.POSITIVE_INFINITY; gpd.inertia.m01 = Float.NEGATIVE_INFINITY; gpd.inertia.m02 = Float.NEGATIVE_INFINITY;
			gpd.inertia.m10 = Float.NEGATIVE_INFINITY; gpd.inertia.m11 = Float.POSITIVE_INFINITY; gpd.inertia.m12 = Float.NEGATIVE_INFINITY;
			gpd.inertia.m20 = Float.NEGATIVE_INFINITY; gpd.inertia.m21 = Float.NEGATIVE_INFINITY; gpd.inertia.m22 = Float.POSITIVE_INFINITY;
			
			BugManager.printf("RCompModel : calcGroupPhysData : \n");
			
			for(int i = 0; i < subModels.size(); i++){
				if(subpdata[i] == null)
					continue;
				else if(subpdata[i].centerMassPoint == null)
					continue;
				else if(!Float.isInfinite(subpdata[i].mass))
					continue;
				BugManager.printf("%d : subtransfs : \n%s, cm : %s\n", i, subtransfs[i], subpdata[i].centerMassPoint);
				
				gpd.centerMassPoint.x += subtransfs[i].m00 * subpdata[i].centerMassPoint.x
								+ subtransfs[i].m01 * subpdata[i].centerMassPoint.y 
								+ subtransfs[i].m02 * subpdata[i].centerMassPoint.z
								+ subtransfs[i].m03;
				gpd.centerMassPoint.y += subtransfs[i].m10 * subpdata[i].centerMassPoint.x
								+ subtransfs[i].m11 * subpdata[i].centerMassPoint.y 
								+ subtransfs[i].m12 * subpdata[i].centerMassPoint.z
								+ subtransfs[i].m13;
				gpd.centerMassPoint.z += subtransfs[i].m20 * subpdata[i].centerMassPoint.x
								+ subtransfs[i].m21 * subpdata[i].centerMassPoint.y 
								+ subtransfs[i].m22 * subpdata[i].centerMassPoint.z
								+ subtransfs[i].m23;
			}
			gpd.centerMassPoint.x /= (float)infMassCnt;
			gpd.centerMassPoint.y /= (float)infMassCnt;
			gpd.centerMassPoint.z /= (float)infMassCnt;
		}else{
			for(int i = 0; i < subModels.size(); i++){
				if(subpdata[i].centerMassPoint != null){
					gpd.centerMassPoint.x += subpdata[i].mass *
							(subpdata[i].centerMassPoint.x * subtransfs[i].m00 + 
									subpdata[i].centerMassPoint.y * subtransfs[i].m01 +
									subpdata[i].centerMassPoint.z * subtransfs[i].m02 + 
									subtransfs[i].m03);
					gpd.centerMassPoint.y += subpdata[i].mass *
							(subpdata[i].centerMassPoint.x * subtransfs[i].m10 + 
									subpdata[i].centerMassPoint.y * subtransfs[i].m11 + 
									subpdata[i].centerMassPoint.z * subtransfs[i].m12 + 
									subtransfs[i].m13);
					gpd.centerMassPoint.z += subpdata[i].mass *
							(subpdata[i].centerMassPoint.x * subtransfs[i].m20 + 
									subpdata[i].centerMassPoint.y * subtransfs[i].m21 + 
									subpdata[i].centerMassPoint.z * subtransfs[i].m22 + 
									subtransfs[i].m23);
				}
				
				if(subpdata[i].inertia != null){
					MathUtility.addTranslatedInertiaTensor(
							MathUtility.getRotatedInertiaTensor(subpdata[i].inertia,
									subtransfs[i].m00, subtransfs[i].m01, subtransfs[i].m02,
									subtransfs[i].m10, subtransfs[i].m11, subtransfs[i].m12,
									subtransfs[i].m20, subtransfs[i].m21, subtransfs[i].m22,
								tmpinrt),
							subtransfs[i].m03, subtransfs[i].m13, subtransfs[i].m23, subpdata[i].mass, gpd.inertia);
				}
			}
			gpd.centerMassPoint.x /= massForCMP;
			gpd.centerMassPoint.y /= massForCMP;
			gpd.centerMassPoint.z /= massForCMP;
		}
		return gpd;
	}// end calcGPD
	
	
	
	
	
	
	
	public void clear(){
		points.clear();
		faces.clear();
		subModels.clear();
		isGroup = false;
		if(phyData != null)
			phyData.clear();
		if(apprData != null)
			apprData.clear();
		if(groupPhyData != null)
			groupPhyData.clear();
	}// end clear()
	
	
	
	public Object clone(){
		RobotCompModel st = new RobotCompModel();
		
		st.faces = new ArrayList<int[]>();
		for(int i = 0; i < faces.size(); i++){
			int[] array = faces.get(i);
			int[] na = new int[array.length];
			System.arraycopy(array, 0, na, 0, array.length);
			st.faces.add(na);
		}
		st.points = new ArrayList<Point3f>();
		for(int i = 0; i < points.size(); i++)
			st.points.add(new Point3f(points.get(i)));
		
		if(this.phyData != null)
			st.phyData = this.phyData.clone();
		else
			st.phyData = null;
		if(this.apprData != null)
			st.apprData = this.apprData.clone();
		else
			st.apprData = null;
		
		st.isGroup = this.isGroup;
		if(this.groupPhyData != null)
			st.groupPhyData = this.groupPhyData.clone();
		else
			st.groupPhyData = null;
		
		return st;
	}// end clone()
	
	
	
	
	
	public void load(Reader reader) throws IOException,
			InputMismatchException, Exception{
		ArrayList<Point3f> lclpoints = new ArrayList<Point3f>();
		ArrayList<int[]> lclfaces = new ArrayList<int[]>();
		
		StringBuffer buf = new StringBuffer();
		BufferedReader bufreader = new BufferedReader(reader);
		
		String readLine = null;
		int[] face = new int[256];
		int facelen = 0;
		int linecnt = 0;
		
		while((readLine = bufreader.readLine()) != null){
			buf.append(readLine);
			linecnt++;
			
			if(readLine.startsWith("#"))
				continue;
			else if(readLine.toLowerCase().startsWith("v")){
				StringTokenizer st = new StringTokenizer(readLine);
				st.nextToken();// throw away v
				Point3f point = new Point3f(0, 0, 0);
				point.x = Float.parseFloat(st.nextToken());
				point.y = Float.parseFloat(st.nextToken());
				point.z = Float.parseFloat(st.nextToken());
				
				if(st.hasMoreTokens())
					throw new InputMismatchException("No more values allowed : in source " + linecnt);
				lclpoints.add(point);
			}else if(readLine.toLowerCase().startsWith("f")){
				StringTokenizer st = new StringTokenizer(readLine);
				st.nextElement();
				
				while(st.hasMoreTokens()){
					if(facelen == 256)
						throw new InputMismatchException("the number of vertices" +
								" organizing one face cannot be over 256.");
					face[facelen++] = Integer.parseInt(st.nextToken()) - 1;
				}
				int[] trimmedFace = new int[facelen];
				System.arraycopy(face, 0, trimmedFace, 0, facelen);
				lclfaces.add(trimmedFace);
				facelen = 0;
			}
		}
		this.points = lclpoints;
		this.faces = lclfaces;
	}// end load(Reader)
	
	public boolean equals(Object obj){
		if(obj == null) return false;
		else if(!(obj instanceof RobotCompModel))
			return false;
		RobotCompModel st = (RobotCompModel)obj;
		return st.faces.equals(this.faces) && st.points.equals(this.points);
	}// end equals(Object)
	
	
	
	
	
	
	static class Pair implements java.io.Serializable{
		RobotCompModel str;
		Matrix4f transform;
		
		public Pair(RobotCompModel str, Matrix4f trans)
		{ this.str = str; this.transform = trans; }
	}// end class Pair
}
