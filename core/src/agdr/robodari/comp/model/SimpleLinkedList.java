package agdr.robodari.comp.model;

public abstract class SimpleLinkedList<T> implements java.util.Enumeration<T>{
	private T head = null;
	private T tail = null;
	private T focus;
	private int size;
	
	public boolean hasMoreElements(){
		if(focus == null)
			// head != null이면 시작을 아예 시작을 안한거
			// head == null이면 갈 곳 없음
			return head != null;
		return getNext(focus) != null;
	}// end hasMoreEleemnts
	public T nextElement(){
		if(focus == null)
			return focus = head;
		T next = getNext(focus);
		return (next == null) ? null : (focus = next);
	}// end nextElem
	public void resetPos()
	{ focus = null; }
	
	
	public T append(){
		if(head == null){
			head = tail = newItem();
		}else{
			T newitm = newItem();
			setNext(tail, newitm);
			tail = newitm;
			//tail.id = size;
		}
		init(tail);
		size++;
		return tail;
	}// end append
	public T getTail()
	{ return tail; }
	public T getFocus()
	{ return focus; }
	public int size()
	{ return size; }
	public void clear(){
		size = 0;
		head = null;
		focus = null;
		tail = null;
	}// end clear()
	
	public abstract T getNext(T t);
	protected abstract T newItem();
	public abstract void init(T t);
	public abstract void setNext(T t, T itm);
}
