package agdr.robodari.edit;

import javax.swing.*;

import agdr.robodari.gui.*;
import agdr.robodari.gui.dialog.*;
import agdr.robodari.project.*;

/**
 * EditorManager is a class which can edit appropriate files(declared in isEditable(file : String).)<br>
 * Constructors of the classes implementing this interface must have two arguments : <br>
 * public &lt;Constructor&gt;(RobodariInterface, JFrame)
 * @author root
 *
 */
public interface EditorManager {
	public void newFile(ProjectFile file);
	public boolean isCached(ProjectFile file);
	/**
	 * If given file is not opened, read the content from file and cache.
	 * If given file is opened, cache edited model.
	 * @param file
	 */
	public void cacheFile(ProjectFile file) throws java.io.IOException;
	public ProjectFile[] getCachedFiles();
	public void openFile(ProjectFile file, boolean reload);
	public void saveFile(ProjectFile file, java.io.File where);
	public void closeFile(ProjectFile file);
	
	public RobodariInterface getInterface();
	
	public int getRightComponentCount();
	public JComponent getRightComponent(int idx);
	public String getRightCompTitle(int idx);
	public int getLeftComponentCount();
	public JComponent getLeftComponent(int idx);
	public String getLeftCompTitle(int idx);
	public JComponent getEditorComp();
	
	public javax.swing.Icon getFileIcon();
	public void customizeMenuBar(JMenuBar menuBar);
	public void undoMenuBar(JMenuBar mb);
	public boolean isEditable(String filename);
	public OptionDialog getFileOptionDialog();
	public String getFileDescription();
	public String getFileExtension();
	
	/* :P */
	public void repaint();
}
