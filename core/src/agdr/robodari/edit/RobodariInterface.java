package agdr.robodari.edit;

import javax.swing.*;

import agdr.robodari.gui.*;
import agdr.robodari.gui.dialog.*;
import agdr.robodari.project.*;

public interface RobodariInterface {
	public ProjectTree getProjectTree();
	
	public Project getProject();
	public void openProject(Project proj);
	//public void cacheFile(ProjectFile pfile);
	//public boolean isCached(ProjectFile pfile);
	public void openFile(ProjectFile pfile, boolean reload, boolean show);
	public void saveFile(ProjectFile pfile, java.io.File file);
	public void closeAll();
	
	public void setStatusBarText(String text);
	
	public ProjectFile[] getOpenedFiles();
	public ProjectFile getEditingFile();
	
	
	public EditorManager[] getEditorManagers();
	public EditorManager getOpenedEditorManager();
	public EditorManager[] getEditorManagers(String file);
	public EditorManager getCurrentEditor(ProjectFile file);
	
	public NewDialog getNewDialog();
	public JFileChooser getOpenDialog();
	public JFileChooser getImportDialog();
	public NoteEditorDialog getNoteEditorDialog();
}
