package agdr.robodari.gui;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.border.*;
import org.xml.sax.*;
//import org.agdr.robodari.store.*;

import agdr.library.lang.*;
import agdr.robodari.appl.*;
import agdr.robodari.comp.*;
import agdr.robodari.edit.*;
import agdr.robodari.project.*;
import agdr.robodari.gui.dialog.*;


public class MainFrame extends JFrame implements RobodariInterface, MouseListener, WindowListener{
	private final static EditorManager[] NULLEARRAY = new EditorManager[]{};
	
	// left
	private ProjectTree projectTree;
	
	private CardLayout centerCardLayout;
	private JPanel centerPanel;
	private JTabbedPane leftPane;
	private JTabbedPane rightPane;
	
	private JSplitPane centerSplitPane;
	private JSplitPane leftSplitPane;
	
	private JPanel openedFilePanel;
	private JLabel openedFilePathLabel;
	
	private JMenuBar menuBar;
		private JMenu fileMenu;
			private JMenu newMenu;
				private JMenuItem newProjectMenu;
				private JMenuItem newItemMenu;
			private JMenuItem openMenu;
			private JMenuItem importMenu;
			private JMenuItem saveMenu;
			private JMenuItem saveAsMenu;
			private JMenuItem saveAllMenu;
			private JMenuItem closeAllMenu;
		//private JMenu editMenu;
		private JMenu projectMenu;
			private JMenuItem editProjectNoteMenu;
	
	private JLabel statusBar;
	
	private NewDialog newDialog;
	private JFileChooser openDialog;
	private JFileChooser importDialog;
	private NoteEditorDialog noteEditorDialog;
	
	
	private ProjectFile editingFile;
	private Project openedProject;
	
	//private RealizationViewer realizationViewer;
	
	private Hashtable<ProjectFile, EditorManager> currentEditors = new Hashtable<ProjectFile, EditorManager>();
	private EditorManager[] editorManagers;
	private EditorManager openedEditorManager;
	
	
	
	
	public MainFrame(Class[] managerClasses) throws Exception{
		Class[] emargcls = new Class[]{
				RobodariInterface.class, Frame.class
		};
		BugManager.println(this);
		this.editorManagers = new EditorManager[managerClasses.length];
		for(int i = 0; i < managerClasses.length; i++){
			BugManager.println("MainFrame : cons : editorMAnagers[i] : " + managerClasses[i].toString());
			Constructor cons = managerClasses[i].getConstructor(emargcls);
			this.editorManagers[i] = (EditorManager)cons.newInstance((RobodariInterface)this, (Frame)this);
		}
		
		
		initComponents();
		initMenuBar();
		initDialogs();
		initStatusBar();
		
		//realizationViewer = new RealizationViewer(this, this);
		
		/* Dot-eum che */
		super.setFont(new Font("\ub3cb\uc6c0\uccb4", 0, 12));
		super.setJMenuBar(menuBar);
		super.getContentPane().setLayout(new BorderLayout());
		super.getContentPane().add(centerSplitPane, BorderLayout.CENTER); 
		super.getContentPane().add(statusBar, BorderLayout.SOUTH);
		//super.getContentPane().add(toolBar, BorderLayout.NORTH);
		
		super.setTitle(ApplicationInfo.getFullName());
		Dimension ssize = Toolkit.getDefaultToolkit().getScreenSize();
		Insets is = Toolkit.getDefaultToolkit().
				getScreenInsets(this.getGraphicsConfiguration());
		
		super.setSize((int)((float)ssize.width * 0.9f) - is.left - is.right,
				(int)((float)ssize.height * 0.9f) - is.top - is.bottom);
		super.setLocation((int)((float)ssize.width * 0.05f) + is.left,
				(int)((float)ssize.height * 0.05f) + is.top);
		super.addWindowListener(this);
		super.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	}// end Constructor()
	
	
	private void initComponents(){
		projectTree = new ProjectTree(this, null, true);
		
		openedFilePanel = new JPanel();
		openedFilePanel.setBackground(Color .white);
		openedFilePanel.setLayout(new BorderLayout());
		openedFilePathLabel = new JLabel();
		openedFilePathLabel.setBorder(new MatteBorder(0, 3, 0, 0, Color.gray));
		openedFilePanel.add(openedFilePathLabel, BorderLayout.CENTER);
		

		centerPanel = new JPanel();
		centerCardLayout = new CardLayout();
		centerPanel.setLayout(centerCardLayout);
		
		for(int i = 0; i < editorManagers.length; i++)
			centerPanel.add(editorManagers[i].getEditorComp(), editorManagers[i].getClass().toString());
		
		leftPane = new JTabbedPane();
		leftPane.addTab("Prj", projectTree);
		rightPane = new JTabbedPane();
		
		JPanel jp = new JPanel();
		jp.setLayout(new BorderLayout());
		jp.add(centerPanel, BorderLayout.CENTER);
		jp.add(openedFilePanel, BorderLayout.NORTH);
		leftSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
				true, leftPane, jp);
		centerSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
				true, leftSplitPane, rightPane);
		centerSplitPane.setDividerLocation(800);
		leftSplitPane.setDividerLocation(200);
		leftSplitPane.setOneTouchExpandable(true);
		centerSplitPane.setOneTouchExpandable(true);
		
		centerSplitPane.setContinuousLayout(false);
		leftSplitPane.setContinuousLayout(false);
	}// end initComponents()
	private void initMenuBar(){
		menuBar = new JMenuBar();
		
		fileMenu = new JMenu("File");
		{
			newMenu = new JMenu("New");
			{
				newProjectMenu = new JMenuItem(new NewAction(this, true, false, -1,
						"New Project", null));
				newItemMenu = new JMenuItem(new NewAction(this, false, true, -1, "New Item..", null));
				
				newMenu.add(newProjectMenu);
				newMenu.add(newItemMenu);
			}
			openMenu = new JMenuItem(new OpenAction(this, "open..", null));
			importMenu = new JMenuItem(new ImportAction(this, "import..", null));
			saveMenu = new JMenuItem(new SaveAction(this, "save..", null));
			saveAsMenu = new JMenuItem(new SaveAsAction(this, "save as..", null));
			saveAllMenu = new JMenuItem(new SaveAllAction(this, "save all", null));
			closeAllMenu = new JMenuItem(new CloseAllAction(this, "close all", null));
			
			importMenu.setEnabled(false);
			saveMenu.setEnabled(false);
			saveAsMenu.setEnabled(false);
			saveAllMenu.setEnabled(false);
			closeAllMenu.setEnabled(false);
			
			fileMenu.add(newMenu);
			fileMenu.add(openMenu);
			fileMenu.addSeparator();
			fileMenu.add(importMenu);
			fileMenu.add(saveMenu);
			fileMenu.add(saveAsMenu);
			fileMenu.add(saveAllMenu);
			fileMenu.add(closeAllMenu);
		}
		projectMenu = new JMenu("Project");
		{
			editProjectNoteMenu = new JMenuItem(new EditProjectNoteAction(this, "Edit project note..", null));
			
			projectMenu.add(editProjectNoteMenu);
			
			editProjectNoteMenu.setEnabled(false);
		}
		
		menuBar.add(fileMenu);
		//menuBar.add(editMenu);
		menuBar.add(projectMenu);
	}// end initMenuBar()
	private void initStatusBar(){
		statusBar = new JLabel();
		statusBar.setPreferredSize(new Dimension(400, 20));
		statusBar.setBorder(new BevelBorder(BevelBorder.LOWERED));
		statusBar.setOpaque(true);
	}// end initStatusBar()
	
	private void initDialogs(){
		newDialog = new NewDialog(this, this){
			public void finish(){
				//System.out.println("in MainFrame : Newdialog.finish!");
				beforeButton.setEnabled(false);
				if(newDialog.editsProject()){
					String prjname = newDialog.getProjectName();
					String prjpath = newDialog.getProjectPath();
					
					if(prjname.trim().length() == 0){
						ExceptionHandler.wrongProjectName(prjname);
						return;
					}
					for(int i = 0; i < prjname.length(); i++){
						char c = prjname.charAt(i);
						if(c == '/' || c == '\\' || c == '|' || c == '"'
								|| c == ':' || c == '*' || c == '?' || c == '<'
								|| c == '>' || !Character.isDefined(c)){
							ExceptionHandler.wrongProjectName(prjname);
							return;
						}
					}
					if(prjpath.trim().length() == 0){
						ExceptionHandler.wrongProjectPath(prjpath);
						return;
					}
					createNewProject(prjname, prjpath);
				}else{
					if(newDialog.getSelectedIndex() < 0 && !newDialog.isFolder()){
						return;
					}
					//System.out.println("MainFrame : NewDialog : finished adding new item");
					String itmname = newDialog.getItemName();
					ProjectCategory itmpos = newDialog.getItemPosition();
					
					if(itmname.trim().length() == 0){
						ExceptionHandler.wrongFileName(itmname);
						return;
					}/*else if(itmname.equals(getProject().getProjectName())){
						JOptionPane.showMessageDialog(null, "??? why is this case prohibited?" + itmname);
						return;
					}*/
					
					for(int i = 0; i < itmname.length(); i++){
						char c = itmname.charAt(i);
						if(c == '/' || c == '\\' || c == '|' || c == '"'
								|| c == ':' || c == '*' || c == '?' || c == '<'
								|| c == '>' || !Character.isDefined(c)){
							ExceptionHandler.wrongFileName(itmname);
							return;
						}
					}
					
					if(itmpos == null)
						itmpos = getProject();
					
					if(newDialog.isFolder()){
						ProjectCategory pc = new ProjectCategory(itmpos, itmname);
						itmpos.add(pc);
						pc.setParent(itmpos);
						File file = new File(pc.getTotalPath());
						file.mkdir();
						
						try{
							ProjectXMLHandler.store(getProject());
							getProjectTree().reload();
						}catch(Exception excp){
							ExceptionHandler.cannotSaveProject();
						}
						getProjectTree().reload();
					}else{
						int idx = newDialog.getSelectedIndex();
						itmname += editorManagers[idx].getFileExtension();
						ProjectFile pfile = new ProjectFile(itmpos, itmname);
						
						try{
							File file = new File(pfile.getTotalPath());
							if(file.exists()){
								ExceptionHandler.fileAlreadyExists(pfile.getTotalPath());
								return;
							}
							if(!file.createNewFile()){
								ExceptionHandler.cannotCreateFile(pfile.getTotalPath());
								itmpos.remove(pfile);
								return;
							}

							itmpos.add(pfile);
							if(getEditingFile() != null)
								saveFile(getEditingFile(), new File(getEditingFile().getTotalPath()));

							getProjectTree().reload();
							editorManagers[idx].newFile(pfile);
							ProjectXMLHandler.store(getProject());
						}catch(IOException excp){
							ExceptionHandler.cannotCreateFile(pfile.getTotalPath());
						}
					}
				}
				newDialog.setVisible(false);
			}// end finish()
			public void dispose(){
				newDialog.setVisible(false);
			}// end dispose()
		};
		
		this.openDialog = new JFileChooser();
		openDialog.addChoosableFileFilter(new javax.swing.filechooser.FileFilter(){
			public String getDescription()
			{ return "RoboDari Project File"; }
			public boolean accept(File file){
				if(file.isDirectory())
					return true;
				String fileName = file.getName();
				if(fileName.indexOf('.') == -1) return false;
				if(fileName.substring(fileName.indexOf('.'), fileName.length()).endsWith(".rbdrproj"))
					return true;
				return false;
			}// end accept(File)
		});
		
		this.importDialog = new JFileChooser();
		for(int i = 0; i < editorManagers.length; i++){
			final int idx = i;
			this.importDialog.addChoosableFileFilter(new javax.swing.filechooser.FileFilter(){
				public String getDescription()
				{ return editorManagers[idx].getFileDescription(); }
				public boolean accept(File file){
					if(file.isDirectory())
						return true;
					else if(editorManagers[idx].isEditable(file.getName()))
						return true;
					return false;
				}// end accept(File)
			});
		}
		
		this.noteEditorDialog = new NoteEditorDialog();
	}// end initDialogs()
	
	
	
	
	
	
	
	
	

	public NewDialog getNewDialog()
	{ return newDialog; }
	public JFileChooser getOpenDialog()
	{ return openDialog; }
	public JFileChooser getImportDialog()
	{ return importDialog; }
	public NoteEditorDialog getNoteEditorDialog()
	{ return noteEditorDialog; }
	
	
	
	
	
	
	
	
	
	
	
	public void windowDeactivated(WindowEvent we){}
	public void windowClosing(WindowEvent we){
		if(this.openedProject != null){
			int button = JOptionPane.showConfirmDialog(null, "Would you like to save opened files?", "?", JOptionPane.YES_NO_OPTION);
			switch(button){
			case JOptionPane.YES_OPTION : 
				BugManager.println("MainFrame : windowClosing : YES option");
				this.saveOpenedProject();
				for(int i = 0; i < editorManagers.length; i++){
					ProjectFile[] cpfs = editorManagers[i].getCachedFiles();
					for(ProjectFile eachpf : cpfs)
						editorManagers[i].saveFile(eachpf, new File(eachpf.getTotalPath()));
				}
			case JOptionPane.NO_OPTION : 
				BugManager.println("MainFrame : windowClosing : NO option");
				super.setVisible(false);
				System.exit(0);
				break;
			default : 
				BugManager.println("MainFrame : windowClosing : CANCEL");
				super.setVisible(true);
			}
		}else{
			System.exit(0);
		}
	}// windowClosing(WindowEvent)
	public void windowClosed(WindowEvent we){}
	public void windowDeiconified(WindowEvent we){}
	public void windowOpened(WindowEvent we){}
	public void windowActivated(WindowEvent we){}
	public void windowIconified(WindowEvent we){}
	
	public void mousePressed(MouseEvent me){}
	public void mouseReleased(MouseEvent me){}
	public void mouseEntered(MouseEvent me){}
	public void mouseExited(MouseEvent me){}
	public void mouseClicked(MouseEvent me){
	}// end mouseClicked(MouseEvent)
	
	
	
	
	
	

	
	
	
	
	
	
	
	public ProjectTree getProjectTree()
	{ return projectTree; }
	
	
	public Project getProject()
	{ return openedProject; }
	public void openProject(Project project){
		this.closeAll();
		this.openedProject = project;
		
		projectTree.loadProject(project);
		openedFilePathLabel.setText("");
		
		if(this.openedProject != null){
			newItemMenu.setEnabled(true);
			saveAllMenu.setEnabled(true);
			importMenu.setEnabled(true);

			this.editProjectNoteMenu.setEnabled(true);
		}else{
			newItemMenu.setEnabled(false);
			saveAllMenu.setEnabled(false);
			importMenu.setEnabled(false);

			this.editProjectNoteMenu.setEnabled(false);
		}
	}// end setOpenedProject(Project)
	public ProjectFile getEditingFile()
	{ return editingFile; }
	public ProjectFile[] getOpenedFiles(){
		ArrayList<ProjectFile> openedFiles = new ArrayList<ProjectFile>();
		for(EditorManager eachem : editorManagers)
			Collections.addAll(openedFiles, eachem.getCachedFiles());
		
		return openedFiles.toArray(new ProjectFile[]{});
	}// end getOpenedFiles()
	
	public void openFile(ProjectFile file, boolean reload)
	{ openFile(file, reload, true); }
	public void openFile(ProjectFile pfile, boolean reload, boolean show){
		BugManager.println("Mainframe : openFile() : " + pfile);
		if(this.editingFile != null){
			BugManager.println("MainFrame : openFile() : closing opened File : " + editingFile);
			int lim = this.leftPane.getTabCount();
			for(int i = 1; i < lim; i++){
				this.leftPane.removeTabAt(i);
			}
			lim = this.rightPane.getTabCount();
			for(int i = 0; i < lim; i++)
				this.rightPane.removeTabAt(i);
			
			EditorManager em = currentEditors.get(this.editingFile);
			em.undoMenuBar(this.menuBar);
		}
		
		File file = new File(pfile.getTotalPath());
		if(!file.exists()){
			int result = JOptionPane.showConfirmDialog(null, "File is missing. Do you want to remove this file?");
			if(result == JOptionPane.YES_OPTION){
				pfile.getParent().remove(pfile);
				projectTree.reload();
				return;
			}else return;
		}
		
		EditorManager[] appmgrs = this.getEditorManagers(pfile.getFileName());
		if(appmgrs.length == 0){
			JOptionPane.showMessageDialog(null, "No editor");
			return;
		}
		EditorManager em = appmgrs[0];
		if(show){
			this.editingFile = pfile;
			this.openedEditorManager = em;
			BugManager.println("Mainfrae : opening : em : " + em.getClass());
			this.centerCardLayout.show(this.centerPanel, this.openedEditorManager.getClass().toString());
			
			int lim = this.openedEditorManager.getLeftComponentCount();
			for(int i = 0; i < lim; i++)
				this.leftPane.addTab(this.openedEditorManager.getLeftCompTitle(i), this.openedEditorManager.getLeftComponent(i));
			
			lim = this.openedEditorManager.getRightComponentCount();
			for(int i = 0; i < lim; i++)
				this.rightPane.addTab(this.openedEditorManager.getRightCompTitle(i), this.openedEditorManager.getRightComponent(i));	
			this.openedEditorManager.openFile(pfile, reload);
			openedEditorManager.customizeMenuBar(this.menuBar);
			openedEditorManager.repaint();
			currentEditors.put(pfile, em);
		}
		this.openedFilePathLabel.setText(pfile.getTotalPath());
		this.saveMenu.setEnabled(true);
		this.saveAsMenu.setEnabled(true);
		this.saveAllMenu.setEnabled(true);
		this.closeAllMenu.setEnabled(true);
		BugManager.println("MainFrame : end openFile()");
	}// end openFile(ProjectFile)
	public void saveFile(ProjectFile pfile, File file){
		if(!file.canWrite()){
			ExceptionHandler.readOnlyFile(file.getAbsolutePath());
			return;
		}
		BugManager.println("MainFrame : saveFile : pfile : " + pfile);
		for(int i = 0; i < editorManagers.length; i++){
			BugManager.println("MF : svFile : em " + editorManagers[i].getClass().getCanonicalName() + " : " + editorManagers[i].isCached(pfile));
			if(editorManagers[i].isCached(pfile)){
				editorManagers[i].saveFile(pfile, file);
				break;
			}
		}
	}// end saveFile(File)
	public void saveOpenedProject(){
		//BugManager.println("MainFrame : saveOpenedProject()");
		if(this.openedProject == null) return;
		
		try{
			ProjectXMLHandler.store(openedProject);
		}catch(IOException excp){
			ExceptionHandler.cannotSaveProject();
		}
	}// end saveOpenedProject()
	public void closeAll(){
		for(int i = 0; i < editorManagers.length; i++){
			ProjectFile[] pfiles = editorManagers[i].getCachedFiles();
			for(ProjectFile eachpf : pfiles)
				editorManagers[i].closeFile(eachpf);
		}
		// FIXME close opened file...
		openedFilePathLabel.setText("");
		
		newItemMenu.setEnabled(false);
		saveMenu.setEnabled(false);
		saveAsMenu.setEnabled(false);
		saveAllMenu.setEnabled(false);
		importMenu.setEnabled(false);
	}// end closeAll()
	
	
	
	public void createNewProject(String prjname, String prjpath){
		this.saveOpenedProject();
		File dir = new File(prjpath + File.separator + prjname);
		Project proj = new Project(prjname, new Note());
		proj.setProjectPath(dir.getAbsolutePath());
		try{
			dir.mkdir();
			ProjectXMLHandler.store(proj);
			this.openProject(proj);
		}catch(Exception excp){
			ExceptionHandler.cannotSaveProject();
			this.openProject(null);
			excp.printStackTrace();
			return;
		}
	}// end createNewProject(String, String)
	
	
	
	
	public void setStatusBarText(String text)
	{ statusBar.setText(text); statusBar.repaint(); }
	
	
	
	
	public EditorManager[] getEditorManagers()
	{ return editorManagers; }
	public EditorManager[] getEditorManagers(String fileName){
		BugManager.println("MainFrame : getEditorManagers : fname : "+fileName);
		ArrayList<EditorManager> ems = new ArrayList<EditorManager>();
		for(EditorManager eachem : editorManagers){
			if(eachem.isEditable(fileName))
				ems.add(eachem);
		}
		return ems.toArray(NULLEARRAY);
	}// end getEdiotrManagers(Stirng)
	public EditorManager getOpenedEditorManager()
	{ return this.openedEditorManager; }
	public EditorManager getCurrentEditor(ProjectFile file){
		if(file == null)
			return null;
		return currentEditors.get(file);
	}// end getCurrentEditor(PrjFile)
	
	
	
	
	
	
	
	
	
	public static class NewAction extends AbstractAction{
		boolean isProj;
		boolean isFolder;
		int emidx;
		RobodariInterface ri;
		
		public NewAction(RobodariInterface ri, boolean isProj, boolean isFolder, int emidx, String label, Icon icon){
			super(label, icon);
			this.ri =ri;
			this.isProj = isProj;
			this.isFolder = isFolder;
			this.emidx = emidx;
		}// end Constructor(int, String)
		
		public void actionPerformed(ActionEvent ae){
			NewDialog dialog = ri.getNewDialog();
			dialog.newItem(isProj, isFolder, emidx);
			dialog.setVisible(true);
		}// end actionPerformed(ActionEvent)
	}// end action NewAction
	public static class OpenAction extends AbstractAction{
		private RobodariInterface manager;
		
		public OpenAction(RobodariInterface manager, String label, Icon icon){
			super(label, icon);
			this.manager = manager;
		}// end Constructor

		public void actionPerformed(ActionEvent ae){
			JFileChooser dialog = manager.getOpenDialog();
			int value = dialog.showOpenDialog(null);
			if(value == JFileChooser.APPROVE_OPTION){
				File f = dialog.getSelectedFile();
				
				try{
					ArrayList<SAXParseException> errs = new ArrayList<SAXParseException>();
					ArrayList<SAXParseException> fterrs
							= new ArrayList<SAXParseException>();
					
					Project proj = ProjectXMLHandler.load(f, errs, fterrs);
					if(fterrs.size() != 0){
						ExceptionHandler.wrongProject(fterrs);
					}else if(errs.size() != 0){
						ExceptionHandler.wrongProject(errs);
					}else
						manager.openProject(proj);
				}catch(Exception ioe){
					Toolkit.getDefaultToolkit().beep();
					JOptionPane.showMessageDialog(null, "Project file damaged.");
					ioe.printStackTrace();
				}
			}
		}// end actionPerformed(ActionEvent)
	}// end action OpenAction(AbstractAction)
	public static class ImportAction extends AbstractAction{
		private RobodariInterface manager;
		
		public ImportAction(RobodariInterface manager, String label, Icon icon){
			super(label, icon);
			this.manager = manager;
		}// end Constructor(RobodariInterface, String, Icon)
		
		public void actionPerformed(ActionEvent ae){
			ProjectTree tree = manager.getProjectTree();
			TreePath treepath = tree.getTree().getSelectionPath();
			if(treepath == null) return;
			ProjectItem item = (ProjectItem)treepath.getLastPathComponent();
			if(item instanceof ProjectFile)
				item = (ProjectItem)item.getParent();
			
			ProjectCategory category = (ProjectCategory)item;
			if(category == null)
				category = manager.getProject();
			
			JFileChooser importDialog =manager.getImportDialog();
			int value = importDialog.showOpenDialog(new JButton());
			if(value == JFileChooser.APPROVE_OPTION){
				File original = importDialog.getSelectedFile();

				String path = null;
				File target = null;
				try{
					byte[] data = new byte[256];
					int len = 0;
					
					path = category.getTotalPath() + File.separator + original.getName();
					target = original;
					
					FileInputStream fis = new FileInputStream(original);
					FileOutputStream fos = new FileOutputStream(path);
					
					len = 256;
					while((len = fis.read(data, 0, len)) == 256)
						fos.write(data, 0, len);
					if(len != -1)
						fos.write(data, 0, len);
					
					fis.close();
					fos.close();
					
					category.add(new ProjectFile(category, original.getName()));
					manager.getProjectTree().reload();
				}catch(IOException excp){
					ExceptionHandler.cannotCopyFile(target.getAbsolutePath(), path);
				}
			}
		}// end actionPerformed(ActionEvent)
	}// end action ImportAction
	public static class SaveAction extends AbstractAction{
		private RobodariInterface manager;
		
		public SaveAction(RobodariInterface manager, String label, Icon icon){
			super(label, icon);
			this.manager = manager;
		}// end Constructor(RobodariInterface, String, Icon)
		
		public void actionPerformed(ActionEvent ae){
			manager.saveFile(manager.getEditingFile(), new File(manager.getEditingFile().getTotalPath()));
		}// end actionPerformed(ActionEvent)
	}// end action ImportAction
	public static class SaveAsAction extends AbstractAction{
		private RobodariInterface itfc;
		
		public SaveAsAction(RobodariInterface itfc, String label, Icon icon){
			super(label, icon);
			this.itfc = itfc;
		}// end Constructor(RobodariInterface, String, Icon)
		
		public void actionPerformed(ActionEvent ae){
			if(itfc.getEditingFile() == null)
				return;
			
			final EditorManager em = itfc.getCurrentEditor(itfc.getEditingFile());
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setFileFilter(new javax.swing.filechooser.FileFilter(){
				public String getDescription()
				{ return "sketch file"; }
				public boolean accept(File file){
					if(file.isDirectory())
						return true;
					else if(em.isEditable(file.getName()))
						return true;
					return false;
				}// end accept(File)
			});
			int appr = fileChooser.showSaveDialog(null);
			if(appr == JFileChooser.APPROVE_OPTION){
				File file = fileChooser.getSelectedFile();
				itfc.saveFile(itfc.getEditingFile(), file);
			}
		}// end actionPerformed(ActionEvent)
	}// end action ImportAction
	public static class SaveAllAction extends AbstractAction{
		private RobodariInterface manager;
		
		public SaveAllAction(RobodariInterface manager, String label, Icon icon){
			super(label, icon);
			this.manager = manager;
		}// end Constructor(RobodariInterface, String, Icon)
		
		public void actionPerformed(ActionEvent ae){
			ProjectFile[] files = manager.getOpenedFiles();
			for(ProjectFile eachf : files)
				manager.saveFile(eachf, new File(eachf.getTotalPath()));
		}// end actionPerformed(ActionEvent)
	}// end action SaveAllAction
	public static class CloseAllAction extends AbstractAction{
		private RobodariInterface manager;
		
		public CloseAllAction(RobodariInterface manager, String label, Icon icon){
			super(label, icon);
			this.manager = manager;
		}// end Constructor(RobodariInterface, String, Icon)
		
		public void actionPerformed(ActionEvent ae){
			manager.closeAll();
		}// end actionPerformed(ActionEvent)
	}// end action SaveAllAction
	public static class EditProjectNoteAction extends AbstractAction{
		private RobodariInterface manager;
		
		public EditProjectNoteAction(RobodariInterface manager, String label, Icon icon){
			super(label, icon);
			this.manager = manager;
		}// end Constructor(RobodariInterface, String, Icon)
		
		public void actionPerformed(ActionEvent ae){
			NoteEditorDialog ned = manager.getNoteEditorDialog();
			Project proj = manager.getProject();
			if(proj == null) return;
			
			if(proj.getNote() == null)
				proj.setNote(new Note());
			ned.openNote(proj.getNote());
		}// end actionPerformed(ActionEvent)
	}// end action EditProjectNoteAction
}
