package agdr.robodari.gui;

import java.awt.*;
import java.net.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;

import agdr.robodari.comp.*;

public class NoteEditor extends JPanel{
	private final static Object[] HEADER = new Object[]{ "References" };
	
	private JTextArea descArea;
	private JTable refTable;
	private DefaultTableModel refTableModel;
	private JButton addButton;
	private JButton removeButton;
	
	private Note currentNote;
	
	
	public NoteEditor(){
		descArea = new JTextArea();
		
		refTableModel = new DefaultTableModel(new Object[][]{{}}, HEADER);
		refTable = new JTable(refTableModel);
		
		addButton = new JButton(new AddRefAction(this));
		removeButton = new JButton(new RemoveRefAction(this));
		addButton.setBackground(Color.white);
		removeButton.setBackground(Color.white);
		
		super.setLayout(new BorderLayout());
		
		JPanel centerp = new JPanel();
		centerp.setLayout(new BorderLayout());
		JScrollPane jsp = new JScrollPane(descArea);
		centerp.add(new JLabel("Description : "), BorderLayout.NORTH);
		centerp.add(jsp, BorderLayout.CENTER);
		centerp.setSize(new Dimension(300, 300));
		
		JPanel southp = new JPanel();
		JPanel ctrlpanel = new JPanel();
		ctrlpanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		ctrlpanel.add(addButton);
		ctrlpanel.add(removeButton);
		JScrollPane rtjsp = new JScrollPane(refTable);
		rtjsp.setPreferredSize(new Dimension(100, 100));
		southp.setLayout(new BorderLayout());
		southp.add(ctrlpanel, BorderLayout.NORTH);
		southp.add(rtjsp, BorderLayout.CENTER);
		southp.setSize(100, 100);
		
		super.add(centerp, BorderLayout.CENTER);
		super.add(southp, BorderLayout.SOUTH);
	}// end Constructor
	
	
	public void open(Note note){
		currentNote = note;
		if(note == null){
			descArea.setText("");
			while(refTableModel.getRowCount() != 0)
				refTableModel.removeRow(0);
			return;
		}
		descArea.setText(currentNote.getDescription());
		
		String[] refs = note.getReferences();
		Object[][] data = new Object[refs.length][1];
		refTableModel.setDataVector(data, HEADER);
	}// end open(Note)
	public Note getNote(){
		saveNote();
		return currentNote;
	}// end getNote()
	public void saveNote(){
		currentNote.setDescription(descArea.getText());
		
		ArrayList<String> refs = new ArrayList<String>();
		for(int i = 0; i < refTableModel.getRowCount(); i++){
			Object obj = refTableModel.getValueAt(i, 0);
			if(obj != null)
				refs.add(obj.toString());
		}
		currentNote.setReferences(refs.toArray(new String[]{}));
	}// end saveNote()
	
	
	public static void main(String[] argv){
		JFrame jf = new JFrame("S");
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.setSize(500, 500);
		jf.setContentPane(new NoteEditor());
		jf.setVisible(true);
	}
	

	public static class AddRefAction extends AbstractAction{
		private NoteEditor editor;
		
		public AddRefAction(NoteEditor editor){
			super("+");
			this.editor = editor;
		}// end Constructor

		public void actionPerformed(ActionEvent ae){
			editor.refTableModel.addRow(new Object[]{""});
		}// end actionPerofrmed(ACtionEvent)
	}// end action AddRefAction
	public static class RemoveRefAction extends AbstractAction{
		private NoteEditor editor;
		
		public RemoveRefAction(NoteEditor editor){
			super("-");
			this.editor = editor;
		}// edn Constructor

		public void actionPerformed(ActionEvent ae){
			if(editor.refTable.getSelectedRow() != -1)
				editor.refTableModel.removeRow(editor.refTable.getSelectedRow());
		}// end actionPerofrmed(ACtionEvent)
	}// end action RemoveRefAction
}
