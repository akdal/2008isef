package agdr.robodari.gui;

public interface PopupGenerator {
	public javax.swing.JPopupMenu generatePopup();
}
