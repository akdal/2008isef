package agdr.robodari.gui;

import java.awt.*;
import java.awt.font.*;
import java.awt.geom.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;

import agdr.robodari.appl.*;
import agdr.robodari.edit.*;
import agdr.robodari.gui.dialog.*;
import agdr.robodari.project.*;

public class ProjectTree extends JPanel implements MouseListener{
	private final static String MESSAGE = "Project not opened";
	
	private JTree tree;
	
	private RobodariInterface rbdritfc;
	private Project project;
	private ProjectCategory root;
	private DefaultTreeModel model;
	private ProjectItem selectedItem;
	
	private boolean hasPopup;
	private JPopupMenu popupMenu;
		private JMenuItem newMenuItem;
		private JMenuItem optionItem = new JMenuItem(new ShowFileOptionDialogAction(this));
	
	
	
	public ProjectTree(RobodariInterface manager, Project project, boolean hasPopup){
		super.setBackground(Color.white);
		super.setOpaque(true);
		
		newMenuItem = new JMenuItem(new MainFrame
				.NewAction(manager, false, true, -1, "New Item", null));
		
		this.hasPopup = hasPopup;
		this.rbdritfc = manager;
		this.tree = new JTree();
		
		JScrollPane jsp = new JScrollPane(tree,
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		super.setLayout(new BorderLayout());
		super.add(jsp, BorderLayout.CENTER);
		
		tree.addMouseListener(this);
		if(hasPopup){
			initPopup();
		}
		
		loadProject(project);
	}// end Constructor(Project)
	
	private void initPopup(){
		popupMenu = new JPopupMenu();
		
		popupMenu.add(newMenuItem);
		popupMenu.add(optionItem);
	}// end initPopup()
	
	
	public RobodariInterface getInterface()
	{ return rbdritfc; }
	public ProjectItem getSelectedItem()
	{ return selectedItem; }
	public void collapseAll(){
		for(int i = tree.getRowCount() - 1; i >= 0; i--){
			tree.collapseRow(i);
			
			if((i + 1) > tree.getRowCount())
				i = tree.getRowCount();
		}
	}// end collapseAll()
	public void reload()
	{ model.reload(); }
	
	
	public void mouseClicked(MouseEvent me){
		if(me.getButton() == MouseEvent.BUTTON1){
			TreePath path = tree.getSelectionPath();
			if(path == null) return;
			selectedItem = (ProjectItem)path.getLastPathComponent();
			
			if(me.getClickCount() >= 2 && selectedItem instanceof ProjectFile){
				BugManager.println();
				BugManager.println("ProjectTree : mouseClicked : open()");
				rbdritfc.openFile((ProjectFile)selectedItem, false, true);
				BugManager.println("ProjectTree : mouseClicked : end open()");
			}
		}else if(me.getButton() == MouseEvent.BUTTON3){
			this.popupMenu.show(tree, me.getX(), me.getY());
		}
	}// end mouseClicked(MouseEvent)
	public void mouseEntered(MouseEvent me){}
	public void mouseExited(MouseEvent me){}
	public void mouseReleased(MouseEvent me){}
	public void mousePressed(MouseEvent me){}
	
	
	
	public void paint(Graphics g){
		if(g == null) return;

		super.paint(g);
		if(project == null){
			g.setColor(new Color(235, 235, 235));
			g.fillRect(0, 0, super.getWidth(), super.getHeight());
			
			g.setColor(Color.gray);
			
			Font f = new Font("돋움체", 0, 12);
			AffineTransform trans = f.getTransform();
			FontRenderContext context = new FontRenderContext(trans, false, false);
			Rectangle2D rec = f.getStringBounds(MESSAGE, context);
			
			int posx = 0, posy = 0, wid, hei;
			int scrwid = super.getWidth(), scrhei = super.getHeight();

			if(rec instanceof Rectangle){
				Rectangle r = (Rectangle)rec;
				posx = (scrwid - (r.width - r.x)) / 2;
				posy = (scrhei - (r.height - r.y)) / 2;
			}else if(rec instanceof Rectangle2D.Float){
				Rectangle2D.Float r = (Rectangle2D.Float)rec;
				posx = (int)(scrwid - (r.width - r.x)) / 2;
				posy = (int)(scrhei - (r.height - r.y)) / 2;
			}else if(rec instanceof Rectangle2D.Double){
				Rectangle2D.Double r = (Rectangle2D.Double)rec;
				posx = (int)(scrwid - (r.width - r.x)) / 2;
				posy = (int)(scrhei - (r.height - r.y)) / 2;
			}
			g.drawString(MESSAGE, posx, posy);
		}
	}// end paint(Graphics)
	
	
	public void loadProject(Project project){
		this.project = project;
		
		if(this.project != null){
			tree.setEnabled(true);
			this.root = project;
			
			if(model == null)
				model = new DefaultTreeModel(this.root);
			else
				model.setRoot(this.root);
			tree.setModel(model);
			tree.expandRow(0);
		}else
			tree.setEnabled(false);
	}// end open(Project)
	public Project getProject()
	{ return project; }
	
	public JTree getTree()
	{ return tree; }
	
	
	
	
	
	public static class ProjectItemRenderer extends DefaultTreeCellRenderer{
		public Component getTreeCellRendererComponent(JTree jtree, Object value,
				boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus){
			super.getTreeCellRendererComponent(jtree, value, selected, expanded,
					leaf, row, hasFocus);
			
			if(value instanceof ProjectCategory){
				ProjectCategory category = (ProjectCategory)value;
				if(category.getParent() == null){
				}else{
				}
			}else{
			}
			
			return this;
		}// end getTreeCellRendererComponent(JTree, Object, boolean, boolean,
		// boolean, int, boolean)
	}// end class ProjectItemRenderer
	
	
	public static class ShowFileOptionDialogAction extends AbstractAction{
		ProjectTree tree;
		
		public ShowFileOptionDialogAction(ProjectTree tree){
			super("File properties..");
			this.tree = tree;
		}// end Con
		public void actionPerformed(ActionEvent ae){
			ProjectItem itm = this.tree.getSelectedItem();
			if(!itm.isLeaf())
				return;
			ProjectFile file = (ProjectFile)itm;
			EditorManager mng = tree.getInterface().getCurrentEditor(file);
			if(mng == null || mng.getFileOptionDialog() == null){
				JOptionPane.showMessageDialog(null, "Cannot open dialog");
				return;
			}
			mng.getFileOptionDialog().start(file);
		}// end actionPerformed
	}// end class ShowFileOptionDialogAction
}
