package agdr.robodari.gui;


import agdr.robodari.appl.*;
import agdr.robodari.comp.*;
import agdr.robodari.edit.*;

import java.util.*;
import java.lang.reflect.*;
import java.awt.*;
import java.io.*;
import javax.swing.*;

public class Splash extends JWindow{
	public final static int WIDTH = 400;
	public final static int HEIGHT = 400;
	
	private Image splashImage;
	private boolean flag = true;
	private String status;
	
	public Splash(){
		super.setSize(WIDTH, HEIGHT);
		
		Dimension scrsize = Toolkit.getDefaultToolkit().getScreenSize();
		super.setLocation(scrsize.width / 2 - WIDTH / 2, scrsize.height / 2 - HEIGHT / 2);
		
		try{
			splashImage = Toolkit.getDefaultToolkit()
				.createImage(Splash.class.getResource("icons/splash.jpg"));
			MediaTracker tracker = new MediaTracker(this);
			tracker.addImage(splashImage, 0);
			tracker.waitForID(0);
		}catch(Exception excp){
			BugManager.log(excp, true);
		}
		this.setVisible(true);
		
		startLoading();
	}// end Constructor
	
	private void startLoading(){
		Runnable run = new Runnable(){
			public void run(){

				try{
					status = "GUI setting..";
					
					Font font = new Font("\ub3cb\uc6c0\uccb4", 0, 12);
					Color background = new Color(250, 250, 250);
					Color background2 = new Color(252, 252, 252);
					UIManager.getDefaults().put("Button.font", font);
					UIManager.getDefaults().put("InternalFrame.titleFont", font);
					UIManager.getDefaults().put("Label.font", font);
					UIManager.getDefaults().put("Label.background", background2);
					UIManager.getDefaults().put("TabbedPane.font", font);
					UIManager.getDefaults().put("TabbedPane.background", background);
					UIManager.getDefaults().put("Panel.background", background);
					UIManager.getDefaults().put("MenuItem.font", font);
					UIManager.getDefaults().put("MenuItem.background", background);
					UIManager.getDefaults().put("Menu.font", font);
					UIManager.getDefaults().put("Menu.background", background2);
					UIManager.getDefaults().put("RadioButton.font", font);
					UIManager.getDefaults().put("CheckBox.font", font);
					UIManager.getDefaults().put("List.font", font);
					UIManager.getDefaults().put("ComboBox.font", font);
					UIManager.getDefaults().put("ComboBox.background", background2);
					UIManager.getDefaults().put("OptionPane.background", background2);
					//UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					
					status = "Loading initializers...";
					repaint();
					
					FileReader freader = new FileReader(new File("bootstrap"));
					BufferedReader breader = new BufferedReader(freader);
					ArrayList<Class> emanagers = new ArrayList<Class>();
					String line;
					int state = 0;
					
					System.out.println(breader);
					while((line = breader.readLine()) != null){
						if(line.equals("[initializers]"))
							state = 1;
						else if(line.equals("[editors]"))
							state = 2;
						else{
							if(line.startsWith("#"))
								continue;
							if(state == 1)
								Initializer.register((Initializer)Class.forName(line).newInstance());
							else if(state == 2){
								emanagers.add(Class.forName(line));
							}
						}
						line = null;
					}
						
					freader.close();
					breader.close();
					
					status = "Initializing...";
					Initializer[] inits = Initializer.getInitializers();
					for(Initializer eachinit : inits)
						eachinit.initialize();
					
					
					status = "Building robodari..";
					
					MainFrame frame = new MainFrame((Class[])emanagers.toArray(new Class[]{}));
					status = "OK";
					frame.setVisible(true);
					setVisible(false);
				}catch(InvocationTargetException ite){
					System.err.println("invocation target excp : ");
					ite.printStackTrace();
					BugManager.log(ite, true);
				}catch(Exception excp){
					BugManager.log(excp, true);
				}
			}
		};
		SwingUtilities.invokeLater(run);
	}// end startLoading()
	
	
	private float time = 0;
	public void paint(Graphics g){
		if(g == null) return;
		
		System.out.println("Splash : paint(Graphics)");
		//g.drawImage(splashImage, 0, 0, this);
		if(status != null)
			g.drawString(status, 200, 250);
	}// end paint(Graphics)
	
	
	
	public static void main(String[] argv){
		final Splash sp = new Splash();
	}
}
