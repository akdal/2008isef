package agdr.robodari.gui.dialog;

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.filechooser.*;

import agdr.robodari.edit.*;
import agdr.robodari.gui.*;
import agdr.robodari.project.*;


public abstract class NewDialog extends SequenceDialog implements ActionListener{
	public final static String[] SEQUENCE = new String[]{ "Create.." };
	
	
	private JPanel[] panels = new JPanel[1];
	
	private JRadioButton newProjectButton;
	private JRadioButton newItemButton;
	private ButtonGroup buttonGroup;
	private JPanel toggleButtonsPanel;
	
	protected JPanel newProjectPanel;
		protected JTextField projectNameField;
		protected JTextField projectPathField;
		protected JButton projectPathButton;
	protected JPanel newItemPanel;
		protected JTree itemProjectTree;
		protected JTextField itemNameField;
		protected JList itemList;
	
	private DefaultTreeModel treeModel;
	private DefaultTreeCellRenderer renderer;
	private RobodariInterface rbdrInterface;
	private EditorManager[] managers;
	
	
	public NewDialog(JFrame parent, RobodariInterface rbdrInterface){
		super(parent, "New Dialog");
		this.rbdrInterface = rbdrInterface;
		if(this.rbdrInterface != null)
			this.managers = rbdrInterface.getEditorManagers();
		else
			this.managers = new EditorManager[0];
		
		initToggleButtons();
		initNewProjectPanel();
		initNewItemPanel();
		
		super.setResizable(false);
		super.getCenterPanel().add(toggleButtonsPanel, BorderLayout.NORTH);
		super.getCenterPanel().add(newItemPanel, BorderLayout.CENTER);
		newItemButton.setSelected(true);
		
		super.getSequenceViewer().refresh();
		super.pack();
		
		Dimension scrsize = Toolkit.getDefaultToolkit().getScreenSize();
		super.setLocation((scrsize.width - this.getWidth()) / 2, (scrsize.height - this.getHeight()) / 2);
	}// end Constructor()
	private void initToggleButtons(){
		buttonGroup = new ButtonGroup();
		
		Image newProjectImg = Toolkit.getDefaultToolkit().createImage(NewDialog
				.class.getResource("images/newproject.jpg"));
		Image newItemImg = Toolkit.getDefaultToolkit().createImage(NewDialog
				.class.getResource("images/newitem.jpg"));
		Image newProject_rollOverImg = Toolkit.getDefaultToolkit().createImage(NewDialog
				.class.getResource("images/newproject_rollover.jpg"));
		Image newItem_rollOverImg = Toolkit.getDefaultToolkit().createImage(NewDialog
				.class.getResource("images/newitem_rollover.jpg"));
		
		newProjectButton = new JRadioButton(new ImageIcon(newProjectImg));
		newItemButton = new JRadioButton(new ImageIcon(newItemImg));
		
		newProjectButton.setRolloverEnabled(true);
		newItemButton.setRolloverEnabled(true);
		newProjectButton.setRolloverIcon(new ImageIcon(newProject_rollOverImg));
		newItemButton.setRolloverIcon(new ImageIcon(newItem_rollOverImg));
		
		newProjectButton.setBackground(Color.white);
		newProjectButton.addActionListener(this);
		newItemButton.setBackground(Color.white);
		newItemButton.addActionListener(this);
		buttonGroup.add(newProjectButton);
		buttonGroup.add(newItemButton);
		
		
		toggleButtonsPanel = new JPanel();
		toggleButtonsPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		toggleButtonsPanel.add(newProjectButton);
		toggleButtonsPanel.add(newItemButton);
		toggleButtonsPanel.setOpaque(true);
		toggleButtonsPanel.setBackground(Color.white);
	}// end initToggleButtons()
	private void initNewProjectPanel(){
		newProjectPanel = new NewPanel();
		newProjectPanel.setPreferredSize(new Dimension(450, 320));
		newProjectPanel.setLayout(null);
		newProjectPanel.setOpaque(false);
		
		projectNameField = new JTextField();
		projectNameField.setBounds(160, 115, 140, 25);
		projectPathField = new JTextField();
		projectPathField.setBounds(160, 145, 140, 25);
		projectPathButton = new JButton("...");
		projectPathButton.setBounds(305, 145, 25, 25);
		projectPathButton.addActionListener(this);
		
		JLabel namelbl = new JLabel("Project Name :");
		JLabel pathlbl = new JLabel("Project Path :" );
		namelbl.setBounds(60, 115, 100, 25);
		pathlbl.setBounds(60, 145, 100, 25);
		
		newProjectPanel.add(projectNameField);
		newProjectPanel.add(projectPathField);
		newProjectPanel.add(projectPathButton);
		newProjectPanel.add(namelbl);
		newProjectPanel.add(pathlbl);
	}// end initNewProjectPanel()
	private void initNewItemPanel(){
		newItemPanel = new NewPanel();
		newItemPanel.setPreferredSize(new Dimension(450, 320));
		newItemPanel.setLayout(null);
		newItemPanel.setOpaque(false);
		
		itemProjectTree = new JTree();
		itemList = new JList();
		itemNameField = new JTextField();
		itemNameField.setBounds(300, 80, 100, 25);
		
		renderer = new DefaultTreeCellRenderer();
		renderer.setLeafIcon(null);
		renderer.setOpenIcon(null);
		renderer.setClosedIcon(null);
		itemProjectTree.setCellRenderer(renderer);
		
		JLabel label = new JLabel("Name : ");
		label.setHorizontalAlignment(JLabel.RIGHT);
		label.setBounds(250, 80, 50, 25);
		
		JLabel ptlabel = new JLabel("Itm type : ");
		ptlabel.setBounds(250, 120, 150, 25);
		
		treeModel = new DefaultTreeModel(null);
		itemProjectTree.setModel(treeModel);
		
		JScrollPane jsp = new JScrollPane(itemProjectTree);
		jsp.setBounds(250, 145, 150, 145);
		JScrollPane listPane = new JScrollPane(itemList);
		listPane.setBounds(50, 30, 140, 260);
		newItemPanel.add(label);
		newItemPanel.add(ptlabel);
		newItemPanel.add(jsp);
		newItemPanel.add(listPane);
		newItemPanel.add(itemNameField);
	}// end initNewItemPanel()
	
	
	
	public JPanel[] getPanels()
	{ return panels; }
	public int getPosition()
	{ return 0; }
	public String[] getSequence()
	{ return SEQUENCE; }
	public boolean isTheFirst()
	{ return true; }
	public boolean isTheLast()
	{ return true; }
	
	
	private void _generateTree(DefaultMutableTreeNode node){
		ProjectCategory ctgry = (ProjectCategory)node.getUserObject();
		for(int i = 0; i < ctgry.getChildCount(); i++){
			ProjectItem itm = ctgry.getChildAt(i);
			if(!itm.isLeaf()){
				DefaultMutableTreeNode cn = new DefaultMutableTreeNode(itm);
				node.add(cn);
				_generateTree(cn);
			}
		}
	}// end _generateTree(DefaultMutableTreeNode)
	public void reset(){
		projectNameField.setText("");
		projectPathField.setText("");
		itemNameField.setText("");
		itemList.setSelectedIndex(-1);
		
		this.managers = rbdrInterface.getEditorManagers();
		String[] listData = new String[managers.length + 1];
		listData[0] = "Folder";
		for(int i = 1; i < listData.length; i++)
			listData[i] = managers[i - 1].getFileDescription();
		itemList.setListData(listData);
		if(rbdrInterface.getProject() == null){
			treeModel.setRoot(new DefaultMutableTreeNode("Project not opened"));

			itemProjectTree.setEnabled(false);
			itemList.setEnabled(false);
			itemNameField.setEditable(false);
			newItemButton.setEnabled(false);
		}else{
			itemProjectTree.setEnabled(true);
			
			Project project = rbdrInterface.getProject();
			DefaultMutableTreeNode node = new DefaultMutableTreeNode(project.getRoot());
			treeModel.setRoot(node);
			_generateTree(node);

			for(int i = 0; i < itemProjectTree.getRowCount(); i++){
				itemProjectTree.expandRow(i);
			}
			itemList.setEnabled(true);
			itemNameField.setEditable(true);
			newItemButton.setEnabled(true);
		}
	}// end reset()
	
	public void before(){
		super.getSequenceViewer().refresh();
	}// end before()
	public void next(){
		super.getSequenceViewer().refresh();
	}// end next()
	
	
	public void newItem(boolean isProject, boolean isFolder, int idx){
		if(isProject){
			newProjectButton.setSelected(true);
			super.getCenterPanel().remove(newItemPanel);
			super.getCenterPanel().add(newProjectPanel, BorderLayout.CENTER);
		}else{
			super.getCenterPanel().remove(newProjectPanel);
			super.getCenterPanel().add(newItemPanel, BorderLayout.CENTER);
			newItemButton.setSelected(true);
			if(isFolder)
				itemList.setSelectedIndex(0);
			else
				itemList.setSelectedIndex(idx + 1);
		}
		reset();
	}// end newItem(int)
	
	
	
	public boolean editsProject()
	{ return newProjectButton.isSelected(); }
	public String getProjectName()
	{ return this.projectNameField.getText(); }
	public String getProjectPath()
	{ return this.projectPathField.getText(); }
	public String getItemName()
	{ return this.itemNameField.getText(); }
	
	public boolean isFolder()
	{ return this.itemList.getSelectedIndex() == 0; }
	/**
	 * EditorManager manager = robodariInterface.getEditorManagers()[newDialog.getSelectedIndex()]
	 * @return
	 */
	public int getSelectedIndex(){
		return this.itemList.getSelectedIndex() - 1;
	}// end getSelectedIndex()
	public ProjectCategory getItemPosition(){
		TreePath path = itemProjectTree.getSelectionPath();
		if(path == null)
			return null;
		return (ProjectCategory)(((DefaultMutableTreeNode)path.getLastPathComponent())
				.getUserObject());
	}// end getItemPosition()
	
	
	
	
	
	
	public void actionPerformed(ActionEvent ae){
		if(ae.getSource() == newItemButton){
			super.getCenterPanel().remove(newProjectPanel);
			super.getCenterPanel().add(newItemPanel, BorderLayout.CENTER);
			super.getCenterPanel().repaint();
		}else if(ae.getSource() == newProjectButton){
			super.getCenterPanel().remove(newItemPanel);
			super.getCenterPanel().add(newProjectPanel, BorderLayout.CENTER);
			super.getCenterPanel().repaint();
		}
		
		
		else if(ae.getSource() == projectPathButton){
			JFileChooser jfc = new JFileChooser();
			jfc.setFileFilter(new javax.swing.filechooser.FileFilter(){
				public boolean accept(File f)
				{ return f.isDirectory(); }
				public String getDescription()
				{ return ""; }
			});
			jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			int result = jfc.showOpenDialog(this);
			if(result == JFileChooser.APPROVE_OPTION){
				this.projectPathField.setText(jfc.getSelectedFile().getAbsolutePath());
			}
		}
	}// end actionPerformed(ActionEvent)
	
	
	
	class NewPanel extends JPanel{
		/*private GradientPaint gp1 = new GradientPaint(0, 0,
				new Color(226, 226, 255), 0, 160, new Color(235, 250, 255));
		private GradientPaint gp2 = new GradientPaint(0, 160,
				new Color(235, 250, 255), 0, 320, new Color(92, 154, 198));
		*/
		public void paint(Graphics g){
			if(g == null) return;
			/*Graphics2D g2d = (Graphics2D)g;
			
			g2d.setPaint(gp1);
			g2d.fillRect(0, 0, 450, 160);
			g2d.setPaint(gp2);
			g2d.fillRect(0, 160, 450, 320);
			g2d.setColor(Color.gray);
			g2d.fillRect(0, 0, 450, 2);
			촌스럽다 ㅋㅋ
			*/
			super.paint(g);
		}// end paint(Graphics)
	}// end class NewProjectPanel
}
