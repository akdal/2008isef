package agdr.robodari.gui.dialog;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import agdr.robodari.comp.*;
import agdr.robodari.gui.*;

public class NoteEditorDialog extends JDialog implements ActionListener{
	private NoteEditor noteEditor;
	private JButton okayButton;
	private JButton closeButton;
	
	public NoteEditorDialog(){
		noteEditor = new NoteEditor();
		okayButton = new JButton("Okay");
		closeButton = new JButton("Close");
		okayButton.addActionListener(this);
		closeButton.addActionListener(this);

		noteEditor.setPreferredSize(new Dimension(400, 400));
		noteEditor.setSize(new Dimension(400, 400));
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(okayButton);
		buttonPanel.add(closeButton);
		
		super.getContentPane().setLayout(new BorderLayout());
		super.getContentPane().add(noteEditor, BorderLayout.CENTER);
		super.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
		super.pack();
		
		Dimension scrsize = Toolkit.getDefaultToolkit().getScreenSize();
		super.setLocation((scrsize.width - this.getWidth()) / 2,
				(scrsize.height - this.getHeight()) / 2);
	}// end Constructor()
	
	public void actionPerformed(ActionEvent ae){
		if(ae.getSource() == okayButton){
			saveNote();
			super.setVisible(false);
		}else if(ae.getSource() == closeButton)
			super.setVisible(false);
	}// end acitonPerformed
	
	public void openNote(Note note){
		noteEditor.open(note);
		this.setVisible(true);
	}// end openNote(Note)
	public void saveNote()
	{ noteEditor.saveNote(); }
}
