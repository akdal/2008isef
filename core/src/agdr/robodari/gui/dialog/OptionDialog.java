package agdr.robodari.gui.dialog;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import javax.swing.border.*;

public abstract class OptionDialog<T> extends JDialog
		implements ActionListener, MouseListener{
	private final static Dimension DIALOG_SIZE = new Dimension(600, 400);
	
	private JTree optionTree;
	private JPanel centerPanel;
	private JPanel viewPanel;
	private CardLayout viewCard;
	
	private JPanel southPanel;
	private JButton okButton;
	private JButton cancelButton;
	
	
	public OptionDialog(Frame parent, String title){
		super(parent, title, true);
		_init();
	}// end Constrcutor(JFrame)
	public OptionDialog(Dialog parent, String title){
		super(parent, title, true);
		_init();
	}// end Constructor(JDialog)
	private void _init(){
		_initcomps();
		super.setSize(DIALOG_SIZE);
		super.setResizable(false);
		Dimension scrsize = Toolkit.getDefaultToolkit().getScreenSize();
		super.setLocation((scrsize.width - DIALOG_SIZE.width) / 2,
				(scrsize.height - DIALOG_SIZE.height) / 2);
	}// end _init()
	private void _initcomps(){
		super.setLayout(new BorderLayout());
		
		centerPanel = new JPanel();
		centerPanel.setLayout(new BorderLayout());
		centerPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
		
		optionTree = new JTree();
		optionTree.setRootVisible(false);
		optionTree.setCellRenderer(this.getTreeCellRenderer());
		optionTree.setModel(this.getTreeModel());
		optionTree.addMouseListener(this);
		this.viewPanel = new JPanel();
		this.viewCard = new CardLayout();
		this.viewPanel.setLayout(this.viewCard);
		
		JScrollPane jsp = new JScrollPane(optionTree);
		jsp.setPreferredSize(new Dimension(100, 100));
		this.centerPanel.add(jsp, BorderLayout.WEST);
		this.centerPanel.add(this.viewPanel, BorderLayout.CENTER);
		
		this.southPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		this.okButton = new JButton("Okay");
		this.cancelButton = new JButton("Cancel");
		this.okButton.addActionListener(this);
		this.cancelButton.addActionListener(this);
		this.southPanel.add(okButton);
		this.southPanel.add(cancelButton);
		
		super.add(centerPanel, BorderLayout.CENTER);
		super.add(southPanel, BorderLayout.SOUTH);
	}// end _initcomps()
	
	
	
	public void actionPerformed(ActionEvent ae){
		Object src = ae.getSource();
		if(src == this.okButton){
			if(!save())
				return;
			super.setVisible(false);
		}else if(src == this.cancelButton){
			super.setVisible(false);
		}
	}// end actionPerofrmed(ActionEvent)
	public void mouseClicked(MouseEvent me){
		if(me.getClickCount() >= 2 && me.getButton() == MouseEvent.BUTTON1){
			TreePath path = this.optionTree.getSelectionPath();
			if(path == null) return;
			this.viewCard.show(this.viewPanel, path.toString());
		}
	}// end mouseClicked(MouseEvent)
	public void mouseEntered(MouseEvent me){}
	public void mouseExited(MouseEvent me){}
	public void mouseReleased(MouseEvent me){}
	public void mousePressed(MouseEvent me){}
	
	
	
	public void start(T obj){
		System.out.println("OptionDialog : start(T)");
		resetInputs(obj);
		for(int i = 0; i < this.optionTree.getRowCount(); i++)
			this.optionTree.collapseRow(i);
		
		this.viewPanel.removeAll();
		TreeModel model = this.getTreeModel();
		TreePath rootpath = new TreePath(model.getRoot());
		_registerPaths(model, rootpath);
		
		this.viewCard.show(this.viewPanel, this.optionTree.getPathForRow(0).toString());
		super.setVisible(true);
	}// end start()
	private void _registerPaths(TreeModel model, TreePath path){
		int cnt = model.getChildCount(path.getLastPathComponent());
		TreePath focuspath;
		for(int i = 0; i < cnt; i++){
			focuspath = path.pathByAddingChild(model.getChild(path.getLastPathComponent(), i));
			JComponent view = this.getView(focuspath);
			view.setPreferredSize(new Dimension(600, 300));
			System.out.println("_regPaths : add : " + focuspath + " , " + this.getView(focuspath));
			this.viewPanel.add(view, focuspath.toString());
			_registerPaths(model, focuspath);
		}
	}// end _registerPaths(TreeModel, TreePath)
	
	// 실패하면 false반환.
	public abstract boolean save();
	public abstract void resetInputs(T t);
	public abstract TreeCellRenderer getTreeCellRenderer();
	public abstract TreeModel getTreeModel();
	public abstract JComponent getView(TreePath key);
}
