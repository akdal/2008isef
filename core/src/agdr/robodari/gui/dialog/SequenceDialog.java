package agdr.robodari.gui.dialog;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;


public abstract class SequenceDialog extends JDialog{
	private SequenceViewer sequenceViewer;
	private JPanel centerPanel;
	private JPanel buttonPanel;
	
	protected JButton nextButton;
	protected JButton beforeButton;
	protected JButton cancelButton;
	
	
	public SequenceDialog(JFrame parent, String dialogName){
		super(parent, dialogName);
		
		initSequenceViewer();
		initButtonPanel();
		initCenterPanel();
		
		super.getContentPane().setLayout(new BorderLayout());
		super.getContentPane().add(sequenceViewer, BorderLayout.WEST);
		JPanel tmppanel = new JPanel();
		tmppanel.setLayout(new BorderLayout());
		tmppanel.add(centerPanel, BorderLayout.CENTER);
		tmppanel.add(buttonPanel, BorderLayout.SOUTH);
		super.getContentPane().add(tmppanel, BorderLayout.CENTER);
	}// end Constructor(String)
	private void initSequenceViewer(){
		sequenceViewer = new SequenceViewer(this);
	}// end initSequenceViewer()
	private void initButtonPanel(){
		buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		
		nextButton = new JButton("Next");
		nextButton.addActionListener(new NextAction());
		if(getSequence().length == 1)
			nextButton.setText("Finish");
		cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new CancelAction());
		beforeButton = new JButton("Before");
		beforeButton.addActionListener(new BeforeAction());
		
		buttonPanel.add(cancelButton);
		buttonPanel.add(nextButton);
		buttonPanel.add(beforeButton);
	}// end initButtonPanel()
	private void initCenterPanel(){
		centerPanel = new JPanel();
		centerPanel.setLayout(new BorderLayout());
	}// end initCenterPane()
	
	
	public abstract String[] getSequence();
	public abstract int getPosition();
	public abstract boolean isTheLast();
	public abstract boolean isTheFirst();
	public abstract JPanel[] getPanels();

	public abstract void reset();
	public abstract void finish();
	public abstract void dispose();
	public abstract void next();
	public abstract void before();
	public void showPanel(int idx){
		JPanel[] panels = this.getPanels();
		
		centerPanel.removeAll();
		centerPanel.add(panels[idx], BorderLayout.CENTER);
	}// end showPanel(int)
	
	public void setVisible(boolean f){
		if(!f){
			super.setVisible(false);
			return;
		}
		nextButton.setText("Next");
		beforeButton.setEnabled(false);
		reset();
		
		super.setVisible(true);
	}// end setVisible(boolean)
	
	public JPanel getCenterPanel()
	{ return centerPanel; }
	public SequenceViewer getSequenceViewer()
	{ return sequenceViewer; }
	
	
	
	class NextAction implements ActionListener{
		public void actionPerformed(ActionEvent ae){
			if(!beforeButton.isEnabled())
				beforeButton.setEnabled(true);
			
			if(isTheLast())
				finish();
			else{
				next();
				if(isTheLast())
					nextButton.setText("Finish");
			}
		}// end actionPerformed
	}// end action OKAction
	class CancelAction implements ActionListener{
		public void actionPerformed(ActionEvent ae){
			dispose();
		}// end actionPerformed
	}// end actionCancelAction
	class BeforeAction implements ActionListener{
		public void actionPerformed(ActionEvent ae){
			if(isTheLast())
				nextButton.setText("Finish");
			before();
			if(isTheFirst())
				beforeButton.setEnabled(false);
		}// end actionPerformed
	}// end actionCancelAction
	
	public static class SequenceViewer extends JPanel{
		private final static Color LINECOLOR = new Color(128, 128, 128, 80);
		
		private SequenceDialog sqcDialog;
		private JLabel sqcLabel;
		
		public SequenceViewer(SequenceDialog sqcDialog){
			this.sqcDialog = sqcDialog;
			
			this.sqcLabel = new JLabel(){
				public void paint(Graphics g){
					if(g == null) return;
	
					Dimension dim = super.getSize();
					Graphics2D g2d = (Graphics2D)g;
					g2d.setColor(Color.white);
					g2d.fillRect(0, 0, 150, 1500);
					
					GradientPaint gp = new GradientPaint(0, 0,
							new Color(0, 128, 192, 80),
							0, dim.height, new Color(0, 64, 128, 200));
					g2d.setPaint(gp);
					g2d.fillRect(0, 0, dim.width, dim.height);
					g2d.setColor(LINECOLOR);
					g2d.fillRect(dim.width - 4, 2, dim.width - 2, dim.height - 2);
					
					super.paint(g);
				}// end paint(Graphics);
			};
			this.sqcLabel.setVerticalAlignment(JLabel.TOP);
			this.sqcLabel.setFont(new Font("List", 0, 15));
			this.sqcLabel.setBorder(new LineBorder(Color.white, 2));
			
			super.setPreferredSize(new Dimension(150, 0));
			super.setOpaque(true);
			super.setLayout(new BorderLayout());
			super.add(sqcLabel, BorderLayout.CENTER);
		}// end Constructor(SequenceDialog)
		
		public void refresh(){
			System.out.println("sequence dialog : refresh");
			String[] list = sqcDialog.getSequence();
			StringBuffer buf = new StringBuffer();
			buf.append("<html>");
			
			for(int i = 0; i < list.length; i++){
				if(sqcDialog.getPosition() == i)
					buf.append("<b>");
				buf.append(i + 1).append(". ").append(list[i]).append("<br>");
				if(sqcDialog.getPosition() == i)
					buf.append("</b>");
			}
			sqcLabel.setText(buf.toString());
		}// end refresh()
		
		
	}// end class SequenceViewer
}
