package agdr.robodari.project;

import java.beans.*;
import java.io.*;
import java.net.*;
import java.util.Date;

import javax.swing.tree.*;
import javax.xml.parsers.*;
import org.xml.sax.helpers.*;
import org.xml.sax.*;

import agdr.robodari.appl.*;
import agdr.robodari.comp.*;

public class Project extends ProjectCategory{
	private final static long serialVersionUID = 673956720345L;
	private Note note;
	private transient String projectPath;
	private String projectName;
	
	
	public Project(String projName, Note note){
		super(null, projName);
		setNote(note);
		setProjectName(projName);
	}// end Constructor(String)
	
	
	public Note getNote()
	{ return note; }
	public String getProjectPath()
	{ return projectPath; }
	
	public void setNote(Note note){
		this.note = note;
	}// end setNote(Note)
	public void setProjectPath(String projectPath){
		this.projectPath = projectPath;
	}// end setDate(Date)
	
	public String getProjectName()
	{ return projectName; }
	public void setProjectName(String prjname)
	{ this.projectName = prjname; }
	
	public void setCategoryName(String cname)
	{ this.projectName = cname; }
	public void setUserObject(Object obj){}
	public void setParent(MutableTreeNode mtn){}
	
	public String getTotalPath(){
		return this.getProjectPath();
	}// end getTotalPath()
	
	public String toString()
	{ return getProjectName(); }
}
