package agdr.robodari.project;

import java.io.*;
import java.util.*;
import javax.swing.tree.*;

import agdr.robodari.comp.*;


public class ProjectCategory extends ProjectItem{
	private String categoryName;
	private Vector<ProjectItem> items = new Vector<ProjectItem>();
	
	
	public ProjectCategory(ProjectCategory parent, String categoryName){
		super(null, parent);
		this.parent = parent; 
		setCategoryName(categoryName);
	}// end Constructor(String)
	public ProjectCategory(ProjectCategory parent, Note note, String categoryName){
		super(note, parent);
		this.parent = parent;
		setCategoryName(categoryName);
	}// end Constructor(Note, String)
	
	
	public ProjectCategory getParent()
	{ return parent; }
	
	
	public String getTotalPath(){
		if(parent == null)
			return categoryName;
		return parent.getTotalPath() + java.io.File.separator + categoryName;
	}// end getTotalPath()
	
	
	public String getCategoryName()
	{ return categoryName; }
	public void setCategoryName(String cn)
	{ this.categoryName = cn; }
	
	public ProjectCategory getRoot(){
		if(this.getParent() == null)
			return this;
		return this.getParent().getRoot();
	}// end getRoot()
	
	public ProjectItem[] getItems()
	{ return items.toArray(new ProjectItem[]{}); }
	public ProjectItem getChildAt(int idx)
	{ return items.get(idx); }
	public void add(ProjectItem itm)
	{ items.add(itm); 
	}
	public void insert(MutableTreeNode mtn, int idx){
		if(!(mtn instanceof ProjectItem))
			return;
		ProjectItem itm = (ProjectItem)mtn;
		items.insertElementAt(itm, idx);
	}// end insert(MutableTreeNode, int)
	public int getChildCount()
	{ return items.size(); }
	public boolean contains(ProjectItem itm)
	{ return items.contains(itm); }
	public int getIndex(TreeNode mtn)
	{ return items.indexOf(mtn); }
	public void remove(MutableTreeNode mtn)
	{ items.remove(mtn); }
	public void remove(int idx)
	{ items.remove(idx); }
	
	public void setUserObject(Object str)
	{ this.categoryName = str.toString(); }
	
	
	
	public boolean getAllowsChildren()
	{ return true; }
	public Enumeration<MutableTreeNode> children(){
		Enumeration<MutableTreeNode> children = new
				Enumeration<MutableTreeNode>(){
			private MutableTreeNode[] nodes = items.toArray(new MutableTreeNode[]{});
			private int idx = 0;
			
			public boolean hasMoreElements()
			{ return idx < nodes.length; }
			public MutableTreeNode nextElement(){
				return nodes[idx++];
			}// end nextElement()
		};
		return children;
	}// end children()
	public boolean isLeaf()
	{ return false; }
	
	
	
	public String toString()
	{ return categoryName; }
	
	
	public boolean equals(Object obj){
		if(obj == this) return true;
		else if(obj == null) return false;
		else if(obj instanceof ProjectCategory){
			ProjectCategory pc = (ProjectCategory)obj;
			return pc.categoryName.equals(this.categoryName)
				&& pc.items.equals(this.items); 
		}
		return false;
	}// end equals(Object)
}
