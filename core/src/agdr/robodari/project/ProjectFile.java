package agdr.robodari.project;

import java.util.*;
import java.io.*;
import javax.swing.tree.*;

import agdr.robodari.comp.*;

public class ProjectFile extends ProjectItem{
	private String fileName;
	private Note note;
	
	
	public ProjectFile(ProjectCategory parent, String fileName)
	{ this(parent, fileName, null); }
	public ProjectFile(ProjectCategory parent, String fileName, Note note){
		super(note, parent);
		this.fileName = fileName;
	}// end Constructor(File, Note)
	
	public String getFileName()
	{ return fileName; }
	public void setFile(String f)
	{ this.fileName = f; }
	
	public int length()
	{ return fileName.length(); }
	
	public int getChildCount()
	{ return 0; }
	public int getIndex(TreeNode mtn)
	{ return -1; }
	public void remove(int idx){}
	public void remove(MutableTreeNode mtn){}
	public boolean isLeaf()
	{ return true; }
	public ProjectItem getChildAt(int idx)
	{ return null; }
	public Enumeration<ProjectItem> children()
	{ return null; }
	public boolean getAllowsChildren()
	{ return false; }
	public void insert(MutableTreeNode mtn, int idx)
	{}
	public void setUserObject(Object obj){
		if(obj == null) return;
		String fname = obj.toString();
		try{
			File orgnl = new File(getTotalPath());
			File dest = new File(parent.getTotalPath() + File.separator
					+ fname);
			
			orgnl.renameTo(dest);
		}catch(Exception excp){
			excp.printStackTrace();
		}
	}// end setUserObject(Object)

	public String getTotalPath()
	{ return parent.getTotalPath() + File.separator + this.fileName; }
	
	public boolean equals(Object obj){
		if(obj == null) return false;
		else if(obj == this) return true;
		else if(obj instanceof ProjectFile){
			ProjectFile pf = (ProjectFile)obj;
			if(pf.note != null)
				return pf.getParent().equals(this.parent)
					&& pf.note.equals(this.note)
					&& pf.fileName.equals(this.fileName); 
			return pf.getParent() == this.parent
				&& pf.fileName.equals(this.fileName);
		}
		return false;
	}// end equals(Object)
	public String toString()
	{ return this.fileName; }
}
