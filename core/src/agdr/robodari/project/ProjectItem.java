package agdr.robodari.project;

import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreePath;

import agdr.robodari.comp.*;

public abstract class ProjectItem implements java.io.Serializable,
		javax.swing.tree.MutableTreeNode{
	protected Note note;
	protected ProjectCategory parent;
	
	public ProjectItem(Note note, ProjectCategory parent){
		setNote(note);
		this.parent = parent;
	}// end cotr
	
	public Note getNote(){ return note; }
	public void setNote(Note note)
	{ this.note = note; }

	public ProjectCategory getParent()
	{ return parent; }
	public void setParent(MutableTreeNode parent){
		if(!(parent instanceof ProjectCategory))
			return;
		
		System.out.println("ProjectItem : setParent : this : " + this.toString() + " , parent :"+ parent);
		removeFromParent();
		this.parent = (ProjectCategory)parent;
		if(!this.parent.contains(this)){
			this.parent.add(this);
		}
	}// end setParent(MutableTreeNode)
	public void removeFromParent(){
		if(this.parent != null && this.parent.contains(this))
			this.parent.remove(this);
	}// end removeFromParent()
	
	public TreePath getTreePath(){
		if(parent == null)
			return new TreePath(this);
		Object[] pobj = parent.getTreePath().getPath();
		Object[] newobj = new Object[pobj.length + 1];
		System.arraycopy(pobj, 0, newobj, 0, pobj.length);
		newobj[newobj.length - 1] = this;
		return new TreePath(newobj);
	}// end getTreePath()
	
	
	public boolean equals(Object obj){
		if(obj == null) return false;
		else if(obj == this) return true;
		else if(obj instanceof ProjectItem){
			ProjectItem pf = (ProjectItem)obj;
			return pf.note.equals(this.note); 
		}
		return false;
	}// end equals(Object)
}
