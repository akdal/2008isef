package agdr.robodari.project;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.*;
import java.text.*;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.*;
import org.xml.sax.helpers.*;

import agdr.library.lang.*;
import agdr.robodari.appl.ApplicationInfo;
import agdr.robodari.comp.Note;

public class ProjectXMLHandler extends DefaultHandler{
	public final static String ERROR_WRONGPROJDECL = "Wrong declaration : project";
	public final static String ERROR_WRONGPROJATTRS = "Wrong Attributes : project :" +
			" only name allowed";
	public final static String ERROR_PROJ_WRONGDATE = "Wrong Value : project : " +
			"Illegal Date";
	public final static String ERROR_WRONGPROJTER = "Wrong termination : project";

	public final static String ERROR_WRONGITMSDECL = "Wrong declaration : items";
	public final static String ERROR_WRONGITMSATTRS = "Wrong Attributes : items :" +
			" no attributes allowed";
	public final static String ERROR_WRONGITMSTER = "Wrong termination : items";
	
	public final static String ERROR_WRONGFILEDECL = "Wrong declaration : file";
	public final static String ERROR_WRONGFILEATTRS = "Wrong Attributes : file :" +
			" only name allowed";
	public final static String ERROR_WRONGFILETER = "Wrong termination : file";
	
	public final static String ERROR_WRONGCTGRDECL = "Wrong declaration : category";
	public final static String ERROR_WRONGCTGRATTRS = "Wrong Attributes : category :" +
			" only name allowed";
	public final static String ERROR_WRONGCTGRTER = "Wrong termination : category";
	
	public final static String ERROR_WRONGNOTEDECL = "Wrong declaration : note";
	public final static String ERROR_WRONGNOTETER = "Wrong termination : note";

	public final static String ERROR_WRONGDESCDECL = "Wrong declaration : description";
	public final static String ERROR_WRONGDESCTER = "Wrong termination : description";

	public final static String ERROR_WRONGREFDECL = "Wrong declaration : reference";
	public final static String ERROR_WRONGREFTER = "Wrong termination : reference";

	public final static String ERROR_UNKNOWNELEM = "Unknown Element : ";
	public final static String ERROR_NOTDECLARED = "Not declared : ";
	
	
	
	
	
	
	///////////////////////////////////////////////
	//              PROJECT STORE                //
	///////////////////////////////////////////////

	public static synchronized void store(Project prj) throws IOException{
		File projectFile = new File(prj.getProjectPath() + File.separator
				+ prj.getProjectName() + ".rbdrproj");
		if(!projectFile.exists())
			projectFile.createNewFile();
		
		FileWriter fw = new FileWriter(projectFile);
		BufferedWriter writer = new BufferedWriter(fw);
		writer.write("<?xml version=\"1.0\" encoding=\""
				+ ApplicationInfo.ENCODING + "\" ?>");
		writer.write((char)Character.LINE_SEPARATOR);
		
		writer.write("<project name=\""
				+ prj.getProjectName()
				+ "\">");
		writer.write((char)Character.LINE_SEPARATOR);
		_storeNote(writer, prj.getNote());
		
		writer.write("<items>");
		writer.write((char)Character.LINE_SEPARATOR);
		
		for(int i = 0; i < prj.getChildCount(); i++){
			//System.out.println("ProjXMLHndler : i : " + i + " , prj.getChidAt(i) : " + prj.getChildAt(i));
			_storeNode(writer, prj.getChildAt(i));
		}
		
		writer.write("</items>");
		writer.write((char)Character.LINE_SEPARATOR);
		
		writer.write("</project>");
		
		writer.flush();
		writer.close();
		fw.close();
	}// end store()
	private static void _storeNote(BufferedWriter writer,
			Note note) throws IOException{
		writer.write("    <note>");
		writer.write((char)Character.LINE_SEPARATOR);
		writer.write("    <description>");
		
		if(note != null)
			writer.write(URLEncoder.encode(note.getDescription(),
				ApplicationInfo.ENCODING));
		writer.write("</description>");
		writer.write((char)Character.LINE_SEPARATOR);
		
		if(note != null){
			for(String eachref : note.getReferences()){
				writer.write("    <reference>");
				writer.write(URLEncoder.encode(eachref,
						ApplicationInfo.ENCODING));
				writer.write("    </reference>");
				writer.write((char)Character.LINE_SEPARATOR);
			}
		}
		writer.write("    </note>");
		writer.write((char)Character.LINE_SEPARATOR);
	}// end _storeNote(BufferedWriter, ProjectItem)
	private static void _storeNode(BufferedWriter writer,
			ProjectItem item) throws IOException{
		if(item instanceof ProjectFile){
			writer.write("<file name=\"" + ((ProjectFile)item).getFileName() + "\">");
			writer.write((char)Character.LINE_SEPARATOR);
			_storeNote(writer, item.getNote());
			writer.write("</file>");
			writer.write((char)Character.LINE_SEPARATOR);
		}else{
			ProjectCategory ctgry = (ProjectCategory)item;
			
			writer.write("<category name=\"" + ctgry.getCategoryName() + "\">");
			writer.write((char)Character.LINE_SEPARATOR);
			_storeNote(writer, item.getNote());
			
			for(int i = 0; i < ctgry.getChildCount(); i++)
				_storeNode(writer, ctgry.getChildAt(i));
			writer.write("</category>");
			writer.write((char)Character.LINE_SEPARATOR);
		}
	}// end _storeNode(BufferedWriter, ProjectItem)
	
	
	public static Project load(File file, ArrayList<SAXParseException> errors,
			ArrayList<SAXParseException> fatalErrors)
			throws IOException, SAXParseException, SAXException,
			ParserConfigurationException{
		File path = file.getParentFile();

		SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
		ProjectXMLHandler prjXMLHandler = new ProjectXMLHandler();
		parser.parse(file, prjXMLHandler);
		errors.addAll(prjXMLHandler.getErrors());
		fatalErrors.addAll(prjXMLHandler.getFatalErrors());
		
		Project prj = prjXMLHandler.getProduct();
		prj.setProjectPath(path.getAbsolutePath());
		return prj;
	}// end load(File, Project)
	
	
	
	
	
	

	private final static int ELEM_PROJECT = 1;
	private final static int ELEM_ITEMS = 2;
	private final static int ELEM_FILE = 3;
	private final static int ELEM_CATEGORY = 4;
	private final static int ELEM_NOTE = 5;
	private final static int ELEM_DESCRIPTION = 6;
	private final static int ELEM_REFERENCE = 7;
	
	private ArrayList<SAXParseException> errors;
	private ArrayList<SAXParseException> fatalErrors;
	private Stack<Integer> position;

	private Project project;
	private ProjectItem focus;
	private Note focusNote;
	
	
	
	
	///////////////////////////////////////////////
	//         SAX PARSER IMPLEMENTATION         //
	///////////////////////////////////////////////
	public void startDocument(){
		errors = new ArrayList<SAXParseException>();
		fatalErrors = new ArrayList<SAXParseException>();
		project = new Project(null, null);
		position = new Stack<Integer>();
		
		focusNote = null;
		focus = project;
	}// end startDocument()
	
	public void error(SAXParseException spe)
	{ errors.add(spe); }
	public void fatalError(SAXParseException spe)
	{ fatalErrors.add(spe); }
	
	
	public void startElement(String uri, String localName, String qName,
			Attributes attrs){
		// start element���� ���� �� �� : 
		// 1. project : root
		// 2. items
		// 2-1. file
		// 2-2. category
		//   3. note
		//   3-1.   description
		//   3-2.   reference
		qName = qName.toLowerCase();
		if(qName.equals("project")){
			if(position.size() != 0){
				errors.add(new SAXParseException(ERROR_WRONGPROJDECL, null));
			}
			String name = "";
			if(attrs.getLength() != 1){
				errors.add(new SAXParseException(ERROR_WRONGPROJATTRS, null));
			}else if(!attrs.getQName(0).toLowerCase().equals("name"))
				errors.add(new SAXParseException(ERROR_WRONGPROJATTRS, null));
			else{
				name = attrs.getValue(0);
			}
			
			//System.out.println("PXH : start : " + qName);
			//System.out.println("PXH : start : Date : " + szdate);
			//for(int i = 0; i < attrs.getLength(); i++)
			//	System.out.println("PXH : start : attr[i] : " + attrs.getQName(i));
			position.push(ELEM_PROJECT);
			this.project = new Project(name, null);
			this.focus = this.project;
		}else if(qName.equals("items")){
			if(position.size() != 1 || position.peek() != ELEM_PROJECT){
				errors.add(new SAXParseException(ERROR_WRONGITMSDECL, null));
			}
			if(attrs.getLength() != 0)
				errors.add(new SAXParseException(ERROR_WRONGITMSATTRS, null));

			//System.out.println("PXH : start : items, ps size : " + position.size());
			position.push(ELEM_ITEMS);
		}else if(qName.equals("file")){
			if((position.size() == 0) || (position.peek() != ELEM_ITEMS
					&& position.peek() != ELEM_CATEGORY)){
				//System.out.println("PXH : start : file : DECL ERR! : pos.size : " + position.size() + " , peek : " + position.peek());
				errors.add(new SAXParseException(ERROR_WRONGFILEDECL, null));
			}
			String szname = "";
			if(attrs.getLength() != 1)
				errors.add(new SAXParseException(ERROR_WRONGFILEATTRS, null));
			else if(!attrs.getQName(0).toLowerCase().equals("name"))
				errors.add(new SAXParseException(ERROR_WRONGFILEATTRS, null));
			else{
				szname = attrs.getValue(0);
			}
			
			position.push(ELEM_FILE);
			while(focus instanceof ProjectFile)
				focus = (ProjectItem)focus.getParent();
			ProjectFile newfile = new ProjectFile((ProjectCategory)focus,
					szname, null);
			//System.out.println("ProjXMLHdlr : startElem : newfile");
			((ProjectCategory)focus).add(newfile);
		}else if(qName.equals("category")){
			if((position.size() == 0) || 
					(position.peek() != ELEM_ITEMS
							&& position.peek() != ELEM_CATEGORY)){
				errors.add(new SAXParseException(ERROR_WRONGFILEDECL, null));
			}
			String szname = "";
			if(attrs.getLength() != 1)
				errors.add(new SAXParseException(ERROR_WRONGCTGRATTRS, null));
			else if(!attrs.getQName(0).toLowerCase().equals("name"))
				errors.add(new SAXParseException(ERROR_WRONGCTGRATTRS, null));
			else{
				szname = attrs.getValue(0);
			}
			
			position.push(ELEM_CATEGORY);
			while(focus instanceof ProjectFile)
				focus = (ProjectItem)focus.getParent();
			ProjectCategory newctgr = new ProjectCategory((ProjectCategory)focus,
					szname);
			((ProjectCategory)focus).add(newctgr);
			focus = newctgr;
		}else if(qName.equals("note")){
			if((position.size() == 0) ||
					(position.peek() != ELEM_FILE
							&& position.peek() != ELEM_CATEGORY
							&& position.peek() != ELEM_PROJECT)){
				errors.add(new SAXParseException(ERROR_WRONGFILEDECL, null));
			}
			//System.out.println("PXH : start : note");
			position.push(ELEM_NOTE);
			this.focusNote = new Note();
		}else if(qName.equals("description")){
			if((position.size() == 0) || 
					(position.peek() != ELEM_NOTE)){
				errors.add(new SAXParseException(ERROR_WRONGNOTEDECL, null));
			}
			position.push(ELEM_DESCRIPTION);
		}else if(qName.equals("reference")){
			if((position.size() == 0) || 
					(position.peek() != ELEM_NOTE)){
				errors.add(new SAXParseException(ERROR_WRONGNOTEDECL, null));
			}
			//System.out.println("PXH : start : rfrnc");
			position.push(ELEM_REFERENCE);
		}else{
			errors.add(new SAXParseException(ERROR_UNKNOWNELEM + qName, null));
		}
		//System.out.println("PXH : start : " + qName + " , focus : " + focus);
	}// end startElement(String, String, String, Attribute)
	public void endElement(String uri, String localName, String qName){
		qName = qName.toLowerCase();
		// 1. project : root
		// 2. items
		// 2-1. file
		// 2-2. category
		//   3. note
		//   3-1.   description
		//   3-2.   reference
		
		if(qName.equals("project")){
			if(!(position.peek() == ELEM_PROJECT)){
				errors.add(new SAXParseException(ERROR_WRONGPROJTER, null));
			}
		}else if(qName.equals("items")){
			if(!(position.peek() == ELEM_ITEMS)){
				errors.add(new SAXParseException(ERROR_WRONGITMSTER, null));
			}
		}else if(qName.equals("file")){
			if(!(position.peek() == ELEM_FILE)){
				errors.add(new SAXParseException(ERROR_WRONGFILETER, null));
			}
		}else if(qName.equals("category")){
			if(!(position.peek() == ELEM_CATEGORY)){
				errors.add(new SAXParseException(ERROR_WRONGCTGRTER, null));
			}
			focus = (ProjectItem)focus.getParent();
		}else if(qName.equals("note")){
			int itm = position.get(position.size() - 2);
			if((position.peek() != ELEM_NOTE) || !(itm == ELEM_CATEGORY
							|| itm == ELEM_PROJECT || itm == ELEM_FILE)){
				errors.add(new SAXParseException(ERROR_WRONGNOTETER, null));
			}
			focus.setNote(this.focusNote);
		}else if(qName.equals("description")){
			int itm = position.get(position.size() - 2);
			if((position.peek() != ELEM_DESCRIPTION)
					|| !(itm == ELEM_NOTE)){
				errors.add(new SAXParseException(ERROR_WRONGDESCTER, null));
			}
		}else if(qName.equals("reference")){
			int itm = position.get(position.size() - 2);
			if((position.peek() != ELEM_REFERENCE) || !(itm == ELEM_NOTE)){
				errors.add(new SAXParseException(ERROR_WRONGREFTER, null));
			}
		}


		if(position.size() == 0){
			errors.add(new SAXParseException(ERROR_NOTDECLARED + qName, null));
		}else
			position.pop();
		//System.out.println("PXH : end : " + qName + " , focus : " + focus);
	}// end endElement(String, String, String)
	public void characters(char[] chrs, int start, int end){
		String cnt = new String(chrs, start, end);
		if(position.size() == 0) return;
		else if(position.peek() == ELEM_DESCRIPTION)
			focusNote.setDescription(cnt);
		else if(position.peek() == ELEM_REFERENCE)
			focusNote.addReference(cnt);
	}// end characters(char[], int, int)
	
	
	
	
	
	

	public ArrayList<SAXParseException> getErrors()
	{ return errors; }
	public ArrayList<SAXParseException> getFatalErrors()
	{ return fatalErrors; }
	
	public Project getProduct()
	{ return project; }
}
