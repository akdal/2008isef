package agdr.robodari.store;

import java.util.Vector;
import agdr.robodari.comp.RobotComponent;

public class RobotComponentStore {
	private final static Vector<Class<? extends RobotComponent>>
			components = new Vector<Class<? extends RobotComponent>>();
	
	
	public final static Class[] getComponents()
	{ return components.toArray(new Class[]{}); }
	public final static boolean contains(Class<? extends RobotComponent> comp){
		return components.contains(comp);
	}// end contains(Language)
	public final static void registerComponent(Class<? extends RobotComponent> comp){
		if(!contains(comp)){
			components.add(comp);
			System.out.println("register! : " + comp.getCanonicalName()); 
		}
	}
}
