package agdr.robodari.store;

import java.util.*;
import agdr.robodari.comp.*;
import agdr.robodari.comp.info.*;

public class RobotInfoStore {
	private static Hashtable<Class<? extends RobotComponent>, InfoGenerator>
			infoTable = 
			new Hashtable<Class<? extends RobotComponent>, InfoGenerator>();
	
	public static void registerInfo(Class<? extends RobotComponent> rc,
			InfoGenerator info){
		infoTable.put(rc, info);
	}// end registerInbfo(Class<? extends RobotCompoennt>, InfoGenerator)
	
	public static Info getInfo(RobotComponent rc){
		if(rc == null) return null;
		return infoTable.get(rc.getClass()).generateInfo(rc);
	}// end getInfo(RobotComponent)
}
