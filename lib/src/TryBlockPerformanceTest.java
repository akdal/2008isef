
public class TryBlockPerformanceTest {
	public static void main(String[] argv){
		// for pre-loading
		for(int i = 0; i < 10; i++){
			try{
				testMethod1();
				testMethod2();
			}catch(Exception e){}
		}
		
		long start = System.currentTimeMillis();
		for(int i = 0; i < 1000000; i++){
			testMethod1();
		}
		long end = System.currentTimeMillis();
		System.out.printf("no try-catch : %d\n", end - start);
		
		start = System.currentTimeMillis();
		for(int i = 0; i < 1000000; i++){
			try{
				testMethod2();
			}catch(Exception e){}
		}
		end = System.currentTimeMillis();
		System.out.printf("with try-catch : %d\n", end - start);
	}// end main
	
	static void testMethod1(){
		double a = Math.toRadians(Math.sqrt(2147483647));
		a = Math.toRadians(Math.sqrt(2147483647));
		a = Math.toRadians(Math.sqrt(2147483647));
		a = Math.toRadians(Math.sqrt(2147483647));
		a = Math.toRadians(Math.sqrt(2147483647));
		a = Math.toRadians(Math.sqrt(2147483647));
		a = Math.toRadians(Math.sqrt(2147483647));
		a = Math.toRadians(Math.sqrt(2147483647));
	}
	static void testMethod2() throws Exception{
		double a = Math.toRadians(Math.sqrt(2147483647));
		a = Math.toRadians(Math.sqrt(2147483647));
		a = Math.toRadians(Math.sqrt(2147483647));
		a = Math.toRadians(Math.sqrt(2147483647));
		a = Math.toRadians(Math.sqrt(2147483647));
		a = Math.toRadians(Math.sqrt(2147483647));
		a = Math.toRadians(Math.sqrt(2147483647));
		a = Math.toRadians(Math.sqrt(2147483647));
	}// end testMethod2()
}
