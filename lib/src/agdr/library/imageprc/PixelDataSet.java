package agdr.library.imageprc;

import java.awt.*;
import java.awt.image.*;

public class PixelDataSet {
	public float[][][] pixelHSBData;
	public float[][][][] pHSBGrd;
	
	
	public PixelDataSet(BufferedImage source)
	{ this.resetData(source); }
	
	public void resetData(BufferedImage source){
		int width = source.getWidth(), height = source.getHeight();
		int rgbval = 0, r = 0, g = 0, b = 0;
		pixelHSBData = new float[height][width][3];
		
		for(int i = 0; i < height; i++){
			for(int j = 0; j < width; j++){
				rgbval = source.getRGB(i, j);
				r = (rgbval >> 16) & 0xFF;
				g = (rgbval >> 8) & 0xFF;
				b = (rgbval >> 0) & 0xFF;
				Color.RGBtoHSB(r, g, b, pixelHSBData[i][j]);
			}
		}
		calcGradient();
		calcLength();
	}// end resetData(BufferedImage)
	
	
	private void calcGradient(){
		/*
		 * ��f R, G, B�� H, S, B������ x, y ���⿡ ���� ��ȭ��; ���Ѵ�.
		 */
		int height = pixelHSBData.length;
		int width = pixelHSBData[0].length;
		
		pHSBGrd = new float[height][width][2][4];
		/*
		 * �ܼ��� �� �ȼ����� ��� �ȼ��� ��; ���� �ͺ��� { �� gradient��
		 * 		 �ǹ̸� �츱 �� �ִ� ���: ��;��?
		 */
		for(int i = 0; i < height - 1; i++){
			for(int j = 0; j < width - 1; j++){
				pHSBGrd[i][j][0][0] = pixelHSBData[i + 1][j][0] - pixelHSBData[i][j][0];
				pHSBGrd[i][j][0][1] = pixelHSBData[i + 1][j][1] - pixelHSBData[i][j][1];
				pHSBGrd[i][j][0][2] = pixelHSBData[i + 1][j][2] - pixelHSBData[i][j][2];

				pHSBGrd[i][j][1][0] = pixelHSBData[i][j + 1][0] - pixelHSBData[i][j][0];
				pHSBGrd[i][j][1][1] = pixelHSBData[i][j + 1][1] - pixelHSBData[i][j][1];
				pHSBGrd[i][j][1][2] = pixelHSBData[i][j + 1][2] - pixelHSBData[i][j][2];
			}
		}
	}// end calcGradient()
	private void calcLength(){
		
	}// end calcLength()
}
