package agdr.library.imageprc.run;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class HSBColorViewer {
	public final static Dimension SIZE = new Dimension(350, 400);
	
	public static void main(String[] argv){
		JFrame jf = new JFrame("Object Extract Runner");
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		final HSBPaintPanel hpp = new HSBPaintPanel();

		final JLabel valLabel = new JLabel("1");
		final JSlider valAdjSlider = new JSlider();
		valAdjSlider.setMaximum(HSBPaintPanel.MAX);
		valAdjSlider.setValue(HSBPaintPanel.MAX);
		valAdjSlider.setMinimum(0);
		
		valAdjSlider.addChangeListener(new ChangeListener(){
			public void stateChanged(ChangeEvent ce){
				if(valAdjSlider.getValueIsAdjusting())
					return;
				
				float sldval = (float)valAdjSlider.getValue() / (float)HSBPaintPanel.MAX;
				hpp.setValue(sldval);
				valLabel.setText("(" + sldval + ")");
				hpp.repaint();
			}// end stateChanged(ChangeEvnt)
		});
		final DefaultComboBoxModel typeModel =
			new DefaultComboBoxModel(
					new Object[]{ "hue", "saturation", "brightness"});
		final JComboBox typeComboBox = new JComboBox(typeModel);
		typeComboBox.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent ie){
				hpp.setType(typeComboBox.getSelectedIndex());
				hpp.setValue(1);
				valLabel.setText("(1)");
				hpp.repaint();
			}// end itemStateChanged
		});
		JPanel northPanel = new JPanel();
		northPanel.setLayout(new BorderLayout());
		northPanel.add(valAdjSlider, BorderLayout.CENTER);
		northPanel.add(valLabel, BorderLayout.SOUTH);
		
		jf.setSize(SIZE);
		jf.setLayout(new BorderLayout());
		jf.add(hpp, BorderLayout.CENTER);
		jf.add(typeComboBox, BorderLayout.SOUTH);
		jf.add(northPanel, BorderLayout.NORTH);
		jf.setVisible(true);
	}// end main(String[])
	
	static class HSBPaintPanel extends JPanel implements Runnable{
		private float value = 0;
		private int type;
		public final static int MAX = 300;
		
		private boolean pauseFlag = true;
		
		public HSBPaintPanel(){
			super.setPreferredSize(new Dimension(MAX, MAX));
			Thread thr = new Thread(this);
			thr.start();
		}// end Constructor()
		
		public void setValue(float v)
		{ this.value = v; }
		public void setType(int t)
		{ this.type = t; }
		public int getType()
		{ return type; }
		
		public void paint(Graphics g){
			super.paint(g);
			pauseFlag = false;
		}// end paint(Graphics)
		
		public void run(){
			while(true){
				while(pauseFlag){
					try{
						Thread.sleep(100);
					}catch(Exception excp)
					{ excp.printStackTrace(); System.exit(-1); }
				}
				pauseFlag = true;
				Graphics g = super.getGraphics();
				if(g == null){
					continue;
				}
				for(int i = 0; i <= MAX; i++){
					for(int j = 0; j <= MAX; j++){
						int val = 0;
						if(type == 0)
							val = Color.HSBtoRGB(value, (float)i / (float)MAX,
								(float)j / (float)MAX);
						else if(type == 1)
							val = Color.HSBtoRGB((float)i / (float)MAX, value, 
								(float)j / (float)MAX);
						else if(type == 2)
							val = Color.HSBtoRGB((float)i / (float)MAX,
								(float)j / (float)MAX, value);
						g.setColor(new Color(val));
						g.drawLine(i, j, i, j);
					}
				}
			}
		}// end run()
	}// end class HSBPaintPanel
}
