package agdr.library.lang;

public class CodeRangeImpl implements CodeRange{
	public final static CodeRange NULL = new CodeRangeImpl(-1, -1);
	
	public final int start;
	public final int length;
	
	public CodeRangeImpl(int start, int len)
	{ this.start = start; this.length = len; }
	
	public int getStartPosition()
	{ return start; }
	public int getLength()
	{ return length; }
	
	public String toString()
	{ return start + "-" + (start + length); }
	
	public boolean equals(Object obj){
		if(obj == null) return false;
		else if(!(obj instanceof CodeRange))
			return false;
		CodeRange range = (CodeRange)obj;
		return range.getStartPosition() == start && range.getLength() == length;
	}// end equalS(Object)
}
