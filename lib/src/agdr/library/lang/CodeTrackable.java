package agdr.library.lang;

public interface CodeTrackable {
	public CodeRange getCodeRange();
}
