package agdr.library.lang;

public interface Console {
	public java.io.InputStream getIn();
	public java.io.OutputStream getOut();
	public java.io.Writer getLog();
}
