package agdr.library.lang;

import java.util.*;
import java.beans.*;

public class ErrorBundle implements java.io.Serializable{
	private final static long serialVersionUID = 10987654321L;
	
	public final static ErrorBundle NULL = new ErrorBundle(){
		private final static long serialVersionUID = 10987654321L;
		public void addErrorMessage(ErrorMessage em){}
		public boolean containsErrorMessage(ErrorMessage em)
		{ return false; }
		public boolean containsErrorMessage(Object source, Object item, Object msg)
		{ return false; }
	};
	
	
	private ArrayList<ErrorMessage> msgs = 
			new ArrayList<ErrorMessage>();
	
	public ErrorBundle()
	{ super(); }

	public void addErrorMessage(ErrorMessage em){
		if(em == null)
			return;
		msgs.add(em);
	}// end addErrorMessage(ErrorMessage)
	
	
	
	public boolean containsGrammarErrorMessage(CodeRange range, Object errcnt){
		for(ErrorMessage eachmsg : msgs){
			if(!(eachmsg instanceof GrammarErrorMessage)) continue;
			
			GrammarErrorMessage gem = (GrammarErrorMessage)eachmsg;
			if(gem.equals(range, errcnt)) return true;
		}
		return false;
	}// end getGrammarErrorMessage
	

	public void removeErrorMessage(ErrorMessage em){
		msgs.remove(em);
	}// end removeErrorMessage(ErrorMessage)


	
	public boolean removeErrorMessage(Object source, Object item){
		boolean chged = false;
		for(ErrorMessage msg : msgs)
			if(msg.equals(source, item, msg.getMessage(),
						msg.getErrorType())){
				msgs.remove(msg);
				chged = true;
			}
		return chged;
	}// end removeErrorMessage(Object, Object)

	public boolean removeErrorMessage(Object source, Object item, Object contents){
		boolean chged = false;
		for(ErrorMessage msg : msgs)
			if(msg.equals(source, item, contents,
						msg.getErrorType())){
				msgs.remove(msg);
				chged = true;
			}
		return chged;
	}// end removeErrorMessage(Object, Object)
	
	
	
	
	public boolean removeGrammarErrorMessage(CodeRange range, Object errcnt){
		boolean chged = false;
		for(int i = 0; i < msgs.size(); i++){
			ErrorMessage eachmsg = msgs.get(i);
			if(!(eachmsg instanceof GrammarErrorMessage)) continue;
			
			GrammarErrorMessage gem = (GrammarErrorMessage)eachmsg;
			if(gem.equals(range, errcnt)){
				msgs.remove(eachmsg);
				chged = true;
			}
		}
		return chged;
	}// end getGrammarErrorMessage

	
	public boolean removeErrorMessage(Object source){
		boolean chged = false;
		for(ErrorMessage msg : msgs)
			if(msg.equals(source, msg.getItem(), msg.getMessage(),
					msg.getErrorType())){
				msgs.remove(msg);
				chged = true;
			}
		return chged;
	}// end removeErrorMessage(Object, Object)
	
	public boolean removeGrammarError(CodeRange range){
		boolean change = false;
		
		for(int i = 0; i < msgs.size(); i++){
			ErrorMessage eachmsg = msgs.get(i);
			if(!(eachmsg instanceof GrammarErrorMessage))
				continue;
			
			GrammarErrorMessage pmsg = (GrammarErrorMessage)eachmsg;
			if(pmsg.getCodeRange().equals(range))
				msgs.remove(pmsg);
			change = true;
		}
		return change;
	}// end removeGrammarError(CodeRange)
	
	
	public boolean containsErrorMessage(ErrorMessage em)
	{ return msgs.contains(em); }
	public boolean containsErrorMessage(Object source){
		for(int i = 0; i < msgs.size(); i++){
			ErrorMessage eachmsg = msgs.get(i);
			if(eachmsg.equals(source, eachmsg.getItem(), eachmsg.getMessage(),
					eachmsg.getErrorType()))
				return true;
		}
		return false;
	}// end containsErrorMessage(Object)
	public boolean containsErrorMessage(Object source, Object item, Object message){
		for(int i = 0; i < msgs.size(); i++){
			ErrorMessage eachmsg = msgs.get(i);
			if(eachmsg.equals(source, item, message, eachmsg.getErrorType()))
				return true;
		}
		return false;
	}// end containsErrorMessage(Object)
	
	public ErrorMessage[] getErrorMessages()
	{ return msgs.toArray(new ErrorMessage[]{}); }
	public int getSizeOfErrorMessages()
	{ return msgs.size(); }
	
	public void clear()
	{ msgs.clear(); }
	public Object clone(){
		ErrorBundle eb = new ErrorBundle();
		eb.msgs = new ArrayList(this.msgs);
		return eb;
	}// end clone()
}
