package agdr.library.lang;

public class ErrorMessage implements java.io.Serializable{
	private final static long serialVersionUID = 12345678910L;
	
	private Object msg;
	private Object source;
	private Object item;
	private CodeRange codeRange;
	private ErrorTypes errorType;
	
	
	public ErrorMessage(Object source, Object item, CodeRange range,
			Object message,
			ErrorTypes errorType){
		this.msg = message;
		this.item = item;
		this.codeRange = range;
		this.source = source;
		this.errorType = errorType;
	}// end Constructor(Object, Object, Object, Object)
	
	
	public CodeRange getCodeRange()
	{ return codeRange; }
	public void setCodeRange(CodeRange cr)
	{ this.codeRange = cr; }
	
	public Object getMessage()
	{ return msg; }
	public void setMessage(Object m)
	{ this.msg = m; }
	
	public Object getSource()
	{ return source; }
	public void setSource(Object s)
	{ this.source = s; }
	
	public Object getItem()
	{ return item; }
	public void setItem(Object o)
	{ this.item = o; }
	
	public ErrorTypes getErrorType()
	{ return errorType; }
	public void setErrorType(ErrorTypes et)
	{ this.errorType = et; }
	
	
	public void set(Object src, Object itm, Object msg, ErrorTypes errtype){
		this.source = src;
		this.item = itm;
		this.msg = msg;
		this.errorType = errtype;
	}// end set
	
	
	public String toString(){
		StringBuffer buf = new StringBuffer();
		
		if(item != null)
			buf.append(" [" + item + "]");
		if(codeRange != null)
			buf.append(codeRange);
		buf.append(" : ");
		buf.append(msg);
		return buf.toString();
	}// end toString()
	
	public boolean equals(Object source, Object item, Object msg,
			ErrorTypes errorType){
		if(this.source != source) return false;
		else if(this.msg != msg) return false;
		else if(this.item != item) return false;
		else if(this.errorType != errorType) return false;
		
		if(this.source != null && !this.source.equals(source))
			return false;
		else if(this.item != null && !this.item.equals(item))
			return false;
		else if(this.msg != null && !this.msg.equals(msg))
			return false;
		else if(this.errorType != null && !this.errorType.equals(errorType))
			return false;
		return true;
	}// end equals(OBject, Object, Object, ErrorTypes)
	
	public boolean equals(Object obj){
		if(obj == this) return true;
		else if(obj == null) return false;
		else if(!(obj instanceof ErrorMessage))
			return false;
		
		ErrorMessage em = (ErrorMessage)obj;
		return equals(em.source, em.item, em.msg, em.errorType);
	}// end equals(Object)
}
