package agdr.library.lang;

public enum ErrorTypes {
	WARNING("warning"),
	GRAMMAR_ERROR("grammar error"),
	CNT_ERROR("content error"),
	RUNTIME_ERROR("runtimeError");
	
	
	final String cnt;
	ErrorTypes(String cnt)
	{ this.cnt = cnt; }
	
	public String toString()
	{ return cnt; }
}
