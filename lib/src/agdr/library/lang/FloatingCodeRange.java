package agdr.library.lang;

public class FloatingCodeRange implements CodeRange{
	private int start;
	private int length;
	
	public FloatingCodeRange(int start, int length){
		this.start = start;
		this.length = length;
	}// length cOnstructor
	
	public int getStartPosition()
	{ return start; }
	public int getLength()
	{ return length; }
	
	public void setStartPosition(int startp)
	{ this.start = startp; }
	public void setLength(int len)
	{ this.length = len; }
}
