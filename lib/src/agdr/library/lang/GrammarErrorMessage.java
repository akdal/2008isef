package agdr.library.lang;

public class GrammarErrorMessage extends ErrorMessage{
	public GrammarErrorMessage(CodeRange range, String errcnt)
	{ super(null, null, range, errcnt, ErrorTypes.GRAMMAR_ERROR); }
	
	public boolean equals(CodeRange range, Object errcnt){
		return super.equals(super.getSource(), range, errcnt, super.getErrorType());
	}// end equals(CodeRange, Object)
}
