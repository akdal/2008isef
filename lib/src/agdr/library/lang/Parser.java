package agdr.library.lang;

import java.io.*;

public interface Parser<E> extends Serializable{
	@NeedSynchronization
	public E parse(Reader reader, ErrorBundle bundle)
			throws Exception;
	
	public WorkingState getState();

	public Parser newInstance();
	
	public ParsingListener getListener(int idx);
	public boolean containsListener(ParsingListener pl);
	public int getListenersCount();
	public ParsingListener[] getListeners();
	public void addParsingListener(ParsingListener pl);
	public void removeParsingListener(ParsingListener pl);
	
	public String getParserName();
}
