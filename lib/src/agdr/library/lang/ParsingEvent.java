package agdr.library.lang;

import java.io.*;

public class ParsingEvent implements java.io.Serializable{
	private Parser parser;
	private Reader reader;
	
	public ParsingEvent(Parser parser, Reader reader){
		this.parser = parser;
		this.reader = reader;
	}// end Constructor(Parser, Reader, Object, int, int)
	
	public Parser getParser(){ return parser; }
	public Reader getReader(){ return reader; }
}
