package agdr.library.lang;

public interface ParsingListener {
	public void parsingStart(ParsingEvent pe);
	public void parsingEnd(ParsingEvent pe);
}
