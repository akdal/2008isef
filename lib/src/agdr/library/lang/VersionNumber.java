package agdr.library.lang;

/**
 * 버젼에 대한 정보를 담는 클래스입니다.<br>
 * 예를 들어 어떤 버젼이 '1.3.3.7' 일 때,<br>
 * version1 = 1,<br>
 * version2 = 3,<br>
 * version3 = 3,<br>
 * version4 = 7<br>
 * 이 됩니다.
 * @author 이준영
 *
 */
public final class VersionNumber implements java.io.Serializable{
	public final int version1, version2, version3, version4, build;
	public VersionNumber(int version1, int version2, int version3,
			int version4, int build){
		this.version1 = version1;
		this.version2 = version2;
		this.version3 = version3;
		this.version4 = version4;
		this.build = build;
	}// end Constructor(int, int, int, int)
	public VersionNumber(String vn) throws Exception{
		int start = 0;
		int end = vn.indexOf(".");
		version1 = Integer.parseInt(vn.substring(start, end));

		start = end + 1;
		end = vn.indexOf(".", start);
		version2 = Integer.parseInt(vn.substring(start, end));
		
		start = end + 1;
		end = vn.indexOf(".", start);
		version3 = Integer.parseInt(vn.substring(start, end));

		start = end + 1;
		end = vn.indexOf(" #", start);
		version4 = Integer.parseInt(vn.substring(start, end));
		
		start = end + 2;
		build = Integer.parseInt(vn.substring(start, vn.length()));
	}// end Constructor(String)
	
	
	/**
	 * 주어진 인자가 이 버젼보다 더 높은 버젼인지를 반환합니다.
	 * @param vn
	 * @return
	 */
	public boolean isHigherVersion(VersionNumber vn){
		if(vn.version1 > version1) return true;
		if(vn.version2 > version2) return true;
		if(vn.version3 > version3) return true;
		if(vn.version4 > version4) return true;
		if(vn.build > build) return true;
		return false;
	}// end isHigherVersion(VersionNumber)
	
	public String toString()
	{ return version1 + "." + version2 + "." + version3 + "." + version4
		+ " #" + build; }
	
	public boolean equals(int version1, int version2, int version3,
			int version4, int build){
		return this.version1 == version1 && 
				this.version2 == version2 && 
				this.version3 == version3 && 
				this.version4 == version4 && 
				this.build == build;
	}// end equals(int, int, int, int)
	public boolean equals(Object obj){
		if(obj == this) return true;
		else if(obj == null) return false;
		else if(obj instanceof VersionNumber){
			VersionNumber vn = (VersionNumber)obj;
			return equals(vn.version1, vn.version2, vn.version3,
					vn.version4, vn.build);
		}
		return false;
	}// end equals(Object)
}// end class VersionNumber