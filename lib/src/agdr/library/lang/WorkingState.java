package agdr.library.lang;

public enum WorkingState {
	WORKING, PREPARING, FINISHING, UNEMPLOYED
}
