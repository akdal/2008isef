package agdr.library.math;


public interface ArgList{
	public int argCount();
	public String argName(int idx);
	public Object argValue(int idx);
}
