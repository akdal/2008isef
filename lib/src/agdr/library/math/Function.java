package agdr.library.math;

//import java.io.*;

public interface Function<A extends ArgList, B>{
	public boolean domain (A a);
	public boolean range (B b);
	
	public B f (A a);
}
