package agdr.library.math;

import java.awt.Point;
import java.awt.Polygon;

import javax.vecmath.*;

import agdr.library.math.BinaryTree.Node;
import static java.lang.Math.*;

/**
 * Class for Advanced Math.<br>
 * 
 * List : <br>
 * Inclusion, Intersection by basic algorithm<br>
 * LCP solver<br>
 * Scalar Operations<br>
 * Vector Operations<br>
 * Matrix Operations<br>
 * Inertia Tensor<br>
 * Trim<br>
 * Normal Generation, Triangulation, Plane inspection<br>
 * Volume, Center mass<br>
 * Convex Hull(2D), Polygon Expansion(2D)<br>
 */
public class MathUtility{
	public final static float FEPSILON = 4.0E-7f;
	public final static double DEPSILON = 1.0E-16;
	
	public final static int LCP_CANNOT_REMOVE_COMPLEMENTARY = 0;
	public final static int LCP_FOUND_SOLUTION = 1;
	public final static int LCP_EXCEEDED_MAX_RETRIES = 2;
	public final static int LCP_FOUND_TRIVIAL_SOLUTION = 3;
	

	//final static Projector DEF_PROJECTOR = new Projector1();
	
	
	final static double SIN30 = Math.sin(Math.PI / 6.0);
	final static double COS30 = Math.cos(Math.PI / 6.0);
	final static double SIN45 = Math.sin(Math.PI / 4.0);
	final static double COS45 = Math.cos(Math.PI / 4.0);
	final static double SQRT3 = Math.sqrt(3);
	
	
	
	

	//public static Projector getDefaultProjector()
	//{ return DEF_PROJECTOR; }
	
	
	
	
	
	/* ********************************************
	 * 			INTERSECTION by basic algorithm
	 * ******************************************/
	private strictfp static boolean _intersects_bsinspc(
			float nx, float ny, float nz,
			float a1x, float a1y, float a1z,
			float b1x, float b1y, float b1z,
			float b2x, float b2y, float b2z){
		
		float c1x = b1x - a1x;
		float c1y = b1y - a1y;
		float c1z = b1z - a1z;
		float c2x = b2x - a1x;
		float c2y = b2y - a1y;
		float c2z = b2z - a1z;
		
		float innerprd1 = c1x * nx + c1y * ny + c1z * nz;
		float innerprd2 = c2x * nx + c2y * ny + c2z * nz;
		if(innerprd1 * innerprd2 > 0)
			return false;
		
		return true;
	}// end _intersects_bsinspc
	private strictfp static boolean _intersects_inspc2(
			float aix, float aiy, float aiz,
			float ai_1x, float ai_1y, float ai_1z,
			float tx, float ty, float tz,
			float nx, float ny, float nz, 
			float b1x, float b1y, float b1z,
			float b2x, float b2y, float b2z){
		float sgnval = tx * nx + ty * ny + tz * nz;
		
		float dx = aix - ai_1x;
		float dy = aiy - ai_1y;
		float dz = aiz - ai_1z;
		float ex = -(b1x - aix);
		float ey = -(b1y - aiy);
		float ez = -(b1z - aiz);
		
		float fx = dy * tz - dz * ty;
		float fy = dz * tx - dx * tz;
		float fz = dx * ty - dy * tx;
		
		if(sgnval * (fx * ex + fy * ey + fz * ez) > 0)
			return false;
		return true;
	}// end _intersects_inspc2
	public strictfp static boolean intersects(Point3f a1, Point3f a2, Point3f a3,
			Point3f b1, Point3f b2){
		float nx = a1.y * a2.z - a1.z * a2.y;
		float ny = a1.z * a2.x - a1.x * a2.z;
		float nz = a1.x * a2.y - a1.y * a2.x;
		if(!_intersects_bsinspc(
				nx, ny, nz,
				a1.x, a1.y, a1.z,
				b1.x, b1.y, b1.z,
				b2.x, b2.y, b2.z))
			return false;
		
		float tx = b1.x - b2.x;
		float ty = b1.y - b2.y;
		float tz = b1.z - b2.z;
		
		float dx, dy, dz;
		float ex, ey, ez;
		if(!_intersects_inspc2(
				a1.x, a1.y, a1.z,
				a2.x, a2.y, a2.z,
				tx, ty, tz, 
				nx, ny, nz,
				b1.x, b1.y, b1.z,
				b2.x, b2.y, b2.x)){
			return false;
		}else if(!_intersects_inspc2(
				a2.x, a2.y, a2.z,
				a3.x, a3.y, a3.z,
				tx, ty, tz, 
				nx, ny, nz,
				b1.x, b1.y, b1.z,
				b2.x, b2.y, b2.x))
			return false;
		else if(!_intersects_inspc2(
				a3.x, a3.y, a3.z,
				a1.x, a1.y, a1.z,
				tx, ty, tz, 
				nx, ny, nz,
				b1.x, b1.y, b1.z,
				b2.x, b2.y, b2.x))
			return false;
		
		return true;
	}// end intersects(Point3f, Point3f, Point3f, Point3f, Point3f)
	public strictfp static boolean intersects(Point3f[] plane, Point3f b1,
			Point3f b2){
		if(plane.length <= 2)
			// ϴ:
			return false;

		float nx = plane[0].y * plane[1].z - plane[0].z * plane[1].y;
		float ny = plane[0].z * plane[1].x - plane[0].x * plane[1].z;
		float nz = plane[0].x * plane[1].y - plane[0].y * plane[1].x;
		
		if(!_intersects_bsinspc(
				nx, ny, nz,
				plane[0].x, plane[0].y, plane[0].z,
				b1.x, b1.y, b1.z,
				b2.x, b2.y, b2.z))
			return false;
		
		float tx = b1.x - b2.x;
		float ty = b1.y - b2.y;
		float tz = b1.z - b2.z;
		
		//float dx, dy, dz;
		//float ex, ey, ez;
		for(int i = 1; i < plane.length; i++){
			if(!_intersects_inspc2(
					plane[i].x, plane[i].y, plane[i].z,
					plane[i - 1].x, plane[i - 1].y, plane[i - 1].z,
					tx, ty, tz, 
					nx, ny, nz,
					b1.x, b1.y, b1.z,
					b2.x, b2.y, b2.x))
				return false;
		}
		int i = plane.length;
		if(!_intersects_inspc2(
				plane[i - 1].x, plane[i - 1].y, plane[i - 1].z,
				plane[0].x, plane[0].y, plane[0].z,
				tx, ty, tz, 
				nx, ny, nz,
				b1.x, b1.y, b1.z,
				b2.x, b2.y, b2.x))
			return false;
		
		return true;
	}// end intersects(Point3f[], Point3f, Point3f)
	private strictfp static boolean _intersects(Point3f[][] vertices1,
			Point3f[][] vertices2){
		for(int i = 0; i < vertices1.length; i++){
			for(int j = 0; j < vertices2.length; j++){
				for(int k = 1; k < vertices2[j].length; k++)
					if(intersects(vertices1[i], vertices2[j][k - 1], vertices2[j][k]))
						return true;
				if(intersects(vertices1[i], vertices2[j][0],
						vertices2[j][vertices2[j].length - 1]))
					return true;
			}
		}
		return false;
	}// end _intersects(Point3f[][], Point3f[][])
	public strictfp static boolean intersects(Point3f[] points1,
			int[][] faces1, Point3f[] points2, int[][] faces2){
		Point3f[][] vertices1 = new Point3f[faces1.length][];
		Point3f[][] vertices2 = new Point3f[faces2.length][];

		for(int i = 0; i < vertices1.length; i++){
			vertices1[i] = new Point3f[faces1[i].length];
			for(int j = 0; j < faces1[i].length; j++)
				vertices1[i][j] = points1[faces1[i][j]];
		}
		for(int i = 0; i < vertices2.length; i++){
			vertices2[i] = new Point3f[faces2[i].length];
			for(int j = 0; j < faces2[i].length; j++)
				vertices2[i][j] = points2[faces2[i][j]];
		}
		
		return _intersects(vertices1, vertices2);
	}// end intersects(Point3f[], int[][], Point3f[], int[][])
	
	
	
	public strictfp static boolean fastContains(float px, float py, float pz, Point3f[] vertices, int[] faces){
		float[] array = new float[faces.length * 3];
		for(int i = 0; i < faces.length; i++){
			array[i * 3] = vertices[faces[i]].x;
			array[i * 3 + 1] = vertices[faces[i]].y;
			array[i * 3 + 2] = vertices[faces[i]].z;
		}
		Vector3f n = MathUtility.generateNormal(array, null);
		return fastContains(px, py, pz, array, n.x, n.y, n.z);
	}// end contains(float, float, float)
	public strictfp static boolean fastContains(
				float px, float py, float pz,
				float[] vertices,
				float nx, float ny, float nz){
		if(vertices.length == 0)
			return false;
		
		float vpx = vertices[0] - px,
				vpy = vertices[1] - py,
				vpz = vertices[2] - pz;
		//float vplen = Math.sqrt(vpx * vpx + vpy * vpy + vpz * vpz);
		if(Math.abs(vpx * nx + vpy * ny + vpz * nz) > 0.000001)
			// not on the plane
			return false;
		
		int len = vertices.length / 3;
		int a = 0, b = 0;
		int X = 0, Y = 1, Z = 2;
		for(int i = 1; i < len + 1; i++){
			a = (i - 1) * 3;
			b = (i % len) * 3;
			// (a - p) X (b - a) = 
			// (a - p)y * (b - a)z - (a - p)y * (b - a)z
			//-(a - p)x * (b - a)z + (a - p)z * (b - a)x
			// (a - p)x * (b - a)y + (a - p)y * (b - a)x
			if(
					nx * ((vertices[a+Y] - py) * (vertices[b+Z] - vertices[a+Z]) - (vertices[a+Y] - py) * (vertices[b+Z] - vertices[a+Z])) -
					ny * ((vertices[a+X] - px) * (vertices[b+Z] - vertices[a+Z]) - (vertices[a+Z] - pz) * (vertices[b+X] - vertices[a+X])) + 
					nz * ((vertices[a+X] - px) * (vertices[b+Y] - vertices[a+Y]) - (vertices[a+Y] - py) * (vertices[b+X] - vertices[a+X]))
					< 0)
				return false;
		}
		return true;
	}// end fastContains()
	public strictfp static boolean fastContains(
			double px, double py, double pz,
			double[] vertices,
			double nx, double ny, double nz){
		if(vertices.length == 0)
			return false;
		
		double vpx = vertices[0] - px,
				vpy = vertices[1] - py,
				vpz = vertices[2] - pz;
		//float vplen = Math.sqrt(vpx * vpx + vpy * vpy + vpz * vpz);
		if(Math.abs(vpx * nx + vpy * ny + vpz * nz) > 0.000001)
			// not on the plane
			return false;
		
		int len = vertices.length / 3;
		int a = 0, b = 0;
		int X = 0, Y = 1, Z = 2;
		for(int i = 1; i < len + 1; i++){
			a = (i - 1) * 3;
			b = (i % len) * 3;
			// (a - p) X (b - a) = 
			// (a - p)y * (b - a)z - (a - p)y * (b - a)z
			//-(a - p)x * (b - a)z + (a - p)z * (b - a)x
			// (a - p)x * (b - a)y + (a - p)y * (b - a)x
			if(
					nx * ((vertices[a+Y] - py) * (vertices[b+Z] - vertices[a+Z]) - (vertices[a+Y] - py) * (vertices[b+Z] - vertices[a+Z])) -
					ny * ((vertices[a+X] - px) * (vertices[b+Z] - vertices[a+Z]) - (vertices[a+Z] - pz) * (vertices[b+X] - vertices[a+X])) + 
					nz * ((vertices[a+X] - px) * (vertices[b+Y] - vertices[a+Y]) - (vertices[a+Y] - py) * (vertices[b+X] - vertices[a+X]))
					< 0)
				return false;
		}
		return true;
	}// end fastContains()
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/* ************************************************
	 *               LCPSolver
	 ************************************************/
	
	private final static strictfp boolean _LCP_initialize(
			char[] eqs_var, int[] eqs_varIdx,
			double[][] eqs_C, double[][] eqs_W, double[][] eqs_Z, 
			double[][] M, double[] Q){
		// This source is from Game Physics Bible CD. WmlLCPSolver
	    int iNumEqP1 = eqs_var.length + 1;
	    for(int i = 0; i < eqs_var.length; i++){
	        // initially w's are basic, z's are non-basic
	        eqs_var[i] = 'w';
	        eqs_varIdx[i] = i+1;
	        
	    	// w indices run from 1 to iNumEquations
	    	
	    	// Every equation includes z0.
	        eqs_Z[i][0] = 1.0;
	        eqs_C[i][i+1] = 1.0;
	    }
	    // Check if all the constant terms are nonnegative.  If so, the solution
	    // is z = 0 and w = constant_terms.  The caller will set the values of z
	    // and w, so just return from here.
	    double dConstTermMin = 0.0;
	    for(int i = 0; i < eqs_var.length; i++){
	        eqs_C[i][0] = Q[i];
	        if (Q[i] < dConstTermMin)
	            dConstTermMin = Q[i];
	    }
	    if(dConstTermMin >= 0.0)
	    	return false;

	    // enter Z terms
	    for(int i = 0; i < eqs_var.length; i++){
	        // set m_adEq.Z[0] to 0.0 for any row in which all m_aadM are 0.0.
	        double dRowOfZeros = 0.0;
	        for(int j = 0; j < eqs_var.length; j++){
	            double dTemp = M[i][j];
	            eqs_Z[i][j+1] = dTemp;
	            if (dTemp != 0.0)
	                dRowOfZeros = 1.0;
	        }
	        eqs_Z[i][0] *= dRowOfZeros;
	    }
	    
	    for(int i = 0; i < eqs_var.length; i++){
	        // Find the max abs value of the coefficients on each row and divide
	        // each row by that max abs value.
	        double dMaxAbs = 0.0f;
            for(int j = 0; j < eqs_C[i].length; j++){
            	if(dMaxAbs < Math.abs(eqs_C[i][j]))
            		dMaxAbs = Math.abs(eqs_C[i][j]);
            	
            	if(dMaxAbs < Math.abs(eqs_W[i][j]))
            		dMaxAbs = Math.abs(eqs_W[i][j]);
            	
            	if(dMaxAbs < Math.abs(eqs_Z[i][j]))
            		dMaxAbs = Math.abs(eqs_Z[i][j]);
            }

	        double dInvMaxAbs = 1.0 / dMaxAbs;
	        for(int j = 0; j < iNumEqP1; j++){
	            eqs_C[i][j] *= dInvMaxAbs;
	            eqs_W[i][j] *= dInvMaxAbs;
	            eqs_Z[i][j] *= dInvMaxAbs;
	        }       
	    }
	    return true;
	}// end 
	
	/*
	 * char[] eqs_var, int[] eqs_varIdx,
			double[][] eqs_C, double[][] eqs_W, double[][] eqs_Z
	 */
	private final static strictfp void _LCP_solve(
			char[] eqs_var, int[] eqs_varIdx, double[][] eqs_C, double[][] eqs_W, double[][] eqs_Z,
			char basicVar, int basicVarIdx, char nonBasicVar, int nonBasicVarIdx){
	    int iFound = -1;
	    int i ,j;
	    for (i = 0; i < eqs_var.length; i++){
	        if(eqs_var[i] == basicVar){
	            if(eqs_varIdx[i] == basicVarIdx)
	                iFound = i;
	        }
	    }
	    if(iFound < 0 || iFound > eqs_var.length - 1){
	        return;
	    }

	    // the equation for the replacement variable in this cycle
	    char kRep_var = nonBasicVar;
	    int kRep_varIdx = nonBasicVarIdx;
	    double[] kRep_C = new double[eqs_var.length + 1];
	    double[] kRep_W = new double[eqs_var.length + 1];
	    double[] kRep_Z = new double[eqs_var.length + 1];

	    double dDenom;
	    if(nonBasicVar == 'z')
	        dDenom = -eqs_Z[iFound][nonBasicVarIdx];
	    else
	        dDenom = -eqs_W[iFound][nonBasicVarIdx];
		//System.out.printf("MathUt : _LCP_solv : iFnd : %d , dDenom : %f\n", iFound, dDenom);
		//System.out.printf("\t, basicVar : %c, basicVarIdx : %d\n", basicVar, basicVarIdx);
		//System.out.printf("\t, nonBscVar : %c, nonBscVrIdx : %d\n", nonBasicVar, nonBasicVarIdx);
		double dInvDenom = 1.0 / dDenom;
	    for(i = 0; i < eqs_var.length + 1; i++){
	        kRep_C[i] = eqs_C[iFound][i] * dInvDenom;
	        kRep_W[i] = eqs_W[iFound][i] * dInvDenom;
	        kRep_Z[i] = eqs_Z[iFound][i] * dInvDenom;
	    }

	    if(nonBasicVar == 'z')
	        kRep_Z[nonBasicVarIdx] = 0.0f;
	    else
	        kRep_W[nonBasicVarIdx] = 0.0f;

	    if(basicVar == 'z')
	        kRep_Z[basicVarIdx] = -dInvDenom;
	    else
	        kRep_W[basicVarIdx] = -dInvDenom;

	    int iNumEqP1 = eqs_var.length + 1;
	    for(i = 0; i < eqs_var.length; i++){
	        if(i != iFound){
	            double dCoef;
	            if ( kRep_var == 'z' )
	                dCoef = eqs_Z[i][nonBasicVarIdx];
	            else
	                dCoef = eqs_W[i][nonBasicVarIdx];

	            if(dCoef != 0.0){
	                for(j = 0; j < iNumEqP1; j++){
	                    eqs_C[i][j] += dCoef * kRep_C[j];
	                    if(abs(eqs_C[i][j]) < FEPSILON * abs(kRep_C[j])){
	                        eqs_C[i][j] = 0.0f;
	                    }

	                    eqs_W[i][j] += dCoef * kRep_W[j];
	                    if(abs(eqs_W[i][j]) < FEPSILON * abs(kRep_W[j])){
	                        eqs_W[i][j] = 0.0f;
	                    }

	                    eqs_Z[i][j] += dCoef * kRep_Z[j];
	                    if(abs(eqs_Z[i][j]) < FEPSILON * abs(kRep_Z[j])){
	                        eqs_Z[i][j] = 0.0f;
	                    }
	                }

	                if(kRep_var == 'z')
	                    eqs_Z[i][kRep_varIdx] = 0.0f;
	                else
	                    eqs_W[i][kRep_varIdx] = 0.0f;
	            }
	        }
	    }

	    // replace the row corresponding to this equation
	    eqs_var[iFound] = kRep_var;
	    eqs_varIdx[iFound] = kRep_varIdx;
	    System.arraycopy(kRep_C, 0, eqs_C[iFound], 0, kRep_C.length);
	    System.arraycopy(kRep_W, 0, eqs_W[iFound], 0, kRep_W.length);
	    System.arraycopy(kRep_Z, 0, eqs_Z[iFound], 0, kRep_Z.length);
	}
	
	/*
	static void _LCP_print(LCPEquation[] eqs){
        System.out.println("MathU : LCPSolv : equations : ");
        for(int i = 0; i < eqs.length; i++){
        	System.out.println("eqs[" + i + "].C : ");
        	for(int j = 0; j < eqs[i].C.length; j++)
        		System.out.print(eqs[i].C[j] + " ");
        	System.out.println();
        	System.out.println("eqs[" + i + "].Z : ");
        	for(int j = 0; j < eqs[i].Z.length; j++)
        		System.out.print(eqs[i].Z[j] + " ");
        	System.out.println();
        	System.out.println("eqs[" + i + "].W : ");
        	for(int j = 0; j < eqs[i].W.length; j++)
        		System.out.print(eqs[i].W[j] + " ");
        	System.out.println("\neqs[" + i + "].var : " + eqs[i].var + " , varidx : " + eqs[i].varIdx);
        	System.out.println();
        }
	}*/

	public final static strictfp int LCPSolve(int eqCount, double[][] M, double[] Q,
		    double[] Z, double[] W){
		// This code is from Game Physics Bible CD - WmlLCPSolver
		
		/*
		 * char[] eqs_var, int[] eqs_varIdx,
			double[][] eqs_C, double[][] eqs_W, double[][] eqs_Z
		 */
		char[] eqs_var = new char[eqCount];
		int[] eqs_varIdx = new int[eqCount];
		double[][] eqs_C = new double[eqCount][eqCount + 1];
		double[][] eqs_W = new double[eqCount][eqCount + 1];
		double[][] eqs_Z = new double[eqCount][eqCount + 1];
		int status = -2;
		
		if(_LCP_initialize(eqs_var, eqs_varIdx, eqs_C, eqs_W, eqs_Z, M, Q)){
		    char departingVar = 0;
		    int departingVarIdx = 0;
		    char nonBasicVar = 0;
		    int nonBasicVarIdx = 0;
		    
		    boolean bZ0Basic = false;
		    int iTry;
		    final int MAX_RETRIES = 100;
		    
		    
		    for (iTry = 0; iTry < MAX_RETRIES; iTry++){
		    	//System.out.println("\n------- NEXT! ----- ");
		    	int iEqu = 0;
			   //// start SelectEquation
			
				// The algorithm for selecting the equation to be solved is:
				// 1. if z0 is not a basic variable, solve for z0
				//      choose the equation with smallest (negative) constant term.
				// 2. if a w, say wj, has just left the basic set, solve for zj.
				//      choose the equation to solve for zj by:
				//              coefficient, cj, of zj is negative
				//              the ratio constj/-cj is smallest.
			
				// determine if z0 is a basic variable
				
				bZ0Basic = false;
				for(int i = 0; i < eqCount; i++){
				    if(eqs_var[i] == 'z' && eqs_varIdx[i] == 0)
				        bZ0Basic = true;
				}
				//_LCP_print(eqs);
				//System.out.println("bZ0Basic :"+ bZ0Basic);
				
				// If z0 is not basic, find the equation with the smallest (negative)
				// constant term and solve that equation for z0.
				if(!bZ0Basic){
				    departingVarIdx = 0;
				    nonBasicVar = 'z';
				    nonBasicVarIdx = 0;
				}else{
					// z0 is basic
					// Since the departing variable left the dictionary, solve for the
					// complementary variable.
					nonBasicVar = (departingVar == 'w' ? 'z' : 'w');
				}
			
			
			
				//// FindEquation
				boolean found = false;
				{
					if(departingVarIdx != 0){
						//System.out.println("LCP : find : firstBlock");
						// Find the limiting equation for variables other than z0.  The
						// coefficient of the variable must be negative.  The ratio of the
						// constant polynomial to the negative of the smallest coefficient
						// of the variable is sought.   The constant polynomial must be
						// evaluated to compute this ratio.  It must be evaluated at a value
						// of the variable, dEpsi, such that the ratio remains smallest for
						// all smaller dEpsi.
						
						///// start EquationAlgorithm
						{
						    // This code loops through the rows of the z or w array to find all the
							// terms for which the coefficient of the chosen term is negative.  The
							// row search is reduced to these.  For the columns of the constants array
							// the rows (equations) for which the ratios of the constant terms to the
							// z or w coefficients of interest is smallest are found. If there are
							// several such rows, they are noted.  The row search is further reduced
							// to these.  Proceed to the next column until there is only one row left.
				
							int[][] aaiFoundArray = new int[eqs_var.length + 1][2];
							
							// Find equations with negative coefficients for selected index.
							double dTemp;
							int i, j;
							for(i = 0, j = 0; i < eqs_var.length; i++){         
								if(nonBasicVar == 'z')
									dTemp = eqs_Z[i][departingVarIdx];
								else
									dTemp = eqs_W[i][departingVarIdx];
							
								if(dTemp < 0.0)
									aaiFoundArray[j++][0] = i;
							}
				
							if(j != 0){
								// no terms with negative coefficients
								aaiFoundArray[j][0] = -1;
								
								// Find equation with smallest ratio of constTerm (polynomial) to 
								// selected (NonBasicVariable, DepartingVariableIndex) coefficient.
								int iFAI1 = 0, iFAI2 = 1;
								for (i = 0; i < eqs_var.length + 1; i++){
								    iFAI2 = ( iFAI1 == 0 ? 1 : 0 );
					
								    int iFI1 = 0, iFI2 = 0;
								    int j1 = aaiFoundArray[iFI1++][iFAI1];
								    aaiFoundArray[iFI2++][iFAI2] = j1;
								    int k = iFI1;
								    
								    while(aaiFoundArray[k][iFAI1] > -1){
								        int j2 = aaiFoundArray[k][iFAI1];
								        if(j2 < 0)
								            break;
								
								        double dDenom1, dDenom2;
								        if(nonBasicVar == 'z'){
									        dDenom1 = eqs_Z[j1][departingVarIdx];
									        dDenom2 = eqs_Z[j2][departingVarIdx];
									    }else{
									        dDenom1 = eqs_W[j1][departingVarIdx]; 
									        dDenom2 = eqs_W[j2][departingVarIdx]; 
									    }
									    dTemp = eqs_C[j2][i] / dDenom2 - eqs_C[j1][i] / dDenom1;
									    if(dTemp < -FEPSILON){
									        // The first equation has the smallest ratio.  Do nothing;
									        // the first equation is the choice.
									    }else if (dTemp > FEPSILON){
									        // The second equation has the smallest ratio.
									        iFI1 = k;  // make second equation comparison standard
									        iFI2 = 0;  // restart the found array index
									        j1 = aaiFoundArray[iFI1++][iFAI1];
									        aaiFoundArray[iFI2++][iFAI2] = j1;
									    }else{
									    	// the ratios are the same
									        aaiFoundArray[iFI2++][iFAI2] = j2;
									    }
									    k++;
									    aaiFoundArray[iFI2][iFAI2] = -1;
									}
					
									if(iFI2 == 1){
										// "correct" exit
										//System.out.println("MathU : LCPSolv : correct exit!");
										iEqu = aaiFoundArray[0][iFAI2]+1;
										found = true;
										break;
									}
					
									iFAI1 = ( iFAI1 == 0 ? 1 : 0 );
								}
						    }
						}
						///// end EquationAlgorithm
					}else{
						// Special case for nonbasic z0; the coefficients are 1.  Find the
						// limiting equation when solving for z0.  At least one C[0] must be
						// negative initially or we start with a solution.  If all of the
						// negative constant terms are different, pick the equation with the
						// smallest (negative) ratio of constant term to the coefficient of
						// z0.  If several equations contain the smallest negative constant
						// term, pick the one with the highest coefficient for that one
						// contains dEpsi to the largest exponent.  NOTE: This is equivalent
						// to using the constant term polynomial in dEpsi but avoids
						// evaluating it.
						//System.out.println("MathU : LCDSolv : FndEq elseblock st");
						double dMin = 0.0f;
						for(int i = 0; i < eqCount; i++){
						    if(eqs_Z[i][0] != 0.0){
						    	// Z0으로 정리했을 때 상수항.
						    	double dQuot = eqs_C[i][0] / eqs_Z[i][0];
						    	if(dQuot <= dMin || dMin == 0.0){
						    		dMin = dQuot;
						    		iEqu = i + 1;
						    	}
						    }
						}
						found = dMin < 0.0;
					}
				}
				//// end FindEquation
			
				if(found){
					nonBasicVarIdx = departingVarIdx;
					departingVar = eqs_var[iEqu-1];
					departingVarIdx = eqs_varIdx[iEqu-1];
				}
				//System.out.println("after change : Departing var : " + departingVar + "(" + departingVarIdx + ")");
				///// end SelectEquation
				    
				if(!found){
					//System.out.println("LCP : Cannot remove complementary..");
					//System.out.flush();
					status = LCP_CANNOT_REMOVE_COMPLEMENTARY;
					break;
				}

				/*
				 * char[] eqs_var, int[] eqs_varIdx,
					double[][] eqs_C, double[][] eqs_W, double[][] eqs_Z
				 */
				_LCP_solve(eqs_var, eqs_varIdx, eqs_C, eqs_W, eqs_Z,
						eqs_var[iEqu-1], eqs_varIdx[iEqu-1], nonBasicVar, nonBasicVarIdx);
				
				// determine if z0 is a basic variable
				bZ0Basic = false;
				for (int i = 0; i < eqCount; i++){
				    if(eqs_var[i] == 'z' && eqs_varIdx[i] == 0){
				        bZ0Basic = true;
				        break;
				    }
				}
				
				if(!bZ0Basic){
				    // solution found when z0 is removed from the basic set
					for(int j = 0; j < Z.length; j++)
						Z[j] = 0;
					for(int j = 0; j < W.length; j++)
						W[j] = 0;
					
					for(int j = 0; j < eqCount; j++){
					    if (eqs_var[j] == 'z')
					    	Z[eqs_varIdx[j] - 1] = eqs_C[j][0];
					    else
					    	W[eqs_varIdx[j] - 1] = eqs_C[j][0];
					}
					status = LCP_FOUND_SOLUTION;
					break;
				}
			}
			
		    if(iTry == MAX_RETRIES)
		    	status = LCP_EXCEEDED_MAX_RETRIES;
		}else{
			status = LCP_FOUND_TRIVIAL_SOLUTION;
		}
		return status;
	}// end LCPSolve
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/* **************************************
	 *       Scalar Operations
	 *******************************************/
	public final static strictfp float fastPow(float a, int cnt){
		if(a == Float.NaN)
			return Float.NaN;
		else if(a == Float.POSITIVE_INFINITY)
			return Float.POSITIVE_INFINITY;
		else if(a == Float.NEGATIVE_INFINITY)
			return (cnt % 2 == 1) ? Float.NEGATIVE_INFINITY : Float.POSITIVE_INFINITY;
		
		if(cnt == 2)
			return a * a;
		else if(cnt == 1)
			return a;
		else if(cnt == 0)
			return 1;
		else if(cnt < 0){
			a = 1.0f / a;
			cnt = -cnt;
		}
		
		float d = 1;
		for(int i = 0; i < cnt; i++)
			d = d * a;
		return d;
	}// end fastPow
	public final static strictfp double fastPow(double a, int cnt){
		if(a == Float.NaN)
			return Float.NaN;
		else if(a == Float.POSITIVE_INFINITY)
			return Float.POSITIVE_INFINITY;
		else if(a == Float.NEGATIVE_INFINITY)
			return (cnt % 2 == 1) ? Float.NEGATIVE_INFINITY : Float.POSITIVE_INFINITY;
		
		if(cnt == 2)
			return a * a;
		else if(cnt == 1)
			return a;
		else if(cnt == 0)
			return 1;
		else if(cnt < 0){
			a = 1.0f / a;
			cnt = -cnt;
		}
		
		double d = 1;
		for(int i = 0; i < cnt; i++)
			d = d * a;
		return d;
	}// end fastPow
	
	
	
	
	
	
	
	
	
	
	
	
	
	/* *****************************************
	 *          Vector Operations
	 *****************************************/
	public final static strictfp float crossVal(float ax, float ay, float az, float bx, float by, float bz){
		return (ay * bz - az * by) - (ax * bz - az * bx) + (ax * by - ay * bx);
	}// end crossval
	public final static strictfp Vector3f minus(Point3f a, Point3f b, Vector3f r){
		r.x = a.x - b.x;
		r.y = a.y - b.y;
		r.z = a.z - b.z;
		return r;
	}// end minus(Point3f, Point3f, Vector3f)
	public final static strictfp Vector3d minus(Point3d a, Point3d b, Vector3d r){
		r.x = a.x - b.x;
		r.y = a.y - b.y;
		r.z = a.z - b.z;
		return r;
	}// end minus(Point3f, Point3f, Vector3f)
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/* *******************************************
	 *        Matrix operations
	 *******************************************/
	
	
	public final static strictfp float[][] toArray(Matrix4f m){
		float[][] array = new float[][]{
				{m.m00, m.m01, m.m02, m.m03},
				{m.m10, m.m11, m.m12, m.m13},
				{m.m20, m.m21, m.m22, m.m23},
				{m.m30, m.m31, m.m32, m.m33}
		};
		return array;
	}// end toArray(Matrix4f)
	public final static strictfp Matrix3f clear(Matrix3f m){
		if(m == null)
			return new Matrix3f();
		m.m00 = m.m01 = m.m02 = m.m10 = m.m11 = m.m12 = m.m20 = m.m21 = m.m22 = 0;
		return m;
	}// end clear(Matrix3f)
	public final static strictfp float[][] block_ver(float[][] ... d) throws MismatchedSizeException{
		if(d.length == 0) return null;
		
		int hei = 0, wid = -1;
		for(int i = 0; i < d.length; i++){
			if(d[i].length == 0) continue;
			
			hei += d[i].length;
			if(wid == -1)
				wid = d[i][0].length;
			for(int j = 0; j < d[i].length; j++)
				if(wid != d[i][j].length)
					throw new MismatchedSizeException("d[" + i + "][" + j + "].width : "
							+ d[i][j].length + " , previous width : " + wid);
		}
		
		float[][] array = new float[hei][wid];
		int curhei = 0;
		for(int i = 0; i < d.length; i++){
			System.arraycopy(d[i], 0, array[curhei], 0, d[i].length);
			curhei += d[i].length;
		}
		return array;
	}// end block_ver(float[][] ...)
	public final static strictfp double[][] block_ver(double[][] ... d) throws MismatchedSizeException{
		if(d.length == 0) return null;
		
		int hei = 0, wid = -1;
		for(int i = 0; i < d.length; i++){
			if(d[i].length == 0) continue;
			
			hei += d[i].length;
			if(wid == -1)
				wid = d[i][0].length;
			for(int j = 0; j < d[i].length; j++)
				if(wid != d[i][j].length)
					throw new MismatchedSizeException("d[" + i + "][" + j + "].width : "
							+ d[i][j].length + " , previous width : " + wid);
		}
		
		double[][] array = new double[hei][wid];
		int curhei = 0;
		for(int i = 0; i < d.length; i++){
			System.arraycopy(d[i], 0, array[curhei], 0, d[i].length);
			curhei += d[i].length;
		}
		return array;
	}// end block_ver(float[][] ...)
	public final static strictfp float[][] block_quad(float[][] a0_0, float[][] a0_1,
			float[][] a1_0, float[][] a1_1) throws MismatchedSizeException{
		if(a0_0 != null && a0_0.length == 0)
			throw new MismatchedSizeException("a0_0 height : 0");
		if(a0_1 != null && a0_1.length == 0)
			throw new MismatchedSizeException("a0_1 height : 0");
		if(a1_0 != null && a1_0.length == 0)
			throw new MismatchedSizeException("a1_0 height : 0");
		if(a1_1 != null && a1_1.length == 0)
			throw new MismatchedSizeException("a1_1 height : 0");
		
		float[][] array = new float[a0_0[0].length + a0_1[0].length]
		                              [a0_0.length + a1_0.length];
		if(a0_0 != null)
			for(int i = 0; i < a0_0.length; i++)
			for(int j = 0; j < a0_0[i].length; j++)
				array[i][j] = a0_0[i][j];
		
		if(a0_1 != null)
			for(int i = 0; i < a0_1.length; i++)
				for(int j = 0; j < a0_1[i].length; j++)
					array[i][j + a0_0[0].length] = a0_1[i][j];
		
		if(a1_0 != null)
			for(int i = 0; i < a1_0.length; i++)
				for(int j = 0; j < a1_0[i].length; j++)
					array[i + a0_0.length][j] = a1_0[i][j];
		
		if(a1_1 != null)
			for(int i = 0; i < a1_1.length; i++)
				for(int j = 0; j < a1_1[0].length; j++)
					array[i + a0_0.length][j + a0_0[0].length] = a1_1[i][j];
		
		return array;
	}// end block_quad()
	public final static strictfp double[][] block_quad(double[][] a0_0, double[][] a0_1,
			double[][] a1_0, double[][] a1_1) throws MismatchedSizeException{
		if(a0_0 != null && a0_0.length == 0)
			throw new MismatchedSizeException("a0_0 height : 0");
		if(a0_1 != null && a0_1.length == 0)
			throw new MismatchedSizeException("a0_1 height : 0");
		if(a1_0 != null && a1_0.length == 0)
			throw new MismatchedSizeException("a1_0 height : 0");
		if(a1_1 != null && a1_1.length == 0)
			throw new MismatchedSizeException("a1_1 height : 0");
		
		double[][] array = new double[a0_0[0].length + a0_1[0].length]
		                              [a0_0.length + a1_0.length];
		if(a0_0 != null)
			for(int i = 0; i < a0_0.length; i++)
			for(int j = 0; j < a0_0[i].length; j++)
				array[i][j] = a0_0[i][j];
		
		if(a0_1 != null)
			for(int i = 0; i < a0_1.length; i++)
				for(int j = 0; j < a0_1[i].length; j++)
					array[i][j + a0_0[0].length] = a0_1[i][j];
		
		if(a1_0 != null)
			for(int i = 0; i < a1_0.length; i++)
				for(int j = 0; j < a1_0[i].length; j++)
					array[i + a0_0.length][j] = a1_0[i][j];
		
		if(a1_1 != null)
			for(int i = 0; i < a1_1.length; i++)
				for(int j = 0; j < a1_1[0].length; j++)
					array[i + a0_0.length][j + a0_0[0].length] = a1_1[i][j];
		
		return array;
	}// end block_quad()
	
	
	
	
	/*
	/**
	 * 
	 * @param matrix
	 * @returns if the matrix is not invertible, false returns 
	 *  /
	public final static strictfp boolean invert(float[][] matrix, float[][] E){
		if(matrix.length == 0 || matrix.length != matrix[0].length)
			return false;
		gaussElimination(matrix, E, matrix.length);
		return true;
	}// end invert
	/**
	 * 
	 * @param matrix
	 * @returns if the matrix is not invertible, false returns 
	 *  /
	public final static strictfp boolean invert(double[][] matrix, double[][] E){
		if(matrix.length == 0 || matrix.length != matrix[0].length)
			return false;
		gaussElimination(matrix, E, matrix.length);
		return true;
	}// end invert
	*/
	
	
	
	
	
	/**
	 * Given matrix(4x4) must be R=block_quad(M, D, 0, 1), (M : rotational matrix)
	 * or this function will calc wrong answer
	 */
	public final static strictfp Matrix4f fastInvert(Matrix4f src, Matrix4f dst){
		if(src == null)
			return null;
		else if(dst == null)
			dst = new Matrix4f();
		// Matrix(4x4) R = block_quad(M, D, 0, 1)
		// And, M^(-1) = M^T 
		// Assume that R^(-1) = block_quad(M^T, D', 0, 1)
		//
		// From R^(-1)R = I,
		// block_quad(M^(-1)M, MD' + D, 0, 1) = I,
		// MD' + D = 0
		// Therefore, D' = -M^(-1)D = -M^(-T)D
		dst.m00 = src.m00; dst.m01 = src.m10; dst.m02 = src.m20;
		dst.m10 = src.m01; dst.m11 = src.m11; dst.m12 = src.m21;
		dst.m20 = src.m02; dst.m21 = src.m12; dst.m22 = src.m22;
		
		dst.m03 = -dst.m00 * src.m03 - dst.m01 * src.m13 - dst.m02 * src.m23;
		dst.m13 = -dst.m10 * src.m03 - dst.m11 * src.m13 - dst.m12 * src.m23;
		dst.m23 = -dst.m20 * src.m03 - dst.m21 * src.m13 - dst.m22 * src.m23;
		
		dst.m33 = 1;
		dst.m30 = dst.m31 = dst.m32 = 0;
		return dst;
	}// end fastInvert()
	public final static strictfp Matrix4d fastInvert(Matrix4d src, Matrix4d dst){
		if(src == null)
			return null;
		else if(dst == null)
			dst = new Matrix4d();
		// Matrix(4x4) R = block_quad(M, D, 0, 1)
		// And, M^(-1) = M^T 
		// Assume that R^(-1) = block_quad(M^T, D', 0, 1)
		//
		// From R^(-1)R = I,
		// block_quad(M^(-1)M, MD' + D, 0, 1) = I,
		// MD' + D = 0
		// Therefore, D' = -M^(-1)D = -M^(-T)D
		dst.m00 = src.m00; dst.m01 = src.m10; dst.m02 = src.m20;
		dst.m10 = src.m01; dst.m11 = src.m11; dst.m12 = src.m21;
		dst.m20 = src.m02; dst.m21 = src.m12; dst.m22 = src.m22;
		
		dst.m03 = -dst.m00 * src.m03 - dst.m01 * src.m13 - dst.m02 * src.m23;
		dst.m13 = -dst.m10 * src.m03 - dst.m11 * src.m13 - dst.m12 * src.m23;
		dst.m23 = -dst.m20 * src.m03 - dst.m21 * src.m13 - dst.m22 * src.m23;
		
		dst.m33 = 1;
		dst.m30 = dst.m31 = dst.m32 = 0;
		return dst;
	}// end fastInvert()
	
	
	
	public final static strictfp float[][] transpose(float[][] original, float[][] a){
		if(a == null){
			if(original.length == 0)
				return a;
			a = new float[original[0].length][original.length];
		}
		for(int i = 0; i < original.length; i++)
			for(int j = 0; j < original[0].length; j++)
				a[j][i] = original[i][j];
		return a;
	}// end transpose(float[][], float[][])
	public final static strictfp double[][] transpose(double[][] original, double[][] a){
		if(a == null){
			if(original.length == 0)
				return a;
			a = new double[original[0].length][original.length];
		}
		for(int i = 0; i < original.length; i++)
			for(int j = 0; j < original[0].length; j++)
				a[j][i] = original[i][j];
		return a;
	}// end transpose(float[][], float[][])
	public final static strictfp float[][] mul(float[][] a, float s, float[][] b){
		if(b == null){
			if(a.length == 0)
				return b;
			b = new float[a.length][a[0].length];
		}
		for(int i = 0; i < a.length; i++)
			for(int j = 0; j < a[i].length; j++)
				b[i][j] = s * a[i][j];
		return b;
	}// end mul(float[][], float)
	public final static strictfp double[][] mul(double[][] a, double s, double[][] b){
		if(b == null){
			if(a.length == 0)
				return b;
			b = new double[a.length][a[0].length];
		}
		for(int i = 0; i < a.length; i++)
			for(int j = 0; j < a[i].length; j++)
				b[i][j] = s * a[i][j];
		return b;
	}// end mul(float[][], float)
	
	
	
	/**
	 * Only for transform matrix available
	 * @param a
	 * @param b
	 * @param res res != a or b
	 */
	public final static strictfp Matrix4d fastMul(Matrix4d a, Matrix4d b, Matrix4d res){
		if(res == null)
			res = new Matrix4d();
		
		if(a == null)
			res.set(b);
		else if(b == null)
			res.set(a);
		else{
			res.m00 = a.m00 * b.m00 + a.m01 * b.m10 + a.m02 * b.m20;
			res.m01 = a.m00 * b.m01 + a.m01 * b.m11 + a.m02 * b.m21;
			res.m02 = a.m00 * b.m02 + a.m01 * b.m12 + a.m02 * b.m22;

			res.m10 = a.m10 * b.m00 + a.m11 * b.m10 + a.m12 * b.m20;
			res.m11 = a.m10 * b.m01 + a.m11 * b.m11 + a.m12 * b.m21;
			res.m12 = a.m10 * b.m02 + a.m11 * b.m12 + a.m12 * b.m22;

			res.m20 = a.m20 * b.m00 + a.m21 * b.m10 + a.m22 * b.m20;
			res.m21 = a.m20 * b.m01 + a.m21 * b.m11 + a.m22 * b.m21;
			res.m22 = a.m20 * b.m02 + a.m21 * b.m12 + a.m22 * b.m22;
			
			res.m33 = 1;

			res.m03 = a.m00 * b.m03 + a.m01 * b.m13 + a.m02 * b.m23 + a.m03;
			res.m13 = a.m10 * b.m03 + a.m11 * b.m13 + a.m12 * b.m23 + a.m13;
			res.m23 = a.m20 * b.m03 + a.m21 * b.m13 + a.m22 * b.m23 + a.m23;
			
			res.m30 = res.m31 = res.m32 = 0;
		}
		return res;
	}// end fastMul
	public final static strictfp Matrix4f fastMul(Matrix4f a, Matrix4f b, Matrix4f res){
		if(res == null)
			res = new Matrix4f();
		
		if(a == null)
			res.set(b);
		else if(b == null)
			res.set(a);
		else{
			res.m00 = a.m00 * b.m00 + a.m01 * b.m10 + a.m02 * b.m20;
			res.m01 = a.m00 * b.m01 + a.m01 * b.m11 + a.m02 * b.m21;
			res.m02 = a.m00 * b.m02 + a.m01 * b.m12 + a.m02 * b.m22;

			res.m10 = a.m10 * b.m00 + a.m11 * b.m10 + a.m12 * b.m20;
			res.m11 = a.m10 * b.m01 + a.m11 * b.m11 + a.m12 * b.m21;
			res.m12 = a.m10 * b.m02 + a.m11 * b.m12 + a.m12 * b.m22;

			res.m20 = a.m20 * b.m00 + a.m21 * b.m10 + a.m22 * b.m20;
			res.m21 = a.m20 * b.m01 + a.m21 * b.m11 + a.m22 * b.m21;
			res.m22 = a.m20 * b.m02 + a.m21 * b.m12 + a.m22 * b.m22;
			
			res.m33 = 1;

			res.m03 = a.m00 * b.m03 + a.m01 * b.m13 + a.m02 * b.m23 + a.m03;
			res.m13 = a.m10 * b.m03 + a.m11 * b.m13 + a.m12 * b.m23 + a.m13;
			res.m23 = a.m20 * b.m03 + a.m21 * b.m13 + a.m22 * b.m23 + a.m23;
			
			res.m30 = res.m31 = res.m32 = 0;
		}
		return res;
	}// end fastMul
	
	
	
	
	
	
	
	
	
	

	/* ************************************************
	 *     INERTIA TENSOR
	 ***********************************************/
	public strictfp static void addInertiaTensor(Point3f[] points,
			int[][] faces, float mass,
			float transX, float transY, float transZ,
			Matrix3f tensor){
		if(Float.isInfinite(mass)){
			tensor.m00 = Float.POSITIVE_INFINITY; tensor.m01 = Float.NEGATIVE_INFINITY; tensor.m02 = Float.NEGATIVE_INFINITY;
			tensor.m10 = Float.NEGATIVE_INFINITY; tensor.m11 = Float.POSITIVE_INFINITY; tensor.m12 = Float.NEGATIVE_INFINITY;
			tensor.m20 = Float.NEGATIVE_INFINITY; tensor.m21 = Float.NEGATIVE_INFINITY; tensor.m22 = Float.POSITIVE_INFINITY;
			return;
		}
		
		// i번째 면에 대해 원점까지의 부피를 구한다. 그리고,
		// 그 부피를 총 합한 것을 volume variable에 집어넣는다.
		float[] pieceVolumes = new float[faces.length];
		float totalVolume = 0;
		
		float tmpx = 0, tmpy = 0, tmpz = 0;
		Point3f a = new Point3f(), b = new Point3f(), c = new Point3f();
		
		float xx = 0, xy = 0, xz = 0,
			yy = 0, yz = 0,
			zz = 0;
		
		float beforevol, aftervol = 0;
		
		for(int i = 0; i < faces.length; i++){
			if(faces[i].length <= 2)
				continue;
			//System.out.println("\ni : " + i);
			
			float tmpval = 0;
			
			tmpx = tmpy = tmpz = 0;
			aftervol = 0;
			// 점 두 개를 잡고 0에서 점 1, 점 1에서 점 2까지의 외적을 구한 후, 그 합을 저장.
			c.set(points[faces[i][0]]);
			c.x += transX;
			c.y += transY;
			c.z += transZ;
			for(int j = 1; j < faces[i].length - 1; j++){
				a.set(points[faces[i][j]]);
				b.set(points[faces[i][j + 1]]);

				a.x += transX;
				a.y += transY;
				a.z += transZ;
				
				b.x += transX;
				b.y += transY;
				b.z += transZ;
				
				beforevol = aftervol;
				tmpx += (a.y - c.y) * (b.z - c.z) - (a.z - c.z) * (b.y - c.y);
				tmpy += (a.z - c.z) * (b.x - c.x) - (a.x - c.x) * (b.z - c.z);
				tmpz += (a.x - c.x) * (b.y - c.y) - (a.y - c.y) * (b.x - c.x);
				aftervol = (tmpx * c.x + tmpy * c.y
						+ tmpz * c.z);
				//System.out.println("  volume : " + (aftervol - beforevol));
				
				// 이 삼각뿔에 대해 inertia moment, inertia product 또한 구한다.
				
				tmpval += (aftervol - beforevol) * 
					 ((a.x*a.x + a.y*a.y) + (b.x*b.x + b.y*b.y) + (c.x*c.x + c.y*c.y)
					+ (a.x*b.x + a.y*b.y) + (a.x*c.x + a.y*c.y) + (b.x*c.x + b.y*c.y));
				zz += (aftervol - beforevol) * 
					 ((a.x*a.x + a.y*a.y) + (b.x*b.x + b.y*b.y) + (c.x*c.x + c.y*c.y)
					+ (a.x*b.x + a.y*b.y) + (a.x*c.x + a.y*c.y) + (b.x*c.x + b.y*c.y));
				yy += (aftervol - beforevol) * 
					 ((a.x*a.x + a.z*a.z) + (b.x*b.x + b.z*b.z) + (c.x*c.x + c.z*c.z)
					+ (a.x*b.x + a.z*b.z) + (a.x*c.x + a.z*c.z) + (b.x*c.x + b.z*c.z));
				xx += (aftervol - beforevol) * 
					 ((a.z*a.z + a.y*a.y) + (b.z*b.z + b.y*b.y) + (c.z*c.z + c.y*c.y)
					+ (a.z*b.z + a.y*b.y) + (a.z*c.z + a.y*c.y) + (b.z*c.z + b.y*c.y));

				xy += (aftervol - beforevol) *
					((a.x + b.x + c.x) * (a.y + b.y + c.y) +
							(a.x * a.y + b.x * b.y + c.x * c.y)
					);
				yz += (aftervol - beforevol) *
					((a.y + b.y + c.y) * (a.z + b.z + c.z) +
							(a.y * a.z + b.y * b.z + c.y * c.z)
					);
				xz += (aftervol - beforevol) *
					((a.x + b.x + c.x) * (a.z + b.z + c.z) +
							(a.x * a.z + b.x * b.z + c.x * c.z)
					);

				a.x -= transX;
				a.y -= transY;
				a.z -= transZ;
				b.x -= transX;
				b.y -= transY;
				b.z -= transZ;
			}
			pieceVolumes[i] = aftervol;
			//System.out.println("  pieceVolumes[" + i + "] : " + pieceVolumes[i]);
			totalVolume += pieceVolumes[i];

			c.x -= transX;
			c.y -= transY;
			c.z -= transZ;
		}
		
		tensor.m00 += xx * mass / 10.0f / totalVolume;
		tensor.m11 += yy * mass / 10.0f / totalVolume;
		tensor.m22 += zz * mass / 10.0f / totalVolume;
		
		tensor.m01 -= xy * mass / 20.0f / totalVolume;
		tensor.m10 -= xy * mass / 20.0f / totalVolume;
		tensor.m20 -= xz * mass / 20.0f / totalVolume;
		tensor.m02 -= xz * mass / 20.0f / totalVolume;
		tensor.m12 -= yz * mass / 20.0f / totalVolume;
		tensor.m21 -= yz * mass / 20.0f / totalVolume;
	}// end getInertiaTensor(Point3f[], int[][], Matrix3f)
	public strictfp static void addInertiaTensor(Point3d[] points,
			int[][] faces, double mass,
			double transX, double transY, double transZ,
			Matrix3d tensor){
		// i번째 면에 대해 원점까지의 부피를 구한다. 그리고,
		// 그 부피를 총 합한 것을 volume variable에 집어넣는다.
		double[] pieceVolumes = new double[faces.length];
		double totalVolume = 0;
		
		double tmpx = 0, tmpy = 0, tmpz = 0;
		Point3d a = new Point3d(), b = new Point3d(), c = new Point3d();
		
		double xx = 0, xy = 0, xz = 0,
			yy = 0, yz = 0,
			zz = 0;
		
		double beforevol, aftervol = 0;
		
		for(int i = 0; i < faces.length; i++){
			if(faces[i].length <= 2)
				continue;
			//System.out.println("\ni : " + i);
			
			double tmpval = 0;
			
			tmpx = tmpy = tmpz = 0;
			aftervol = 0;
			// 점 두 개를 잡고 0에서 점 1, 점 1에서 점 2까지의 외적을 구한 후, 그 합을 저장.
			c.set(points[faces[i][0]]);
			c.x += transX;
			c.y += transY;
			c.z += transZ;
			for(int j = 1; j < faces[i].length - 1; j++){
				a.set(points[faces[i][j]]);
				b.set(points[faces[i][j + 1]]);

				a.x += transX;
				a.y += transY;
				a.z += transZ;
				
				b.x += transX;
				b.y += transY;
				b.z += transZ;
				
				beforevol = aftervol;
				tmpx += (a.y - c.y) * (b.z - c.z) - (a.z - c.z) * (b.y - c.y);
				tmpy += (a.z - c.z) * (b.x - c.x) - (a.x - c.x) * (b.z - c.z);
				tmpz += (a.x - c.x) * (b.y - c.y) - (a.y - c.y) * (b.x - c.x);
				aftervol = (tmpx * c.x + tmpy * c.y
						+ tmpz * c.z);
				//System.out.println("  volume : " + (aftervol - beforevol));
				
				// 이 삼각뿔에 대해 inertia moment, inertia product 또한 구한다.
				
				tmpval += (aftervol - beforevol) * 
					 ((a.x*a.x + a.y*a.y) + (b.x*b.x + b.y*b.y) + (c.x*c.x + c.y*c.y)
					+ (a.x*b.x + a.y*b.y) + (a.x*c.x + a.y*c.y) + (b.x*c.x + b.y*c.y));
				zz += (aftervol - beforevol) * 
					 ((a.x*a.x + a.y*a.y) + (b.x*b.x + b.y*b.y) + (c.x*c.x + c.y*c.y)
					+ (a.x*b.x + a.y*b.y) + (a.x*c.x + a.y*c.y) + (b.x*c.x + b.y*c.y));
				yy += (aftervol - beforevol) * 
					 ((a.x*a.x + a.z*a.z) + (b.x*b.x + b.z*b.z) + (c.x*c.x + c.z*c.z)
					+ (a.x*b.x + a.z*b.z) + (a.x*c.x + a.z*c.z) + (b.x*c.x + b.z*c.z));
				xx += (aftervol - beforevol) * 
					 ((a.z*a.z + a.y*a.y) + (b.z*b.z + b.y*b.y) + (c.z*c.z + c.y*c.y)
					+ (a.z*b.z + a.y*b.y) + (a.z*c.z + a.y*c.y) + (b.z*c.z + b.y*c.y));

				xy += (aftervol - beforevol) *
					((a.x + b.x + c.x) * (a.y + b.y + c.y) +
							(a.x * a.y + b.x * b.y + c.x * c.y)
					);
				yz += (aftervol - beforevol) *
					((a.y + b.y + c.y) * (a.z + b.z + c.z) +
							(a.y * a.z + b.y * b.z + c.y * c.z)
					);
				xz += (aftervol - beforevol) *
					((a.x + b.x + c.x) * (a.z + b.z + c.z) +
							(a.x * a.z + b.x * b.z + c.x * c.z)
					);

				a.x -= transX;
				a.y -= transY;
				a.z -= transZ;
				b.x -= transX;
				b.y -= transY;
				b.z -= transZ;
			}
			//System.out.println("delta Izz : " + tmpval);
			pieceVolumes[i] = aftervol;
			//System.out.println("  pieceVolumes[" + i + "] : " + pieceVolumes[i]);
			totalVolume += pieceVolumes[i];

			c.x -= transX;
			c.y -= transY;
			c.z -= transZ;
			 
			//System.out.println("xx : "  +xx + " , yy : " + yy + " , zz : " + zz);
		}
		
		tensor.m00 += xx * mass / 10.0 / totalVolume;
		tensor.m11 += yy * mass / 10.0 / totalVolume;
		tensor.m22 += zz * mass / 10.0 / totalVolume;
		
		tensor.m01 -= xy * mass / 20.0 / totalVolume;
		tensor.m10 -= xy * mass / 20.0 / totalVolume;
		tensor.m20 -= xz * mass / 20.0 / totalVolume;
		tensor.m02 -= xz * mass / 20.0 / totalVolume;
		tensor.m12 -= yz * mass / 20.0 / totalVolume;
		tensor.m21 -= yz * mass / 20.0 / totalVolume;
	}// end getInertiaTensor(Point3f[], int[][], Matrix3f)
	public final static strictfp
			void addTranslatedInertiaTensor(Matrix3f cmtensor,
					float transX, float transY, float transZ, float mass,
					Matrix3f target){
		if((cmtensor.m00 == Float.POSITIVE_INFINITY || cmtensor.m00 == Float.NEGATIVE_INFINITY)
				&& (cmtensor.m01 == Float.POSITIVE_INFINITY || cmtensor.m01 == Float.NEGATIVE_INFINITY)
				&& (cmtensor.m02 == Float.POSITIVE_INFINITY || cmtensor.m02 == Float.NEGATIVE_INFINITY)
				&& (cmtensor.m10 == Float.POSITIVE_INFINITY || cmtensor.m10 == Float.NEGATIVE_INFINITY)
				&& (cmtensor.m11 == Float.POSITIVE_INFINITY || cmtensor.m11 == Float.NEGATIVE_INFINITY)
				&& (cmtensor.m12 == Float.POSITIVE_INFINITY || cmtensor.m12 == Float.NEGATIVE_INFINITY)
				&& (cmtensor.m20 == Float.POSITIVE_INFINITY || cmtensor.m20 == Float.NEGATIVE_INFINITY)
				&& (cmtensor.m21 == Float.POSITIVE_INFINITY || cmtensor.m21 == Float.NEGATIVE_INFINITY)
				&& (cmtensor.m22 == Float.POSITIVE_INFINITY || cmtensor.m22 == Float.NEGATIVE_INFINITY))
			return;
		target.m00 += (cmtensor.m00 + (transY * transY
				+ transZ * transZ) * mass);
		target.m11 += (cmtensor.m11 + (transX * transX
				+ transZ * transZ) * mass);
		target.m22 += (cmtensor.m22 + (transX * transX
				+ transY * transY) * mass);
		
		target.m01 += (cmtensor.m01 - transX * transY * mass);
		target.m10 += (cmtensor.m10 - transX * transY * mass);
		target.m20 += (cmtensor.m20 - transX * transZ * mass);
		target.m02 += (cmtensor.m02 - transX * transZ * mass);
		target.m21 += (cmtensor.m21 - transY * transZ * mass);
		target.m12 += (cmtensor.m12 - transY * transZ * mass);
	}// end trasnlateInertiaTensor
	public final static strictfp
			void addTranslatedInertiaTensor(Matrix3d cmtensor,
					double transX, double transY, double transZ, double mass,
					Matrix3d target){
		target.m00 += (cmtensor.m00 + (transY * transY
				+ transZ * transZ) * mass);
		target.m11 += (cmtensor.m11 + (transX * transX
				+ transZ * transZ) * mass);
		target.m22 += (cmtensor.m22 + (transX * transX
				+ transY * transY) * mass);
		
		target.m01 += (cmtensor.m01 - transX * transY * mass);
		target.m10 += (cmtensor.m10 - transX * transY * mass);
		target.m20 += (cmtensor.m20 - transX * transZ * mass);
		target.m02 += (cmtensor.m02 - transX * transZ * mass);
		target.m21 += (cmtensor.m21 - transY * transZ * mass);
		target.m12 += (cmtensor.m12 - transY * transZ * mass);
		}// end trasnlateInertiaTensor
	public final static strictfp Matrix3f getRotatedInertiaTensor(
				Matrix3f tensor, 
				float m00, float m01, float m02,
				float m10, float m11, float m12,
				float m20, float m21, float m22,
				Matrix3f target){
		if(target == null)
			target = new Matrix3f();
		
		// target += R T R^T
		float t00 = m00 * tensor.m00 + m01 * tensor.m10 + m02 * tensor.m20,
				t01 = m00 * tensor.m01 + m01 * tensor.m11 + m02 * tensor.m21,
				t02 = m00 * tensor.m02 + m01 * tensor.m12 + m02 * tensor.m22;
		float t10 = m10 * tensor.m00 + m11 * tensor.m10 + m12 * tensor.m20,
				t11 = m10 * tensor.m01 + m11 * tensor.m11 + m12 * tensor.m21,
				t12 = m10 * tensor.m02 + m11 * tensor.m12 + m12 * tensor.m22;
		float t20 = m20 * tensor.m00 + m21 * tensor.m10 + m22 * tensor.m20,
				t21 = m20 * tensor.m01 + m21 * tensor.m11 + m22 * tensor.m21,
				t22 = m20 * tensor.m02 + m21 * tensor.m12 + m22 * tensor.m22;
		
		// m00 10 20
		// m01 11 21
		// m02 12 22
		target.m00 = t00 * m00 + t01 * m01 + t02 * m02;
		target.m01 = t00 * m10 + t01 * m11 + t02 * m12;
		target.m02 = t00 * m20 + t01 * m21 + t02 * m22;

		target.m10 = t10 * m00 + t11 * m01 + t12 * m02;
		target.m11 = t10 * m10 + t11 * m11 + t12 * m12;
		target.m12 = t10 * m20 + t11 * m21 + t12 * m22;

		target.m20 = t20 * m00 + t21 * m01 + t22 * m02;
		target.m21 = t20 * m10 + t21 * m11 + t22 * m12;
		target.m22 = t20 * m20 + t21 * m21 + t22 * m22;
		
		return target;
	}// end addRotatedInertiaTensor
	
	
	
	
	
	
	
	/* *****************************************
	 *              TRIM
	 ****************************************/
	public final static strictfp void trim(Matrix4f matrix){
		if(abs(matrix.m00) < FEPSILON)
			matrix.m00 = 0;
		if(abs(matrix.m01) < FEPSILON)
			matrix.m01 = 0;
		if(abs(matrix.m02) < FEPSILON)
			matrix.m02 = 0;
		if(abs(matrix.m03) < FEPSILON)
			matrix.m03 = 0;
		if(abs(matrix.m10) < FEPSILON)
			matrix.m10 = 0;
		if(abs(matrix.m11) < FEPSILON)
			matrix.m11 = 0;
		if(abs(matrix.m12) < FEPSILON)
			matrix.m12 = 0;
		if(abs(matrix.m13) < FEPSILON)
			matrix.m13 = 0;
		if(abs(matrix.m20) < FEPSILON)
			matrix.m20 = 0;
		if(abs(matrix.m21) < FEPSILON)
			matrix.m21 = 0;
		if(abs(matrix.m22) < FEPSILON)
			matrix.m22 = 0;
		if(abs(matrix.m23) < FEPSILON)
			matrix.m23 = 0;
		if(abs(matrix.m30) < FEPSILON)
			matrix.m30 = 0;
		if(abs(matrix.m31) < FEPSILON)
			matrix.m31 = 0;
		if(abs(matrix.m32) < FEPSILON)
			matrix.m32 = 0;
		if(abs(matrix.m33) < FEPSILON)
			matrix.m33 = 0;
	}// end trim(Matrix4f)
	public final static strictfp void trim(Matrix4d matrix){
		if(abs(matrix.m00) < FEPSILON)
			matrix.m00 = 0;
		if(abs(matrix.m01) < FEPSILON)
			matrix.m01 = 0;
		if(abs(matrix.m02) < FEPSILON)
			matrix.m02 = 0;
		if(abs(matrix.m03) < FEPSILON)
			matrix.m03 = 0;
		if(abs(matrix.m10) < FEPSILON)
			matrix.m10 = 0;
		if(abs(matrix.m11) < FEPSILON)
			matrix.m11 = 0;
		if(abs(matrix.m12) < FEPSILON)
			matrix.m12 = 0;
		if(abs(matrix.m13) < FEPSILON)
			matrix.m13 = 0;
		if(abs(matrix.m20) < FEPSILON)
			matrix.m20 = 0;
		if(abs(matrix.m21) < FEPSILON)
			matrix.m21 = 0;
		if(abs(matrix.m22) < FEPSILON)
			matrix.m22 = 0;
		if(abs(matrix.m23) < FEPSILON)
			matrix.m23 = 0;
		if(abs(matrix.m30) < FEPSILON)
			matrix.m30 = 0;
		if(abs(matrix.m31) < FEPSILON)
			matrix.m31 = 0;
		if(abs(matrix.m32) < FEPSILON)
			matrix.m32 = 0;
		if(abs(matrix.m33) < FEPSILON)
			matrix.m33 = 0;
	}// end trim(Matrix4f)
	public final static strictfp void trim(Matrix3f matrix){
		if(abs(matrix.m00) < FEPSILON)
			matrix.m00 = 0;
		if(abs(matrix.m01) < FEPSILON)
			matrix.m01 = 0;
		if(abs(matrix.m02) < FEPSILON)
			matrix.m02 = 0;
		if(abs(matrix.m10) < FEPSILON)
			matrix.m10 = 0;
		if(abs(matrix.m11) < FEPSILON)
			matrix.m11 = 0;
		if(abs(matrix.m12) < FEPSILON)
			matrix.m12 = 0;
		if(abs(matrix.m20) < FEPSILON)
			matrix.m20 = 0;
		if(abs(matrix.m21) < FEPSILON)
			matrix.m21 = 0;
		if(abs(matrix.m22) < FEPSILON)
			matrix.m22 = 0;
	}// end trim(Matrix3f)
	public final static strictfp void trim(Matrix3d matrix){
		if(abs(matrix.m00) < FEPSILON)
			matrix.m00 = 0;
		if(abs(matrix.m01) < FEPSILON)
			matrix.m01 = 0;
		if(abs(matrix.m02) < FEPSILON)
			matrix.m02 = 0;
		if(abs(matrix.m10) < FEPSILON)
			matrix.m10 = 0;
		if(abs(matrix.m11) < FEPSILON)
			matrix.m11 = 0;
		if(abs(matrix.m12) < FEPSILON)
			matrix.m12 = 0;
		if(abs(matrix.m20) < FEPSILON)
			matrix.m20 = 0;
		if(abs(matrix.m21) < FEPSILON)
			matrix.m21 = 0;
		if(abs(matrix.m22) < FEPSILON)
			matrix.m22 = 0;
	}// end trim(Matrix3f)
	public final static strictfp void trim(Vector3f vector){
		if(abs(vector.x) < FEPSILON)
			vector.x = 0;
		if(abs(vector.y) < FEPSILON)
			vector.y = 0;
		if(abs(vector.z) < FEPSILON)
			vector.z = 0;
	}// end trim(Matrix3f)
	public final static strictfp void trim(Point3f vector){
		if(abs(vector.x) < FEPSILON)
			vector.x = 0;
		if(abs(vector.y) < FEPSILON)
			vector.y = 0;
		if(abs(vector.z) < FEPSILON)
			vector.z = 0;
	}// end trim(Matrix3f)
	
	
	
	
	
	
	
	
	
	/* *******************************************
	 *       NORMAL GENERATION
	 ***************************************/
	public final static strictfp Vector3f[] generateNormals(Point3f[] vertices, int[][] faces, Vector3f[] normals){
		if(normals == null)
			normals = new Vector3f[faces.length];
		
		Vector3f a = new Vector3f(), b = new Vector3f();
		for(int i = 0; i < faces.length; i++){
			if(normals[i] == null)
				normals[i] = new Vector3f();
			else
				normals[i].set(0, 0, 0);
			
			for(int j = 0; j < faces[i].length; j++){
				MathUtility.minus(vertices[faces[i][(j + 1) % faces[i].length]],
						vertices[faces[i][j]],
						a);
				MathUtility.minus(vertices[faces[i][(j + 2) % faces[i].length]],
						vertices[faces[i][(j + 1) % faces[i].length]],
						b);
				a.cross(a, b);
				normals[i].add(a);
			}
			normals[i].normalize();
		}
		return normals;
	}// end generateNormals
	public final static strictfp Vector3d[] generateNormals(Point3d[] vertices, int[][] faces, Vector3d[] normals){
		if(normals == null)
			normals = new Vector3d[faces.length];
		
		Vector3d a = new Vector3d(), b = new Vector3d();
		for(int i = 0; i < faces.length; i++){
			if(normals[i] == null)
				normals[i] = new Vector3d();
			else
				normals[i].set(0, 0, 0);
			
			for(int j = 0; j < faces[i].length; j++){
				MathUtility.minus(vertices[faces[i][(j + 1) % faces[i].length]],
						vertices[faces[i][j]],
						a);
				MathUtility.minus(vertices[faces[i][(j + 2) % faces[i].length]],
						vertices[faces[i][(j + 1) % faces[i].length]],
						b);
				a.cross(a, b);
				normals[i].add(a);
			}
			normals[i].normalize();
		}
		return normals;
	}// end generateNormals
	public final static strictfp Vector3f generateNormal(float[] points, Vector3f output){
		if(output == null)
			output = new Vector3f(0, 0, 0);

		int len = points.length / 3;
		int a, b, c;
		float x = 0, y = 0, z = 0;
			
		int X = 0, Y = 1, Z = 2;
		for(int i = 3; i < len + 3; i++){
			// see dot product of normal and i-2, i-1, i
			a = ((i - 3) % len) * 3;
			b = ((i - 2) % len) * 3;
			c = ((i - 1) % len) * 3;
			
			/* (b-a) X (c-a) */
			// (B-A)y * (C-A)z - (B-A)z * (C-A)y
			x += (points[b+Y] - points[a+Y]) * (points[c+Z] - points[a+Z])
				- (points[b+Z] - points[a+Z]) * (points[c+Y] - points[a+Y]);
			// -((B-A)x * (C-A)z - (B-A)z * (C-A)x)
			y += -(points[b+X] - points[a+X]) * (points[c+Z] - points[a+Z])
				+ (points[b+Z] - points[a+Z]) * (points[c+X] - points[a+X]);
			// (B-A)x * (C-A)y - (B-A)y * (C-A)x
			z += (points[b+X] - points[a+X]) * (points[c+Y] - points[a+Y])
				- (points[b+Y] - points[a+Y]) * (points[c+X] - points[a+X]);
		}
		float dlen = (float)Math.sqrt(x * x + y * y + z * z);
		x /= dlen;
		y /= dlen;
		z /= dlen;
		output.x = x;
		output.y = y;
		output.z = z;
		return output;
	}// end generateNormal
	
	
	
	
	
	
	
	
	/* ******************************************
	 *       VOLUME, CM
	 ***************************************/
	public final static strictfp double getVolume(Point3d[] points,
			int[][] faces){
		double total = 0;
		Point3d a = null, b = null;
		Point3d s = null;
		double nx = 0, ny = 0, nz = 0;
		
		for(int i = 0; i < faces.length; i++){
			if(faces[i].length == 0) continue;
			
			nx = ny = nz = 0;
			s = points[faces[i][0]];
			for(int j = 1; j < faces[i].length - 1; j++){
				a = points[faces[i][j]];
				b = points[faces[i][j + 1]];
				nx += (a.y - s.y) * (b.z - s.z) - (a.z - s.z) * (b.y - s.y);
				ny += (a.z - s.z) * (b.x - s.x) - (a.x - s.x) * (b.z - s.z);
				nz += (a.x - s.x) * (b.y - s.y) - (a.y - s.y) * (b.x - s.x);
			}
			total += nx * s.x + ny * s.y + nz * s.z;
		}
		total /= 6.0;
		return total;
	}// end getVolume(Point3f[], int[][])
	public final static strictfp float getVolume(Point3f[] points,
			int[][] faces){
		float total = 0;
		Point3f a = null, b = null;
		Point3f s = null;
		float nx = 0, ny = 0, nz = 0;
		
		for(int i = 0; i < faces.length; i++){
			if(faces[i].length == 0) continue;
			
			nx = ny = nz = 0;
			s = points[faces[i][0]];
			for(int j = 1; j < faces[i].length - 1; j++){
				a = points[faces[i][j]];
				b = points[faces[i][j + 1]];
				nx += (a.y - s.y) * (b.z - s.z) - (a.z - s.z) * (b.y - s.y);
				ny += (a.z - s.z) * (b.x - s.x) - (a.x - s.x) * (b.z - s.z);
				nz += (a.x - s.x) * (b.y - s.y) - (a.y - s.y) * (b.x - s.x);
			}
			total += nx * s.x + ny * s.y + nz * s.z;
		}
		total /= 6.0;
		return total;
	}// end getVolume(Point3f[], int[][])
	public final static strictfp Point3f getCenterMassPoint(Point3f[] points, int[][] faces, Point3f result){
		if(result == null)
			result = new Point3f();
		
		float totalvolume = 0;
		float befx, befy, befz;
		float areax, areay, areaz;
		float x, y, z;
		float heightx, heighty, heightz;
		float segvolume;
		
		for(int i = 0; i < faces.length; i++){
			if(faces.length == 0)
				continue;
			
			// total cmp += cone cmp * cone volume
			// cone : O ~ faces[i][0], faces[i][j], faces[i][j + 1]
			befx = points[faces[i][1]].x - points[faces[i][0]].x;
			befy = points[faces[i][1]].y - points[faces[i][0]].y;
			befz = points[faces[i][1]].z - points[faces[i][0]].z;
			heightx = points[faces[i][0]].x;
			heighty = points[faces[i][0]].y;
			heightz = points[faces[i][0]].z;
			
			for(int j = 1; j < faces[i].length - 1; j++){
				// 0.5 (faces[j] - faces[0]) cross (faces[j + 1] - faces[0])
				// bef = faces[j] - faces[0]
				x = points[faces[i][j + 1]].x - points[faces[i][0]].x;
				y = points[faces[i][j + 1]].y - points[faces[i][0]].y;
				z = points[faces[i][j + 1]].z - points[faces[i][0]].z;
				areax = (befy * z - befz * y) / 2.0f;
				areay = -(befx * z - befz * x) / 2.0f;
				areaz = (befx * y - befy * x) / 2.0f;
				segvolume = (areax * heightx + areay * heighty + areaz * heightz) / 3.0f;

				result.x += segvolume
						* (points[faces[i][0]].x + points[faces[i][j]].x + points[faces[i][j + 1]].x) / 4.0f;
				result.y += segvolume
						* (points[faces[i][0]].y + points[faces[i][j]].y + points[faces[i][j + 1]].y) / 4.0f;
				result.z += segvolume 
						* (points[faces[i][0]].z + points[faces[i][j]].z + points[faces[i][j + 1]].z) / 4.0f;
				
				totalvolume += segvolume;
				befx = x; befy = y; befz = z;
			}
		}
		result.x /= totalvolume;
		result.y /= totalvolume;
		result.z /= totalvolume;
		
		return result;
	}// end getCenterMassPoint
	public final static strictfp Point3d getCenterMassPoint(Point3d[] points, int[][] faces, Point3d result){
		if(result == null)
			result = new Point3d();
		
		double totalvolume = 0;
		double befx, befy, befz;
		double areax, areay, areaz;
		double x, y, z;
		double heightx, heighty, heightz;
		double segvolume;
		
		for(int i = 0; i < faces.length; i++){
			if(faces.length == 0)
				continue;
			
			// total cmp += cone cmp * cone volume
			// cone : O ~ faces[i][0], faces[i][j], faces[i][j + 1]
			befx = points[faces[i][1]].x - points[faces[i][0]].x;
			befy = points[faces[i][1]].y - points[faces[i][0]].y;
			befz = points[faces[i][1]].z - points[faces[i][0]].z;
			heightx = points[faces[i][0]].x;
			heighty = points[faces[i][0]].y;
			heightz = points[faces[i][0]].z;
			
			for(int j = 1; j < faces[i].length - 1; j++){
				// 0.5 (faces[j] - faces[0]) cross (faces[j + 1] - faces[0])
				// bef = faces[j] - faces[0]
				x = points[faces[i][j + 1]].x - points[faces[i][0]].x;
				y = points[faces[i][j + 1]].y - points[faces[i][0]].y;
				z = points[faces[i][j + 1]].z - points[faces[i][0]].z;
				areax = (befy * z - befz * y) / 2.0f;
				areay = -(befx * z - befz * x) / 2.0f;
				areaz = (befx * y - befy * x) / 2.0f;
				segvolume = (areax * heightx + areay * heighty + areaz * heightz) / 3.0f;

				result.x += segvolume
						* (points[faces[i][0]].x + points[faces[i][j]].x + points[faces[i][j + 1]].x) / 4.0f;
				result.y += segvolume
						* (points[faces[i][0]].y + points[faces[i][j]].y + points[faces[i][j + 1]].y) / 4.0f;
				result.z += segvolume 
						* (points[faces[i][0]].z + points[faces[i][j]].z + points[faces[i][j + 1]].z) / 4.0f;
				
				totalvolume += segvolume;
				befx = x; befy = y; befz = z;
			}
		}
		result.x /= totalvolume;
		result.y /= totalvolume;
		result.z /= totalvolume;
		
		return result;
	}// end getCenterMassPoint
	
	
	
	
	
	
	
	/* *******************************************
	 *        TRIANGULATION
	 ********************************************/
	public final static int[] triangulate3d(float[] points)
			throws NotPlaneException{
		Vector3f normal = generateNormal(points, null);
		return triangulate3d(points, normal.x, normal.y, normal.z);
	}// end triangulate3d(float[])
	public final static int[] triangulate3d(float[] points,
			float normaltotalx, float normaltotaly, float normaltotalz)
			/*throws NotPlaneException*/{
		/*
		if(!checkPlane(points, normaltotalx, normaltotaly, normaltotalz)){
			System.out.println("MUtil : triangulate3d : points : ");
			for(int i = 0; i < points.length / 3; i++)
				System.out.println(points[i * 3] + " , " + points[i * 3 + 1] + " , " + points[i * 3 + 2]);
			throw new NotPlaneException();
		}*/
		int[] connection = new int[points.length / 3];
		for(int i = 0; i < connection.length; i++)
			connection[i] = i;
		//System.out.println("MUtil : triangulate3d : normal : " + normaltotalx
		//		+ " , " + normaltotaly + " , " + normaltotalz);
		return _triangulate3d(points, connection, connection.length,
				normaltotalx, normaltotaly, normaltotalz);
	}
	private static int[] _triangulate3d(float[] points,
			int[] connection, int connlen,
			final float normalx, final float normaly, final float normalz){
		//System.out.println("MUtil :      START _triangulate3d!");
		//System.out.print("MUtil : _triangulate3d : connection : ");
		//for(int k = 0; k < connlen; k++)
		//	System.out.print(connection[k] + " ");
		//System.out.println();
		if(connlen == 3){
			// 지금 연결된게 3개밖에 없삼
		//	System.out.println("MUtil : END(2) _tri3d");
			return new int[]{ connection[0], connection[1], connection[2] };
		}
		float line1x = 0, line1y = 0, line1z = 0;
		float line2x = 0, line2y = 0, line2z = 0;
		
		// n-2 ~ n-1번째 선분과 n-1 ~ n번째 선분을 두 변으로 하는 삼각형을 떼어낼 수 있는지를
		// 확인한다.
		
		float tmpx = 0, tmpy = 0, tmpz = 0;
		
		float n2px = 0, n2py = 0, n2pz = 0;
		float n1px = 0, n1py = 0, n1pz = 0;
		float npx = 0, npy = 0, npz = 0;
		
		int idx1 = 0, idx2 = 0, idx3 = 0;
		boolean containsPoint = false;
		float tmp1x, tmp1y, tmp1z;
		float tmp2x, tmp2y, tmp2z;
		float tmp3x, tmp3y, tmp3z;
		int i = 0;
		for(i = 2; i < connlen; i++){
			idx1 = connection[i - 2];
			idx2 = connection[i - 1];
			idx3 = connection[i];
			line1x = points[idx2 * 3] - points[idx1 * 3];
			line1y = points[idx2 * 3 + 1] - points[idx1 * 3 + 1];
			line1z = points[idx2 * 3 + 2] - points[idx1 * 3 + 2];
			line2x = points[idx3 * 3] - points[idx2 * 3];
			line2y = points[idx3 * 3 + 1] - points[idx2 * 3 + 1];
			line2z = points[idx3 * 3 + 2] - points[idx2 * 3 + 2];
			// 일단 n-2 ~ n-1 선분과 n ~ n-1 선분의 외적값이 음수인지를 확인함.
			
			tmpx = (line1y * line2z - line2y * line1z);
			tmpy = (line1z * line2x - line2z * line1x);
			tmpz = (line1x * line2y - line2x * line1y);
			//System.out.println("MUtil : _triangulate3d : " + i + " : tmpx, y, z : " + tmpx + " , " + tmpy + " , " + tmpz);
			//System.out.println("MUtil : _triangulate3d : " + i + " : line1 : " + line1x + " , " + line1y + " , " + line1z);
			//System.out.println("MUtil : _triangulate3d : " + i + " : line2 : " + line2x + " , " + line2y + " , " + line2z);
			if(normalx * tmpx + normaly * tmpy
					+ normalz * tmpz < 0)
				continue;
			// 이외에, 만약 만들어질 삼각형 넓이가 0인지를 확인.
			else if(tmpx * tmpx + tmpy * tmpy + tmpz * tmpz < FEPSILON)
				continue;
			
			// 이제 n - 2와 n의 vertex를 연결한다고 하자.
			// triangle n-2 ~ n-1 ~ n 안에 점이 있으면 무효.
			containsPoint = false;
			for(int j = 0; j < connlen; j++){
				if(j == i - 2 || j == i - 1 || j == i)
					continue;
				//System.out.println("j : " + j);
				// n-2 ~ n-1와 n-2 ~ p -> line1와 n2p,
				// n-1 ~ n와 n-1 ~ p -> line2와 n1p,
				// n ~ n-2와 n ~ p -> -line2-line1와 np
				// 각각의 외적값과 normal vector와의 내적이 0이거나 양수이면 contains Point.
				n2px = points[connection[j] * 3] - points[idx1 * 3];
				n2py = points[connection[j] * 3 + 1] - points[idx1 * 3 + 1];
				n2pz = points[connection[j] * 3 + 2] - points[idx1 * 3 + 2];
				
				n1px = points[connection[j] * 3] - points[idx2 * 3];
				n1py = points[connection[j] * 3 + 1] - points[idx2 * 3 + 1];
				n1pz = points[connection[j] * 3 + 2] - points[idx2 * 3 + 2];
				
				npx = points[connection[j] * 3] - points[idx3 * 3];
				npy = points[connection[j] * 3 + 1] - points[idx3 * 3 + 1];
				npz = points[connection[j] * 3 + 2] - points[idx3 * 3 + 2];

				tmp1x = line1y * n2pz - n2py * line1z;
				tmp1y = line1x * n2pz - n2px * line1z;
				tmp1z = line1x * n2py - n2px * line1y;
				//System.out.println(tmp1x + " , " + tmp1y + " , " + tmp1z);
				
				tmp2x = line2y * n1pz - n1py * line2z;
				tmp2y = line2x * n1pz - n1px * line2z;
				tmp2z = line2x * n1py - n1px * line2y;
				//System.out.println(tmp2x + " , " + tmp2y + " , " + tmp2z);
				
				tmp3x = (-line2y - line1y) * npz - npy * (-line2z - line1z);
				tmp3y = (-line2x - line1x) * npz - npx * (-line2z - line1z);
				tmp3z = (-line2x - line1x) * npy - npx * (-line2y - line1y);
				//System.out.println(tmp3x + " , " + tmp3y + " , " + tmp3z);
				
				if((tmp1x * tmp2x + tmp1y * tmp2y + tmp1z * tmp2z > 0) && 
						(tmp2x * tmp3x + tmp2y * tmp3y + tmp2z * tmp3z > 0) && 
						(tmp3x * tmp1x + tmp3y * tmp1y + tmp3z * tmp1z > 0)){
					containsPoint = true;
					j = 21474836;
					continue;
				}
			}
			if(!containsPoint){
				//System.out.println("MUtil : _triangulate3d : BREAK!");
				break;
			}
		}
		//System.out.println("MUtil : _triangulate3d : idx1, idx2, idx3 : " + idx1 + " , " + idx2 + " , " + idx3 + " , i : " + i);
		// connection[i - 1]에 있는 idx2 점을 뺀다.
		connection[i - 1] = 0;
		for(int j = i; j < connlen; j++)
			connection[j - 1] = connection[j];
		connection[connlen - 1] = 0;
		connlen--;
		
		int[] appendarray = _triangulate3d(points, connection, connlen,
				normalx, normaly, normalz);
		//System.out.print("MUtil : _tri3d : appendarray : ");
		//for(i = 0; i < appendarray.length; i++)
		//	System.out.print(appendarray[i] + " ");
		//System.out.println();
		int[] totalarray = new int[3 + appendarray.length];
		System.arraycopy(appendarray, 0, totalarray, 3, appendarray.length);
		totalarray[0] = idx1;
		totalarray[1] = idx2;
		totalarray[2] = idx3;
		//System.out.println("MUtil : END(1) _tri3d");
		return totalarray;
	}// end _triangulate3d(float[], int[], int, float, float, float)
	
	
	
	

	public final static strictfp boolean checkPlane(float[] points, float normalx, float normaly, float normalz, float EPSILON){
		int len = points.length / 3;
		int a, b, c;
		float x, y, z;
		float costheta;
		
		int X = 0, Y = 1, Z = 2;
		for(int i = 3; i < len + 3; i++){
			// see dot product of normal and i-2, i-1, i
			a = ((i - 3) % len) * 3;
			b = ((i - 2) % len) * 3;
			c = ((i - 1) % len) * 3;
			
			/* (b-a) X (c-a) */
			// (B-A)y * (C-A)z - (B-A)z * (C-A)y
			x = (points[b+Y] - points[a+Y]) * (points[c+Z] - points[a+Z])
				- (points[b+Z] - points[a+Z]) * (points[c+Y] - points[a+Y]);
			// -((B-A)x * (C-A)z - (B-A)z * (C-A)x)
			y = -(points[b+X] - points[a+X]) * (points[c+Z] - points[a+Z])
				+ (points[b+Z] - points[a+Z]) * (points[c+X] - points[a+X]);
			// (B-A)x * (C-A)y - (B-A)y * (C-A)x
			z = (points[b+X] - points[a+X]) * (points[c+Y] - points[a+Y])
				- (points[b+Y] - points[a+Y]) * (points[c+X] - points[a+X]);

			// 코사인값
			costheta = (float)Math.abs((x * normalx + y * normaly + z * normalz) / sqrt((x * x + y * y + z * z)));
			
			if(costheta < 1.0f - EPSILON){
				//System.out.println("MUtil : triangulate3d : a : " + points[a] + " , " + points[a + 1] + " , " + points[a + 2]);
				//System.out.println("MUtil : triangulate3d : b : " + points[b] + " , " + points[b + 1] + " , " + points[b + 2]);
				//System.out.println("MUtil : triangulate3d : c : " + points[c] + " , " + points[c + 1] + " , " + points[c + 2]);
				//System.out.println("MUtil : triangulate3d : d : " + points[d] + " , " + points[d + 1] + " , " + points[d + 2]);
				//System.out.println("MUtil : triangulate3d : 1 : " + x1 + " , " + y1 + " , " + z1);
				//System.out.println("MUtil : triangulate3d : 2 : " + x2 + " , " + y2 + " , " + z2);
				//System.out.println("MUtil : triangulate3d : costheta : " + costheta);
				return false;
			}
		}
		return true;
	}// end checkPlane
	public final static strictfp boolean checkPlane(float[] points, float EPSILON){
		Vector3f n = generateNormal(points, null);
		return checkPlane(points, n.x, n.y, n.z, EPSILON);
	}// end checkPlane(float[])
	
	
	
	
	
	
	
	
	
	
	
	/* ********************************************
	 *         CONVEX HULL
	 ******************************************/

	private static int store_right(int[] points, int psize, Node n){
		if(n == null) return psize;

		psize = store_right(points, psize, n.right);
		points[psize++] = (Integer)n.data;
		psize = store_right(points, psize, n.left);
		return psize;
	}// end store(Node)
	private static int store_left(int[] points, int psize, Node n){
		if(n == null) return psize;
		
		psize = store_left(points, psize, n.left);
		points[psize++] = (Integer)n.data;
		psize = store_left(points, psize, n.right);
		return psize;
	}// end store(Node)
	private static Polygon createPolygon(Point[] pList, 
			BinaryTree tree, int stp, int edp){
		int[] points = new int[tree.size() + 2];
		int psize = 0;
		
		points[psize++] = stp;
		psize = store_left(points, psize, tree.root.left);
		points[psize++] = edp;
		psize = store_right(points, psize, tree.root.right);
		
		Polygon polygon = new Polygon();
		for(int i = 0; i < psize; i++){
			//System.out.println(points[i]);
			polygon.addPoint(pList[points[i]].x, pList[points[i]].y);
		}
		return polygon;
	}// end createPointIndices(BinaryTree)
	public final static strictfp Polygon convexHull(Point[] points){
		if(points == null) return new Polygon();
		else if(points.length == 0) return new Polygon();
		
		// ready
		boolean[] visited = new boolean[points.length];
		
		// 거리가 가장 먼 두 점을 가져온다.
		int stpoint = 0, edpoint = 0;
		float sqLen = -1.0f;
		float tmpval = 0;
		for(int i = 0; i < points.length; i++){
			for(int j = i + 1; j < points.length; j++){
				tmpval = (points[i].x - points[j].x) * (points[i].x - points[j].x) +
					(points[i].y - points[j].y) * (points[i].y - points[j].y);
				if(tmpval > sqLen){
					sqLen = tmpval;
					stpoint = i;
					edpoint = j;
				}
			}
		}/* <= 있어야 할까?
		if(points[stpoint].x > points[edpoint].x){
			int tmp = stpoint;
			stpoint = edpoint;
			edpoint = tmp;
		}*/
		visited[stpoint] = visited[edpoint] = true;
		
		//System.out.println("st : " + stpoint + " , ed : " + edpoint);
		//System.out.println("stpoint : " + stpoint + " , edp : " + edpoint);
		
		vector2f centerLine = new vector2f(points[stpoint],
				points[edpoint]);
		vector2f cwdir = new vector2f(centerLine.y, -1.0f * centerLine.x);
		vector2f ccwdir = new vector2f(-1.0f * centerLine.y, centerLine.x);
		
		BinaryTree tree = new BinaryTree();
		Node focus = new Node();
		tree.root = focus;
		
		buildConvexHull(points, stpoint, edpoint, cwdir,
				true, tree, focus, visited);
		buildConvexHull(points, stpoint, edpoint, ccwdir,
				false, tree, focus, visited);
		
		//printTree(tree);
		
		return createPolygon(points, tree, stpoint, edpoint);
	}// end convexHull()
	private static strictfp void buildConvexHull(Point[] points, int stpoint, int edpoint,
			vector2f direction, boolean isout, BinaryTree tree,
			Node focus, boolean[] visited){
		//System.out.println("\nbCH : stp : " + stpoint + " , edp : " + edpoint + " , dir : " + direction + " , iscw : " + isout);
		vector2f pos, projection;
		
		vector2f max = null;
		int maxidx = -1;
		//float directionSqrdSize = direction.sqrdSize();
		//float posSqrdSize;
		for(int i = 0; i < points.length; i++){
			if(visited[i]) continue;
			
			pos = new vector2f(points[stpoint], points[i]);
			//posSqrdSize = pos.sqrdSize();
			projection = direction.proj(pos);
			
			if(pos.dot(direction) < 0)
				continue;
			if(max == null){
				max = projection;
				maxidx = i;
			}else if(max.sqrdSize() < projection.sqrdSize()){
				max = projection;
				maxidx = i;
			}
		}
		if(maxidx == -1){
			//System.out.println("END bCH(1) : stp : " + stpoint + " , edp : " + edpoint + "\n");
			return;
		}else if(max.sqrdSize() < 1){
			//System.out.println("END bCH(2) : stp : " + stpoint + " , edp : " + edpoint + "\n");
			return;
		}
		//System.out.println("maxidx : " + maxidx + " , max : " + max);
		
		visited[maxidx] = true;
		
		// 새로 찾은 점을 바탕으로 두 변을 새로 만든다 : 왼쪽 변, 오른쪽 변(점을 기준으로)
		vector2f left = new vector2f(points[stpoint], points[maxidx]),
			right = new vector2f(points[maxidx], points[edpoint]);
		//System.out.println("left : " + left + " , right : " + right);
		
		
		Node n = new Node();
		n.data = maxidx;
		if(isout)
			focus.left = n;
		else
			focus.right = n;
		focus = n;

		vector2f leftcw = new vector2f(left.y, left.x * -1.0f);
		vector2f rightcw = new vector2f(right.y, right.x * -1.0f);
		
		boolean lcw = true, rcw = true;
		//System.out.println("leftcw : " + leftcw + " , rightcw : " + rightcw);
		if(leftcw.dot(max) < 0)
			lcw = false;
		if(rightcw.dot(max) < 0)
			rcw = false;
		
		
		//System.out.println("lcw : " + lcw + " , rcw : " + rcw);
		//System.out.println("call rccw from " + stpoint + " , " + edpoint);
		
		if(lcw){
			buildConvexHull(points, stpoint, maxidx, 
					leftcw, true, tree, focus, visited);
		}else{
			buildConvexHull(points, stpoint, maxidx, 
					new vector2f(left.y * -1.0f, left.x),
					true, tree, focus, visited);
		}if(rcw){
			buildConvexHull(points, maxidx, edpoint,
					rightcw, false, tree, focus, visited);
		}else{;
			buildConvexHull(points, maxidx, edpoint, 
					new vector2f(right.y * -1.0f, right.x),
					false, tree, focus, visited);
		}
		//System.out.println("END bCH : stp : " + stpoint + " , edp : " + edpoint);
		//System.out.println();
	}// end build(Point[], int, int, vector2f, boolean, BinaryTree, Node)
	private void printNode(Node n){
		if(n == null)
			return;
		
		System.out.print("(");
		if(n.left != null)
			printNode(n.left);
		System.out.print(n.data);
		if(n.right != null)
			printNode(n.right);
		System.out.print(")");
	}// end printNode(Node)
	private void printTree(BinaryTree tree)
	{ printNode(tree.root); System.out.println(); }
	
	
	
	
	
	
	/* *********************************************
	 *           POLYGON EXPANSION
	 ****************************************** */
	public static strictfp Polygon expand(Polygon polygon, float range){
		if(polygon == null) return null;
		range = -1.0f * range;
		
		Polygon newp = new Polygon();
		int cnt = polygon.npoints;
		for(int i = cnt; i < cnt * 2; i++){
			vector2f precede = new vector2f(polygon.xpoints[(i - 1) % cnt] - polygon.xpoints[i % cnt],
					polygon.ypoints[(i - 1) % cnt] - polygon.ypoints[i % cnt]);
			vector2f following = new vector2f(polygon.xpoints[(i + 1) % cnt] - polygon.xpoints[i % cnt],
					polygon.ypoints[(i + 1) % cnt] - polygon.ypoints[i % cnt]);
			precede = precede.hat();
			following = following.hat(); 
			
			float sin2theta = Math.abs(following.crossSize(precede));
			//float cos2theta = following.dot(precede);
			//System.out.println("sin2theta : " + sin2theta + " , cos2theta : " + cos2theta);
			float sintheta = (float)Math.sqrt((1.0 + Math.sqrt(1.0 - sin2theta * sin2theta)) / 2.0);
			
			vector2f pointer = null;
			if(sintheta == 0){
				pointer = precede.plus(following).hat().mul(range);
			}else
				pointer = precede.plus(following).hat().mul(range / sintheta);
			if(pointer.x > 0)
				pointer.x += 0.0000000000001;
			else
				pointer.x += -0.0000000000001;
			if(pointer.y > 0)
				pointer.y += 0.0000000000001;
			else
				pointer.y += -0.0000000000001;
			//System.out.println(i + " : " + "pointer : " + pointer);
			newp.addPoint((int)pointer.x + polygon.xpoints[i % cnt], (int)pointer.y + polygon.ypoints[i % cnt]);
		}
		return newp;
	}// end expand(Polygon, float)
	
	
	

	
	
	
	
	
	
	
	
	static class vector2f{
		float x, y;
		vector2f(float x, float y)
		{ this.x = x; this.y = y; }
		vector2f(Point st, Point ed)
		{ this.x = ed.x - st.x; this.y = ed.y - st.y; }
		
		strictfp float size()
		{ return (float)Math.sqrt(sqrdSize()); }
		strictfp float sqrdSize()
		{ return x * x + y * y; }
		
		strictfp float dot(vector2f v){
			float a = v.x * x, b = v.y * y;
			return a + b;
		}// end dot(vector2f)
		strictfp float crossSize(vector2f v){
			float a = x * v.y;
			float b = y * v.x;
			return a + b;
		}// end crossSize(vector2f)
		strictfp vector2f mul(float d)
		{ return new vector2f(x * d, y * d); }
		strictfp vector2f plus(vector2f v)
		{ return new vector2f(x + v.x, y + v.y); }
		strictfp vector2f proj(vector2f v){
			return mul(dot(v) / sqrdSize());
		}// end proj(vector2f)
		strictfp vector2f hat()
		{ return mul(1.0f / size()); }
		
		public String toString()
		{ return "(" + x + "," + y + ")"; }
	}// end vector2f
	
	
	
	
	
	
	
	
	public static void main(String[] argv) throws Exception{/*
		Point3f[] points = new Point3f[8];/*
		points[0] = new Point3f(-1.5f, -2.5f,  -0.5f);
		points[1] = new Point3f(-1.5f, -2.5f,   0.5f);
		points[2] = new Point3f(-1.5f,  2.5f,  -0.5f);
		points[3] = new Point3f(-1.5f,  2.5f,   0.5f);
		points[4] = new Point3f( 1.5f, -2.5f,  -0.5f);
		points[5] = new Point3f( 1.5f, -2.5f,   0.5f);
		points[6] = new Point3f( 1.5f,  2.5f,  -0.5f);
		points[7] = new Point3f( 1.5f,  2.5f,   0.5f);
		points[0] = new Point3f(1, 1, 1);
		points[1] = new Point3f(1, 1, 2);
		points[2] = new Point3f(1, 2, 1);
		points[3] = new Point3f(1, 2, 2);
		points[4] = new Point3f(2, 1, 1);
		points[5] = new Point3f(2, 1, 2);
		points[6] = new Point3f(2, 2, 1);
		points[7] = new Point3f(2, 2, 2);
		
		int[][] f = new int[6][4];
		f[0][0] = 1; f[0][1] = 5; f[0][2] = 6; f[0][3] = 2;
		f[1][0] = 5; f[1][1] = 7; f[1][2] = 8; f[1][3] = 6;
		f[2][0] = 2; f[2][1] = 6; f[2][2] = 8; f[2][3] = 4;
		f[3][0] = 1; f[3][1] = 3; f[3][2] = 7; f[3][3] = 5;
		f[4][0] = 4; f[4][1] = 3; f[4][2] = 1; f[4][3] = 2;
		f[5][0] = 8; f[5][1] = 7; f[5][2] = 3; f[5][3] = 4;

		Matrix3f tensor = new Matrix3f();
		MathUtility.addInertiaTensor(points, f, 2.0f, 0, 0, 0, tensor);
		System.out.println(tensor);
		System.out.println(getVolume(points, f));
		
		
		
		Point3f b = new Point3f(1, 1, 1);
		Point3f a = new Point3f(1, 1, 0);
		Point3f c = new Point3f(0, 1, 0);
		float xy = 0;
	
		//System.out.println(xy);
		

		// data1 : 8, 0의 순서가 뒤바꼈는지는 모르겠으나, 5, 3, 2 또는
		// 6, 7의 나열 순서 바뀜
		Point[] data1 = new Point[]{
				new Point(28 , 0),
				new Point(0 , 14),
				new Point(-29 , 0),
				new Point(-15 , -8),
				new Point(0 , 0),
				new Point(14 , -8),
				new Point(28 , 17),
				new Point(0 , 31),
				new Point(-29 , 17),
				new Point(-15 , 10),
				new Point(0 , 17),
				new Point(14 , 10)
		};
		Point[] data2 = new Point[]{
				new Point(2, 4),
				new Point(3, 2),
				new Point(-1, 2),
				new Point(-4, 1),
				new Point(-1, -1),
				new Point(-1, -5),
				new Point(6, 0),
				new Point(2, -2),
				new Point(2, -7),
				new Point(-3, -7)
		};
		Polygon p = MathUtility.convexHull(data2);*/
		/*
		float[] triangulate_testcase1 = new float[]{
				0, 0, 0,
				0, 0, 1,
				1, 0, 1,
				1, 0, 0
		};
		int[] array = MathUtility.triangulate3d(triangulate_testcase1);
		for(int i = 0; i < array.length; i++)
			System.out.print(array[i] + " ");
		System.out.println();
		float[] triangulate_testcase12 = new float[]{
				0, 0, 0,
				0, 0, 1,
				0, 1, 1,
				0, 1, 0
		};
		int[] array2 = MathUtility.triangulate3d(triangulate_testcase12);
		for(int i = 0; i < array2.length; i++)
			System.out.print(array2[i] + " ");
		System.out.println();
		*/
		/*
		float[] triangulate_testcase2 = new float[]{
				0, 0, 0,
				3, 0, 0,
				3, 3, 0,
				0, 3, 0,
				0, 0, 0, 
				1, 1, 0,
				1, 2, 0,
				2, 2, 0,
				2, 1, 0,
				1, 1, 0
		};
		int[] array = MathUtility.triangulate3d(triangulate_testcase2);
		for(int i = 0; i < array.length; i++)
			System.out.print(array[i] + " ");*/
		/*float[] triangulate_testcase3 = new float[]{
				0, 0, 0,
				3, 0, 6,
				3, 3, 6,
				0, 3, 0,
				0, 0, 0, 
				1, 1, 2,
				1, 2, 2,
				2, 2, 4,
				2, 1, 4,
				1, 1, 1
		};
		int[] array = MathUtility.triangulate3d(triangulate_testcase3);
		for(int i = 0; i < array.length; i++)
			System.out.print(array[i] + " ");
		*/
		
		/*
		float[][] I = new float[][]{
				{2, 0},
				{0, 2}
		};
		float r2 = Math.sqrt(2);
		float[][] n = new float[][]{
				{1.0 / r2, 1.0 / r2},
				{-1, 0},
				{0, -1}
		};
		float[][] p = new float[][]{
				{1, 3},
				{1, 1},
				{3, 1}
		};
		
		float[][] n_1 = new float[n.length][n[0].length];
		float[][] n_T = new float[n[0].length][n.length];
		MathUtility.transpose(n, n_T);
		MathUtility.mul(n, -1, n_1);
		
		float[][] zero = new float[3][3];

		float[] Q = new float[2 + 3];
		Q[0] = -1;
		Q[1] = -5;
		for(int i = 0; i < n.length; i++)
			Q[i + 2] = n[i][0] * p[i][0] + n[i][1] * p[i][1];
		
		
		float[][] M = MathUtility
				.block_quad(I,
						n_T,
						n_1,
						zero);
		float[] W = new float[5];
		float[] Z = new float[5];
		MathUtility.LCPSolve(5, M, Q, Z, W);
		System.out.printf("%f %f, \n", Z[0], Z[1]);
		*/
		/*
		float[][] M = new float[][]{
				{0, 0, -1, 2, 3},
				{0, 0, 1, -1, 1},
				{1, -1, 0, 0, 0},
				{-2, 1, 0, 0, 0},
				{-3, -1, 0, 0, 0}
		};
		float[] W=new float[5], Z = new float[5];
		float[] Q = new float[]{
				-1, -1, 2, -1, 3
		};*/
		/*
		float[][] M = new float[][]{
				{2, 0, 1},
				{0, 2, 1},
				{-1, -1, 0},
		};
		float[] W=new float[3], Z = new float[3];
		float[] Q = new float[]{
				-2, -4, 1
		};
		System.out.println(MathUtility.LCPSolve(3, M, Q, Z, W));
		System.out.printf("sol : \nw : %f %f %f\n, z : %f %f %f\n", W[0], W[1], W[2], Z[0], Z[1], Z[2]);
		*/
	}// end main(String[])
}
