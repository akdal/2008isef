package agdr.library.math;

/**
 * MathUtility triangulate3dǿ  : ; triangulationϷ
 *  Ҷ ̴ exceptionԴϴ.
 * @author ؿ
 *
 */
public class NotPlaneException extends Exception{
	public NotPlaneException(){}
	public NotPlaneException(String message)
	{ super(message); }
	public NotPlaneException(String message, Throwable cause)
	{ super(message, cause); }
	public NotPlaneException(Throwable cause)
	{ super(cause); }
}
