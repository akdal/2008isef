package agdr.robodari.plugin.rae.comp;

import javax.vecmath.*;

import agdr.robodari.appl.*;
import agdr.library.math.MathUtility;
import agdr.robodari.comp.*;
import agdr.robodari.plugin.rme.rdpi.*;
import agdr.robodari.plugin.rme.rlztion.*;

public class Indicator extends VirtualComponent implements MergingObserver, Dynamic{
	public final static long serialVersionUID = 8284057877459766838L;
	
	private transient Matrix4d reltrans = null;
	private transient Matrix3d relrottrans = null;
	private transient RealizationMap.Key key;

	public void initRlztion(RealizationModel rmodel)
	{ this.reltrans = null; this.key = null; }
	public void resetRlztion(){}
	public void disposeRlztion()
	{ this.reltrans = null; this.key = null; }
	
	public void merged(RealizationMap.Key key, Matrix4d reltrans)
	{
		this.key = key; this.reltrans = reltrans;
		
		Matrix3d newrel = new Matrix3d();
		newrel.m00 = reltrans.m00; 
		newrel.m01 = reltrans.m01; 
		newrel.m02 = reltrans.m02;
		newrel.m10 = reltrans.m10; 
		newrel.m11 = reltrans.m11; 
		newrel.m12 = reltrans.m12;
		newrel.m20 = reltrans.m20; 
		newrel.m21 = reltrans.m21; 
		newrel.m22 = reltrans.m22; 
		
		if(this.relrottrans == null)
			this.relrottrans = newrel;
		else{
			newrel.mul(this.relrottrans);
			this.relrottrans = newrel;
		}
	}
	
	public RealizationMap.Key getKey()
	{ return key; }
	public void setKey(RealizationMap.Key key)
	{ this.key = key; }
	
	public void getTransform(RealizationModel rmodel, Matrix4d m4f){
		int idx = rmodel.getRealizationMap().getIndex(this.key);
		Matrix4d trans = new Matrix4d();
		
		rmodel.getWorld().getTransform(idx, trans);
		if(reltrans == null)
			m4f.set(trans);
		else
			MathUtility.fastMul(trans, reltrans, m4f);
	}// end getTransform
	public void getDirection(RealizationModel rmodel, Vector3d dir){
		//BugManager.printf("Indicator : %s : key : %s, \n", this, this.key);
		int idx = rmodel.getRealizationMap().getIndex(this.key);
		Matrix3d m3f = new Matrix3d();
		
		rmodel.getWorld().getRotMatrix(idx, m3f);
		if(reltrans != null)
			m3f.mul(this.relrottrans);
		dir.x = m3f.m00;
		dir.y = m3f.m10;
		dir.z = m3f.m20;
		//System.out.printf("\tgetDirection : this.relrottrans : \n%s\n, trans : \n%s", this, relrottrans, m3f);
	}// end getDirection
}
