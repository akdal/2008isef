package agdr.robodari.plugin.rae.comp;

import java.io.*;
import javax.vecmath.*;
import agdr.robodari.comp.model.*;

public class IndicatorModelGenerator implements ModelGenerator<Indicator>{
	final static String code = new StringBuffer("v 3.3 -0.1 0\n")
			.append("v 3.1 0.1 0\n")
			.append("v 2.6 0.6 0\n")
			.append("v 2.4 0.8 0\n")
			.append("v 2.4 0.1 0\n")
			.append("v -0.1 0.1 0\n")
			.append("v -0.1 -0.1 0\n")
			.append("v 2.4 -0.1 0\n")
			.append("v 2.4 -0.6 0\n")
			.append("v 2.6 -0.6 0\n")
			.append("v 2.6 -0.1 0\n")
			.append("v 2.6 0.1 0\n")
			.append("v 2.6 0.4 0\n")
			.append("v 2.9 0.1 0\n")
			.append("f 1 2 6 7\n")
			.append("f 3 4 5 8 9 10 11 12 13\n")
			.append("f 2 3 13 14\n")
			.append("f 7 6 2 1\n")
			.append("f 13 12 11 10 9 8 5 4 3\n")
			.append("f  14 13 3 2\n").toString();
	
	public RobotCompModel generate(Indicator model) throws Exception{
		RobotCompModel rcm = new RobotCompModel();
		StringReader sr = new StringReader(code);
		rcm.load(sr);
		
		PhysicalData pd = rcm.getPhysicalData();
		pd.mass = 0;
		pd.inertia = new Matrix3f();
		pd.volume = 0;
		pd.bounds = null;
		pd.centerMassPoint = new Point3f(0, 0, 0);
		
		AppearanceData ad = rcm.getApprData();
		ad.setColor(java.awt.Color.red);
		
		return rcm;
	}// end generat
}
