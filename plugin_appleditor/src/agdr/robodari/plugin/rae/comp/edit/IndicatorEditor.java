package agdr.robodari.plugin.rae.comp.edit;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Frame;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

import agdr.robodari.appl.BugManager;
import agdr.robodari.comp.Note;
import agdr.robodari.gui.*;
import agdr.robodari.plugin.rae.comp.*;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.gui.*;

public class IndicatorEditor extends CompEditor{
	private JTabbedPane tabbedPane;
	
	private JPanel propertyPanel;
	private NoteEditor noteEditor;
	private JTextField nameField;
	
	private SkObject object;
	

	public IndicatorEditor(SketchEditorInterface manager, Dialog parent){
		super(manager, parent, "Indicator Editor");
		__set();
	}// end Constructor(EditorManager)
	public IndicatorEditor(SketchEditorInterface manager, Frame parent){
		super(manager, parent, "Indicator Editor");
		__set();
	}// end Constructor(EditorManager)
	private void __set(){
		initPropertyPanel();
		tabbedPane = new JTabbedPane();
		tabbedPane.setTabPlacement(JTabbedPane.LEFT);
		tabbedPane.addTab("Properties", propertyPanel);
		
		super.getEditorPanel().setLayout(new BorderLayout());
		super.getEditorPanel().add(tabbedPane, BorderLayout.CENTER);
		
		super.setSize(480, 430);
	}// end __set
	private void initPropertyPanel(){
		propertyPanel = new JPanel();
		propertyPanel.setLayout(new BorderLayout());
		
		JPanel northPanel = new JPanel(new BorderLayout());
		{
			nameField = new JTextField();
			JLabel nameLabel = new JLabel("Component name : ");
			northPanel.add(nameLabel, BorderLayout.WEST);
			northPanel.add(nameField, BorderLayout.CENTER);
		}
		
		noteEditor = new NoteEditor();
		
		propertyPanel.add(northPanel, BorderLayout.NORTH);
		propertyPanel.add(noteEditor, BorderLayout.CENTER);
	}// end initPropertyPanel()
	
	
	public void startEditing(SkObject object){
		if(!(object.getRobotComponent() instanceof Indicator))
			return;
		
		this.object = object;

		Indicator rcomp = (Indicator)object.getRobotComponent();
		nameField.setText(rcomp.getName());
		if(rcomp.getNote() == null)
			rcomp.setNote(new Note());
		noteEditor.open(rcomp.getNote());
	}// end startEditing(SkObject)
	public void finishEditing(){
		Indicator rcomp = (Indicator)object.getRobotComponent();
		noteEditor.saveNote();
		rcomp.setNote(noteEditor.getNote());
		rcomp.setName(nameField.getText());
	}// end finishEditing()
	public SkObject getObject()
	{ return object; }
}
