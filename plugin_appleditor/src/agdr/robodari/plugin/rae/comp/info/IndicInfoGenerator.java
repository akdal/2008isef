package agdr.robodari.plugin.rae.comp.info;

import static agdr.robodari.comp.info.InfoKeys.*;
import agdr.robodari.plugin.defcomps.*;
import agdr.robodari.plugin.rae.comp.Indicator;
import agdr.robodari.comp.info.*;

public class IndicInfoGenerator implements InfoGenerator<Indicator>{
	public Info generateInfo(Indicator ti){
		Info info = new Info(ti);
		info.put(NAME, "Indicator");
		info.put(BUSINESS, "");
		info.put(WEIGHT, new Float(0));
		info.put(ISOUTEROBJECT, false);
		return info;
	}// end generate
}
