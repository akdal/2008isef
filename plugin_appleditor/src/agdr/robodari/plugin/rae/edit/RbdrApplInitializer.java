package agdr.robodari.plugin.rae.edit;

import agdr.robodari.comp.*;
import agdr.robodari.store.*;
import agdr.robodari.plugin.rae.comp.*;
import agdr.robodari.plugin.rae.comp.edit.*;
import agdr.robodari.plugin.rae.comp.info.*;
import agdr.robodari.plugin.rae.rlztion.*;
import agdr.robodari.plugin.rme.store.*;

public class RbdrApplInitializer extends Initializer{
	public void initialize(){
		RobotComponentStore.registerComponent(Indicator.class);
		RobotModelStore.registerModel(Indicator.class, new IndicatorModelGenerator());
		RobotInfoStore.registerInfo(Indicator.class, new IndicInfoGenerator());
		RobotCompEditorStore.register(Indicator.class, IndicatorEditor.class);
		
		RealizationCompStore.registerGraphGenerator(IndicGraphGenerator.INSTANCE);
	}// end initialize()
}
