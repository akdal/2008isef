package agdr.robodari.plugin.rae.edit;

import java.io.*;
import java.util.*;
import javax.vecmath.*;


import agdr.robodari.appl.*;
import agdr.robodari.comp.*;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.store.ModelNotRegisteredException;

public class RbdrApplication extends SketchModel{
	public final static long serialVersionUID = 100;
	
	public final static int SRCTYPE_PRJFILE = 2;
	public final static int SRCTYPE_FILE = 1;
	public final static int SRCTYPE_URL = 0;
	public final static int SRCTYPE_NULL = -1;
	
	private Hashtable<SkObject, Boolean> gravityEffected = new Hashtable<SkObject, Boolean>(); 
	private String serverClassName;
	private File serverSrc;
	private String adminClassName;
	private File administratorSrc;
	private String codegenCode;
	private String codegenOutput;
	private double coeffOfRestt = 1.0;
	

	
	/**
	 * Overrided methods
	 */
	public void addRobotComponent(RobotComponent rc, Matrix4f trans)
			throws ModelNotRegisteredException, NullPointerException,
			Exception{
		SkObject obj = new SkObject(rc, trans);
		addObject(obj);
	}// end addRobotComponent(RobotComponent, Matrix4f)

	public void addObject(SkObject obj){
		if(obj == null) return;
		
		RobotComponent rc = obj.getRobotComponent();
		if(rc != null){
			if(!(rc instanceof StructureComponent || rc instanceof VirtualComponent)){
				return;
			}
		}else if(!(obj instanceof RobotObject) && !obj.isGroup())
			return;
		
		this.setGravityEffection(obj, Boolean.TRUE);
		super.addObject(obj);
	}// end addObject(SkObject)
	
	
	public SourceEditable getStarter()
	{ return null; }
	public void setStarter(SourceEditable se){}
	
	
	
	public boolean isGravityEffectionSpecified(SkObject obj)
	{ return gravityEffected.containsKey(obj); }
	public boolean gravityEffected(SkObject obj)
	{ return gravityEffected.get(obj); }
	public void setGravityEffection(SkObject obj, Boolean b)
	{ gravityEffected.put(obj, b); }
	public double getCoeffOfRestt()
	{ return this.coeffOfRestt; }
	public void setCoeffOfRestt(double d)
	{ this.coeffOfRestt = d; }
	

	public void readExternal(ObjectInput input) throws ClassNotFoundException, ClassCastException, IOException{
		super.readExternal(input);
		this.serverClassName = (String)input.readObject();
		String src = (String)input.readObject();
		if(src == null)
			this.serverSrc = null;
		else
			this.serverSrc = new File(src);
		this.adminClassName = (String)input.readObject();
		src = (String)input.readObject();
		if(src == null)
			this.administratorSrc = null;
		else
			this.administratorSrc = new File(src);
		
		this.gravityEffected = (Hashtable)input.readObject();
		this.codegenCode = (String)input.readObject();
		this.codegenOutput = (String)input.readObject();
	}// end readExternal(OBjectinput)
	public void writeExternal(ObjectOutput output) throws IOException{
		super.writeExternal(output);
		output.writeObject(this.serverClassName);
		if(this.serverSrc == null)
			output.writeObject((String)null);
		else
			output.writeObject(this.serverSrc.getAbsolutePath());
		output.writeObject(this.adminClassName);
		if(this.administratorSrc == null)
			output.writeObject((String)null);
		else
			output.writeObject(this.administratorSrc.getAbsolutePath());
		
		output.writeObject(gravityEffected);
		output.writeObject(codegenCode);
		output.writeObject(codegenOutput);
	}// end readExternal(OBjectinput)

	
	public String getCodegenCode()
	{ return codegenCode; }
	public void setCodegenCode(String c)
	{ this.codegenCode = c; }
	public String getCodegenOutput()
	{ return codegenOutput; }
	public void setCodegenOutput(String o)
	{ this.codegenOutput = o; }
	
	public String getServerClassName()
	{ return serverClassName; }
	public void setServerClassName(String cn)
	{ this.serverClassName = cn; }
	public String getAdminClassName()
	{ return adminClassName; }
	public void setAdminClassName(String c)
	{ this.adminClassName = c; }
	public File getServerSrc() {
		return serverSrc;
	}
	public void setServerSrc(File serverSrc) {
		this.serverSrc = serverSrc;
	}
	public File getAdministratorSrc() {
		return administratorSrc;
	}
	public void setAdministratorSrc(File administratorSrc) {
		this.administratorSrc = administratorSrc;
	}
}
