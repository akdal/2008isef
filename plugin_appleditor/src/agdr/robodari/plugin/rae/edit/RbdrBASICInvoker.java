package agdr.robodari.plugin.rae.edit;

import java.io.*;
import agdr.library.lang.*;
import agdr.robodari.appl.BugManager;
import agdr.robodari.plugin.rae.rapi.*;
import agdr.robodari.plugin.rae.lang.*;
import agdr.robodari.plugin.rae.lang.parser.*;

public class RbdrBASICInvoker implements CommandInvoker, FunctionInvoker, ConstantPool{
	private final static String LINESEPARATOR = System.getProperty("line.separator");
	
	private LearningEngine engine;
	
	public RbdrBASICInvoker(LearningEngine engine){
		this.engine = engine;
	}// end Constructor
	
	
	
	/* *********************************************
	 *                 CommandInvoker impl
	 **********************************************/
	public final static String ERRCNT_CMD_UNKNWNCMD = "Unknown command : ";
	
	public final static String CMD_LOG = "log";
	public final static String CMD_LOGLN = "logln";
	public final static String CMD_PRINT = "print";
	public final static String CMD_PRINTLN = "println";
	public final static String CMD_END = "end";
	
	
	public boolean isCommand(String cmd){
		cmd = cmd.toLowerCase();
		if(CMD_PRINT.equals(cmd))
			return true;
		else if(CMD_END.equals(cmd))
			return true;
		else if(CMD_PRINTLN.equals(cmd))
			return true;
		else if(CMD_LOG.equals(cmd))
			return true;
		else if(CMD_LOGLN.equals(cmd))
			return true;
		return false;
	}// end isCommand
	public ResultType invokeCmd(CommandStatement source, String cmd, Valuable[] args, ErrorBundle bundle, BASICRunner runner){
		cmd = cmd.toLowerCase();
		
		if(CMD_PRINT.equals(cmd) || CMD_PRINTLN.equals(cmd) || CMD_LOG.equals(cmd) || CMD_LOGLN.equals(cmd)){
			StringBuffer buf = new StringBuffer();
			Receptor receptor = new Receptor();
			receptor.bundle = bundle;
			ResultType res;

			for(int i = 0; i < args.length; i++){
				res = runner.getValue(source, args[i], receptor);
				if(res != ResultType.DONE)
					return res;
				buf.append(receptor.value);
			}
			if(CMD_PRINTLN.equals(cmd) || CMD_LOGLN.equals(cmd))
				buf.append(LINESEPARATOR);
			
			try{
				agdr.library.lang.Console console = runner.getConsole();
				BufferedWriter writer = null;
				
				if(CMD_LOGLN.equals(cmd) || CMD_LOG.equals(cmd))
					writer = new BufferedWriter(console.getLog());
				else
					writer = new BufferedWriter(new OutputStreamWriter(console.getOut()));
				
				writer.write(buf.toString());
				writer.flush();
			}catch(IOException ioe){
				BugManager.log(ioe, true);
			}
			return ResultType.DONE;
		}else if(CMD_END.equals(cmd)){
			return ResultType.EXITED;
		}
		bundle.addErrorMessage(new ErrorMessage(source.getParent(),
				source,
				source.getCodeRange(),
				ERRCNT_CMD_UNKNWNCMD + cmd,
				ErrorTypes.RUNTIME_ERROR));
		return ResultType.RUNTIME_ERROR;
	}// end invokeCmd
	

	
	
	/* *********************************************
	 *               FunctionInvoker impl
	 **********************************************/
	
	public final static String ERRCNT_FUNC_UNKNOWNF = "Unknown function.";
	public final static String ERRCNT_FUNC_WRONGARGS = "Wrong parameters.";
	// substring(string, int, int) function
		public final static String ERRCNT_FUNC_SUBSTRING_ARG1_TYPE = "Arg 1 of SUBSTRING should be string type.";
		public final static String ERRCNT_FUNC_SUBSTRING_ARG2_TYPE = "Arg 2 of SUBSTRING should be integer type.";
		public final static String ERRCNT_FUNC_SUBSTRING_ARG2 = "Wrong begin index : begin index < 0 or STRLEN < begin index.";
		public final static String ERRCNT_FUNC_SUBSTRING_ARG3_TYPE = "Arg 3 of SUBSTRING should be integer type.";
		public final static String ERRCNT_FUNC_SUBSTRING_ARG3 = "Wrong end index : end index < 0 or STRLEN(str) < end index. ";
	// strlen(string) function
		public final static String ERRCNT_FUNC_STRLEN_ARG1_TYPE = "Arg 1 of SUBSTRING should be string type.";
	// not(boolean) function
		public final static String ERRCNT_FUNC_NOT_ARG1_TYPE = "Arg 1 of NOT should be boolean type";
	// getstatecount(int) function
		public final static String ERRCNT_FUNC_GETSTATECOUNT_ARG1_TYPE = "Entity index should be integer type.";
		public final static String ERRCNT_FUNC_GETSTATECOUNT_ARG1 = "Wrong entity index : index < 0 or ENTITYCOUNT <= index";
	// getaction(int, int) function
		public final static String ERRCNT_FUNC_GETACTION_ARG1_TYPE = "Entity index should be integer type.";
		public final static String ERRCNT_FUNC_GETACTION_ARG1 = "Wrong entity index : index < 0 or ENTITYCOUNT <= index";
		public final static String ERRCNT_FUNC_GETACTION_ARG2_TYPE = "State index should be integer type.";
		public final static String ERRCNT_FUNC_GETACTION_ARG2 = "Wrong state index : index < 0 or STATECOUNT(entityIndex) <= index";
	// getProperty(String) function
		public final static String ERRCNT_FUNC_GETPROPERTY_ARG1_TYPE = "Arg 1 of SUBSTRING should be string type.";
	// getentitycount() fjunction 
		
	
	public final static String FUNC_SUBSTRING = "substring";
	public final static String FUNC_STRLEN = "strlen";
	public final static String FUNC_NOT = "not";
	public final static String FUNC_GETSTATECOUNT = "getstatecount";
	public final static String FUNC_GETACTION = "getaction";
	public final static String FUNC_GETPROPERTY = "getproperty";
	public final static String FUNC_GETENTITYCOUNT = "getentitycount";
	
	
	public boolean isFunction(String f){
		f = f.toLowerCase();
		
		if(FUNC_SUBSTRING.equals(f))
			return true;
		else if(FUNC_STRLEN.equals(f))
			return true;
		else if(FUNC_GETSTATECOUNT.equals(f))
			return true;
		else if(FUNC_NOT.equals(f))
			return true;
		else if(FUNC_GETACTION.equals(f))
			return true;
		else if(FUNC_GETPROPERTY.equals(f))
			return true;
		else if(FUNC_GETENTITYCOUNT.equals(f))
			return true;
		
		return false;
	}// end isFunction
	public ResultType invokeFunction(CodeTrackable focus, FunctionCallValue fcv,
			String f, Valuable[] args, Receptor rec, BASICRunner brun){
		f = f.toLowerCase();
		ResultType res;
		
		if(FUNC_SUBSTRING.equals(f)){
			// substring(string, int, int)
			if(args.length != 3){
				rec.bundle.addErrorMessage(new ErrorMessage(focus, fcv, fcv.getCodeRange(),
						ERRCNT_FUNC_WRONGARGS, ErrorTypes.RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			
			Object objTarg, objStIdx, objEdIdx;
			String szTarg;
			int iStIdx, iEdIdx;
			
			res = brun.getValue(fcv, args[0], rec);
			if(res != ResultType.DONE)
				return res;
			else if(rec.type != ValueType.TYPE_STRING){
				rec.bundle.addErrorMessage(new ErrorMessage(focus, fcv, fcv.getCodeRange(),
						ERRCNT_FUNC_SUBSTRING_ARG1_TYPE, ErrorTypes.RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			objTarg = rec.value;
			
			res = brun.getValue(fcv, args[1], rec);
			if(res != ResultType.DONE)
				return res;
			else if(rec.type != ValueType.TYPE_INT){
				rec.bundle.addErrorMessage(new ErrorMessage(focus, fcv, fcv.getCodeRange(),
						ERRCNT_FUNC_SUBSTRING_ARG2_TYPE, ErrorTypes.RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			objStIdx = rec.value;
			
			res = brun.getValue(fcv, args[2], rec);
			if(res != ResultType.DONE)
				return res;
			else if(rec.type != ValueType.TYPE_INT){
				rec.bundle.addErrorMessage(new ErrorMessage(focus, fcv, fcv.getCodeRange(),
						ERRCNT_FUNC_SUBSTRING_ARG3_TYPE, ErrorTypes.RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			objEdIdx = rec.value;
			
			
			szTarg = (String)objTarg;
			iStIdx = (Integer)objStIdx;
			iEdIdx = (Integer)objEdIdx;
			
			if(iEdIdx >= szTarg.length() || 0 < iEdIdx){
				rec.bundle.addErrorMessage(new ErrorMessage(focus, fcv, fcv.getCodeRange(),
						ERRCNT_FUNC_SUBSTRING_ARG3, ErrorTypes.RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}else if(iStIdx < 0 || szTarg.length() <= iStIdx){
				rec.bundle.addErrorMessage(new ErrorMessage(focus, fcv, fcv.getCodeRange(),
						ERRCNT_FUNC_SUBSTRING_ARG2, ErrorTypes.RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			
			rec.type = ValueType.TYPE_STRING;
			rec.value = szTarg.substring(iStIdx, iEdIdx);
			return ResultType.DONE;
			
			
			
		
		}else if(FUNC_NOT.equals(f)){
			if(args.length != 1){
				rec.bundle.addErrorMessage(new ErrorMessage(focus, fcv, fcv.getCodeRange(),
						ERRCNT_FUNC_WRONGARGS, ErrorTypes.RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}

			Object objval;
			boolean bobjval;
			res = brun.getValue(fcv, args[0], rec);
			if(res != ResultType.DONE)
				return res;
			else if(rec.type != ValueType.TYPE_BOOLEAN){
				rec.bundle.addErrorMessage(new ErrorMessage(focus, fcv, fcv.getCodeRange(),
						ERRCNT_FUNC_NOT_ARG1_TYPE, ErrorTypes.RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			objval = rec.value;
			bobjval = (Boolean)objval;

			rec.type = ValueType.TYPE_BOOLEAN;
			rec.value = !bobjval ? Boolean.TRUE : Boolean.FALSE;
			return ResultType.DONE;
			
			
			
		}else if(FUNC_GETSTATECOUNT.equals(f)){
			// statecount(int)
			if(args.length != 1){
				rec.bundle.addErrorMessage(new ErrorMessage(focus, fcv, fcv.getCodeRange(),
						ERRCNT_FUNC_WRONGARGS, ErrorTypes.RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			
			Object objEntIdx;
			int iEntIdx;
			res = brun.getValue(fcv, args[0], rec);
			if(res != ResultType.DONE)
				return res;
			else if(rec.type != ValueType.TYPE_INT){
				rec.bundle.addErrorMessage(new ErrorMessage(focus, fcv, fcv.getCodeRange(),
						ERRCNT_FUNC_GETSTATECOUNT_ARG1_TYPE, ErrorTypes.RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			objEntIdx = rec.value;
			iEntIdx = (Integer)objEntIdx;
			
			int entCount = 0;
			if(engine != null)
				entCount = engine.entityCount();
			
			if(iEntIdx < 0 || entCount <= iEntIdx){
				rec.bundle.addErrorMessage(new ErrorMessage(focus, fcv, fcv.getCodeRange(),
						ERRCNT_FUNC_GETSTATECOUNT_ARG1, ErrorTypes.RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			
			rec.type = ValueType.TYPE_INT;
			rec.value = engine == null ? new Integer(0) : engine.stateCount(iEntIdx);
			return ResultType.DONE;
			
			
			
		}else if(FUNC_STRLEN.equals(f)){
			// strlen(str)
			if(args.length != 1){
				rec.bundle.addErrorMessage(new ErrorMessage(focus, fcv, fcv.getCodeRange(),
						ERRCNT_FUNC_WRONGARGS, ErrorTypes.RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			
			Object objStr;
			String szStr;
			res = brun.getValue(fcv, args[0], rec);
			if(res != ResultType.DONE)
				return res;
			else if(rec.type != ValueType.TYPE_STRING){
				rec.bundle.addErrorMessage(new ErrorMessage(focus, fcv, fcv.getCodeRange(),
						ERRCNT_FUNC_STRLEN_ARG1_TYPE, ErrorTypes.RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			objStr = rec.value;
			szStr = objStr.toString();
			
			rec.type = ValueType.TYPE_INT;
			rec.value = szStr.length();
			return ResultType.DONE;

			
			
		}else if(FUNC_GETPROPERTY.equals(f)){
			// getproperty(str)
			if(args.length != 1){
				rec.bundle.addErrorMessage(new ErrorMessage(focus, fcv, fcv.getCodeRange(),
						ERRCNT_FUNC_WRONGARGS, ErrorTypes.RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			
			Object objKey;
			String szKey;
			res = brun.getValue(fcv, args[0], rec);
			if(res != ResultType.DONE)
				return res;
			else if(rec.type != ValueType.TYPE_STRING){
				rec.bundle.addErrorMessage(new ErrorMessage(focus, fcv, fcv.getCodeRange(),
						ERRCNT_FUNC_GETPROPERTY_ARG1_TYPE, ErrorTypes.RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			objKey = rec.value;
			szKey = objKey.toString();
			
			rec.type = ValueType.TYPE_STRING;
			rec.value = engine == null ? "" : engine.getProperty(szKey);
			return ResultType.DONE;
			
			
			
		}else if(FUNC_GETACTION.equals(f)){
			// getaction(int, int)
			if(args.length != 2){
				rec.bundle.addErrorMessage(new ErrorMessage(focus, fcv, fcv.getCodeRange(),
						ERRCNT_FUNC_WRONGARGS, ErrorTypes.RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			
			Object objEntIdx, objStateIdx;
			int iEntIdx, iStateIdx;
			
			res = brun.getValue(fcv, args[0], rec);
			if(res != ResultType.DONE)
				return res;
			else if(rec.type != ValueType.TYPE_INT){
				rec.bundle.addErrorMessage(new ErrorMessage(focus, fcv, fcv.getCodeRange(),
						ERRCNT_FUNC_GETACTION_ARG1_TYPE, ErrorTypes.RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			objEntIdx = rec.value;
			iEntIdx = (Integer)objEntIdx;
			res = brun.getValue(fcv, args[1], rec);
			if(res != ResultType.DONE)
				return res;
			else if(rec.type != ValueType.TYPE_INT){
				rec.bundle.addErrorMessage(new ErrorMessage(focus, fcv, fcv.getCodeRange(),
						ERRCNT_FUNC_GETACTION_ARG2_TYPE, ErrorTypes.RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			objStateIdx = rec.value;
			iStateIdx = (Integer)objEntIdx;
			
			int entCount = 0, stCount = 0;
			if(this.engine != null){
				entCount = this.engine.entityCount();
				stCount = this.engine.stateCount(iEntIdx);
			}
			
			if(iEntIdx < 0 || entCount <= iEntIdx){
				rec.bundle.addErrorMessage(new ErrorMessage(focus, fcv, fcv.getCodeRange(),
						ERRCNT_FUNC_GETACTION_ARG1, ErrorTypes.RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}else if(iStateIdx < 0 || stCount <= iStateIdx){
				rec.bundle.addErrorMessage(new ErrorMessage(focus, fcv, fcv.getCodeRange(),
						ERRCNT_FUNC_GETACTION_ARG2, ErrorTypes.RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			
			rec.type = ValueType.TYPE_INT;
			rec.value = engine == null ? new Integer(-1) : engine.getAction(iEntIdx, iStateIdx);
			return ResultType.DONE;
			
			
		}else if(FUNC_GETENTITYCOUNT.equals(f)){
			if(args.length != 0){
				rec.bundle.addErrorMessage(new ErrorMessage(focus, fcv, fcv.getCodeRange(),
						ERRCNT_FUNC_WRONGARGS, ErrorTypes.RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			
			rec.type = ValueType.TYPE_INT;
			rec.value = engine == null ? 0 : engine.entityCount();
			return ResultType.DONE;
			
		}
		
		
		rec.bundle.addErrorMessage(new ErrorMessage(focus, fcv, fcv.getCodeRange(),
				ERRCNT_FUNC_UNKNOWNF, ErrorTypes.RUNTIME_ERROR));
		return ResultType.RUNTIME_ERROR;
	}// end invokeFunction
	

	
	
	/* *********************************************
	 *               ConstantPool impl
	 **********************************************/
	
	
	public boolean isConstant(String identf){
		identf = identf.toLowerCase();
		
		return false;
	}// end isConstant
	public Value getConstValue(String identf){
		identf = identf.toLowerCase();
		
		return null;
	}// end getConstVAlue
}
