package agdr.robodari.plugin.rae.edit;

import java.awt.Color;
import java.io.*;

import javax.vecmath.*;

import java.util.*;


import agdr.robodari.appl.BugManager;
import agdr.robodari.comp.*;
import agdr.robodari.comp.model.RobotCompModel;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.rlztion.*;

public class RobotObject extends SkObject implements Externalizable{
	private final static RobotCompModel[] NULLSTARRAY = new RobotCompModel[0];
	
	private RobotModel robotModel;
	private String robotName;
	
	
	public RobotObject(){}
	public RobotObject(RobotModel model, Matrix4f trans){
		this.robotModel = model;
		super.setTransform(trans);
		
		refresh();
	}// end Constructor()
	

	
	public String getRobotName()
	{ return getGroupName(); }
	public void setRobotName(String rname)
	{ super.setGroupName(rname); }
	
	public void refresh(){
		SkObject[] objs = robotModel.getSketchModel().getObjects();
		for(SkObject eachobj : objs)
			super.addChild(eachobj);
	}// end generateStructures(Structure[])
	
	public RobotModel getRobotModel()
	{ return robotModel; }
	
	public boolean isGroup()
	{ return true; }
	public void setGroup(boolean b){}
	
	public String toString()
	{ return robotModel.getSketchModel().getName(); }
	

	public void writeExternal(ObjectOutput output) throws IOException{
		output.writeObject(this.robotModel);
		output.writeObject(super.getTransform());
		output.writeObject(this.getRobotName());
	}// end writeExternal(ObjectOutput)
	public void readExternal(ObjectInput input) throws IOException, ClassNotFoundException{
		this.robotModel = (RobotModel)input.readObject();
		super.setTransform((Matrix4f)input.readObject());
		this.setRobotName((String)input.readObject());
		refresh();
	}// end readExternal(ObjectINput)
}
