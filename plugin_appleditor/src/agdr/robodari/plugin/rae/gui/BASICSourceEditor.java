package agdr.robodari.plugin.rae.gui;

import javax.swing.*;
import javax.swing.text.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.undo.*;
import java.io.StringReader;
import java.io.IOException;
import agdr.robodari.plugin.rae.lang.*;
import agdr.robodari.plugin.rae.lang.parser.*;


public class BASICSourceEditor extends JTextPane implements DocumentListener{
	private final static StyleContext PALETTE = createPalette();
	private final static Font FONT = createFont();
	
	private final static String STYLE_COMMAND = "command";
	private final static String STYLE_TEXT = "text";
	private final static String STYLE_COMMENT_SIGN = "comment_sign";
	private final static String STYLE_COMMENT = "comment";
	private final static String STYLE_CONSTANT = "constant";
	private final static String STYLE_STRING = "string";
	private final static String STYLE_NUMBER = "number";
	private final static String STYLE_FUNCTION = "function";
	private final static String STYLE_OPERATOR = "operator";
	private final static String STYLE_RESERVED = "reserved";
	private final static String STYLE_PARENTHESIS = "parenthesis";
	
	
	private final static StyleContext createPalette(){
		StyleContext sc = new StyleContext();
		Style cmdStyle = sc.addStyle(STYLE_COMMAND, null);
		StyleConstants.setForeground(cmdStyle, Color.BLUE);

		Style textStyle = sc.addStyle(STYLE_TEXT, null);

		
		Style commentSignStyle = sc.addStyle(STYLE_COMMENT_SIGN, null);
		StyleConstants.setForeground(commentSignStyle, new Color(85, 147, 85));
		Style commentStyle = sc.addStyle(STYLE_COMMENT, null);
		StyleConstants.setForeground(commentStyle, new Color(85, 147, 85));
		
		Style constantStyle = sc.addStyle(STYLE_CONSTANT, null);
		StyleConstants.setForeground(constantStyle, new Color(85, 147, 85));

		Style string = sc.addStyle(STYLE_STRING, null);
		StyleConstants.setForeground(string, Color.BLUE);

		Style numberStyle = sc.addStyle(STYLE_NUMBER, null);
		StyleConstants.setForeground(numberStyle, Color.BLACK);

		Style funcStyle = sc.addStyle(STYLE_FUNCTION, null);
		StyleConstants.setForeground(funcStyle, Color.RED);
		
		Style opStyle = sc.addStyle(STYLE_OPERATOR, null);
		StyleConstants.setForeground(opStyle, new Color(255, 192, 64));

		Style resStyle = sc.addStyle(STYLE_RESERVED, null);
		StyleConstants.setForeground(resStyle, new Color(0, 0, 128));

		Style prthsisStyle = sc.addStyle(STYLE_PARENTHESIS, null);
		StyleConstants.setForeground(prthsisStyle, Color.black);
		StyleConstants.setBold(prthsisStyle, true);
		
		return sc;
	}// end createPalette()
	private final static Font createFont(){
		return new Font("Courier New", 0, 12);
	}// end createFont()
	

	
	
	
	private StyledDocument document;
	private DataFetcher fetcher;
	private TextRenderer renderer;
	
	private CommandInvoker cmdInvoker;
	private FunctionInvoker funcInvoker;
	private ConstantPool constPool;
	
	private UndoManager undoManager;
	
	
	public BASICSourceEditor(){
		super.setPreferredSize(new Dimension(2000, 100));
		this.document = new SrcStyledDocument(PALETTE);
		this.document.addDocumentListener(this);
		
		this.undoManager = new UndoManager();
		this.document.addUndoableEditListener(this.undoManager);
		
		super.setDocument(document);
		super.setFont(FONT);
		
		this.fetcher = new DataFetcher(document);
		this.renderer = new TextRenderer(document, fetcher);
	}// end Constructor(CodeHighlighter)
	
	
	
	public void setInvokers(CommandInvoker cmdInvoker, FunctionInvoker funcInvoker, ConstantPool cp){
		this.cmdInvoker = cmdInvoker;
		this.funcInvoker = funcInvoker;
		this.constPool = cp;
	}// end setInvokers
	
	
	private final Object _lock = new Object();
	
	public boolean getScrollableTracksViewportWidth()
	{ return true; }
	public void changedUpdate(DocumentEvent de){
	}// end changedUpdate(DocumentEvent)
	public void insertUpdate(DocumentEvent de){
		if(renderer.working) return;
		synchronized(_lock){
			try{
				String str = document.getText(de.getOffset(), de.getLength());
				if(str.equals("\n") || str.equals("\r\n")){
					javax.swing.text.Element parelem = document.getParagraphElement(de.getOffset());
					String paragraph = document.getText(parelem.getStartOffset(), parelem.getEndOffset() - parelem.getStartOffset());
					int indentidx = 0;
					StringBuffer buf = new StringBuffer();
					
					while(paragraph.charAt(indentidx++) == '\t')
						buf.append('\t');
				}
			}catch(BadLocationException ble){}
		
			updateHighlight(de.getOffset(), de.getOffset() + de.getLength());
		}
	}// end insertUpdate(DocuemtnEvent)
	public void removeUpdate(DocumentEvent de){
		if(renderer.working) return;
		synchronized(_lock){
			updateHighlight(de.getOffset(), de.getOffset() + de.getLength());
		}
	}// end removeUpdate(DocumentEvent)
	
	
	
	
	private void updateHighlight(int start, int end){
		try{
			document.render(fetcher);
			synchronized(fetcher){
				while(!fetcher.done){
					fetcher.wait();
				}
			}
			renderer.start = start;
			renderer.end = end;
			Thread thr = new Thread(renderer);
			thr.start();
		}catch(InterruptedException ie){
			ie.printStackTrace();
		}
	}// end updateHighlight
	
	
	public void undo()
	{ try{ this.undoManager.undo(); }catch(Exception e){} }
	public void redo()
	{ try{ this.undoManager.redo(); }catch(Exception e){} }
	
	
	
	public static void main(String[] argv){
		JFrame jfr = new JFrame("jf");
		jfr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jfr.setSize(400, 400);
		
		jfr.setLayout(new BorderLayout());
		jfr.add(new JScrollPane(new BASICSourceEditor()));
		jfr.setVisible(true);
	}// end main
	
	
	
	

	
	
	class DataFetcher implements Runnable{
		boolean done;
		javax.swing.text.Element root;
		String content;
		
		private Document doc;
		
		public DataFetcher(Document d){
			this.doc = d;
		}// end Con
		
		public void run(){
			done = false;
			try{
				root = doc.getRootElements()[0];
				content = doc.getText(root.getStartOffset(), root.getEndOffset());
			}catch(Exception excp){
				excp.printStackTrace();
			}
			done = true;
		}// end run()
	}// end class DataFetcher
	class TextRenderer implements Runnable{
		private StyledDocument document;
		private DataFetcher fetcher;
		
		public int start, end;
		public boolean working = false;
		
		
		public TextRenderer(StyledDocument d, DataFetcher df){
			this.document = d;
			this.fetcher = df;
		}// end Cons
		
		public void run(){
			working = true;
			try{
				javax.swing.text.Element rootelem = fetcher.root;
				javax.swing.text.Element paragraph;
				boolean flag = false;
	
				LinkedList<Token> queue = new LinkedList<Token>();
				StringReader reader = null;
				// 하나씩 뽑아서 색깔을 입히자. 여기서는 텍스쳐라고 표현.
				// startidx, endidx, Style
				int texture_stidx = 0, texture_edidx = 0;
				Style texture = null;
				int totalszlen = 0;
				String szparagraph;
				
				for(int i = 0; i < rootelem.getElementCount(); i++){
					// 해당 라인에 위치하는 paragraph element를 뽑는다.
					paragraph = rootelem.getElement(i);
					flag = (end <= paragraph.getStartOffset()) || (paragraph.getEndOffset() <= start);
					if(flag)
						continue;
	
					queue.clear();
					szparagraph = document.getText(paragraph.getStartOffset(),
							paragraph.getEndOffset() - paragraph.getStartOffset());
					totalszlen = paragraph.getStartOffset();
					reader = new StringReader(szparagraph);
					BASICTokenizer.tokenize(queue, reader);
					
					/*
					 * Parser에서 적용시킨 원리를 여기서도 적용.
					 * action이,
					 * -1일 때 : 아무거나.
					 * 0일 때 : 문자열 내용.
					 * 1일 때 : 큰따옴표. <- deprecated
					 * 2일 때 : 주석 내용.
					 */
					int action = -1;
					
					while(queue.size() != 0){
						String elem = queue.poll().toString();
						int elemlen = elem.length();
						totalszlen += elemlen;
						if(elem.trim().length() == 0) continue;
						
						if(action == 0){
							// 문자열.
							texture_edidx = totalszlen;
							action = 0;
							if(elem.equals("\"")){
								applyHighlight(texture_stidx, texture_edidx, texture);
								action = -1;
							}
							
						}else if(action == 2){
							// 주석.
							texture_edidx = totalszlen;
							
						}else if(action == -1){
							if(texture != null)
								applyHighlight(texture_stidx, texture_edidx, texture);
							
							if(elem.equals("\"")){
								// 문자열 시작!
								action = 0;
								
								texture_stidx = totalszlen - 1;
								texture_edidx = totalszlen;
								texture = document.getStyle(STYLE_STRING);
							}else if(elem.equals("\'")){
								// 주석 시작!
								action = 2;
								
								texture_stidx = totalszlen - 1;
								texture_edidx = totalszlen;
								texture = document.getStyle(STYLE_COMMENT);
							}else if(elem.equals("(") || elem.equals(")")){
								applyHighlight(totalszlen - 1, totalszlen, document.getStyle(STYLE_PARENTHESIS));
							}else if(BASICGrammar.isReserved(elem)){
								applyHighlight(totalszlen - elemlen, totalszlen, document.getStyle(STYLE_RESERVED));
							}else if(cmdInvoker != null && cmdInvoker.isCommand(elem)){
								applyHighlight(totalszlen - elemlen, totalszlen, document.getStyle(STYLE_COMMAND));
							}else if(funcInvoker != null && funcInvoker.isFunction(elem)){
								applyHighlight(totalszlen - elemlen, totalszlen, document.getStyle(STYLE_FUNCTION));
							}else if(Operator.isOperator(elem)){
								applyHighlight(totalszlen - elemlen, totalszlen, document.getStyle(STYLE_OPERATOR));
							}else if(BASICGrammar.isDigitSet(elem)){
								applyHighlight(totalszlen - elemlen, totalszlen, document.getStyle(STYLE_NUMBER));
								continue;
							}else if(constPool != null && constPool.isConstant(elem)){
								applyHighlight(totalszlen - elemlen, totalszlen, document.getStyle(STYLE_CONSTANT));
								continue;
							}else{
								// 아무것도 아니다.
								applyHighlight(totalszlen - elemlen, totalszlen, document.getStyle(STYLE_TEXT));
								continue;
							}
						}
					}
					applyHighlight(texture_stidx, texture_edidx, texture);
				}
			}catch(BadLocationException ble){
				ble.printStackTrace();
				System.err.println("robobasic code highlighter에서 : 코드 위치가 잘못됨");
			}catch(IOException ioe){
				ioe.printStackTrace();
			}
			working = false;
		}// end run(
		private void applyHighlight(int startidx, int endidx, Style style){
			document.setCharacterAttributes(startidx, endidx - startidx, style, true);
		}// end appylHightlight
	}// end class TextRenderer
	
	class SrcStyledDocument extends DefaultStyledDocument{
		public SrcStyledDocument(StyleContext sc)
		{ super(sc); }
  		public void insertString(int off, String sz, AttributeSet as) throws BadLocationException{
  			super.insertString(off, sz, as);
  			if(sz.equals("\n") || sz.equals("\r\n")){
  	  			javax.swing.text.Element parelem = super.getParagraphElement(off - 1);
  	  			String szContent = super.getText(parelem.getStartOffset(), parelem.getEndOffset() - parelem.getStartOffset());
  	  			
  	  			int tcount = 0;
  	  			int i = 0;
  	  			while(szContent.charAt(i++) == '\t') tcount++;
  	  			
  	  			StringBuffer buf = new StringBuffer();
  	  			while(0 != tcount){
  	  				buf.append("\t");
  	  				tcount--;
  	  			}
 				super.insertString(off + 1, 
  	  						buf.toString(), null);
  			}
		}// end insertString(int, String, AttributeSet)
	}
}
