package agdr.robodari.plugin.rae.gui;

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.text.*;
import javax.swing.border.*;
import agdr.library.lang.*;
import agdr.robodari.plugin.rae.rapi.*;
import agdr.robodari.plugin.rae.edit.*;
import agdr.robodari.plugin.rae.lang.*;
import agdr.robodari.plugin.rae.lang.parser.*;


public abstract class CodeGenPanel extends JPanel implements ActionListener,
		agdr.library.lang.Console, Runnable, MouseListener{
	private final static String LINESEPARATOR = System.getProperty("line.separator");
	
	private JMenuBar menuBar;
		private JMenu functionsMenu;
		private JMenu commandsMenu;
	
	private BASICRunner basicRunner;
	private BASICParser basicParser;
	private RbdrBASICInvoker basicInvoker;
	
	
	private JPanel centerPanel;
		private BASICSourceEditor sourceEditor;
		private JPanel c_southPanel;
			private JTabbedPane tabbedPane;
				private JList errList;
				private DefaultListModel errListModel;
				private JTextArea logArea;
			private JPanel c_s_eastPanel;
				private JButton runButton;
				private JButton haltButton;
	private JPanel southPanel;
		private JPanel s_northPanel;
			private JTextField pathField;
			private JButton pathButton;
		private JLabel statusLabel;
		private JPanel s_southPanel;
			private JButton saveButton;
			private JButton cancelButton;		
			
			
	// for Console
	private Writer logWriter;
	private OutputStream outputStream;
	// final인 이유 : 별 이유 없음, 여기서 null값이 변하지 않음을 미리 명시.
	private final InputStream inputStream = null;
	
	
		
	
	public CodeGenPanel(){
		_initcomp();
		_initmenu();
		
		logWriter = new Writer(){
			Document doc;
			
			{
				doc = logArea.getDocument();
			}// end Constructor
			
			public void flush(){}
			public void close(){}
			public void write(char[] chr, int off, int len){
				try{
					System.out.printf("CodeGenPanel : logWriter : write() CALLED!\n");
					doc.insertString(doc.getLength(), new String(chr, off, len), null);
				}catch(BadLocationException ble){
					agdr.robodari.appl.BugManager.log(ble, true);
				}
			}// end write(char[], int, int)
		};
		basicParser = new BASICParser();
		basicRunner = new BASICRunner();
	}// end Constructor
	private void _initcomp(){
		super.setLayout(new BorderLayout());
		
		centerPanel = new JPanel(new BorderLayout());
		{
			sourceEditor = new BASICSourceEditor();
			c_southPanel = new JPanel(new BorderLayout());
			{
				tabbedPane = new JTabbedPane();
				{
					errList = new JList();
					errListModel = new DefaultListModel();
					errList.setModel(errListModel);
					errList.addMouseListener(this);
					
					logArea = new JTextArea();
					logArea.setEditable(false);
					JScrollPane laScrPane = new JScrollPane(logArea, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
					laScrPane.setPreferredSize(new Dimension(300, 150));
					tabbedPane.addTab("Errors", new JScrollPane(errList));
					tabbedPane.addTab("Log", laScrPane);
				}
				c_s_eastPanel = new JPanel(new GridLayout(3, 0, 4, 4));
				{
					runButton = new JButton("Run");
					runButton.addActionListener(this);
					haltButton = new JButton("Stop");
					haltButton.addActionListener(this);
					
					c_s_eastPanel.add(runButton);
					c_s_eastPanel.add(new JLabel(""));
					c_s_eastPanel.add(haltButton);
				}
				c_southPanel.add(tabbedPane, BorderLayout.CENTER);
				c_southPanel.add(c_s_eastPanel, BorderLayout.EAST);
			}
			
			centerPanel.add(new JScrollPane(sourceEditor), BorderLayout.CENTER);
			centerPanel.add(c_southPanel, BorderLayout.SOUTH);
		}
		southPanel = new JPanel(new BorderLayout());
		{
			s_northPanel = new JPanel(new BorderLayout(5, 5));
			{
				JLabel lbl = new JLabel("Output : ");
				
				pathField = new JTextField();
				pathButton = new JButton("...");
				pathButton.addActionListener(this);
				
				s_northPanel.add(lbl, BorderLayout.WEST);
				s_northPanel.add(pathField, BorderLayout.CENTER);
				s_northPanel.add(pathButton, BorderLayout.EAST);
			}
			
			statusLabel = new JLabel(" ");
			statusLabel.setBorder(new BevelBorder(BevelBorder.LOWERED));
			
			s_southPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
			{
				saveButton = new JButton("Save");
				saveButton.addActionListener(this);
				cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(this);
				
				s_southPanel.add(saveButton);
				s_southPanel.add(cancelButton);
			}
			
			southPanel.add(s_northPanel, BorderLayout.NORTH);
			southPanel.add(statusLabel, BorderLayout.CENTER);
			southPanel.add(s_southPanel, BorderLayout.SOUTH);
		}
		super.add(southPanel, BorderLayout.SOUTH);
		super.add(centerPanel, BorderLayout.CENTER);
	}// end _initcomp
	private void _initmenu(){
		menuBar = new JMenuBar();
		{
			functionsMenu = new JMenu("Functions");
			{
				JMenuItem[] itms = new JMenuItem[7];
				itms[0] = new JMenuItem("substring(string str)");
				itms[1] = new JMenuItem("strlen(string str)");
				itms[2] = new JMenuItem("getstatecount(int entity)");
				itms[3] = new JMenuItem("getaction(int entity, int state)");
				itms[4] = new JMenuItem("getproperty(string name)");
				itms[5] = new JMenuItem("getentitycount()");
				itms[6] = new JMenuItem("not(boolean b)");
				for(int i = 0; i < 7; i++){
					itms[i].addActionListener(this);
					functionsMenu.add(itms[i]);
				}
			}
			commandsMenu = new JMenu("Commands");
			{
				JMenuItem[] itms = new JMenuItem[5];
				itms[0] = new JMenuItem("LOG value");
				itms[1] = new JMenuItem("LOGLN value");
				itms[2] = new JMenuItem("PRINT value");
				itms[3] = new JMenuItem("PRINTLN value");
				itms[4] = new JMenuItem("END");
				for(int i = 0; i < 5; i++){
					itms[i].addActionListener(this);
					commandsMenu.add(itms[i]);
				}
			}
		}
		this.menuBar.add(functionsMenu);
		this.menuBar.add(commandsMenu);
		super.add(this.menuBar, BorderLayout.NORTH);
	}// end _initmenu
	
	
	
	
	public void reset(LearningEngine le, String source, String output){
		this.sourceEditor.setText(source);
		this.pathField.setText(output);
		this.logArea.setText("");
		this.statusLabel.setText(" ");

		this.basicInvoker = new RbdrBASICInvoker(le);
		this.sourceEditor.setInvokers(basicInvoker, basicInvoker, basicInvoker);
	}// end setSource(String)
	
	
	// Console implementation
	public BASICSourceEditor getSourceEditor()
	{ return sourceEditor; }
	
	
	public String getSource()
	{ return this.sourceEditor.getText(); }
	public String getOutput()
	{ return this.pathField.getText(); }
	public Writer getLog()
	{ return logWriter; }
	public OutputStream getOut()
	{ return outputStream; }
	public InputStream getIn()
	{ return inputStream; }
	
	

	// for BASIC compile & run
	public void run(){
		File output = new File(this.pathField.getText());
		if(!output.exists()){
			try{
				output.createNewFile();
			}catch(IOException ioe){
				javax.swing.JOptionPane.showMessageDialog(null, "Cannot create file : '" + output + "'");
				return;
			}
		}
		try{
			this.outputStream = new FileOutputStream(output);
		}catch(IOException ioe){
			javax.swing.JOptionPane.showMessageDialog(null, "Cannot open output stream : '" + output + "'");
			return;
		}
		String str = this.sourceEditor.getText();
		str = str.replaceAll("\r\n", "\n");
		StringReader sr = new StringReader(str);
		
		ErrorBundle bundle = new ErrorBundle();
		try{
			this.statusLabel.setText("Compiling..");
			Block root = basicParser.parse(sr, bundle);
			
			if(bundle.getSizeOfErrorMessages() != 0){
				ErrorMessage[] msgs = bundle.getErrorMessages();
				for(int i = 0; i < msgs.length; i++){
					this.errListModel.addElement(msgs[i]);
				}
				
				this.tabbedPane.setSelectedIndex(0);
				this.statusLabel.setText("Compile error.");
				return;
			}

			this.statusLabel.setText("Running...");
			this.tabbedPane.setSelectedIndex(1);
			this.logArea.setText("");
			Document logdoc = this.logArea.getDocument();
			
			ResultType res = basicRunner.start(root, this, basicInvoker, basicInvoker, basicInvoker, bundle);
			if(res != ResultType.DONE && res != ResultType.EXITED){
				if(bundle.getSizeOfErrorMessages() != 0){
					ErrorMessage[] msgs = bundle.getErrorMessages();
					for(int i = 0; i < msgs.length; i++)
						this.errListModel.addElement(msgs[i]);
					
					this.tabbedPane.setSelectedIndex(0);
					this.statusLabel.setText("Runtime error.");
					return;
				}
				if(res == ResultType.HALTED){
					logdoc.insertString(logdoc.getLength(), LINESEPARATOR, null);
					logdoc.insertString(logdoc.getLength(), "Halted.", null);
				}
			}else{
				logdoc.insertString(logdoc.getLength(), LINESEPARATOR, null);
				logdoc.insertString(logdoc.getLength(), "Terminated", null);
			}
			
			if(this.outputStream != null){
				this.outputStream.flush();
				this.outputStream.close();
			}
			this.outputStream = null;
			this.statusLabel.setText("");
		}catch(IOException excp){
			agdr.robodari.appl.BugManager.log(excp, true, false);
		}catch(NullPointerException npe){
			agdr.robodari.appl.BugManager.log(npe, true);
		}catch(BadLocationException ble){
			agdr.robodari.appl.BugManager.log(ble, true);
		}
	}// end run()
	
	
	
	
	// ActionListener implementation
	public void actionPerformed(ActionEvent ae){
		if(ae.getSource() == runButton){
			this.errListModel.clear();
			
			javax.swing.SwingUtilities.invokeLater(this);
			
		}else if(ae.getSource() == haltButton){
			this.basicRunner.halt();
			
		}else if(ae.getSource() == pathButton){
			JFileChooser jfc = new JFileChooser();
			
			if(jfc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION){
				this.pathField.setText(jfc.getSelectedFile().getAbsolutePath());
			}
			
			
		}else if(ae.getSource() == saveButton){
			save();
			close();
		}else if(ae.getSource() == cancelButton){
			close();
		}else if(ae.getSource() instanceof JMenuItem){
			JMenuItem itm = (JMenuItem)ae.getSource();
			
			try{
				this.sourceEditor.getDocument().insertString(this.sourceEditor.getSelectionStart(), itm.getText(), null);	
			}catch(Exception excp){
				excp.printStackTrace();
			}
		}
	}// end actionPerformed
	
	
	public void mouseEntered(MouseEvent me){}
	public void mouseExited(MouseEvent me){}
	public void mousePressed(MouseEvent me){}
	public void mouseReleased(MouseEvent me){}
	public void mouseClicked(MouseEvent me){
		if(me.getSource() == this.errList){
			if(me.getClickCount() >= 2 && me.getButton() == MouseEvent.BUTTON1){
				ErrorMessage msg = (ErrorMessage)this.errList.getSelectedValue();
				CodeRange cr = msg.getCodeRange();
				this.sourceEditor.requestFocus();
				this.sourceEditor.select(cr.getStartPosition(),
						cr.getStartPosition() + cr.getLength());
			}
		}
	}// end mouseClicked
	
	
	public abstract void save();
	public abstract void close();
	
	
	
	public static void main(String[] argv){
		final JFrame jf = new JFrame("Test");
		CodeGenPanel cgp = new CodeGenPanel(){
			public void save(){}
			public void close()
			{ jf.setVisible(false); System.exit(0); }
		};
		ExampleInput ei = new ExampleInput();
		cgp.reset(ei, "", "");
		jf.setSize(600, 500);
		jf.add(cgp, BorderLayout.CENTER);
		jf.add(ei, BorderLayout.EAST);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.setVisible(true);
	}// end main(String[])
	
	static class ExampleInput extends JPanel implements LearningEngine, ActionListener{
		private JTextField entitycntField;
		private JButton entitycntButton;
		
		private JTable statecntTable;
			private DefaultTableModel stcntTableModel;
		
		private JTable propTable;
			private DefaultTableModel propTableModel;
		private JButton addButton;
		
		private int entitycnt = 1;
		
		
		public ExampleInput(){
			super.setLayout(new BorderLayout());
			super.setPreferredSize(new Dimension(200, 400));
			
			JPanel entitycntPanel = new JPanel(new BorderLayout());
			{
				JLabel entitylabel = new JLabel("entity count : ");
				entitycntField = new JTextField("1");
				entitycntButton = new JButton("Update");
				entitycntButton.addActionListener(this);
				
				entitycntPanel.add(entitylabel, BorderLayout.WEST);
				entitycntPanel.add(entitycntField, BorderLayout.CENTER);
				entitycntPanel.add(entitycntButton, BorderLayout.EAST);
			}
			JPanel centerPanel = new JPanel(new BorderLayout());
			{
				JLabel toplbl = new JLabel("state count : ");
				stcntTableModel = new DefaultTableModel(new Object[]{ "state count"},  1);
				statecntTable = new JTable(stcntTableModel);
				statecntTable.setValueAt(new Integer(1), 0, 0);
				JLabel btmlbl = new JLabel("*Action is randomly set");
				
				JScrollPane jsp = new JScrollPane(statecntTable);
				//jsp.setPreferredSize(new Dimension(250, 200));
				centerPanel.add(toplbl, BorderLayout.NORTH);
				centerPanel.add(jsp, BorderLayout.CENTER);
				centerPanel.add(btmlbl, BorderLayout.SOUTH);
			}
			JPanel southPanel = new JPanel(new BorderLayout());
			{
				JPanel southNorthPanel = new JPanel(new BorderLayout());
				{
					JLabel lbl = new JLabel("properties : ");
					addButton = new JButton("Add");
					addButton.addActionListener(this);
					southNorthPanel.add(lbl, BorderLayout.CENTER);
					southNorthPanel.add(addButton, BorderLayout.EAST);
				}
				
				propTableModel = new DefaultTableModel(new Object[]{ "Name", "Value" }, 2);
				propTable = new JTable(propTableModel);
				
				JScrollPane jsp = new JScrollPane(propTable);
				jsp.setPreferredSize(new Dimension(200, 200));
				
				southPanel.add(southNorthPanel, BorderLayout.NORTH);
				southPanel.add(jsp, BorderLayout.CENTER);
			}
			super.add(entitycntPanel, BorderLayout.NORTH);
			super.add(centerPanel, BorderLayout.CENTER);
			super.add(southPanel, BorderLayout.SOUTH);
		}// end Constructor
		
		public void actionPerformed(ActionEvent ae){
			Object src = ae.getSource();
			if(src == entitycntButton){
				String sztext = entitycntField.getText();
				try{
					entitycnt = Integer.parseInt(sztext);
					stcntTableModel.setRowCount(entitycnt);
				}catch(NumberFormatException nfe){
					Toolkit.getDefaultToolkit().beep();
				}
			}else if(src == addButton){
				propTableModel.addRow(new Object[2]);
				propTable.setModel(propTableModel);
			}
		}// end actionPerformed

		public int entityCount()
		{
			System.out.println("entityCount() : " + entitycnt);
			return entitycnt; 
		}
		public int stateCount(int entityIdx){
			try{
				return Integer.parseInt(String.valueOf(stcntTableModel.getValueAt(entityIdx, 0)));
			}catch(Exception excp){}
			return -1;
		}
		public int getAction(int entityIdx, int state){
			return (int)(Math.random() * (double)stateCount(entityIdx));
		}
		public String getProperty(String key){
			int i;
			for(i = 0; i < propTable.getRowCount(); i++){
				if(key.equals(propTable.getValueAt(i, 0)))
					return String.valueOf(propTable.getValueAt(i, 1));
			}
			return null;
		}
	}// end class ExampleInput
}
