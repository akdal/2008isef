package agdr.robodari.plugin.rae.gui;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.vecmath.*;

import agdr.robodari.comp.*;
import agdr.robodari.comp.info.*;
import agdr.robodari.edit.*;
import agdr.robodari.appl.*;
import agdr.robodari.plugin.rae.edit.*;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.gui.*;
import agdr.robodari.plugin.rme.store.*;
import agdr.robodari.project.*;
import agdr.robodari.store.*;

public class RAComponentTree extends JPanel implements MouseListener, ActionListener{
	private RbdrApplEditor raEditor;
	
	private JTree tree;
	private DefaultTreeModel treeModel;
	private DefaultMutableTreeNode rootNode;
		private DefaultMutableTreeNode robotModelNodes;
		private DefaultMutableTreeNode strCompNodes;
		private DefaultMutableTreeNode vCompNodes;
	
	private JPopupMenu popupMenu;
		private JMenuItem refreshMenu;
	
		
	public RAComponentTree(RbdrApplEditor rae){
		this.raEditor = rae;
		
		super.setLayout(new BorderLayout());

		this.rootNode = new DefaultMutableTreeNode("Items");
		this.treeModel = new DefaultTreeModel(rootNode);
		
		robotModelNodes = new DefaultMutableTreeNode("Robots");
		strCompNodes = new DefaultMutableTreeNode("Structure components");
		vCompNodes = new DefaultMutableTreeNode("Virtual components");
		this.rootNode.add(robotModelNodes);
		this.rootNode.add(strCompNodes);
		this.rootNode.add(vCompNodes);
		
		this.tree = new JTree(this.treeModel);
		this.tree.setEditable(false);
		DefaultTreeCellRenderer renderer = new DefaultTreeCellRenderer();
		renderer.setLeafIcon(new ImageIcon());
		this.tree.setCellRenderer(renderer);
		this.tree.addMouseListener(this);
		super.add(new JScrollPane(this.tree));
		
		popupMenu = new JPopupMenu();
		refreshMenu = new JMenuItem("refresh");
		popupMenu.add(refreshMenu);
		refreshMenu.addActionListener(this);
		
		refresh();
	}// end 
	
	public void refresh(){
		try{
			BugManager.println("RAComponentTree : refresh()");
			robotModelNodes.removeAllChildren();
			strCompNodes.removeAllChildren();
			
			Project prj = raEditor.getInterface().getProject();
			RbModelEditorManager rbModelEM = null;
			EditorManager[] managers = raEditor.getInterface().getEditorManagers();
			for(EditorManager eachem : managers)
				if(eachem instanceof RbModelEditorManager){
					rbModelEM = (RbModelEditorManager)eachem;
					break;
				}
			
			if(prj != null){
				_fetchRobotModels(prj, rbModelEM);
			}
			
			Class[] rcomps = RobotComponentStore.getComponents();
			for(Class eachcls : rcomps){
				boolean flag1 = (StructureComponent.class.isAssignableFrom(eachcls) && RobotCompEditorStore.hasCompEditor(eachcls));
				boolean flag2 = VirtualComponent.class.isAssignableFrom(eachcls);
				if(flag1 || flag2){
					try{
						if(flag1)
							strCompNodes.add(new OtherCompNode(eachcls));
						else
							vCompNodes.add(new OtherCompNode(eachcls));
					}catch(Exception excp){
						excp.printStackTrace();
						agdr.robodari.appl.BugManager.printf("RAComponentTree : canntGetInfo : %s\n", eachcls);
						agdr.robodari.plugin.rme.appl.ExceptionHandler.cannotGetInfo(eachcls);
					}
				}
			}
			
			this.treeModel.reload();
		}catch(Exception excp){
			agdr.robodari.appl.BugManager.log(excp, true);
		}
	}// end refresh()
	private void _fetchRobotModels(ProjectCategory prj, RbModelEditorManager rmem) throws Exception{
		for(int i = 0; i < prj.getChildCount(); i++){
			ProjectItem itm = prj.getChildAt(i);
			if(itm instanceof ProjectFile && RobotModelEditor.isEditableFile(((ProjectFile)itm).getFileName())){
				ProjectFile pf = (ProjectFile)itm;
				if(rmem.isCached(pf)){
					this.robotModelNodes.add(new RbModelNode(rmem.getRobotModel(pf)));
				}else{
					FileInputStream fis = new FileInputStream(pf.getTotalPath());
					ObjectInputStream ois = new ObjectInputStream(fis);
					
					RobotModel model = (RobotModel)ois.readObject();
					this.robotModelNodes.add(new RbModelNode(model));
					
					ois.close();
					fis.close();
				}
			}else if(!(itm instanceof ProjectFile))
				_fetchRobotModels((ProjectCategory)itm, rmem);
		}
	}// end _fetchRobotModels
	
	
	
	
	public void actionPerformed(ActionEvent ae){
		if(ae.getSource() == this.refreshMenu){
			refresh();
		}
	}// end actionPerformed(ActionEvent)
	public void mousePressed(MouseEvent me){}
	public void mouseReleased(MouseEvent me){}
	public void mouseEntered(MouseEvent me){}
	public void mouseExited(MouseEvent me){}
	public void mouseClicked(MouseEvent me){
		if(me.getButton() == MouseEvent.BUTTON3)
			this.popupMenu.show(tree, me.getX(), me.getY());
		
		else if(me.getClickCount() >= 2 && me.getButton() == MouseEvent.BUTTON1){
			if(this.tree.getSelectionCount() == 0) return;

			Object obj = this.tree.getSelectionPath().getLastPathComponent();
			BugManager.printf("RACompTree : mouseClicked : obj : %s\n", obj);
			
			if(obj instanceof RbModelNode){
				RobotModel target = ((RbModelNode)obj).target;
				Matrix4f trans = new Matrix4f();
				trans.m00 = trans.m11 = trans.m22 = trans.m33 = 1;
				RobotObject robj = new RobotObject(target, trans);
				robj.setRobotName(JOptionPane.showInputDialog("Write robot name : "));
				
				this.raEditor.addObject(robj);
			}else if(obj instanceof OtherCompNode){
				Class target = ((OtherCompNode)obj).target;
				
				try{
					RobotComponent[] comps = this.raEditor.getModel().getRobotComponents();
					int count = 1;
					for(int i = 0; i < comps.length; i++)
						if(comps[i].getClass() == target)
							count++;
					String name = target.getSimpleName() + count;
					
					RobotComponent rc = (RobotComponent)target.newInstance();
					
					Matrix4f trans = new Matrix4f();
					trans.m00 = trans.m11 = trans.m22 = trans.m33 = 1;
					rc.setName(name);
					
					this.raEditor.addObject(rc, trans);
				}catch(Exception excp){
					agdr.robodari.appl.BugManager.log(excp, true);
				}
			}
		}
	}// end mouseClicked(
	
	
	

	static class RbModelNode implements MutableTreeNode{
		TreeNode parent;
		RobotModel target;
		
		public RbModelNode(RobotModel t){
			target = t;
		}
		
		public Enumeration children()
		{ return null; }
		public boolean getAllowsChildren()
		{ return false; }
		public TreeNode getChildAt(int childIndex)
		{ return null; }
		public int getChildCount()
		{ return 0; }
		public int getIndex(TreeNode node)
		{ return -1; }
		public TreeNode getParent()
		{ return parent; }
		public boolean isLeaf()
		{ return true; }
		
		public void insert(MutableTreeNode child, int index){}

		public void remove(int index){}

		public void remove(MutableTreeNode node){}

		public void removeFromParent(){}

		public void setParent(MutableTreeNode newParent){
			this.parent = newParent;
		}

		public void setUserObject(Object object){}
		
		public String toString()
		{ return target.getSketchModel().getName(); }
	}// end Rb
	static class OtherCompNode implements MutableTreeNode{
		TreeNode parent;
		Class target;
		Info info;
		
		public OtherCompNode(Class t) throws Exception{
			target = t;
			try{
				info = RobotInfoStore.getInfo((RobotComponent)target.newInstance());
			}catch(Exception e)
			{ throw e; }
			if(info == null)
				throw new Exception("Info is not given");
		}
		
		public Enumeration children()
		{ return null; }
		public boolean getAllowsChildren()
		{ return false; }
		public TreeNode getChildAt(int childIndex)
		{ return null; }
		public int getChildCount()
		{ return 0; }
		public int getIndex(TreeNode node)
		{ return -1; }
		public TreeNode getParent()
		{ return parent; }
		public boolean isLeaf()
		{ return true; }
		
		public void insert(MutableTreeNode child, int index){}

		public void remove(int index){}

		public void remove(MutableTreeNode node){}

		public void removeFromParent(){}

		public void setParent(MutableTreeNode newParent){
			this.parent = newParent;
		}

		public void setUserObject(Object object){}
		
		public String toString(){
			return info.get(InfoKeys.NAME).toString();
		}// end toString()
	}// end Rb
}
