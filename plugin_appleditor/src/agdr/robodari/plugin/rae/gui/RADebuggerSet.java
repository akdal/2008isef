package agdr.robodari.plugin.rae.gui;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.event.*;

import agdr.robodari.appl.*;
import agdr.robodari.gui.dialog.NewDialog;

import agdr.robodari.plugin.rme.gui.dialog.*;
import agdr.robodari.plugin.rme.rdpi.*;
import agdr.robodari.plugin.rme.rlztion.*;
import agdr.robodari.plugin.rme.rlztion.gui.*;
import agdr.robodari.plugin.rme.rlztion.gui.RMDebuggerSet.WatchTableCellRenderer;
import agdr.robodari.plugin.rme.rlztion.gui.RMDebuggerSet.WatchTableModel;
import agdr.robodari.plugin.rme.rlztion.gui.RDObjEvListSet.RDObjEvListModel;
import agdr.robodari.plugin.rae.rlztion.*;
import agdr.robodari.plugin.rae.edit.*;

public class RADebuggerSet extends DebuggerSet
		implements ListSelectionListener, ActionListener, ItemListener, RDObjectListener{
	private JPanel dsp_northPanel;
		private DefaultComboBoxModel entityCBoxModel;
		private JComboBox entityCBox;
		private JTextField entitySrcField;
		private Hashtable<RDEntity, DefaultComboBoxModel> objCBoxModels;
		private JComboBox objCBox;
		private JTextField objSrcField;

	private JPanel dsp_centerPanel;
		private JPanel dsp_cp_rdobjevPanel;
			private JPanel dsp_cp_roep_southPanel;
				private JButton rdobjevInfoButton;
			private Hashtable<RDObject, RDObjEvListModel> rdobjevListModels;
			private JScrollPane rdobjevScrPane;
			private JList rdobjevList;
		private JPanel dsp_cp_watchPanel;
			private JPanel dsp_cp_wp_southPanel;
			private JButton watchDetailButton;
			private Hashtable<RDObject, WatchTableModel> watchTableModels;
			private JTable watchTable;
			private RMDebuggerSet.WatchTableCellRenderer cellRenderer;
			
	private JButton codegenButton;
	private CodeGenPanel codegenPanel;
	private JMenuBar cgmenuBar;
		private JMenu cgeditMenu;
			private JMenuItem cgundoItem;
			private JMenuItem cgredoItem;
	private JDialog codegenDialog;
	
	private RARealizationModel realizationModel;
	private Frame parent;
	
		
	public RADebuggerSet(Frame _parent){
		this.parent = _parent;
		_init();
	}// end Constructor
	
	private void _init(){
		super.setLayout(new BorderLayout());
		
		dsp_northPanel = new JPanel(new GridLayout(4, 0));
		{
			entityCBoxModel = new DefaultComboBoxModel();
			entityCBox = new JComboBox(entityCBoxModel);
			entityCBox.addItemListener(this);
			entitySrcField = new JTextField();
			entitySrcField.setEditable(false);
			
			objCBoxModels = new Hashtable<RDEntity, DefaultComboBoxModel>();
			objCBox = new JComboBox();
			objSrcField = new JTextField();
			objSrcField.setEditable(false);
	
			dsp_northPanel.add(entityCBox);
			dsp_northPanel.add(entitySrcField);
			dsp_northPanel.add(objCBox);
			dsp_northPanel.add(objSrcField);
		}
		dsp_centerPanel = new JPanel(new GridLayout(2, 0));
		{
			dsp_cp_rdobjevPanel = new JPanel(new BorderLayout());
			{
				dsp_cp_roep_southPanel = new JPanel(new FlowLayout(
						FlowLayout.RIGHT));
				{
					rdobjevInfoButton = new JButton("Info..");
	
					rdobjevInfoButton.addActionListener(this);
					dsp_cp_roep_southPanel.add(rdobjevInfoButton);
				}
				rdobjevListModels = new Hashtable<RDObject, RDObjEvListModel>();
				rdobjevList = new JList();
				rdobjevList.setCellRenderer(new RDObjEvListSet.RDObjEvListCellRenderer());
				rdobjevList.getSelectionModel()
						.addListSelectionListener(this);
				rdobjevList
						.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	
				dsp_cp_rdobjevPanel.add(new JLabel("RDObj events",
						JLabel.CENTER), BorderLayout.NORTH);
				dsp_cp_rdobjevPanel.add(
						rdobjevScrPane = new JScrollPane(rdobjevList),
						BorderLayout.CENTER);
				dsp_cp_rdobjevPanel.add(dsp_cp_roep_southPanel,
						BorderLayout.SOUTH);
			}
			dsp_cp_watchPanel = new JPanel(new BorderLayout());
			{
				dsp_cp_wp_southPanel = new JPanel(new FlowLayout(
						FlowLayout.RIGHT));
				{
					watchDetailButton = new JButton("Detail..");
					dsp_cp_wp_southPanel.add(watchDetailButton);
				}
				watchDetailButton.addActionListener(this);
				
				watchTableModels = new Hashtable<RDObject, WatchTableModel>();
				watchTable = new JTable();
				cellRenderer = new WatchTableCellRenderer();
				watchTable.setDefaultRenderer(Object.class, cellRenderer);
				watchTable.getSelectionModel()
						.addListSelectionListener(this);
				watchTable
						.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	
				dsp_cp_watchPanel.add(new JLabel("Watch",
						JLabel.CENTER), BorderLayout.NORTH);
				dsp_cp_watchPanel.add(new JScrollPane(watchTable),
						BorderLayout.CENTER);
				dsp_cp_watchPanel.add(dsp_cp_wp_southPanel,
						BorderLayout.SOUTH);
			}
			
			dsp_centerPanel.add(dsp_cp_rdobjevPanel);
			dsp_centerPanel.add(dsp_cp_watchPanel);
		}
		
		codegenButton = new JButton(new ImageIcon(Toolkit.getDefaultToolkit().createImage(RADebuggerSet
						.class.getResource("icons/clcgen.png"))));
		codegenButton.addActionListener(this);
		codegenButton.setBackground(Color.white);
		codegenPanel = new CodeGenPanel(){
			public void save(){
				realizationModel.getApplication().setCodegenCode(getSource());
				realizationModel.getApplication().setCodegenOutput(getOutput());
			}// end save
			public void close(){
				codegenDialog.setVisible(false);
			}// end close()
		};
		cgmenuBar = new JMenuBar();
		{
			cgeditMenu = new JMenu("Edit");
			{
				cgundoItem = new JMenuItem("Undo");
				cgredoItem = new JMenuItem("Redo");
				cgundoItem.addActionListener(this);
				cgredoItem.addActionListener(this);
				cgeditMenu.add(cgundoItem);
				cgeditMenu.add(cgredoItem);
			}
			cgmenuBar.add(cgeditMenu);
		}
		
		codegenDialog = new JDialog(parent, "CodeGenerator");
		codegenDialog.setContentPane(codegenPanel);
		codegenDialog.setSize(500, 600);
		codegenDialog.setJMenuBar(cgmenuBar);
		
		add(dsp_northPanel, BorderLayout.NORTH);
		add(dsp_centerPanel, BorderLayout.CENTER);
		add(codegenButton, BorderLayout.SOUTH);
	}// end _init
	
	
	
	
	
	
	
	public void initDebugger(RealizationModel rmodel){
		BugManager.printf("RADbgSet : initDebugger() start!!\n");
		if(!(rmodel instanceof RARealizationModel))
			return;
		this.realizationModel = (RARealizationModel)rmodel;
		
		objCBox.removeItemListener(this);
		entityCBox.removeItemListener(this);
		
		watchDetailButton.setEnabled(false);
		rdobjevInfoButton.setEnabled(false);
		
		String[] entNames = realizationModel.getRDEntityNames();
		BugManager.printf("RADbgSet : initDbg : RlzModel.getEntNames().len : %d\n", entNames.length);
		
		this.entityCBoxModel.removeAllElements();
		this.rdobjevListModels.clear();
		this.watchTableModels.clear();
		
		for (int i = 0; i < entNames.length; i++){
			this.entityCBoxModel.addElement(entNames[i]);
		}
			
		for(int i = 0; i < entNames.length; i++){
			RDEntity entity = realizationModel.getEntity(entNames[i]);
			String[] objNames = realizationModel.getRDObjectNames(entity);
			
			DefaultComboBoxModel model = new DefaultComboBoxModel();
			for(int j = 0; j < objNames.length; j++){
				BugManager.printf("RADbgSet : initDbg : entity.objNames[%d] : %s\n", j, objNames[j]);
				RDObject rdobj = realizationModel.getRDObject(entity, objNames[j]);
				WatchTableModel wtModel = new WatchTableModel(this.realizationModel, rdobj);

				this.watchTableModels.put(rdobj, wtModel);
				this.rdobjevListModels.put(rdobj, new RDObjEvListModel());
				model.addElement(objNames[j]);
			}
			this.objCBoxModels.put(entity, model);
		}
		
		if(entNames.length != 0)
			this.openEntity(entNames[0]);
		this.entityCBox.addItemListener(this);
		this.objCBox.addItemListener(this);
		
		RbdrApplication appl = this.realizationModel.getApplication();
		if(this.realizationModel.getServer() != null)
			this.codegenPanel.reset(this.realizationModel.getAdministrator().getLearningEngine(),
				appl.getCodegenCode(), appl.getCodegenOutput());
		else
			this.codegenPanel.reset(null, 
					appl.getCodegenCode(), appl.getCodegenOutput());
	}// end initDebugger
	
	public void finishDebugger(){
		this.objCBox.removeItemListener(this);
		this.entityCBox.removeItemListener(this);
		
		synchronized(this.realizationModel){
			try{
				this.realizationModel.removeRDObjectListener(this);
			}catch(NullPointerException npe){
				BugManager.log(npe, true, false);
			}
			this.watchTableModels.clear();
			this.rdobjevListModels.clear();
			this.objCBoxModels.clear();
			this.entityCBoxModel.removeAllElements();
			this.realizationModel = null;
		}
	}// end finishDebugger
	
	
	public void openEntity(String ctrler){
		RDEntity ent = this.realizationModel.getEntity(ctrler);
		String[] ctrlerNames = this.realizationModel.getRDObjectNames(ent);
		if(ctrlerNames.length == 0)
			return;
		
		BugManager.printf("RADbgSet : openEntity : ent : %s, this.objCBoxModels.get() : %s\n", ent, this.objCBoxModels.get(ent));
		this.objCBox.setModel(this.objCBoxModels.get(ent));
		if(ctrlerNames.length != 0) return;
		
		RDObject obj = this.realizationModel.getRDObject(ent, ctrlerNames[0]);
		
		this.rdobjevList.setModel(rdobjevListModels.get(obj));
		this.rdobjevScrPane.getVerticalScrollBar().setValue(
				this.rdobjevScrPane.getVerticalScrollBar().getMaximum());
		this.watchTable.setModel(this.watchTableModels.get(obj));
	}// end openDebugTools(SourceEdtiable)
	public void openObject(String objName){
		RDEntity ent = this.realizationModel.getEntity((String)this.entityCBox.getSelectedItem());

		RDObject obj = this.realizationModel.getRDObject(ent, objName);
		this.rdobjevList.setModel(rdobjevListModels.get(obj));
		this.rdobjevScrPane.getVerticalScrollBar().setValue(
				this.rdobjevScrPane.getVerticalScrollBar().getMaximum());
		this.watchTable.setModel(this.watchTableModels.get(obj));
	}// end openObject(String)
	
	
	public void methodCalled(RDObjectEvent event)
	{ addRDObjectEvent(event); }
	public void fieldAccessed(RDObjectEvent e)
	{ addRDObjectEvent(e); }
	public void fieldWritten(RDObjectEvent e)
	{ addRDObjectEvent(e); }
	public void addRDObjectEvent(RDObjectEvent event){
		RDObjEvListModel model = rdobjevListModels.get(event.target);
		if(model.getSize() > 40)
			model.removeElementAt(0);
		model.addElement(event);
	}// end addRDObjectEvent(SourceEditable, RDObjectEvent)
	
	
	
	/* *********************************************
	 *                EVENT LISTENERS
	 *******************************************/
	
	
	public void itemStateChanged(ItemEvent ie) {
		if (ie.getSource() == this.entityCBox) {
			BugManager.printf("RADbgSet : itemStateChanged : %s\n", ie.getItem());
			this.openEntity((String) this.entityCBox.getSelectedItem());
		}else if(ie.getSource() == this.objCBox)
			this.openObject(this.objCBox.getSelectedItem().toString());
	}// end itemStateChanged(ItemEvent)
	
	
	public void actionPerformed(ActionEvent ae){
		Object source = ae.getSource();
		RDEntity ent = this.realizationModel.getEntity((String)this.entityCBox.getSelectedItem());
		if(ent == null){
			JOptionPane.showMessageDialog(null, "There's no entity in this application.");
			return;
		}
		RDObject rdobj = this.realizationModel.getRDObject(ent, (String)this.objCBox.getSelectedItem());
		
		if(source == this.rdobjevInfoButton){
			int selidx = rdobjevList.getSelectedIndex();
			if(selidx == -1) return;
			
			RDObjectEvent obj = (RDObjectEvent)rdobjevListModels
					.get(rdobj).getElementAt(selidx);

			if(obj.type == RDObjectEvent.TYPE_FIELD_ACCESS || obj.type == RDObjectEvent.TYPE_FIELD_WRITTEN){
				DbgSetValInfoDialog dialog = new DbgSetValInfoDialog(parent);
				dialog.showObject(obj.values[0], obj.types[0]);
			}else{
				DbgSetRDObjEvInfoDialog dialog = new DbgSetRDObjEvInfoDialog(parent);
				dialog.showMethod(obj);
			}
		}
		
		
		else if(source == this.watchDetailButton){
			int selidx = watchTable.getSelectedRow();
			if(selidx == -1) return;
			
			WatchTableModel model = watchTableModels.get(rdobj);
			DbgSetValInfoDialog dsvid = new DbgSetValInfoDialog(parent);
			Class type = null;
			Object value = model.getFieldValue(selidx);
			if(value != null)
				type = value.getClass();
			dsvid.showObject(value, type);
		}
		
		else if(this.codegenButton == source){
			this.codegenDialog.setVisible(true);
		}else if(this.cgundoItem == source)
			this.codegenPanel.getSourceEditor().undo();
		else if(this.cgredoItem == source)
			this.codegenPanel.getSourceEditor().redo();
	}// end actionPerformed
	
	
	public void valueChanged(ListSelectionEvent lse) {
		Object src = lse.getSource();
		if (src == rdobjevList.getSelectionModel()) {
			if (lse.getFirstIndex() == -1)
				rdobjevInfoButton.setEnabled(false);
			else
				rdobjevInfoButton.setEnabled(true);
		} else if (src == watchTable.getSelectionModel()) {
			if (lse.getFirstIndex() == -1)
				watchDetailButton.setEnabled(false);
			else
				watchDetailButton.setEnabled(true);
		}
	}// end valueChanged(ListSelectionEvent)
}
