package agdr.robodari.plugin.rae.gui;

import java.awt.*;
import java.awt.event.*;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Hashtable;

import javax.swing.*;
import javax.vecmath.Matrix4f;


import agdr.robodari.appl.ApplicationInfo;
import agdr.robodari.appl.BugManager;
import agdr.robodari.comp.*;
import agdr.robodari.edit.*;
import agdr.robodari.gui.dialog.OptionDialog;
import agdr.robodari.plugin.rae.rapi.*;
import agdr.robodari.plugin.rae.edit.RbdrApplication;
import agdr.robodari.plugin.rae.edit.RobotObject;
import agdr.robodari.plugin.rae.rlztion.*;
import agdr.robodari.plugin.rme.appl.ExceptionHandler;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.gui.*;
import agdr.robodari.plugin.rme.rlztion.*;
import agdr.robodari.plugin.rme.rlztion.gui.RealizationPanel;
import agdr.robodari.plugin.rme.store.ModelNotRegisteredException;
import agdr.robodari.plugin.rme.store.RobotCompEditorStore;
import agdr.robodari.project.ProjectFile;

public class RbdrApplEditor extends JPanel implements EditorManager,
		SketchEditorInterface, ActionListener, SketchEditorListener{
	public final static String EXT_RBDRAPPL = ".rbdrappl";
	public final static String EXTDESC = "Application";
	public final static Icon FILEICON = new Icon(){
		public int getIconWidth()
		{ return 16; }
		public int getIconHeight()
		{ return 16; }
		public void paintIcon(Component c, Graphics g, int x, int y){}
	};
	
	
	
	
	
	
	private RobodariInterface rbdrInterface;
	
	
	private DefaultMainSketchEditor mainSketchEditor;
	private JPanel southPanel;
		private JPanel ssPanel;
			private SideSketchViewer[] sideSketchViewers;
		private JPanel configPanel;
			private JCheckBox grvEffCheckBox;
	
	private String[] rightCompTitles;
	private JComponent[] rightComps;
	private RAComponentTree racompTree;
	


	private Frame frame_parent;
	
	private Hashtable<Class<? extends RobotComponent>, CompEditor> compEditors
			= new Hashtable<Class<? extends RobotComponent>, CompEditor>();
	private RotationEditor rotationEditor;
	private CompEditor currentEditor;
	private GroupEditor groupEditor;
	private ProjectionEditor projEditor;
	
	private JDialog rlzDialog;
		private RealizationPanel rlzPanel;
		private RADebuggerSet radbgSet;
	
	private RbdrApplOptionDialog raoptDlg;
	
	private Hashtable<ProjectFile, RbdrApplication> cachedRbdrAppls
			= new Hashtable<ProjectFile, RbdrApplication>();
	
	
	private JMenu viewMenu;
		private JMenuItem setProjMenu;
	private JMenu realizationMenu;
		private JMenuItem coefOfResttSetMenu;
		private JMenuItem runMenu;
	
	
	
	
	public RbdrApplEditor(RobodariInterface itfc, Frame parent){
		this.rbdrInterface = itfc;
		this.frame_parent = parent;
		
		initComps();
		initMenuItems();

		Class<? extends RobotComponent>[] compclses = RobotCompEditorStore.getRegisteredComps();
		Object[] con_args = new Object[]{ this, frame_parent };
		Constructor<CompEditor> cons = null;
		
		for(Class<? extends RobotComponent> eachcls : compclses){
			try{
				Class instcls = RobotCompEditorStore.getCompEditorClass(eachcls);
				
				cons = instcls.getConstructor(new Class[]{ SketchEditorInterface.class, Frame.class });
				compEditors.put(eachcls, cons.newInstance(con_args));
			}catch(NoSuchMethodException nsme){
				BugManager.log(nsme, true);
			}catch(InstantiationException ie){
				BugManager.log(ie, true);
			}catch(IllegalAccessException iae){
				BugManager.log(iae, true);
			}catch(InvocationTargetException ite){
				BugManager.log(ite, true);
			}
		}

		rotationEditor = new RotationEditor(this, frame_parent);
		groupEditor = new GroupEditor(this, frame_parent);
		projEditor = new ProjectionEditor(frame_parent){
			public void finish(){
				mainSketchEditor.setProjector(projEditor.getProjector());
				projEditor.setVisible(false);
			}// end finish()
		};
		this.raoptDlg = new RbdrApplOptionDialog(frame_parent, this);
	}// end Constructor(RobodariInterface, Frame)
	
	private void initComps(){
		// sketch viewers
		
		mainSketchEditor = new DefaultMainSketchEditor(this);
		mainSketchEditor.setPreferredSize(new Dimension(1300, 700));
		mainSketchEditor.addSketchEditorListener(this);
		
		this.southPanel = new JPanel(new BorderLayout());
		{
			this.ssPanel = new JPanel(new GridLayout(0, 3));
			{
				this.ssPanel.setPreferredSize(new Dimension(400, 250));
				
				this.sideSketchViewers = new SideSketchViewer[3];
				this.sideSketchViewers[0] = new SideSketchViewer(this.mainSketchEditor, SketchEditor.PROJECTORS[0]);
				this.sideSketchViewers[1] = new SideSketchViewer(this.mainSketchEditor, SketchEditor.PROJECTORS[1]);
				this.sideSketchViewers[2] = new SideSketchViewer(this.mainSketchEditor, SketchEditor.PROJECTORS[2]);
				
				this.ssPanel.add(this.sideSketchViewers[0]);
				this.ssPanel.add(this.sideSketchViewers[1]);
				this.ssPanel.add(this.sideSketchViewers[2]);
			}
			
			// configuration [panel
			this.configPanel = new JPanel(new FlowLayout());
			{
				this.grvEffCheckBox = new JCheckBox("Gravity effected");
				this.grvEffCheckBox.addActionListener(this);
				
				this.configPanel.add(this.grvEffCheckBox);
			}
			this.southPanel.add(this.configPanel, BorderLayout.SOUTH);
			this.southPanel.add(this.ssPanel, BorderLayout.CENTER);
		}
		this.setLayout(new BorderLayout());
		this.add(this.southPanel, BorderLayout.SOUTH);
		this.add(new JScrollPane(mainSketchEditor,
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS), BorderLayout.CENTER);
		
		
		this.racompTree = new RAComponentTree(this);
		this.rightComps = new JComponent[]{
				racompTree
		};
		this.rightCompTitles = new String[]{
				"Comps"
		};
		
		this.radbgSet = new RADebuggerSet(this.frame_parent);
		this.rlzPanel = new RealizationPanel(this.frame_parent, this.radbgSet);
		this.rlzDialog = new JDialog(this.frame_parent, "Realization");

		Dimension scrsize = Toolkit.getDefaultToolkit().getScreenSize();
		Insets is = Toolkit.getDefaultToolkit().getScreenInsets(
				frame_parent.getGraphicsConfiguration());
		this.rlzDialog.setSize(scrsize.width - is.left - is.right, scrsize.height
				- is.top - is.bottom);
		this.rlzDialog.setLocation(0, 0);
		this.rlzDialog.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent we){
				rlzPanel.finish();
			}// end windowCosing
		});
		this.rlzDialog.setContentPane(this.rlzPanel);
	}// end initComps()
	private void initMenuItems(){
		viewMenu = new JMenu("View");
		{
			setProjMenu = new JMenuItem("Edit Projection..");
			setProjMenu.addActionListener(this);
			viewMenu.add(setProjMenu);
		}
		realizationMenu = new JMenu("Realization");
		{
			coefOfResttSetMenu = new JMenuItem("Coefficient of Restitution...");
			coefOfResttSetMenu.addActionListener(this);
			runMenu = new JMenuItem("Run..");
			runMenu.addActionListener(this);
			realizationMenu.add(coefOfResttSetMenu);
			realizationMenu.add(runMenu);
		}
	}// end initMenuItems()
	
	
	/*
	 * Implementation of SketchEditorInterface
	 */
	public int getSideSketchViewerCount()
	{ return 3; }
	public SideSketchViewer getSideSketchViewer(int idx)
	{ return sideSketchViewers[idx]; }
	
	public DefaultMainSketchEditor getMainEditor()
	{ return this.mainSketchEditor; }
	
	public void addObject(RobotComponent rc, Matrix4f transf){
		if(this.mainSketchEditor.getModel() != null){
			try{
				if(BugManager.DEBUG)
					BugManager.printf("RbdrApplEditor : addObject() : %s\n", rc);
				mainSketchEditor.getModel().addRobotComponent(rc, transf);
			}catch(ModelNotRegisteredException excp){
				ExceptionHandler.modelNotRegistered(rc.getClass());
			}catch(Exception excp){
				agdr.robodari.appl.BugManager.log(excp, true);
			}
		}else
			ExceptionHandler.sketchNotOpened();
	}// end addObject(RobotComponent, Matrix4d)
	public void addObject(RobotObject obj){
		if(this.mainSketchEditor.getModel() != null){
			try{
				if(BugManager.DEBUG)
					BugManager.println("RbdrApplEditor : addObject(obj)");
				mainSketchEditor.getModel().addObject(obj);
			}catch(Exception excp){
				agdr.robodari.appl.BugManager.log(excp, true);
			}
		}else
			ExceptionHandler.sketchNotOpened();
	}
	public SketchModel getModel()
	{ return mainSketchEditor.getModel(); }
	public void refreshObject(SkObject obj){
		mainSketchEditor.refreshObject(obj);
		for(int i = 0; i < sideSketchViewers.length; i++)
			sideSketchViewers[i].refreshObject(obj);
	}// end refreshObject(SkObject)

	
	public RotationEditor getRotationEditor()
	{ return this.rotationEditor; }
	public GroupEditor getGroupEditor()
	{ return this.groupEditor; }
	public CompEditor getCurrentCompEditor()
	{ return this.currentEditor; }
	public void setCurrentCompEditor(CompEditor ce){
		if(this.currentEditor != null && this.currentEditor.isVisible())
			this.currentEditor.setVisible(false);
		this.currentEditor = ce;
		if(ce != null)
			ce.setVisible(true);
	}// end showCompEditor(CompEditor)
	public CompEditor getStoredCompEditor(Class<? extends RobotComponent> cls)
	{ return compEditors.get(cls); }
	
	
	public int getLayoutMode()
	{ return -1; }
	public void setLayoutMode(int mode)
	{}
	
	
	
	
	
	/*
	 * Implementation of EditorManager
	 */

	public void newFile(ProjectFile pfile){
		RbdrApplication appl = new RbdrApplication();
		
		cachedRbdrAppls.put(pfile, appl);
		saveFile(pfile, new File(pfile.getTotalPath()));
		cachedRbdrAppls.remove(pfile);
	}// end newFile(ProjectFile)
	public boolean isCached(ProjectFile file)
	{ return cachedRbdrAppls.containsKey(file); }
	public ProjectFile[] getCachedFiles()
	{ return cachedRbdrAppls.keySet().toArray(new ProjectFile[]{}); }
	/**
	 * If given file is not opened, read the content from file and cache.
	 * If given file is opened, cache edited model.
	 * @param file
	 */
	public void cacheFile(ProjectFile pfile){
		if(!cachedRbdrAppls.containsKey(pfile)){
			ByteArrayInputStream bais = null;
			FileInputStream fis = null;
			ObjectInputStream ois = null;
			File file = new File(pfile.getTotalPath());
			try{
				fis = new FileInputStream(file);
				byte[] data = new byte[fis.available()];
				fis.read(data, 0, data.length);
				fis.close();
				
				bais = new ByteArrayInputStream(data);
				ois = new ObjectInputStream(bais);
				RbdrApplication openRobotModel =
					(RbdrApplication)ois.readObject();
				
				cachedRbdrAppls.put(pfile, openRobotModel);
				
				ois.close();
				bais.close();
			}catch(java.io.InvalidClassException ice){
				BugManager.log(ice, true);
			}catch(IOException excp){
				agdr.robodari.appl.ExceptionHandler.wrongFile(file.getAbsolutePath());
				excp.printStackTrace();
			}catch(ClassNotFoundException excp){
				BugManager.log(excp, true);
			}
		}else{
			// nothing to do
		}
	}// end cacheFile(ProjectFile)
	public void openFile(ProjectFile file, boolean reload){
		if(!this.isCached(file) || reload)
			cacheFile(file);
		if(BugManager.DEBUG)
			BugManager.println("RbdrApplEditor : openFile() : " + file);

		RbdrApplication model = this.cachedRbdrAppls.get(file);
		this.mainSketchEditor.loadSketch(model);
		this.grvEffCheckBox.setSelected(false);
	}// end openFile(ProjectFile, boolean)
	public void saveFile(ProjectFile pfile, java.io.File file){
		FileOutputStream fos = null;
		ObjectOutputStream oos = null;
		try{
			if(!file.exists())
				file.createNewFile();
			fos = new FileOutputStream(file);
			oos = new ObjectOutputStream(fos);
			
			oos.writeObject(this.cachedRbdrAppls.get(pfile));
		}catch(IOException ioe){
			BugManager.log(ioe, true);
		}finally{
			try{
				oos.close();
				fos.close();
			}catch(Exception excp)
			{ excp.printStackTrace(); }
		}
	}// end saveFile(ProjectFile, java.io.File)
	public void closeFile(ProjectFile file){
		if(this.getMainEditor().getModel() == this.cachedRbdrAppls.get(file)){
			this.mainSketchEditor.loadSketch(null);
			this.grvEffCheckBox.setEnabled(false);
		}
		this.cachedRbdrAppls.remove(file);
	}// end closeFile(ProjectFile)
	
	
	public RbdrApplication getRbdrApplication(ProjectFile pf)
	{ return this.cachedRbdrAppls.get(pf); }
	
	
	public RobodariInterface getInterface()
	{ return rbdrInterface; }
	
	public int getRightComponentCount()
	{ return rightComps.length; }
	public JComponent getRightComponent(int idx)
	{ return rightComps[idx]; }
	public String getRightCompTitle(int idx)
	{ return rightCompTitles[idx]; }
	public int getLeftComponentCount()
	{ return 0; }
	public JComponent getLeftComponent(int idx)
	{ return null; }
	public String getLeftCompTitle(int idx)
	{ return null; }
	public JComponent getEditorComp()
	{ return this; }
	
	public javax.swing.Icon getFileIcon()
	{ return FILEICON; }
	public void customizeMenuBar (JMenuBar menuBar){
		menuBar.add(this.viewMenu);
		menuBar.add(this.realizationMenu);
		menuBar.revalidate();
		menuBar.repaint();
	}// end customizeMenuBar(JMenuBar);
	public void undoMenuBar(JMenuBar mb){
		mb.remove(this.viewMenu);
		mb.remove(this.realizationMenu);
		mb.revalidate();
		mb.repaint();
	}// end undoMenuBar(JMenuBar);
	public boolean isEditable(String szfile){
		if(szfile.indexOf('.') == -1) return false;
		return szfile.substring(szfile.indexOf('.'), szfile.length()).toLowerCase()
		.equals(RbdrApplEditor.EXT_RBDRAPPL);
	}// end isEditable(String)
	public OptionDialog getFileOptionDialog()
	{ return this.raoptDlg; }
	public String getFileDescription()
	{ return EXTDESC; }
	public String getFileExtension()
	{ return EXT_RBDRAPPL; }
	
	
	
	
	/*
	 * Implementation of EventListeners
	 */ 
	public void actionPerformed(ActionEvent ae){
		if(ae.getSource() == this.grvEffCheckBox){
			RbdrApplication appl = (RbdrApplication)this.mainSketchEditor.getModel();
			if(this.mainSketchEditor.getSelectedItems().length != 0){
				SkObject obj = this.mainSketchEditor.getSelectedItems()[0];
				appl.setGravityEffection(obj, this.grvEffCheckBox.isSelected());
			}
		}
		
		else if(ae.getSource() == this.runMenu){
			Runnable runnable = new Runnable(){
				public void run(){
					try{
						int state = rlzPanel.getState();//realizationViewer.getState();
						while(state != RealizationPanel.ST_UNEMPLOYED){
							if(state == RealizationPanel.ST_DBG_PAUSE || state == RealizationPanel.ST_DBG_ONPROC){
								rbdrInterface.setStatusBarText("Finished.");
								break;
							}else if(state == RealizationPanel.ST_INIT_RPANEL)
								rbdrInterface.setStatusBarText("canvas initializing..");
							else if(state == RealizationPanel.ST_INIT_COMPS)
								rbdrInterface.setStatusBarText("initializing components..");
							else if(state == RealizationPanel.ST_INIT_RDPLOAD)
								rbdrInterface.setStatusBarText("initializing RDPI modules..");
							Thread.sleep(100);
							state = rlzPanel.getState();
						}
						rbdrInterface.setStatusBarText("");
					}catch(Exception excp){
						BugManager.log(excp, true);
					}
				}// end run()
			};
			SwingUtilities.invokeLater(runnable);
			Runnable run2 = new Runnable(){
				public void run(){
					RARealizationModel model = new RARealizationModel();
					try{
						BugManager.printf("--- RbdrApplEditor : start Realization\n");
						model.init((RbdrApplication)mainSketchEditor.getModel(), rbdrInterface.getProject());
						rlzPanel.initialize(model);
						rlzDialog.setVisible(true);
						BugManager.printf("--- RbdrApplEditor : end startRealization\n");
					}catch(RDPLoadException rdpexcp){
						if(rdpexcp.errorCase == RDPLoadException.CASE_INSTANTIATING)
							agdr.robodari.plugin.rme.appl.ExceptionHandler.rdpi_init_exceptionOccured(rdpexcp.ctrlerName, rdpexcp.getCause());
						else if(rdpexcp.errorCase == RDPLoadException.CASE_STREAMING)
							agdr.robodari.plugin.rme.appl.ExceptionHandler.rdpi_cannotOpenStream(rdpexcp.ctrlerName, rdpexcp.getCause());

						try{
							model.disposeRealization();
						}catch(Exception excp){}
					}catch(IllegalOptionException iopexcp){
						JOptionPane.showMessageDialog(null, "Wrong option : " + iopexcp.getMessage(),
								ApplicationInfo.getFullName(),
								JOptionPane.OK_OPTION);

						try{
							model.disposeRealization();
						}catch(Exception excp){}
					}catch(Exception excp){
						BugManager.log(excp, true);
					}
				}// end run()
			};// end run2()
			this.saveFile(rbdrInterface.getEditingFile(), new File(rbdrInterface.getEditingFile().getTotalPath()));
			SwingUtilities.invokeLater(run2);
		}else if(ae.getSource() == setProjMenu){
			this.projEditor.start((Projector.OrthographicProjector)mainSketchEditor.getProjector());
		}else if(ae.getSource() == coefOfResttSetMenu){
			boolean flag = false;
			RbdrApplication appl = (RbdrApplication)this.mainSketchEditor.getModel();
			
			String szval = JOptionPane.showInputDialog(this, "Input coefficient of restituion : ", 
				new Double(appl.getCoeffOfRestt()));
			double val = 0.0;
			
			try{
				val = Double.parseDouble(szval);
				if(val < 0.0 || 1.0 < val)
					throw new Exception();
			}catch(Exception e){
				flag = true;
			}
			while(flag){
				try{
					flag = false;
					szval = JOptionPane.showInputDialog(this, "<html>Value should be more than 0.0 & less than 1.0<br>Input coefficient of restituion : ", 
							new Double(appl.getCoeffOfRestt()));
					if(szval == null)
						break;
					val = Double.parseDouble(szval);
					if(val < 0.0 || 1.0 < val)
						throw new Exception();
				}catch(Exception nfe){
					flag = true;
				}
			}
			if(szval != null)
				appl.setCoeffOfRestt(val);
		}
	}// end actionPerformed(ActionEvnet)
	

	public void componentDrifted(SketchEditorEvent see){}
	public void componentRotated(SketchEditorEvent see){}
	public void componentSelected(SketchEditorEvent see){
		if(this.mainSketchEditor.getSelectedItems().length != 1){
			this.grvEffCheckBox.setEnabled(false);
		}
		this.grvEffCheckBox.setEnabled(true);
		SkObject obj = see.getAppearance();
		RbdrApplication appl = (RbdrApplication)this.mainSketchEditor.getModel();
		
		this.grvEffCheckBox.setSelected(appl.gravityEffected(obj));
	}// end componentSelected(SketchEditorEvent)
	public void componentDeselected(SketchEditorEvent se){
		this.grvEffCheckBox.setEnabled(false);
	}// end componentDeselected(SketchEditorEvent)
	public void modelChanged(SketchEditorEvent see){
		this.grvEffCheckBox.setEnabled(false);
	}// end modelChanged(SketchEditorEvent)
}
