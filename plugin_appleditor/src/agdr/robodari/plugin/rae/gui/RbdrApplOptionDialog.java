package agdr.robodari.plugin.rae.gui;

import java.io.*;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.tree.*;


import agdr.robodari.gui.dialog.*;
import agdr.robodari.plugin.rae.edit.RbdrApplication;
import agdr.robodari.project.*;

public class RbdrApplOptionDialog extends OptionDialog<ProjectFile> implements ActionListener{
	private TreeModel treeModel;
	private DefaultTreeCellRenderer treeCellRenderer;
	
	private JPanel configPanel;
		private JTextField serverClsNameField;
		private JTextField serverSrcField;
		private JButton serverSrcButton;
		private JTextField adminClsNameField;
		private JTextField adminSrcField;
		private JButton adminSrcButton;
	private RbdrApplEditor editor;
	
		
	
	public RbdrApplOptionDialog(Frame parent, RbdrApplEditor rae){
		super(parent, "RbdrAppl Editor");
		this.editor = rae;
		_init();
	}// end Constructor
	public RbdrApplOptionDialog(Dialog parent, RbdrApplEditor rae){
		super(parent, "RbdrAppl Editor");
		this.editor = rae;
		_init();
	}// end Constructor
	
	private void _init(){
		configPanel = new JPanel();
		BoxLayout blayout = new BoxLayout(configPanel, BoxLayout.Y_AXIS);
		configPanel.setLayout(blayout);
		{
			JPanel svNamePanel = new JPanel(new FlowLayout());
			serverClsNameField = new JTextField();
			serverClsNameField.setColumns(10);
			
			svNamePanel.add(new JLabel("Server ClassName : "));
			svNamePanel.add(serverClsNameField);
			
			JPanel serverpanel = new JPanel(new FlowLayout());
			serverSrcField = new JTextField();
			serverSrcField.setColumns(14);
			serverSrcButton = new JButton("..");
			serverSrcButton.addActionListener(this);
			
			serverpanel.add(new JLabel("Server : "));
			serverpanel.add(serverSrcField);
			serverpanel.add(serverSrcButton);
			
			JPanel adminNamePanel = new JPanel(new FlowLayout());
			adminClsNameField = new JTextField();
			adminClsNameField.setColumns(10);
			
			adminNamePanel.add(new JLabel("Administartor ClassName : "));
			adminNamePanel.add(adminClsNameField);
			
			JPanel adminpanel = new JPanel(new FlowLayout());
			
			adminSrcField = new JTextField();
			adminSrcField.setColumns(14);
			adminSrcButton = new JButton("..");
			adminSrcButton.addActionListener(this);
			
			adminpanel.add(new JLabel("Administrator : "));
			adminpanel.add(adminSrcField);
			adminpanel.add(adminSrcButton);
			
			
			configPanel.add(svNamePanel);
			configPanel.add(serverpanel);
			configPanel.add(adminNamePanel);
			configPanel.add(adminpanel);
		}
	}// end _init()
	
	
	
	/* *************************************8
	 *  ActionListener Implementation
	 ***********************************/
	
	public void actionPerformed(ActionEvent ae){
		super.actionPerformed(ae);
		
		if(ae.getSource() == this.serverSrcButton || ae.getSource() == this.adminSrcButton){
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setFileFilter(new javax.swing.filechooser.FileFilter(){
				public boolean accept(File file)
				{ return file.isDirectory() || file.getName().endsWith(".class"); }
				public String getDescription()
				{ return "directory or class file"; }
			});
			fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
			fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			
			int res = fileChooser.showOpenDialog(this);
			if(res == JFileChooser.APPROVE_OPTION){
				File file = fileChooser.getSelectedFile();
				if(ae.getSource() == this.serverSrcButton)
					this.serverSrcField.setText(file.getAbsolutePath());
				else if(ae.getSource() == this.adminSrcButton)
					this.adminSrcField.setText(file.getAbsolutePath());
			}
		}
	}// end actionPErformed
	
	
	
	
	/* **********************************************************
	 *     OptionDialog Implementation
	 * ***********************************************************/
	
	
	public TreeCellRenderer getTreeCellRenderer(){
		if(treeCellRenderer == null){
			this.treeCellRenderer = new DefaultTreeCellRenderer();
			this.treeCellRenderer.setLeafIcon(new Icon(){
				public int getIconWidth()
				{ return 0; }
				public int getIconHeight()
				{ return 0; }
				public void paintIcon(Component c, Graphics g, int x, int y){}
			});
		}
		return treeCellRenderer;
	}// end getTCR
	public TreeModel getTreeModel(){
		if(this.treeModel == null){
			DefaultMutableTreeNode root = new DefaultMutableTreeNode("Application Options");
			DefaultMutableTreeNode cfgnode = new DefaultMutableTreeNode("Configurations");
			root.add(cfgnode);
			treeModel = new DefaultTreeModel(root);
			
			treeCellRenderer = new DefaultTreeCellRenderer();
			this.treeCellRenderer.setLeafIcon(new Icon(){
				public int getIconWidth()
				{ return 0; }
				public int getIconHeight()
				{ return 0; }
				public void paintIcon(Component c, Graphics g, int x, int y){}
			});
		}
		return treeModel;
	}// end getTreeModel()
	
	
	
	
	public JComponent getView(TreePath tp)
	{ return this.configPanel; }
	
	public void resetInputs(ProjectFile pf){
		this.serverSrcField.setText("");
		this.adminSrcField.setText("");
		
		if(pf != null){
			RbdrApplication ra = this.editor.getRbdrApplication(pf);
			if(ra.getServerSrc() != null)
				this.serverSrcField.setText(ra.getServerSrc().getAbsolutePath());
			else
				this.serverSrcField.setText("");
			if(ra.getAdministratorSrc() != null)
				this.adminSrcField.setText(ra.getAdministratorSrc().getAbsolutePath());
			else
				this.adminSrcField.setText("");
			
			this.adminClsNameField.setText(ra.getAdminClassName());
			this.serverClsNameField.setText(ra.getServerClassName());
		}
	}// end resetInputs(ProjectFile)
	
	public boolean save(){
		try{
			RbdrApplication ra = (RbdrApplication)this.editor.getModel();
			ra.setServerSrc(new File(this.serverSrcField.getText()));
			ra.setServerClassName(this.serverClsNameField.getText());
			ra.setAdministratorSrc(new File(this.adminSrcField.getText()));
			ra.setAdminClassName(this.adminClsNameField.getText());
		}catch(NullPointerException npe){
			npe.printStackTrace();
			return false;
		}
		return true;
	}// end save()
}
