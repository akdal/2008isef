package agdr.robodari.plugin.rae.lang;

import java.util.*;
import agdr.library.lang.*;
import agdr.robodari.appl.*;
import agdr.robodari.edit.*;
import agdr.robodari.comp.*;
import agdr.robodari.comp.info.*;
import agdr.robodari.store.*;
import agdr.robodari.plugin.rae.lang.parser.*;


public class BASICRunner{
	private final static String ERRCNT_UNKNIDTF = "Unknown identifier.";
	private final static String ERRCNT_DUPLVAR = "Duplicated variable name.";
	private final static String ERRCNT_CANNOTCHANGECONST = "Cannot change constant variable.";
	private final static String ERRCNT_TYPEMISMATCHED = "Type mismatched : ";
	private final static String ERRCNT_IF_WRONGCOND = "Incompatible condition : only boolean type is allowed.";
	private final static String ERRCNT_WHILE_WRONGCOND = "Incompatible condition : only boolean type is allowed.";
	private final static String ERRCNT_FOR_ONLYINTAVAIL = "Only integer type is acceptable for init var.";
	
	
	private Stack<VarList> varstack = new Stack<VarList>();
	private WorkingState workingState;
	
	private Block root;
	private Console console;
	private boolean halt = false;
	
	private FunctionInvoker funcInvoker;
	private CommandInvoker commandInvoker;
	private ConstantPool constPool;
	
	
	public Block getRoot()
	{ return root; }
	
	
	public synchronized ResultType start(Block root, Console console,
			FunctionInvoker funcInvkr,
			CommandInvoker cmdInvoker,
			ConstantPool constPool,
			ErrorBundle bundle){
		workingState = WorkingState.PREPARING;
		
		this.funcInvoker = funcInvkr;
		this.commandInvoker = cmdInvoker;
		this.constPool = constPool;
		this.halt = false;
		
		this.console = console;
		this.varstack.clear();
		this.root = root;
		
		workingState = WorkingState.WORKING;
		ResultType result = runBlock(root, bundle);
		workingState = WorkingState.UNEMPLOYED;
		return result;
	}// end startDebugging(CodeStructure[], Object)
	public synchronized void exit() throws Exception{
		workingState = WorkingState.UNEMPLOYED;
	}// end exitDebugging
	
	
	
	
	
	private ResultType runBlock(Block block, ErrorBundle bundle){
		if(this.halt)
			return ResultType.HALTED;
		
		BugManager.println("BASICRunner : runBlock : class : " + block.getClass());
		
		ResultType res = ResultType.DONE;
		this.varstack.push(new VarList());
		
		if(block instanceof IfBlock){
			IfBlock ib = (IfBlock)block;
			return _procIfBlock(ib, bundle);
		}else if(block instanceof ForBlock){
			ForBlock fb = (ForBlock)block;
			return _procForBlock(fb, bundle);
		}else if(block instanceof WhileBlock){
			WhileBlock wb = (WhileBlock)block;
			return _procWhileBlock(wb, bundle);
		}else{
			int lim = block.getElementCount();
			
			for(int i = 0; i < lim; i++){
				Element elem = block.getElement(i);
				
				if(elem instanceof Statement){
					ResultType stmtres = runStmt((Statement)elem, bundle);
					if(stmtres != ResultType.DONE){
						res = stmtres;
						break;
					}
				}else{
					ResultType blckres = runBlock((Block)elem, bundle);
					if(blckres != ResultType.DONE){
						res = blckres;
						break;
					}
				}
			}
		} 

		this.varstack.pop();
		return res;
	}// end runBlock
		private ResultType _procIfBlock(IfBlock ifBlock, ErrorBundle bundle){
			BugManager.printf("BASICRUnner : _procIfBlock : %s\n", ifBlock);
			ResultType res = null;
			Receptor receptor = new Receptor();
			receptor.bundle = bundle;
			
			Valuable cond = null;
			Block execBlock = null;
			Boolean condVal = null;
			
			
			cond = ifBlock.getIfCondition();
			execBlock = ifBlock.getIfBlock();
			if((res = this.getValue(ifBlock, cond, receptor)) != ResultType.DONE)
				return res;
			
			if(!(receptor.value instanceof Boolean)){
				bundle.addErrorMessage(new ErrorMessage(ifBlock,
						cond, cond.getCodeRange(), ERRCNT_IF_WRONGCOND, ErrorTypes.RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			condVal = (Boolean)receptor.value;
			if(condVal)
				return runBlock(execBlock, bundle);
			
			
			for(int i = 0; i < ifBlock.getElseifBlocksCount(); i++){
				cond = ifBlock.getElseifCondition(i);
				execBlock = ifBlock.getElseifBlock(i);
				
				if((res = this.getValue(ifBlock, cond, receptor)) != ResultType.DONE)
					return res;
				
				if(!(receptor.value instanceof Boolean)){
					bundle.addErrorMessage(new ErrorMessage(ifBlock,
							cond, cond.getCodeRange(), ERRCNT_IF_WRONGCOND, ErrorTypes.RUNTIME_ERROR));
					return ResultType.RUNTIME_ERROR;
				}
				condVal = (Boolean)receptor.value;
				if(condVal)
					return runBlock(execBlock, bundle);
			}
			
			if(ifBlock.hasElseBlock()){
				return runBlock(ifBlock.getElseBlock(), bundle);
			}
			return ResultType.DONE;
		}// end _procIfBlock
		private ResultType _procWhileBlock(WhileBlock block, ErrorBundle bundle){
			ResultType res = ResultType.DONE;
			Valuable cond = block.getCondition();
			Receptor recp = new Receptor();
			recp.bundle = bundle;
			
			int lim = block.getElementCount();
			boolean breakfound = false;
			
			while((res = this.getValue(block, cond, recp)) == ResultType.DONE && !breakfound){
				if(!(recp.value instanceof Boolean)){
					res = ResultType.RUNTIME_ERROR;
					bundle.addErrorMessage(new ErrorMessage(block,
								cond, cond.getCodeRange(), ERRCNT_WHILE_WRONGCOND, ErrorTypes.RUNTIME_ERROR));
					break;
				}
				if(!(Boolean)recp.value)
					break;
				
				for(int i = 0; i < lim; i++){
					Element elem = block.getElement(i);
					if(elem instanceof Block)
						res = runBlock((Block)elem, bundle);
					else
						res = this.runStmt((Statement)elem, bundle);
					
					if(res == ResultType.CONTINUE){
						res = ResultType.DONE;
						i = lim + 1;
					}else if(res == ResultType.BREAK){
						breakfound = true;
						res = ResultType.DONE;
						break;
					}
					if(res != ResultType.DONE)
						return res;
				}
			}
			return res;
		}// end _procWhileBlock
		private ResultType _procForBlock(ForBlock block, ErrorBundle bundle){
			ResultType res = ResultType.DONE;
			
			String identifier = block.getTarget();
			Receptor receptor = new Receptor();
			receptor.bundle = bundle;
			
			Valuable stValue = block.getStart();
			Object objStVal;
			int iStVal;
			Valuable goalValue = block.getEnd();
			Object objGoalVal;
			int iGoalVal;
			Object objCurrVal;
			int iCurrVal;
			
			
			// variable type check
			if(!this.getValue(identifier, receptor)){
				bundle.addErrorMessage(new ErrorMessage(block,
						block,
						block.getCodeRange(),
						ERRCNT_UNKNIDTF,
						ErrorTypes.RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			if(receptor.type != ValueType.TYPE_INT){
				bundle.addErrorMessage(new ErrorMessage(block,
						block, block.getCodeRange(), ERRCNT_FOR_ONLYINTAVAIL, ErrorTypes.RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			
			// start value type check & setting
			res = this.getValue(block, stValue, receptor);
			if(res != ResultType.DONE) return res;
			
			if(receptor.type != ValueType.TYPE_INT){
				bundle.addErrorMessage(new ErrorMessage(block,
						block, block.getCodeRange(), ERRCNT_FOR_ONLYINTAVAIL, ErrorTypes.RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			objStVal = receptor.value;
			iStVal = ((Number)objStVal).intValue();
			
			// goal value type check & setting
			res = this.getValue(block, goalValue, receptor);
			if(res != ResultType.DONE) return res;
			
			if(receptor.type != ValueType.TYPE_INT){
				bundle.addErrorMessage(new ErrorMessage(block,
						block, block.getCodeRange(), ERRCNT_FOR_ONLYINTAVAIL, ErrorTypes.RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			objGoalVal = receptor.value;
			iGoalVal = ((Number)objGoalVal).intValue();
			
			// initializing
			res = this.setValue(block, identifier, stValue, bundle);
			if(res != ResultType.DONE)
				return res;
			
			
			// 시작!
			objCurrVal = objStVal;
			iCurrVal = iStVal;
			int lim = block.getElementCount();
			boolean breakfound = false;
			do{
				for(int i = 0; i < lim; i++){
					Element elem = block.getElement(i);
					if(elem instanceof Block)
						res = runBlock((Block)elem, bundle);
					else
						res = this.runStmt((Statement)elem, bundle);
					
					if(res == ResultType.CONTINUE){
						res = ResultType.DONE;
						break;
					}else if(res == ResultType.BREAK){
						breakfound = true;
						res = ResultType.DONE;
						break;
					}
					if(res != ResultType.DONE)
						return res;
				}

				this.getValue(identifier, receptor);
				objCurrVal = receptor.value;
				iCurrVal = ((Number)objCurrVal).intValue();
				iCurrVal++;
				this.setValue(block, identifier, new Value(null, receptor.type, iCurrVal), bundle);
			}while(iGoalVal >= iCurrVal && !breakfound);
			
			return res;
		}// end _procForBlock
	
		
		
	
	
	private ResultType runStmt(Statement stmt, ErrorBundle bundle){
		if(this.halt)
			return ResultType.HALTED;
		
		if(stmt instanceof DeclareStatement){
			DeclareStatement ds = (DeclareStatement)stmt;
			VarList list = this.varstack.peek();
			String idtf = null;
			ValueType vtype;
			
			for(int i = 0; i < ds.getCount(); i++){
				idtf = ds.getIdentifier(i);
				vtype = ds.getValueType(i);
				
				for(int j = 0; j < this.varstack.size(); j++){
					VarList jlist = this.varstack.get(j);
					
					if(jlist.varTypeTable.containsKey(idtf) || jlist.constTypeTable.containsKey(idtf)){
						bundle.addErrorMessage(new ErrorMessage(stmt.getParent(),
								stmt, stmt.getCodeRange(), ERRCNT_DUPLVAR, ErrorTypes.RUNTIME_ERROR));
						return ResultType.RUNTIME_ERROR;
					}
				}
				
				if(vtype == ValueType.TYPE_INT)
					list.varTable.put(idtf, new Integer(0));
				else if(vtype == ValueType.TYPE_BOOLEAN)
					list.varTable.put(idtf, Boolean.FALSE);
				else if(vtype == ValueType.TYPE_STRING)
					list.varTable.put(idtf, "");
				list.varTypeTable.put(ds.getIdentifier(i), ds.getValueType(i));
			}
			return ResultType.DONE;
			
			
		}else if(stmt instanceof ConstStatement){
			ConstStatement ds = (ConstStatement)stmt;
			String idtf = ds.getIdentifier();
			
			for(int j = 0; j < this.varstack.size(); j++){
				VarList jlist = this.varstack.get(j);
				
				if(jlist.varTypeTable.containsKey(idtf) || jlist.constTypeTable.containsKey(idtf)){
					bundle.addErrorMessage(new ErrorMessage(stmt.getParent(),
							stmt, stmt.getCodeRange(), ERRCNT_DUPLVAR, ErrorTypes.RUNTIME_ERROR));
					return ResultType.RUNTIME_ERROR;
				}
			}
			VarList list = this.varstack.peek();
			list.constTypeTable.put(ds.getIdentifier(), ValueType.TYPE_INT);
				
			return ResultType.DONE;
			
			
		}else if(stmt instanceof InsertStatement){
			InsertStatement is = (InsertStatement)stmt;
			return this.setValue(is, is.getLeft(), is.getRight(), bundle);
			
			
		}else if(stmt instanceof CommandStatement){
			CommandStatement cs = (CommandStatement)stmt;
			return this.commandInvoker.invokeCmd(cs, cs.getCommand(), cs.getParameters(), bundle, this);
		}else if(stmt instanceof Comment){
			return ResultType.DONE;
		}else if(stmt instanceof BreakStatement){
			return ResultType.BREAK;
		}else if(stmt instanceof ContinueStatement)
			return ResultType.CONTINUE;
		throw new IllegalArgumentException(stmt.getClass().toString());
	}// end runStmt
	
	
	
	
	
	
	public ResultType getValue(CodeTrackable focus, Valuable val, Receptor rcp){
		if(this.halt)
			return ResultType.HALTED;
		
		if(val == null){
			rcp.value = null;
			rcp.type = null;
			return ResultType.RUNTIME_ERROR;
			
			
		}else if(val instanceof Value){
			rcp.value = ((Value)val).getValue();
			rcp.type = ((Value)val).getValueType();
			return ResultType.DONE;
			
			
		}else if(val instanceof VariableCallValue){
			VariableCallValue vcv = (VariableCallValue)val;
			
			if(!getValue(vcv.getTarget(), rcp)){
				rcp.bundle.addErrorMessage(new ErrorMessage(focus,
						val,
						val.getCodeRange(),
						ERRCNT_UNKNIDTF,
						ErrorTypes.RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			return ResultType.DONE;
			
			
		}else if(val instanceof FunctionCallValue){
			FunctionCallValue fcv = (FunctionCallValue)val;
			return this.funcInvoker.invokeFunction(focus, fcv, fcv.getFunction(), fcv.getParameters(), rcp, this);
		
		
		}else if(val instanceof Expression){
			Expression expr = (Expression)val;
			return expr.getOperator().calc(focus, expr.getLeft(), expr.getRight(), rcp, this);
		}
		throw new IllegalArgumentException("-_- 이상한거 : " + val.getClass());
	}// end getValue
	
	public boolean getValue(String identifier, Receptor rcp){
		if(this.constPool.isConstant(identifier)){
			Value v = this.constPool.getConstValue(identifier);
			rcp.type = v.getValueType();
			rcp.value = v.getValue();
			return true;
		}
		identifier = identifier.toLowerCase();
		for(int i = 0; i < varstack.size(); i++){
			VarList vl = varstack.get(i);
			
			if(vl.varTypeTable.containsKey(identifier)){
				rcp.value = vl.varTable.get(identifier);
				rcp.type = vl.varTypeTable.get(identifier);
				return true;
			}
		}
		return false;
	}// end getValue(String)
	public ResultType setValue(Element elem, String idtf, Valuable val, ErrorBundle bundle){
		if(this.halt)
			return ResultType.HALTED;
		
		Receptor receptor = new Receptor();
		receptor.bundle = bundle;
		ValueType currentType;
		
		for(int i = 0; i < this.varstack.size(); i++){
			VarList jlist = this.varstack.get(i);
			
			if(jlist.constTypeTable.containsKey(idtf)){
				bundle.addErrorMessage(new ErrorMessage(elem,
						elem, elem.getCodeRange(), ERRCNT_CANNOTCHANGECONST, ErrorTypes.RUNTIME_ERROR));
				
				return ResultType.RUNTIME_ERROR;
			}else if(jlist.varTypeTable.containsKey(idtf)){
				ResultType rt = this.getValue(elem, val, receptor);
				if(rt != ResultType.DONE)
					return rt;
				
				currentType = jlist.varTypeTable.get(idtf);
				if(currentType != null && currentType != receptor.type){
					// cannot convert.
					bundle.addErrorMessage(new ErrorMessage(elem,
							elem,
							elem.getCodeRange(),
							ERRCNT_TYPEMISMATCHED + currentType + " , " + receptor.type,
							ErrorTypes.RUNTIME_ERROR));
					return ResultType.RUNTIME_ERROR;
				}
				
				jlist.varTable.put(idtf, receptor.value);
				jlist.varTypeTable.put(idtf, receptor.type);
				return ResultType.DONE;
			}
		}
		bundle.addErrorMessage(new ErrorMessage(elem, elem,
				elem.getCodeRange(), ERRCNT_UNKNIDTF, ErrorTypes.RUNTIME_ERROR));
		
		return ResultType.RUNTIME_ERROR;
	}// end setValue
	
	
	
	
	public WorkingState getState()
	{ return workingState; }
	
	public void halt()
	{ this.halt = true; }
	
	public Console getConsole()
	{ return console; }
	public void setConsole(Console cons)
	{ this.console = cons; }
	
	
	
	
	
	static class VarList{
		Hashtable<String, Object> varTable = new Hashtable<String, Object>();
		Hashtable<String, ValueType> varTypeTable = new Hashtable<String, ValueType>(); 
		Hashtable<String, Object> constTable = new Hashtable<String, Object>();
		Hashtable<String, ValueType> constTypeTable = new Hashtable<String, ValueType>();
	}// end class VarList
}
