package agdr.robodari.plugin.rae.lang;

import agdr.library.lang.*;
import agdr.robodari.plugin.rae.lang.parser.*;

public interface CommandInvoker {
	public ResultType invokeCmd(CommandStatement focus, String cmdName, Valuable[] args, ErrorBundle bundle, BASICRunner runner);
	public boolean isCommand(String cmdName);
}
