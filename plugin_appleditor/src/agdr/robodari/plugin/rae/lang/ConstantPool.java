package agdr.robodari.plugin.rae.lang;

import agdr.robodari.plugin.rae.lang.parser.*;

public interface ConstantPool{
	public boolean isConstant(String identf);
	public Value getConstValue(String identf);
}
