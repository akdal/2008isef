package agdr.robodari.plugin.rae.lang;

import agdr.library.lang.*;
import agdr.robodari.plugin.rae.lang.parser.*;

public interface FunctionInvoker {
	public ResultType invokeFunction(CodeTrackable focus, FunctionCallValue fcv,
			String f, Valuable[] args, Receptor rec, BASICRunner brun);
	public boolean isFunction(String functionName);
}
