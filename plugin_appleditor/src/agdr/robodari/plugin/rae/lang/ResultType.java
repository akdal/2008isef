package agdr.robodari.plugin.rae.lang;

public enum ResultType {
	HALTED, EXITED, RUNTIME_ERROR, DONE, BREAK, CONTINUE;
}
