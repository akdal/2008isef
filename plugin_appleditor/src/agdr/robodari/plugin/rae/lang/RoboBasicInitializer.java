package agdr.robodari.plugin.rae.lang;

import javax.swing.text.*;

import java.util.*;
import java.awt.Color;
import java.awt.Font;

public class RoboBasicInitializer{
	
	static StyleContext registerStyles(){
		StyleContext sc = new StyleContext();
		Style cmdStyle = sc.addStyle("roboBasic_command", null);
		StyleConstants.setForeground(cmdStyle, Color.BLUE);

		Style textStyle = sc.addStyle("roboBasic_text", null);
		
		Style commentStyle = sc.addStyle("roboBasic_comment", null);
		StyleConstants.setForeground(commentStyle, Color.gray);

		Style commentSignStyle = sc.addStyle("roboBasic_comment_sign", null);
		StyleConstants.setForeground(commentSignStyle, Color.DARK_GRAY);

		Style numberStyle = sc.addStyle("roboBasic_number", null);
		StyleConstants.setForeground(numberStyle, Color.BLACK);

		Style funcStyle = sc.addStyle("roboBasic_function", null);
		StyleConstants.setForeground(funcStyle, Color.RED);
		
		Style opStyle = sc.addStyle("roboBasic_operator", null);
		StyleConstants.setForeground(opStyle, new Color(0, 64, 64));
		
		Style prepStyle = sc.addStyle("roboBasic_preprocessor", null);
		StyleConstants.setForeground(prepStyle, Color.orange);

		Style resStyle = sc.addStyle("roboBasic_reserved", null);
		StyleConstants.setForeground(resStyle, new Color(0, 0, 128));
		StyleConstants.setItalic(resStyle, true);

		Style rdxsgnStyle = sc.addStyle("roboBasic_radix_sign", null);
		StyleConstants.setForeground(rdxsgnStyle, new Color(0, 128, 128));
		StyleConstants.setItalic(rdxsgnStyle, true);
		
		Style rdxtypStyle = sc.addStyle("roboBasic_radix_type", null);
		StyleConstants.setForeground(rdxtypStyle, Color.red);
		StyleConstants.setItalic(rdxtypStyle, true);

		Style prthsisStyle = sc.addStyle("roboBasic_parenthesis", null);
		StyleConstants.setForeground(prthsisStyle, Color.black);
		StyleConstants.setBold(prthsisStyle, true);
		
		
		//public final static String KEY_COMMAND = "command";
		//public final static String KEY_OPERATOR = "operator";
		//public final static String KEY_NUMBER = "number";
		///public final static String KEY_STRING = "string";
		//public final static String KEY_QUOT = "quot";
		//public final static String KEY_CMTSTART = "commentStart";
		//public final static String KEY_DOLLAR = "dollar";
		//public final static String KEY_CMT = "comment";
		//public final static String KEY_TEXT = "text";
		
		return sc;
	}// end registerStyles
}
