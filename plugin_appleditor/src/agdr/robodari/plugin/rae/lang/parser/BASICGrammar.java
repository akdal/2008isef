package agdr.robodari.plugin.rae.lang.parser;


public class BASICGrammar {
	public final static String[] RESERVED = new String[]{
		"DIM", "INTEGER", "STRING", "BOOLEAN", "AS", "CONST",
		"FOR", "NEXT", "TO", "IF", "THEN", "ELSEIF", "ENDIF", "WHILE", "WEND", "BREAK", "CONTINUE",
		"AND", "OR", "XOR"};
	public final static String ERRMSG_RESERVED = "Given literal is reserved word.";
	public final static String ERRMSG_WRONG = "Given string is wrong.";
	
	public static boolean isReserved(String str){
		str = str.toUpperCase();
		for(String eachc : RESERVED){
			if(eachc.equals(str))
				return true;
		}
		return false;
	}// end isResreved
	
	public static boolean isDigitSet(String str){
		if(str.length() == 0)
			return false;
		
		for(int i = 0; i < str.length(); i++)
			if(!Character.isDigit(str.charAt(i)))
				return false;
		return true;
	}// end isDigitSet(STring)
	
	public static String checkVarName(String str){
		if(isReserved(str))
			return ERRMSG_RESERVED;
		else if(Operator.isOperator(str))
			return ERRMSG_RESERVED;
		
		if(!Character.isJavaIdentifierStart(str.charAt(0)) || str.charAt(0) == '$')
			return ERRMSG_WRONG;
		
		for(int i = 0; i < str.length(); i++){
			char chr = str.charAt(i);
			if(!Character.isDefined(chr))
				return ERRMSG_WRONG;
			else if(!Character.isJavaIdentifierStart(str.charAt(0)) ||
					str.charAt(0) == '$')
				return ERRMSG_WRONG;
		}
		
		return null;
	}// end isProperNAme(String, int)
}
