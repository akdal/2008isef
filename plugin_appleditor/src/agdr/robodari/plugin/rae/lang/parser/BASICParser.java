package agdr.robodari.plugin.rae.lang.parser;

import java.io.*;
import java.util.*;

import agdr.library.lang.*;
import agdr.robodari.appl.*;


/**
 * By. June-Young Lee. 2007. 2.14, Finished!
 */

public class BASICParser implements Parser<Block>{
	/**
	 * 변수 선언 : 변수선언 중 갑자기 멈추었을 때.
	 */
	private final static String ERRCNT_VARDEC_NOTEND = "Incomplete declaration statement.";
	/**
	 * 변수 선언 : 변수선언 중 as차례에서 as가 나오지 않았을 때.
	 */
	private final static String ERRCNT_VARDEC_NOTAS = "'AS' token should be here.";
	/**brea
	 * 변수 선언 : 변수선언 중 올바르지 않은 타잎이 나왔을 때.
	 */
	private final static String ERRCNT_VARDEC_NOTTYPE = "Unknown type";
	/**
	 * 잘못된 토큰인 경우
	 */
	private final static String ERRCNT_WRONGTOKEN = "Wrong token.";

	/**
	 * 문자열이 끝나지 않았을 때 나는 grammar 에러.
	 */
	private final static String ERRCNT_STRINGNOTEND = "String is not terminated.";

	/**
	 * 원래 아무것도 나오지 말아야 하는데 나왔을 때.
	 */
	private final static String ERRCNT_NOTADMITTED = "Wrong token.";

	/**
	 * 잘못된 문장일 경우
	 */
	private final static String ERRCNT_WRONGSENT = "Wrong sentence.";

	/**
	 * 공백이 와야 하는 경우
	 */
	private final static String ERRCNT_ONLYSPACE = "Whitespace is required.";


	/**
	 * 대입문이 중간에서 끊겨버린 경우.
	 */
	private final static String ERRCNT_INSSTMT_NOTEND = "Insertion statement is not finished.";

	/**
	 * expression에서 연산자 다음 값이 나오자 않은 경우
	 */
	private final static String ERRCNT_NOTVALUE = "Value is required after operator.";

	/**
	 * 읽는 도중에 쉼표가 나와야 하는 경우.
	 */
	private final static String ERRCNT_NOTCOMMA = "Comma(,) should be here.";

	/**
	 * 상수 선언 도중에 끊겼을 경
	 */
	private final static String ERRCNT_CONST_NOTEND = "Constant declaration is not terminated.";

	/**
	 * =이 들어가야 하는데 없는 경우
	 */
	private final static String ERRCNT_NOTEQUALOP = "Operator = required";

	/**
	 * 정수 범위 이상의 값이 들어온 경우
	 */
	private final static String ERRCNT_TOOBIGVALUE = "Too large value.";
	

	/**
	 * 변수나 상수 선언에서 잘못된 이름인 경우
	 */
	private final static String ERRCNT_EMPTYNAME = "Wrong name declaration.";
	
	private final static String ERRCNT_GOTO_NEEDSPACE = "Whitespace needed after 'GOTO' token.";
	
	private final static String ERRCNT_GOTO_NOTEND = "GOTO statement is not finished.";

	/**
	 * 함수가 끝나지 않았는데 도중에 끊긴 경우
	 */
	private final static String ERRCNT_FUNC_NOTEND = "Function statement is not closed.";

	/**
	 * 괄호가 열리지 않은 경우
	 */
	private final static String ERRCNT_FUNC_NOTOPPRTHSIS = "Opening parenthesis needed here.";

	/**
	 * 괄호가 닫기지 않은 경우
	 */
	private final static String ERRCNT_IF_THEN_NOTBLANK = "Whitespace needed before THEN token.";

	/**
	 * 괄호가 닫기지 않은 경우
	 */
	private final static String ERRCNT_IF_NOTEND = "IF block is not closed.";

	/**
	 * 괄호가 닫기지 않은 경우
	 */
	private final static String ERRCNT_IF_NOSPACE = "Whitespace is required after IF token.";

	/**
	 * if문을 닫을 수 없는경우
	 */
	private final static String ERRCNT_IF_NOCLOSE = "No IF block is available for termination.";

	/**
	 * if문이 덜 닫힌 경우
	 */
	private final static String ERRCNT_IF_NOTCLOSED = "IF block is not closed.";

	/**
	 * if문의 조건이 잘못되었을 경우
	 */
	private final static String ERRCNT_IF_WRONGCOND = "Incompatible condition.";

	/**
	 * elseif 선언에서 if문이 존재하지 않는 경우
	 */
	private final static String ERRCNT_ELSEIF_NEEDIF = "IF block does not exist.";

	/**
	 * elseif가 종결되지 않은 경우
	 */
	private final static String ERRCNT_ELSEIF_NOTEND = "ELSEIF block is not closed.";

	/**
	 * elseif 선언에서 elseif 뒤에 공백이 없는경우
	 */
	private final static String ERRCNT_ELSEIF_NOSPACE = "Whitespace needed after ELSEIF token.";

	/**
	 * else 선언에서 if가 없는경우
	 */
	private final static String ERRCNT_ELSE_NEEDIF = "IF block is required in order to use ELSE block.";
	
	/**
	 * for문이 종결되지 않은 경우
	 */
	private final static String ERRCNT_FOR_NOTEND = "FOR block is not closed.";
	/**
	 * for문에서 for 다음에 공백이 와야 하는 경우
	 */
	private final static String ERRCNT_FOR_NEEDSPACEAFTERFOR = "Whitespace is required after 'for'.";
	/**
	 * for문에서 =이 오지 않은경우
	 */
	private final static String ERRCNT_FOR_NOEQUAL = "Operator = required.";
	/**
	 * for문에서 to 다음에 공백이 오지 않은경우
	 */
	private final static String ERRCNT_FOR_NEEDBLANKAFTERTO = "Whitespace needed after TO token.";
	/**
	 * for문에서 to가 오지 않은 경우
	 */
	private final static String ERRCNT_FOR_NOTO = "TO token must be here.";
	/**
	 * for문이 닫히지 않은경우.
	 */
	private final static String ERRCNT_FOR_NOTCLOSED = "FOR block is not closed ; NEXT token needed";
	
	
	/**
	 * next에서 변수가 존재하지 않는 것인 경우
	 */
	private final static String ERRCNT_NEXT_WRONGVAR = "Mismatching variable name.";
	/**
	 * next문이 종결되지 않은 경우
	 */
	private final static String ERRCNT_NEXT_NOTEND = "NEXT statement is not closed.";
	/**
	 * next문이 종결되지 않은 경우
	 */
	private final static String ERRCNT_NEXT_FORNOTAVAILABLE = "FOR block matching NEXT statement does not exist.";
	/**
	 * next 다음에 공백이 오지않은 경우
	 */
	private final static String ERRCNT_NEXT_NEEDSPACE = "Whitespace is needed after NEXT token.";
	
	private final static String ERRCNT_WHILE_NEEDSPACE = "Whitespace is required after WHILE token.";
	
	private final static String ERRCNT_WHILE_NOCONDITION = "Condition needed";
	
	private final static String ERRCNT_WHILE_NOTEND = "While block is not closed ; WEND needed";
	
	private final static String ERRCNT_WEND_WHILENOTAVAILABLE = "WHILE block matching WEND statement does not exist.";
	
	private final static String ERRCNT_NOLOOP = "No loop block";
	
	// FIXME Constant Pool
	
	
	private final static boolean DEBUG = true;
	private final static boolean ERRPRINT = true;
	
	
	
	
	private final static String linebreak = "\n";

	public static void main(String[] ar){
		StringReader fr = null;
		BufferedReader br = null;
		
		//StringBuffer buf = new StringBuffer();
		//String source = null;
		ErrorBundle bundle = new ErrorBundle();
		
		String str = 
			"DIM a AS INTEGER, b AS STRING 'comment \n" + 
			"a = (1+";
		try{
			fr = new StringReader(str);
			br = new BufferedReader(fr);
			BASICParser p = new BASICParser();

			p.parse(br, bundle);
		}catch(Exception excp){
			excp.printStackTrace();
		}finally{
			try{
				br.close();
				fr.close();
			}catch(IOException excp){
				excp.printStackTrace();
			}
		}
	}// end main(String[]
	

	private static EventShooter eventShooter = new EventShooter();
	private Vector<ParsingListener> listeners = new Vector<ParsingListener>();
	private WorkingState state = WorkingState.UNEMPLOYED;

	
	
	
	private void logln(Object obj)
	{ this.logln(obj.toString()); }
	private void logln(String str)
	{ if(DEBUG) System.out.println(str); }
	private void log(Object obj)
	{ if(obj == null) this.log("null");
	else this.log(obj.toString()); }
	private void log(String str)
	{ if(DEBUG) System.out.print(str); }
	private void logln()
	{ if(DEBUG) System.out.println(); }

	private void printerror(ErrorMessage msg)
	{ if(ERRPRINT) System.out.println(msg); }
	private void printerror(String msg)
	{ if(ERRPRINT) System.out.println(msg); }

	private Queue<Token> tokenQueue = new LinkedList<Token>();
	public synchronized Block parse(Reader reader, ErrorBundle bundle)
			throws IOException, NullPointerException {
		try{
			logln("=============== START BASICParser!!! ===========");
			//eventShooter.start();
			tokenQueue.clear();
			fireParsingStart(reader);
			long startmillitime = System.currentTimeMillis();

			state = WorkingState.PREPARING;
			BASICTokenizer.tokenize(tokenQueue, reader);
			Token[] tokens = (Token[])tokenQueue.toArray(new Token[]{});
			logln("token : " + tokens.length);

			state = WorkingState.WORKING;

			Object[] elems = cohere(tokens, bundle);
			logln("----- coherence result -----");
			logln("length : " + elems.length);
			for(Object eachelem : elems)
				logln(eachelem + " (" + eachelem.getClass() + ")");
			
			printerror("--- fatal errors ---");
			for(ErrorMessage msg : bundle.getErrorMessages())
				printerror(msg);

			// 진짜 parsing 시작

			Object[] params = new Object[2048];
			// 사용중인 params의 처음부터의 총 길이
			int paramlen = 0;
			// params에서 시작하는 구간.
			int[] startps = new int[1024];
			int spslen = 0;
			// 어떤 값이 실제 코드상에서 시작하는 위치(일종의 startidx).
			int[] vstartidcs = new int[1024];
			int vsilen = 0;
			// FloatingCodeRange[] fcranges = new FloatingCodeRange[30];
			// int fcrlen = 0;

			int totallen = 0;
			if(tokens.length != 0)
				totallen = tokens[tokens.length - 1].getStartPosition() + 
					tokens[tokens.length - 1].getLength();

			Block root = new PlainBlock(new CodeRangeImpl(0, totallen),
					null);
			Block focus = root;

			// 진짜 parsing을 시작한다.
			/*
			 * -2 : 주석을 제외한 어떤 것도 올 수 없다.
			 * -1 : 아무것도 하지 않음. 
			 * 0 : 변수 선언 
			 * 1 : 명령어 선언.
			 * 2 : 값 읽기. 
			 * 3 : 상수 선언. 
			 * 4 : 대입 식 읽기. 
			 * 5 : 함수 읽기 
			 * 6 : label문(int) <- deprecated
			 * 7 : label문 <- deprecated
			 * 8 : if문 bitcount
			 * 9 : endif문 
			 * 10 : elseif문 
			 * 11 : else문 
			 * 12 : for문 
			 * 13 : next문
			 * 14 : goto문 <- deprecated
			 * 15 : while문
			 * 16 : wend문
			 * 17 : break
			 */
			/*
			 * -1 : 아무것도 하지 않음.
			 * -2. 아무것도 허락하지 않을 때
			 *  1 : 계속 받아들임. 
			 * 0. 변수선언일 때 
			 *  0 : dim 다음 공백을 기다림. 
			 *  1 : 공백 다음 변수. 
			 *  2 : 변수 다음 공백 
			 *  3 : 공백 다음 as 
			 *  4 : as 다음 공백 
			 *  5 : 공백 다음 type 
			 *  6 : type 다음 공백. 
			 *  7 : 공백 다음 쉼표. 
			 *  8 : 쉼표 다음 공백. 
			 * 1. 명령어일 때 
			 *  0 : 명령어 읽음
			 *  1 : 명령어 다음 공백을 기다림. 
			 *  2 : 공백 다음 값을 읽음. 
			 *  3 : 값 다음 공백을 읽음. 
			 *  4 : 공백 다음 쉼표를 읽음. 
			 *  5 : 쉼표 다음 공백을 읽음. 
			 * 2. 값 읽기 
			 *  1 : 값을 읽음.(경우에 따라 subaction : xx, action : y로 감) 
			 *  2 : 값을 읽은 다음 공백을 읽음. 
			 *  3 : 공백을 읽은 다음 operator를 읽음. 
			 *  4 : operator를 읽은 다음 공백을 읽음. 
			 *  7 : 함수 읽기 : 함수 이름. 
			 *  8 : 함수 읽기 : 완료. 
			 *  9 : 괄호 내 새로운 값 읽기 : opening parenthesis에 focus맞춤
			 *  10 : 괄호 내 새로운 값 읽기 : 완료, focus : )에 있음.
			 *  99 ; 끝내기, Expression생성하기
			 * 3. 상수 선언시에
			 *  0 : const 다음 공백 
			 *  1 : 공백 다음 변수명 
			 *  2 : 변수명 다음 공백 
			 *  3 : 공백 다음 = 
			 *  4 : = 다음 공백 
			 *  5 : 공백 다음 값 
			 *  6 : 값 다음 호출. 
			 * 4. 대입 식 생성시에 
			 *  0 : 변수명 읽기
			 *  1 : 변수명 다음 공백 
			 *  2 : 공백 다음 = 
			 *  3 : = 다음 공백 
			 *  4 : 공백 다음 값 
			 *  6 : (함수인지 체크를 위해) 값 다음 다른 값 
			 *  5 : 값 다음 호출. * 주의!!!!!! 6, 5와 순서가 바뀌어 있음. 
			 * 5. 함수 호출
			 *  1 : 함수명 읽기 
			 *  2 : 함수명 다음 공백 
			 *  3 : 공백 다음 열린 괄호
			 *  4 : 괄호 다음 공백 
			 *  5 : 공백 다음 값
			 *  6 : 값 다음 공백 
			 *  7 : 공백 다음 쉼표 
			 *  8 : 쉼표 다음 공백 
			 *  9 : 닫는 괄호(마무리) 
			 *  99 : 함수 읽는 도중 syntax error가 뜰 경우
			 * 6. label문(숫자). <- deprecated
			 *  0 : label 다음 공백. 
			 * 7. label문. <- deprecated
			 *  0 : label 이름 읽기 
			 *  1 : 이름 다음 :
			 *  2 : [에러용] 알고보니 대입문일 때 계속 읽어들임
			 *  3 : [에러용] 알고보니 명령어일때
			 * 8. IF문 
			 *  0 : if 다음 공백, 식 호출 
			 *  1 : 식 다음 'then' // 참고 : 공백은 식에서 처리함 
			 *  2 : 식 다음 'then'(여전히) 
			 * 9. endif문 
			 *  0 : endif 부분
			 * 10. elseif문
			 *  0 : elseif 다음 공백, 식 호출 // if문의 처리와 동일 
			 *  1 : 식 다음 'then' 
			 *  2 : 식 다음 'then' // if문의 처리와 동일함. 
			 * 11. else문 
			 *  0 : else 다음 공백.
			 * 12. for문
			 *  0 : for 다음 공백
			 *  1 : 공백 다음 변수명
			 *  2 : 변수명 다음 공백
			 *  3 : 공백 다음 =
			 *  4 : = 다음 공백
			 *  5 : 공백 다음 값
			 *  6 : 값 다음 공백
			 *  7 : 공백 다음 to
			 *  8 : to 다음 값
			 *  9 : 값 다음 끝
			 * 13. next문
			 *  0 : next 다음 공백
			 *  1 : 공백 다음 변수명
			 * 14. goto문
			 *  0 : goto 다음 공백
			 *  1 : 공백 다음 label명
			 * 15 : while문
			 *  0 : while문 다음 공백, 식 호출
			 * 16 : wend문
			 *  0 : wend부분
			 * 17 : break
			 *  0 : break 부분
			 */
			int[] subactions = new int[100];
			int[] actions = new int[100];
			int actlen = 1;
			
			subactions[0] = actions[0] = -1;
			
			// 코드상에서의 위치.
			int curidx = 0, startidx = 0;
			// 한 줄 내에서 선언된 아이템을 모아놓는 곳
			int[] declareditems = new int[10];
			int dcilen = 0;

			for(int i = 0; i < elems.length; i++){
				int elemlen = getlen(elems[i]);

				curidx += elemlen;
				//logln("-- actlen : " + actlen + ", action : " + actions[actlen - 1] + " , subaction : "
				//		+ subactions[actlen - 1] + " , startidx : " + startidx + " , curidx : " + curidx + " , elem : ["
				//		+ elems[i] + "] , focus.p : " + (focus.getParent() == null ? null : focus.getParent()
				//				.getClass().getSimpleName()) + " , paramlen : " + paramlen + ", startps[spslen - 1] : " + (spslen != 0 ? startps[spslen - 1] : "-"));
				
				/*
				log("params : ");
				for(int j = 0; j < 8; j++)
					log("[" + params[j] + "] , ");
				log("\nvstartidcs : ");
				for(int j = 0; j < vsilen; j++)
					log("[" + vstartidcs[j] + "] , ");
				log("\nstartps : ");
				for(int j = 0; j < spslen; j++)
					log("[" + startps[j] + "] , ");
				logln();
				logln("errcnt : " + bundle.getSizeOfErrorMessages());
*/
				if(elems[i].equals(linebreak)){
					// 여때까지 하던 일은 마쳐야 함.
					curidx -= linebreak.length();
					//logln("linebreak!");
					focus = knot(actions, subactions, actlen, startidx, curidx, bundle,
							paramlen, params, focus,
							vstartidcs, vsilen, startps, spslen);
					//logln("------------------");
					//logln("focus : " + focus.getClass().getSimpleName());
					focus = checkSimplifiedIfBlock(declareditems, dcilen,
							bundle, focus, root);
					//logln("focus : " + focus.getClass().getSimpleName());
					dcilen = 0;
					// FIXME

					curidx += linebreak.length();
					paramlen = 0;
					spslen = 0;
					actions[actlen - 1] = subactions[actlen - 1] = -1;
					startidx = curidx;
					vsilen = 0;

					continue;
				}

				
				
				
				
				
				
				
				
				if(actions[actlen - 1] == 0){
					// 변수 선언!
					if(!(elems[i] instanceof Token)){
						// 루프를 한단계 앞으로 돌려서 다시 시작.
						curidx -= elemlen;
						if(subactions[actlen - 1] == 6
								|| subactions[actlen - 1] == 7){
							generatedec(focus, bundle, startidx, curidx,
									params, paramlen);
						}else{
							// 다중 변수 선언을 개시하는 subaction = 6, 7일때는 ok.
							// 에러 띄운다 : '종결되지 않은 변수 선언 문장입니다.'
							/*
							logln("종결되지 않은 문장! curidx : " + curidx
									+ " , getlen : " + elemlen
									+ " , subaction : "
									+ subactions[actlen - 1]);
							*/
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(startidx, curidx
											- startidx), ERRCNT_VARDEC_NOTEND));
						}
						actions[actlen - 1] = -1;
						subactions[actlen - 1] = -1;
						i--;
						continue;
					}

					if(subactions[actlen - 1] == 0
							|| subactions[actlen - 1] == 2
							|| subactions[actlen - 1] == 4){
						//logln("subaction - "
						//		+ subactions[actlen - 1]);
						// dim 다음 또는 변수 다음 as 다음 공백.
						if(elems[i].toString().trim().length() != 0){
							// 공백이 올 자리에 공백이 오지 않는다.
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(curidx
											- elems[i].toString().length(),
											elems[i].toString().length()),
									ERRCNT_ONLYSPACE));
							if(subactions[actlen - 1] == 2){
								// 주어진 토큰을 변수이름에 붙이기로 결정.
								curidx -= elemlen;
								i--;
								subactions[actlen - 1] = 1;
								continue;
							}
						}
						if(subactions[actlen - 1] == 2){
							String errcnt = BASICGrammar.
								checkVarName(params[paramlen - 1].toString());
							if(errcnt != null){
								// 잘못된 변수명입니다!
								bundle.addErrorMessage(new GrammarErrorMessage(
										new CodeRangeImpl(curidx
												- elems[i].toString().length() -
												params[paramlen - 1].toString().length(),
												params[paramlen - 1].toString().length()),
												errcnt));
							}
						}
						subactions[actlen - 1] = subactions[actlen - 1] + 1;
					}else if(subactions[actlen - 1] == 1){
						// 변수 이름 설정.
						//logln("변수 - subaction 1. set var name : "
						//		+ elems[i] + " , paramlen : " + paramlen);

						if(paramlen == 0)
							// 초기 설정.
							paramlen = 1;

						if(elems[i].equals("as")){
							// 아직 변수명이 나오지 않았는데 바로 as 등이 나왔다.
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(startidx, curidx
											- startidx), ERRCNT_EMPTYNAME));
							curidx -= elemlen;
							i--;
							subactions[actlen - 1] = 3;
							continue;
						}

						if(params[paramlen - 1] != null)
							params[paramlen - 1] = params[paramlen - 1].toString()
									+ elems[i];
						else
							params[paramlen - 1] = elems[i];
						subactions[actlen - 1] = 2;
					}else if(subactions[actlen - 1] == 3){
						//logln("subaction 3.");
						// 공백 다음 as
						if(!elems[i].toString().toLowerCase().equals("as"))
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(startidx, curidx
											- startidx), ERRCNT_VARDEC_NOTAS));
						
						subactions[actlen - 1] = 4;
					}else if(subactions[actlen - 1] == 5){
						// 타잎 읽기
						String type = elems[i].toString();
						paramlen++;

						if(ValueType.hasType(type))
							params[paramlen - 1] = ValueType.getType(type);
						else {
							//logln("맞는 타잎이 없다?!");
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(curidx - type.length(),
											type.length()),
									ERRCNT_VARDEC_NOTTYPE));
							// 그냥 새로운 타잎을 추가-_-
							params[paramlen - 1] = new ValueType(type);
						}

						// paramlen을 두 번 더합니다.
						paramlen++;
						subactions[actlen - 1] = 6;
					}else if(subactions[actlen - 1] == 6){
						// type 다음 공백
						String str = elems[i].toString();
						if(str.trim().length() != 0){
							if(str.equals(",")){
								// 공백을 쓰지 않은 다중 변수 선언일 수 있다.
								// 한 차례 루프를 뒤로 돌린다.
								i--;
								curidx -= str.length();
							}
						}
						subactions[actlen - 1] = 7;
					}else if(subactions[actlen - 1] == 7){
						String str = elems[i].toString();
						//logln("subaction 7!");
						if(!str.equals(",")){
							// 다중 변수 선언이 아니다.
							// 일단 주어진 것으로 변수선언.
							//logln("다중 변수 선언이 아니므로 바로 선언을 들어간다.");
							generatedec(focus, bundle, startidx, curidx,
									params, paramlen);

							startidx = curidx;
							// line이 끝날때까지 코멘트를 제외한 아무것도 오면 안된다.
							// 루프를 한단계 되감음.
							i--;
							curidx -= str.length();
							actions[actlen - 1] = -2;
							subactions[actlen - 1] = -1;

							for(int j = 0; j < paramlen; j++)
								params[j] = null;
							paramlen = 0;

							continue;
						}
						// 다중 변수 선언이다.
						// 다음 올 것은 공백이라 예측.
						subactions[actlen - 1] = 8;
					}else if(subactions[actlen - 1] == 8){
						String str = elems[i].toString();
						if(str.trim().length() != 0){
							// 공백 없이 바로 변수선언을 들어갔다는 것.
							// 루프를 한단계 뒤로 돌린다.
							i--;
							curidx -= str.length();
							subactions[actlen - 1] = 0;
							continue;
						}
						// 공백을 읽어들였으므로 다음으론 변수선언이 올것이다.
						subactions[actlen - 1] = 1;
					}
					// end action 0

					
					
					
					
					
					
					
					
					
				
				}else if(actions[actlen - 1] == 1){
					// FIXME 명령문.
					// 명령문 section의 실행은 label을 읽다가 건너온 것임.
					if(subactions[actlen - 1] == 0){
						params[paramlen] = elems[i].toString();
						paramlen++;
						subactions[actlen - 1] = 1;
					}else if(subactions[actlen - 1] == 1){
						//logln("명령어 subaction 1.");
						// params[paramlen - 1]에는 명령어 이름이 들어가 있다.
						// 명령어 다음 공백을 기다림.
						if(!(elems[i] instanceof Token)){
							if(elems[i] instanceof Comment){
								// 코멘트가 나왔다.
								// 지금 나온 것으로 명령어를 생성한다.
								//logln("끝! in subaction 0.");
								generatecmd(focus, bundle, startidx, curidx,
										params, paramlen/*,
										subactions[actlen - 1]*/);
								// 한단계 루프를 뒤로 돌린다.
								curidx -= elemlen;
								i--;
								subactions[actlen - 1] = actions[actlen - 1] = -1;
								continue;
							}else if(elems[i] instanceof Valuable){
								// 루프를 뒤로 돌린 뒤, subaction = 1일 때(공백 다음 값을 읽음)로
								// 둠.
								curidx -= elemlen;
								i--;
								subactions[actlen - 1] = 1;
								continue;
							}
						}
						if(elems[i].toString().trim().length() != 0){
							// 공백이 올 자리에 공백이 오지 않는다.
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(curidx
											- elemlen,
											elemlen),
									ERRCNT_ONLYSPACE));
							i--;
							curidx -= elemlen;
						}
						/*
						 * 왜 있는진 모르지만, 혹시나 하므로 일단 추가. startps[0] = paramlen;
						 * spslen++;
						 */
						subactions[actlen - 1] = 2;
					}else if(subactions[actlen - 1] == 2){
						if(elems[i] instanceof Token && ((Token)elems[i]).toString().equals("=") && (params[paramlen - 1] instanceof String)){
							// 대입문이다.
							curidx -= elemlen;
							i--;
							actions[actlen - 1] = 4;
							subactions[actlen - 1] = 2;
							continue;
						}
						// 공백 다음 값을 읽음.
						// 값을 읽기 위해 action을 변경한다.
						// subaction에 현재 위치 저장.
						actlen++;
						subactions[actlen - 1] = 1;
						actions[actlen - 1] = 2;
						curidx -= elemlen;
						vsilen = 1;
						vstartidcs[0] = curidx;
						spslen++;
						startps[0] = paramlen;
						// startps[0] = 1;

						i--;
						// vsilen은 1이 되어 있어야 한다.
						// params[0]은 명령어가 들어 있어야 한다.
					}else if(subactions[actlen - 1] == 3){
						// 값 다음 공백
						if(!(elems[i] instanceof Token)){
							// string이 아니다.
							if(elems[i] instanceof Valuable){
								// 값이라면 잘못된 연산자이거나, 쉼표가 빠졌거나 등등.
								// '쉼표가 빠졌다'로 선언한다.
								bundle.addErrorMessage(new GrammarErrorMessage(
										new CodeRangeImpl(curidx
												- elemlen,
												elemlen),
										ERRCNT_NOTCOMMA));
								// 루프를 한단계 뒤로 돌린다.
								// 그 값을 읽기 위해 바로 subaction = 1로 점프한다.
								curidx -= elemlen;
								i--;
								subactions[actlen - 1] = 1;
								continue;
							}else{
								// 코멘트, 전처리명령어.
								// 허용된다. 일단 있는 것으로 생성 후 루프를 한단계 뒤로 돌림.
								//logln("generating a command..");
								generatecmd(focus, bundle, startidx, curidx,
										params, paramlen/*,
										subactions[actlen - 1]*/);
								// 한단계 루프를 뒤로 돌린다.
								curidx -= elemlen;
								i--;
								// 코멘트로 전환.
								subactions[actlen - 1] = actions[actlen - 1] = -1;
								continue;
							}
						}
						// 공백인지 확인.
						String str = elems[i].toString();
						if(str.trim().length() != 0){
							// 공백 없이 바로 쉼표든 뭐든 어쨌든 나온 경우.
							// (에러 처리는 subaction3에서 하면 된다)
							// 루프를 한단계 뒤로 돌리고, subaction = 3으로 감.
							curidx -= str.length();
							i--;
						}
						subactions[actlen - 1] = 4;
						continue;
					}else if(subactions[actlen - 1] == 4){
						// 공백 다음 쉼표.

						// 아래 에러 처리 소스는 subaction = 2일때와 동일하다.
						if(!elems[i].equals(elems[i].toString())){
							// string이 아니다.
							if(elems[i] instanceof Valuable){
								// 값이라면 잘못된 연산자이거나, 쉼표가 빠졌거나 등등.
								// '쉼표가 빠졌다'로 선언한다.
								bundle.addErrorMessage(new GrammarErrorMessage(
										new CodeRangeImpl(curidx
												- elemlen,
												elemlen),
										ERRCNT_NOTCOMMA));
								// 루프를 한단계 뒤로 돌린다.
								// 바로 subaction = 1로 점프한다.
								curidx -= elemlen;
								i--;
								subactions[actlen - 1] = 1;
								continue;
							}else{
								// 코멘트, 전처리명령어.
								// 허용된다. 일단 있는 것으로 생성 후 루프를 한단계 뒤로 돌림.
								generatecmd(focus, bundle, startidx, curidx,
										params, paramlen/*,
										subactions[actlen - 1]*/);
								// 한단계 루프를 뒤로 돌린다.
								curidx -= elemlen;
								i--;
								// 코멘트로 전환.
								subactions[actlen - 1] = actions[actlen - 1] = -1;
								continue;
							}
						}
						String str = elems[i].toString();
						if(!str.equals(",")){
							// 이건 뭐냐? '잘못된 토큰이다'로 선언.
							//logln("명령에서 값 다음 잘못된 토큰 발견");
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(curidx
											- elemlen,
											elemlen),
									ERRCNT_WRONGTOKEN));
						}
						subactions[actlen - 1] = 5;
					}else if(subactions[actlen - 1] == 5){
						// 쉼표 다음 공백
						if(elems[i].equals(elems[i].toString())){
							// 문자열.
							// 공백인지 확인한다.
							String str = elems[i].toString();
							if(str.trim().length() == 0){
								// 성립.
								subactions[actlen - 1] = 2;
								continue;
							}
						}
						// 쉼표 뒤에는 공백이 없었던 것.
						// subactions[actlen - 1] = 2으로 돌린다.
						subactions[actlen - 1] = 2;
						curidx -= elemlen;
						i--;
						continue;
					}
					// end action == 1

					
					
					
					
					
					
					
					
					
					
					
					
				}else if(actions[actlen - 1] == 2){
					// FIXME 값읽기 section
					if(subactions[actlen - 1] == 1){
						// 값을 읽음.
						// 처음 값일 수도 있고, 연산자 다음 값일 수도 있다.
						Valuable val = null;
						//System.out.println("elem[i] class : " + elems[i].getClass());
						if(elems[i] instanceof Valuable){
							// 값이다.
							val = (Valuable) elems[i];
						}else if(elems[i] instanceof Token){
							// string이다.
							String str = elems[i].toString();
							if(str.equals("(")){
								// 앞에 연산자가 있든 없든 모두 괜찮음.
								// 1 + 2 (3 + 4) 이런 경우는 여기에 도달할 수 없다.
								curidx -= str.length();
								i--;
								subactions[actlen - 1] = 9;
								continue;
							}else if(str.equals(")") || checkUnknownValueOrOperator(str, actions[actlen - 2], subactions[actlen - 2])){
								// 다시 되돌아갑니다.
								//logln("finished.");

								// 만약 여태까지 아무것도 나오지 않았다면 에러가 아닐 수 있으나,
								// 이미 한번 연산자가 나왔다면 값이 없게 되므로 문제가 있다.
								if(params[paramlen - 1] != null){
									// 연산자 다음에 값이 안 나왔으므로, 에러를 선언.
									bundle.addErrorMessage(new GrammarErrorMessage(
													new CodeRangeImpl(vstartidcs[vsilen - 1],
															curidx - vstartidcs[vsilen - 1]),
													ERRCNT_NOTVALUE));
								}else if(str.equals(")")){
									// 값을 읽어야 하는데 )가 오는 경우엔 무조건 에러.
									
									bundle.addErrorMessage(new GrammarErrorMessage(
											new CodeRangeImpl(curidx - str.length(),
													1),
											ERRCNT_WRONGTOKEN));
								}else if(actions[actlen - 2] == 8){
									int beforelen = getlen(elems[i - 1]);
									// if문에서 then 차례. 지금 elems[i]는 then을 가리키고
									// 있다.
									// then 이전에 오는 공백이 있는지 확인해보자.
									if(elems[i - 1] instanceof Token){
										String checkblank = elems[i - 1]
												.toString();
										if(checkblank.toString().trim().length() != 0){
											// 공백이 아니다! 에러!
											bundle.addErrorMessage(new GrammarErrorMessage(
															new CodeRangeImpl(curidx - str.length() - beforelen,
																	beforelen),
															ERRCNT_IF_THEN_NOTBLANK));
										}
									}else
										// 역시나 공백이 아니다. 에러!
										bundle.addErrorMessage(new GrammarErrorMessage(
														new CodeRangeImpl(curidx - str.length() - beforelen,
																beforelen),
														ERRCNT_IF_THEN_NOTBLANK));
								}
								curidx -= str.length();
								i--;
								// 끝
								subactions[actlen - 1] = 99;
								continue;
							}
							CodeRange crange = new CodeRangeImpl(curidx - str.length(), str.length());
							if(str.toLowerCase().equals("true"))
								val = new Value(crange, ValueType.TYPE_BOOLEAN, Boolean.TRUE);
							else if(str.toLowerCase().equals("false"))
								val = new Value(crange, ValueType.TYPE_BOOLEAN, Boolean.FALSE);
							if(val == null){
								try{
									java.math.BigInteger rawval = new java.math.BigInteger(str);
									int value = rawval.intValue();
									if(rawval.bitLength() > 32)
										bundle.addErrorMessage(new GrammarErrorMessage(
														new CodeRangeImpl(
																curidx- str.length(), str.length()),
														ERRCNT_TOOBIGVALUE));
									val = new Value(crange, ValueType.TYPE_INT, value);
								}catch(NumberFormatException nfexcp){
									// 숫자가 아니다.
									// variable일 것이다.
									val = new VariableCallValue(str, crange);
								}catch (Exception excp){
									// 어떻게 된거지?!
									excp.printStackTrace();
								}
							}
						}else{
							//logln("comment");
							// valuable도 아니고 string도 아니다 -> comment
							// 이것은 값 읽기 작업을 맡긴 프로세스에게 맡긴다.
							if(paramlen == startps[spslen - 1])
								// 아무것도 안했다...
								// 값읽기 section을 호출한 곳에서 처리. 
								;
							else if((paramlen != startps[spslen - 1])
									&& (params[paramlen - 1] != null))
								// 이미 한번 연산자가 나왔다면 값이 없게 되므로 문제가 있다.
								// 연산자 다음에 값이 안 나왔으므로, 에러를 선언.
								bundle.addErrorMessage(new GrammarErrorMessage(
												new CodeRangeImpl(vstartidcs[vsilen - 1],
														curidx - vstartidcs[vsilen - 1]),
												ERRCNT_NOTVALUE));
							// 여태까지 읽어들인 값을 저장한다.

							curidx -= getlen(elems[i]);
							i--;
							// 끝
							subactions[actlen - 1] = 99;
							continue;
						}
						//logln("val : " + val + ", " + val.getCodeRange());
						// 이렇게 해서 값 val을 읽었다.
						// 만약 현재 expression을 편집중이다면 반드시 오른쪽 node가 빌 것이다.
						if(val == null){
							// 분명 존재하지 않는 변수라는 에러를 만났을 것이다.
							// 그냥 아무 값이나 대입
							val = new VariableCallValue(elems[i].toString(), null);
						}
						params[paramlen++] = val;
						subactions[actlen - 1]++;
					}else if(subactions[actlen - 1] == 2){
						// 값 다음 공백.
						if(!(elems[i] instanceof Token)){
							if(elems[i] instanceof Valuable){
								// 값 다음에 값이 나올 수 없다. 아마도 연산자가 빠져서 그럴 것이다.
								// 에러를 내지 않는다.
							}else{
								// comment.
								// 구지 에러를 내지 않아도 된다.
							}
							// 값 읽기를 종료한다.
							curidx -= elemlen;
							i--;
							// 끝
							subactions[actlen - 1] = 99;
							continue;
						}
						String str = elems[i].toString();
						if(str.trim().length() != 0){
							// 바로 오퍼레이터가 나오는 가보다.
							// 루프를 한 단계 뒤로 돌린다.
							curidx -= str.length();
							i--;
						}
						subactions[actlen - 1]++;
					}else if(subactions[actlen - 1] == 3){
						// 공백 다음 연산자가 올 차례이다.
						if(!(elems[i] instanceof Token)){
							// 뭔진 모르겠지만, 원래 하던 것으로 귀환한다.
							curidx -= elemlen;
							i--;
							// 끝
							subactions[actlen - 1] = 99;

							//logln("return! because of the unknown element");
							//logln("(final) paramlen : " + paramlen + " , params[paramlen - 1] : "
							//		+ params[paramlen - 1]);
							continue;
						}
						
						String str = elems[i].toString();
						if(str.equals("(") && params[paramlen - 1] instanceof VariableCallValue){
							// 함수.
							subactions[actlen - 1] = 7;
							i--;
							curidx -= elemlen;
							continue;
						}else if(!Operator.isOperator(str) || checkUnknownValueOrOperator(str, actions[actlen - 2],
								subactions[actlen - 2])){
							// 뭔진 모르지만 끝낸다.
							// 원래 하던 것으로 귀환.
							//logln("return! because of the unknown operator : "
							//				+ str);
							//logln("paramlen : " + paramlen);
							curidx -= elemlen;
							i--;
							// 끝
							subactions[actlen - 1] = 99;
							continue;
						}
						
						Operator op = Operator.getOperator(str);
						params[paramlen++] = op;
						//logln("vfocus : " + params[paramlen - 1]
						//		+ " , vroot : " + params[paramlen - 2]);
						subactions[actlen - 1]++;
						continue;
						// 공백 다음 operator.
					}else if(subactions[actlen - 1] == 4){
						// operator 다음 공백.
						if(!(elems[i] instanceof Token)){
							// 이미 첫번째 단계에서 처리가 완료됨.
							// 루프를 한단계 뒤로 돌려서 subaction이 첫번째 단계로 가도록.
							subactions[actlen - 1] = 1;
							i--;
							curidx -= elemlen;
							continue;
						}
						String str = elems[i].toString();
						if(str.trim().length() != 0){
							// 바로 오퍼레이터가 나오는 가보다.
							// 루프를 한 단계 뒤로 돌린다.
							curidx -= str.length();
							i--;
						}
						// 처음으로 
						subactions[actlen - 1] = 1;
						
						
						
					}else if(subactions[actlen - 1] == 7){
						//logln("START reading a function!");
						// operator읽기에서 왔음. 함수 시작. 현재 위치 : 함수 이름 다음 (에 있다.
						// 함수 호출.
						// 만약 아래 문장이 에러가 나면 틀림없이 action의 이동간에 문제가 생긴 것.
						i--;
						curidx -= elemlen;
						// params[paramlen - 1]엔 VariableCallValue가 있음.
						VariableCallValue vcv = (VariableCallValue)params[paramlen - 1];
						params[paramlen - 1] = vcv.getTarget();
						
						// 시작 인덱스 포인터에 추가.
						vstartidcs[vsilen] = vcv.getCodeRange().getStartPosition();
						vsilen++;
						startps[spslen] = paramlen - 1;
						spslen++;

						actlen++;
						actions[actlen - 1] = 5;
						subactions[actlen - 1] = 3;
						// 이동!
						continue;
					}else if(subactions[actlen - 1] == 8){
						// 함수 읽기를 종료한다.
						// params[len - 1]에 이미 load되어 있으므로 ok.
						// 값 다음 공백으로 가자.
						subactions[actlen - 1] = 2;
						
						
						
					}else if(subactions[actlen - 1] == 9){
						//logln("START reading (~~)!");
						// elems[i] = '('
						// 한 단계 물릴 필요 없다.
						
						// 시작 인덱스 포인터에 추가.
						vstartidcs[vsilen] = curidx;
						vsilen++;
						startps[spslen] = paramlen;
						spslen++;

						actlen++;
						actions[actlen - 1] = 2;
						subactions[actlen - 1] = 1;
						// 이동!
						continue;
					}else if(subactions[actlen - 1] == 10){
						if(!elems[i].toString().equals(")")){
							// 에러!!
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(curidx - elemlen,
											elemlen),
									ERRCNT_WRONGTOKEN));
						}
						// elems[i] = ')'인 상황이다.
						// 한 단계 물릴 필요 없음. 바로 다음 토큰으로 넘어간다.
						// 이미 params[paramlen - 1]에 다 있다.
						// 값 다음 공백으로 가자.
						subactions[actlen - 1] = 2;
					}
					
					
					else if(subactions[actlen - 1] == 99){
						// 값 읽기를 종료한다.
						actlen--;
						subactions[actlen - 1]++;
						curidx -= elemlen;

						value_knot(params, startps[spslen - 1], paramlen, vstartidcs,
								vsilen, curidx);
						vsilen--;
						if(paramlen != startps[spslen - 1]){
							while(startps[spslen - 1] + 1 != paramlen){
								params[paramlen - 1] = null;
								paramlen--;
							}
						}
						spslen--;
						// 이제 되돌아간다.
						i--;
						continue;
					}
					// end action == 2

					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
				}else if(actions[actlen - 1] == 3){
					// FIXME 상수 선언 문장
					if(!elems[i].equals(elems[i].toString())
							&& subactions[actlen - 1] != 6){
						if(!(elems[i] instanceof Valuable)){
							// comment 또는 processor statement.
							// 에러를 내고 i--.
							//logln("에러.");
							bundle
									.addErrorMessage(new GrammarErrorMessage(
											new CodeRangeImpl(curidx - elemlen, elemlen),
											ERRCNT_CONST_NOTEND));
							// 더 이상 주석 등을 제외한 아무것도 올 수 없다.
							curidx -= elemlen;
							i--;
							actions[actlen - 1] = -2;
							subactions[actlen - 1] = -1;
							paramlen = 0;
							continue;
						}
					}

					if(subactions[actlen - 1] == 0){
						// const 다음 공백
						if(!elems[i].equals(elems[i].toString())){
							// string 값이 아니다!
							if(elems[i] instanceof Valuable){
								// valuable 이 지금 올 수 없다.
								// 잘못된 토큰입니다를 선언.
								bundle.addErrorMessage(new GrammarErrorMessage(
										new CodeRangeImpl(
												curidx - elemlen, elemlen),
										ERRCNT_WRONGTOKEN));
								// 더 이상 나갈 진도가 없다.
								actions[actlen - 1] = -2;
								subactions[actlen - 1] = -1;
								paramlen = 0;
								continue;
							}
						}
						String str = elems[i].toString();
						if(str.trim().length() != 0){
							// 공백이 오지 않았다.
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(curidx - elemlen, elemlen), ERRCNT_ONLYSPACE));
						}

						subactions[actlen - 1] = 1;
					}else if(subactions[actlen - 1] == 1){
						// 공백 다음 상수명
						if(!elems[i].equals(elems[i].toString())){
							// 문자열 형태가 아니다.
							// 아래 소스는 subaction = 0의 것과 비슷합니다.
							if(elems[i] instanceof Valuable){
								// valuable 이 지금 올 수 없다.
								// 잘못된 토큰입니다를 선언.
								bundle.addErrorMessage(new GrammarErrorMessage(
										new CodeRangeImpl(
												curidx - elemlen, elemlen),
										ERRCNT_WRONGTOKEN));
							}
							// 더 이상 나갈 진도가 없다.
							actions[actlen - 1] = -2;
							subactions[actlen - 1] = -1;
							paramlen = 0;
							continue;
						}

						String str = elems[i].toString();
						if(str.equals("=")){
							// 왜 벌써 =이 여기 온 걸까?
							// subaction = 3으로 점프.
							// grammar error 발사.
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(curidx - elemlen, elemlen), ERRCNT_EMPTYNAME));
							subactions[actlen - 1] = 3;
							curidx -= str.length();
							i--;
							continue;
						}

						if(params[0] == null)
							params[0] = str;
						else
							params[0] = params[0].toString() + str;
						subactions[actlen - 1] = 2;
					}else if(subactions[actlen - 1] == 2){
						// 변수명 다음 공백
						if(!elems[i].equals(elems[i].toString())){
							// 문자열 형태가 아니다.
							// 아래 소스는 subaction = 1의 것과 비슷합니다.
							if(elems[i] instanceof Valuable){
								// =이 없다 라고 판단할 수 있다.
								bundle.addErrorMessage(new GrammarErrorMessage(
										new CodeRangeImpl(
												curidx - elemlen, elemlen),
										ERRCNT_NOTEQUALOP));
								// subaction 5로 바로 뛰어간.
								subactions[actlen - 1] = 5;
								curidx -= elemlen;
								i--;
								continue;
							}
						}
						String str = elems[i].toString();
						if(str.trim().length() != 0){
							// 공백이 오지 않았다. 상수명의 일부로 처리한다.
							// subaction = 1로 가서 const 명을 계속 받아온다.
							subactions[actlen - 1] = 1;
							curidx -= str.length();
							i--;
							continue;
						}

						subactions[actlen - 1] = 3;
					}else if(subactions[actlen - 1] == 3){
						// 공백 다음 =
						boolean flag = !elems[i].equals(elems[i].toString());
						if(!flag)
							// =이 아니라면 바로 값이 나온 것이라고 해석이 가능하다.
							flag = !elems[i].toString().equals("=");
						if(flag){
							//logln("=가 없음! at sa 3");
							// 아래 소스는 subaction = 2의 것과 비슷합니다.
							// =이 없다 라고 판단할 수 있다.
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(curidx - elemlen,
											elemlen), ERRCNT_NOTEQUALOP));
							// subaction 5로 바로 뛰어간.
							subactions[actlen - 1] = 5;
							curidx -= elemlen;
							i--;
							continue;
						}

						subactions[actlen - 1] = 4;
					}else if(subactions[actlen - 1] == 4){
						// = 다음 공백
						boolean flag = !elems[i].equals(elems[i].toString());
						if(!flag)
							// 공백이 아니라면 바로 값이 나온 것이라고 해석 가능.
							flag = elems[i].toString().trim().length() != 0;
						if(flag){
							// 아래 소스는 subaction = 3의 것과 비슷합니다.
							// 공백이 없이 값이 바로 나왔다.
							// subaction 5로 뛴다.
							subactions[actlen - 1] = 5;
							curidx -= elemlen;
							i--;
							continue;
						}

						subactions[actlen - 1] = 5;
					}else if(subactions[actlen - 1] == 5){
						// 공백 다음 값.
						// 값 읽기 프로시져 시작.
						curidx -= elemlen;
						vstartidcs[0] = curidx;
						vsilen++;
						i--;
						// startps[0]에 1을 대입한다.
						startps[0] = paramlen;
						// spslen은 1이 된다.
						spslen++;
						subactions[actlen] = 1;
						actions[actlen] = 2;
						actlen++;
					}else if(subactions[actlen - 1] == 6){
						// 값 다음 끝났을 때 호출되는 부분.
						generateconst(subactions, actlen, focus, bundle,
								startidx, curidx, params, paramlen);

						// 이제 아무것도 할 수 없다. 주석이나 proc문 등만 가능.
						curidx -= elemlen;
						i--;
						actions[actlen - 1] = -2;
						subactions[actlen - 1] = -1;
					}
					// end action 3

					
					
					
					
					
					
					
					
				
				}else if(actions[actlen - 1] == 4){
					// FIXME 대입식 만들기.
					// 대입식 만들기는 label 또는 명령어 읽기에서 건너온 것.
					if(!(elems[i] instanceof Token)
							&& subactions[actlen - 1] != 5){
						if(!(elems[i] instanceof Valuable)){
							// comment 또는 processor statement.
							// 에러를 내고 i--.
							//logln("에러. in insert");
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(startidx,
											curidx - startidx),
									ERRCNT_INSSTMT_NOTEND));
							// 더 이상 주석 등을 제외한 아무것도 올 수 없다.
							curidx -= elemlen;
							i--;
							actions[actlen - 1] = -2;
							subactions[actlen - 1] = -1;

							params[paramlen] = null;
							params[paramlen - 1] = null;
							paramlen = 0;
							continue;
						}
					}
					if(subactions[actlen - 1] == 0){
						//logln("변수명 읽기. vsilen : " + vsilen);
						if(!elems[i].equals(elems[i].toString())
								&& elems[i] instanceof Valuable){
							// 변수명 차례에 valuable이 와버렸다.
							// 딱히 어떤 형태라 인식할 수 없는 형태. 에러로 둔다.
							//logln("에러. in subaction 0");
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(startidx, curidx - startidx),
									ERRCNT_INSSTMT_NOTEND));
							// 더 이상 주석, pre-processor 문장을 제외한 아무것도 올 수 없다.
							actions[actlen - 1] = -2;
							subactions[actlen - 1] = -1;
							params[paramlen] = null;
							params[paramlen - 1] = null;
							paramlen = 0;
							continue;
						}
						// 변수명 읽기.
						params[paramlen] = elems[i].toString();
						paramlen++;
						subactions[actlen - 1] = 1;
					}else if(subactions[actlen - 1] == 1){
						// 변수명 다음 공백.
						if(!elems[i].equals(elems[i].toString())){
							// 문자열 형태가 아니다.
							if(elems[i] instanceof Valuable){
								// =이 없다 라고 판단할 수 있다.
								bundle.addErrorMessage(new GrammarErrorMessage(
										new CodeRangeImpl(
												startidx, curidx - startidx),
										ERRCNT_NOTEQUALOP));
								// subaction 4로 바로 jump.
								subactions[actlen - 1] = 4;
								curidx -= elemlen;
								i--;
								continue;
							}else{
								throw new Exception(
										"에러입니다! 컴파일러 구조상 절대 실행될 수 없는 부분입니다.");
							}
						}
						String str = elems[i].toString();
						if(str.trim().length() != 0){
							// 공백이 오지 않았다. 변수명의 일부로 처리한다.
							// subaction = 1로 가서 const 명을 계속 받아온다.
							subactions[actlen - 1] = 0;
							curidx -= elemlen;
							i--;
							continue;
						}

						subactions[actlen - 1] = 2;
					}else if(subactions[actlen - 1] == 2){
						// 공백 다음 =.
						boolean flag = !(elems[i] instanceof Token);
						if(!flag)
							// =이 아니라면 바로 값이 나온 것이라고 해석이 가능하다.
							flag = !elems[i].toString().equals("=");
						if(flag){
							// 아래 소스는 subaction 1의 것과 비슷.
							// =이 없다 라고 판단할 수 있다.
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(startidx, curidx - startidx), ERRCNT_NOTEQUALOP));
							subactions[actlen - 1] = 4;
							curidx -= elemlen;
							i--;
							continue;
						}

						subactions[actlen - 1] = 3;
					}else if(subactions[actlen - 1] == 3){
						// = 다음 공백.
						boolean flag = !(elems[i] instanceof Token);
						if(!flag)
							// 공백이 아니라면 바로 값이 나온 것이라고 해석 가능.
							flag = elems[i].toString().trim().length() != 0;
						if(flag){
							// 공백이 없이 값이 바로 나왔다.
							subactions[actlen - 1] = 4;
							curidx -= elemlen;
							i--;
							continue;
						}

						subactions[actlen - 1] = 4;
					}else if(subactions[actlen - 1] == 4){
						//logln("대입식에서 값을 읽어 들이기를 명령합니다... paramlen : "
						//				+ paramlen);
						// 공백 다음 값.
						// 드디어 값을 읽어들인다.
						/*
						if(elems[i] instanceof Valuable){
							// 원래 subaction = 6 단계를 건너야 하는데,(함수 체킹을 위해) 바로 값이
							// 나왔으니
							// 이는 반드시 수식임을 말한다.(함수체킹 필요 X)
							// 수식 읽기 모드로 들어간다.
							curidx -= elemlen;
							i--;
							actions[actlen - 1] = 2;
							subactions[actlen - 1] = 1;
							continue;
						}
						 	*/
						// 루프를 되돌림,
						curidx -= elemlen;
						i--;

						vstartidcs[0] = curidx;
						vsilen = 1;
						startps[spslen] = paramlen;
						spslen++;
						// '값 읽기'로 가자.
						actlen++;
						subactions[actlen - 1] = 1;
						actions[actlen - 1] = 2;
					}else if(subactions[actlen - 1] == 5){
						//logln("값 읽기 끝.");
						// 값 다음 ending.
						generateinsstmt(focus, bundle, startidx, curidx,
								params, paramlen);
						// 이제 아무것도 할 수 없다. 주석이나 proc문 등만 가능.
						params[paramlen] = null;
						params[paramlen - 1] = null;
						curidx -= elemlen;
						i--;
						actions[actlen - 1] = -2;
						subactions[actlen - 1] = -1;
						paramlen = 0;
					}
				// end action == 4

					
					
					
					
					
					
					
					
					
				}else if(actions[actlen - 1] == 5){
					// FIXME 함수 읽어들이기.
					if(subactions[actlen - 1] == 1){
						// 함수명 읽기.
						if(!elems[i].equals(elems[i].toString())){
							// 어떤 일이든 지금 다른 것이 나오는 것은 용서할 수 없다.
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(vstartidcs[vsilen - 1],
											curidx - elemlen
													- vstartidcs[vsilen - 1]),
									ERRCNT_FUNC_NOTEND));
							curidx -= elemlen;
							i--;
							subactions[actlen - 1] = 9;
							continue;
						}
						String str = elems[i].toString();
						params[paramlen - 1] = str;
						subactions[actlen - 1]++;
					}else if(subactions[actlen - 1] == 2){
						// 함수명 다음 공백.
						if(!(elems[i] instanceof Token)){
							// 어떤 일이든 지금 다른 것이 나오는 것은 용서할 수 없다.
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(vstartidcs[vsilen - 1],
											curidx - elemlen
													- vstartidcs[vsilen - 1]),
									ERRCNT_FUNC_NOTEND));
							curidx -= elemlen;
							i--;
							subactions[actlen - 1] = 9;
							continue;
						}
						String str = elems[i].toString();
						if(str.equals("(")){
							// 공백이 없이 바로 괄호를 열었다.
							// 한 단계 루프를 뒤로 보낸 뒤, 서브액션 3 호출.
							subactions[actlen - 1]++;
							curidx -= str.length();
							i--;
							continue;
						}else if(str.trim().length() != 0){
							// 그 외에 어떤 두려운 물체.
							// 그냥 이 함수 프로시저를 호출한 곳으로 되돌아가자.
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(vstartidcs[vsilen - 1],
											curidx - str.length()
													- vstartidcs[vsilen - 1]),
									ERRCNT_FUNC_NOTEND));
							curidx -= elemlen;
							i--;
							subactions[actlen - 1] = 9;
							continue;
						}
						subactions[actlen - 1]++;
					}else if(subactions[actlen - 1] == 3){
						// 공백 다음 opening parenthesis.
						if(!(elems[i] instanceof Token)){
							bundle.addErrorMessage(new GrammarErrorMessage(
											new CodeRangeImpl(
													vstartidcs[vsilen - 1],
													curidx
															- elemlen
															- vstartidcs[vsilen - 1]),
											ERRCNT_FUNC_NOTEND));
							curidx -= elemlen;
							i--;
							subactions[actlen - 1] = 9;
							continue;
						}

						String str = elems[i].toString();
						//logln("str : [" + str + "]");
						if(!str.equals("(")){
							// 원래 이 프로시저를 호출했던 곳으로 되돌아가자.
							bundle
									.addErrorMessage(new GrammarErrorMessage(
											new CodeRangeImpl(
													vstartidcs[vsilen - 1],
													curidx
															- str.length()
															- vstartidcs[vsilen - 1]),
											ERRCNT_FUNC_NOTOPPRTHSIS));
							curidx -= elemlen;
							i--;
							subactions[actlen - 1] = 9;
							continue;
						}
						subactions[actlen - 1]++;
					}else if(subactions[actlen - 1] == 4){
						// 열리는 괄호 다음 공백.
						boolean flag = !elems[i].equals(elems[i].toString());
						if(!flag)
							flag = elems[i].toString().trim().length() != 0;
						if(flag){
							// 한 루프 돌린 후, subaction 5에서 처리하도록.
							curidx -= elemlen;
							i--;
						}
						subactions[actlen - 1]++;
					}else if(subactions[actlen - 1] == 5){
						// (함수에서) 괄호 다음 공백 다음 값.
						// 드디어 값을 받아들인다.
						// 만약에 closing parenthesis가 나왔다면 subaction 6으로 뜀
						if(elems[i] instanceof Token && elems[i].equals(")")){
							curidx -= elemlen;
							i--;
							subactions[actlen - 1] = 6;
							continue;
						}
						// 값을 읽는 액션 2 호출.
						actlen++;
						subactions[actlen - 1] = 1;
						actions[actlen - 1] = 2;
						vsilen++;
						vstartidcs[vsilen - 1] = curidx;
						spslen++;
						startps[spslen - 1] = paramlen;

						curidx -= elemlen;
						i--;
						// params[0]은 명령어가 들어 있어야 한다.
					}else if(subactions[actlen - 1] == 6){
						// 값 다음 공백.
						if(!elems[i].equals(elems[i].toString())){
							if(elems[i] instanceof Valuable){
								// 쉼표가 빠졌다고 가정한다.
								// 루프를 되감은 후, subaction 8로 점프!
								bundle
										.addErrorMessage(new GrammarErrorMessage(
												new CodeRangeImpl(
														vstartidcs[vsilen - 1],
														curidx
																- elemlen
																- vstartidcs[vsilen - 1]),
												ERRCNT_NOTCOMMA));
								subactions[actlen - 1] = 8;
								curidx -= elemlen;
								i--;
								continue;
							}
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(vstartidcs[vsilen - 1],
											curidx - elemlen
													- vstartidcs[vsilen - 1]),
									ERRCNT_FUNC_NOTEND));
							curidx -= elemlen;
							i--;
							subactions[actlen - 1] = 9;
							continue;
						}
						String str = elems[i].toString();
						if(str.equals(")")){
							// 닫는 parenthesis가 등장했다.
							// subaction을 9로 맞춰두고, 루프를 한단계 뒤로.
							subactions[actlen - 1] = 9;
							curidx -= elemlen;
							i--;
							continue;
						}else if(str.trim().length() != 0){
							// subaction 7로 간다. 루프를 한 단계 감음.
							curidx -= elemlen;
							i--;
						}
						subactions[actlen - 1]++;
					}else if(subactions[actlen - 1] == 7){
						// 공백 다음 쉼표.
						boolean flag1 = !elems[i].equals(elems[i].toString());
						boolean flag2 = (flag1 && (elems[i] instanceof Valuable));
						boolean flag3 = !flag2
								&& !elems[i].toString().equals(",");
						if(flag2 || flag3){
							if(flag3 && elems[i].equals(")")){
								// 닫힘 기호 선언.
								// subaction을 9로 맞춰두고, 루프를 한단계 뒤로.
								subactions[actlen - 1] = 9;
								curidx -= elemlen;
								i--;
								continue;
							}
							// 쉼표가 빠졌다고 가정한다.
							// 루프를 되감은 후, subaction 8로 점프.
							curidx -= elemlen;
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(vstartidcs[vsilen - 1],
											curidx - vstartidcs[vsilen - 1]),
									ERRCNT_NOTCOMMA));
							subactions[actlen - 1] = 8;
							i--;
							continue;
						}else if(flag1){
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(vstartidcs[vsilen - 1],
											curidx - elemlen
													- vstartidcs[vsilen - 1]),
									ERRCNT_FUNC_NOTEND));
							curidx -= elemlen;
							i--;
							subactions[actlen - 1] = 9;
							continue;
						}
						subactions[actlen - 1]++;
						continue;
					}else if(subactions[actlen - 1] == 8){
						// 쉼표 다음 공백.
						boolean flag = !elems[i].equals(elems[i].toString());
						if(!flag)
							flag = elems[i].toString().trim().length() != 0;

						if(flag){
							// 바로 값으로 간다.
							curidx -= elemlen;
							i--;
						}
						subactions[actlen - 1] = 5;
					}else if(subactions[actlen - 1] == 9){
						// 괄호 닫기.
						// 여기로 올 떄는 항상 의도한 함수 호출 구문의 종결이다. 따라서, 검사할 필요가 X.
						params[startps[spslen - 1]] = generatefunc(
								subactions[actlen - 1], bundle, curidx, params,
								paramlen, vstartidcs, vsilen, startps, spslen);
						// 메모리 비우기
						for(int j = startps[spslen - 1] + 1; j < paramlen; j++)
							params[j] = null;
						// 이 프로시저를 호출한 곳으로 되돌아간다.
						subactions[actlen - 2]++;
						actlen--;
						// paramlen 고치기.
						paramlen = startps[spslen - 1] + 1;
						spslen--;
						// vsilen 삭제.
						vsilen--;

						curidx -= elemlen;
						i--;
					}
				}
				// end action 5

				
				
				
				
				
				
				
				else if(actions[actlen - 1] == 6){
					//logln("숫자 label.");
					if(subactions[actlen - 1] == 0){
						// label 다음 공백.
						if(!elems[i].equals(elems[i].toString())){
							// 다른 무언가 오고 있다.
							curidx -= elemlen;
							i--;
						}
						subactions[actlen - 1] = actions[actlen - 1] = -1;
						generatelabel(focus, bundle, startidx,
								curidx - elemlen, params, paramlen);

						paramlen = 0;
						params[0] = null;
						continue;
					}
				}
				// end action 6

				
				
				
				
				
				
				
				
				else if(actions[actlen - 1] == 7){
					// label읽기
					// FIXME label부분(int아님)
					if(subactions[actlen - 1] == 0){
						// 레이블 읽기
						if(!(elems[i] instanceof Token)){
							// 레이블을 읽다가 갑자기 다른 게 튀어나왔다.
							// 명령문인것 같다.
							curidx -= elemlen;
							i--;
							subactions[actlen - 1] = 3;
							continue;
						}

						String str = elems[i].toString();
						if(str.equals(":")){
							// originally label statement, but
							// 명령문으로 처리하자.
							curidx -= elemlen;
							i--;
							subactions[actlen - 1] = 3;
							continue;
						}else if(str.equals("=")){
							// 알고보니 대입문이었다-_-
							curidx -= elemlen;
							i--;
							subactions[actlen - 1] = 2;
							continue;
						}else if(str.trim().length() == 0){
							// 명령문인것 같다.
							curidx -= elemlen;
							i--;
							subactions[actlen - 1] = 3;
							continue;
						}

						if(params[paramlen - 1] == null)
							params[paramlen - 1] = str;
						else
							params[paramlen - 1] = params[paramlen - 1].toString() + str;
					}else if(subactions[actlen - 1] == 1){
						// 레이블 다음 :
						// goto문을 없앴으므로 여긴 올 수가 없는 곳이다.
						
						/*
						// DEPRECATED : no label statement right now
						// 에러가 날 수 없다. 왜냐하면, 이 블록으로 넘어오는 조건이 elems[i]가 ':'가 되는
						// 것이기 때문.
						generatelabel(focus, bundle, startidx,
								curidx - elemlen, params, paramlen);
						params[0] = params[1] = null;
						paramlen = 0;
						spslen = 0;
						actions[actlen - 1] = subactions[actlen - 1] = -1;
						continue;
						*/
						
						
						
					}else if(subactions[actlen - 1] == 2 || subactions[actlen - 1] == 3){
						// 알고 보니 대입문일 때(2), 명령문일 때(3)
						//logln("대입문이다! 또는 명령문이다! subactions[actlen - 1] : " + subactions[actlen - 1]);

						int starti = (Integer) params[paramlen - 2];
						while (starti != i){
							curidx -= getlen(elems[i]);
							i--;
						}
						// current position : starti = i인 상황, curidx에서 elems[starti]빼줌..
						
						if(subactions[actlen - 1] == 2){
							actions[actlen - 1] = 4;
							subactions[actlen - 1] = 0;
							declareditems[dcilen - 1] = 4;
						}else{
							actions[actlen - 1] = 1;
							subactions[actlen - 1] = 0;
							declareditems[dcilen - 1] = 1;
						}

						spslen = 0;
						params[paramlen - 1] = params[paramlen - 2] = null;
						
						paramlen -= 2;
						
						// 한번 더 당겨야 한다.
						curidx -= getlen(elems[i]);
						startidx = curidx;
						i--;
						continue;
					}
				}
				// end action 7

				
				
				
				
				
				
				
				
				
				else if(actions[actlen - 1] == 8){
					// FIXME IF문이다!!
					/*
					 * 0 : if 다음 공백, 식을 호출. 2 : 식 다음 'then' // 참고 : then 이전의 공백은
					 * 식에서 처리함 3 : 'then' 다음 공백
					 */
					if(subactions[actlen - 1] == 0){
						// if 다음 공백.
						if(!elems[i].equals(elems[i].toString())){
							// if 바로 뒤엔 어떤 것이 오든 '종결되지 않은 if문'이라는 에러가 가능하다.
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(startidx, curidx
											- elemlen - startidx),
									ERRCNT_IF_NOTEND));
							curidx -= elemlen;
							i--;
							actions[actlen - 1] = subactions[actlen - 1] = -1;
							continue;
						}
						String str = elems[i].toString();
						if(str.trim().length() != 0){
							// 바로 값이 튀어나왔다고 해석이 가능하다.
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(startidx, curidx
											- elemlen - startidx),
									ERRCNT_IF_NOSPACE));
							curidx -= elemlen;
							i--;
						}
						// 수식 읽기 프로시저를 부른다.(action = 2)
						subactions[actlen] = 1;
						actions[actlen] = 2;
						actlen++;

						vstartidcs[0] = curidx;
						vsilen++;

						// startps[0]에 현재 위치를 대입한다.
						startps[0] = paramlen;
						// spslen은 1이 된다.
						spslen++;
					}else if(subactions[actlen - 1] == 1){
						// 식 다음 then
						// 참고로 에러 검사를 할 필요가 없는게, 이 프로시저로 오는 조건이 elems[i]가
						// then이 된다는 것이기 때문이다.
						//logln("if문에서 then을 읽어들입니다!");

						curidx -= elemlen;
						i--;
						subactions[actlen - 1] = 2;
					}else if(subactions[actlen - 1] == 2){
						// 여전히 then을 가리킴.
						// simplified if문일 가능성이 있으므로, action = -2가 아닌 -1로 열어둔다.

						actions[actlen - 1] = subactions[actlen - 1] = -1;

						// fcranges[fcrlen] = new FloatingCodeRange(startidx,
						// curidx);
						// fcrlen++;

						IfBlock rbifb = generateifblock(startidx, curidx,
								focus, bundle, params, paramlen);
						focus = rbifb;
						// fcranges[fcrlen] = new FloatingCodeRange(curidx,
						// curidx);
						// fcrlen++;
						PlainBlock rbplb = new PlainBlock(
								new CodeRangeImpl(startidx, curidx - startidx),
								focus);
						rbifb.setIfBlock(rbplb);
						if(paramlen > 0)
							rbifb.setIfCondition((Valuable) params[0]);
						else
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(startidx, curidx
											- elemlen - startidx),
									ERRCNT_IF_WRONGCOND));

						focus = rbplb;

						paramlen = 0;
					}
				}
				// end action 8

				
				
				
				
				
				
				
				else if(actions[actlen - 1] == 9){
					// endif를 받아들이는 곳.
					if(subactions[actlen - 1] == 0){
						// endif 부분.
						// 여기로 오는 조건이 이번 elem[i]가 endif가 되는 것이므로
						// 따로 값 체크 필요 X.
						//logln("closing... focus.getParent : " + focus.getParent());
						if((focus instanceof PlainBlock)
								&& (focus.getParent() != null)
								&& (focus.getParent() instanceof IfBlock)){
							// if문을 닫을 수 있다.
							//logln("if문을 닫습니다!!");
							focus = ((IfBlock) focus.getParent()).getParent();
						}else{
							// 에러. 존재하지 않는 if문.
							bundle.addErrorMessage(new GrammarErrorMessage(
											new CodeRangeImpl(curidx - elemlen,
													elemlen), ERRCNT_IF_NOCLOSE));
						}
						subactions[actlen - 1] = -1;
						actions[actlen - 1] = -2;
						continue;
					}
				}
				// end action 9

				
				
				
				
				
				
				
				else if(actions[actlen - 1] == 10){
					// FIXME elseif를 받아들이는 곳

					if(subactions[actlen - 1] == 0){
						// elseif 다음 공백.
						if(!elems[i].equals(elems[i].toString())){
							// elseif 바로 뒤엔 어떤 것이 오든 '종결되지 않은 elseif문'이라는 에러가
							// 가능하다.
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(startidx, curidx
											- elemlen - startidx),
									ERRCNT_ELSEIF_NOTEND));
							curidx -= elemlen;
							i--;
							actions[actlen - 1] = subactions[actlen - 1] = -1;
							continue;
						}
						String str = elems[i].toString();
						if(str.trim().length() != 0){
							// 바로 값이 튀어나왔다고 해석이 가능하다.
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(startidx, curidx
											- elemlen - startidx),
									ERRCNT_ELSEIF_NOSPACE));
							curidx -= elemlen;
							i--;
						}
						// 수식 읽기 프로시저를 부른다.(action = 2)
						subactions[actlen] = 1;
						actions[actlen] = 2;
						actlen++;

						vstartidcs[0] = curidx;
						vsilen++;

						// startps[0]에 현재 위치를 대입한다.
						startps[0] = paramlen;
						// spslen은 1이 된다.
						spslen++;
					}else if(subactions[actlen - 1] == 1){
						// 식 다음 then
						// 참고로 에러 검사를 할 필요가 없는게, 이 프로시저로 오는 조건이 elems[i]가
						// then이 된다는 것이기 때문이다.
						//logln("else if문에서 then을 읽어들입니다!");

						curidx -= elemlen;
						i--;
						subactions[actlen - 1] = 2;
					}else if(subactions[actlen - 1] == 2){
						// 여전히 then을 가리킴.
						// 지금 if문이 있는지를 확인한다.
						boolean flag = focus.isRoot();
						if(!flag)
							flag = !(focus.getParent() instanceof IfBlock);
						if(flag){
							// if문일 리가 없다. 에러 발포
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(startidx, curidx
											- elemlen - startidx),
									ERRCNT_ELSEIF_NEEDIF));
							actions[actlen - 1] = -1;
							subactions[actlen - 1] = -1;
							continue;
						}

						// elseif문에서도 simplified 문이 있다 - -1로 열어둔다.

						actions[actlen - 1] = -1;
						subactions[actlen - 1] = -1;
						// fcranges[fcrlen] = new FloatingCodeRange(startidx,
						// curidx);
						// fcrlen++;

						Valuable v = null;
						if(paramlen > 0)
							v = (Valuable) params[paramlen - 1];
						else
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(startidx, curidx
											- elemlen - startidx),
									ERRCNT_IF_WRONGCOND));

						PlainBlock rbplb = new PlainBlock(
								new CodeRangeImpl(startidx, curidx - startidx),
								focus.getParent());

						IfBlock ifblck = (IfBlock) focus.getParent();
						ifblck.addElseifBlock(rbplb, v);
						focus = rbplb;

						paramlen = 0;
					}
				}
				// end action 10

				
				
				
				
				
				
				
				
				
				
				
				
				else if(actions[actlen - 1] == 11){
					// else문.
					if(subactions[actlen - 1] == 0){
						// else를 가리킨다.
						// 지금 if문이 있는지를 확인한다.
						boolean flag = focus.isRoot();
						if(!flag)
							flag = !(focus.getParent() instanceof IfBlock);
						if(flag){
							// if문일 리가 없다. 에러 발포
							//logln("THERE's NO IF");
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(startidx, curidx
											- elemlen - startidx),
									ERRCNT_ELSE_NEEDIF));
							actions[actlen - 1] = -1;
							subactions[actlen - 1] = -1;
							continue;
						}

						actions[actlen - 1] = -2;
						subactions[actlen - 1] = -1;
						// fcranges[fcrlen] = new FloatingCodeRange(startidx,
						// curidx);
						// fcrlen++;

						PlainBlock rbplb = new PlainBlock(
								new CodeRangeImpl(startidx, curidx - startidx),
								focus.getParent());

						IfBlock ifblck = (IfBlock) focus.getParent();
						ifblck.setElseBlock(rbplb);
						focus = rbplb;
						//logln("has elseb : " + ifblck.hasElseBlock());

						paramlen = 0;
					}
				}
				// end action 11
				
				
				
				
				
				
				
				else if(actions[actlen - 1] == 12){
					/*
					 * FIXME for문
					 *  0 : for 다음 공백
					 *  1 : 공백 다음 변수명
					 *  2 : 변수명 다음 공백
					 *  3 : 공백 다음 =
					 *  4 : = 다음 공백
					 *  5 : 공백 다음 값
					 *  6 : 값 다음 공백
					 *  7 : 공백 다음 to
					 *  8 : to 다음 값
					 *  9 : 값 다음 끝
					 */
					if((subactions[actlen - 1] != 9 && subactions[actlen - 1] != 8 &&
							subactions[actlen - 1] != 5)
							&& !elems[i].equals(elems[i].toString())){
						// 종결되지 않은 for문이닷!
						bundle.addErrorMessage(new GrammarErrorMessage(
								new CodeRangeImpl(startidx, curidx
										- elemlen - startidx),
								ERRCNT_FOR_NOTEND));
						
						curidx -= elemlen;
						i--;
						
						for(int j = 0; j < paramlen; j++)
							params[j] = null;
						paramlen = 0;
						if(vsilen != 0)
							vstartidcs[--vsilen] = 0;
						if(spslen != 0)
							startps[--spslen] = 0;
						
						actions[actlen - 1] = subactions[actlen - 1] = -1;
						
						continue;
					}
					
					if(subactions[actlen - 1] == 0){
						// for 다음 공백
						String str = elems[i].toString();
						if(str.trim().length() != 0){
							// 뭘까? 일단 공백이 없다라고 선언
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(startidx, curidx
											- elemlen - startidx),
										ERRCNT_FOR_NEEDSPACEAFTERFOR));
							curidx -= elemlen;
							i--;
						}
						subactions[actlen - 1]++;
					}else if(subactions[actlen - 1] == 1){
						// for 다음 공백 다음 변수.
						
						paramlen = 1;
						if(params[0] == null)
							params[0] = elems[i];
						else
							params[0] = params[0].toString() + elems[i];
						
						subactions[actlen - 1] = 2;
					}else if(subactions[actlen - 1] == 5 || subactions[actlen - 1] == 8){
						// = 다음 공백 다음 값.
						// 또는 to 다음 값.
						
						// 루프를 되돌린다.
						curidx -= elemlen;
						i--;

						vstartidcs[0] = curidx;
						vsilen = 1;
						startps[spslen] = paramlen;
						spslen++;
						
						// '값 읽기'로 가자.
						actlen++;
						subactions[actlen - 1] = 1;
						actions[actlen - 1] = 2;
					}else if(subactions[actlen - 1] == 2 || subactions[actlen - 1] == 4){
						// for 다음 공백 다음 변수명 다음 공백,
						// 또는 변수명 다음 공백 다음 = 다음 공백
						
						String str = elems[i].toString();
						if(str.trim().length() != 0){
							curidx -= elemlen;
							i--;
						}
						subactions[actlen - 1]++;
					}else if(subactions[actlen - 1] == 3){
						// 변수명 다음 공백 다음 =
						String str = elems[i].toString();
						if(!str.equals("=")){
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(startidx, curidx
											- elemlen - startidx),
										ERRCNT_FOR_NOEQUAL));
							curidx -= elemlen;
							i--;
						}
						subactions[actlen - 1]++;
					}else if(subactions[actlen - 1] == 6){
						// 공백 다음 to
						String str = elems[i].toString();
						if(!(str.toLowerCase().equals("to"))){
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(startidx, curidx
											- elemlen - startidx),
										ERRCNT_FOR_NOTO));
							curidx -= elemlen;
							i--;
						}
						subactions[actlen - 1]++;
					}else if(subactions[actlen - 1] == 7){
						// to 다음 공백
						String str = elems[i].toString();
						if(str.trim().length() != 0){
							curidx -= elemlen;
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(startidx, curidx
											- elemlen - startidx),
										ERRCNT_FOR_NEEDBLANKAFTERTO));
							i--;
						}
						subactions[actlen - 1]++;
					}else if(subactions[actlen - 1] == 9){
						// 끝
						focus = generateforblock(startidx, curidx, focus, bundle, params, paramlen);
						paramlen = vsilen = spslen = 0;
						actions[actlen - 1] = -2;
						subactions[actlen - 1] = 0;
					}
				}
				// end action 12
				
				
				
				
				
				
				
				
				
				
				else if(actions[actlen - 1] == 13){
					// next.
					if(subactions[actlen - 1] == 0){
						// 공백
						if(!(elems[i] instanceof Token)){
							// next문이 중간에 끊겼다.
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(startidx, curidx
											- elemlen - startidx),
									ERRCNT_NEXT_NOTEND));
							
							// termination.
							curidx -= elemlen;
							i--;
							
							for(int j = 0; j < paramlen; j++)
								params[j] = null;
							paramlen = 0;
							if(vsilen != 0)
								vstartidcs[--vsilen] = 0;
							if(spslen != 0)
								startps[--spslen] = 0;
							
							actions[actlen - 1] = subactions[actlen - 1] = -1;
							
							continue;
						}
						String str = elems[i].toString();
						if(str.trim().length() != 0){
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(startidx, curidx
											- elemlen - startidx),
									ERRCNT_NEXT_NEEDSPACE));
							curidx -= elemlen;
							i--;
						}
						subactions[actlen - 1]++;
					}else if(subactions[actlen - 1] == 1){
						// 공백 다음 변수명
						if(!elems[i].equals(elems[i].toString())){
							// 변수명 읽기 끝.
							i--;
							curidx -= elemlen;
							subactions[actlen - 1]++;
							continue;
						}
						String str = elems[i].toString();
						if(str.trim().length() == 0){
							i--;
							curidx -= elemlen;
							subactions[actlen - 1]++;
							continue;
						}
						
						if(params[0] == null)
							params[0] = str;
						else
							params[0] = params[0].toString() + str;
					}else if(subactions[actlen - 1] == 2){
						// 변수명이 끝났을 때
						i--;
						curidx -= elemlen;
						
						actions[actlen - 1] = -2;
						subactions[actlen - 1] = -1;
						
						if(!(focus instanceof ForBlock)){
							// for문이 없다.
							//logln("not valid termination!");
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(curidx - elemlen,
											elemlen), ERRCNT_NEXT_FORNOTAVAILABLE));
							continue;
						}
						
						//logln("closing... this FOR BLOCK");
						if(!((ForBlock)focus).getTarget().equals(params[0].toString())){
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(curidx - elemlen,
											elemlen), ERRCNT_NEXT_WRONGVAR));
							continue;
						}
						
						//logln("closing the for block here!");
						focus = focus.getParent();
						continue;
					}
				}
				
				
				
				
				
				
				
				/* NOT USED
				else if(actions[actlen - 1] == 14){
					// FIXME goto 문입니다.
					if(subactions[actlen - 1] == 0){
						// goto 다음 공백
						if(!(elems[i] instanceof Token)){
							// goto문이 중간에 끊겼다.
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(startidx, curidx
											- elemlen - startidx),
									ERRCNT_GOTO_NOTEND));
							
							// termination.
							curidx -= elemlen;
							i--;
							
							for(int j = 0; j < paramlen; j++)
								params[j] = null;
							paramlen = 0;
							if(vsilen != 0)
								vstartidcs[--vsilen] = 0;
							if(spslen != 0)
								startps[--spslen] = 0;
							
							actions[actlen - 1] = subactions[actlen - 1] = -1;
							
							continue;
						}
						String str = elems[i].toString();
						if(str.trim().length() != 0){
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(startidx, curidx
											- elemlen - startidx),
									ERRCNT_GOTO_NEEDSPACE));
							curidx -= elemlen;
							i--;
						}
						subactions[actlen - 1]++;
					}else if(subactions[actlen - 1] == 1){
						// 공백 다음 label명
						if(!(elems[i] instanceof Token)){
							// goto문이 중간에 끊겼다.
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(startidx, curidx
											- elemlen - startidx),
									ERRCNT_GOTO_NOTEND));
							
							// termination.
							curidx -= elemlen;
							i--;
							
							for(int j = 0; j < paramlen; j++)
								params[j] = null;
							paramlen = 0;
							if(vsilen != 0)
								vstartidcs[--vsilen] = 0;
							if(spslen != 0)
								startps[--spslen] = 0;
							
							actions[actlen - 1] = subactions[actlen - 1] = -1;
							
							continue;
						}
						params[0] = elems[i].toString();
						paramlen = 1;

						// 값 다음 ending.
						generategotostmt(focus, bundle, startidx, curidx,
								params, paramlen);
						// 이제 아무것도 할 수 없다. 주석문만 가능.
						params[0] = null;
						paramlen = 0;
						curidx -= elemlen;
						i--;
						
						vstartidcs[0] = 0;
						vsilen = 0;
						startps[0] = 0;
						spslen = 0;
						
						actions[actlen - 1] = -2;
						subactions[actlen - 1] = -1;
					}
				}
				*/
				
				
				
				
				
				
				
				
				
				else if(actions[actlen - 1] == 15){
					// FIXME while문
					if(subactions[actlen - 1] == 0){
						// while다음 공백
						
						// whitespace가 아니면 일단 value reader로 떠넘김.
						if(!(elems[i] instanceof Token) || ((Token)elems[i]).toString().trim().length() != 0){
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(startidx, curidx
											- elemlen - startidx),
									ERRCNT_WHILE_NEEDSPACE));
							curidx -= elemlen;
							i--;
						}
						subactions[actlen - 1]++;
					}else if(subactions[actlen - 1] == 1){
						// while다음 공백 다음 값읽기
						i--;
						curidx -= elemlen;
						
						// 수식 읽기 프로시저를 부른다.(action = 2)
						subactions[actlen] = 1;
						actions[actlen] = 2;
						actlen++;

						vstartidcs[0] = curidx;
						vsilen++;

						// startps[0]에 현재 위치를 대입한다.
						startps[0] = paramlen;
						// spslen은 1이 된다.
						spslen++;
					}else if(subactions[actlen - 1] == 2){
						focus = generatewhileblock(startidx, curidx, focus, bundle, params, paramlen);
						// 남은 것은 한 스텝 뒤로 물린 뒤, 다른 section에서 처리하도록 함.
						params[0] = null;
						paramlen = 0;
						curidx -= elemlen;
						i--;

						vstartidcs[0] = 0;
						vsilen = 0;
						startps[0] = 0;
						spslen = 0;
						
						actions[actlen - 1] = -2;
						subactions[actlen - 1] = -1;
					}
				}
				
				
				
				
				
				
				
				
				
				
				else if(actions[actlen - 1] == 16){
					// WEND부분.
					if(subactions[actlen - 1] == 0){
						// 여기에 올 조건이 wend가 오는 때이다.
						
						actions[actlen - 1] = -2;
						subactions[actlen - 1] = -1;
						
						if(!(focus instanceof WhileBlock)){
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(curidx - elemlen,
											elemlen), ERRCNT_WEND_WHILENOTAVAILABLE));
							continue;
						}
						focus = focus.getParent();
					}
				}
				
				
				
				
				
				
				
				else if(actions[actlen - 1] == 17){
					// FIXME break부분.
					if(subactions[actlen - 1] == 0){
						// 여기에 올 조건이 break이 오는 때이다.
						
						actions[actlen - 1] = -2;
						subactions[actlen - 1] = -1;
						
						Block bfocus = focus;
						boolean found = false;
						while(bfocus != root){
							if(bfocus instanceof WhileBlock || bfocus instanceof ForBlock){
								found = true;
								break;
							}
							bfocus = bfocus.getParent();
						}
						
						if(!found){
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(curidx - elemlen,
											elemlen), ERRCNT_NOLOOP));
						}
						generatebreakstatement(focus, bundle, startidx, curidx);
					}
				}
				
				
				
				
				
				
				
				
				
				
				
				else if(actions[actlen - 1] == 18){
					// FIXME continue부분.
					if(subactions[actlen - 1] == 0){
						actions[actlen - 1] = -2;
						subactions[actlen - 1] = -1;
						
						Block bfocus = focus;
						boolean found = false;
						while(bfocus != root){
							if(bfocus instanceof WhileBlock || bfocus instanceof ForBlock){
								found = true;
								break;
							}
							bfocus = bfocus.getParent();
						}
						
						if(!found){
							bundle.addErrorMessage(new GrammarErrorMessage(
									new CodeRangeImpl(curidx - elemlen,
											elemlen), ERRCNT_NOLOOP));
						}
						generatecontinuestatement(focus, bundle, startidx, curidx);
					}
				}
				
				
				
				
				
				
				
				
				
				// FIXME 이 아래는 기본 프로시저입니다.
				else if(actions[actlen - 1] == -1){
					if(elems[i] instanceof Element)
						focus.addElement((Element) elems[i]);
					else if(elems[i] instanceof Valuable){
						// 이런 때 가능한 케이스가 있나? 없다고 일단 가정하자
						bundle.addErrorMessage(new GrammarErrorMessage(
								new CodeRangeImpl(startidx, curidx - startidx),
								ERRCNT_WRONGSENT));
						continue;
					}else if(elems[i] instanceof Token){
						String str = elems[i].toString();
						if(str.trim().length() == 0){
							continue;
						}

						startidx = curidx - elemlen;
						
						String lowerstr = str.toLowerCase();
						
						if(lowerstr.equals("dim")){
							// 변수 선언
							actions[actlen - 1] = 0;
							paramlen = 0;
							subactions[actlen - 1] = 0;

							declareditems[dcilen] = 0;
							dcilen++;
							continue;
						}else if(lowerstr.equals("const")){
							//logln("상수 선언 시작.");
							actions[actlen - 1] = 3;
							subactions[actlen - 1] = 0;

							declareditems[dcilen] = 3;
							dcilen++;
							continue;
						}else if(lowerstr.equals("if")){
							// if문이다.
							//logln("IF문 시작!");
							actions[actlen - 1] = 8;
							subactions[actlen - 1] = 0;

							declareditems[dcilen] = 8;
							dcilen++;
							continue;
						}else if(lowerstr.equals("elseif")){
							// if문이다.
							//logln("ELSEIF문 시작!");
							actions[actlen - 1] = 10;
							subactions[actlen - 1] = 0;

							declareditems[dcilen] = 10;
							dcilen++;
							continue;
						}else if(lowerstr.equals("for")){
							// for문이다.
							//logln("FOR문 시작!");
							actions[actlen - 1] = 12;
							subactions[actlen - 1] = 0;
							
							declareditems[dcilen] = 12;
							dcilen++;
							continue;
						}else if(lowerstr.equals("next")){
							// next.
							//logln("FOR 닫기 시작!");
							actions[actlen - 1] = 13;
							subactions[actlen - 1] = 0;
							
							declareditems[dcilen] = 13;
							dcilen++;
							continue;
						}else if(lowerstr.equals("else")){
							// else문이다.
							//logln("else문 선언!");

							actions[actlen - 1] = 11;
							subactions[actlen - 1] = 0;

							declareditems[dcilen] = 11;
							dcilen++;

							continue;
						}else if(lowerstr.equals("while")){
							//logln("while문 선언");
							
							actions[actlen - 1] = 15;
							subactions[actlen - 1] = 0;
							
							declareditems[dcilen] = 15;
							dcilen++;
							
							continue;
						}else if(lowerstr.equals("wend")){
							//logln("wend문 선언");
							actions[actlen - 1] = 16;
							subactions[actlen - 1] = 0;

							curidx -= elemlen;
							i--;
							
							declareditems[dcilen] = 16;
							dcilen++;
							continue;/*
						}else if(str.toLowerCase().equals("goto")){
							//logln("goto문 선언.");
							
							actions[actlen - 1] = 14;
							subactions[actlen - 1] = 0;
							
							paramlen = 0;

							startidx = curidx - elemlen;
							curidx -= elemlen;
							i--;
							declareditems[dcilen] = 11;
							dcilen++;
							
							continue;*/
						}else if(lowerstr.equals("endif")){
							// endif문이다.
							//logln("endif 선언!");
							actions[actlen - 1] = 9;
							subactions[actlen - 1] = 0;
							
							curidx -= elemlen;
							i--;
							
							declareditems[dcilen] = 9;
							dcilen++;
							continue;/*
						}else if(isDigitSet(str)){
							//logln("number-label 시작.");
							int len = elemlen;
							startidx = curidx - len;
							actions[actlen - 1] = 6;
							subactions[actlen - 1] = 0;

							params[0] = str;
							paramlen = 1;

							declareditems[dcilen] = 6;
							dcilen++;
							continue;*/
						}else if(lowerstr.equals("break")){
							actions[actlen - 1] = 17;
							subactions[actlen - 1] = 0;
							
							curidx -= elemlen;
							i--;
							
							declareditems[dcilen] = 17;
							dcilen++;
							continue;
						}else if(lowerstr.equals("continue")){
							actions[actlen - 1] = 18;
							subactions[actlen - 1] = 0;
							
							curidx -= elemlen;
							i--;
							
							declareditems[dcilen] = 18;
							dcilen++;
							continue;
						}else{
							//logln("label 시작.");

							// params[1]은 label 이름 수록
							// params[0]은 만약 알고보니 대입문이거나 명령문일 때 되돌아가기 위해 idx수록.
							paramlen += 2;
							params[paramlen - 2] = i;

							curidx -= elemlen;
							i--;
							
							actions[actlen - 1] = 7;
							subactions[actlen - 1] = 0;

							declareditems[dcilen] = 7;
							dcilen++;
							continue;
						}
					}
				}else if(actions[actlen - 1] == -2){
					if(elems[i] instanceof Comment){
						// comment에 해당하는 action이 없기에, -3으로 대체한ㄷ.
						declareditems[dcilen] = -3;
						dcilen++;
						focus.addElement((Comment) elems[i]);
					}else if(subactions[actlen - 1] == 1){
						// 아무것도 못오는 걸 연장시키고 있음.
						;
					}else{
						if(elems[i].equals(elems[i].toString()))
							if(elems[i].toString().trim().length() == 0)
								continue;
						// comment를 제외한 아무것도 올 수 없는데 아무거나 왔다! 에러다!\
						//logln("더 이상 올 수 없음!!");
						int len = elemlen;
						startidx = curidx - len;
						subactions[actlen - 1] = 1;
					}
				}
			}
			
			
			logln("----------- FINISHED ----------");
			logln("actlen : " + actlen + " , action : "
					+ actions[actlen - 1] + " , subaction : "
					+ subactions[actlen - 1] + " , startidx : " + startidx
					+ " , curidx : " + curidx);
			log("params : ");
			for(int j = 0; j < paramlen; j++)
				log("[" + params[j] + "] , ");
			logln();

			focus = knot(actions, subactions, actlen, startidx, curidx, bundle,
					paramlen, params, focus, vstartidcs,
					vsilen, startps, spslen);
			focus = checkSimplifiedIfBlock(declareditems, dcilen, bundle,
					focus, root);
			checkBlockTermination(focus, bundle);
			
			logln("generated items : ");
			logln(focus + " , " + focus.getClass());
			
			printerror("--- errors ---");
			for(ErrorMessage msg : bundle.getErrorMessages())
				printerror(msg);
			printerror("TOTAL TIME : "
					+ (System.currentTimeMillis() - startmillitime));
			
			
			fireParsingEnd(reader);
			//eventShooter.stop();
			logln("============= END BASICParser! ==========");
			return root;
			// FIXME 마지막
		}catch (IOException ioexcp){
			state = WorkingState.UNEMPLOYED;
			throw ioexcp;
		}catch (NullPointerException npexcp){
			state = WorkingState.UNEMPLOYED;
			throw npexcp;
		}catch (Exception excp){
			state = WorkingState.UNEMPLOYED;
			excp.printStackTrace();
		}
		return null;
	}// end parse(Reader, ErrorBundle)

	private boolean isDigitSet(String str){
		for(int i = 0; i < str.length(); i++)
			if('0' > str.charAt(i) || str.charAt(i) > '9')
				return false;
		return true;
	}// end isNumber



	private CommandStatement generatecmd(Block focus, ErrorBundle bundle,
			int startidx, int curidx, Object[] params, int paramlen){
		Valuable[] vars = new Valuable[paramlen - 1];
		System.arraycopy(params, 1, vars, 0, vars.length);

		//logln("generating cmd! vars len : " + vars.length	+ " , name : " + params[0]);
		for(Valuable eachv : vars)
			log(eachv/* + "(" + eachv.getCodeRange() + ") , "*/);
		//logln();

		CommandStatement cs = new CommandStatement(
				new CodeRangeImpl(startidx, curidx - startidx),
				focus,
				params[0].toString(),
				vars);

		focus.addElement(cs);
		for(int i = 0; i < vars.length; i++)
			if(vars[i] != null){
				vars[i].setParent(cs);
				vars[i].setPosition(i);
			}
		
		for(int i = 0; i < paramlen; i++)
			params[i] = null;
		
		return cs;
	}// end generatecmd

	private DeclareStatement generatedec(Block focus, ErrorBundle bundle,
			int startidx, int curidx, Object[] params, int paramlen){
		//logln("generatedec : " + params[0] + " , " + params[1] + " , paramlen : " + paramlen);
		//logln("[gen] " + params[0] + " , " + params[1]);

		String name = null;
		ValueType valueType = null;

		if(params[0] != null)
			name = params[0].toString();
		if(params[1] != null)
			valueType = (ValueType) params[1];

		CodeRange crange = new CodeRangeImpl(startidx, curidx - startidx);
		DeclareStatement rbds = new DeclareStatement(crange, focus, name, valueType);
		
		for(int j = 2; j < paramlen - 1 /*
											 * valueType을 param에 추가시 일부러 1을 한 번
											 * 더 더한다.
											 */; j += 2){
			//logln("[gen] " + params[j] + " , " + params[j + 1]);
			name = null;
			valueType = null;
			
			if(params[j] != null)
				name = params[j].toString();
			if(params[j + 1] != null)
				valueType = (ValueType) params[j + 1];
			
			String errmsg = null;
			if((errmsg = BASICGrammar.checkVarName(name)) != null){
				bundle.addErrorMessage(new GrammarErrorMessage(crange, errmsg));
			}
			rbds.addDeclaration(name, valueType);
		}
		
		for(int j = 0; j < paramlen; j++)
			params[j] = null;
		
		focus.addElement(rbds);
		return rbds;
	}// end generatedec

	private FunctionCallValue generatefunc(int subaction, ErrorBundle bundle,
			int curidx, Object[] params, int paramlen, int[] vstartidcs,
			int vsilen, int[] startps, int spslen){
		//logln("generatefunc : vsilen : " + vsilen + " , spslen : " + spslen + " ,paramlen : " + paramlen + " , sps : " + startps[spslen - 1] + " , params.len : " + params.length);
		String name = null;
		if(params[startps[spslen - 1]] != null)
			name = params[startps[spslen - 1]].toString();

		Valuable[] valparams = new Valuable[paramlen - startps[spslen - 1] - 1];
		System.arraycopy(params, startps[spslen - 1] + 1, valparams, 0,
				valparams.length);

		FunctionCallValue fcv = new FunctionCallValue(new CodeRangeImpl(
				vstartidcs[vsilen - 1], curidx - vstartidcs[vsilen - 1]), name, 
				valparams);
		
		int limit = fcv.sizeOfParameters();
		for(int j = 0; j < limit; j++){
			Valuable paramj = fcv.getParameter(j);
			paramj.setParent(fcv);
			paramj.setPosition(j);
		}
		
		return fcv;
	}// end generatefunc

	private ConstStatement generateconst(int[] subactions, int actlen,
			Block focus, ErrorBundle bundle, int startidx, int curidx,
			Object[] params, int paramlen){
		//logln("generateconst : " + params[0] + " , " + params[1]);
		String name = null;
		Valuable value = (Valuable) params[1];

		if(subactions[actlen - 1] <= 1){
			// 이름이 없다.
			bundle.addErrorMessage(new GrammarErrorMessage(new CodeRangeImpl(
					startidx, curidx - startidx), ERRCNT_EMPTYNAME));
		}else if(params[0] != null)
			name = params[0].toString();
		if(subactions[actlen - 1] == 5){
			// 값을 받아들이는 부분을 놓쳤다.
			bundle.addErrorMessage(new GrammarErrorMessage(new CodeRangeImpl(
					startidx, curidx - startidx), ERRCNT_CONST_NOTEND));
		}

		ConstStatement rbcs = new ConstStatement(new CodeRangeImpl(
				startidx, curidx - startidx), focus, name,
				(Value) value);
		focus.addElement(rbcs);
		return rbcs;
	}// end generateconst

	private InsertStatement generateinsstmt(Block focus,
			ErrorBundle bundle, int startidx, int curidx, Object[] params,
			int paramlen){
		//logln("generateInsStmt : " + params[0] + " , " + params[1]);

		if(params[1] == null){
			// 대입하는 값이 없다!
			bundle.addErrorMessage(new GrammarErrorMessage(new CodeRangeImpl(
					startidx, curidx - startidx), ERRCNT_INSSTMT_NOTEND));
		}

		InsertStatement is = new InsertStatement(new CodeRangeImpl(startidx, curidx
				- startidx), focus, params[0].toString(), (Valuable) params[1]);

		focus.addElement(is);
		return is;
	}// end generateinsstmt

	
	private BreakStatement generatebreakstatement(Block focus,
			ErrorBundle bundle, int startidx, int curidx){
		BreakStatement gs = new BreakStatement(new CodeRangeImpl(startidx, curidx - startidx), focus);

		focus.addElement(gs);
		return gs;
	}// end generateinsstmt

	private ContinueStatement generatecontinuestatement(Block focus,
			ErrorBundle bundle, int startidx, int curidx){
		ContinueStatement gs = new ContinueStatement(new CodeRangeImpl(startidx, curidx - startidx), focus);

		focus.addElement(gs);
		return gs;
	}// end generateinsstmt
	

	private LabelStatement generatelabel(Block focus, ErrorBundle bundle,
			int startidx, int curidx, Object[] params, int paramlen){
		LabelStatement ls = new LabelStatement(new CodeRangeImpl(startidx, curidx
				- startidx), params[1].toString(), focus);

		focus.addElement(ls);
		return ls;
	}// end generatelabel

	
	
	
	private IfBlock generateifblock(int startidx, int curidx, Block focus,
			ErrorBundle bundle, Object[] params, int paramlen){
		//logln("generateIfBlock : " + params[0] + " , curidx : " + curidx);

		IfBlock ifBlock = new IfBlock(new CodeRangeImpl(startidx, curidx
				- startidx), focus);
		focus.addElement(ifBlock);
		return ifBlock;
	}// end generatelabel

	private WhileBlock generatewhileblock(int startidx, int curidx, Block focus,
			ErrorBundle bundle, Object[] params, int paramlen){
		//logln("generateWhileBlock : " + params[0] + " , curidx : " + curidx);

		WhileBlock whileBlock = new WhileBlock(new CodeRangeImpl(startidx, curidx
				- startidx), focus, (Valuable)params[0]);
		focus.addElement(whileBlock);
		return whileBlock;
	}// end generatelabel
	
	private ForBlock generateforblock(int startidx, int curidx, Block focus,
			ErrorBundle bundle, Object[] params, int paramlen){
		//logln("generateForBlock : " + params[0] + " = " + params[1] + " TO " + params[2] + " , curidx : " + curidx);

		ForBlock forBlock = new ForBlock(new CodeRangeImpl(startidx, curidx
				- startidx), focus, params[0].toString(), (Valuable)params[1],
				(Valuable)params[2]);
		focus.addElement(forBlock);
		//logln("generated one : [" + forBlock + "]");
		
		for(int j = 0; j < paramlen; j++)
			params[j] = null;
		
		return forBlock;
	}// end generatelabel

	
	
	
	private void value_knot(Object[] params, int paramst, int paramlen, int[] vstartidcs,
			int vsilen, int curidx){
		//logln("value_knot : paramst : " + paramst + ", paramlen : " + paramlen);
		if(paramlen - paramst == 1){
			CodeRange range = new CodeRangeImpl(vstartidcs[vsilen - 1], curidx
					- vstartidcs[vsilen - 1]);
			
			if(params[paramst] instanceof Value){
				((Value)params[paramst]).setCodeRange(range);
			}else if(params[paramst] instanceof FunctionCallValue){
				((FunctionCallValue)params[paramst]).setCodeRange(range);
			}else if(params[paramst] instanceof VariableCallValue){
				((VariableCallValue)params[paramst]).setCodeRange(range);
			}else{
				// 뭐야??
				if(BugManager.DEBUG){
					BugManager.printf("BASICParser : BUG!! in value_knot : \n");
					BugManager.printf("   params[param_start = %d] has strange value! : %s\n", paramst, params[paramst]);
				}
			}
		}else{
			// Expression을 제작해야 한다.
			if(paramlen - paramst > 2){
				Valuable rootleft = (Valuable)params[paramst];
				Valuable rootright = (Valuable)params[paramst + 2];
				Expression root = new Expression(new CodeRangeImpl(
								rootleft.getCodeRange().getStartPosition(),
								rootright.getCodeRange().getStartPosition() + rootright.getCodeRange().getLength()),
							rootleft,
							rootright,
							(Operator)params[paramst + 1]);
				
				Expression focus;
				Operator op;
				Valuable nxtval;
				int idx = paramst + 3;
				
				while(idx < paramlen){
					op = (Operator)params[idx];
					nxtval = (Valuable)params[idx + 1];
					
					if(op == null || nxtval == null)
						break;
					
					if(Operator.compareRank(root.getOperator(), op) >= 0){
						root = new Expression(new CodeRangeImpl(
									root.getCodeRange().getStartPosition(),
									nxtval.getCodeRange().getStartPosition() + rootright.getCodeRange().getLength()),
								root,
								nxtval,
								op);
					}else{
						focus = root;
						while(focus.getRight() instanceof Expression &&
								Operator.compareRank(((Expression)focus.getRight()).getOperator(), op) == -1){
							focus = (Expression)focus.getRight();
						}
						
						CodeRange focusrcr = focus.getRight().getCodeRange();
						Expression exp = new Expression(new CodeRangeImpl(
									focusrcr.getStartPosition(),
									nxtval.getCodeRange().getStartPosition() + nxtval.getCodeRange().getLength()),
								focus.getRight(),
								nxtval,
								op);
						focus.setRight(exp);
					}
					
					idx += 2;
				}
				params[paramst] = root;
			}else
				params[paramst] = null;
			
			for(int i = paramst + 1; i < paramlen; i++)
				params[i] = null;
		}
	}// end value_knot

	private Block checkSimplifiedIfBlock(int[] declareditems, int dcilen,
			ErrorBundle bundle, Block focus, Block root){
		// 만약 이 라인에 if문 선언이 있었을 때, 그 if 블록이 과연 단순화된 if블록이
		// 될 수 있는가에 대한 것을 묻고 있다.
		// 단순화된 if블록이 될 조건을 제시한다.
		if(dcilen == 0)
			return focus;
		// FIXME checkSimpIfBlck
		boolean labelStmtNotFinished = true;
		boolean canBeSimplifiedIfBlock = true;
		boolean commonStatementAppeared = false;
		boolean hasIfBlock = false;
		boolean hasElseifBlock = false;
		if(declareditems[dcilen - 1] == 8 || declareditems[dcilen - 1] == 10){
			canBeSimplifiedIfBlock = false;
		}else{
			log("dcis : ");
			for(int j = 0; j < dcilen; j++){
				log(declareditems[j] + " , ");
				if(labelStmtNotFinished
						&& (declareditems[j] == 6 || declareditems[j] == 7))
					continue;

				labelStmtNotFinished = false;
				if(declareditems[j] == -3){
					// 주석이었다.
					// 어떤 일이 있어도 주석 뒤에 다른 아이템이 올 수 없다. 따라서 상황을 종료한다.
					if(commonStatementAppeared){
						// simplified if block이 될 조건을 갖추었다.
						canBeSimplifiedIfBlock = true;
						j = dcilen;
						log("(finished in the first)");
						continue;
					}
				}else if(declareditems[j] == 9){
					// endif가 나왔다란 말이다.
					// 실제로는 한 라인 내에 선언과 종결이 동시에 나오면 안되지만 이 에러는
					// endif 처리시에 처리했다.
					canBeSimplifiedIfBlock = false;
					log("(finished in the second)");
					j = dcilen;
					continue;
				}else if(commonStatementAppeared
						&& (declareditems[j] != 7 && declareditems[j] != 6)){
					// 일반 문장이 두번 나왔다.
					// simplified가 될 수 없다.
					canBeSimplifiedIfBlock = false;
					j = dcilen;
					log("(finished in the third)");
					continue;
				}else if(declareditems[j] == 10 && hasIfBlock){
					// elseif문이 지금 나올 순 없지만, 일단 simplified로는 처리한다.
					canBeSimplifiedIfBlock = true;
					hasElseifBlock = true;
					j = dcilen;
					continue;
				}
				if(declareditems[j] == 8)
					hasIfBlock = true;
				else if(declareditems[j] == 10)
					hasElseifBlock = true;
				else if(declareditems[j] != 7 && declareditems[j] != 6)
					commonStatementAppeared = true;
			}
			//logln("\nFINISHED checking simplified : canBe : " + canBeSimplifiedIfBlock + " , commstap : " + commonStatementAppeared + ", hasif : " + hasIfBlock);
		}
		// 이제 적용.
		if(canBeSimplifiedIfBlock && (hasIfBlock || hasElseifBlock)){
			Block current = focus.getParent();
			while (current != null){
				if(!(current instanceof IfBlock))
					current = current.getParent();
				else
					break;
			}
			//logln("found the block : [" + current + "]");
			if(current != root){
				((IfBlock) current).setSimplified(canBeSimplifiedIfBlock);
				focus = current.getParent();
			}
		}
		return focus;
	}// end checkSimplifiedIfBlock()
	private boolean checkUnknownValueOrOperator(String str, int action, int subaction){
		return (str.equals(",") && (action == 1 || action == 5))
			// str이 ,이면서 이 프로시저 호출자가 명령이거나 function일때
			|| (str.equals(")") && action == 5)
			// str이 )이면서 호출자가 function일 때
			|| ((str.toLowerCase().equals("then")) && (action == 8 || action == 10))
			// '이면' 이거나 then이면서 if문에서 호출하거나 elseif에서 호출했을 때
			|| (str.equals("=") && (action == 12 || (action == 5 &&
					subaction == 0)))
			// =이면서 호출자가 for문 또는 대입문의 변수부분일 때
			|| ((str.toLowerCase().equals("to")) && (action == 12
					&& subaction == 5));
	}
	private void checkBlockTermination(Block focus, ErrorBundle eb){
		// focus가 덜 닫혔을 경우에 대해서.
		if(focus.isRoot())
			return;

		//logln("------NOT CLOSED! -------- ");
		Block checkfocus = focus;
		while (!checkfocus.isRoot()){
			//logln("checkfocus : " + checkfocus.getClass().getSimpleName());
			if(checkfocus instanceof IfBlock){
				eb.addErrorMessage(new GrammarErrorMessage(checkfocus
						.getCodeRange(), ERRCNT_IF_NOTCLOSED));
			}else if(checkfocus instanceof ForBlock){
				eb.addErrorMessage(new GrammarErrorMessage(checkfocus
						.getCodeRange(), ERRCNT_FOR_NOTCLOSED));
			}else if(focus instanceof WhileBlock)
				eb.addErrorMessage(new GrammarErrorMessage(checkfocus
						.getCodeRange(), ERRCNT_WHILE_NOTEND));
			
			checkfocus = checkfocus.getParent();
		}
	}// end checkBlockTErmination

	
	
	
	
	
	// 여태까지 만든 것들의 끝을 맺어준다.
	private Block knot(int[] actions, int[] subactions, int actlen,
			int startidx, int curidx, ErrorBundle eb, int paramlen,
			Object[] params, Block focus, int[] vstartidcs,
			int vsilen, int[] startps, int spslen){
		// FIXME knot
		//logln(" --끝맺기. actlen : " + actlen + " , action : " + actions[actlen - 1] + ", subaction : " + subactions[actlen - 1] + " , curidx : " + curidx + " , vsilen : " + vsilen);
		log("params : ");
		for(int j = 0; j < paramlen; j++)
			log("[" + params[j] + "] , ");
		log("\nvstartidcs : ");
		for(int j = 0; j < vsilen; j++)
			log("[" + vstartidcs[j] + "] , ");
		log("\nstartps : ");
		for(int j = 0; j < spslen; j++)
			log("[" + startps[j] + "] , ");
		//logln();
		
		if(actions[actlen - 1] == -2 && subactions[actlen - 1] == 1){
			//logln("아무것도 못 오는 것을 연장하는 경우였다.");
			eb.addErrorMessage(new GrammarErrorMessage(new CodeRangeImpl(
					startidx, curidx - startidx), ERRCNT_NOTADMITTED));
			
			
		}else if(actions[actlen - 1] == 0){
			//logln("변수를 선언하고 있었다.");
			// 변수선언.
			if(subactions[actlen - 1] != 6 && subactions[actlen - 1] != 7)
				// 변수 선언 도중 마친 셈이 된다.
				eb.addErrorMessage(new GrammarErrorMessage(new CodeRangeImpl(
						startidx, curidx - startidx), ERRCNT_VARDEC_NOTEND));
			generatedec(focus, eb, startidx, curidx, params, paramlen);
			
			
		}else if(actions[actlen - 1] == 1){
			//logln("명령어를 만들고 있었다.");
			// 명령어 제작.
			if(subactions[actlen - 1] == 4){
				// 쉼표를 읽어들이다 끊겼다.
				eb.addErrorMessage(new GrammarErrorMessage(new CodeRangeImpl(
						startidx, curidx - startidx), ERRCNT_NOTCOMMA));
			}else if(subactions[actlen - 1] == 2){
				if(paramlen != 1){
					// params 제작 도중 끊김. 끊긴 것을 param이 null이라고 판단한다.(move처럼)
					// 마지막에 null을 추가해주자.
					params[paramlen] = null;
					paramlen++;
				}
			}
			generatecmd(focus, eb, startidx, curidx, params, paramlen/*,
					subactions[actlen - 1]*/);
			
			
		}else if(actions[actlen - 1] == 2){
			//logln("수식(값)을 만들고 있었다.");
			if(subactions[actlen - 1] == 1)
				// 에러 발생.
				eb.addErrorMessage(new GrammarErrorMessage(
						new CodeRangeImpl(vstartidcs[vsilen - 1], curidx
								- vstartidcs[vsilen - 1]), ERRCNT_WRONGSENT));
			else if(subactions[actlen - 1] == 8){
				// 함수를 읽어들이고 있었음.
				//logln("수식 값에서 함수값을 읽고 있었다.");
				// 아무것도 안해도 됨. 
			}else if(subactions[actlen - 1] == 10){
				// 수식을 읽고 있었음.
			}

			actlen--;
			subactions[actlen - 1]++;
			// params[paramlen++] = vroot;
			value_knot(params, startps[spslen - 1], paramlen, vstartidcs, vsilen, curidx);

			// 'value reader' section을 invoke한 곳으로 다시 돌아감.
			paramlen = startps[spslen - 1] + 1;
			vsilen--;
			spslen--;

			focus = knot(actions, subactions, actlen, startidx, curidx, eb, paramlen,
					params, focus, vstartidcs, vsilen,
					startps, spslen);
			
			
		}else if(actions[actlen - 1] == 3){
			// 상수 선언중이었다.
			//logln("상수 선언중이었음.");
			if(subactions[actlen - 1] != 6)
				eb.addErrorMessage(new GrammarErrorMessage(new CodeRangeImpl(
						startidx, curidx - startidx), ERRCNT_CONST_NOTEND));
			generateconst(subactions, actlen, focus, eb, startidx, curidx,
					params, paramlen);
			
			
		}else if(actions[actlen - 1] == 4){
			// 대입식.
			//logln("대입식을 생성하고 있었다.");
			generateinsstmt(focus, eb, startidx, curidx, params, paramlen);
			
			
		}else if(actions[actlen - 1] == 5){
			//logln("함수를 만들고 있었다.");
			// 함수 선언.
			// 구지 subaction == 9인지 평가할 필요가 없다.
			// 왜냐하면, subaction == 9일 때 에러가 없는데, subaction == 9가
			// 될 조건이 이번 token이 ')'가 된다 라는 것이기 때문이다. 이번 토큰은 ')'가 아닌
			// linebreak이므로, 항상 에러.
			if(subactions[actlen - 1] <= 3){
				eb.addErrorMessage(new GrammarErrorMessage(
						new CodeRangeImpl(vstartidcs[vsilen - 1], curidx
								- vstartidcs[vsilen - 1]),
						ERRCNT_FUNC_NOTOPPRTHSIS));
				// 함수명 삭제.
				params[startps[spslen - 1] - 1] = null;
			}else{
				eb.addErrorMessage(new GrammarErrorMessage(
						new CodeRangeImpl(vstartidcs[vsilen - 1], curidx
								- vstartidcs[vsilen - 1]), ERRCNT_FUNC_NOTEND));
				// 함수명 삭제.
				params[startps[spslen - 1]] = null;
			}
			actlen--;
			subactions[actlen - 1]++;

			// 더하기 1을 하는 이유는 function이 들어가므로
			paramlen = startps[spslen - 1] + 1;
			// startidx 삭제.
			vsilen--;
			spslen--;

			focus = knot(actions, subactions, actlen, startidx, curidx, eb, paramlen,
					params, focus, vstartidcs, vsilen,
					startps, spslen);
			
			
		}else if(actions[actlen - 1] == 7){
			// label을 읽고 있었다.
			//logln("label을 읽고 있었다.");
			if(subactions[actlen - 1] == 0){
				// 명령어로 돌린다. 아래와 같은 문장일 수도 있다.
				// asdfasdfasdfsfda
				// current paramlen == 2 -> paramlen = 1
				paramlen = 1;
				params[0] = params[1];
				generatecmd(focus, eb, startidx, curidx, params, paramlen/*,
						subactions[actlen - 1]*/);
				//eb.addErrorMessage(new GrammarErrorMessage(new CodeRangeImpl(
				//		startidx, curidx - startidx), ERRCNT_WRONGSENT));
			}
			
			
		}else if(actions[actlen - 1] == 8){
			if(subactions[actlen - 1] == 1){
				// 식이 아직 오지 않았다.
				eb.addErrorMessage(new GrammarErrorMessage(new CodeRangeImpl(
						startidx, curidx - startidx), ERRCNT_IF_NOTEND));
			}
			
			
		}else if(actions[actlen - 1] == 11){
			PlainBlock rbplb = new PlainBlock(
					new CodeRangeImpl(startidx, curidx - startidx),
					focus.getParent());

			IfBlock ifblck = (IfBlock) focus.getParent();
			ifblck.setElseBlock(rbplb);
			focus = rbplb;
			
			
		}else if(actions[actlen - 1] == 12){
			//logln("for문을 선언하고 있었다.");
			focus = generateforblock(startidx, curidx, focus, eb, params, paramlen);
			paramlen = vsilen = spslen = 0;
			
			
		}else if(actions[actlen - 1] == 13){
			//logln("for문을 닫고 있었다.");
			if(!(focus instanceof ForBlock))
				eb.addErrorMessage(new GrammarErrorMessage(
						new CodeRangeImpl(curidx -= (params[0].toString()).length(),
								(params[0].toString()).length()), ERRCNT_NEXT_FORNOTAVAILABLE));
			
			
			else{
				if(subactions[actlen - 1] == 0){
					// 중간에 끊김
					eb.addErrorMessage(new GrammarErrorMessage(
							new CodeRangeImpl(startidx, curidx - startidx),
							ERRCNT_NEXT_NOTEND));
				}else{
					String identifier = params[0].toString();
					//logln("closing... this FOR BLOCK");
					if(!((ForBlock)focus).getTarget().equals(identifier.toLowerCase())){
						eb.addErrorMessage(new GrammarErrorMessage(
								new CodeRangeImpl(curidx -= (params[0].toString()).length(),
										(params[0].toString()).length()), ERRCNT_NEXT_WRONGVAR));
					}else{
						focus = focus.getParent();
					}
				}
			}
			
			
		}else if(actions[actlen - 1] == 14){
			//logln("goto문 선언중이었음.");
			// curidx는 linebreak뺀거임
			eb.addErrorMessage(new GrammarErrorMessage(
					new CodeRangeImpl(startidx, curidx
							- startidx),
					ERRCNT_GOTO_NOTEND));
			
			
		}else if(actions[actlen - 1] == 15){
			//logln("while문을 선언하고 있었다.");

			if(params[0] == null)
				eb.addErrorMessage(new GrammarErrorMessage(
						new CodeRangeImpl(startidx, curidx - startidx),
						ERRCNT_WHILE_NOCONDITION));
			focus = generatewhileblock(startidx, curidx, focus, eb, params, paramlen);
			paramlen = vsilen = spslen = 0;
			
			
			
		}

		paramlen = 0;
		params[0] = null;
		params[1] = null;
		
		return focus;
	}// end knot()

	private int getlen(Object obj) throws Exception {
		if(obj instanceof Token)
			return obj.toString().length();
		else if(obj instanceof String)
			return obj.toString().length();
		else if(obj instanceof Element)
			return ((Element) obj).getCodeRange().getLength();
		else if(obj instanceof Valuable)
			return ((Valuable) obj).getCodeRange().getLength();
		throw new Exception("일어날 수 없는 경우 : " + obj.getClass());
	}// end getlen

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * expression 등을 응집시킵니다.
	 * 
	 * @param token
	 * @return
	 */
	private Object[] cohere(Token[] token, ErrorBundle eb){
		// stack에 올 수 있는 것들 : Token, 
		Stack<Object> stack = new Stack<Object>();

		Object[] params = new Object[40];
		int paramlen = 1;
		// 이건 코드 기록용
		params[0] = new StringBuffer();
		/*
		 * action에 가능한 경우들 : -1 : not working. 0 : 주석이다. 1 : pre-processor이다. 2 :
		 * 문자열.
		 */
		int action = -1;
		/*
		 * subaction에 가능한 경우들 : -2 : 다음에 오는 것은 무조건 에러. 단, 주석은 제외. -1 : 아무거나.
		 * 
		 * 1. comment 0 : 주석 내용을 받아들임 2. pre-processor options 0 : device/limit
		 * 읽어들임. 1 : 옵션 읽음. 2 : 네임 다음 공백 읽기. 3 : 옵션 다음 공백 읽기. 3. 문자열 0 : 읽어들임.
		 */
		int subaction = -1;
		// action을 시작할 때의 위치
		int startidx = -1;
		// 현재 위치
		int curidx = 0;
		for(int i = 0; i < token.length; i++){
			curidx = token[i].getStartPosition() + token[i].getLength();
			//logln("token[" + i + "] : '" + token[i] + "'[" + token[i].getStartPosition() + "-" + token[i].getLength() + "]");
			// //logln("action : " + action + " , subaction : " +
			// subaction +
			// " , startidx : " + startidx + " , curidx : " + curidx +
			// " , token : [" + token[i] + "]");
			if(token[i].equals(linebreak)){
				curidx -= token[i].getLength();
				Object obj = cohere_knot(action, startidx, curidx,
						eb,
						paramlen, params);
				if(obj != null)
					stack.push(obj);
				params[0] = new StringBuffer();
				params[1] = null;
				// //logln("LINE-BREAK! token[" + i + "] : " +
				// token[i]);
				stack.push(token[i]);
				curidx += token[i].getLength();

				action = -1;
				subaction = -1;
				startidx = curidx;
				continue;
			}

			if(action == 0){
				((StringBuffer) params[0]).append(token[i]);
				paramlen = 1;
				continue;
			}else if(action == 2){
				if(token[i].equals("\"")){
					// 끝났다.
					// //logln("문자열 끝!");
					action = -1;
					subaction = -1;
					Value v = new Value(new CodeRangeImpl(startidx, curidx
							- startidx), ValueType.TYPE_STRING, params[0]
							.toString());
					stack.push(v);
					startidx = curidx;
					continue;
				}
				((StringBuffer) params[0]).append(token[i]);
			}

			// 아무것도 하지 않거나 오직 주석만 허용될
			else if(action == -1 || action == -2){
				if(token[i].equals("'")){
					// //logln(i + " [" + token[i] + "] : 주석 파싱
					// 시작.");
					startidx = curidx - token[i].length();
					((StringBuffer) params[0]).setLength(0);
					action = 0;
					continue;
				}else if(token[i].toString().startsWith("\"")){
					// 문자열 모드.
					//logln("문자열 시작 in cohering : " + curidx);
					((StringBuffer) params[0]).setLength(0);
					startidx = curidx - token[i].length();
					paramlen = 1;
					action = 2;
					subaction = 0;
				}else{
					// //logln("token[" + i + "] : " + token[i]);
					stack.push(token[i]);
				}
			}
		}

		Object obj = cohere_knot(action, startidx, curidx, eb, paramlen, params);
		if(obj != null)
			stack.push(obj);
		return stack.toArray(new Object[] {});
	}// end cohere(String[])

	// 전개해 온 것을 매듭짓는다.
	private Object cohere_knot(int action, int startidx, int curidx,
			ErrorBundle eb, int paramlen, Object[] params){
		if(action == 0){
			// 주석.
			//logln("cohere_knot : new comment : startidx : " + startidx + " , curidx : " + curidx);
			return new Comment(new CodeRangeImpl(startidx, curidx - startidx),
					null, params[0].toString());
		}else if(action == 2){
			// 값.
			Value v = new Value(new CodeRangeImpl(startidx, curidx - startidx),
					ValueType.TYPE_STRING, params[0].toString());
			if(!params[0].toString().endsWith("\""))
				eb.addErrorMessage(new GrammarErrorMessage(new CodeRangeImpl(
						startidx, curidx), ERRCNT_STRINGNOTEND));
			return v;
		}
		return null;
	}// end knot()

	
	
	
	
	public int getListenersCount()
	{ return listeners.size(); }
	public ParsingListener getListener(int idx)
	{ return listeners.get(idx); }
	public void removeParsingListener(ParsingListener pl)
	{ listeners.remove(pl); }
	public boolean containsListener(ParsingListener pl)
	{ return listeners.contains(pl); }
	public ParsingListener[] getListeners()
	{ return listeners.toArray(new ParsingListener[]{}); }
	public void addParsingListener(ParsingListener pl){
		listeners.add(pl);
	}// end addParsingListener(ParsingListener)

	protected void fireParsingStart(Reader r){
		for(ParsingListener eachpl : listeners)
			eventShooter.add(eachpl, new ParsingEvent(this, r),
					EventShooter.TYPE_START);
	}// end fireParsingStart(Reader)
	protected void fireParsingEnd(Reader r){
		for(ParsingListener eachpl : listeners)
			eventShooter.add(eachpl, new ParsingEvent(this, r),
					EventShooter.TYPE_END);
	}// end fireParsingStart(Reader)
	
	public WorkingState getState(){
		return state;
	}// end getState()
	
	public String getParserName()
	{ return "roboBASIC PARSER beta"; }
	
	public BASICParser newInstance(){
		BASICParser parser = new BASICParser();
		return parser;
	}// end newInstance()
	
	
	
	
	
	private static class EventShooter implements Runnable{
		private Thread thread;
		private boolean stopFlag;
		public final static int TYPE_START = 0;
		public final static int TYPE_END = 1;
		
		
		private Queue<Pair> queue;
		
		public EventShooter(){
			thread = new Thread(this);
			thread.setDaemon(true);
			queue = new LinkedList<Pair>();
		}// end Constructor
		
		public void start(){
			stopFlag = false;
			queue.clear();
			thread.start();
		}// end start()
		
		public Thread getThread()
		{ return thread; }
		public void stop()
		{ this.stopFlag = true; }
		
		public void add(ParsingListener pl, ParsingEvent pe, int type)
		{ queue.add(new Pair(pl, pe, type)); }
		
		public void run(){
			while(!stopFlag){
				try{
					shoot();
					Thread.sleep(20);
				}catch(Exception excp){
					excp.printStackTrace();
				}
			}
		}// end run()
		private void shoot(){
			while(!stopFlag){
				Pair p = queue.poll();
				if(p == null)
					continue;
				
				if(p.type == TYPE_START)
					p.listener.parsingStart(p.event);
				else if(p.type == TYPE_END)
					p.listener.parsingEnd(p.event);
			}
		}// end shoot()
		
		class Pair{
			ParsingEvent event;
			ParsingListener listener;
			int type;
			Pair(ParsingListener pl, ParsingEvent pe, int type)
			{ listener = pl; event = pe; this.type = type;}
		}
	}// end class EventShooter
}