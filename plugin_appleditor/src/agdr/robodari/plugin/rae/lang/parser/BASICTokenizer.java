package agdr.robodari.plugin.rae.lang.parser;

import java.io.*;
import java.util.*;

import agdr.robodari.plugin.rae.lang.parser.*;

public class BASICTokenizer {
	public static synchronized void tokenize(Queue<Token> tokens, Reader reader)
			throws IOException{
		int data = 0;
		StringBuffer tokenbuf = new StringBuffer();
		StringBuffer blankbuf = new StringBuffer();
		StringBuffer plainbuf = new StringBuffer();
		
		String sdata;
		int start = 0;
		int totalsize = 0;
		while((data = reader.read()) != -1){
			sdata = String.valueOf((char)data);
			totalsize++;
			
			if(Operator.isOperator(String.valueOf(sdata))){ // token
				if(blankbuf.length() != 0){
					tokens.add(new Token(blankbuf.toString(), start,
							blankbuf.length()));
					blankbuf.setLength(0);
				}
				if(plainbuf.length() != 0){
					tokens.add(new Token(plainbuf.toString(), start,
							plainbuf.length()));
					plainbuf.setLength(0);
				}
				tokenbuf.append(sdata);
				start = totalsize;
			}else if(data == '\"' || data == '\''
					|| data == '\n' || data == ':' || data == '.' || 
					data == '$' || data == '(' || data == ')' || data == ','){
				if(blankbuf.length() != 0){
					tokens.add(new Token(blankbuf.toString(), start,
							blankbuf.length()));
					blankbuf.setLength(0);
				}
				if(plainbuf.length() != 0){
					tokens.add(new Token(plainbuf.toString(), start,
							plainbuf.length()));
					plainbuf.setLength(0);
				}
				if(tokenbuf.length() != 0){
					tokens.add(new Token(tokenbuf.toString(), start,
							tokenbuf.length()));
					tokenbuf.setLength(0);
				}
				tokens.add(new Token(String.valueOf((char)data), start,
						1));
				start = totalsize;
			}else if(data == ' ' || data == '\t' || data == '\r'){
				if(tokenbuf.length() != 0){
					tokens.add(new Token(tokenbuf.toString(), start,
							tokenbuf.length()));
					tokenbuf.setLength(0);
				}
				if(plainbuf.length() != 0){
					tokens.add(new Token(plainbuf.toString(), start,
							plainbuf.length()));
					plainbuf.setLength(0);
				}
				blankbuf.append(sdata);
				start = totalsize;
			}else{
				if(tokenbuf.length() != 0){
					tokens.add(new Token(tokenbuf.toString(), start,
							tokenbuf.length()));
					tokenbuf.setLength(0);
				}
				if(blankbuf.length() != 0){
					tokens.add(new Token(blankbuf.toString(), start,
							blankbuf.length()));
					blankbuf.setLength(0);
				}
				plainbuf.append(sdata);
				start = totalsize;
			}
		}
		if(tokenbuf.length() != 0)
			tokens.add(new Token(tokenbuf.toString(), start,
					tokenbuf.length()));
		else if(blankbuf.length() != 0)
			tokens.add(new Token(blankbuf.toString(), start,
					blankbuf.length()));
		else if(plainbuf.length() != 0)
			tokens.add(new Token(plainbuf.toString(), start,
					plainbuf.length()));
	}// end tokenize(Reader)
}
