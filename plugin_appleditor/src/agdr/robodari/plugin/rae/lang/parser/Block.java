package agdr.robodari.plugin.rae.lang.parser;

import agdr.library.lang.*;

import java.util.*;

public abstract class Block extends Element{
	protected Block(CodeRange range, Block parent){
		super(range, parent);
	}// end Constructor(CodeRange, ErrorBundle
	
	public boolean isRoot(){
		if(super.parent == null) return true;
		return false;
	}// end isRoot()
	public Block getRoot(){
		if(this.isRoot())
			return this;
		return parent.getRoot();
	}// end getRoot()
	
	public abstract Element[] getElements();
	public abstract int getElementCount();
	public abstract Element getElement(int idx);
	public abstract void removeElement(Element elem);
	public abstract int indexOfElement(Element elem);
	public abstract void clear();
	public abstract void addElement(Element elem);
}
