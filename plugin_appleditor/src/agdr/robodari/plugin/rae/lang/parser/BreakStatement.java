package agdr.robodari.plugin.rae.lang.parser;

import agdr.library.lang.CodeRange;

public class BreakStatement extends Statement{
	public BreakStatement(CodeRange range, Block parent){
		super(range, parent);
	}// end Constructor
	
	public String toString()
	{ return "BREAK"; }
}
