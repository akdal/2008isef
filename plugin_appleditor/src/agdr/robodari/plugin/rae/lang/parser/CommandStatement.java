package agdr.robodari.plugin.rae.lang.parser;

import agdr.robodari.plugin.rae.lang.*;

import agdr.library.lang.*;
import agdr.robodari.plugin.rae.lang.*;
import static agdr.library.lang.ErrorTypes.*;

import java.util.*;

public class CommandStatement extends Statement{
	private final String cmd;
	private final Valuable[] parameters;
	
	public CommandStatement(CodeRange range, Block parent, String cmd, Valuable... values){
		super(range, parent);
		this.cmd = cmd;
		this.parameters = values;
	}// end Constructor(int, int, String, String, int, ErrorBundle,
	//                                               ValueInspector)
	

	public int sizeOfParameters()
	{ return parameters.length; }
	public Valuable getParameter(int idx)
	{ return parameters[idx]; }
	public Valuable[] getParameters()
	{ return parameters; }
	
	

	public String getCommand()
	{ return cmd; }
	
	public String toString(){
		StringBuffer buf = new StringBuffer();
		buf.append(getCommand());
		
		if(this.sizeOfParameters() > 0){
			buf.append(" ");
			if(this.getParameter(0) != null)
				buf.append(this.getParameter(0));
		}
		for(int i = 1; i < this.sizeOfParameters(); i++){
			buf.append(", ");
			if(this.getParameter(i) != null){
				buf.append(this.getParameter(i));
			}
		}
		return buf.toString();
	}// end toString()
}
