package agdr.robodari.plugin.rae.lang.parser;

import agdr.library.lang.*;

public class Comment extends Statement{
	private String content;
	
	public Comment(CodeRange range, Block parent, String content){
		super(range, parent);
		setContent(content);
	}// end Constructor(CodeRange, ErrorBundle, String, int)
	
	

	public String content()
	{ return content; }
	public void setContent(String content){
		if(content.endsWith("\r"))
			content = content.substring(0, content.length() - 1);
		this.content = content;
	}// end setAppearance(int)
	
	
	public boolean equals(Object obj){
		if(!super.equals(obj)) return false;
		else if(!(obj instanceof Comment)) return false;
		
		Comment c = (Comment)obj;
		return c.content.equals(this.content);
	}// end equals(Object)
	
	
	public String toString(){
		StringBuffer buf = new StringBuffer("'");
		return buf.append(content).toString();
	}// end toString()
}
