package agdr.robodari.plugin.rae.lang.parser;

import agdr.library.lang.*;

public class ConstStatement extends Statement{
	private String identifier;
	private Value value;
	
	public ConstStatement(CodeRange range, Block parent, 
			String identifier, Value value){
		super(range, parent);
		setIdentifier(identifier);
		setValue(value);
	}// end Constructor(CodeRange, ErrorBundle, RBBlock. Sring, Value)
	
	public String getIdentifier()
	{ return identifier; }
	public Value getValue()
	{ return value; }

	public void setIdentifier(String str){
		this.identifier = str;
	}// end setIdentifier(String)
	public void setValue(Value val){
		this.value = val;
	}// end setValue(Value)
	
	
	public String toString(){
		StringBuffer buf = new StringBuffer("CONST ");
		
		if(identifier != null)
			buf.append(identifier);
		if(identifier != null && value != null)
			buf.append(" = ");
		if(value != null)
			buf.append(value);
		return buf.toString();
	}// end toString()
}
