package agdr.robodari.plugin.rae.lang.parser;

import agdr.library.lang.*;

public class Constant implements java.io.Serializable{
	private Value value;
	private String identifier;
	
	public Constant(Value value, String identifier){
		setValue(value);
		setIdentifier(identifier);
	}// end Constructor(ValueType, String)
	
	public String getIdentifier()
	{ return identifier; }
	public void setIdentifier(String identf)
	{ this.identifier = identf; }
	
	public ValueType getValueType(){ return value.getValueType(); }
	public Value getOriginal()
	{ return value; }
	public void setValue(Value value)
	{ this.value = value; }
	
	public String toString()
	{ return identifier; }
}
