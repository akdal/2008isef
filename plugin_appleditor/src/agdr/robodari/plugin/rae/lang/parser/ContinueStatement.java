package agdr.robodari.plugin.rae.lang.parser;

import agdr.library.lang.CodeRange;

public class ContinueStatement extends Statement{
	public ContinueStatement(CodeRange range, Block parent){
		super(range, parent);
	}// end Constructor
	
	public String toString()
	{ return "CONTINUE"; }
}
