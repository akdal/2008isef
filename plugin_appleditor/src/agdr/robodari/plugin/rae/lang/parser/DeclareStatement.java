package agdr.robodari.plugin.rae.lang.parser;

import java.util.*;

import agdr.library.lang.*;

public class DeclareStatement extends Statement{
	private Vector<Pair> decls = new Vector<Pair>();
	
	public DeclareStatement(CodeRange range, Block parent,
			String identifier, ValueType valueType){
		super(range, parent);
		addDeclaration(identifier, valueType);
	}// end Constructor
	
	
	public void addDeclaration(String identifier, ValueType vt){
		Pair p = new Pair(identifier, vt);
		decls.add(p);
	}// end addDeclaration(String, ValueType)
	public void insertDeclaration(String identifier, ValueType vt, int idx){
		Pair p = new Pair(identifier, vt);
		decls.insertElementAt(p, idx);
	}// end addDeclaration(String, ValueType)
	public void setIdentifier(String identifier, int idx){
		decls.get(idx).identf = identifier;
	}// end setIdentifier(String, int)
	public void setValueType(ValueType vt, int idx){
		decls.get(idx).vt = vt;
	}// end setValueType(ValueTYpe, int)
	public void removeDeclaration(int idx){
		decls.remove(idx);
	}// end removeDeclaration(int)
	
	public String getIdentifier(int idx){
		if(decls.size() <= idx) return null;
		return decls.get(idx).identf;
	}// end getIdentifier
	public ValueType getValueType(int idx){ 
		if(decls.size() <= idx) return null;
		return decls.get(idx).vt;
	}// end getValueType
	public Pair getPair(int idx)
	{ return decls.get(idx); }
	
	public boolean containsIdentifier(String identf){
		for(Pair eachp : decls){
			if(eachp == null) continue;
			
			String eachidf = eachp.identf;
			if(eachidf == null && identf == null)
				return true;
			else if(eachidf == null) return false;
			
			if(eachidf.equals(identf))
				return true;
		}
		return false;
	}// end contgainsIDentifer
	
	public int getCount()
	{ return decls.size(); }
	
	
	public String toString(){
		StringBuffer buf = new StringBuffer();
		buf.append("DIM ").append(decls.get(0));
		for(int i = 1; i < decls.size(); i++)
			buf.append(", ").append(decls.get(i));
		return buf.toString();
	}// end toString()
	
	
	
	public static class Pair implements java.io.Serializable{
		String identf;
		ValueType vt;
		
		public Pair(String iden, ValueType v)
		{ this.identf = iden; this.vt = v; }
		
		public String getIdentifier()
		{ return identf; }
		public ValueType getValueType()
		{ return vt; }
		
		public boolean equals(Object o){
			if(o == null) return false;
			else if(!(o instanceof Pair))
				return false;
			Pair p = (Pair)o;
			boolean check = true;
			
			if(p.vt == null)
				check &= vt == null;
			else if(this.vt == null)
				return false;
			else check &= vt.equals(this.vt);
			
			return check && p.identf.equals(this.identf);
		}// end equals
		public Pair clone()
		{ return new Pair(identf, vt); }
		
		public String toString(){
			StringBuffer buf = new StringBuffer();
			if(identf != null)
				buf.append(identf);
			buf.append(" AS ");
			if(vt != null)
				buf.append(vt.toString());
			return buf.toString();
		}// end toString()
	}// end class Pair
}
