package agdr.robodari.plugin.rae.lang.parser;

import agdr.library.lang.*;

public abstract class Element implements CodeTrackable{
	protected CodeRange range;
	protected Block parent;
	
	protected Element(CodeRange range, 
			Block parent){
		setCodeRange(range);
		setParent(parent);
	}// end Constructor(int, int, int)
	
	

	public CodeRange getCodeRange()
	{ return range; }
	public void setCodeRange(CodeRange range){
		this.range = range;
	}// end setCodeRange(int, int)
	
	
	
	public Block getParent()
	{ return parent; }
	protected void setParent(Block block){
		Block old = this.parent;
		this.parent = block;
		if(old != null)
			old.removeElement(this);
	}// end setParent(RBBlock)
}
