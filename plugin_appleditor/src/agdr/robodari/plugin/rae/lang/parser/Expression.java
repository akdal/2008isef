package agdr.robodari.plugin.rae.lang.parser;

import agdr.library.lang.*;

public class Expression implements Valuable{
	private Valuable left;
	private Valuable right;
	private Operator operator;
	private CodeRange range;
	private Object parent;
	private int position;
	
	public Expression(CodeRange range, 
			Valuable left, Valuable right, Operator op){
		setCodeRange(range);
		setLeft(left);
		setRight(right);
		setOperator(op);
	}// end Expression(Valuable, Valuable, Operator)

	

	public Object getParent()
	{ return parent; }
	public int getPosition()
	{ return position; }
	public void setParent(Object p)
	{ this.parent = p; }
	public void setPosition(int p)
	{ this.position = p; }
	
	public CodeRange getCodeRange()
	{ return range; }
	public void setCodeRange(CodeRange range){
		this.range = range;
	}// end setCodeRange(int, int)
	
	
	public Valuable getLeft()
	{ return left; }

	public void setLeft(Valuable left)
	{ this.left = left; }

	public Operator getOperator() {
		return operator;
	}// end getOperator()

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public Valuable getRight() {
		return right;
	}

	public void setRight(Valuable right) {
		this.right = right;
	}// end setRight(Valuable)
	
	
	public String toString(){
		StringBuffer buf = new StringBuffer();
		if(left != null)
			buf.append("(").append(left).append(") ");
		
		buf.append(operator);
		if(right != null)
			buf.append(" (").append(right).append(")");
		
		return buf.toString();
	}// end toString()
}
