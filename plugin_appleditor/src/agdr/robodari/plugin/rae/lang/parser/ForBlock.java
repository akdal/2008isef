package agdr.robodari.plugin.rae.lang.parser;

import java.util.Vector;

import agdr.library.lang.*;

public class ForBlock extends Block{
	private final static Statement[] NULL_ARRAY = new Statement[]{};
	
	private String targetIdtf;
	private Valuable start, end;
	private Vector<Element> elements = new Vector<Element>();
	
	public ForBlock(CodeRange range, Block parent, String
			target, Valuable start, Valuable end){
		super(range, parent);
		this.start = start;
		this.targetIdtf = target;
		this.end = end;
	}// end Constructor
	
	
	public String getTarget()
	{ return targetIdtf; }
	public Valuable getStart()
	{ return start; }
	public Valuable getEnd()
	{ return end; }

	public Element getElement(int idx)
	{ return elements.get(idx); }
	public Element[] getElements()
	{ return elements.toArray(NULL_ARRAY); }
	public int getElementCount()
	{ return elements.size(); }
	public boolean containsElement(Element cs)
	{ return elements.contains(cs); }
	
	public void clear()
	{ elements.clear(); }
	
	public int indexOfElement(Element elem)
	{ return elements.indexOf(elem); }
	public void addElement(Element elem){
		elements.add(elem);
	}// end addNode(RBElement)
	public void removeElement(Element elem)
	{ elements.remove(elem); }
	
	
	
	public String toString(){
		StringBuffer buf = new StringBuffer();
		buf.append("FOR ");
		if(this.targetIdtf != null)
			buf.append(targetIdtf);
		buf.append(" = ");
		if(this.start != null)
			buf.append(start);
		buf.append(" TO ");
		if(this.end != null)
			buf.append(end);
		
		buf.append("\n");
		for(int i = 0; i < this.getElementCount(); i++)
			buf.append(getElement(i)).append("\n");
		buf.append("NEXT ").append(this.targetIdtf == null ? "" : this.targetIdtf);
		return buf.toString();
	}// end toString()
}
