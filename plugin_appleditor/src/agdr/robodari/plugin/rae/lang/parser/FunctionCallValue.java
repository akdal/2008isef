package agdr.robodari.plugin.rae.lang.parser;


import java.util.Collections;
import java.util.Vector;

import agdr.library.lang.*;


public class FunctionCallValue implements Valuable{
	protected CodeRange range;

	private String function;
	private Valuable[] parameters;

	private Object parent;
	private int position;
	
	
	public FunctionCallValue(CodeRange range, String func, Valuable... args){
		this.setCodeRange(range);
		this.setFunction(func);
		this.setParameters(args);
	}// end Constructor
	

	
	public Object getParent()
	{ return parent; }
	public int getPosition()
	{ return position; }
	public void setParent(Object p)
	{ this.parent = p; }
	public void setPosition(int p)
	{ this.position = p; }
	
	public String getFunction()
	{ return function; }
	public void setFunction(String f)
	{ this.function = f; }
	
	public CodeRange getCodeRange()
	{ return range; }
	public void setCodeRange(CodeRange range)
	{ this.range = range; }
	
	
	

	
	
	public int sizeOfParameters()
	{ return parameters.length; }
	public Valuable getParameter(int idx)
	{ return parameters[idx]; }
	public void setParameter(int idx, Valuable v){
		parameters[idx] = v;
	}// end setParameter(int, Valuable)
	public void setParameters(Valuable... v){
		parameters = v;
	}// end setParameter(int, Valuable)
	public Valuable[] getParameters()
	{ return parameters; }
	

	
	public String toString(){
		StringBuffer buf = new StringBuffer(function);
		buf.append("(");
		
		if(this.sizeOfParameters() > 0 && this.getParameter(0) != null)
			buf.append(this.getParameter(0));
		
		for(int i = 1; i < this.sizeOfParameters(); i++){
			buf.append(", ");
			if(this.getParameter(i) != null)
				buf.append(this.getParameter(i));
		}
		return buf.append(")").toString();
	}// end toString()
}
