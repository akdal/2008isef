package agdr.robodari.plugin.rae.lang.parser;

import agdr.library.lang.*;
//import org.agdr.robodari.plugin.miniROBOT.lang.struct.RBDeclareStatement.Pair;

import java.util.*;

public class IfBlock extends Block{
	private Pair ifPair = new Pair(null, null);
	private Pair elsePair = new Pair(null, null);
	private Vector<Pair> blocks = new Vector<Pair>();
	private boolean simplified = false;
	
	/**
	 * if 블록을 초기화 합니다.<br>
	 * @param range
	 * @param eb
	 * @param parent
	 * @param components
	 */
	public IfBlock(CodeRange range, Block parent){
		super(range, parent);
	}// end RBIfBlock(CodeRange, ErrorBundle, RBBlock)
	
	
	
	public boolean containsElement(Element cs){
		for(Pair p : blocks){
			if(p.block.equals(cs))
				return true;
		}
		return false;
	}// end containsCSNode(CodeStructure)
	public Block getElement(int idx)
	{ return blocks.get(idx).block; }
	public PlainBlock[] getElements(){
		PlainBlock[] arrayBlocks = new PlainBlock[blocks.size()];
		for(int i = 0; i < blocks.size(); i++)
			arrayBlocks[i] = blocks.get(i).block;
		return arrayBlocks;
	}// end getCSNodes()
	public int getElementCount()
	{ return blocks.size(); }
	/**
	 * 주어진 블록이 몇 번째 위치하는지를 반환합니다.<br>
	 * if문은 0, 첫번재 elseif문은 1, ..., else문은 blocks.length - 1번째가 됩니다.<br>
	 * - 주의 : getElseIfBlock(int)등의 메소드에서 사용하는 인덱스보다
	 * 1이 큽니다.<br> 
	 * @param blck
	 * @return
	 */
	public int indexOfElement(Element blck){
		for(int i = 0; i < blocks.size(); i++)
			if(blocks.get(i).block.equals(blck))
				return i;
		return 0;
	}// end indexOfElement(RBBlock)
	/**
	 * 비활성화 되어 있는 메소드입니다.
	 */
	public void addElement(Element elem){
	}// end addElement(RBElement)
	public void removeElement(Element elem){
		for(int i = 0; i < blocks.size(); i++)
			if(blocks.get(i).block.equals(elem)){
				blocks.remove(i);
				break;
			}
	}// end removeElement(RBElement)
	
	/**
	 * if문 내에 있는 걸 모두 지웁니다.<br>
	 * 단, if 블록은 안에 있는 내용만 사라지고 사라지지 않습니다.
	 */
	public void clear(){
		blocks.clear();
		elsePair.cond = null;
		elsePair.block = null;
	}// end clear()
	
	
	
	public boolean isSimplified()
	{ return simplified; }
	public void setSimplified(boolean smpl){
		if(smpl == simplified) return;
		
		this.simplified = smpl;
	}// end setSimplified
	
	/**
	 * elseif에서 idx번째 조건을 반환합니다.
	 * @param idx
	 * @return
	 */
	public Valuable getElseifCondition(int idx)
	{ return blocks.get(idx).cond; }
	public void setElseifCondition(Valuable val, int idx){
		blocks.get(idx).cond = val;
	}// end setCondition(Valuable)
	

	/**
	 * if문의 조건을 반환합니다.
	 * @param idx
	 * @return
	 */
	public Valuable getIfCondition()
	{ return ifPair.cond; }
	public void setIfCondition(Valuable val){
		this.ifPair.cond = val;
	}// end setCondition(Valuable)
	
	
	public PlainBlock getIfBlock()
	{ return ifPair.block; }
	public PlainBlock getElseBlock()
	{ return elsePair.block; }
	public PlainBlock getElseifBlock(int idx)
	{ return blocks.get(idx).block; }
	public boolean hasElseBlock(){
		return elsePair.block != null;
	}// end hasElseBlock

	public void setIfBlock(PlainBlock block){
		this.ifPair.block = block;
	}// end setIfBlock(RBPlainBlock)

	public void setElseBlock(PlainBlock block){
		this.elsePair.block = block;
	}// end setElseBlock(RBPlainBlock)

	public void setElseifBlock(PlainBlock block, int idx){
		blocks.get(idx).block = block;
	}// end setElseifBlock(RBPlainBlock)
	public void addElseifBlock(PlainBlock block, Valuable condition){
		Pair newp = new Pair(block, condition);
		blocks.add(newp);
	}// end addElseifBlock(RBPlainBlock, Valuable)
	
	public void removeElseifBlock(int idx){
		blocks.remove(idx);
	}// end removeElseifBlock(RBPlainBlock, Valuable)
	public int getElseifBlocksCount()
	{ return blocks.size(); }
	
	
	public String toString(){
		StringBuffer buf = new StringBuffer("IF ");
		buf.append(this.getIfCondition());
		buf.append(" THEN");
		
		if(this.isSimplified()){
			if(this.getElseifBlocksCount() == 0 && !this.hasElseBlock()){
				buf.append(" ");
				String blockcon = this.getIfBlock().toString();
				blockcon = blockcon.replace('\n', ' ');
				
				buf.append(blockcon);
				return buf.toString();
			}
		}
		buf.append("\n");
		buf.append(this.getIfBlock());
		
		int range = this.getElseifBlocksCount();
		if(!this.hasElseBlock() && this.isSimplified())
			range--;
		for(int i = 0; i < range; i++)
			buf.append("ELSEIF ").append(this.getElseifCondition(i)).append(" THEN\n").
			append(this.getElseifBlock(i));
		if(range == this.getElseifBlocksCount() - 1)
			buf.append("ELSEIF ").append(this.getElseifCondition(range)).append(" THEN ").
			append(this.getElseifBlock(range).toString().replace('\n', ' '));

		if(this.hasElseBlock()){
			buf.append("ELSE\n").append(this.getElseBlock());
			buf.append("ENDIF");
		}else if(!this.isSimplified())
			buf.append("ENDIF");
		return buf.toString();
	}// end toString(
	
	
	
	

	public static class Pair implements java.io.Serializable{
		PlainBlock block;
		Valuable cond;
		
		public Pair(PlainBlock block, Valuable cond)
		{ this.block = block; this.cond = cond; }
		
		public PlainBlock getBlock()
		{ return block; }
		public Valuable getCondition()
		{ return cond; }
		
		public boolean equals(Object o){
			if(o == null) return false;
			else if(!(o instanceof Pair))
				return false;
			Pair p = (Pair)o;
			boolean check = true;
			
			if(p.cond == null)
				check &= cond == null;
			else if(this.cond == null)
				return false;
			else check &= cond.equals(this.cond);
			
			return check && p.block.equals(this.block);
		}// end equals
		public Pair clone()
		{ return new Pair(block, cond); }
	}// end class Pair
}