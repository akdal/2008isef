package agdr.robodari.plugin.rae.lang.parser;

import agdr.library.lang.*;

public class InsertStatement extends Statement{
	public final String ERRCNT_TYPENOTMATCH = "Type does not match.";
	
	private Valuable right;
	private String identifier;
	
	public InsertStatement(CodeRange range, Block parent, String left, Valuable right){
		super(range, parent);
		setLeft(left);
		setRight(right);
	}// end Constructor(Variable, Valuable)
	
	
	
	public String getLeft()
	{ return identifier; }
	
	public Valuable getRight(){
		return right;
	}// end getRight()
	
	public void setLeft(String identifier){
		this.identifier = identifier;
	}// end setLeft
	public void setRight(Valuable val){
		this.right = val;
	}// end setRight(Valuable)
	
	public String toString(){
		StringBuffer buf = new StringBuffer();
		buf.append(identifier).append(" = ");
		if(right != null)
			buf.append(right);
		return buf.toString();
	}// end toString()
}
