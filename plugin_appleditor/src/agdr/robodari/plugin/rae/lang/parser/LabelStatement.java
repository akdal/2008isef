package agdr.robodari.plugin.rae.lang.parser;

import agdr.library.lang.*;

public class LabelStatement extends Statement{
	private String labelname;
	//private Label lobj = null;
	
	public LabelStatement(CodeRange range, String label, Block parent){
		super(range, parent);
		setLabelName(label);
	}// end Constructor(CodeRange, String)
	
	public String getLabelName()
	{ return labelname; }
	
	public void setLabelName(String label){
		this.labelname = label;
	}// end setLabel(String)
	
	public String toString(){
		return labelname + ": ";
	}// end toString()
}
