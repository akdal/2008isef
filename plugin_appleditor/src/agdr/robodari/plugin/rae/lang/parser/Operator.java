package agdr.robodari.plugin.rae.lang.parser;


import static agdr.library.lang.ErrorTypes.*;
import static agdr.robodari.plugin.rae.lang.parser.ValueType.*;

import agdr.library.lang.*;
import agdr.robodari.plugin.rae.lang.*;

public abstract class Operator implements java.io.Serializable{
	public final static String ERRCNT_LEFTVAL_WRONGTYPE = "Left value has incompatible type.";
	public final static String ERRCNT_RIGHTVAL_WRONGTYPE = "Right value has incompatible type.";
	public final static String ERRCNT_LEFTVAL_NULL = "Left value is empty.";
	public final static String ERRCNT_RIGHTVAL_NULL = "Right value is empty.";
	public final static String ERRCNT_DIVIDED_BY_ZERO = "Divide by zero.";
	
	
	
	
	
	
	final static ResultType checkLeftNotNull(CodeTrackable source, Valuable left, ErrorBundle bundle){
		if(left == null){
			bundle.addErrorMessage(new ErrorMessage(source,
					null,
					source.getCodeRange(),
					ERRCNT_LEFTVAL_NULL,
					RUNTIME_ERROR));
			return ResultType.RUNTIME_ERROR;
		}
		return ResultType.DONE;
	}// end checkLeftNull
	final static ResultType checkRightNotNull(CodeTrackable source, Valuable right, ErrorBundle bundle){
		if(right == null){
			bundle.addErrorMessage(new ErrorMessage(source,
					null,
					source.getCodeRange(),
					ERRCNT_RIGHTVAL_NULL,
					RUNTIME_ERROR));
			return ResultType.RUNTIME_ERROR;
		}
		return ResultType.DONE;
	}// end checkLeftNull
	
	
	
	
	public final static Operator PLUS = new Operator(){
		public ResultType calc(CodeTrackable source, Valuable left, Valuable right, Receptor receptor, BASICRunner vt){
			ResultType res;

			res = checkLeftNotNull(source, left, receptor.bundle);
			if(res != ResultType.DONE) return res;
			res = checkRightNotNull(source, right, receptor.bundle);
			if(res != ResultType.DONE) return res;
			
			Receptor lrecp = new Receptor();
			lrecp.bundle = receptor.bundle;
			Receptor rrecp = new Receptor();
			rrecp.bundle = receptor.bundle;
			
			res = vt.getValue(source, left, lrecp);
			if(res != ResultType.DONE)
				return res;
			res = vt.getValue(source, right, rrecp);
			if(res != ResultType.DONE)
				return res;
			
			if(lrecp.type == ValueType.TYPE_STRING || rrecp.type == ValueType.TYPE_STRING){
				receptor.value = (String)(String.valueOf(lrecp.value) + String.valueOf(rrecp.value));
				receptor.type = ValueType.TYPE_STRING;
			}else if(lrecp.type == ValueType.TYPE_INT && rrecp.type == ValueType.TYPE_INT){
				int lval = ((Number)lrecp.value).intValue();
				int rval = ((Number)rrecp.value).intValue();
				receptor.value = (Integer)(lval + rval);
				receptor.type = ValueType.TYPE_INT;
			}else{
				if(lrecp.type == ValueType.TYPE_BOOLEAN || lrecp.type == ValueType.TYPE_UNKNOWN){
					receptor.bundle.addErrorMessage(new ErrorMessage(source,
							left,
							left.getCodeRange(),
							ERRCNT_RIGHTVAL_WRONGTYPE,
							RUNTIME_ERROR));
					return ResultType.RUNTIME_ERROR;
				}else{
					receptor.bundle.addErrorMessage(new ErrorMessage(source,
							right,
							right.getCodeRange(),
							ERRCNT_LEFTVAL_WRONGTYPE,
							RUNTIME_ERROR));
					return ResultType.RUNTIME_ERROR;
				}
			}
			
			return ResultType.DONE;
		}// end calc(Object, Valuable, Valuable)
	};
	
	
	
	
	
	public final static Operator MINUS = new Operator(){
		public ResultType calc(CodeTrackable source, Valuable left, Valuable right, Receptor receptor, BASICRunner vt){
			ResultType res;

			res = checkLeftNotNull(source, left, receptor.bundle);
			if(res != ResultType.DONE) return res;
			res = checkRightNotNull(source, right, receptor.bundle);
			if(res != ResultType.DONE) return res;
			
			Receptor lrecp = new Receptor();
			lrecp.bundle = receptor.bundle;
			Receptor rrecp = new Receptor();
			rrecp.bundle = receptor.bundle;
			
			res = vt.getValue(source, left, lrecp);
			if(res != ResultType.DONE)
				return res;
			res = vt.getValue(source, right, rrecp);
			if(res != ResultType.DONE)
				return res;
			
			if(lrecp.type != ValueType.TYPE_INT){
				receptor.bundle.addErrorMessage(new ErrorMessage(source,
						left,
						left.getCodeRange(),
						ERRCNT_RIGHTVAL_WRONGTYPE,
						RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}else if(rrecp.type != ValueType.TYPE_INT){
				receptor.bundle.addErrorMessage(new ErrorMessage(source,
						right,
						right.getCodeRange(),
						ERRCNT_LEFTVAL_WRONGTYPE,
						RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			
			int lval = ((Number)lrecp.value).intValue();
			int rval = ((Number)rrecp.value).intValue();
			receptor.value = (Integer)(lval - rval);
			receptor.type = ValueType.TYPE_INT;
			return ResultType.DONE;
		}// end calc(Object, Valuable, Valuable)
	};
	
	
	
	
	
	public final static Operator DIVISION = new Operator(){
		public ResultType calc(CodeTrackable source, Valuable left, Valuable right, Receptor receptor, BASICRunner vt){
			ResultType res;

			res = checkLeftNotNull(source, left, receptor.bundle);
			if(res != ResultType.DONE) return res;
			res = checkRightNotNull(source, right, receptor.bundle);
			if(res != ResultType.DONE) return res;
			
			Receptor lrecp = new Receptor();
			lrecp.bundle = receptor.bundle;
			Receptor rrecp = new Receptor();
			rrecp.bundle = receptor.bundle;
			
			res = vt.getValue(source, left, lrecp);
			if(res != ResultType.DONE)
				return res;
			res = vt.getValue(source, right, rrecp);
			if(res != ResultType.DONE)
				return res;
			
			if(lrecp.type != ValueType.TYPE_INT){
				receptor.bundle.addErrorMessage(new ErrorMessage(source,
						left,
						left.getCodeRange(),
						ERRCNT_RIGHTVAL_WRONGTYPE,
						RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}else if(rrecp.type != ValueType.TYPE_INT){
				receptor.bundle.addErrorMessage(new ErrorMessage(source,
						right,
						right.getCodeRange(),
						ERRCNT_LEFTVAL_WRONGTYPE,
						RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			
			int lval = ((Number)lrecp.value).intValue();
			int rval = ((Number)rrecp.value).intValue();
			
			if(rval == 0){
				receptor.bundle.addErrorMessage(new ErrorMessage(source,
						right,
						right.getCodeRange(),
						ERRCNT_DIVIDED_BY_ZERO,
						RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			
			receptor.value = (Integer)(lval / rval);
			receptor.type = ValueType.TYPE_INT;
			return ResultType.DONE;
		}// end calc(Object, Valuable, Valuable)
	};
	
	
	
	
	
	public final static Operator MULTIPLY = new Operator(){
		public ResultType calc(CodeTrackable source, Valuable left, Valuable right, Receptor receptor, BASICRunner vt){
			ResultType res;

			res = checkLeftNotNull(source, left, receptor.bundle);
			if(res != ResultType.DONE) return res;
			res = checkRightNotNull(source, right, receptor.bundle);
			if(res != ResultType.DONE) return res;
			
			Receptor lrecp = new Receptor();
			lrecp.bundle = receptor.bundle;
			Receptor rrecp = new Receptor();
			rrecp.bundle = receptor.bundle;
			
			res = vt.getValue(source, left, lrecp);
			if(res != ResultType.DONE)
				return res;
			res = vt.getValue(source, right, rrecp);
			if(res != ResultType.DONE)
				return res;
			
			if(lrecp.type != ValueType.TYPE_INT){
				receptor.bundle.addErrorMessage(new ErrorMessage(source,
						left,
						left.getCodeRange(),
						ERRCNT_RIGHTVAL_WRONGTYPE,
						RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}else if(rrecp.type != ValueType.TYPE_INT){
				receptor.bundle.addErrorMessage(new ErrorMessage(source,
						right,
						right.getCodeRange(),
						ERRCNT_LEFTVAL_WRONGTYPE,
						RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			
			int lval = ((Number)lrecp.value).intValue();
			int rval = ((Number)rrecp.value).intValue();
			receptor.value = (Integer)(lval * rval);
			receptor.type = ValueType.TYPE_INT;
			return ResultType.DONE;
		}// end calc(Object, Valuable, Valuable)
	};
	
	
	
	
	
	public final static Operator MOD = new Operator(){
		public ResultType calc(CodeTrackable source, Valuable left, Valuable right, Receptor receptor, BASICRunner vt){
			ResultType res;

			res = checkLeftNotNull(source, left, receptor.bundle);
			if(res != ResultType.DONE) return res;
			res = checkRightNotNull(source, right, receptor.bundle);
			if(res != ResultType.DONE) return res;
			
			Receptor lrecp = new Receptor();
			lrecp.bundle = receptor.bundle;
			Receptor rrecp = new Receptor();
			rrecp.bundle = receptor.bundle;
			
			res = vt.getValue(source, left, lrecp);
			if(res != ResultType.DONE)
				return res;
			res = vt.getValue(source, right, rrecp);
			if(res != ResultType.DONE)
				return res;
			
			if(lrecp.type != ValueType.TYPE_INT){
				receptor.bundle.addErrorMessage(new ErrorMessage(source,
						left,
						left.getCodeRange(),
						ERRCNT_RIGHTVAL_WRONGTYPE,
						RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}else if(rrecp.type != ValueType.TYPE_INT){
				receptor.bundle.addErrorMessage(new ErrorMessage(source,
						right,
						right.getCodeRange(),
						ERRCNT_LEFTVAL_WRONGTYPE,
						RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			
			int lval = ((Number)lrecp.value).intValue();
			int rval = ((Number)rrecp.value).intValue();
			
			if(rval == 0){
				receptor.bundle.addErrorMessage(new ErrorMessage(source,
						right,
						right.getCodeRange(),
						ERRCNT_DIVIDED_BY_ZERO,
						RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			
			receptor.value = (Integer)(lval % rval);
			receptor.type = ValueType.TYPE_INT;
			return ResultType.DONE;
		}// end calc(Object, Valuable, Valuable)
	};
	
	
	
	public final static Operator EQUAL = new Operator(){
		public ResultType calc(CodeTrackable source, Valuable left, Valuable right, Receptor receptor, BASICRunner dbg){
			ResultType res;

			res = checkLeftNotNull(source, left, receptor.bundle);
			if(res != ResultType.DONE) return res;
			res = checkRightNotNull(source, right, receptor.bundle);
			if(res != ResultType.DONE) return res;

			Receptor lrecp = new Receptor();
			lrecp.bundle = receptor.bundle;
			Receptor rrecp = new Receptor();
			rrecp.bundle = receptor.bundle;
			
			res = dbg.getValue(source, left, lrecp);
			if(res != ResultType.DONE)
				return res;
			res = dbg.getValue(source, right, rrecp);
			if(res != ResultType.DONE)
				return res;

			boolean cond1 = lrecp.type == ValueType.TYPE_BOOLEAN && rrecp.type == ValueType.TYPE_BOOLEAN;
			boolean cond2 = lrecp.type == ValueType.TYPE_STRING && rrecp.type == ValueType.TYPE_STRING;
			boolean cond3 = lrecp.type == ValueType.TYPE_INT && rrecp.type == ValueType.TYPE_INT;
			if(!(cond1 || cond2 || cond3)){
				receptor.bundle.addErrorMessage(new ErrorMessage(source,
						null,
						source.getCodeRange(),
						ERRCNT_LEFTVAL_WRONGTYPE,
						RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			
			receptor.type = ValueType.TYPE_BOOLEAN;
			if(lrecp.value != null)
				receptor.value = new Boolean(lrecp.value.equals(rrecp.value));
			else
				receptor.value = new Boolean(rrecp.value == null);
			return ResultType.DONE;
		}// end hasError(Object, Object, Object)
	};
	
	
	
	
	
	
	public final static Operator LESS = new Operator(){
		public ResultType calc(CodeTrackable source, Valuable left, Valuable right, Receptor receptor, BASICRunner vt){
			ResultType res;

			res = checkLeftNotNull(source, left, receptor.bundle);
			if(res != ResultType.DONE) return res;
			res = checkRightNotNull(source, right, receptor.bundle);
			if(res != ResultType.DONE) return res;
			
			Receptor lrecp = new Receptor();
			lrecp.bundle = receptor.bundle;
			Receptor rrecp = new Receptor();
			rrecp.bundle = receptor.bundle;
			
			res = vt.getValue(source, left, lrecp);
			if(res != ResultType.DONE)
				return res;
			res = vt.getValue(source, right, rrecp);
			if(res != ResultType.DONE)
				return res;
			
			if(lrecp.type != ValueType.TYPE_INT){
				receptor.bundle.addErrorMessage(new ErrorMessage(source,
						left,
						left.getCodeRange(),
						ERRCNT_RIGHTVAL_WRONGTYPE,
						RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}else if(rrecp.type != ValueType.TYPE_INT){
				receptor.bundle.addErrorMessage(new ErrorMessage(source,
						right,
						right.getCodeRange(),
						ERRCNT_LEFTVAL_WRONGTYPE,
						RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			
			int lval = ((Number)lrecp.value).intValue();
			int rval = ((Number)rrecp.value).intValue();
			
			receptor.value = (Boolean)(lval < rval);
			receptor.type = ValueType.TYPE_BOOLEAN;
			return ResultType.DONE;
		}// end calc(Object, Valuable, Valuable)
	};
	
	
	
	
	
	
	public final static Operator GREATER = new Operator(){
		public ResultType calc(CodeTrackable source, Valuable left, Valuable right, Receptor receptor, BASICRunner vt){
			ResultType res;

			res = checkLeftNotNull(source, left, receptor.bundle);
			if(res != ResultType.DONE) return res;
			res = checkRightNotNull(source, right, receptor.bundle);
			if(res != ResultType.DONE) return res;
			
			Receptor lrecp = new Receptor();
			lrecp.bundle = receptor.bundle;
			Receptor rrecp = new Receptor();
			rrecp.bundle = receptor.bundle;
			
			res = vt.getValue(source, left, lrecp);
			if(res != ResultType.DONE)
				return res;
			res = vt.getValue(source, right, rrecp);
			if(res != ResultType.DONE)
				return res;
			
			if(lrecp.type != ValueType.TYPE_INT){
				receptor.bundle.addErrorMessage(new ErrorMessage(source,
						left,
						left.getCodeRange(),
						ERRCNT_RIGHTVAL_WRONGTYPE,
						RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}else if(rrecp.type != ValueType.TYPE_INT){
				receptor.bundle.addErrorMessage(new ErrorMessage(source,
						right,
						right.getCodeRange(),
						ERRCNT_LEFTVAL_WRONGTYPE,
						RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			
			int lval = ((Number)lrecp.value).intValue();
			int rval = ((Number)rrecp.value).intValue();
			
			receptor.value = (Boolean)(lval > rval);
			receptor.type = ValueType.TYPE_BOOLEAN;
			return ResultType.DONE;
		}// end calc(Object, Valuable, Valuable)
	};
	
	
	
	
	
	
	
	public final static Operator NOTSAME = new Operator(){
		public ResultType calc(CodeTrackable source, Valuable left, Valuable right, Receptor receptor, BASICRunner dbg){
			ResultType res;

			res = checkLeftNotNull(source, left, receptor.bundle);
			if(res != ResultType.DONE) return res;
			res = checkRightNotNull(source, right, receptor.bundle);
			if(res != ResultType.DONE) return res;

			Receptor lrecp = new Receptor();
			lrecp.bundle = receptor.bundle;
			Receptor rrecp = new Receptor();
			rrecp.bundle = receptor.bundle;
			
			res = dbg.getValue(source, left, lrecp);
			if(res != ResultType.DONE)
				return res;
			res = dbg.getValue(source, right, rrecp);
			if(res != ResultType.DONE)
				return res;

			boolean cond1 = lrecp.type == ValueType.TYPE_BOOLEAN && rrecp.type == ValueType.TYPE_BOOLEAN;
			boolean cond2 = lrecp.type == ValueType.TYPE_STRING && rrecp.type == ValueType.TYPE_STRING;
			boolean cond3 = (lrecp.type == ValueType.TYPE_INT && rrecp.type == ValueType.TYPE_INT);
			if(!(cond1 || cond2 || cond3)){
				receptor.bundle.addErrorMessage(new ErrorMessage(source,
						null,
						source.getCodeRange(),
						ERRCNT_LEFTVAL_WRONGTYPE,
						RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			
			receptor.type = ValueType.TYPE_BOOLEAN;
			if(lrecp.value != null)
				receptor.value = new Boolean(!lrecp.value.equals(rrecp.value));
			else
				receptor.value = new Boolean(rrecp.value != null);
			return ResultType.DONE;
		}// end hasError(Object, Object, Object)
	};
	
	
	
	
	
	
	public final static Operator LESSOREQUAL = new Operator(){
		public ResultType calc(CodeTrackable source, Valuable left, Valuable right, Receptor receptor, BASICRunner vt){
			ResultType res;

			res = checkLeftNotNull(source, left, receptor.bundle);
			if(res != ResultType.DONE) return res;
			res = checkRightNotNull(source, right, receptor.bundle);
			if(res != ResultType.DONE) return res;
			
			Receptor lrecp = new Receptor();
			lrecp.bundle = receptor.bundle;
			Receptor rrecp = new Receptor();
			rrecp.bundle = receptor.bundle;
			
			res = vt.getValue(source, left, lrecp);
			if(res != ResultType.DONE)
				return res;
			res = vt.getValue(source, right, rrecp);
			if(res != ResultType.DONE)
				return res;
			
			if(lrecp.type != ValueType.TYPE_INT){
				receptor.bundle.addErrorMessage(new ErrorMessage(source,
						left,
						left.getCodeRange(),
						ERRCNT_RIGHTVAL_WRONGTYPE,
						RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}else if(rrecp.type != ValueType.TYPE_INT){
				receptor.bundle.addErrorMessage(new ErrorMessage(source,
						right,
						right.getCodeRange(),
						ERRCNT_LEFTVAL_WRONGTYPE,
						RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			
			int lval = ((Number)lrecp.value).intValue();
			int rval = ((Number)rrecp.value).intValue();
			
			receptor.value = (Boolean)(lval <= rval);
			receptor.type = ValueType.TYPE_BOOLEAN;
			return ResultType.DONE;
		}// end calc(Object, Valuable, Valuable)
	};
	
	
	
	
	
	
	
	public final static Operator GREATEROREQUAL = new Operator(){
		public ResultType calc(CodeTrackable source, Valuable left, Valuable right, Receptor receptor, BASICRunner vt){
			ResultType res;

			res = checkLeftNotNull(source, left, receptor.bundle);
			if(res != ResultType.DONE) return res;
			res = checkRightNotNull(source, right, receptor.bundle);
			if(res != ResultType.DONE) return res;
			
			Receptor lrecp = new Receptor();
			lrecp.bundle = receptor.bundle;
			Receptor rrecp = new Receptor();
			rrecp.bundle = receptor.bundle;
			
			res = vt.getValue(source, left, lrecp);
			if(res != ResultType.DONE)
				return res;
			res = vt.getValue(source, right, rrecp);
			if(res != ResultType.DONE)
				return res;
			
			if(lrecp.type != ValueType.TYPE_INT){
				receptor.bundle.addErrorMessage(new ErrorMessage(source,
						left,
						left.getCodeRange(),
						ERRCNT_RIGHTVAL_WRONGTYPE,
						RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}else if(rrecp.type != ValueType.TYPE_INT){
				receptor.bundle.addErrorMessage(new ErrorMessage(source,
						right,
						right.getCodeRange(),
						ERRCNT_LEFTVAL_WRONGTYPE,
						RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			
			int lval = ((Number)lrecp.value).intValue();
			int rval = ((Number)rrecp.value).intValue();
			
			receptor.value = (Boolean)(lval >= rval);
			receptor.type = ValueType.TYPE_BOOLEAN;
			return ResultType.DONE;
		}// end calc(Object, Valuable, Valuable)
	};
	
	
	
	
	public final static Operator BITLEFTSHIFT = new Operator(){
		public ResultType calc(CodeTrackable source, Valuable left, Valuable right, Receptor receptor, BASICRunner vt){
			ResultType res;

			res = checkLeftNotNull(source, left, receptor.bundle);
			if(res != ResultType.DONE) return res;
			res = checkRightNotNull(source, right, receptor.bundle);
			if(res != ResultType.DONE) return res;
			
			Receptor lrecp = new Receptor();
			lrecp.bundle = receptor.bundle;
			Receptor rrecp = new Receptor();
			rrecp.bundle = receptor.bundle;
			
			res = vt.getValue(source, left, lrecp);
			if(res != ResultType.DONE)
				return res;
			res = vt.getValue(source, right, rrecp);
			if(res != ResultType.DONE)
				return res;
			
			if(lrecp.type != ValueType.TYPE_INT){
				receptor.bundle.addErrorMessage(new ErrorMessage(source,
						left,
						left.getCodeRange(),
						ERRCNT_RIGHTVAL_WRONGTYPE,
						RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}else if(rrecp.type != ValueType.TYPE_INT){
				receptor.bundle.addErrorMessage(new ErrorMessage(source,
						right,
						right.getCodeRange(),
						ERRCNT_LEFTVAL_WRONGTYPE,
						RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			
			int lval = ((Number)lrecp.value).intValue();
			int rval = ((Number)rrecp.value).intValue();
			
			if(rval == 0){
				receptor.bundle.addErrorMessage(new ErrorMessage(source,
						right,
						right.getCodeRange(),
						ERRCNT_DIVIDED_BY_ZERO,
						RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			
			receptor.value = (Integer)(lval << rval);
			receptor.type = ValueType.TYPE_INT;
			return ResultType.DONE;
		}// end calc(Object, Valuable, Valuable)
	};
	
	
	
	
	
	public final static Operator BITRIGHTSHIFT = new Operator(){
		public ResultType calc(CodeTrackable source, Valuable left, Valuable right, Receptor receptor, BASICRunner vt){
			ResultType res;

			res = checkLeftNotNull(source, left, receptor.bundle);
			if(res != ResultType.DONE) return res;
			res = checkRightNotNull(source, right, receptor.bundle);
			if(res != ResultType.DONE) return res;
			
			Receptor lrecp = new Receptor();
			lrecp.bundle = receptor.bundle;
			Receptor rrecp = new Receptor();
			rrecp.bundle = receptor.bundle;
			
			res = vt.getValue(source, left, lrecp);
			if(res != ResultType.DONE)
				return res;
			res = vt.getValue(source, right, rrecp);
			if(res != ResultType.DONE)
				return res;
			
			if(lrecp.type != ValueType.TYPE_INT){
				receptor.bundle.addErrorMessage(new ErrorMessage(source,
						left,
						left.getCodeRange(),
						ERRCNT_RIGHTVAL_WRONGTYPE,
						RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}else if(rrecp.type != ValueType.TYPE_INT){
				receptor.bundle.addErrorMessage(new ErrorMessage(source,
						right,
						right.getCodeRange(),
						ERRCNT_LEFTVAL_WRONGTYPE,
						RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			
			int lval = ((Number)lrecp.value).intValue();
			int rval = ((Number)rrecp.value).intValue();
			
			if(rval == 0){
				receptor.bundle.addErrorMessage(new ErrorMessage(source,
						right,
						right.getCodeRange(),
						ERRCNT_DIVIDED_BY_ZERO,
						RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			
			receptor.value = (Integer)(lval >> rval);
			receptor.type = ValueType.TYPE_INT;
			return ResultType.DONE;
		}// end calc(Object, Valuable, Valuable)
	};
	
	
	
	
	public final static Operator AND = new Operator(){
		public ResultType calc(CodeTrackable source, Valuable left, Valuable right, Receptor receptor, BASICRunner vt){
			ResultType res;

			res = checkLeftNotNull(source, left, receptor.bundle);
			if(res != ResultType.DONE) return res;
			res = checkRightNotNull(source, right, receptor.bundle);
			if(res != ResultType.DONE) return res;
			
			Receptor lrecp = new Receptor();
			lrecp.bundle = receptor.bundle;
			Receptor rrecp = new Receptor();
			rrecp.bundle = receptor.bundle;
			
			res = vt.getValue(source, left, lrecp);
			if(res != ResultType.DONE)
				return res;
			res = vt.getValue(source, right, rrecp);
			if(res != ResultType.DONE)
				return res;
			
			if(lrecp.type != ValueType.TYPE_BOOLEAN){
				receptor.bundle.addErrorMessage(new ErrorMessage(source,
						left,
						left.getCodeRange(),
						ERRCNT_RIGHTVAL_WRONGTYPE,
						RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}else if(rrecp.type != ValueType.TYPE_BOOLEAN){
				receptor.bundle.addErrorMessage(new ErrorMessage(source,
						right,
						right.getCodeRange(),
						ERRCNT_LEFTVAL_WRONGTYPE,
						RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			
			boolean lval = (Boolean)lrecp.value;
			boolean rval = (Boolean)rrecp.value;
			
			receptor.value = (Boolean)(lval && rval);
			receptor.type = ValueType.TYPE_BOOLEAN;
			return ResultType.DONE;
		}// end calc(Object, Valuable, Valuable)
	};
	
	
	
	
	public final static Operator OR = new Operator(){
		public ResultType calc(CodeTrackable source, Valuable left, Valuable right, Receptor receptor, BASICRunner vt){
			ResultType res;

			res = checkLeftNotNull(source, left, receptor.bundle);
			if(res != ResultType.DONE) return res;
			res = checkRightNotNull(source, right, receptor.bundle);
			if(res != ResultType.DONE) return res;
			
			Receptor lrecp = new Receptor();
			lrecp.bundle = receptor.bundle;
			Receptor rrecp = new Receptor();
			rrecp.bundle = receptor.bundle;
			
			res = vt.getValue(source, left, lrecp);
			if(res != ResultType.DONE)
				return res;
			res = vt.getValue(source, right, rrecp);
			if(res != ResultType.DONE)
				return res;
			
			if(lrecp.type != ValueType.TYPE_BOOLEAN){
				receptor.bundle.addErrorMessage(new ErrorMessage(source,
						left,
						left.getCodeRange(),
						ERRCNT_RIGHTVAL_WRONGTYPE,
						RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}else if(rrecp.type != ValueType.TYPE_BOOLEAN){
				receptor.bundle.addErrorMessage(new ErrorMessage(source,
						right,
						right.getCodeRange(),
						ERRCNT_LEFTVAL_WRONGTYPE,
						RUNTIME_ERROR));
				return ResultType.RUNTIME_ERROR;
			}
			
			boolean lval = (Boolean)lrecp.value;
			boolean rval = (Boolean)rrecp.value;
			
			receptor.value = (Boolean)(lval || rval);
			receptor.type = ValueType.TYPE_BOOLEAN;
			return ResultType.DONE;
		}// end calc(Object, Valuable, Valuable)
	};
	
	
	
	
	
	
	
	
	public static boolean isOperator(String op){
		op = op.toLowerCase();
		if(op.equals("+"))
			return true;
		else if(op.equals("-"))
			return true;
		else if(op.equals("*"))
			return true;
		else if(op.equals("/"))
			return true;
		else if(op.equals("%") || op.equals("mod"))
			return true;
		else if(op.equals(">>"))
			return true;
		else if(op.equals("<<"))
			return true;
		else if(op.equals(">"))
			return true;
		else if(op.equals("<"))
			return true;
		else if(op.equals(">="))
			return true;
		else if(op.equals("<="))
			return true;
		else if(op.equals("="))
			return true;
		else if(op.equals("<>"))
			return true;
		else if(op.equals("and"))
			return true;
		else if(op.equals("or"))
			return true;
		return false;
	}// end isOperator(String)
	public static Operator getOperator(String op){
		op = op.toLowerCase();
		if(op.equals("+"))
			return PLUS;
		else if(op.equals("-"))
			return MINUS;
		else if(op.equals("*"))
			return MULTIPLY;
		else if(op.equals("/"))
			return DIVISION;
		else if(op.equals("%") || op.equals("mod"))
			return MOD;
		else if(op.equals(">>"))
			return BITRIGHTSHIFT;
		else if(op.equals("<<"))
			return BITLEFTSHIFT;
		else if(op.equals(">"))
			return GREATER;
		else if(op.equals("<"))
			return LESS;
		else if(op.equals(">="))
			return GREATEROREQUAL;
		else if(op.equals("<="))
			return LESSOREQUAL;
		else if(op.equals("="))
			return EQUAL;
		else if(op.equals("<>"))
			return NOTSAME;
		else if(op.equals("and"))
			return AND;
		else if(op.equals("or"))
			return OR;
		return null;
	}// end getOperator
	public static String getCharacter(Operator op){
		if(op == PLUS)
			return "+";
		else if(op == MINUS)
			return "-";
		else if(op == MULTIPLY)
			return "*";
		else if(op == DIVISION)
			return "/";
		else if(op == MOD)
			return "%";
		else if(op == BITRIGHTSHIFT)
			return ">>";
		else if(op == BITLEFTSHIFT)
			return "<<";
		else if(op == GREATER)
			return ">";
		else if(op == LESS)
			return "<";
		else if(op == GREATEROREQUAL)
			return ">=";
		else if(op == LESSOREQUAL)
			return "<=";
		else if(op == EQUAL)
			return "=";
		else if(op == NOTSAME)
			return "<>";
		else if(op == AND)
			return "AND";
		else if(op == OR)
			return "OR";
		return null;
	}// end getCharacter(Operator)
	
	// 1이면 a가 곱셈, b가 덧셈과 같은 경우
	// 0이면 같은 랭킹
	// -1이면 a가 덧셈, b가 곱셈
	public static int compareRank(Operator a, Operator b){
		int arank = _getrank(a), brank = _getrank(b);
		if(arank > brank)
			return 1;
		else if(arank == brank)
			return 0;
		return -1;
	}// end compareRank
	static int _getrank(Operator op){
		if(op == MULTIPLY || op == DIVISION || op == MOD)
			return 4;
		else if(op == PLUS || op == MINUS)
			return 3;
		else if(op == BITRIGHTSHIFT || op == BITLEFTSHIFT)
			return 2;
		else if(op == GREATER || op == LESS || op == GREATEROREQUAL || op == LESSOREQUAL || op == EQUAL || op == NOTSAME)
			return 1;
		else if(op == AND || op == OR)
			return 0;
		return -1;
	}// end _getrank
	
	
	

	public abstract ResultType calc(CodeTrackable source, Valuable left, Valuable right, Receptor receptor, BASICRunner debugger);
	
	public String toString()
	{ return getCharacter(this); }
}
