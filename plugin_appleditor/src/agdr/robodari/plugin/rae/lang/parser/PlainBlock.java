package agdr.robodari.plugin.rae.lang.parser;

import agdr.library.lang.*;

import java.util.*;

public class PlainBlock extends Block{
	private final static Statement[] NULL_ARRAY = new Statement[]{};
	
	private Vector<Element> elements = new Vector<Element>();
	
	public PlainBlock(CodeRange range, Block parent){
		super(range, parent);
	}// end Constructor(CodeRnage, ErrorBundle)
	
	
	public Element getElement(int idx)
	{ return elements.get(idx); }
	public Element[] getElements()
	{ return elements.toArray(NULL_ARRAY); }
	public int getElementCount()
	{ return elements.size(); }
	public boolean containsElement(Element cs)
	{ return elements.contains(cs); }
	
	public void clear()
	{ elements.clear(); }
	
	public int indexOfElement(Element elem)
	{ return elements.indexOf(elem); }
	public void addElement(Element elem){
		elem.setParent(this);
		elements.add(elem);
	}// end addNode(RBElement)
	public void removeElement(Element elem){
		if(!elements.contains(elem))
			return;
		elements.remove(elem);
		elem.setParent(null);
	}// end remove
	
	public String toString(){
		StringBuffer buf = new StringBuffer();
		for(int i = 0; i < this.getElementCount(); i++)
			buf.append(this.getElement(i)).append("\n");
		return buf.toString();
	}// end toString
}
