package agdr.robodari.plugin.rae.lang.parser;

import agdr.library.lang.*;

public class Statement extends Element{
	protected Statement(CodeRange range, Block parent){
		super(range, parent);
	}// end Constructor(int, int, int)

	public Block getRoot(){
		if(parent == null)
			return null;
		return parent.getRoot();
	}// end getRoot()
}
