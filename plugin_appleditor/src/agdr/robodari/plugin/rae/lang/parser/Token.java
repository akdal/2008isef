package agdr.robodari.plugin.rae.lang.parser;

import agdr.library.lang.*;

public class Token implements CodeRange{
	private String cnt;
	private int start, len;
	
	public Token(String cnt, int start, int len){
		this.cnt = cnt;
		this.start = start;
		this.len = len;
	}// end Constructor(String, int, int)
	
	public String content()
	{ return cnt; }
	public int getStartPosition()
	{ return start; }
	public int getLength()
	{ return len; }
	public int length()
	{ return len; }
	public String toString()
	{ return cnt; }
	
	public boolean equals(Object obj){
		if(obj == null) return false;
		else if(obj instanceof String)
			return this.cnt.equals(obj);
		else if(!(obj instanceof Token))
			return false;
		
		Token t = (Token)obj;
		return this.cnt == t.cnt && this.start == t.start && this.len == t.len;
	}// end equals(Object)
}
