package agdr.robodari.plugin.rae.lang.parser;


import agdr.library.lang.*;
import agdr.robodari.plugin.rae.lang.*;

public interface Valuable extends java.io.Serializable, CodeTrackable{
	public CodeRange getCodeRange();
	
	public Object getParent();
	public int getPosition();
	public void setParent(Object parent);
	public void setPosition(int idx);
}
