package agdr.robodari.plugin.rae.lang.parser;

import agdr.library.lang.CodeRange;

public class Value implements Valuable{
	public final static int BIN = 1;
	public final static int OCT = 2;
	public final static int DEC = 3;
	public final static int HEX = 4;
	
	private ValueType valueType;
	private Object value;
	private CodeRange range;
	private Object parent;
	private int position;
	
	public Value(CodeRange range, ValueType valueType, Object value){
		this.valueType = valueType;
		this.value = value;
		this.range = range;
		
		if(value instanceof String){
			this.value = _rpEscCodes((String)value);
		}
	}// end Constructor(ValueType, Object)
	
	public ValueType getValueType()
	{ return valueType; }
	public void setValueType(ValueType vt)
	{ this.valueType = vt;} 
	
	public Object getParent()
	{ return parent; }
	public int getPosition()
	{ return position; }
	public void setParent(Object p)
	{ this.parent = p; }
	public void setPosition(int p)
	{ this.position = p; }
	
	public Object getValue()
	{ return value; }
	public void setValue(Object obj)
	{ this.value = obj; 
	if(value instanceof String){
		this.value = _rpEscCodes((String)value);
	}
	}
	
	
	private String _rpEscCodes(String str){
		str = _rpEscCodes(str, "\\t", '\t');
		str = _rpEscCodes(str, "\\n", '\n');
		str = _rpEscCodes(str, "\\r", '\r');
		str = _rpEscCodes(str, "\\f", '\n');
		str = _rpEscCodes(str, "\\b", '\b');
		str = _rpEscCodes(str, "\\f", '\f');
		return str;
	}// end _RpEscCodes(String)
	private String _rpEscCodes(String str, String a, char b){
		StringBuffer result = new StringBuffer();
		int szlen = str.length();
		int alen = a.length();
		
		for(int i = 0; i < szlen - alen; i++){
			boolean flag = false;
			for(int j = 0; j < alen; j++)
				if(a.charAt(j) != str.charAt(i + j)){
					flag = true;
					j = 21474836;
				}
			
			if(!flag){
				//matches
				result.append(b);
				i += alen - 1;
			}else
				result.append(str.charAt(i));
		}
		for(int i = (szlen - alen < 0 ? 0 : szlen - alen); i < szlen; i++)
			result.append(str.charAt(i));
		return result.toString();
	}// end _rpEscCodes
	
	public boolean equals(Object obj){
		if(obj == this) return true;
		else if(obj == null) return false;
		else if(!(obj instanceof Value)) return false;
		
		Value val = (Value)obj;
		return val.value.equals(this.value) && val.valueType.equals(valueType);
	}// end equals(Object)

	public CodeRange getCodeRange()
	{ return range; }
	public void setCodeRange(CodeRange range){
		this.range = range;
	}// end setCodeRange(int, int)
	
	public String toString(){
		if(valueType == ValueType.TYPE_STRING)
			return "\"" + value + "\"";
		return value.toString();
	}// end toString()
	
	
/*
	public void readExternal(ObjectInput input) throws IOException,
			ClassNotFoundException{
		valueType = (ValueType)input.readObject();
		value = input.readObject();
		radix = input.readInt();
		range = (CodeRange)input.readObject();
		position = input.readInt();
	}// end readExternal(OBjectInput)
	public void writeExternal(ObjectOutput output) throws IOException{
		output.writeObject(valueType);
		output.writeObject(value);
		output.writeInt(radix);
		output.writeObject(range);
		output.writeInt(position);
	}// end readExternal(OBjectInput)*/
}
