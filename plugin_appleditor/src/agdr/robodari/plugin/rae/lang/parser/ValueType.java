package agdr.robodari.plugin.rae.lang.parser;

public class ValueType implements java.io.Serializable{
	public final static ValueType TYPE_INT = new ValueType("integer");
	public final static ValueType TYPE_STRING = new ValueType("string");
	public final static ValueType TYPE_BOOLEAN = new ValueType("boolean");
	public final static ValueType TYPE_UNKNOWN = new ValueType("");
	
	
	
	public final static boolean isUsable(ValueType type){
		return type == TYPE_INT || 
				type == TYPE_STRING || 
				type == TYPE_BOOLEAN;
	}// end isUsable
	public final static boolean hasType(String str){
		str = str.toLowerCase();
		if(str.equals(TYPE_INT.name))
			return true;
		else if(str.equals(TYPE_STRING.name))
			return true;
		else if(str.equals(TYPE_BOOLEAN.name))
			return true;
		return false;
	}// end hasType(STring)
	public final static ValueType getType(String str){
		str = str.toLowerCase();
		if(str.equals(TYPE_INT.name))
			return TYPE_INT;
		else if(str.equals(TYPE_STRING.name))
			return TYPE_STRING;
		else if(str.equals(TYPE_BOOLEAN.name))
			return TYPE_BOOLEAN;
		return null;
	}// end getType
	
	
	private String name;
	
	public ValueType(String name)
	{ this.name = name; }
	
	public String getName1()
	{ return name; }
	
	public String toString(){
		return name;
	}// end toString()
}
