package agdr.robodari.plugin.rae.lang.parser;

import agdr.library.lang.*;

public class VariableCallValue implements Valuable{
	private String target;
	private CodeRange range;
	private Object parent;
	private int position;
	
	public VariableCallValue(String target, CodeRange range){
		this.target = target; 
		this.range = range;
	}// end Constructor(Variable)
	
	public String getTarget()
	{ return target; }
	public void setTarget(String t)
	{ this.target = t;}

	public CodeRange getCodeRange()
	{ return range; }
	public void setCodeRange(CodeRange cr)
	{ this.range = cr; }
	
	public Object getParent()
	{ return parent; }
	public int getPosition()
	{ return position; }
	public void setParent(Object p)
	{ this.parent = p; }
	public void setPosition(int p)
	{ this.position = p; }
	
	public String toString()
	{ return target; }
}
