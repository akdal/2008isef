package agdr.robodari.plugin.rae.lang.parser;

import java.util.Vector;
import agdr.library.lang.CodeRange;

public class WhileBlock extends Block{
	private final static Statement[] NULL_ARRAY = new Statement[]{};
	
	private Valuable condition;
	private Vector<Element> elements = new Vector<Element>();
	
	public WhileBlock(CodeRange range, Block parent, Valuable condition){
		super(range, parent);
		this.condition = condition;
	}// end Constructor
	
	
	public Valuable getCondition()
	{ return condition; }
	public void setCondition(Valuable cond)
	{ this.condition = cond; }
	
	public Element getElement(int idx)
	{ return elements.get(idx); }
	public Element[] getElements()
	{ return elements.toArray(NULL_ARRAY); }
	public int getElementCount()
	{ return elements.size(); }
	public boolean containsElement(Element cs)
	{ return elements.contains(cs); }
	
	public void clear()
	{ elements.clear(); }
	
	public int indexOfElement(Element elem)
	{ return elements.indexOf(elem); }
	public void addElement(Element elem){
		elements.add(elem);
	}// end addNode(RBElement)
	public void removeElement(Element elem)
	{ elements.remove(elem); }
	
	
	
	public String toString(){
		StringBuffer buf = new StringBuffer();
		buf.append("WHILE ");
		if(this.condition != null)
			buf.append(condition);
		
		buf.append("\n");
		for(int i = 0; i < this.getElementCount(); i++)
			buf.append(getElement(i)).append("\n");
		buf.append("WEND");
		return buf.toString();
	}// end toString()
}
