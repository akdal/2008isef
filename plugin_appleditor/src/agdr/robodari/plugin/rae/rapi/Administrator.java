package agdr.robodari.plugin.rae.rapi;

import agdr.robodari.plugin.rae.edit.*;
import agdr.robodari.plugin.rae.rlztion.RARealizationModel;

public interface Administrator {
	public abstract void init(RARealizationModel rmodel) throws IllegalOptionException;
	public abstract void start();
	public abstract void destroy();
	public abstract boolean isRunning();
	public abstract LearningEngine getLearningEngine();
}
