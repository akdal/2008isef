package agdr.robodari.plugin.rae.rapi;

public interface CommunicationTools {
	public int getAntennaCount();
	public String getAntennaName(int idx);
	public void send(String atname, Object data);
}
