package agdr.robodari.plugin.rae.rapi;

public interface LearningEngine {
	public int entityCount();
	public int stateCount(int entityIdx);
	public int getAction(int entityIdx, int state);
	public String getProperty(String key);
}
