package agdr.robodari.plugin.rae.rapi;

import agdr.robodari.plugin.rme.rdpi.RDEntity;

public interface RDEntityManager {
	public String[] getRDEntityNames();
	public RDEntity getEntity(String name);

	public void startEntities();
	public void pauseEntities();
	public void resumeEntities();
	public void disposeEntities();
	
	public boolean isEntRunning();
}
