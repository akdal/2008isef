package agdr.robodari.plugin.rae.rapi;

import agdr.robodari.comp.*;
import agdr.robodari.plugin.rme.rdpi.*;

// server의 주기는 processor와 동일함.
public interface Server{
	// FIXME Server에 장착할 수 있는 컴포넌트로는 카메라도 있다. 매우 다양함
	public void initServer(CommunicationTools tools) throws IllegalOptionException;
	public void startServer();
	public void stopServer();
	public void resumeServer();
	public void destroyServer();
	
	public boolean isRunning();
}
