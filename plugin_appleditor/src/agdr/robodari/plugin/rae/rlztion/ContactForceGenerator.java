package agdr.robodari.plugin.rae.rlztion;

import java.util.*;
import javax.vecmath.*;

import agdr.library.math.*;
import agdr.robodari.appl.*;
import agdr.robodari.comp.model.*;
import agdr.robodari.plugin.rme.rlztion.*;

public class ContactForceGenerator implements ForceGenerator{
	private World world;
	private RealizationMap rmap;
	private int strcount = 0;
	private double coeffOfRestt = 0;
	private ArrayList<ClosestPosInfo> cpipool;
	//private ArrayList<ClosestPosInfo> cpipool_nxt;
	private ArrayList<ContactModel.Contact> calcedContacts;
	
	
	
	public int getGeneratorType()
	{ return ForceGenerator.DEPENDSON_POSITION | LCP_NEEDED; }
	
	public ArrayList<ContactModel.Contact> getCalcedContacts()
	{ return calcedContacts; }
	
	public void init(RealizationModel rmodel, RealizationMap rmap, World world){
		this.world = world;
		this.rmap = rmap;
		this.strcount = rmap.getModelCount();
		this.coeffOfRestt = ((RARealizationModel)rmodel).getCoeffOfRestt();
		this.cpipool = new ArrayList<ClosestPosInfo>();
		this.calcedContacts = new ArrayList<ContactModel.Contact>();
		__oitsc_init();
		__oitsc_find();
	}// end init()
	
	
	
	//////////////////// Originally intersected structures
	private boolean[] __oitsc;
	
	private void __oitsc_init(){
		__oitsc = new boolean[(strcount - 1) * strcount / 2];
	}// end __oitsc_init()
	private void __oitsc_find(){
		BoundObject a, b;
		
		Matrix4d thistr = new Matrix4d(),
				bobjtr= new Matrix4d();
		
		for(int i = 0; i < strcount; i++){
			a = world.getBound(i);
			world.getTransform(i, thistr);
			for(int j = i + 1; j < strcount; j++){
				b = world.getBound(j);
				world.getTransform(j, bobjtr);
				
				if(a == null || b == null)
					__oitsc_mark(i, j);
				else if((a != null) && a.intersects(b, thistr, bobjtr, cpipool)){
					__oitsc_mark(i, j);
				}else if((b != null) && b.intersects(a, bobjtr, thistr, cpipool)){
					__oitsc_mark(i, j);
				}
			}
		}
	}// end __oitsc_find
	private void __oitsc_mark(int i, int j){
		if(i == j) return;
		else if(i > j)
			__oitsc_mark(j, i);
		else{
			__oitsc[__toidx(i, j, strcount)] = true;
			BugManager.printf("ContFrGen : __oitsc_mark : %d, %d is originally intersected!!!!\n", i, j);
		}
	}// end __oitsc_mark
	private boolean __oitsc_marked(int i, int j){
		if(i == j) return true;
		else if(i > j)
			return __oitsc_marked(j, i);
		
		return __oitsc[__toidx(i, j, strcount)];
	}// end __oitsc_marked
	private int __toidx(int i, int j, int n){
		return ((n - 1) * (n - 2) / 2) - ((n - 1 - i) * (n - i - 2) / 2) + j - 1;
	}// end __toidx
	
	
	
	public boolean effective(RealizationModel rmodel, RealizationMap rmap, World world){
		return rmodel instanceof RARealizationModel;
	}// end effective
	
	
	private Vector3d normal = new Vector3d(),
			edgnormb = new Vector3d(),
			edgnorma = new Vector3d(),
			
			ivel = new Vector3d(),
			iavel = new Vector3d(),
			iaccl = new Vector3d(),
			iangAccl = new Vector3d(),
			
			jvel = new Vector3d(),
			javel = new Vector3d(),
			jaccl = new Vector3d(),
			jangAccl = new Vector3d();
	private Matrix4d itrans_nxt = new Matrix4d(),
			itrans = new Matrix4d(),
			jtrans_nxt = new Matrix4d(),
			jtrans = new Matrix4d();
	private Matrix3d iinrtInv = new Matrix3d(),
			jinrtInv = new Matrix3d();
	
	public void generateForce(){
		if(BugManager.DEBUG && World.W_DEBUG)
			BugManager.printf("ContactFG Start\n");
		calcedContacts.clear();
		
		/*
		 * 다음 프레임(_nxt)에서 두 물체가 겹칠 수 있다.
		 * 따라서, _nxt에서 겹칠 예정인 물체 쌍을 찾아낸 후, 현재 프레임에서 두 물체가 충돌한다고 가정한다.
		 */
		int lim = rmap.getModelCount();
		BoundObject iobj, jobj;
		ContactModel contmodel = world.getContactModel();
		ContactModel.Contact contact = null;
		double ddotx, ddoty, ddotz, ddot;
		double ddotdotx, ddotdoty, ddotdotz, ddotdot;
		double apx, apy, apz, bpx, bpy, bpz;
		boolean onedge;
		ClosestPosInfo cpi, cpi_nxt;
		
		for(int i = 0; i < lim; i++){
			iobj = world.getBound(i);
			if(iobj == null)
				continue;
			
			world.getTransform_nxt(i, itrans_nxt);
			world.getTransform(i, itrans);
			
			world.getVelocity_bef(i, ivel);
			world.getAngularVelocity_bef(i, iavel);
			world.getAcceleration(i, iaccl);
			world.getAngularAccl(i, iangAccl);
			world.getInrtTensor_Inv(i, iinrtInv);
			
			for(int j = i + 1; j < lim; j++){
				if(BugManager.DEBUG && World.W_DEBUG)
					BugManager.printf("ContactFG : Intersection test : %d, %d : \n", i, j);
				if(__oitsc_marked(i, j))
					continue;
				
				// ci.bid = j, ci.aid = i.
				jobj = world.getBound(j);
				if(jobj == null) continue;
				
				onedge = false;
				world.getTransform_nxt(j, jtrans_nxt);
				
				cpipool.clear();
				if(!iobj.intersects(jobj, itrans_nxt, jtrans_nxt, null))
					continue;
				world.getTransform(j, jtrans);


				if(BugManager.DEBUG && World.W_DEBUG)
					BugManager.printf("ContactFG : !!INTERSECTS!!\n");
				

				if(BugManager.DEBUG && World.W_DEBUG)
					BugManager.printf("ContactFG : currently intersects? : \n");
				if(iobj.intersects(jobj, itrans, jtrans, cpipool)){
					if((BugManager.DEBUG)){
						BugManager.printf("BUG!!! in COntactForceGenerator : already intersects...\n");
						System.exit(0);
					}
					continue;
				}

				if(BugManager.DEBUG && World.W_DEBUG)
					BugManager.printf("ContactFG : end curr intersection inspection\n");
				
				world.getVelocity_bef(j, jvel);
				world.getAngularVelocity_bef(j, javel);
				world.getAngularAccl(j, jangAccl);
				world.getAcceleration(j, jaccl);
				world.getInrtTensor_Inv(j, jinrtInv);
				if(BugManager.DEBUG && World.W_DEBUG)
					BugManager.printf("ContactFG : \n   ivel : %s\n   iavel : %s\n   jvel : %s\n   javel : %s\n", ivel, iavel, jvel, javel);
				
				for(int l = 0; l < cpipool.size(); l++){
					if(BugManager.DEBUG && World.W_DEBUG)
						BugManager.printf("--- cpipool.nextElement()\n");
					
					cpi = cpipool.get(l);

					if((BugManager.DEBUG && World.W_DEBUG)){
						BugManager.printf("ContactFG : next elem : aType : %d, bType : %d\n",
								cpi.aType, cpi.bType);
						BugManager.printf("  cpi.aobj : %s, bobj : %s\n", cpi.aBoundsObj, cpi.bBoundsObj);
						BugManager.printf("  cpi.aPoints : %g %g %g\n", cpi.aPoints[0], cpi.aPoints[1], cpi.aPoints[2]);
						BugManager.printf("  cpi.bPoints : %g %g %g\n", cpi.bPoints[0], cpi.bPoints[1], cpi.bPoints[2]);
					}
					
					if(cpi.aBoundsObj == jobj && cpi.bBoundsObj == iobj){
						cpi.aBoundsObj = iobj;
						cpi.bBoundsObj = jobj;
						
						double[] tmp = cpi.aPoints;
						cpi.aPoints = cpi.bPoints;
						cpi.bPoints = tmp;
						
						int tmptype = cpi.aType;
						cpi.aType = cpi.bType;
						cpi.bType = tmptype;
					}
					
					if(cpi.aPoints.length == 3 && cpi.bPoints.length == 3){
						// 점과 점이 만남.
						/*if(!jobj.onFace(jtrans, cpi.bPoints[0], cpi.bPoints[1], cpi.bPoints[2], normal)){
							if(!iobj.onFace(itrans, cpi.aPoints[0], cpi.aPoints[1], cpi.aPoints[2], normal)){
								if(!jobj.onEdge(jtrans, cpi.bPoints[0], cpi.bPoints[1], cpi.bPoints[2], edgnormb) || 
										!iobj.onEdge(itrans, cpi.aPoints[0], cpi.aPoints[1], cpi.aPoints[2], edgnorma)){
									BugManager.println("BUG!!! in ContactForceGenerator : cpi.points len : 3,");
									BugManager.printf("  a : %s\n  b : %s\n", iobj, jobj);
									BugManager.printf("  itrans : \n%s  jtrans : \n%s", itrans, jtrans);
									BugManager.printf("  aType : %d, aPoints : %g %g %g\n", cpi.aType, cpi.aPoints[0], cpi.aPoints[1], cpi.aPoints[2]);
									BugManager.printf("  bType : %d, bPoints : %g %g %g\n", cpi.bType, cpi.bPoints[0], cpi.bPoints[1], cpi.bPoints[2]);
									System.exit(-1);
								}
								normal.x = normal.y = normal.z = 0;
								normal.cross(edgnormb, edgnorma);
							}else{
								// iobj face에서 나가는 방향
								// jobj에서 나가는 방향으로 고쳐주어야함
								normal.negate();
							}
						}*/
						normal.x = cpi.bPoints[0] - cpi.aPoints[0];
						normal.y = cpi.bPoints[1] - cpi.aPoints[1];
						normal.z = cpi.bPoints[2] - cpi.aPoints[2];
						normal.normalize();
					}else if(cpi.aPoints.length == 6 && cpi.bPoints.length == 6){
						/*if(!jobj.onFace(jtrans, cpi.bPoints[0], cpi.bPoints[1], cpi.bPoints[2], normal)){
							if(!iobj.onFace(itrans, cpi.aPoints[0], cpi.aPoints[1], cpi.aPoints[2], normal)){
								BugManager.println("BUG!!! in ContactForceGenerator : not on face : cpi.points len : 6,");
								BugManager.printf("  a : %s\n  b : %s\n", iobj, jobj);
								BugManager.printf("  itrans : \n%s  jtrans : \n%s", itrans, jtrans);
								BugManager.printf("  aType : %d, aPoints : %g %g %g, %g %g %g\n", cpi.aType, cpi.aPoints[0], cpi.aPoints[1], cpi.aPoints[2], cpi.aPoints[3], cpi.aPoints[4], cpi.aPoints[5]);
								BugManager.printf("  bType : %d, bPoints : %g %g %g, %g %g %g\n", cpi.bType, cpi.bPoints[0], cpi.bPoints[1], cpi.bPoints[2], cpi.bPoints[3], cpi.bPoints[4], cpi.bPoints[5]);
								System.exit(0);
							}else
								normal.negate();
						}*/
						if((cpi.bPoints[0] - cpi.aPoints[0]) * (cpi.bPoints[0] - cpi.aPoints[0])
								+ (cpi.bPoints[1] - cpi.aPoints[1]) * (cpi.bPoints[1] - cpi.aPoints[1])
								+ (cpi.bPoints[2] - cpi.aPoints[2]) * (cpi.bPoints[2] - cpi.aPoints[2])
								< 
								(cpi.bPoints[3] - cpi.aPoints[0]) * (cpi.bPoints[3] - cpi.aPoints[0])
								+ (cpi.bPoints[4] - cpi.aPoints[1]) * (cpi.bPoints[4] - cpi.aPoints[1])
								+ (cpi.bPoints[5] - cpi.aPoints[2]) * (cpi.bPoints[5] - cpi.aPoints[2])){
							
							normal.x = cpi.bPoints[0] - cpi.aPoints[0];
							normal.y = cpi.bPoints[1] - cpi.aPoints[1];
							normal.z = cpi.bPoints[2] - cpi.aPoints[2];
						}else{
							normal.x = cpi.bPoints[3] - cpi.aPoints[0];
							normal.y = cpi.bPoints[4] - cpi.aPoints[1];
							normal.z = cpi.bPoints[5] - cpi.aPoints[2];
						}
						normal.normalize();
					}else{
						normal.x = cpi.onFaceCase_normalx;
						normal.y = cpi.onFaceCase_normaly;
						normal.z = cpi.onFaceCase_normalz;
						BugManager.printf("\tface by face! normal : %s\n", normal);
					}
	
					// 충돌접촉인지 비충돌접촉인지 알아야 한다.
					// 여기서 확인 : _nxt가 아닌 현재의 velocity와 angular vel을 얻어서 p점의 속도가 0이면 비충돌접촉
					
					for(int k = 0; k < cpi.aPoints.length / 3; k++){
						contact = null;
						// cpi.aPoints.len = cpi.bPoints.len
						
						apx = cpi.aPoints[k * 3] - itrans.m03;
						apy = cpi.aPoints[k*3+1] - itrans.m13;
						apz = cpi.aPoints[k*3+2] - itrans.m23;
						bpx = cpi.aPoints[k * 3] - jtrans.m03;//cpi.bPoints[k * 3] - jtrans.m03;
						bpy = cpi.aPoints[k*3+1] - jtrans.m13;//cpi.bPoints[k*3+1] - jtrans.m13;
						bpz = cpi.aPoints[k*3+2] - jtrans.m23;//cpi.bPoints[k*3+2] - jtrans.m23;
						
						// ci.bid = j, ci.aid = i.
						ddotx = ivel.x - jvel.x
								+ (iavel.y * apz - iavel.z * apy)
								- (javel.y * bpz - javel.z * bpy);
						ddoty = ivel.y - jvel.y
								- (iavel.x * apz - iavel.z * apx)
								+ (javel.x * bpz - javel.z * bpx);
						ddotz = ivel.z - jvel.z
								+ (iavel.x * apy - iavel.y * apx)
								- (javel.x * bpy - javel.y * bpx);
	
						// val1 : relAccl : a + alpha cross r_a
						ddotdotx =   iangAccl.y * apz - iangAccl.z * apy + iaccl.x;
						ddotdoty = -(iangAccl.x * apz - iangAccl.z * apx) + iaccl.y;
						ddotdotz =   iangAccl.x * apy - iangAccl.y * apx + iaccl.z;
						
						ddotdotx -=   jangAccl.y * bpz - jangAccl.z * bpy + jaccl.x;
						ddotdoty -= -(jangAccl.x * bpz - jangAccl.z * bpx) + jaccl.y;
						ddotdotz -=   jangAccl.x * bpy - jangAccl.y * bpx + jaccl.z;
						
						
						if(BugManager.DEBUG && World.W_DEBUG){
							BugManager.printf("%dth point : ddot : %g %g %g\n  normal : %g %g %g\n", k, ddotx, ddoty, ddotz, normal.x, normal.y, normal.z);
							BugManager.printf("    ap : %g %g %g, bp : %g %g %g\n", apx, apy, apz, bpx, bpy, bpz);
							BugManager.printf("    ddotdot : %g %g %g\n", ddotdotx, ddotdoty, ddotdotz);
							BugManager.printf("    iaccl : %s, iangaccl : %s, jaccl : %s, jangaccl : %s\n", iaccl, iangAccl, jaccl, jangAccl);
						}
						ddot =    normal.x * ddotx + normal.y * ddoty + normal.z * ddotz;
						ddotdot = normal.x * ddotdotx + normal.y * ddotdoty + normal.z * ddotdotz;
						
						contact = contmodel.append();

						contact.type = ContactModel.Contact.TYPE_BOTH;
						
						contact.currDDot = ddot;
						contact.currRelVelX = ddotx;
						contact.currRelVelY = ddoty;
						contact.currRelVelZ = ddotz;
						
						contact.currDDotDot = ddotdot;
						//contact.currRelAcclX = ddotdotx;
						//contact.currRelAcclY = ddotdoty;
						//contact.currRelAcclZ = ddotdotz;
						
						contact.apx = apx;
						contact.apy = apy;
						contact.apz = apz;
						contact.bpx = bpx;
						contact.bpy = bpy;
						contact.bpz = bpz;
						
						contact.aid = i;
						contact.bid = j;
						contact.onEdge = false;
						// FIXME
						// Physical Data에 들어가야 하지 않을까...
						contact.coefOfRestt = this.coeffOfRestt;
						contact.nx = normal.x;
						contact.ny = normal.y;
						contact.nz = normal.z;
						if(onedge){
							contact.edgeax = edgnorma.x;
							contact.edgeay = edgnorma.y;
							contact.edgeaz = edgnorma.z;
							contact.edgebx = edgnormb.x;
							contact.edgeby = edgnormb.y;
							contact.edgebz = edgnormb.z;
							contact.onEdge = true;
						}
						contact.result_force = contact.result_impulse = 0;
						this.calcedContacts.add(contact);
					}
				}
			}
		}
	}// end generateForce
}
