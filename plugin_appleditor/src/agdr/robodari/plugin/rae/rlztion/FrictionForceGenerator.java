package agdr.robodari.plugin.rae.rlztion;

import java.util.*;
import javax.vecmath.*;

import agdr.robodari.appl.*;
import agdr.robodari.plugin.rme.rlztion.*;

public class FrictionForceGenerator implements ForceGenerator{
	private ContactForceGenerator contactfg;
	private World world;
	private RealizationMap rmap;
	
	
	public FrictionForceGenerator(ContactForceGenerator cfg)
	{ this.contactfg = cfg; }
	
	

	public boolean effective(RealizationModel rmodel, RealizationMap rmap, World world){
		return rmodel instanceof RARealizationModel;
	}// end effective
	
	public void init(RealizationModel rmodel, RealizationMap rmap, World world){
		this.world = world;
		this.rmap = rmap;
	}// end init()
	
	public int getGeneratorType()
	{ return ForceGenerator.DEPENDSON_SITUATION
		| ForceGenerator.DEPENDSON_VELOCITY
		| ForceGenerator.DEPENDSON_ACCEL; }

	
	private Vector3d avel = new Vector3d(),
						aangvel = new Vector3d(),
						bvel = new Vector3d(),
						bangvel = new Vector3d();
	private Vector3d amom = new Vector3d(),
						aamom = new Vector3d(),
						bmom = new Vector3d(),
						bamom = new Vector3d();
	
	public void generateForce(){
		ArrayList<ContactModel.Contact> carray = contactfg.getCalcedContacts();
		double ndotrv;
		double relvelx, relvely, relvelz;
		double dirx, diry, dirz, dirlen;
		// FIXME
		double mu = 1.0 / 2.0;
		
		for(int i = 0; i < carray.size(); i++){
			ContactModel.Contact con = carray.get(i);
			//System.out.printf("FrictionFG : nextelem : %s\n", con);
			
			world.getVelocity_bef(con.aid, avel);
			world.getAngularVelocity_bef(con.aid, aangvel);
			world.getVelocity_bef(con.bid, bvel);
			world.getAngularVelocity_bef(con.bid, bangvel);

			relvelx = -(avel.x + (aangvel.y * con.apz - aangvel.z * con.apy)) + (bvel.x + (bangvel.y * con.bpz - bangvel.z * con.bpy));
			relvely = -(avel.y - (aangvel.x * con.apz - aangvel.z * con.apx)) + (bvel.y - (bangvel.x * con.bpz - bangvel.z * con.bpx));
			relvelz = -(avel.z + (aangvel.x * con.apy - aangvel.y * con.apx)) + (bvel.z + (bangvel.x * con.bpy - bangvel.y * con.bpx));
			
			// '운동 방향과 반대로'를 얻는 방법 : normal벡터를 알고 있으므로, relvel에서 normal을 뺀다.
			// dir = relvel - (N dot relvel)N
			// n dot relvel
			ndotrv = con.nx * relvelx + con.ny * relvely + con.nz * relvelz;
			dirx = (relvelx - ndotrv * con.nx);
			diry = (relvely - ndotrv * con.ny);
			dirz = (relvelz - ndotrv * con.nz);
			
			dirlen = Math.sqrt(dirx * dirx + diry * diry + dirz * dirz);
			//if(dirlen < 0.08)
			//	continue;
				
			dirx /= dirlen;
			diry /= dirlen;
			dirz /= dirlen;
			
			/*
			if(con.type == ContactModel.Contact.TYPE_BOTH || con.type == ContactModel.Contact.TYPE_COLLISION){
				// 물체가 다른 물체와 부딪힘.
				BugManager.printf("FrictionFG : Friction Impulse gen : dir : %g %g %g\n", dirx, diry, dirz);
				// 마찰력에 의한 충격량
				// I_fr = mu |N delta t| hat dir
				//      = mu (con.result_impulse) hat dir
				world.getMomentum_bef(con.aid, imom);
				world.getAngularMomentum_bef(con.aid, iamom);
				imom.x += mu * con.result_impulse * dirx;
				imom.y += mu * con.result_impulse * diry;
				imom.z += mu * con.result_impulse * dirz; 
				iamom.x +=  mu * con.result_impulse * (con.apy * dirz - con.apz * diry);
				iamom.y += -mu * con.result_impulse * (con.apx * dirz - con.apz * dirx);
				iamom.z +=  mu * con.result_impulse * (con.apx * diry - con.apy * dirx);
				world.setMomentum_bef(con.aid, imom);
				world.setAngularMomentum_bef(con.aid, iamom);

				world.getMomentum_bef(con.bid, jmom);
				world.getAngularMomentum_bef(con.bid, jamom);
				jmom.x -= mu * con.result_impulse * dirx;
				jmom.y -= mu * con.result_impulse * diry;
				jmom.z -= mu * con.result_impulse * dirz; 
				jamom.x -=  mu * con.result_impulse * (con.bpy * dirz - con.bpz * diry);
				jamom.y -= -mu * con.result_impulse * (con.bpx * dirz - con.bpz * dirx);
				jamom.z -=  mu * con.result_impulse * (con.bpx * diry - con.bpy * dirx);
				world.setMomentum_bef(con.bid, jmom);
				world.setAngularMomentum_bef(con.bid, jamom);
			}*/
			
			if(con.type == ContactModel.Contact.TYPE_BOTH || con.type == ContactModel.Contact.TYPE_NONCOLLISION){
				System.out.printf("Friction : currRelVel : %g %g %g\n", relvelx, relvely, relvelz);
				System.out.printf("\tdir : %g %g %g\n", dirx, diry, dirz);
				System.out.printf("\tcon.result_force : %g\n", con.result_force);
				
				world.getForce(con.aid, amom);
				world.getTorque(con.aid, aamom);
				
				if(con.result_force < 0)
					continue;
				
				double size = mu * con.result_force;
				
				amom.x += size * dirx;
				amom.y += size * diry;
				amom.z += size *  dirz; 
				aamom.x +=  size * (con.apy * dirz - con.apz * diry);
				aamom.y += -size * (con.apx * dirz - con.apz * dirx);
				aamom.z +=  size * (con.apx * diry - con.apy * dirx);
				world.setForce(con.aid, amom);
				world.setTorque(con.aid, aamom);

				world.getForce(con.bid, bmom);
				world.getTorque(con.bid, bamom);
				bmom.x -= size * dirx;
				bmom.y -= size * diry;
				bmom.z -= size * dirz; 
				bamom.x -=  size * (con.bpy * dirz - con.bpz * diry);
				bamom.y -= -size * (con.bpx * dirz - con.bpz * dirx);
				bamom.z -=  size * (con.bpx * diry - con.bpy * dirx);
				world.setForce(con.bid, bmom);
				world.setTorque(con.bid, bamom);
			}
		}
	}// end generateForce()
}
