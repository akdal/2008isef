package agdr.robodari.plugin.rae.rlztion;

import java.util.*;
import javax.vecmath.*;

import agdr.robodari.appl.*;
import agdr.robodari.plugin.rae.edit.RbdrApplication;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.rlztion.*;

public class GravityGenerator implements ForceGenerator{
	public final static float GRAVITY_CONSTANT = 980.00000000000000f;
	public final static GravityGenerator INSTANCE = new GravityGenerator();

	
	public int getGeneratorType()
	{ return ForceGenerator.DEPENDSON_SITUATION; }
	/**
	 * @param model
	 * @param smap
	 * @param world
	 * @return
	 */
	
	private RARealizationModel rarm = null;
	private RbdrApplication appl;
	private RealizationMap rmap = null;
	private World world = null;
	private RealizationMap.Key[] keys = null;
	private Hashtable<SkObject, Boolean> gravity;
	
	public void init(RealizationModel rmodel, RealizationMap rmap, World world){
		this.rarm = (RARealizationModel)rmodel;
		this.rmap = rmap;
		this.appl = this.rarm.getApplication();
		this.world = world;
		this.keys = rmap.getKeys();
		
		this.gravity = new Hashtable<SkObject, Boolean>();
		for(int i = 0; i < keys.length; i++){
			SkObject obj = rmap.getOwner(keys[i]);
			gravity.put(obj, _checkGravity(obj));
		}
	}
	private boolean _checkGravity(SkObject obj){
		BugManager.printf("GravGen : checkGrav : %s\n", obj);
		if(this.appl.isGravityEffectionSpecified(obj)){
			return this.appl.gravityEffected(obj);
		}else{
			SkObject p = this.rmap.getParent(obj);
			BugManager.printf("GravGen : checkGrav : %s : parent : %s\n", obj, p);
			if(p == null)
				return false;
			else
				return _checkGravity(p);
		}
	}// end _checkGravity
	
	public boolean effective(RealizationModel model,
			RealizationMap smap, World world){
		if(!(model instanceof RARealizationModel))
			return false;
		return true;
	}// end effective
	
	public void generateForce(){
		RbdrApplication ra = rarm.getApplication();
		int len = rmap.getModelCount();
		Vector3d force = new Vector3d();
		double mass = 0;
		
		for(int i = 0; i < len; i++){
			Boolean b = gravity.get(rmap.getOwner(keys[i]));
			if(BugManager.DEBUG && World.W_DEBUG)
				BugManager.printf("GravityGen : %d : owner : %s, b : %s\n", i, rmap.getOwner(keys[i]), b);
			
			if(gravity.get(rmap.getOwner(keys[i]))){
				world.getForce(i, force);
				mass = world.getMass(i);
				if(!Double.isInfinite(mass)){
					force.z -= mass * GRAVITY_CONSTANT;
					world.setForce(i, force);
				}
			}
		}
	}// end generateForce
}
