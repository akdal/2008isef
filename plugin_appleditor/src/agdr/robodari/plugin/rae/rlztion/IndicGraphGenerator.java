package agdr.robodari.plugin.rae.rlztion;

import javax.vecmath.*;

import agdr.library.math.MathUtility;
import agdr.robodari.comp.model.*;
import agdr.robodari.plugin.rme.edit.SkObject;
import agdr.robodari.plugin.rme.rlztion.*;
import agdr.robodari.plugin.rae.comp.*;

public class IndicGraphGenerator implements GraphGenerator{
	public final static IndicGraphGenerator INSTANCE = new IndicGraphGenerator();
	
	public int getGeneratorType()
	{ return GraphGenerator.SPECFC_OBJECT; }
	
	public boolean editable(RobotCompModel mainStructure,
			SkObject skObject, RealizationMap map, World world){
		if(skObject == null)
			return false;
		if(skObject.getRobotComponent() == null)
			return false;
		return skObject.getRobotComponent() instanceof Indicator;
	}// end edtiable
	/**
	 */
	public RealizationMap.Key generateGraph(SkObject skObj,
			Matrix4f orgtrans, RealizationMap map, World world){
		Matrix4f trans = new Matrix4f();
		MathUtility.fastMul(orgtrans, skObj.getTransform(), trans);
		
		Indicator indc = (Indicator)skObj.getRobotComponent();
		RealizationMap.Key key = map.addElement(skObj.getModel(), trans, skObj, indc, false);
		indc.setKey(key);
		return key;
	}// end generateGraph
}
