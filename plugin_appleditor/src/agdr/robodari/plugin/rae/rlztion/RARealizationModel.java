package agdr.robodari.plugin.rae.rlztion;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import agdr.robodari.appl.*;
import agdr.robodari.comp.*;
import agdr.robodari.plugin.rae.edit.RbdrApplication;
import agdr.robodari.plugin.rae.edit.RobotObject;
import agdr.robodari.plugin.rae.rapi.*;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.rdpi.*;
import agdr.robodari.plugin.rme.rlztion.*;
import agdr.robodari.plugin.rme.rlztion.RMRealizationModel.*;
import agdr.robodari.project.*;

public class RARealizationModel extends RealizationModel implements RDEntityManager, RDConnManager, RDObjectManager, CommunicationTools{
	private final static String[] NULLSTRARRAY = new String[]{};
	
	private static RDPClassLoader rdpLoader = new RDPClassLoader();
	
	private RbdrApplication appl;
	private Entity[] entities;
	private String[] entityNames;
	private ConnectionModel[] connModels;
	
	private String[] vcNames;
	private VirtualComponent[] virtualComps;
	
	private double coeffOfRestt;
	
	private String[] antNames;
	private Antenna[] antennae;
	
	private boolean isRlzStarted = false;
	private Hashtable<RDObject, Sender[]> senders;
	private Hashtable<RDObject, Receiver[]> receivers;
	
	private Server server;
	private Administrator admin;

	private EventCaster eventCaster = new EventCaster();
	private ArrayList<RDObjectListener> eventListeners = new
			ArrayList<RDObjectListener>();
	
	
	
	public void init(RbdrApplication appl, Project proj) throws RDPLoadException, IllegalOptionException{
		BugManager.printf("RARmodel : init() start\n");
		this.appl = appl;
		
		if(senders == null) senders = new Hashtable<RDObject, Sender[]>();
		else senders.clear();
		
		if(receivers == null) receivers = new Hashtable<RDObject, Receiver[]>();
		else receivers.clear();
		
		this.coeffOfRestt = appl.getCoeffOfRestt();
		
		SkObject[] skObjs = appl.getObjects();
		ArrayList<Entity> entityList = new ArrayList<Entity>();
		ArrayList<String> entityNameList = new ArrayList<String>();
		ArrayList<ConnectionModel> connModelList = new ArrayList<ConnectionModel>();
		
		ArrayList<String> vcnamelist = new ArrayList<String>();
		ArrayList<VirtualComponent> vclist = new ArrayList<VirtualComponent>();
		
		ArrayList<String> antnamelist = new ArrayList<String>();
		ArrayList<Antenna> antlist = new ArrayList<Antenna>();
		InputStream is = null;
		
		for(int i = 0; i < skObjs.length; i++){
			if(skObjs[i] instanceof RobotObject){
				RobotObject ro = (RobotObject)skObjs[i];
				RobotModel model = ro.getRobotModel();
				ConnectionModel cmodel = model.getConnectionModel();
				try{
					BugManager.printf("RARModel : init : recognized robot model : %s\n", ro.getRobotName());
					Entity ent = new Entity(this, ro.getRobotName(), model, proj,
							vcnamelist, vclist,
							antnamelist, antlist);
					
					connModelList.add(cmodel);
					entityNameList.add(ro.getRobotName());
					entityList.add(ent);
					
					for(int j = 0; j < ent.rdObjNames.length; j++){
						RDObject rdo = ent.rdObjTable.get(ent.rdObjNames[j]);
						if(!(rdo instanceof RobotComponent))
							continue;
						
						RobotComponent rc = (RobotComponent)rdo;
						if(rc.getSenders() != null)
							this.senders.put(rdo, rc.getSenders());
						if(rc.getReceivers() != null)
							this.receivers.put(rdo, rc.getReceivers());
					}
				}catch(IOException ioe){
					throw new RDPLoadException(ro.getRobotName(), RDPLoadException.CASE_STREAMING, ioe);
				}catch(NoClassDefFoundError cnfe){
					throw new RDPLoadException(ro.getRobotName(), RDPLoadException.CASE_INSTANTIATING, cnfe);
				}catch(IllegalAccessException iae){
					throw new RDPLoadException(ro.getRobotName(), RDPLoadException.CASE_INSTANTIATING, iae);
				}catch(InstantiationException ie){
					throw new RDPLoadException(ro.getRobotName(), RDPLoadException.CASE_INSTANTIATING, ie);
				}catch(ClassFormatError cfe){
					throw new RDPLoadException(ro.getRobotName(), RDPLoadException.CASE_INSTANTIATING, cfe);
				}
			}
		}
		
		this.entityNames = entityNameList.toArray(NULLSTRARRAY);
		this.entities = entityList.toArray(new Entity[]{});
		this.connModels = connModelList.toArray(new ConnectionModel[]{});
		
		this.antNames = antnamelist.toArray(NULLSTRARRAY);
		this.antennae = antlist.toArray(new Antenna[]{});
		
		this.vcNames = vcnamelist.toArray(NULLSTRARRAY);
		this.virtualComps = vclist.toArray(new VirtualComponent[]{});
		
		RealizationMap smap = new RealizationMap();
		
		ContactForceGenerator cfg = new ContactForceGenerator();
		smap.addForceGenerator(new GravityGenerator());
		smap.addForceGenerator(cfg);
		//smap.addForceGenerator(new FrictionForceGenerator(cfg));
		smap.load(this.appl.getObjects());
		
		super.init(smap);

		// server and administrators : 
		this.server = (Server)_loadObject(appl.getServerSrc(), appl.getServerClassName(), new Object[0]);
		BugManager.printf("RARModel : init : server src : %s, this.server : %s\n", appl.getServerSrc(), this.server);
		if(this.server != null)
			this.server.initServer(this);
		
		this.admin = (Administrator)_loadObject(appl.getAdministratorSrc(), appl.getAdminClassName(), new Object[0]);
		BugManager.printf("RARModel : init : admin src : %s, this.admin : %s\n", appl.getAdministratorSrc(), this.admin);
		if(this.admin != null)
			this.admin.init(this);
		System.gc();
	}// end load
	
	private Object _loadObject(java.io.File src, String name, Object[] args){
		if(src == null)
			return null;
		try{
			Class c = Class.forName(name);
			
			Class[] argcls = new Class[args.length];
			for(int i = 0; i < args.length; i++)
				argcls[i] = args[i] == null ? null : args[i].getClass();
			return c.getConstructor(argcls).newInstance(args);
		}catch(Exception excp){
			try{
				InputStream is = new FileInputStream(src);
				_TmpClassLoader tcl = new _TmpClassLoader();
				return tcl.loadObject(is, name, args);
			}catch(Exception e){
				BugManager.log(excp, true);
			}
		}
		return null;
	}// end _loadServer
	
	
	
	
	
	
	/* *****************************************
	 *         COMMUNICATIOn TOOLS
	 ***********************************************/
	public int getAntennaCount()
	{ return antNames.length; }
	public String getAntennaName(int idx)
	{ return antNames[idx]; }
	public void send(String ant, Object data){
		if(ant == null) return;
		
		for(int i = 0; i < antNames.length; i++){
			if(ant.equals(antennae[i].getName()))
				antennae[i].receive(data);
		}
	}// end send
	
	
	
	/* **********************************************
	 *            RDConnManager
	 ***********************************************/

	public boolean hasReceiver(RDObject rdo)
	{ return receivers.containsKey(rdo); }
	public boolean hasSender(RDObject rdo)
	{ return senders.containsKey(rdo); }
	
	public Receiver[] getReceivers(RDObject obj)
	{ return receivers.get(obj); }
	public Sender[] getSenders(RDObject obj)
	{ return senders.get(obj); }
	
	public RDObject getConn(Object obj){
		RobotComponent rc;
		
		for(int i = 0; i < connModels.length; i++){
			rc = null;
			if(obj instanceof Sender){
				Receiver rcv = connModels[i].getReceiver((Sender)obj);
				if(rcv != null)
					rc = connModels[i].getOwner(rcv).getRobotComponent();
			}
			if(rc == null && obj instanceof Receiver){
				Sender sd = connModels[i].getSender((Receiver)obj);
				if(sd != null)
					rc = connModels[i].getOwner(sd).getRobotComponent();
			}
			
			if(rc instanceof RDObject)
				return (RDObject)rc;
		}
		return null;
	}// end getConn
	
	
	/* **********************************************
	 *   EntityManager implementation
	 **************************************************/
	
	public void disposeEntities(){
		if(server != null)
			this.server.destroyServer();
		if(admin != null)
			this.admin.destroy();

		if(entities != null)
			try{
				for(Entity ent : entities)
					ent.disposeProcessors();
			}catch(Exception e){
				agdr.robodari.appl.BugManager.log(e, true);
			}
	}// end finishProcessors()
	public boolean isEntRunning(){
		if(server != null && server.isRunning())
			return true;
		else if(admin != null && admin.isRunning())
			return true;
		
		if(entities != null)
			for(int i = 0; i < entities.length; i++){
				if(entities[i].isRunning())
					return true;
			}
		return false;
	}// end isRunning()
	public void startEntities(){
		BugManager.printf("RARModel : startEntities() : admin : %s\n", admin);
		if(entities != null)
			try{
				for(Entity eache : entities)
					eache.startProcessors();
			}catch(Exception e){
				agdr.robodari.appl.BugManager.log(e, true);
			}

		if(server != null)
			server.startServer();
		if(admin != null){
			admin.start();
		}
	}// end startProcessors()
	public void pauseEntities(){
		try{
			if(entities != null)
				for(Entity eache : entities)
					eache.pauseProcessors();
		}catch(Exception e){
			agdr.robodari.appl.BugManager.log(e, true);
		}
	}// end startProcessors()
	public void resumeEntities(){
		try{
			for(Entity eache : entities)
				eache.resumeProcessors();
		}catch(Exception e){
			agdr.robodari.appl.BugManager.log(e, true);
		}
	}// end startProcessors()
	
	public RDEntity getEntity(String name){
		if(name == null)
			return null;
		for(Entity eache : entities)
			if(name.equals(eache.getEntityName()))
				return eache;
		return null;
	}// end getEntity
	public String[] getRDEntityNames()
	{ return this.entityNames; }
	
	
	
	/* *********************************************
	 * RDObjMngr implementation
	 ****************************************************/
	public RDObject getRDObject(RDEntity proc, String name){
		Entity ent = (Entity)proc;
		return ent.rdObjTable.get(name);
	}// end getRDObject
	public boolean exists(RDEntity proc, String name){
		Entity ent = (Entity)proc;
		return ent.rdObjTable.containsKey(name);
	}// end exists(String[])
	public String[] getRDObjectNames(RDEntity proc)
	{ return ((Entity)proc).rdObjNames; }
	


	public Object runMethod(RDEntity source,
			RDObject object, String methodName, Object[] args)
			throws RealizationNotReadyException, RDMethodNotExistException{
		if(!isRlzStarted)
			return null;
		
		int idx = RDPUtility.getRDMethodIndex(object, methodName, args);

		if(idx == -1){
			throw new RDMethodNotExistException("",
					object, methodName, args);
		}

		// event .
		Class[] argclasses = new Class[args.length];
		for(int i = 0; i < args.length; i++)
			argclasses[i] = (args[i] == null) ? null : args[i].getClass();
		
		Object value = object.callRDMethod(this, methodName, args);
		fireMethodCalled(new RDObjectEvent(source, object, RDObjectEvent.TYPE_METHOD, idx,
				args, argclasses));
		//rdobjevListModels.get(this.ctrlerComboBox.getSelectedIndex()).addElement(event);
		
		return value;
	}// end getMethodValue(String, Object[])
	
	public Object getFieldValue(RDEntity source, RDObject obj, String fieldName)
			throws RealizationNotReadyException, RDFieldNotExistException{
		if(!isRlzStarted)
			return null;

		int idx = RDPUtility.getRDFieldIndex(obj, fieldName);
		if(idx == -1)
			throw new RDFieldNotExistException("Field not found", obj, fieldName);
		
		Object fieldval = obj.getRDFieldValue(this, fieldName);
		fireFieldAccessed(new RDObjectEvent(source, obj,
				RDObjectEvent.TYPE_FIELD_ACCESS,
				idx,
				new Object[]{ fieldval },
				new Class[]{ fieldval == null ? null : fieldval.getClass() }));
		
		return fieldval;
	}// end getFieldValue(String, String)
	
	public void setFieldValue(RDEntity source, RDObject obj, String fieldName,
			Object value) throws RealizationNotReadyException, RDFieldNotExistException{
		if(!isRlzStarted)
			return;
		
		String[] names = obj.getRDFieldNames();
		RDObject.Type[] types = obj.getRDFieldTypes();
		int idx = RDPUtility.getRDFieldIndex(obj, fieldName);
		if(idx == -1)
			throw new RDFieldNotExistException("Field not found", obj, fieldName);
		
		// event .
		Object[] values = new Object[]{ value };
		Class[] valclasses = new Class[]{ null };
		if(value != null)
			valclasses[0] = value.getClass();
		RDObjectEvent event = new RDObjectEvent(source, obj, RDObjectEvent.TYPE_FIELD_WRITTEN,
				idx, values, valclasses);
		
		obj.setRDFieldValue(this, fieldName, value);
		fireFieldWritten(event);
	}// end setFieldValue(RoboDariProcessor, String, String, Object)
	
	

	
	/* ************************************************
	 *                Overrides
	 **************************************************/

	public void startRealization() throws RealizationStartedException, RealizationNotReadyException{
		BugManager.printf("RARModel : startRealization()\n");
		this.isRlzStarted = true;
		this.eventCaster.start();
		this.startEntities();
		super.startRealization();
	}// end startRealization()
	
	public void resumeRealization() throws RealizationNotReadyException{
		super.resumeRealization();
		this.resumeEntities();
	}// end resumeRealization
	
	public void pauseRealization() throws RealizationNotReadyException{
		super.pauseRealization();
		this.pauseEntities();
	}// end pauseRlz
	
	public void disposeRealization() throws RealizationNotReadyException{
		this.eventCaster.stop();
		this.disposeEntities();
		while(this.isEntRunning()){
			try{
				Thread.sleep(50);
			}catch(Throwable t){}
		}
		super.disposeRealization();
		this.isRlzStarted = false;
	}// end finishRealization
	
	public boolean isRlzStarted()
	{ return isRlzStarted; }
	
	
	
	
	
	
	
	
	
	/* *********************************************
	 * Others
	 *******************************************/
	public RbdrApplication getApplication()
	{ return appl; }
	
	public Server getServer()
	{ return this.server; }
	public Administrator getAdministrator()
	{ return this.admin; }
	
	public double getCoeffOfRestt()
	{ return this.coeffOfRestt; }

	
	
	public void addRDObjectListener(RDObjectListener rdol)
	{ eventListeners.add(rdol); }
	public void removeRDObjectListener(RDObjectListener l)
	{ eventListeners.add(l); }
	public EventCaster getEventCaster()
	{ return this.eventCaster; }

	
	protected void fireMethodCalled(RDObjectEvent rdoe)
	{ eventCaster.fire(new RMRealizationModel.RDCast(rdoe, eventListeners, RDCast.METHOD_CALLED)); }
	protected void fireFieldAccessed(RDObjectEvent rdoe)
	{ eventCaster.fire(new RMRealizationModel.RDCast(rdoe, eventListeners, RDCast.FIELD_ACCESSED)); }
	protected void fireFieldWritten(RDObjectEvent rdoe)
	{ eventCaster.fire(new RDCast(rdoe, eventListeners, RDCast.FIELD_WRITTEN)); }
	

	
	
	
	
	
	
	
	
	static class _TmpClassLoader extends ClassLoader{
		public Object loadObject(InputStream is, String name, Object[] args)
				throws IOException, ClassFormatError, InstantiationException, IllegalAccessException,
				NoSuchMethodException, NoClassDefFoundError, java.lang.reflect.InvocationTargetException{
			byte[] data = new byte[is.available()];
			is.read(data, 0, data.length);
			Class cls = null;
			
			if((cls = super.findLoadedClass(name)) == null){
				cls = super.defineClass(name, data, 0, data.length);
			}
			
			Class[] argcls = new Class[args.length];
			for(int i = 0; i < args.length; i++)
				argcls[i] = args[i] == null ? null : args[i].getClass();
			return cls.getConstructor(argcls).newInstance(args);
		}// end loadRDProcessor(File)
	}// end class RDPClassLoader
	
	
	static class Entity implements RDEntity{
		private String entName;
		private String[] ctrlerNames;
		private String starter;
		private Hashtable<String, RDProcessor> processors;
		private RARealizationModel rarModel;
		
		public String[] rdObjNames;
		public Hashtable<String, RDObject> rdObjTable;
		
		public Entity(RARealizationModel rarModel,
				String entName, RobotModel model, Project proj,
				ArrayList<String> vcnamelist,
				ArrayList<VirtualComponent> vclist,
				
				ArrayList<String> antnamelist,
				ArrayList<Antenna> antlist)
				throws InstantiationException, ClassFormatError,
					NoClassDefFoundError,
					IOException, IllegalAccessException{
			this.rarModel = rarModel;
			this.entName = entName; 
			
			this.rdObjTable = new Hashtable<String, RDObject>();
			this.processors = new Hashtable<String, RDProcessor>();
			RobotComponent[] comps = model.getSketchModel().getRobotComponents();
			
			if(model.getStarter() != null)
				starter = model.getStarter().getName();
			else
				starter = null;
			ArrayList<String> ctrlerNameList = new ArrayList<String>();
			ArrayList<String> rdObjNameList = new ArrayList<String>();
			InputStream is = null;
			
			for(int j = 0; j < comps.length; j++){
				if(comps[j] instanceof RDObject){
					RDObject obj = (RDObject)comps[j];
					rdObjNameList.add(obj.getName());
					rdObjTable.put(obj.getName(), obj);
				}
				if(comps[j] instanceof SourceEditable){
					String name = comps[j].getName();
					SourceEditable ctrler = (SourceEditable)comps[j];
					
					ctrlerNameList.add(name);
					
					// Loading Class from class file...
					int type = model.getSourceType(ctrler);
					
					if(type == RobotModel.STYPE_NULL)
						continue;
					else if(type == RobotModel.STYPE_FILE){
						is = new FileInputStream(model.getSourceFile(ctrler));
					}else if(type == RobotModel.STYPE_PRJFILE){
						String[] path = model.getSourcePrjFilePath(ctrler);
						ProjectItem focus = proj;
						for(int k = 0; k < path.length; k++){
							for(int l = 0; l < focus.getChildCount(); l++){
								if(focus.getChildAt(l).toString().equals(path[k])){
									focus = (ProjectItem)focus.getChildAt(l);
									l = 2147483647;
									continue;
								}
							}
						}
						if(!(focus instanceof ProjectFile))
							continue;

						is = new FileInputStream(((ProjectFile)focus).getTotalPath());
					}else if(type == RobotModel.STYPE_URL){
						is = model.getSourceURL(ctrler).openStream();
					}
					
					processors.put(name, rdpLoader.loadRDProcessor(is, model.getSourceName(ctrler)));
				
					is.close();
				}
				if(comps[j] instanceof VirtualComponent){
					vcnamelist.add(comps[j].getName());
					vclist.add((VirtualComponent)comps[j]);
				}
				if(comps[j] instanceof Antenna){
					antnamelist.add(comps[j].getName());
					antlist.add((Antenna)comps[j]);
				}
			}
			this.rdObjNames = rdObjNameList.toArray(new String[]{});
			this.ctrlerNames = ctrlerNameList.toArray(new String[]{});
		}// end Constructor
		
		
		
		
		public String getEntityName()
		{ return entName; }
		
		public RDProcessor getProcessor(String prc)
		{ return this.processors.get(prc); }
		public String[] getCtrlerNames()
		{ return ctrlerNames; }
		public boolean isStarter(String ctrlName)
		{ return starter.equals(ctrlName); }
		
		public void resetProcessors(){
			final RDEntity _thisptr = this;
			final RARealizationModel _rarmptr = this.rarModel;
			
			for(int i = 0; i < ctrlerNames.length; i++){
				RDProcessor rdp = this.processors.get(ctrlerNames[i]);
				if(rdp == null) continue;
				rdp.dispose(this, this.rarModel, this.rarModel, ctrlerNames[i]);
			}
			
			while(this.isRunning()){
				try{ Thread.sleep(30); }catch(Exception e){}
			}
			
			for (int i = 0; i < ctrlerNames.length; i++) {
				final String name = ctrlerNames[i];
				final RDProcessor proc = getProcessor(name);
				if(proc == null)
					continue;
				
				Thread thr = new Thread(new Runnable() {
					public void run() {
						try{
							getProcessor(name).start(_thisptr, _rarmptr, _rarmptr, name,
											null, _rarmptr, _rarmptr.getRDOut(), _rarmptr.getRDErr());
						}catch(Exception excp){
							agdr.robodari.plugin.rme.appl.ExceptionHandler
									.rdpi_unknownErrorOccured(name, excp);
						}
					}
				});
				thr.start();
			}

			try{
				for(String rdobjName : this.rdObjNames)
					this.rdObjTable.get(rdobjName).resetRlztion();
			}catch(Exception e){
				agdr.robodari.appl.BugManager.log(e, true);
			}
		}// end resetProcessors()
		public void startProcessors(){
			final RDEntity _thisptr = this;
			final RARealizationModel _rarmptr = this.rarModel;
			
			try{
				for(String rdobjName : this.rdObjNames)
					this.rdObjTable.get(rdobjName).initRlztion(_rarmptr);
			}catch(Exception e){
				agdr.robodari.appl.BugManager.log(e, true);
			}
			
			for (int i = 0; i < ctrlerNames.length; i++) {
				final String name = ctrlerNames[i];
				final RDProcessor proc = getProcessor(name);
				BugManager.printf("RARModel.Entity : startRlztion : ctrlerNames[%d] : %s, proc : %s\n", i, ctrlerNames[i], proc);
				if(proc == null)
					continue;
				
				Thread thr = new Thread(new Runnable() {
					public void run() {
						try{
							getProcessor(name).start(_thisptr, _rarmptr, _rarmptr, name,
											null, _rarmptr, _rarmptr.getRDOut(), _rarmptr.getRDErr());
						}catch(Exception excp){
							agdr.robodari.plugin.rme.appl.ExceptionHandler
									.rdpi_unknownErrorOccured(name, excp);
						}
					}
				});
				thr.start();
			}
		}// end startProcessors()
		public void pauseProcessors(){
			for(int i = 0; i < ctrlerNames.length; i++){
				RDProcessor rdp = this.processors.get(ctrlerNames[i]);
				if(rdp == null) continue;
				rdp.pause(this, this.rarModel, this.rarModel, ctrlerNames[i]);
			}
		}// end finishProcessors
		public void resumeProcessors(){
			for(int i = 0; i < ctrlerNames.length; i++){
				RDProcessor rdp = this.processors.get(ctrlerNames[i]);
				if(rdp == null) continue;
				rdp.resume(this, this.rarModel, this.rarModel, ctrlerNames[i]);
			}
		}// end finishProcessors
		public void disposeProcessors(){
			for(int i = 0; i < ctrlerNames.length; i++){
				RDProcessor rdp = this.processors.get(ctrlerNames[i]);
				if(rdp == null) continue;
				rdp.dispose(this, this.rarModel, this.rarModel, ctrlerNames[i]);
			}

			try{
				for(String rdobjName : this.rdObjNames){
					RDObject obj = this.rdObjTable.get(rdobjName);
					obj.disposeRlztion();
				}
			}catch(RealizationNotReadyException rnre){
			}catch(Exception e){
				agdr.robodari.appl.BugManager.log(e, true);
			}
		}// end finishProcessors
		
		public boolean isRunning(){
			for(int i = 0; i < ctrlerNames.length; i++){
				RDProcessor rdp = this.processors.get(ctrlerNames[i]);
				if(rdp == null) continue;
				if(rdp.isRunning(this, this.rarModel, this.rarModel, ctrlerNames[i]))
					return true;
			}
			return false;
		}// end isRunning
	}// end class Entity
}
