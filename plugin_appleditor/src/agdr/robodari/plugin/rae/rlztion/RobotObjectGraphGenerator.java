package agdr.robodari.plugin.rae.rlztion;

import javax.vecmath.Matrix4f;

import agdr.robodari.comp.*;
import agdr.robodari.comp.model.RobotCompModel;
import agdr.robodari.plugin.rae.edit.*;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.rlztion.*;
import agdr.robodari.plugin.rme.store.RealizationCompStore;

public class RobotObjectGraphGenerator implements GraphGenerator{
	public final static RobotObjectGraphGenerator INSTANCE = new RobotObjectGraphGenerator();
	
	
	public int getGeneratorType()
	{ return GraphGenerator.SPECFC_OBJECT; }
	public boolean editable(RobotCompModel str, SkObject skobj, RealizationMap smap, World world){
		return skobj instanceof RobotObject;
	}// end editable(Structure, RobotComponent, Skobject, Structuremap, World)
	public RealizationMap.Key generateGraph(SkObject obj, Matrix4f trans, RealizationMap map, World world){
		RobotObject robj = (RobotObject)obj;
		
		Matrix4f robjtrans = new Matrix4f();
		if(trans != null)
			// parent already specified transform matrix
			robjtrans.set(trans);
		else
			robjtrans.m00 = robjtrans.m11 = robjtrans.m22 = robjtrans.m33 = 1;
		robjtrans.mul(robj.getTransform());

		SkObject[] children = robj.getChildren();
		RealizationMap.Key key = null;
		
		for(int i = 0; i < children.length; i++){
			Matrix4f transi = new Matrix4f();
			transi.set(robjtrans);
			
			if(key == null)
				key = RealizationCompStore.generateGraph(
						children[i].getModel(),
						children[i], 
						transi,
						map, world);
			else
				RealizationCompStore.generateGraph(
					children[i].getModel(),
					children[i], 
					transi,
					map, world);
		}
		return key;
	}// end generateGraph()
}
