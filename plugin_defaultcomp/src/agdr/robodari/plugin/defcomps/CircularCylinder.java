package agdr.robodari.plugin.defcomps;

import agdr.robodari.comp.*;

public class CircularCylinder extends StructureComponent{
	public final static float DEFAULT_HEIGHT = 3;
	public final static float DEFAULT_RADIUS = 2;
	public final static float DEFAULT_MASS = 10;
	
	private float height = DEFAULT_HEIGHT;
	private float radius = DEFAULT_RADIUS;
	private float mass = DEFAULT_MASS;
	
	public float getHeight()
	{ return height; }
	public float getRadius()
	{ return radius; }
	public void setHeight(float height)
	{ this.height = height; }
	public void setRadius(float r)
	{ this.radius = r; }
	
	public float getMass()
	{ return mass; }
	public void setMass(float d)
	{ this.mass = d; }
	
	public boolean hasSubComponent()
	{ return false; }
	public RobotComponent[] getSubComponents()
	{ return null; }
}
