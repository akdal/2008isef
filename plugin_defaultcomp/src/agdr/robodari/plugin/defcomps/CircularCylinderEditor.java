package agdr.robodari.plugin.defcomps;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Frame;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;


import agdr.robodari.appl.BugManager;
import agdr.robodari.comp.Note;
import agdr.robodari.gui.NoteEditor;
import agdr.robodari.plugin.rme.edit.SkObject;
import agdr.robodari.plugin.rme.edit.SketchEditorInterface;
import agdr.robodari.plugin.rme.gui.*;

public class CircularCylinderEditor extends CompEditor{
	private JTabbedPane tabbedPane;
	
	private JPanel propertyPanel;
	private NoteEditor noteEditor;
	private JTextField nameField;
	private JTextField radiusField;
	private JTextField massField;
	private JTextField heightField;
	
	private SkObject object;
	

	public CircularCylinderEditor(SketchEditorInterface manager, Dialog parent){
		super(manager, parent, "Cylinder Editor");
		__set();
	}// end Constructor(EditorManager)
	public CircularCylinderEditor(SketchEditorInterface manager, Frame parent){
		super(manager, parent, "Cylinder Editor");
		__set();
	}// end Constructor(EditorManager)
	private void __set(){
		initPropertyPanel();
		tabbedPane = new JTabbedPane();
		tabbedPane.setTabPlacement(JTabbedPane.LEFT);
		tabbedPane.addTab("Properties", propertyPanel);
		
		super.getEditorPanel().setLayout(new BorderLayout());
		super.getEditorPanel().add(tabbedPane, BorderLayout.CENTER);
		
		super.setSize(480, 430);
	}// end __set
	private void initPropertyPanel(){
		propertyPanel = new JPanel();
		propertyPanel.setLayout(null);
		
		nameField = new JTextField();
		JLabel nameLabel = new JLabel("Component name : ");
		nameLabel.setBounds(20, 20, 100, 25);
		nameField.setBounds(130, 20, 200, 25);
		
		heightField = new JTextField();
		radiusField = new JTextField();
		massField = new JTextField();
		JLabel heightLabel = new JLabel("Height(cm) : ");
		JLabel radiusLabel = new JLabel("Radius(cm) : ");
		JLabel massLabel = new JLabel("Mass(g) : ");
		
		heightLabel.setHorizontalAlignment(JLabel.RIGHT);
		radiusLabel.setHorizontalAlignment(JLabel.RIGHT);
		massLabel.setHorizontalAlignment(JLabel.RIGHT);
		heightLabel.setBounds(20, 55, 100, 25);
		radiusLabel.setBounds(20, 90, 100, 25);
		heightField.setBounds(130, 55, 70, 25);
		radiusField.setBounds(130, 90, 70, 25);
		massLabel.setBounds(210, 55, 80, 25);
		massField.setBounds(300, 55, 50, 25);
		
		noteEditor = new NoteEditor();
		noteEditor.setBounds(20, 120, 350, 210);
		
		propertyPanel.add(nameLabel);
		propertyPanel.add(nameField);
		
		propertyPanel.add(heightField);
		propertyPanel.add(radiusField);
		propertyPanel.add(heightLabel);
		propertyPanel.add(radiusLabel);
		propertyPanel.add(massField);
		propertyPanel.add(massLabel);
		
		propertyPanel.add(noteEditor);
	}// end initPropertyPanel()
	
	
	public void startEditing(SkObject object){
		if(!(object.getRobotComponent() instanceof CircularCylinder))
			return;
		
		this.object = object;

		CircularCylinder rcomp = (CircularCylinder)object.getRobotComponent();
		nameField.setText(rcomp.getName());
		if(rcomp.getNote() == null)
			rcomp.setNote(new Note());
		heightField.setText(String.valueOf(rcomp.getHeight()));
		radiusField.setText(String.valueOf(rcomp.getRadius()));
		massField.setText(String.valueOf(rcomp.getMass()));
		
		noteEditor.open(rcomp.getNote());
	}// end startEditing(SkObject)
	public void finishEditing(){
		CircularCylinder rcomp = (CircularCylinder)object.getRobotComponent();
		rcomp.setNote(noteEditor.getNote());
		rcomp.setName(nameField.getText());
		
		try{
			float height = Float.parseFloat(heightField.getText());
			rcomp.setHeight(height);
		}catch(Exception excp){}
		try{
			float radius = Float.parseFloat(radiusField.getText());
			rcomp.setRadius(radius);
		}catch(Exception excp){}
		try{
			float mass = Float.parseFloat(massField.getText());
			rcomp.setMass(mass);
		}catch(Exception excp){}
		
		SkObject skobj = super.getInterface().getModel().getObject(rcomp);
		try{
			skobj.refresh();
		}catch(Exception excp){
			BugManager.log(excp, true);
		}
		super.getInterface().refreshObject(skobj);
		
		this.object = null;
		this.setVisible(false);
		super.getInterface().setCurrentCompEditor(null);
	}// end finishEditing()
	public SkObject getObject()
	{ return object; }
}
