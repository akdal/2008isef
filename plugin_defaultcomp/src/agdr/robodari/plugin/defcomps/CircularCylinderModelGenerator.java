package agdr.robodari.plugin.defcomps;

import javax.vecmath.*;

import agdr.robodari.comp.model.*;

public class CircularCylinderModelGenerator implements ModelGenerator<CircularCylinder>{
	public RobotCompModel generate(CircularCylinder cc) throws Exception{
		StringBuffer buf = new StringBuffer();
		
		for(int i = 0; i < 20; i++){
			double cos = Math.cos((2.0 * Math.PI / 20.0) * (double)i);
			double sin = Math.sin((2.0 * Math.PI / 20.0) * (double)i);
			buf.append("v " + (cos * cc.getRadius()) + " " + (sin * cc.getRadius()) + " 0\n");
		}
		for(int i = 0; i < 20; i++){
			double cos = Math.cos((2.0 * Math.PI / 20.0) * (double)i);
			double sin = Math.sin((2.0 * Math.PI / 20.0) * (double)i);
			buf.append("v " + (cos * cc.getRadius()) + " " + (sin * cc.getRadius()) + " " + cc.getHeight() + "\n");
		}
		buf.append("f 1 20 19 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1\n");
		buf.append("f 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40\n");
		buf.append("f 1 2 22 21\n");
		buf.append("f 2 3 23 22\n");
		buf.append("f 3 4 24 23\n");
		buf.append("f 4 5 25 24\n");
		buf.append("f 5 6 26 25\n");
		buf.append("f 6 7 27 26\n");
		buf.append("f 7 8 28 27\n");
		buf.append("f 8 9 29 28\n");
		buf.append("f 9 10 30 29\n");
		buf.append("f 10 11 31 30\n");
		buf.append("f 11 12 32 31\n");
		buf.append("f 12 13 33 32\n");
		buf.append("f 13 14 34 33\n");
		buf.append("f 14 15 35 34\n");
		buf.append("f 15 16 36 35\n");
		buf.append("f 16 17 37 36\n");
		buf.append("f 17 18 38 37\n");
		buf.append("f 18 19 39 38\n");
		buf.append("f 19 20 40 39\n");
		buf.append("f 20 1 21 40\n");
		
		RobotCompModel rcm = new RobotCompModel();
		rcm.load(new java.io.StringReader(buf.toString()));
		
		PhysicalData pd = rcm.getPhysicalData();
		pd.mass = cc.getMass();
		pd.volume = (float)Math.PI * cc.getRadius() * cc.getRadius() * cc.getHeight();
		pd.inertia = new Matrix3f(
			(pd.mass * cc.getRadius() * cc.getRadius() / 4.0f) + (pd.mass * cc.getHeight() * cc.getHeight() / 12.0f), 0, 0,
			0, (pd.mass * cc.getRadius() * cc.getRadius() / 4.0f) + (pd.mass * cc.getHeight() * cc.getHeight() / 12.0f), 0,
			0, 0, pd.mass * cc.getRadius() * cc.getRadius() / 2.0f
		);
		pd.centerMassPoint = new Point3f(0, 0, cc.getHeight() / 2.0f);
		BoundObject.Cylinder cyl = new BoundObject.Cylinder(0, 0, 0, 0, 0, (float)cc.getHeight(), (float)cc.getRadius());
		pd.bounds = cyl;
		
		AppearanceData ad = rcm.getApprData();
		ad.setColor(java.awt.Color.gray);
		
		return rcm;
	}// end generateModel(CircularCylinder)
}
