package agdr.robodari.plugin.defcomps;


import agdr.robodari.comp.*;
import agdr.robodari.plugin.defcomps.info.*;
import agdr.robodari.plugin.defcomps.rlztion.*;
import agdr.robodari.plugin.rme.store.*;
import agdr.robodari.store.*;

public class DefaultComponentInitializer extends Initializer{
	public void initialize() throws Exception{
		RobotComponentStore.registerComponent(LED1.class);
		RobotComponentStore.registerComponent(ElectricPanel.class);
		RobotComponentStore.registerComponent(Transistor1.class);
		RobotComponentStore.registerComponent(OnePin.class);
		RobotComponentStore.registerComponent(SenderSerial.class);
		RobotComponentStore.registerComponent(ThreePin.class);
		RobotComponentStore.registerComponent(PowerConnector1.class);
		RobotComponentStore.registerComponent(RoundedRectanglePane.class);
		RobotComponentStore.registerComponent(Sphere.class);
		RobotComponentStore.registerComponent(RectanglePane.class);
		RobotComponentStore.registerComponent(Stage.class);
		RobotComponentStore.registerComponent(CircularCylinder.class);
		RobotComponentStore.registerComponent(Hinge.class);
		RobotComponentStore.registerComponent(GenericDCMotor.class);
		RobotComponentStore.registerComponent(GenericReceiver.class);
		RobotComponentStore.registerComponent(GenericSimplexAntenna.class);
		RobotComponentStore.registerComponent(GenericController.class);
		
		
		// infos
		RobotInfoStore.registerInfo(ElectricPanel.class, new ElectricPanelInfoGenerator());
		RobotInfoStore.registerInfo(LED1.class, new LED1InfoGenerator());
		RobotInfoStore.registerInfo(RoundedRectanglePane.class, new RoundedPlasticPaneInfoGenerator());
		RobotInfoStore.registerInfo(Sphere.class, new SphereInfoGenerator());
		RobotInfoStore.registerInfo(RectanglePane.class, new RectanglePaneInfoGenerator());
		RobotInfoStore.registerInfo(Stage.class, new StageInfoGenerator());
		RobotInfoStore.registerInfo(CircularCylinder.class, new CircularCylinderInfoGenerator());
		RobotInfoStore.registerInfo(Hinge.class, new HingeInfoGenerator());
		RobotInfoStore.registerInfo(GenericDCMotor.class, new GenericDCMotorInfoGenerator());
		RobotInfoStore.registerInfo(GenericSimplexAntenna.class, new GenericSimplexAntennaInfoGenerator());
		RobotInfoStore.registerInfo(GenericController.class, new GenericControllerInfoGenerator());
		
		// models
		RobotModelStore.registerModel(LED1.class,
				new LED1ModelGenerator());
		RobotModelStore.registerModel(ElectricPanel.class,
				new ElectricPanelModelGenerator());
		RobotModelStore.registerModel(Transistor1.class,
				new Transistor1ModelGenerator());
		RobotModelStore.registerModel(SenderSerial.class,
				new SenderSerialModelGenerator());
		RobotModelStore.registerModel(ThreePin.class,
				new ThreePinModelGenerator());
		RobotModelStore.registerModel(OnePin.class,
				new OnePinModelGenerator());
		RobotModelStore.registerModel(PowerConnector1.class,
				new PowerConnector1ModelGenerator());
		RobotModelStore.registerModel(RoundedRectanglePane.class,
				new RoundedRectanglePaneModelGenerator());
		RobotModelStore.registerModel(Sphere.class,
				new SphereModelGenerator());
		RobotModelStore.registerModel(RectanglePane.class,
				new RectanglePaneModelGenerator());
		RobotModelStore.registerModel(Stage.class,
				new StageModelGenerator());
		RobotModelStore.registerModel(CircularCylinder.class,
				new CircularCylinderModelGenerator());
		RobotModelStore.registerModel(Hinge.class,
				new HingeModelGenerator());
		RobotModelStore.registerModel(GenericDCMotor.class,
				new GenericDCMotorModelGenerator());
		RobotModelStore.registerModel(GenericSimplexAntenna.class,
				new GenericSimplexAntennaModelGenerator());
		RobotModelStore.registerModel(GenericController.class,
				new GenericControllerModelGenerator());
		
		// comp editors
		RobotCompEditorStore.register(RoundedRectanglePane.class,
				RoundedRectanglePaneEditor.class);
		RobotCompEditorStore.register(Sphere.class,
				SphereEditor.class);
		RobotCompEditorStore.register(RectanglePane.class,
				RectanglePaneEditor.class);
		RobotCompEditorStore.register(Stage.class,
				StageEditor.class);
		RobotCompEditorStore.register(CircularCylinder.class,
				CircularCylinderEditor.class);
		RobotCompEditorStore.register(Hinge.class,
				HingeEditor.class);
		RobotCompEditorStore.register(GenericDCMotor.class,
				GenericDCMotorEditor.class);
		RobotCompEditorStore.register(GenericController.class,
				GenericControllerEditor.class);
		
		// rlztion
		RealizationCompStore.registerGraphGenerator(
				GroupObjectGraphGenerator.INSTANCE);
		RealizationCompStore.registerGraphGenerator(
				OneItemGraphGenerator.INSTANCE);
		RealizationCompStore.registerGraphGenerator(
				SenderSerialGraphGenerator.INSTANCE);
		RealizationCompStore.registerGraphGenerator(
				HingeGraphGenerator.INSTANCE);
		RealizationCompStore.registerGraphGenerator(
				GenericDCMotorGraphGenerator.INSTANCE);
/*
		RealizationCompStore.registerForceGenerator(new ForceGenerator(){
			public int getGeneratorType()
			{ return ForceGenerator.DEPENDSON_TIME; }
			public boolean effective(RealizationModel rmodel, RealizationMap smap, World world)
			{ return true; }
			public boolean forceWillBeGenerated(RealizationModel rmodel, RealizationMap smap, World world, double time)
			{ return true; }
			public void generateForce(RealizationModel rmode, RealizationMap smap, World world){
				System.out.println("DbgSet : arbitrary f : generateForce()");
				Vector3f f = new Vector3f();
				
				Vector3f vecp = new Vector3f();
				Vector3f vecr = new Vector3f();
				float m = world.getMass(0);
				float r = world.getTranslation(0, vecr).length();
				float p = world.getMomentum(0, vecp).length();
				
				vecr.normalize();
				vecr.scale(-(p * p) / (r * m));
				world.getForce(0, f);
				f.add(vecr);
				world.setForce(0, f);
				System.out.println("r : " + r);
			}// end generateForce
		});
*/
	}// end initModels()
}
