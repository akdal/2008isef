package agdr.robodari.plugin.defcomps;


import agdr.robodari.comp.*;
import agdr.robodari.edit.*;

public class ElectricPanel extends StructureComponent{
	public final static float THICKNESS_DEFAULT = 0.05f;
	private final static float[] XCOORDS_DEFAULT = new float[]{ -0.5f, 0.5f,
		0.5f, -0.5f };
	private final static float[] YCOORDS_DEFAULT = new float[]{ -0.5f, -0.5f,
		0.5f, 0.5f };
	
	private float[] xcoords;
	private float[] ycoords;
	private float thickness;
	

	public ElectricPanel(){
		xcoords = XCOORDS_DEFAULT;
		ycoords = YCOORDS_DEFAULT;
		thickness = THICKNESS_DEFAULT;
		setShape(xcoords, ycoords);
		setThickness(thickness);
	}// end Constructor(Image, Image, Polygon)


	
	public boolean hasSubComponent()
	{ return false; }
	public RobotComponent[] getSubComponents()
	{ return null; }
	
	public float[] getXCoords()
	{ return xcoords; }
	public float[] getYCoords()
	{ return ycoords; }
	public void setShape(float[] xcoords, float[] ycoords){
		this.xcoords = xcoords;
		this.ycoords = ycoords;
	}// end setShape(Polygon)


	public float getThickness() {
		return thickness;
	}


	public void setThickness(float thickness) {
		this.thickness = thickness;
	}
}