package agdr.robodari.plugin.defcomps;

import java.io.StringReader;
import javax.vecmath.*;

import agdr.library.math.MathUtility;
import agdr.robodari.comp.info.*;
import agdr.robodari.comp.model.AppearanceData;
import agdr.robodari.comp.model.ModelGenerator;
import agdr.robodari.comp.model.RobotCompModel;
import agdr.robodari.store.*;


public class ElectricPanelModelGenerator implements ModelGenerator<ElectricPanel>{
	public final static String GENPROP_THICKNESS = "thickness";
	public final static String GENPROP_POINT = "point";
	public final static float DEFAULT_THICKNESS = 0.05f;
	
	public RobotCompModel generate(ElectricPanel model) throws Exception{
		if(model.getXCoords() == null || model.getYCoords() == null)
			throw new NullPointerException("null coordinates");
		else if(model.getXCoords().length != model.getYCoords().length)
			throw new IllegalArgumentException();
		
		StringBuffer buf = new StringBuffer();
		buf.append("g electricpanel\n");
		
		float[] xcoords = model.getXCoords();
		float[] ycoords = model.getYCoords();
		int len = xcoords.length;
		
		for(int i = 0; i < len; i++){
			buf.append("v ").append(xcoords[i]).append(" ").
				append(ycoords[i]).append(" 0\n");
		}
		for(int i = 0; i < len; i++){
			buf.append("v ").append(xcoords[i]).append(" ").
				append(ycoords[i]).append(" ").append(-1.0 *
						model.getThickness()).append("\n");
		}
		buf.append("\n");
		buf.append("f");
		for(int i = 1; i <= xcoords.length; i++)
			buf.append(" ").append(i);
		buf.append("\nf");
		for(int i = xcoords.length; i >= 1; i--)
			buf.append(" ").append(i + xcoords.length);
		
		for(int i = 1; i < len; i++){
			buf.append("\nf").append(" ").append(i).append(" ")
				.append(i + len).append(" ")
				.append(i + len + 1).append(" ")
				.append(i + 1);
		}
		buf.append("\nf").append(" ").append(len).append(" ")
			.append(len * 2).append(" ").append(1 + len)
			.append(" ").append(1);
		buf.append("\n");

		StringReader sr = new StringReader(buf.toString());
		RobotCompModel str = new RobotCompModel();
		str.load(sr);
		
		Info info = RobotInfoStore.getInfo(model);
		str.getPhysicalData().mass = ((Number)info.get(InfoKeys.WEIGHT)).floatValue();
		
		Point3f cmp = new Point3f();
		cmp.z = -0.5f * model.getThickness();
		for(int i = 0; i < xcoords.length; i++){
			cmp.x += xcoords[i];
			cmp.y += ycoords[i];
		}
		cmp.x /= (double)xcoords.length;
		cmp.y /= (double)ycoords.length;
		str.getPhysicalData().centerMassPoint = cmp;
		str.getPhysicalData().inertia = new Matrix3f();
		MathUtility.addInertiaTensor(str.getPoints(), str.getFaces(),
				str.getPhysicalData().mass, -cmp.x, -cmp.y, -cmp.z,
				str.getPhysicalData().inertia);
		str.getPhysicalData().bounds = null;

		AppearanceData ap = str.getApprData();
		ap.setColor(java.awt.Color.gray);
		
		
		return str;
	}// end generate(Object[][])
}
