package agdr.robodari.plugin.defcomps;

import java.io.*;
import agdr.robodari.comp.*;
import agdr.robodari.plugin.rme.rdpi.*;
import agdr.robodari.plugin.rme.rlztion.RealizationModel;
import agdr.robodari.plugin.rme.rlztion.RealizationNotReadyException;
import agdr.robodari.plugin.rme.rlztion.RealizationStartedException;


public class GenericController implements Controller, SourceEditable, RDObject{
	public final static float DEFAULT_MASS = 20;
	
	private ThreePin[] pins;
	private String name;
	private RobotComponent parent;
	private Note note;
	private float mass = DEFAULT_MASS;
	private boolean isInrtTsInf = false;
	
	
	public GenericController(){
		_initPins();
	}// end Constructor
	void _initPins(){
		final RobotComponent _thisptr = this;
		
		pins = new ThreePin[12];
		for(int i = 0; i < pins.length; i++){
			pins[i] = new ThreePin(){
				public String getCondition()
				{ return ""; }
				public RobotComponent getParent()
				{ return _thisptr; }
				public void setParent(RobotComponent rc){}
				
				public boolean isCompatible(Sender s)
				{ return s != this && s.getParent() != getParent(); }
				public boolean isCompatible(Receiver r)
				{ return r != this && r.getParent() != getParent(); }
			};
		}
	}// end _initPins()

	
	/* **** RobotComponent items ********/
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getMass()
	{ return mass; }
	public void setMass(float m)
	{ this.mass = m; }
	
	public boolean isInrtTsInf()
	{ return isInrtTsInf; }
	public void setInrtTsInf(boolean flg)
	{ this.isInrtTsInf = flg; }
	
	public RobotComponent getParent() {
		return parent;
	}
	public void setParent(RobotComponent parent) {
		this.parent = parent;
	}
	public Note getNote() {
		return note;
	}
	public void setNote(Note note) {
		this.note = note;
	}
	
	public boolean hasSubComponent()
	{ return true; }
	public RobotComponent[] getSubComponents()
	{ return pins; }
	
	public boolean hasSender()
	{ return true; }
	public Sender[] getSenders()
	{ return pins; }
	public boolean hasReceiver()
	{ return true; }
	public Receiver[] getReceivers()
	{ return pins; }
	
	
	public String toString()
	{ return name; }
	

	/* **********************************************
	 *               RDObject IMPLEMENTATION
	 ************************************************/
	private transient RealizationModel rmodel;
	
	private final static boolean[] rd_fieldconsts = new boolean[0];
	private final static RDObject.Type[] rd_fieldtypes = new RDObject.Type[0];
	private final static String[] rd_fieldnames = new String[0];
	private final static String[] rd_fielddescs = new String[0];
	
	private final static String[] rd_methodnames = new String[0];
	private final static String[] rd_methoddescs = new String[0];
	private final static String[][] rd_methodparamnames = new String[0][0];
	private final static RDObject.Type[][] rd_methodparamtypes = new RDObject.Type[0][0];
	private final static RDObject.Type[] rd_methodtypes = new RDObject.Type[0];
	
	
	public void initRlztion(RealizationModel rm) throws RealizationStartedException{
		if(rmodel != null && rmodel.isRlzStarted())
			throw new RealizationStartedException();
		rmodel = rm;
	}// end initRlztion
	public void disposeRlztion(){
		rmodel = null;
	}// end disposeRlztion
	public void resetRlztion(){}
	
	public java.awt.Component getPropertyViewer(RealizationModel rmodel)
	{ return null; }
	

	public String[] getRDMethodNames()
	{ return rd_methodnames; }
	public RDObject.Type[][] getRDMethodParamTypes()
	{ return rd_methodparamtypes; }
	public RDObject.Type[] getRDMethodTypes()
	{ return rd_methodtypes; }
	public String[] getRDMethodDescs()
	{ return rd_methoddescs; }
	public String[][] getRDMethodParamNames()
	{ return rd_methodparamnames; }

	
	public String[] getRDFieldNames()
	{ return rd_fieldnames; }
	public boolean[] getRDConstants()
	{ return rd_fieldconsts; }
	public RDObject.Type[] getRDFieldTypes()
	{ return rd_fieldtypes; }
	public String[] getRDFieldDescs()
	{ return rd_fielddescs; }
	

	public Object getRDFieldValue(RealizationModel model,
			String fname)
			throws RealizationNotReadyException, RDFieldNotExistException{
		if(rmodel == null || !rmodel.isRlzStarted())
			throw new RealizationNotReadyException();
		throw new RDFieldNotExistException("", this, fname);
	}// end getRDFieldVAlue
	public void setRDFieldValue(RealizationModel model,
			String fname, Object val)
			throws RealizationNotReadyException, RDFieldNotExistException{
		if(rmodel == null || !rmodel.isRlzStarted())
			throw new RealizationNotReadyException();
		throw new RDFieldNotExistException("", this, fname);
	}// end setRDFieldValue
	public Object callRDMethod(RealizationModel model,
			String fname, Object[] params)
			throws RealizationNotReadyException, RDMethodNotExistException{
		if(rmodel == null || !rmodel.isRlzStarted())
			throw new RealizationNotReadyException();
		throw new RDMethodNotExistException("", this, fname, params);
	}// end callRDMethod
}
