package agdr.robodari.plugin.defcomps;

import java.awt.*;
import javax.swing.*;

import agdr.robodari.appl.BugManager;
import agdr.robodari.comp.Note;
import agdr.robodari.gui.*;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.gui.*;

public class GenericControllerEditor extends CompEditor{
	private JTabbedPane tabbedPane;
	
	private JPanel propertyPanel;
	private NoteEditor noteEditor;
	private JTextField nameField;
	private JCheckBox inrtInfBox;
	
	private SkObject object;
	

	public GenericControllerEditor(SketchEditorInterface manager, Dialog parent){
		super(manager, parent, "GenericController Editor");
		__set();
	}// end Constructor(EditorManager)
	public GenericControllerEditor(SketchEditorInterface manager, Frame parent){
		super(manager, parent, "GenericController Editor");
		__set();
	}// end Constructor(EditorManager)
	private void __set(){
		initPropertyPanel();
		tabbedPane = new JTabbedPane();
		tabbedPane.setTabPlacement(JTabbedPane.LEFT);
		tabbedPane.addTab("Properties", propertyPanel);
		
		super.getEditorPanel().setLayout(new BorderLayout());
		super.getEditorPanel().add(tabbedPane, BorderLayout.CENTER);
		
		super.setSize(480, 430);
	}// end __set
	private void initPropertyPanel(){
		propertyPanel = new JPanel();
		propertyPanel.setLayout(new BorderLayout());
		
		JPanel northPanel = new JPanel(new BorderLayout());
		{
			nameField = new JTextField();
			JLabel nameLabel = new JLabel("Component name : ");
			northPanel.add(nameLabel, BorderLayout.WEST);
			northPanel.add(nameField, BorderLayout.CENTER);
		}
		
		noteEditor = new NoteEditor();
		
		inrtInfBox = new JCheckBox("Infinite Inertia Tensor");
		
		propertyPanel.add(northPanel, BorderLayout.NORTH);
		propertyPanel.add(noteEditor, BorderLayout.CENTER);
		propertyPanel.add(inrtInfBox, BorderLayout.SOUTH);
	}// end initPropertyPanel()
	
	
	public void startEditing(SkObject object){
		if(!(object.getRobotComponent() instanceof GenericController))
			return;
		
		this.object = object;

		GenericController rcomp = (GenericController)object.getRobotComponent();
		nameField.setText(rcomp.getName());
		if(rcomp.getNote() == null)
			rcomp.setNote(new Note());
		noteEditor.open(rcomp.getNote());
		inrtInfBox.setSelected(rcomp.isInrtTsInf());
	}// end startEditing(SkObject)
	public void finishEditing(){
		GenericController rcomp = (GenericController)object.getRobotComponent();
		noteEditor.saveNote();
		rcomp.setNote(noteEditor.getNote());
		rcomp.setName(nameField.getText());
		rcomp.setInrtTsInf(inrtInfBox.isSelected());
	}// end finishEditing()
	public SkObject getObject()
	{ return object; }
}