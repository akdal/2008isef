package agdr.robodari.plugin.defcomps;

import java.io.*;
import javax.vecmath.*;
import agdr.library.math.*;
import agdr.robodari.comp.model.*;

public class GenericControllerModelGenerator implements ModelGenerator<GenericController>{
	private static String code =
		new StringBuffer("v 3 3 0\n")
			.append("v -3 3 0\n")
			.append("v -3 -3 0\n")
			.append("v 3 -3 0\n")
			
			.append("v 3 3 -0.5\n")
			.append("v -3 3 -0.5\n")
			.append("v -3 -3 -0.5\n")
			.append("v 3 -3 -0.5\n")
			
			.append("v 1.2 1.2 0.4\n")
			.append("v -1.2 1.2 0.4\n")
			.append("v -1.2 -1.2 0.4\n")
			.append("v 1.2 -1.2 0.4\n")
			
			.append("v 1.2 1.2 0.1\n")
			.append("v -1.2 1.2 0.1\n")
			.append("v -1.2 -1.2 0.1\n")
			.append("v 1.2 -1.2 0.1\n")
			
			.append("f 1 2 3 4\n")
			.append("f 8 7 6 5\n")
			.append("f 1 4 8 5\n")
			.append("f 1 5 6 2\n")
			.append("f 2 6 7 3\n")
			.append("f 3 7 8 4\n")
			
			.append("f 9 10 11 12\n")
			.append("f 16 15 14 13\n")
			.append("f 9 12 16 13\n")
			.append("f 9 13 14 10\n")
			.append("f 10 14 15 11\n")
			.append("f 11 15 16 12\n").toString();
	
	public RobotCompModel generate(GenericController model) throws Exception{
		RobotCompModel rcm = new RobotCompModel();
		StringReader sr = new StringReader(code);
		rcm.load(sr);
		
		PhysicalData pd = rcm.getPhysicalData();
		pd.mass = model.getMass();
		pd.volume = 6.0f * 6.0f * 0.5f + 2.4f * 2.4f * 0.3f;
		pd.centerMassPoint = new Point3f();
		pd.centerMassPoint.x = 0;
		pd.centerMassPoint.y = 0;
		pd.centerMassPoint.z = (18.0f * -0.25f + (5.76f * 0.3f) * 0.25f) / (18.0f + 5.76f * 0.3f);
		pd.bounds = null;
		pd.inertia = new Matrix3f();
		
		if(model.isInrtTsInf())
			pd.inertia.m00 = pd.inertia.m01 = pd.inertia.m02
				= pd.inertia.m10 = pd.inertia.m11 = pd.inertia.m12
				= pd.inertia.m20 = pd.inertia.m21 = pd.inertia.m22 = Float.POSITIVE_INFINITY;
		else
			MathUtility.addInertiaTensor(rcm.getPoints(),
				rcm.getFaces(),
				pd.mass,
				-pd.centerMassPoint.x,
				-pd.centerMassPoint.y,
				-pd.centerMassPoint.z,
				pd.inertia);
		
		AppearanceData ad = rcm.getApprData();
		ad.setColor(java.awt.Color.blue);
		
		return rcm;
	}// end generate
}
