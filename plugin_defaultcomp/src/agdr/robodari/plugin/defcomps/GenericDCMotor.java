package agdr.robodari.plugin.defcomps;

import java.io.*;
import java.util.*;

import agdr.library.math.*;
import agdr.robodari.comp.*;
import agdr.robodari.plugin.defcomps.rlztion.*;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.rdpi.*;
import agdr.robodari.plugin.rme.rlztion.*;

public class GenericDCMotor implements DCMotor, RDObject, java.io.Externalizable{
	public final static long serialVersionUID = -674866673306868027L;
	
	private final static RobotComponent[] NULLRCARRAY = new RobotComponent[]{};
	public final static float DEFAULT_WEIGHT = 40;
	public final static float AXIS_X = 1.0f;
	public final static float AXIS_Y = 0;
	public final static float AXIS_Z = 0;
	
	
	private TorqueFunction torque = new TorqueFunctions.LinearTorque();
	private SkObject attachedObject = null;
	
	private float weight = DEFAULT_WEIGHT;
	private Note note = new Note();
	private String name;
	private RobotComponent parent;

	private Receiver[] receivers = new Receiver[]{
			new GenericReceiver("rcv", this)
	};
	private transient ArrayList<RobotComponent> subComponents = new ArrayList<RobotComponent>();
	
	
	public float getWeight()
	{ return weight; }
	public void setWeight(float w)
	{ this.weight = w; }

	public TorqueFunction getTorqueFunc()
	{ return torque; }
	public void setTorqueFunc(TorqueFunction f)
	{ this.torque = f; }

	public SkObject getAttachedObject()
	{ return attachedObject; }
	public void setAttachedObject(SkObject obj){
		if(this.attachedObject != null){
			if(this.attachedObject.isGroup()){
				RobotComponent[] comps = this.attachedObject.getIntegratedRobotComponents();
				for(int i = 0; i < comps.length; i++)
					comps[i].setParent(null);
			}else{
				this.attachedObject.getRobotComponent().setParent(null);
			}
		}
		
		this.attachedObject = obj;
		subComponents.clear();

		if(this.attachedObject != null){
			if(this.attachedObject.isGroup()){
				RobotComponent[] comps = this.attachedObject.getIntegratedRobotComponents();
				for(int i = 0; i < comps.length; i++)
					comps[i].setParent(this);
				Collections.addAll(this.subComponents, comps);
			}else{
				this.attachedObject.getRobotComponent().setParent(this);
				this.subComponents.add(this.attachedObject.getRobotComponent());
			}
		}
	}// end setAttachedObject(SkObject)

	
	/* **********************************************
	 *          ROBOT COMPONENT IMPLEMENTATION
	 *************************************************/
	public Note getNote()
	{ return note; }
	public void setNote(Note n)
	{ this.note = n; }
	
	public String getName()
	{ return name; }
	public void setName(String n)
	{ this.name = n; }
	
	public RobotComponent getParent()
	{ return parent; }
	public void setParent(RobotComponent rc)
	{ this.parent = rc; }

	public boolean hasSubComponent()
	{ return true; }
	public RobotComponent[] getSubComponents()
	{ return subComponents.toArray(NULLRCARRAY); }
	public boolean hasReceiver()
	{ return true; }
	public Receiver[] getReceivers()
	{ return receivers; }
	public boolean hasSender()
	{ return false; }
	public Sender[] getSenders()
	{ return null; }
	
	
	/* ************************************************
	 *                   Externalizable
	 *************************************************/
	// order : 
	// 1. torque function
	// 2. attached obj
	// 3 . weight
	// 4. note
	// 5. parent
	// 6. name
	public void readExternal(ObjectInput input) throws IOException, ClassNotFoundException{
		agdr.robodari.appl.BugManager.printf("GenDCMotor : readExt : \n");
		TorqueFunction tf = (TorqueFunction)input.readObject();
		SkObject obj = (SkObject)input.readObject();
		
		weight = input.readFloat();
		note = (Note)input.readObject();
		parent = (RobotComponent)input.readObject();
		name = input.readObject().toString();
		receivers = (Receiver[])input.readObject();
		
		if(subComponents == null)
			subComponents = new ArrayList<RobotComponent>();
		else
			subComponents.clear();
		
		this.setTorqueFunc(tf);
		this.setAttachedObject(obj);
	}// end readExternal
	public void writeExternal(ObjectOutput output) throws IOException{
		output.writeObject(torque);
		output.writeObject(attachedObject);
		
		output.writeFloat(weight);
		output.writeObject(note);
		output.writeObject(parent);
		output.writeObject(name);
		output.writeObject(receivers);
	}// end writeExternal
	
	
	/* **********************************************
	 *               RDObject IMPLEMENTATION
	 ************************************************/
	private transient RealizationModel rmodel;
	private transient Object bodyKey, motorKey;
	private transient Integer direction;

	public final static Byte DIR_CW = new Byte((byte)1);
	public final static Byte DIR_NONE = new Byte((byte)0);
	public final static Byte DIR_CCW = new Byte((byte)-1);
	
	private final static boolean[] rd_fieldconsts = new boolean[]{
		true,
		true,
		true,
		true,
		true
	};
	private final static RDObject.Type[] rd_fieldtypes = new RDObject.Type[]{
		new RDObject.Type(Integer.class, RDObject.Type.NORMAL),
		new RDObject.Type(Integer.class, RDObject.Type.NORMAL),
		new RDObject.Type(Byte.class, RDObject.Type.NORMAL),
		new RDObject.Type(Byte.class, RDObject.Type.NORMAL),
		new RDObject.Type(Byte.class, RDObject.Type.NORMAL),
	};
	private final static String[] rd_fieldnames = new String[]{
		"BODY_ID",
		"MOTOR_ID",
		"DIR_CW",
		"DIR_CCW",
		"DIR_NONE"
	};
	private final static String[] rd_fielddescs = new String[]{
		"",
		"",
		"",
		"",
		""
	};
	
	
	private final static String[] rd_methodnames = new String[]{
		"getDir",
		"setDir"
	};
	private final static String[] rd_methoddescs = new String[]{
		"returns direction : DIR_CW if clockwise, DIR_NONE if stopped, DIR_CCW if counter-clockwise",
		"set direction : DIR_CW if clockwise, DIR_NONE if stopped, DIR_CCW if counter-clockwise"
	};
	private final static String[][] rd_methodparamnames = new String[][]{
		{},
		{"direction"},
	};
	private final static RDObject.Type[][] rd_methodparamtypes = new RDObject.Type[][]{
		{},
		{new RDObject.Type(Number.class, RDObject.Type.NORMAL)}
	};
	private final static RDObject.Type[] rd_methodtypes = new RDObject.Type[]{
		new RDObject.Type(Number.class, RDObject.Type.NORMAL),
		new RDObject.Type(null, RDObject.Type.VOID)
	};
	

	public Object getBodyKey()
	{ return bodyKey; }
	public void setBodyKey(Object bodyKey)
	{ this.bodyKey = bodyKey; }
	public Object getMotorKey()
	{ return motorKey; }
	public void setMotorKey(Object motorKey)
	{ this.motorKey = motorKey; }
	
	public void initRlztion(RealizationModel rm) throws RealizationStartedException{
		rmodel = rm;
		direction = new Integer(0);
	}// end initRlztion
	public void disposeRlztion(){
		rmodel = null;
		bodyKey = motorKey = null;
	}// end disposeRlztion
	public void resetRlztion(){
		direction = new Integer(0);
	}// end resetRlztion
	
	public java.awt.Component getPropertyViewer(RealizationModel rmodel)
	{ return null; }
	

	public String[] getRDMethodNames()
	{ return rd_methodnames; }
	public RDObject.Type[][] getRDMethodParamTypes()
	{ return rd_methodparamtypes; }
	public RDObject.Type[] getRDMethodTypes()
	{ return rd_methodtypes; }
	public String[] getRDMethodDescs()
	{ return rd_methoddescs; }
	public String[][] getRDMethodParamNames()
	{ return rd_methodparamnames; }

	
	public String[] getRDFieldNames()
	{ return rd_fieldnames; }
	public boolean[] getRDConstants()
	{ return rd_fieldconsts; }
	public RDObject.Type[] getRDFieldTypes()
	{ return rd_fieldtypes; }
	public String[] getRDFieldDescs()
	{ return rd_fielddescs; }
	

	public Object getRDFieldValue(RealizationModel model,
			String fname)
			throws RealizationNotReadyException, RDFieldNotExistException{
		if(rmodel == null || !rmodel.isRlzStarted())
			throw new RealizationNotReadyException();
		
		if(fname.equals(rd_fieldnames[0]))
			return this.bodyKey;
		else if(fname.equals(rd_fieldnames[1]))
			return this.motorKey;
		else if(fname.equals(rd_fieldnames[2]))
			return DIR_CW;
		else if(fname.equals(rd_fieldnames[3]))
			return DIR_CCW;
		else if(fname.equals(rd_fieldnames[4]))
			return DIR_NONE;
		
		throw new RDFieldNotExistException("", this, fname);
	}// end getRDFieldVAlue
	public void setRDFieldValue(RealizationModel model,
			String fname, Object val)
			throws RealizationNotReadyException, RDFieldNotExistException{
		if(rmodel == null || !rmodel.isRlzStarted())
			throw new RealizationNotReadyException();
		
		if(fname.equals(rd_fieldnames[0]))
			// it's constant!
			;
		else if(fname.equals(rd_fieldnames[1])) ;
		else if(fname.equals(rd_fieldnames[2])) ;
		else if(fname.equals(rd_fieldnames[3])) ;
		else if(fname.equals(rd_fieldnames[4])) ;
		
		throw new RDFieldNotExistException("", this, fname);
	}// end setRDFieldValue
	public Object callRDMethod(RealizationModel model,
			String fname, Object[] params)
			throws RealizationNotReadyException, RDMethodNotExistException{
		/*if(rmodel == null || !rmodel.isRlzStarted())
			throw new RealizationNotReadyException();
		// parameter검사 안함 귀찮아
		*/
		if(rd_methodnames[0].equals(fname)){
			// getDir
			return direction;
		}else if(rd_methodnames[1].equals(fname)){
			// setdir
			this.direction = ((Number)params[0]).intValue();
			return null;
		}
		
		throw new RDMethodNotExistException("", this, fname, params);
	}// end callRDMethod
}
