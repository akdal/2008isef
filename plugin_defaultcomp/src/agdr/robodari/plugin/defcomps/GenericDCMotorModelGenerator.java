package agdr.robodari.plugin.defcomps;

import java.awt.Color;
import javax.vecmath.*;

import static java.lang.Math.*;

import agdr.library.math.*;
import agdr.robodari.appl.*;
import agdr.robodari.comp.model.*;
import agdr.robodari.plugin.rme.edit.SkObject;

public class GenericDCMotorModelGenerator implements ModelGenerator<GenericDCMotor>{
	private StringBuffer motorcode = new StringBuffer();
	private final static float RADIUS = 2;
	private final static float HEIGHT = 3;
	private final static Matrix3f INERTIA_UNIT = new Matrix3f(
			(RADIUS * RADIUS / 4.0f) + (HEIGHT * HEIGHT / 12.0f), 0, 0,
			0, (RADIUS * RADIUS / 4.0f) + (HEIGHT * HEIGHT / 12.0f), 0,
			0, 0, RADIUS * RADIUS / 2.0f
		);
	
	// 3cm height, 2cm radius
	public GenericDCMotorModelGenerator(){  
		for(int i = 0; i < 20; i++)
			motorcode.append(String.format("v 0 %g %g\n", RADIUS * sin(PI / 10 * i), RADIUS * cos(PI / 10 * i)));
		for(int i = 0; i < 20; i++)
			motorcode.append(String.format("v -%g %g %g\n", HEIGHT, RADIUS * sin(PI / 10 * i), RADIUS * cos(PI / 10 * i)));
		for(int i = 0; i < 10; i++)
			motorcode.append(String.format("v 0 %g %g\n", 0.3 * sin(PI / 5 * i), 0.3 * cos(PI / 5 * i)));
		for(int i = 0; i < 10; i++)
			motorcode.append(String.format("v 1 %g %g\n", 0.3 * sin(PI / 5 * i), 0.3 * cos(PI / 5 * i)));
		
		for(int i = 1; i <= 19; i++)
			motorcode.append(String.format("f %d %d %d %d\n", i, i + 1, 20 + i + 1, 20 + i));
		motorcode.append("f 20 1 21 40\n");
		motorcode.append("f 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 21\n");
		motorcode.append("f 20 19 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 20\n");
		
		for(int i = 1; i <= 9; i++)
			motorcode.append(String.format("f %d %d %d %d\n", 40 + i + 1, 40 + i, 50 + i, 50 + i + 1));
		motorcode.append("f 41 50 60 51\n");
		motorcode.append("f 50 41 42 43 44 45 46 47 48 49 50\n");
		motorcode.append("f 60 59 58 57 56 55 54 53 52 51 60\n");
	}// end Constructor
	
	public RobotCompModel generate(GenericDCMotor motor) throws Exception{
		RobotCompModel structure = new RobotCompModel();
		structure.setGroup(true);
		
		_genBody(structure, motor);

		SkObject attobj = motor.getAttachedObject();
		if(attobj != null){
			if(attobj.isGroup()){
				structure.addSubModel(attobj.getIntegratedModel(),
						attobj.getTransform());
			}else structure.addSubModel(attobj.getModel(),
					attobj.getTransform());
		}
		
		return structure;
	}// end generate
	void _genBody(RobotCompModel structure, GenericDCMotor motor) throws Exception{
		RobotCompModel rcm = new RobotCompModel();
		rcm.load(new java.io.StringReader(motorcode.toString()));
		int[][] faces = rcm.getFaces();
		Point3f[] points = rcm.getPoints();
		
		AppearanceData ad = rcm.getApprData();
		ad.setColor(Color.blue);
		
		PhysicalData pd = rcm.getPhysicalData();
		pd.bounds = null;
		pd.mass = motor.getWeight();

		pd.volume = (float)(Math.PI * RADIUS * RADIUS * HEIGHT);
		
		pd.inertia = new Matrix3f(0, 0, -1,
				0, 1, 0,
				1, 0, 0);
		pd.inertia.mul(INERTIA_UNIT);
		pd.inertia.mul(new Matrix3f(
				0, 0, 1,
				0, 1, 0, 
				-1, 0, 0
		));
		pd.inertia.mul(pd.mass);
		pd.centerMassPoint = new Point3f(-HEIGHT / 2.0f, 0, 0);
		
		Matrix4f trans = new Matrix4f();
		trans.m00 = trans.m11 = trans.m22 = trans.m33 = 1;
		structure.addSubModel(rcm, trans);
	}// end _genBody
}
