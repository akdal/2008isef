package agdr.robodari.plugin.defcomps;

import java.io.*;
import java.awt.*;

import javax.swing.*;

import agdr.robodari.comp.*;

public class GenericReceiver implements Receiver, java.io.Externalizable{
	private final static Icon ICON = new Icon(){
		public int getIconWidth()
		{ return 11; }
		public int getIconHeight()
		{ return 11; }
		public void paintIcon(Component c, Graphics g, int x, int y){
			if(g == null) return;
			
			Color original = g.getColor();
			g.setColor(Color.white);
			g.fillRect(x + 1, y + 1, 9, 9);
			g.setColor(original);
			g.drawRect(x + 1, y + 1, 9, 9);
		}// end paintIcon(Component, Graphics, int, int)
	};
	private RobotComponent parent;
	private String name;
	private Note note;
	
	public GenericReceiver(){}
	public GenericReceiver(String name, RobotComponent p){
		this.parent = p;
		this.name = name;
		this.note = new Note();
	}// end Constructor
	
	
	public String getName()
	{ return name; }
	public void setName(String n)
	{ this.name = n; }
	public Note getNote()
	{ return note; }
	public void setNote(Note note)
	{ this.note = note; }
	
	public Icon getSimpleIcon()
	{ return ICON; }
	
	public boolean hasSubComponent()
	{ return false; }
	public RobotComponent[] getSubComponents()
	{ return null; }
	
	public boolean hasSender()
	{ return false; }
	public Sender[] getSenders()
	{ return null; }
	public boolean hasReceiver()
	{ return false; }
	public Receiver[] getReceivers()
	{ return null; }
	
	public RobotComponent getParent()
	{ return parent; }
	
	public void setParent(RobotComponent rc){}
	
	public String getCondition()
	{ return ""; }

	public boolean isEnabled()
	{ return true; }
	public boolean isCompatible(Sender rcv){
		return rcv != this && rcv.getParent() != this.getParent();
	}// end isCompatible(Receiver)
	
	
	
	public void readExternal(ObjectInput input) throws ClassNotFoundException, IOException{
		this.parent = (RobotComponent)input.readObject();
		this.name = (String)input.readObject();
		this.note = (Note)input.readObject();
	}// end readExternal
	public void writeExternal(ObjectOutput output) throws IOException{
		output.writeObject(this.parent);
		output.writeObject(this.name);
		output.writeObject(this.note);
	}// end writeExternal
}
