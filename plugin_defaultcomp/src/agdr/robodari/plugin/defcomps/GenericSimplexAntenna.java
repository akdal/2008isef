package agdr.robodari.plugin.defcomps;

import java.io.*;
import java.awt.*;
import javax.swing.*;
import agdr.robodari.comp.*;
import agdr.robodari.plugin.rme.rdpi.*;
import agdr.robodari.plugin.rme.rlztion.*;

public class GenericSimplexAntenna implements Antenna, RDObject, Externalizable{
	public final static float DEFAULT_MASS = 10.0f;
	public final static Icon ICON = new Icon(){
		public int getIconWidth()
		{ return 9; }
		public int getIconHeight()
		{ return 10; }
		public void paintIcon(Component c, Graphics g, int x, int y){
			Color org = g.getColor();
			g.setColor(Color.white);
			g.fillRect(x, y, 8, 9);
			g.setColor(org);
			g.drawRect(x, y, 8, 9);
			g.drawLine(x + 4, y, x + 4, y + 9);
			g.drawLine(x, y + 1, x + 8, y + 1);
			g.drawLine(x, y + 3, x + 8, y + 3);
			g.drawLine(x, y + 5, x + 8, y + 5);
		}// end paintIcon(Compoentn, GRaphics, int, int
	};
	
	private float mass = DEFAULT_MASS;
	private String name;
	private Note note;
	private RobotComponent parent;
	private transient Sender[] senders = new Sender[]{
			new _Sender(this)
	};
	
	
	
	public RobotComponent getParent()
	{ return parent; }
	public void setParent(RobotComponent rc)
	{ this.parent = rc; }
	
	public Note getNote()
	{ return note; }
	public void setNote(Note n)
	{ this.note = n; }
	
	public String getName()
	{ return name; }
	public void setName(String n)
	{ this.name = n; }
	
	public float getMass()
	{ return mass; }
	public boolean isSimplex()
	{ return true; }
	
	
	public boolean hasReceiver()
	{ return false; }
	public Receiver[] getReceivers()
	{ return null; }
	
	public boolean hasSender()
	{ return true; }
	public Sender[] getSenders()
	{ return senders; }
	
	public boolean hasSubComponent()
	{ return true; }
	public RobotComponent[] getSubComponents()
	{ return senders; }
	
	
	/* ***************************************************
	 *             Externalizable Implementation
	 ****************************************************/
	public void readExternal(ObjectInput input) throws IOException, ClassNotFoundException{
		mass = input.readFloat();
		name = (String)input.readObject();
		note = (Note)input.readObject();
		parent = (RobotComponent)input.readObject();
		senders = (Sender[])input.readObject();
	}// end readExternal
	public void writeExternal(ObjectOutput output) throws IOException{
		output.writeFloat(mass);
		output.writeObject(name);
		output.writeObject(note);
		output.writeObject(parent);
		output.writeObject(senders);
	}// end writeExternal
	
	
	
	/* ***************************************************
	 *                  RDObject Implementation
	 ****************************************************/
	
	private final static boolean[] rd_fieldconsts = new boolean[]{};
	private final static RDObject.Type[] rd_fieldtypes = new RDObject.Type[]{};
	private final static String[] rd_fieldnames = new String[]{};
	private final static String[] rd_fielddescs = new String[]{};
	
	
	private final static String[] rd_methodnames = new String[]{
		"read"
	};
	private final static String[] rd_methoddescs = new String[]{
		""
	};
	private final static String[][] rd_methodparamnames = new String[][]{
		{}
	};
	private final static RDObject.Type[][] rd_methodparamtypes = new RDObject.Type[][]{
		{},
	};
	private final static RDObject.Type[] rd_methodtypes = new RDObject.Type[]{
		new RDObject.Type(Object.class, RDObject.Type.NORMAL),
	};	

	private transient RealizationModel rmodel;
	private transient Object receivedData;
	
	
	public void receive(Object obj)
	{ this.receivedData = obj; }

	public void initRlztion(RealizationModel rm) throws RealizationStartedException{
		if(rmodel != null && !rmodel.isRlzStarted())
			throw new RealizationStartedException();
		rmodel = rm;
		receivedData = null;
	}// end initRlztion
	public void disposeRlztion() throws RealizationNotReadyException{
		if(rmodel == null || !rmodel.isRlzStarted())
			throw new RealizationNotReadyException();
		rmodel = null;
		receivedData = null;
	}// end disposeRlztion
	public void resetRlztion()
	{ receivedData = null; }
	
	public java.awt.Component getPropertyViewer(RealizationModel rmodel)
	{ return null; }
	

	public String[] getRDMethodNames()
	{ return rd_methodnames; }
	public RDObject.Type[][] getRDMethodParamTypes()
	{ return rd_methodparamtypes; }
	public RDObject.Type[] getRDMethodTypes()
	{ return rd_methodtypes; }
	public String[] getRDMethodDescs()
	{ return rd_methoddescs; }
	public String[][] getRDMethodParamNames()
	{ return rd_methodparamnames; }

	
	public String[] getRDFieldNames()
	{ return rd_fieldnames; }
	public boolean[] getRDConstants()
	{ return rd_fieldconsts; }
	public RDObject.Type[] getRDFieldTypes()
	{ return rd_fieldtypes; }
	public String[] getRDFieldDescs()
	{ return rd_fielddescs; }
	

	public Object getRDFieldValue(RealizationModel model,
			String fname)
			throws RealizationNotReadyException, RDFieldNotExistException{
		if(rmodel == null || !rmodel.isRlzStarted())
			throw new RealizationNotReadyException();
		
		throw new RDFieldNotExistException("", this, fname);
	}// end getRDFieldVAlue
	public void setRDFieldValue(RealizationModel model,
			String fname, Object val)
			throws RealizationNotReadyException, RDFieldNotExistException{
		if(rmodel == null || !rmodel.isRlzStarted())
			throw new RealizationNotReadyException();
		
		throw new RDFieldNotExistException("", this, fname);
	}// end setRDFieldValue
	public Object callRDMethod(RealizationModel model,
			String fname, Object[] params)
			throws RealizationNotReadyException, RDMethodNotExistException{
		if(rmodel == null || !rmodel.isRlzStarted())
			throw new RealizationNotReadyException();
		
		if(rd_methodnames[0].equals(fname)){
			// getDir
			Object tmp = this.receivedData;
			this.receivedData = null;
			return tmp;
		}
		
		throw new RDMethodNotExistException("", this, fname, params);
	}// end callRDMethod
	
	
	
	
	
	
	
	
	class _Sender implements Sender{
		private RobotComponent p;
		
		public _Sender(RobotComponent p)
		{ this.p = p; }
		
		public String getName()
		{ return "antenna"; }
		public void setName(String s){}
		public RobotComponent getParent()
		{ return p; }
		public void setParent(RobotComponent r){}
		
		public Note getNote()
		{ return null; }
		public void setNote(Note n){}
		
		public boolean hasReceiver()
		{ return false; }
		public Receiver[] getReceivers()
		{ return null; }
		public boolean hasSender()
		{ return false; }
		public Sender[] getSenders()
		{ return null; }
		
		public boolean hasSubComponent()
		{ return false; }
		public RobotComponent[] getSubComponents()
		{ return null; }
		
		public String getCondition()
		{ return ""; }
		public boolean isCompatible(Receiver rc)
		{ return true; }
		public Icon getSimpleIcon()
		{ return ICON; }
		public boolean isEnabled()
		{ return true; }
		
	}// end class _Sender
}
