package agdr.robodari.plugin.defcomps;

import java.awt.*;
import java.io.*;
import javax.vecmath.*;
import agdr.robodari.comp.model.*;
import agdr.library.math.*;

public class GenericSimplexAntennaModelGenerator implements ModelGenerator<GenericSimplexAntenna>{
	private static String code = new StringBuffer("v 0.3 0.3 7\n")
				.append("v -0.3 0.3 7\n")
				.append("v -0.3 -0.3 7\n")
				.append("v 0.3 -0.3 7\n")
				.append("v 0.5 0.3 0\n")
				.append("v -0.5 0.3 0\n")
				.append("v -0.5 -0.3 0\n")
				.append("v 0.5 -0.3 0\n")
				
				.append("f 1 2 3 4\n")
				.append("f 8 7 6 5\n")
				.append("f 1 5 6 2\n")
				.append("f 2 6 7 3\n")
				.append("f 3 7 8 4\n")
				.append("f 4 8 5 1\n").toString();
	
	public RobotCompModel generate(GenericSimplexAntenna gsa) throws Exception{
		RobotCompModel rcm = new RobotCompModel();
		StringReader sr = new StringReader(code);
		rcm.load(sr);
		
		Point3f[] ps = rcm.getPoints();
		int[][] faces = rcm.getFaces();
		PhysicalData pd = rcm.getPhysicalData();
		//pd.mass = gsa.getMass();
		pd.mass = 0;
		pd.volume = MathUtility.getVolume(ps, faces);
		pd.centerMassPoint = new Point3f(0, 0, 3.5f);
		pd.bounds = null;
		pd.inertia = new Matrix3f();
		//MathUtility.addInertiaTensor(ps, faces, pd.mass, -pd.centerMassPoint.x, -pd.centerMassPoint.y, -pd.centerMassPoint.z, pd.inertia);
		
		AppearanceData ad = new AppearanceData();
		ad.setColor(java.awt.Color.blue);
		rcm.setApprData(ad);
		
		return rcm;
	}// end generate
}
