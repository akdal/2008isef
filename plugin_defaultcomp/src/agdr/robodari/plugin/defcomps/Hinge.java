package agdr.robodari.plugin.defcomps;

import java.util.*;
import javax.vecmath.*;
import agdr.robodari.comp.*;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.rdpi.*;
import agdr.robodari.plugin.rme.rlztion.*;

public class Hinge extends StructureComponent{
	public final static float DEFAULT_HINGEGAP = 1.2f;
	public final static float DEFAULT_HINGEHEIGHT = 1.2f;
	
	private SkObject hangingObj;
	private SkObject fixedObj;
	private float hingeGap = DEFAULT_HINGEGAP;
	private float hingeHeight = DEFAULT_HINGEHEIGHT;
	
	public Hinge(){
		resetDefaultHangingObj();
		resetDefaultFixedObj();
	}// end Constructor
	
	
	
	public void resetDefaultHangingObj(){
		try{
			RoundedRectanglePane hangingcomp = new RoundedRectanglePane();
			hangingcomp.setThickness(hingeGap * 0.5f - 0.2f);
			hangingcomp.setRadius(hingeHeight - 0.2f);
			hangingcomp.setName("Hinge_Hanging");
			Matrix4f hctrans = new Matrix4f();
			//z축으로 90도 회전, x축으로 90도 회전.
			/*
1.0, 0, 0, 0.0
0, 0, 1.0, 0.0
0, -1.0, 0, 0.0
0.0, 0.0, 0.0, 1.0
			 */
			hctrans.m00 = 1;
			hctrans.m12 = 1;
			hctrans.m21 = -1;
			hctrans.m33 = 1;
			hctrans.m03 = 0; // x
			hctrans.m13 = hangingcomp.getThickness() / 2.0f;
			hctrans.m23 = hingeHeight + 0.05f;
			
			hangingObj = new SkObject(hangingcomp, hctrans);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}// end resetDefaultHangingObj()
	public void resetDefaultFixedObj(){
		try{
			RectanglePane fixedcomp = new RectanglePane();
			fixedcomp.setWidth(hingeGap * 3.0f);
			fixedcomp.setHeight(hingeGap * 3.0f);
			fixedcomp.setThickness(hingeHeight / 2.0f);
			fixedcomp.setName("Hinge_fixed");
			
			Matrix4f fctrans = new Matrix4f();
			fctrans.m00 = fctrans.m11 = fctrans.m22 = fctrans.m33 = 1;
			fctrans.m03 = -hingeGap * 1.5f;
			fctrans.m13 = -hingeGap * 1.5f;
			fctrans.m23 = -hingeHeight * 0.5f;
			
			fixedObj = new SkObject(fixedcomp, fctrans);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}// end resetDefaultFixedObj
	
	public float getHingeGap()
	{ return hingeGap; }
	public void setHingeGap(float hg)
	{ this.hingeGap = hg; }
	
	public float getHingeHeight()
	{ return hingeHeight; }
	public void setHingeHeight(float hh)
	{ this.hingeHeight = hh; }
	
	public SkObject getHangingObject()
	{ return hangingObj; }
	public void setHangingObject(SkObject o)
	{ this.hangingObj = o; }
	
	public SkObject getFixedObject()
	{ return fixedObj; }
	public void setFixedObject(SkObject obj)
	{ this.fixedObj = obj; }
	
	public boolean hasSubComponent()
	{ return true; }
	public RobotComponent[] getSubComponents(){
		ArrayList<RobotComponent> rclist = new ArrayList<RobotComponent>();
		if(this.hangingObj != null){
			if(this.hangingObj.isGroup())
				Collections.addAll(rclist, this.hangingObj.getIntegratedRobotComponents());
			else
				rclist.add(this.hangingObj.getRobotComponent());
		}
		if(this.fixedObj != null){
			if(this.fixedObj.isGroup())
				Collections.addAll(rclist, this.fixedObj.getIntegratedRobotComponents());
			else
				rclist.add(this.fixedObj.getRobotComponent());
		}
		return rclist.toArray(new RobotComponent[]{});
	}
}
