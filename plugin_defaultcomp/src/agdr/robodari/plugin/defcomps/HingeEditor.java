package agdr.robodari.plugin.defcomps;

import java.awt.*;
import java.awt.event.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import javax.swing.*;
import javax.vecmath.*;


import agdr.library.math.MathUtility;
import agdr.robodari.appl.BugManager;
import agdr.robodari.comp.*;
import agdr.robodari.gui.*;
import agdr.robodari.plugin.rme.appl.ExceptionHandler;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.gui.*;
import agdr.robodari.plugin.rme.store.RobotCompEditorStore;


public class HingeEditor extends CompEditor
		implements SketchEditorInterface, MainSketchEditor,
		ActionListener, ItemListener, FocusListener{
	private final static SketchEditorListener[] NULL_SELARRAY = new SketchEditorListener[]{};
	
	
	private JTabbedPane tabbedPane;
	
	private JPanel propertiesPanel;
		private JTextField nameField;
		private NoteEditor noteEditor;
	private JPanel objectsPanel;
		private JTextField xposField;
		private JTextField yposField;
		private JTextField zposField;
		private JButton xposIncrButton, xposDecrButton;
		private JButton yposIncrButton, yposDecrButton;
		private JButton zposIncrButton, zposDecrButton;
		
		private JTextField itvField;
		private float interval = 2.0f;
		private JTextField scaleField;
		private float scale = 5.0f;
		
		private JTextField hingeHeightField;
		private JTextField hingeGapField;
		
		private JRadioButton fixedObjButton;
		private JRadioButton hangingObjButton;
		private ButtonGroup objButtonGroup;
		
		private JButton rottButton;
		
		private SideSketchViewer[] ssViewers;
		private SkObjectViewer objViewer;
		private JComboBox objsComboBox;
		private DefaultComboBoxModel objsComboBoxModel;

		// original fixedobj, hangingobj는 currFixeDObj, currHObj의 원본.
		private SkObject originalObject;
		private SkObject initialFixedObject;
		private SkObject initialHangingObject;
		private SkObject originalFixedObject;
		private SkObject originalHangingObject;
		// currHinge does not have informations related to fixed obj or hanging obj
		private Hinge currHinge;
		private SkObject currHingeObject;
		private SkObject currFixedObject;
		private SkObject currHangingObject;
		private SketchModel sketchModel;

		private RotationEditor rottEditor;

	
		
		
		
	public HingeEditor(SketchEditorInterface manager, Frame parent){
		super(manager, parent, "Hinge editor");
		__set();
	}// end Constructor
	public HingeEditor(SketchEditorInterface manager, Dialog parent){
		super(manager, parent, "Hinge editor");
		__set();
	}// end Constructor(EditorManager)
	private void __set(){
		_initPropertiesPanel();
		_initObjectsPanel();
		tabbedPane = new JTabbedPane();
		tabbedPane.setTabPlacement(JTabbedPane.LEFT);
		tabbedPane.addTab("Properties", propertiesPanel);
		tabbedPane.addTab("Objects", objectsPanel);

		this.rottEditor = new RotationEditor(this, this);
		super.getEditorPanel().setLayout(new BorderLayout());
		super.getEditorPanel().add(tabbedPane, BorderLayout.CENTER);
		
		super.setSize(700, 700);
	}// end __set()
	
	private void _initPropertiesPanel(){
		propertiesPanel = new JPanel(new BorderLayout());
		{
			JPanel northPanel = new JPanel(new BorderLayout(3, 3));
			{
				nameField = new JTextField();
				northPanel.add(new JLabel("Name : "), BorderLayout.WEST);
				northPanel.add(nameField, BorderLayout.CENTER);
			}
			noteEditor = new NoteEditor();
			
			propertiesPanel.add(northPanel, BorderLayout.NORTH);
			propertiesPanel.add(noteEditor, BorderLayout.CENTER);
		}
	}// end _initPropertiesPAnel
	private void _initObjectsPanel(){
		objectsPanel = new JPanel(new BorderLayout());
		{
			JPanel northPanel = new JPanel();
			northPanel.setLayout(new BorderLayout());
			{
				// hanging or fixed selection
				JPanel nnpanel = new JPanel(new FlowLayout());
				{
					hangingObjButton = new JRadioButton("Hanging Object");
					fixedObjButton = new JRadioButton("Fixed Object");
					objButtonGroup = new ButtonGroup();
					objButtonGroup.add(hangingObjButton);
					objButtonGroup.add(fixedObjButton);
					
					hangingObjButton.setOpaque(false);
					hangingObjButton.addItemListener(this);
					fixedObjButton.setOpaque(false);
					fixedObjButton.addItemListener(this);
					nnpanel.add(hangingObjButton);
					nnpanel.add(fixedObjButton);
				}
				// x, y, z adjustment + rotation + comp edit
				JPanel ncpanel = new JPanel(new FlowLayout());
				{
					JPanel adjButtonPanel = new JPanel(new GridLayout(3, 3, 2, 2));
					{
						xposIncrButton = new JButton("+");
						yposIncrButton = new JButton("+");
						zposIncrButton = new JButton("+");
						xposDecrButton = new JButton("-");
						yposDecrButton = new JButton("-");
						zposDecrButton = new JButton("-");

						__cfgAdjButton(xposIncrButton);
						__cfgAdjButton(xposDecrButton);
						__cfgAdjButton(yposIncrButton);
						__cfgAdjButton(yposDecrButton);
						__cfgAdjButton(zposIncrButton);
						__cfgAdjButton(zposDecrButton);

						adjButtonPanel.add(new JLabel("x : "));
						adjButtonPanel.add(xposIncrButton);
						adjButtonPanel.add(xposDecrButton);
						adjButtonPanel.add(new JLabel("y : "));
						adjButtonPanel.add(yposIncrButton);
						adjButtonPanel.add(yposDecrButton);
						adjButtonPanel.add(new JLabel("z : "));
						adjButtonPanel.add(zposIncrButton);
						adjButtonPanel.add(zposDecrButton);
					}
					JPanel adjFieldPanel = new JPanel(new GridLayout(3, 0, 2, 2));
					{
						xposField = new JTextField();
						yposField = new JTextField();
						zposField = new JTextField();
						__cfgAdjField(xposField);
						__cfgAdjField(yposField);
						__cfgAdjField(zposField);
						
						adjFieldPanel.add(xposField);
						adjFieldPanel.add(yposField);
						adjFieldPanel.add(zposField);
					}
					JPanel adjIntvPanel = new JPanel();
					BoxLayout blayout = new BoxLayout(adjIntvPanel, BoxLayout.Y_AXIS);
					adjIntvPanel.setLayout(blayout);
					{
						itvField = new JTextField();
						itvField.addFocusListener(this);
						itvField.setColumns(8);
						scaleField = new JTextField();
						scaleField.addFocusListener(this);
						scaleField.addActionListener(this);
						scaleField.setColumns(8);
						
						adjIntvPanel.add(new JLabel("Interval : "));
						adjIntvPanel.add(itvField);
						adjIntvPanel.add(new JLabel("Scale : "));
						adjIntvPanel.add(scaleField);
					}
					JPanel hingePropPanel = new JPanel();
					BoxLayout hpplayout = new BoxLayout(hingePropPanel, BoxLayout.Y_AXIS);
					hingePropPanel.setLayout(hpplayout);
					{
						hingeHeightField = new JTextField();
						hingeHeightField.addActionListener(this);
						hingeHeightField.addFocusListener(this);
						hingeHeightField.setColumns(8);
						hingeGapField = new JTextField();
						hingeGapField.addActionListener(this);
						hingeGapField.addFocusListener(this);
						hingeGapField.setColumns(8);
						
						hingePropPanel.add(new JLabel("Hinge Height : "));
						hingePropPanel.add(hingeHeightField);
						hingePropPanel.add(new JLabel("Hinge Gap : "));
						hingePropPanel.add(hingeGapField);
					}
					JPanel ctrlPanel = new JPanel();
					BoxLayout ctrlbl = new BoxLayout(ctrlPanel, BoxLayout.Y_AXIS);
					ctrlPanel.setLayout(ctrlbl);
					{
						rottButton = new JButton("Rotation..");
						rottButton.addActionListener(this);
						ctrlPanel.add(rottButton);
					}
					
					ncpanel.add(adjButtonPanel);
					ncpanel.add(adjFieldPanel);
					ncpanel.add(adjIntvPanel);
					ncpanel.add(hingePropPanel);
					ncpanel.add(ctrlPanel);
				}
				northPanel.add(nnpanel, BorderLayout.NORTH);
				northPanel.add(ncpanel, BorderLayout.CENTER);
			}
			JPanel centerPanel = new JPanel(new GridLayout(2, 2, 2, 2));
			{
				ssViewers = new SideSketchViewer[3];
				ssViewers[0] = new SideSketchViewer(null, SketchEditor.PROJECTORS[0]);
				ssViewers[1] = new SideSketchViewer(null, SketchEditor.PROJECTORS[1]);
				ssViewers[2] = new SideSketchViewer(null, SketchEditor.PROJECTORS[2]);
				
				JPanel objViewerPanel = new JPanel(new BorderLayout());
				{
					objsComboBoxModel = new DefaultComboBoxModel();
					objsComboBox = new JComboBox();
					objsComboBox.setModel(objsComboBoxModel);
					this.objsComboBox.addItemListener(this);
					objViewer = new SkObjectViewer();
					
					objViewerPanel.add(objsComboBox, BorderLayout.NORTH);
					objViewerPanel.add(objViewer, BorderLayout.CENTER);
				}
				
				centerPanel.add(ssViewers[0]);
				centerPanel.add(objViewerPanel);
				centerPanel.add(ssViewers[1]);
				centerPanel.add(ssViewers[2]);
			}
			objectsPanel.add(northPanel, BorderLayout.NORTH);
			objectsPanel.add(centerPanel, BorderLayout.CENTER);
		}
		
		this.sketchModel = new SketchModel();
		this.ssViewers[0].loadSketch(sketchModel);
		this.ssViewers[1].loadSketch(this.sketchModel);
		this.ssViewers[2].loadSketch(this.sketchModel);
	}// end _initObjectsPanel
	private void __cfgAdjButton(JButton jb){
		jb.setMargin(new Insets(1, 1, 1, 1));
		jb.setPreferredSize(new Dimension(23, 23));
		jb.setSize(new Dimension(23, 23));
		jb.addActionListener(this);
	}// end __cfgAdjButton
	private void __cfgAdjField(JTextField jtf){
		jtf.setColumns(8);
		jtf.setEditable(false);
	}// end __cfgAdjFied
	
	private void setScale(){
		try{
			this.scale = Float.parseFloat(this.scaleField.getText());
			for(SideSketchViewer eachssv : ssViewers)
				eachssv.setScale(scale);
			this.repaint();
		}catch(Exception excp){
			Toolkit.getDefaultToolkit().beep();
			scaleField.setText(String.valueOf(this.scale));
		}
	}// end setScale()
	
	private void setHingeHeight(){
		try{
			float hh = Float.parseFloat(this.hingeHeightField.getText());
			this.currHinge.setHingeHeight(hh);
			this.refreshObject(this.currHingeObject);
		}catch(Exception excp){
			Toolkit.getDefaultToolkit().beep();
			this.hingeHeightField.setText(String.valueOf(this.currHinge.getHingeHeight()));
		}
	}// end setHingeHeight()
	private void setHingeGap(){
		try{
			float hg = Float.parseFloat(this.hingeGapField.getText());
			this.currHinge.setHingeGap(hg);
			this.refreshObject(this.currHingeObject);
		}catch(Exception excp){
			Toolkit.getDefaultToolkit().beep();
			this.hingeGapField.setText(String.valueOf(this.currHinge.getHingeGap()));
		}
	}// end setHingeHeight()
	
	
	
	
	
	/* ************************************************
	 *             EVENT LISTENERS
	 ****************************************************/
	
	public void focusGained(FocusEvent fe){}
	public void focusLost(FocusEvent fe){
		if(fe.getSource() == itvField){
			try{
				this.interval = Float.parseFloat(this.itvField.getText());
			}catch(Exception excp){
				Toolkit.getDefaultToolkit().beep();
				itvField.setText(String.valueOf(this.interval));
			}
		}else if(fe.getSource() == scaleField){
			setScale();
		}else if(fe.getSource() == hingeHeightField)
			setHingeHeight();
		else if(fe.getSource() == hingeGapField)
			setHingeGap();
	}// end focusLost(FocusEvent)
	
	public void actionPerformed(ActionEvent ae){
		Object src = ae.getSource();
		
		SkObject targetobj = null;
		Matrix4f targettrans;
		if(hangingObjButton.isSelected()){
			targetobj = this.currHangingObject;
		}else{
			targetobj = this.currFixedObject;
		}
		targettrans = targetobj.getTransform();
		boolean refreshPosFields = false;
		
		if(src == this.xposIncrButton){
			targettrans.m03 += this.interval;
			refreshPosFields = true;
		}else if(src == this.xposDecrButton){
			targettrans.m03 -= this.interval;
			refreshPosFields = true;
		}else if(src == this.yposIncrButton){
			targettrans.m13 += this.interval;
			refreshPosFields = true;
		}else if(src == this.yposDecrButton){
			targettrans.m13 -= this.interval;
			refreshPosFields = true;
		}else if(src == this.zposIncrButton){
			targettrans.m23 += this.interval;
			refreshPosFields = true;
		}else if(src == this.zposDecrButton){
			targettrans.m23 -= this.interval;
			refreshPosFields = true;
		}
		// buttons
		else if(src == this.rottButton){
			rottEditor.startEditing(targetobj);
			setCurrentCompEditor(rottEditor);
		}
		// hinge props
		else if(src == this.hingeGapField){
			setHingeGap();
		}else if(src == this.hingeHeightField){
			setHingeHeight();
		}
		// scae
		else if(src == this.scaleField){
			this.setScale();
		}
		
		
		if(refreshPosFields){
			this.xposField.setText(String.format("%.5f", targettrans.m03).toString());
			this.yposField.setText(String.format("%.5f", targettrans.m13).toString());
			this.zposField.setText(String.format("%.5f", targettrans.m23).toString());
		}
		this.refreshObject(targetobj);
	}// end actionPerformed(ActionEvent)
	
	public void itemStateChanged(ItemEvent ie){
		if(ie.getSource() == fixedObjButton)
			this.fillObjInfo(false);
		else if(ie.getSource() == hangingObjButton)
			this.fillObjInfo(true);
		else if(ie.getSource() == this.objsComboBox){
			try{
				System.out.printf("HingeEdior : itmStateChnged : objectComboBox : selecteditm : %s\n", ie.getItem());
				Object obj = objsComboBox.getSelectedItem();
				SkObject target_currObj = null;
				
				if(originalObject == null) return;
				
				if(this.fixedObjButton.isSelected())
					target_currObj = this.currFixedObject;
				else
					target_currObj = this.currHangingObject;
				
				if(obj == null) return;
				else if(objsComboBox.getSelectedIndex() == 0){
					if(this.fixedObjButton.isSelected())
						this.originalFixedObject = null;
					else
						this.originalHangingObject = null;
					
					target_currObj.setRobotComponent(null);
					this.refreshObject(target_currObj);
				}else{
					SkObject skobj = (SkObject)obj;
					
					if(this.fixedObjButton.isSelected())
						this.originalFixedObject = skobj;
					else
						this.originalHangingObject = skobj;
					
					BugManager.printf("HingeEditor : itmStCh : before target_currObj : %s\n", target_currObj);
					
					if(!skobj.isGroup()){
						target_currObj.setGroup(false);
						target_currObj.setRobotComponent(skobj.getRobotComponent());
					}else{
						target_currObj.setGroup(true);
						target_currObj.setGroupName(skobj.getGroupName());
						target_currObj.removeChildren();
						SkObject[] children = skobj.getChildren();
						for(int i = 0; i < children.length; i++)
							target_currObj.addChild(children[i]);
					}
					target_currObj.refresh();
					this.refreshObject(target_currObj);
				}
			}catch(Exception excp){
				BugManager.log(excp, true);
			}
		}
	}// end itemStateChanged
	
	
	
	
	
	
	
	
	/* ************************************************************
	 *                 SketchEditorInterface Impl
	 ***********************************************************/

	public void refreshObject(SkObject object){
		if(object == null) return;
		
		try{
			object.refresh();
			if(object != this.currHingeObject)
				objViewer.setObject(object);
			refreshSideSketchViewers();
			this.repaint();
		}catch(Exception excp){
			agdr.robodari.appl.BugManager.log(excp, true);
		}
	}// end refresh()
	private void refreshSideSketchViewers(){
		for(int i = 0; i < getSideSketchViewerCount(); i++){
			getSideSketchViewer(i).refreshObject(this.currFixedObject);
			getSideSketchViewer(i).refreshObject(this.currHangingObject);
			getSideSketchViewer(i).refreshObject(this.currHingeObject);
		}
	}// end refreshSideSketchViewers()
	
	public void addObject(RobotComponent rc, Matrix4f trans){}
	
	public int getSideSketchViewerCount()
	{ return 3; }
	public SideSketchViewer getSideSketchViewer(int idx)
	{ return this.ssViewers[idx]; }
	public RotationEditor getRotationEditor()
	{ return rottEditor; }
	public GroupEditor getGroupEditor()
	{ return null; }
	public CompEditor getCurrentCompEditor()
	{ return this.rottEditor.isVisible() ? this.rottEditor : null; }
	public void setCurrentCompEditor(CompEditor ce){
		if(this.rottEditor == ce && this.rottEditor.isVisible()) return;
		if(ce != null)
			ce.setVisible(true);
	}// end showCompEditor(CompEditor)
	public CompEditor getStoredCompEditor(Class<? extends RobotComponent> cls)
	{ return null; }
	public MainSketchEditor getMainEditor()
	{ return this; }
	public SketchModel getModel()
	{ return this.sketchModel; }
	public void setLayoutMode(int i){}
	public int getLayoutMode()
	{ return -1; }
	public void startCompEdit(SkObject obj){}
	
	
	/* ***********************************************************
	 *                MainSketchEditor, SketchScreen
	 *************************************************************/
	public void addSketchEditorListener(SketchEditorListener sl){}
	public void removeSketchEditorListener(SketchEditorListener sl){}
	public boolean containsSketchEditorListener(SketchEditorListener sel)
	{ return false; }
	public SketchEditorListener[] getSketchEditorListeners()
	{ return NULL_SELARRAY; }
	
	public java.awt.Polygon getBound(SkObject obj)
	{ return this.objViewer.getBound(obj); }
	public void disposeAllSelections(){}
	public void setSelected(SkObject obj, boolean b){}
	public Projector getProjector()
	{ return this.objViewer.getProjector(); }
	public float getScale()
	{ return this.objViewer.getScale(); }
	public float getAxisStartX()
	{ return this.objViewer.getAxisStartX(); }
	public float getAxisStartY()
	{ return this.objViewer.getAxisStartY(); }
	
	
	/* ************************************************************
	 *                  CompEditor implementation
	 ************************************************************** */
	
	public void startEditing(SkObject obj) throws Exception{
		if(!(obj.getRobotComponent() instanceof Hinge))
			return;
		
		Hinge hinge = (Hinge)obj.getRobotComponent();
		this.originalHangingObject = hinge.getHangingObject();
		this.initialHangingObject = this.originalHangingObject;
		if(originalHangingObject != null){
			this.currHangingObject = (SkObject)(this.originalHangingObject.clone());
		}else{
			Matrix4f trans = new Matrix4f();
			trans.m00 = trans.m11 = trans.m22 = trans.m33 = 1;
			this.currHangingObject = new SkObject((RobotComponent)null, trans);
		}
		
		this.originalFixedObject = hinge.getFixedObject();
		this.initialFixedObject = this.originalFixedObject;
		if(originalFixedObject != null)
			this.currFixedObject = (SkObject)(this.originalFixedObject.clone());
		else{
			Matrix4f trans = new Matrix4f();
			trans.m00 = trans.m11 = trans.m22 = trans.m33 = 1;
			this.currFixedObject = new SkObject((RobotComponent)null, trans);
		}
		
		this.currHinge = new Hinge();
		this.currHinge.setHingeGap(hinge.getHingeGap());
		this.currHinge.setHingeHeight(hinge.getHingeHeight());
		this.currHinge.setFixedObject(null);
		this.currHinge.setHangingObject(null);
		Matrix4f currHingeTrans = new Matrix4f();
		currHingeTrans.m00 = currHingeTrans.m11 = currHingeTrans.m22 = currHingeTrans.m33 = 1;
		this.currHingeObject = new SkObject(currHinge, currHingeTrans);
		
		this.sketchModel.clear();
		this.sketchModel.addObject(this.currHingeObject);
		this.sketchModel.addObject(this.currFixedObject);
		this.sketchModel.addObject(this.currHangingObject);
		
		this.scaleField.setText(String.format("%.4f", scale));
		this.itvField.setText(String.format("%.4f", interval));
		this.hingeHeightField.setText(String.format("%.4f", this.currHinge.getHingeHeight()));
		this.hingeGapField.setText(String.format("%.4f", this.currHinge.getHingeGap()));

		this.originalObject = obj;
		this.hangingObjButton.setSelected(true);
		this.fillObjInfo(true);

		
		this.nameField.setText(hinge.getName());
		this.noteEditor.open(hinge.getNote());
	}// end startEditing(SkObject)
	
	private void fillObjInfo(boolean hangingobj){
		System.out.println("HingeEditor : fillObjInfo");
		// This method performs : 
		// - fill translation data
		// - load combobox model
		Matrix4f targettrans = null;
		if(hangingobj)
			targettrans = this.currHangingObject.getTransform();
		else
			targettrans = this.currFixedObject.getTransform();
		
		this.xposField.setText(String.format("%.5f", targettrans.m03).toString());
		this.yposField.setText(String.format("%.5f", targettrans.m13).toString());
		this.zposField.setText(String.format("%.5f", targettrans.m23).toString());
		
		this.objsComboBox.removeItemListener(this);
		this.objsComboBoxModel.removeAllElements();
		this.objsComboBoxModel.addElement("[Empty]");
		SkObject[] objects = super.getInterface().getModel().getObjects();
		
		BugManager.printf("HingeEditor : fillObj : originalObj : %s\n", originalObject);
		for(int i = 0; i < objects.length; i++){
			BugManager.printf("HingeEditor : fillObj : %dth obj : %s, this.orgObj == obj : %b\n", i, objects[i], objects[i] == this.originalObject);
			if(objects[i] != this.originalObject){
				this.objsComboBoxModel.addElement(objects[i]);
			}
		}
		this.objsComboBox.addItemListener(this);
		this.objsComboBox.setSelectedIndex(0);
		if(hangingobj){
			if(this.initialHangingObject != null){
				this.objsComboBoxModel.addElement(this.initialHangingObject);
				this.objsComboBox.setSelectedIndex(this.objsComboBoxModel.getSize() - 1);
			}
		}else{
			if(this.initialFixedObject != null){
				this.objsComboBoxModel.addElement(this.initialFixedObject);
				this.objsComboBox.setSelectedIndex(this.objsComboBoxModel.getSize() - 1);
			}
		}
	}// end fillObjInfo
	
	public void finishEditing() throws Exception{
		SketchModel orgmodel = this.getInterface().getModel();
		Hinge hinge = (Hinge)this.originalObject.getRobotComponent();
		Matrix4f originalTrans = new Matrix4f(originalObject.getTransform());
		// properties panel에서 설정했떤 name, note 대입
		this.noteEditor.saveNote();
		hinge.setName(this.nameField.getText());
		hinge.setNote(this.noteEditor.getNote());
		
		// 원래 hinge의 hangingobj, fixedobj를 getInterface()의 모델로 귀속시킴.
		if(hinge.getHangingObject() != null){
			SkObject hobj = hinge.getHangingObject();
			Matrix4f transf = new Matrix4f();
			MathUtility.fastMul(originalTrans, hobj.getTransform(), transf);
			hobj.setTransform(transf);
			
			orgmodel.addObject(hobj);
			hinge.setHangingObject(null);
		}
		if(hinge.getFixedObject() != null){
			SkObject fobj = hinge.getFixedObject();
			Matrix4f transf = new Matrix4f();
			MathUtility.fastMul(originalTrans, fobj.getTransform(), transf);
			fobj.setTransform(transf);
			
			orgmodel.addObject(fobj);
			hinge.setHangingObject(null);
		}
		// 선택한 hangingobj, fixedobj를 orgmodel에서 제거한 후에 hinge에 등록.
		Matrix4f orgTransInv = new Matrix4f();
		MathUtility.fastInvert(originalTrans, orgTransInv);
		
		if(this.originalFixedObject != null){
			this.originalFixedObject.setTransform(new Matrix4f(this.currFixedObject.getTransform()));
			hinge.setFixedObject(this.originalFixedObject);
			orgmodel.removeObject(this.originalFixedObject);
		}else
			hinge.setFixedObject(null);
		
		if(this.originalHangingObject != null){
			this.originalHangingObject.setTransform(new Matrix4f(this.currHangingObject.getTransform()));
			hinge.setHangingObject(this.originalHangingObject);
			orgmodel.removeObject(this.originalHangingObject);
		}else
			hinge.setHangingObject(null);

		originalObject.refresh();
		getInterface().refreshObject(originalObject);
		
		// hinge 고유의 특성 설정
		hinge.setHingeGap(this.currHinge.getHingeGap());
		hinge.setHingeHeight(this.currHinge.getHingeHeight());
		if(this.originalFixedObject == null)
			BugManager.printf("HingeEditor : finish : orgnlFixedObj is null\n");
		else
			BugManager.printf("HingeEditor : finish : orgnlFixedObj.childCnt : %d\n", this.originalFixedObject.getChildCount());
		this.objViewer.setObject(null);
		
		this.originalFixedObject = null;
		this.originalHangingObject = null;
		this.originalObject = null;
		this.currFixedObject = null;
		this.currHangingObject = null;
		this.currHinge = null;
		this.currHingeObject = null;
		this.originalObject = null;
		this.setVisible(false);
		BugManager.printf("HingeEditor : finish : hinge.getFixedObj(): %s\n", hinge.getFixedObject());
		getInterface().setCurrentCompEditor(null);
	}// end finishEditing
	
	public SkObject getObject()
	{ return this.originalObject; }
}
