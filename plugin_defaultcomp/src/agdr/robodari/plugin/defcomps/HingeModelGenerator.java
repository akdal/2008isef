package agdr.robodari.plugin.defcomps;

import java.io.*;
import javax.vecmath.*;

import agdr.robodari.appl.BugManager;
import agdr.robodari.comp.model.*;
import agdr.robodari.plugin.rme.edit.*;

public class HingeModelGenerator implements ModelGenerator<Hinge>{
	public RobotCompModel generate(Hinge hinge) throws Exception{
		RobotCompModel model = new RobotCompModel();
		model.setGroup(true);
		
		SkObject hobj = hinge.getHangingObject();
		if(hobj != null){
			// [1]
			if(hobj.isGroup()){
				RobotCompModel imodel = hobj.getIntegratedModel();
				model.addSubModel(imodel, hobj.getTransform());
			}else
				model.addSubModel(hobj.getModel(), hobj.getTransform());
		}
		
		SkObject fobj = hinge.getFixedObject();
		if(fobj != null){
			// [2]
			if(fobj.isGroup())
				model.addSubModel(fobj.getIntegratedModel(), fobj.getTransform());
			else
				model.addSubModel(fobj.getModel(), fobj.getTransform());
		}
		
		StringBuffer axiscode = new StringBuffer();
		String bl = " ", prefix1 = "v 0.05 ", prefix2 = "v -0.05 ";
		float xcoord = hinge.getHingeGap() / 2.0f;
		axiscode.append(prefix1).append(-xcoord + 0.05).append(" 0\n")
		        .append(prefix1).append(-xcoord - 0.05).append(" 0\n")
		        .append(prefix1).append(-xcoord - 0.05).append(bl).append(hinge.getHingeHeight() + 0.05).append("\n")
		        .append(prefix1).append(xcoord + 0.05).append(bl).append(hinge.getHingeHeight() + 0.05).append("\n")
		        .append(prefix1).append(xcoord + 0.05).append(" 0\n")
		        .append(prefix1).append(xcoord - 0.05).append(" 0\n")
		        .append(prefix1).append(xcoord - 0.05).append(bl).append(hinge.getHingeHeight() - 0.05).append("\n")
		        .append(prefix1).append(-xcoord + 0.05).append(bl).append(hinge.getHingeHeight() - 0.05).append("\n")
		        
		        .append(prefix2).append(-xcoord + 0.05).append(" 0\n")
		        .append(prefix2).append(-xcoord - 0.05).append(" 0\n")
		        .append(prefix2).append(-xcoord - 0.05).append(bl).append(hinge.getHingeHeight() + 0.05).append("\n")
		        .append(prefix2).append(xcoord + 0.05).append(bl).append(hinge.getHingeHeight() + 0.05).append("\n")
		        .append(prefix2).append(xcoord + 0.05).append(" 0\n")
		        .append(prefix2).append(xcoord - 0.05).append(" 0\n")
		        .append(prefix2).append(xcoord - 0.05).append(bl).append(hinge.getHingeHeight() - 0.05).append("\n")
		        .append(prefix2).append(-xcoord + 0.05).append(bl).append(hinge.getHingeHeight() - 0.05).append("\n");
		
		axiscode.append("f 1 2 10 9\n")
		        .append("f 8 7 6 5 4 3 2 1\n")
		        .append("f 9 10 11 12 13 14 15 16\n")
		        .append("f 5 6 14 13\n")
		        .append("f 2 3 11 10\n")
		        .append("f 3 4 12 11\n")
		        .append("f 4 5 13 12\n")
		        .append("f 1 9 16 8\n")
		        .append("f 7 8 16 15\n")
		        .append("f 7 15 14 6\n");
		RobotCompModel axismodel = new RobotCompModel();
		axismodel.load(new StringReader(axiscode.toString()));
		axismodel.getApprData().setColor(java.awt.Color.gray);
		axismodel.getPhysicalData().centerMassPoint = new Point3f(0, 0, 0);
		Matrix4f axistrans = new Matrix4f();
		axistrans.m00 = axistrans.m11 = axistrans.m22 = axistrans.m33 = 1;
		// [3]
		model.addSubModel(axismodel, axistrans);
		BugManager.printf("HingeMGen : gen : %s : model.childCount : %d\n", hinge.getName(), model.getSubModelCount());
		return model;
	}// emnd generate
}
