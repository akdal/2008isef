package agdr.robodari.plugin.defcomps;

import java.awt.*;
import javax.swing.*;

import agdr.robodari.comp.*;
import agdr.robodari.edit.*;

import java.awt.Color;



public class LED1 implements LED{
	public final static double PLUS_DEFAULTLEN = 3.0;
	public final static double MINUS_DEFAULTLEN = 2.0;
	public final static double RADIUS_DEFAULT = 1;
	
	private double plusLen = PLUS_DEFAULTLEN, minusLen = MINUS_DEFAULTLEN;
	private double radius = RADIUS_DEFAULT;
	private Color color = Color.red;
	private Note note = new Note();
	private Receiver[] receivers = new Receiver[]{new LED1Receiver(this)};
	private String name;
	private RobotComponent parent;
	
	
	public Color getColor()
	{ return color; }
	public void setColor(Color color)
	{ this.color = color; }
	
	public double getRadius()
	{ return radius; }
	public void setRadius(double r)
	{ this.radius = r; }
	
	public Note getNote()
	{ return note; }
	public void setNote(Note n)
	{ this.note = n; }
	
	
	public String getName()
	{ return name; }
	public void setName(String name)
	{ this.name = name; }
	
	
	public double getPlusRodLength()
	{ return plusLen; }
	public void setPlusRodLength(double len)
	{ this.plusLen = len; }
	public double getMinusRodLength()
	{ return minusLen; }
	public void setMinusRodLength(double len)
	{ this.minusLen = len; }

	public boolean hasSubComponent()
	{ return false; }
	public RobotComponent[] getSubComponents()
	{ return null; }
	public boolean hasSender()
	{ return false; }
	public boolean hasReceiver()
	{ return true; }
	public Sender[] getSenders()
	{ return null; }
	public Receiver[] getReceivers()
	{ return receivers; }
	
	
	public RobotComponent getParent()
	{ return parent; }
	public void setParent(RobotComponent p){
		this.parent = p;
	}// end setParent(RobotComponent)
	
	
	
	public static class LED1Receiver implements Receiver, java.io.Serializable{
		private final static byte[] ICON_DATA = new byte[]
		    {71, 73, 70, 56, 57, 97, 17, 0, 17, 0, -9, 0, 0, 0, 0, 0, -128
			, 0, 0, 0, -128, 0, -128, -128, 0, 0, 0, -128, -128, 0, -128, 0, -128
			, -128, -128, -128, -128, -64, -64, -64, -1, 0, 0, 0, -1, 0, -1, -1, 0
			, 0, 0, -1, -1, 0, -1, 0, -1, -1, -1, -1, -1, 0, 0, 0, 0
			, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
			, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
			, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
			, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
			, 0, 0, 0, 0, 0, 0, 0, 0, 0, 51, 0, 0, 102, 0, 0, -103
			, 0, 0, -52, 0, 0, -1, 0, 51, 0, 0, 51, 51, 0, 51, 102, 0
			, 51, -103, 0, 51, -52, 0, 51, -1, 0, 102, 0, 0, 102, 51, 0, 102
			, 102, 0, 102, -103, 0, 102, -52, 0, 102, -1, 0, -103, 0, 0, -103, 51
			, 0, -103, 102, 0, -103, -103, 0, -103, -52, 0, -103, -1, 0, -52, 0, 0
			, -52, 51, 0, -52, 102, 0, -52, -103, 0, -52, -52, 0, -52, -1, 0, -1
			, 0, 0, -1, 51, 0, -1, 102, 0, -1, -103, 0, -1, -52, 0, -1, -1
			, 51, 0, 0, 51, 0, 51, 51, 0, 102, 51, 0, -103, 51, 0, -52, 51
			, 0, -1, 51, 51, 0, 51, 51, 51, 51, 51, 102, 51, 51, -103, 51, 51
			, -52, 51, 51, -1, 51, 102, 0, 51, 102, 51, 51, 102, 102, 51, 102, -103
			, 51, 102, -52, 51, 102, -1, 51, -103, 0, 51, -103, 51, 51, -103, 102, 51
			, -103, -103, 51, -103, -52, 51, -103, -1, 51, -52, 0, 51, -52, 51, 51, -52
			, 102, 51, -52, -103, 51, -52, -52, 51, -52, -1, 51, -1, 0, 51, -1, 51
			, 51, -1, 102, 51, -1, -103, 51, -1, -52, 51, -1, -1, 102, 0, 0, 102
			, 0, 51, 102, 0, 102, 102, 0, -103, 102, 0, -52, 102, 0, -1, 102, 51
			, 0, 102, 51, 51, 102, 51, 102, 102, 51, -103, 102, 51, -52, 102, 51, -1
			, 102, 102, 0, 102, 102, 51, 102, 102, 102, 102, 102, -103, 102, 102, -52, 102
			, 102, -1, 102, -103, 0, 102, -103, 51, 102, -103, 102, 102, -103, -103, 102, -103
			, -52, 102, -103, -1, 102, -52, 0, 102, -52, 51, 102, -52, 102, 102, -52, -103
			, 102, -52, -52, 102, -52, -1, 102, -1, 0, 102, -1, 51, 102, -1, 102, 102
			, -1, -103, 102, -1, -52, 102, -1, -1, -103, 0, 0, -103, 0, 51, -103, 0
			, 102, -103, 0, -103, -103, 0, -52, -103, 0, -1, -103, 51, 0, -103, 51, 51
			, -103, 51, 102, -103, 51, -103, -103, 51, -52, -103, 51, -1, -103, 102, 0, -103
			, 102, 51, -103, 102, 102, -103, 102, -103, -103, 102, -52, -103, 102, -1, -103, -103
			, 0, -103, -103, 51, -103, -103, 102, -103, -103, -103, -103, -103, -52, -103, -103, -1
			, -103, -52, 0, -103, -52, 51, -103, -52, 102, -103, -52, -103, -103, -52, -52, -103
			, -52, -1, -103, -1, 0, -103, -1, 51, -103, -1, 102, -103, -1, -103, -103, -1
			, -52, -103, -1, -1, -52, 0, 0, -52, 0, 51, -52, 0, 102, -52, 0, -103
			, -52, 0, -52, -52, 0, -1, -52, 51, 0, -52, 51, 51, -52, 51, 102, -52
			, 51, -103, -52, 51, -52, -52, 51, -1, -52, 102, 0, -52, 102, 51, -52, 102
			, 102, -52, 102, -103, -52, 102, -52, -52, 102, -1, -52, -103, 0, -52, -103, 51
			, -52, -103, 102, -52, -103, -103, -52, -103, -52, -52, -103, -1, -52, -52, 0, -52
			, -52, 51, -52, -52, 102, -52, -52, -103, -52, -52, -52, -52, -52, -1, -52, -1
			, 0, -52, -1, 51, -52, -1, 102, -52, -1, -103, -52, -1, -52, -52, -1, -1
			, -1, 0, 0, -1, 0, 51, -1, 0, 102, -1, 0, -103, -1, 0, -52, -1
			, 0, -1, -1, 51, 0, -1, 51, 51, -1, 51, 102, -1, 51, -103, -1, 51
			, -52, -1, 51, -1, -1, 102, 0, -1, 102, 51, -1, 102, 102, -1, 102, -103
			, -1, 102, -52, -1, 102, -1, -1, -103, 0, -1, -103, 51, -1, -103, 102, -1
			, -103, -103, -1, -103, -52, -1, -103, -1, -1, -52, 0, -1, -52, 51, -1, -52
			, 102, -1, -52, -103, -1, -52, -52, -1, -52, -1, -1, -1, 0, -1, -1, 51
			, -1, -1, 102, -1, -1, -103, -1, -1, -52, -1, -1, -1, 44, 0, 0, 0
			, 0, 17, 0, 17, 0, 0, 8, 89, 0, 17, 8, 28, 72, -80, -32, -64
			, 7, 8, 19, 42, 92, 40, 112, -95, 67, 0, 9, 27, 62, 0, 0, -15
			, 97, 68, 4, 8, 33, 82, 92, 88, -15, -127, -60, -114, 27, 51, 94, 20
			, -87, 112, 99, -57, -113, 14, 39, -98, 68, 112, -32, 64, -57, -110, 0, 90
			, 30, 64, 9, 115, -30, 72, -101, 25, 65, -34, -44, -8, 18, -89, 71, -116
			, 42, 83, -82, 76, 89, -14, 38, 81, -122, 64, -113, 42, 52, -56, -76, 96
			, 64, 0, 59};
		private final static ImageIcon ICON = new ImageIcon(Toolkit.getDefaultToolkit().createImage(ICON_DATA));
		
		
		private LED1 led;
		private Note note;
		private String name = "C1";
		private boolean enabled = true;
		private RobotComponent parent;
		
		
		public LED1Receiver(LED1 led)
		{ this.led = led; }

		
		public LED1 getParent()
		{ return led; }
		public Icon getSimpleIcon()
		{ return ICON; }
		
		public Note getNote()
		{ return note; }
		public void setNote(Note note)
		{ this.note = note; }
		
		public boolean isEnabled()
		{ return enabled; }
		public void setEnabled(boolean e)
		{ this.enabled = e; }
		
		public boolean hasSubComponent()
		{ return false; }
		public boolean hasReceiver()
		{ return false; }
		public boolean hasSender()
		{ return false; }
		public Receiver[] getReceivers()
		{ return null; }
		public Sender[] getSenders()
		{ return null; }
		
		public RobotComponent[] getSubComponents()
		{ return null; }
		
		public void setParent(RobotComponent p){
			this.parent = p;
		}// end setParent(RobotComponent)
		
		public String getCondition()
		{ return "Must be connected to power-suppliable."; }
		
		
		public String getName()
		{ return name; }
		public void setName(String n)
		{ this.name = n; }
		
		public boolean suppliesPower()
		{ return false; }
		public PowerSupply getPowerSupply()
		{ return null; }
		
		public boolean isCompatible(Sender sender){
			if(sender == null)
				 return true;
			return false;
		}// end isCompatible(Sender, RobotComponent)
		
		public boolean hasEditor()
		{ return false; }
		public void showEditor(EditorManager em)
		{}
	}// end class LED1Receiver
}

