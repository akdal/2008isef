package agdr.robodari.plugin.defcomps;

import javax.vecmath.*;


import agdr.library.math.MathUtility;
import agdr.robodari.comp.model.AppearanceData;
import agdr.robodari.comp.model.ModelGenerator;
import agdr.robodari.comp.model.PhysicalData;
import agdr.robodari.comp.model.RobotCompModel;
import agdr.robodari.plugin.rme.rlztion.*;

import java.io.StringReader;

public class LED1ModelGenerator implements ModelGenerator<LED1>{
	public RobotCompModel generate(LED1 model) throws Exception{
		double pluslen = model.getPlusRodLength();
		double minuslen = model.getMinusRodLength();
		
		StringBuffer buf = new StringBuffer();
		double r = model.getRadius();
		
		buf.append("g LED\n");
		buf.append("# 가장 위\n");
		buf.append("v " + 0.075 * r + " " + 0.0 * r + " " + 0.5 * r + "\n");
		buf.append("v " + 0.05303300 * r + " " + 0.05303300 * r + " " + 0.5 * r + "\n");
		buf.append("v " + 0 * r + " " + 0.075 * r + " " + 0.5 * r + "\n");
		buf.append("v " + -0.05303300 * r + " " + 0.05303300 * r + " " + 0.5 * r + "\n");
		buf.append("v " + -0.075 * r + " " + 0 * r + " " + 0.5 * r + "\n");
		buf.append("v " + -0.05303300 * r + " " + -0.05303300 * r + " " + 0.5 * r + "\n");
		buf.append("v " + 0 * r + " " + -0.075 * r + " " + 0.5 * r + "\n");
		buf.append("v " + 0.05303300 * r + " " + -0.05303300 * r + " " + 0.5 * r + "\n");
		buf.append("# 8 \n");
		buf.append("\n");
		buf.append("# 그 다음\n");
		buf.append("v " + 0.1200154 * r + " " + 0.04971203 * r + " " + 0.45 * r + "\n");
		buf.append("v " + 0.04971203 * r + " " + 0.1200154 * r + " " + 0.45 * r + "\n");
		buf.append("v " + -0.04971203 * r + " " + 0.1200154 * r + " " + 0.45 * r + "\n");
		buf.append("v " + -0.1200154 * r + " " + 0.04971205 * r + " " + 0.45 * r + "\n");
		buf.append("v " + -0.1200154 * r + " " + -0.04971203 * r + " " + 0.45 * r + "\n");
		buf.append("v " + -0.04971203 * r + " " + -0.1200154 * r + " " + 0.45 * r + "\n");
		buf.append("v " + 0.04971203 * r + " " + -0.1200154 * r + " " + 0.45 * r + "\n");
		buf.append("v " + 0.1200154 * r + " " + -0.049712035 * r + " " + 0.45 * r + "\n");
		buf.append("# 16\n");
		buf.append("\n");
		buf.append("# 세 번째\n");
		buf.append("v " + 0.15 * r + " " + 0.0 * r + " " + 0.4 * r + "\n");
		buf.append("v " + 0.1060660 * r + " " + 0.1060660 * r + " " + 0.4 * r + "\n");
		buf.append("v " + 0 * r + " " + 0.15 * r + " " + 0.4 * r + "\n");
		buf.append("v " + -0.1060660 * r + " " + 0.1060660 * r + " " + 0.4 * r + "\n");
		buf.append("v " + -0.15 * r + " " + 0 * r + " " + 0.4 * r + "\n");
		buf.append("v " + -0.1060660 * r + " " + -0.1060661 * r + " " + 0.4 * r + "\n");
		buf.append("v " + 0 * r + " " + -0.15 * r + " " + 0.4 * r + "\n");
		buf.append("v " + 0.1060660 * r + " " + -0.1060660 * r + " " + 0.4 * r + "\n");
		buf.append("# 24\n");
		buf.append("\n");
		buf.append("# 아래에서 두번째\n");
		buf.append("v " + 0.15 * r + " " + 0.0 * r + " " + 0.1 * r + "\n");
		buf.append("v " + 0.1060660 * r + " " + 0.1060660 * r + " " + 0.1 * r + "\n");
		buf.append("v " + 0 * r + " " + 0.15 * r + " " + 0.1 * r + "\n");
		buf.append("v " + -0.1060660 * r + " " + 0.1060660 * r + " " + 0.1 * r + "\n");
		buf.append("v " + -0.15 * r + " " + 0 * r + " " + 0.1 * r + "\n");
		buf.append("v " + -0.1060660 * r + " " + -0.1060660 * r + " " + 0.1 * r + "\n");
		buf.append("v " + 0 * r + " " + -0.15 * r + " " + 0.1 * r + "\n");
		buf.append("v " + 0.1060660 * r + " " + -0.1060660 * r + " " + 0.1 * r + "\n");
		buf.append("# 32\n");
		buf.append("\n");
		buf.append("# ..\n");
		buf.append("v " + 0.175 * r + " " + 0.0 * r + " " + 0.1 * r + "\n");
		buf.append("v " + 0.1237436 * r + " " + 0.1237436 * r + " " + 0.1 * r + "\n");
		buf.append("v " + 0 * r + " " + 0.175 * r + " " + 0.1 * r + "\n");
		buf.append("v " + -0.1237436 * r + " " + 0.1237436 * r + " " + 0.1 * r + "\n");
		buf.append("v " + -0.175 * r + " " + 0 * r + " " + 0.1 * r + "\n");
		buf.append("v " + -0.1237436 * r + " " + -0.123743 * r + " " + 0.1 * r + "\n");
		buf.append("v " + 0 * r + " " + -0.15 * r + " " + 0.1 * r + "\n");
		buf.append("v " + 0.1237436 * r + " " + -0.1237436 * r + " " + 0.1 * r + "\n");
		buf.append("# 40\n");
		buf.append("\n");
		buf.append("# 가장 아래\n");
		buf.append("v " + 0.175 * r + " " + 0.0 * r + " " + 0 * r + "\n");
		buf.append("v " + 0.1237436 * r + " " + 0.1237436 * r + " " + 0 * r + "\n");
		buf.append("v " + 0 * r + " " + 0.175 * r + " " + 0 * r + "\n");
		buf.append("v " + -0.1237436 * r + " " + 0.1237436 * r + " " + 0 * r + "\n");
		buf.append("v " + -0.175 * r + " " + 0 * r + " " + 0 * r + "\n");
		buf.append("v " + -0.1237436 * r + " " + -0.1237436 * r + " " + 0 * r + "\n");
		buf.append("v " + 0 * r + " " + -0.15 * r + " " + 0 * r + "\n");
		buf.append("v " + 0.1237436 * r + " " + -0.1237436 * r + " " + 0 * r + "\n"); // FIXME
		buf.append("# 48\n");
		buf.append("\n");
		buf.append("\n");
		buf.append("# +극\n");
		buf.append("v " + -0.15 * r + " " + 0.025 * r + " " + 0 * r + "\n");
		buf.append("v " + -0.15 * r + " " + -0.025 * r + " " + 0 * r + "\n");
		buf.append("v " + -0.10 * r + " " + -0.025 * r + " " + 0 * r + "\n");
		buf.append("v " + -0.10 * r + " " + 0.025 * r + " " + 0 * r + "\n");
		buf.append("v " + -0.15 * r + " " + 0.025 * r + " " + -(float)pluslen + "\n");
		buf.append("v " + -0.15 * r + " " + -0.025 * r + " " + -(float)pluslen + "\n");
		buf.append("v " + -0.10 * r + " " + -0.025 * r + " " + -(float)pluslen + "\n");
		buf.append("v " + -0.10 * r + " " + 0.025 * r + " " + -(float)pluslen + "\n");
		buf.append("# 56\n");
		buf.append("# -극\n");
		buf.append("v " + 0.10 * r + " " + 0.025 * r + " " + 0 * r + "\n");
		buf.append("v " + 0.10 * r + " " + -0.025 * r + " " + 0 * r + "\n");
		buf.append("v " + 0.15 * r + " " + -0.025 * r + " " + 0 * r + "\n");
		buf.append("v " + 0.15 * r + " " + 0.025 * r + " " + 0 * r + "\n");
		buf.append("v " + 0.10 * r + " " + 0.025 * r + " " + -(float)minuslen + "\n");
		buf.append("v " + 0.10 * r + " " + -0.025 * r + " " + -(float)minuslen + "\n");
		buf.append("v " + 0.15 * r + " " + -0.025 * r + " " + -(float)minuslen + "\n");
		buf.append("v " + 0.15 * r + " " + 0.025 * r + " " + -(float)minuslen + "\n");
		buf.append("# 64\n");
		buf.append("\n");
		
		
		buf.append("\n");
		buf.append("f 1 2 3 4 5 6 7 8\n");
		buf.append("\n");
		buf.append("f 9 1 16\n");
		buf.append("f 9 2 1\n");
		buf.append("f 9 10 2\n");
		buf.append("f 10 3 2\n");
		buf.append("f 3 10 11\n");
		buf.append("f 3 11 4\n");
		buf.append("f 4 11 12\n");
		buf.append("f 4 12 5\n");
		buf.append("f 5 12 13\n");
		buf.append("f 5 13 6\n");
		buf.append("f 6 13 14\n");
		buf.append("f 6 14 7\n");
		buf.append("f 7 14 15\n");
		buf.append("f 8 7 15\n");
		buf.append("f 8 15 16\n");
		buf.append("f 8 16 1\n");
		buf.append("\n");
		buf.append("f 17 9 16\n");
		buf.append("f 18 9 17\n");
		buf.append("f 9 18 10\n");
		buf.append("f 10 18 19\n");
		buf.append("f 10 19 11\n");
		buf.append("f 11 19 20\n");
		buf.append("f 11 20 12\n");
		buf.append("f 12 20 21\n");
		buf.append("f 21 13 12\n");
		buf.append("f 13 21 22\n");
		buf.append("f 13 22 14\n");
		buf.append("f 14 22 23\n");
		buf.append("f 14 23 15\n");
		buf.append("f 15 23 24\n");
		buf.append("f 15 24 16\n");
		buf.append("f 24 17 16\n");
		buf.append("\n");
		buf.append("f 17 25 26 18\n");
		buf.append("f 18 26 27 19\n");
		buf.append("f 19 27 28 20\n");
		buf.append("f 20 28 29 21\n");
		buf.append("f 21 29 30 22\n");
		buf.append("f 22 30 31 23 \n");
		buf.append("f 23 31 32 24\n");
		buf.append("f 24 32 25 17\n");
		buf.append("\n");
		buf.append("f 25 33 34 26\n");
		buf.append("f 26 34 35 27\n");
		buf.append("f 27 35 36 28\n");
		buf.append("f 28 36 37 29\n");
		buf.append("f 29 37 38 30\n");
		buf.append("f 30 38 39 31\n");
		buf.append("f 31 39 40 26\n");
		buf.append("f 32 40 33 25\n");
		buf.append("\n");
		buf.append("f 33 41 42 34\n");
		buf.append("f 34 42 43 35\n");
		buf.append("f 35 43 44 36\n");
		buf.append("f 36 44 45 37\n");
		buf.append("f 37 45 46 38\n");
		buf.append("f 38 46 47 39\n");
		buf.append("f 39 47 48 40\n");
		buf.append("f 40 48 41 33\n");
		buf.append("\n");
		buf.append("f 45 44 43 42 41 60 57 52 49\n");
		buf.append("f 41 48 47 46 45 49 50 51 52 57 58 59 60\n");
		buf.append("\n");
		buf.append("f 53 54 50 49\n");
		buf.append("f 56 55 54 53\n");
		buf.append("f 52 51 55 56\n");
		buf.append("f 50 54 55 51\n");
		buf.append("f 52 56 53 49\n");
		buf.append("\n");
		buf.append("f 61 62 58 57\n");
		buf.append("f 64 63 62 61\n");
		buf.append("f 60 59 63 64\n");
		buf.append("f 58 62 63 59\n");
		buf.append("f 60 64 61 57\n");
		
		StringReader sr = new StringReader(buf.toString());
		RobotCompModel str = new RobotCompModel();
		str.load(sr);
		
		PhysicalData pd = str.getPhysicalData();
		Point3f[] ps = str.getPoints();
		int[][] fs = str.getFaces();
		pd.mass = 1;
		pd.volume = MathUtility.getVolume(ps, fs);
		pd.bounds = null;
		pd.centerMassPoint = MathUtility.getCenterMassPoint(ps, fs, pd.centerMassPoint);
		pd.inertia = new Matrix3f();
		MathUtility.addInertiaTensor(ps, fs,
				pd.mass, pd.centerMassPoint.x, pd.centerMassPoint.y,
				pd.centerMassPoint.z, pd.inertia);

		AppearanceData ap = str.getApprData();
		ap.setColor(model.getColor());
		
		
		return str;
	}// end generate(String[][])
}
