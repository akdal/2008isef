package agdr.robodari.plugin.defcomps;

import java.awt.*;
import javax.swing.Icon;

import agdr.robodari.comp.*;

public abstract class OnePin implements Sender, Receiver{
	public final static float DEFAULT_PINLENGTH = 0.5F;
	
	private Note note = new Note();
	private float pinLength = DEFAULT_PINLENGTH;
	private boolean enabled;
	private Icon simpleIcon = new OnePinIcon();
	private String name;
	
	
	
	public String getName()
	{ return name; }
	public void setName(String name)
	{ this.name = name; }
	
	public Icon getSimpleIcon()
	{ return simpleIcon; }
	
	public boolean isEnabled()
	{ return enabled; }
	public void setEnabled(boolean enabled)
	{ this.enabled = enabled; }
	
	public Note getNote()
	{ return note; }
	public void setNote(Note n)
	{ this.note = n; }
	public float getPinLength()
	{ return pinLength; }
	
	public void setPinLength(float pl)
	{ this.pinLength = pl; }

	public boolean hasSubComponent()
	{ return false; }
	public boolean hasReceiver()
	{ return false; }
	public boolean hasSender()
	{ return false; }
	public RobotComponent[] getSubComponents()
	{ return null; }
	public Receiver[] getReceivers()
	{ return null; }
	public Sender[] getSenders()
	{ return null; }
	
	
	private static class OnePinIcon implements Icon, java.io.Serializable{
		public int getIconWidth()
		{ return 11; }
		public int getIconHeight()
		{ return 11; }
		public void paintIcon(Component c, Graphics g, int x, int y){
			g.drawRect(x + 1, y + 1, 9, 9);
			g.drawRect(x + 5, y + 5, 3, 3);
		}// end paintIcon(Component, Graphics, int, int)
	}// end class OnePinIcon
}
