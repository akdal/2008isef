package agdr.robodari.plugin.defcomps;

import java.io.StringReader;

import javax.vecmath.*;


import agdr.library.math.MathUtility;
import agdr.robodari.comp.model.AppearanceData;
import agdr.robodari.comp.model.ModelGenerator;
import agdr.robodari.comp.model.PhysicalData;
import agdr.robodari.comp.model.RobotCompModel;


public class OnePinModelGenerator implements ModelGenerator<OnePin>{
	public RobotCompModel generate(OnePin model) throws Exception{
		double left, top;
		StringBuffer code = new StringBuffer();
		
		left = 0;
		top = 0;
		
		code.append("v ").append(left).append(" ").append(top).
			append(" 0\n")
		.append("v ").append(left + 0.246).append(" ").append(top).
			append(" 0\n")
		.append("v ").append(left + 0.246).append(" ").append(top + 0.246).
			append(" 0\n")
		.append("v ").append(left).append(" ").append(top + 0.246).
			append(" 0\n")
		
		.append("v ").append(left).append(" ").append(top).
			append(" 0.25\n")
		.append("v ").append(left + 0.246).append(" ").append(top).
			append(" 0.25\n")
		.append("v ").append(left + 0.246).append(" ").append(top + 0.246).
			append(" 0.25\n")
		.append("v ").append(left).append(" ").append(top + 0.246).
			append(" 0.25\n")
		
		.append("v ").append(left + 0.10).append(" ").append(top + 0.10).
			append(" 0.25\n")
		.append("v ").append(left + 0.15).append(" ").append(top + 0.10).
			append(" 0.25\n")
		.append("v ").append(left + 0.15).append(" ").append(top + 0.15).
			append(" 0.25\n")
		.append("v ").append(left + 0.10).append(" ").append(top + 0.15).
			append(" 0.25\n")
			
		.append("v ").append(left + 0.10).append(" ").append(top + 0.10).
			append(" 0.85\n")
		.append("v ").append(left + 0.15).append(" ").append(top + 0.10).
			append(" 0.85\n")
		.append("v ").append(left + 0.15).append(" ").append(top + 0.15).
			append(" 0.85\n")
		.append("v ").append(left + 0.10).append(" ").append(top + 0.15).
			append(" 0.85\n")
				
		.append("v ").append(left + 0.10).append(" ").append(top + 0.10).
			append(" 0\n")
		.append("v ").append(left + 0.15).append(" ").append(top + 0.10).
			append(" 0\n")
		.append("v ").append(left + 0.15).append(" ").append(top + 0.15).
			append(" 0\n")
		.append("v ").append(left + 0.10).append(" ").append(top + 0.15).
			append(" 0\n")
		
		.append("v ").append(left + 0.10).append(" ").append(top + 0.10).
			append(" ").append(-1.0 * model.getPinLength())
			.append("\n")
		.append("v ").append(left + 0.15).append(" ").append(top + 0.10).
			append(" ").append(-1.0 * model.getPinLength())
			.append("\n")
		.append("v ").append(left + 0.15).append(" ").append(top + 0.15).
			append(" ").append(-1.0 * model.getPinLength())
			.append("\n")
		.append("v ").append(left + 0.10).append(" ").append(top + 0.15).
			append(" ").append(-1.0 * model.getPinLength())
			.append("\n")
		.append("\n");

		append(model, code, 18, 19, 20, 17, 1, 4, 3, 2, 1, 17);
		append(model, code, 2, 3, 7, 6);
		append(model, code, 3, 4, 8, 7);
		append(model, code, 1, 2, 6, 5);
		append(model, code, 1, 5, 8, 4);
		append(model, code, 5, 12, 11, 10, 9, 12, 5, 6, 7, 8, 5);
		append(model, code, 13, 14, 15, 16);
		append(model, code, 10, 11, 15, 14);
		append(model, code, 11, 12, 16, 15);
		append(model, code, 9, 10, 14, 13);
		append(model, code, 9, 13, 16, 12);
		append(model, code, 24, 23, 22, 21);
		append(model, code, 22, 23, 19, 18);
		append(model, code, 23, 24, 20, 19);
		append(model, code, 21, 22, 18, 17);
		append(model, code, 24, 21, 17, 20);
		code.append("\n");
		
		StringReader sr = new StringReader(code.toString());
		RobotCompModel str = new RobotCompModel();
		str.load(sr);

		
		PhysicalData pd = str.getPhysicalData();
		Point3f[] ps = str.getPoints();
		int[][] fs = str.getFaces();
		pd.mass = 0.1f;
		pd.volume = MathUtility.getVolume(ps, fs);
		pd.bounds = null;
		pd.centerMassPoint = MathUtility.getCenterMassPoint(ps, fs, pd.centerMassPoint);
		pd.inertia = new Matrix3f();
		MathUtility.addInertiaTensor(ps, fs,
				pd.mass, pd.centerMassPoint.x, pd.centerMassPoint.y,
				pd.centerMassPoint.z, pd.inertia);

		AppearanceData ap = str.getApprData();
		ap.setColor(java.awt.Color.gray);
		
		return str;
	}// end generate(ThreePin)
	

	private void append(OnePin m, StringBuffer buf, 
			int... coords){
		buf.append("f");
		for(Integer eachi : coords)
			buf.append(" ").append(eachi);
		buf.append("\n");
	}// edn append(StringBuffer, int, int, int...)
}