package agdr.robodari.plugin.defcomps;

import java.awt.*;
import javax.swing.*;

import agdr.robodari.comp.*;

public class PowerConnector1 implements Receiver{
	public final static Icon ICON = new PowerConnector1Icon();
	public final static String CONDITION = "";
	
	private Note note;
	private String name = "PowerConn";
	private RobotComponent parent;
	
	
	public Note getNote()
	{ return note; }
	public void setNote(Note note)
	{ this.note = note; }
	
	public boolean hasSubComponent()
	{ return false; }
	public boolean hasReceiver()
	{ return false; }
	public boolean hasSender()
	{ return false; }
	public Receiver[] getReceivers()
	{ return null; }
	public RobotComponent[] getSubComponents()
	{ return null; }
	public Sender[] getSenders()
	{ return null; }
	
	public RobotComponent getParent()
	{ return parent; }
	public void setParent(RobotComponent p){
		this.parent = p;
	}// end setParent(RobotComponent)
	
	public boolean suppliesPower()
	{ return false; }
	public PowerSupply getPowerSupply()
	{ return null; }
	
	public boolean isEnabled()
	{ return true; }
	public Icon getSimpleIcon()
	{ return ICON; }
	
	public String getCondition()
	{ return CONDITION; }
	
	public boolean isCompatible(Sender sender){
		if(sender == null) return true;
		return sender.getParent() instanceof Battery;
	}// end isCompatible(Sender)
	
	public String getName()
	{ return name; }
	public void setName(String name)
	{ this.name = name; }
	
	
	static class PowerConnector1Icon implements Icon, java.io.Serializable{
		public int getIconWidth()
		{ return 19; }
		public int getIconHeight()
		{ return 19; }
		public void paintIcon(Component c, Graphics g, int x, int y){
			Color orgcol = g.getColor();
			g.setColor(Color.white);
			g.fillOval(x + 2, y + 2, 16, 16);
			g.setColor(orgcol);
			g.drawOval(x + 2, y + 2, 16, 16);
			g.drawLine(x + 9, y + 2, x + 9, y + 17);
		}// end paint(Component, Graphics, int, int)
	}// end class PowerConnectorIcon
}
