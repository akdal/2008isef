package agdr.robodari.plugin.defcomps;

import java.io.StringReader;

import javax.vecmath.*;


import agdr.library.math.MathUtility;
import agdr.robodari.comp.model.AppearanceData;
import agdr.robodari.comp.model.ModelGenerator;
import agdr.robodari.comp.model.PhysicalData;
import agdr.robodari.comp.model.RobotCompModel;
import agdr.robodari.plugin.rme.rlztion.*;

public class PowerConnector1ModelGenerator implements
ModelGenerator<PowerConnector1>{
	public RobotCompModel generate(PowerConnector1 model) throws Exception{
		StringBuffer buf = new StringBuffer();
		buf.append("v 0 0 0.65\n")
			.append("v 0 0 0\n")
			.append("v 1.05 0 0\n")
			.append("v 1.05 0 0.7\n")
			.append("v 0.9 0 0.7\n")
			.append("v 0.9 0 0.65\n")
			.append("# 6\n")
		
			.append("v 0 0.5 0.65\n")
			.append("v 0 0.5 0\n")
			.append("v 1.05 0.5 0\n")
			.append("v 1.05 0.5 0.7\n")
			.append("v 0.9 0.5 0.7\n")
			.append("v 0.9 0.5 0.65\n")
			.append("# 12\n")
		
			.append("v 0.9 0.175 0.6875\n")
			.append("v 0.9 0.25 0.7\n")
			.append("v 0.9 0.375 0.6875\n")
			.append("# 15\n")
		
			.append("v 0.3 0 0.65\n")
			.append("v 0.3 0.175 0.6875\n")
			.append("v 0.3 0.25 0.7\n")
			.append("v 0.3 0.375 0.6875\n")
			.append("v 0.3 0.5 0.65\n")
			.append("# 20\n")
		
			.append("f 1 2 3 4 5 6\n")
			.append("f 7 12 11 10 9 8\n")
			.append("f 8 9 3 2\n")
			.append("f 1 7 8 2\n")
			.append("f 4 3 9 10\n")
			.append("f 5 4 10 11\n")
			.append("f 1 16 20 7\n")
		
			.append("f 6 13 17 16\n")
			.append("f 17 13 14 18\n")
			.append("f 18 14 15 19\n")
			.append("f 19 15 12 20\n")
			.append("f 16 17 18 19 20\n")
		
			.append("f 6 5 14 13\n")
			.append("f 14 11 12 15\n");
		RobotCompModel str = new RobotCompModel();
		str.load(new StringReader(buf.toString()));
		
		PhysicalData pd = str.getPhysicalData();
		Point3f[] ps = str.getPoints();
		int[][] faces = str.getFaces();
		pd.mass = 1.982f;
		pd.volume = MathUtility.getVolume(ps, faces);
		pd.bounds = null;
		pd.centerMassPoint = MathUtility.getCenterMassPoint(ps, faces, pd.centerMassPoint);
		pd.inertia = new Matrix3f();
		MathUtility.addInertiaTensor(ps, faces,
				pd.mass, pd.centerMassPoint.x, pd.centerMassPoint.y,
				pd.centerMassPoint.z, pd.inertia);

		AppearanceData ap = str.getApprData();
		ap.setColor(java.awt.Color.black);
		
		
		return str;
	}// end generatE(PowerConnector1)
/*
* 

*/
}

