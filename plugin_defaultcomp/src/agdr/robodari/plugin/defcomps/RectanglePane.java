package agdr.robodari.plugin.defcomps;


import agdr.robodari.comp.*;
import agdr.robodari.plugin.rme.rlztion.RealizationModel;

public class RectanglePane extends StructureComponent{
	public final static float DEFAULT_WIDTH = 5;
	public final static float DEFAULT_HEIGHT = 4;
	public final static float DEFAULT_THICKNESS = 0.5f;
	public final static float DEFAULT_WEIGHT = 10;

	private float thickness = DEFAULT_THICKNESS;
	private float width = DEFAULT_WIDTH;
	private float height = DEFAULT_HEIGHT;
	private float weight = DEFAULT_WEIGHT;
	
	

	
	public boolean hasSubComponent()
	{ return false; }
	public RobotComponent[] getSubComponents()
	{ return null; }
	
	public float getWeight()
	{ return weight; }
	public void setWeight(float d)
	{ this.weight = d; }
	public float getHeight() {
		return height;
	}
	public void setHeight(float h) {
		this.height = h;
	}
	public float getThickness() {
		return thickness;
	}
	public void setThickness(float thickness) {
		this.thickness = thickness;
	}
	public float getWidth() {
		return width;
	}
	public void setWidth(float width) {
		this.width = width;
	}
}
