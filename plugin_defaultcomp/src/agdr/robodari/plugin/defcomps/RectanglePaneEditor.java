package agdr.robodari.plugin.defcomps;

import java.awt.*;
import javax.swing.*;
//import org.agdr.robodari.edit.*;

import agdr.robodari.appl.BugManager;
import agdr.robodari.comp.Note;
import agdr.robodari.gui.*;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.gui.CompEditor;

public class RectanglePaneEditor extends CompEditor{
	private JTabbedPane tabbedPane;
	
	private JPanel propertyPanel;
	private NoteEditor noteEditor;
	private JTextField nameField;
	private JTextField widthField;
	private JTextField heightField;
	private JTextField thicknessField;
	private JTextField weightField;
	private JCheckBox hasBoundBox;
	
	private SkObject object;
	

	public RectanglePaneEditor(SketchEditorInterface manager, Dialog parent){
		super(manager, parent, "Rectangle Pane Editor");
		__set();
	}// end Constructor(EditorManager)
	public RectanglePaneEditor(SketchEditorInterface manager, Frame parent){
		super(manager, parent, "Rectangle Pane Editor");
		__set();
	}// end Constructor(EditorManager)
	private void __set(){
		initPropertyPanel();
		tabbedPane = new JTabbedPane();
		tabbedPane.setTabPlacement(JTabbedPane.LEFT);
		tabbedPane.addTab("Properties", propertyPanel);
		
		super.getEditorPanel().setLayout(new BorderLayout());
		super.getEditorPanel().add(tabbedPane, BorderLayout.CENTER);
		
		super.setSize(490, 440);
		super.setResizable(false);
	}// end __set
	private void initPropertyPanel(){
		propertyPanel = new JPanel();
		propertyPanel.setLayout(null);
		
		nameField = new JTextField();
		JLabel nameLabel = new JLabel("Component name : ");
		nameLabel.setBounds(20, 20, 100, 25);
		nameField.setBounds(130, 20, 200, 25);
		
		widthField = new JTextField();
		heightField = new JTextField();
		thicknessField = new JTextField();
		weightField = new JTextField();
		JLabel widthLabel = new JLabel("Width(cm) : ");
		JLabel heightLabel = new JLabel("Height(cm) : ");
		JLabel thicknessLabel = new JLabel("Thickness(cm) : ");
		JLabel weightLabel = new JLabel("Weight(gram) : ");
		hasBoundBox = new JCheckBox("Has a boundary");
		
		thicknessLabel.setHorizontalAlignment(JLabel.RIGHT);
		weightLabel.setHorizontalAlignment(JLabel.RIGHT);
		widthLabel.setHorizontalAlignment(JLabel.RIGHT);
		heightLabel.setHorizontalAlignment(JLabel.RIGHT);
		
		widthLabel.setBounds(20, 55, 100, 25);
		widthField.setBounds(130, 55, 60, 25);
		heightLabel.setBounds(20, 90, 100, 25);
		heightField.setBounds(130, 90, 60, 25);
		thicknessLabel.setBounds(200, 55, 90, 25);
		thicknessField.setBounds(300, 55, 50, 25);
		weightLabel.setBounds(200, 90, 90, 25);
		weightField.setBounds(300, 90, 70, 25);
		
		
		noteEditor = new NoteEditor();
		noteEditor.setBounds(20, 120, 350, 210);
		
		hasBoundBox.setBounds(20, 340, 350, 30);
		
		propertyPanel.add(nameLabel);
		propertyPanel.add(nameField);
		
		propertyPanel.add(widthField);
		propertyPanel.add(heightField);
		propertyPanel.add(widthLabel);
		propertyPanel.add(heightLabel);
		propertyPanel.add(thicknessLabel);
		propertyPanel.add(thicknessField);
		propertyPanel.add(weightLabel);
		propertyPanel.add(weightField);
		propertyPanel.add(hasBoundBox);
		
		propertyPanel.add(noteEditor);
	}// end initPropertyPanel()
	
	
	public void startEditing(SkObject object){
		if(!(object.getRobotComponent() instanceof RectanglePane))
			return;
		
		this.object = object;

		RectanglePane rcomp = (RectanglePane)object.getRobotComponent();
		nameField.setText(rcomp.getName());
		if(rcomp.getNote() == null)
			rcomp.setNote(new Note());
		widthField.setText(String.valueOf(rcomp.getWidth()));
		thicknessField.setText(String.valueOf(rcomp.getThickness()));
		heightField.setText(String.valueOf(rcomp.getHeight()));
		weightField.setText(String.valueOf(rcomp.getWeight()));
		
		noteEditor.open(rcomp.getNote());
		hasBoundBox.setSelected(rcomp.hasBound());
	}// end startEditing(SkObject)
	public void finishEditing(){
		RectanglePane rcomp = (RectanglePane)object.getRobotComponent();
		rcomp.setNote(noteEditor.getNote());
		rcomp.setName(nameField.getText());
		rcomp.setBound(hasBoundBox.isSelected());
		
		try{
			float width = Float.parseFloat(widthField.getText());
			rcomp.setWidth(width);
		}catch(Exception excp){}
		try{
			float thickness = Float.parseFloat(thicknessField.getText());
			rcomp.setThickness(thickness);
		}catch(Exception excp){}
		try{
			float h = Float.parseFloat(heightField.getText());
			rcomp.setHeight(h);
		}catch(Exception excp){}
		try{
			float we = Float.parseFloat(weightField.getText());
			rcomp.setWeight(we);
		}catch(Exception excp){}
		
		SkObject skobj = getInterface().getModel().getObject(rcomp);
		try{
			skobj.refresh();
		}catch(Exception excp){
			BugManager.log(excp, true);
		}
		getInterface().refreshObject(skobj);
		
		this.object = null;
		this.setVisible(false);
		getInterface().setCurrentCompEditor(null);
	}// end finishEditing()
	public SkObject getObject()
	{ return object; }
}