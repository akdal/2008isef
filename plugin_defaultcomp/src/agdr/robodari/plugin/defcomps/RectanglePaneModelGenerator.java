package agdr.robodari.plugin.defcomps;

import javax.vecmath.*;


import agdr.robodari.comp.*;
import agdr.robodari.comp.model.AppearanceData;
import agdr.robodari.comp.model.BoundObject;
import agdr.robodari.comp.model.ModelGenerator;
import agdr.robodari.comp.model.PhysicalData;
import agdr.robodari.comp.model.RobotCompModel;
import agdr.robodari.plugin.rme.rlztion.*;

public class RectanglePaneModelGenerator
		implements ModelGenerator<RectanglePane>{
	public RobotCompModel generate (RectanglePane model) throws Exception{
		StringBuffer code = new StringBuffer();
		code.append("v 0 0 0\n")
			.append("v ").append(model.getWidth()).append(" 0 0\n")
			.append("v ").append(model.getWidth()).append(" ")
					.append(model.getHeight()).append(" 0\n")
			.append("v 0 ").append(model.getHeight()).append(" 0\n")
			.append("v 0 0 ").append(model.getThickness()).append("\n")
			.append("v ").append(model.getWidth()).append(" 0 ")
					.append(model.getThickness()).append("\n")
			.append("v ").append(model.getWidth()).append(" ")
					.append(model.getHeight()).append(" ")
					.append(model.getThickness()).append("\n")
			.append("v 0 ").append(model.getHeight()).append(" ")
					.append(model.getThickness()).append("\n");
		code.append("f 4 3 2 1\n")
			.append("f 5 6 7 8\n")
			.append("f 6 2 3 7\n")
			.append("f 7 3 4 8\n")
			.append("f 5 8 4 1\n")
			.append("f 6 5 1 2\n");
		RobotCompModel str = new RobotCompModel();
		str.load(new java.io.StringReader(code.toString()));

		PhysicalData pd = str.getPhysicalData();
		pd.mass = model.getWeight();
		pd.volume = model.getHeight() * model.getWidth() * model.getThickness();
		
		if(model.hasBound()){
			BoundObject.ConvexPolyhedron cp = new BoundObject.ConvexPolyhedron();
			cp.set(str.getPoints(), str.getFaces());
			pd.bounds = cp;
		}else{
			pd.bounds = null;
		}
		
		pd.centerMassPoint = new Point3f(model.getWidth() / 2.0f,
				model.getHeight() / 2.0f, model.getThickness() / 2.0f);
		pd.inertia = new Matrix3f();
		pd.inertia.m00 = pd.mass * (model.getHeight() * model.getHeight() + model.getThickness() * model.getThickness()) / 12.0f;
		pd.inertia.m11 = pd.mass * (model.getWidth() * model.getWidth() + model.getThickness() * model.getThickness()) / 12.0f;
		pd.inertia.m22 = pd.mass * (model.getWidth() * model.getWidth() + model.getHeight() * model.getHeight()) / 12.0f;

		AppearanceData ap = str.getApprData();
		ap.setColor(java.awt.Color.gray);
		
		
		return str;
	}// end generateModel(RectanglePane)
}
