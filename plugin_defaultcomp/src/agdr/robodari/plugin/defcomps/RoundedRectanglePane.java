package agdr.robodari.plugin.defcomps;

import agdr.robodari.comp.*;

public class RoundedRectanglePane extends StructureComponent{
	public final static float DEFAULT_THICKNESS = 0.5F;
	public final static float DEFAULT_WIDTH = 5.0f;
	public final static float DEFAULT_RADIUS = 1.0f;
	public final static float DEFAULT_WEIGHT = 20.0f;
	
	
	private float thickness = DEFAULT_THICKNESS;
	private float width = DEFAULT_WIDTH;
	private float radius = DEFAULT_RADIUS;
	private float weight = DEFAULT_WEIGHT;
	
	

	
	public boolean hasSubComponent()
	{ return false; }
	public RobotComponent[] getSubComponents()
	{ return null; }
	public float getRadius() {
		return radius;
	}
	public void setRadius(float radius) {
		this.radius = radius;
	}
	public float getThickness() {
		return thickness;
	}
	public void setThickness(float thickness) {
		this.thickness = thickness;
	}
	public float getWidth() {
		return width;
	}
	public void setWidth(float width) {
		this.width = width;
	}
	public float getWeight() {
		return weight;
	}
	public void setWeight(float weight) {
		this.weight = weight;
	}
}
