package agdr.robodari.plugin.defcomps;

import java.awt.*;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;


import agdr.robodari.appl.*;
import agdr.robodari.comp.Note;
import agdr.robodari.comp.RobotComponent;
import agdr.robodari.edit.*;
import agdr.robodari.gui.NoteEditor;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.gui.*;

public class RoundedRectanglePaneEditor extends CompEditor{
	private JTabbedPane tabbedPane;
	
	private JPanel propertyPanel;
	private NoteEditor noteEditor;
	private JTextField nameField;
	private JTextField radiusField;
	private JTextField widthField;
	private JTextField thicknessField;
	private JTextField weightField;
	
	private SkObject object;
	

	public RoundedRectanglePaneEditor(SketchEditorInterface manager, Dialog parent){
		super(manager, parent, "Rounded Plastic Pane Editor");
		__set();
	}// end Constructor(EditorManager)
	public RoundedRectanglePaneEditor(SketchEditorInterface manager, Frame parent){
		super(manager, parent, "Rounded Plastic Pane Editor");
		__set();
	}// end Constructor(EditorManager)
	private void __set(){
		initPropertyPanel();
		tabbedPane = new JTabbedPane();
		tabbedPane.setTabPlacement(JTabbedPane.LEFT);
		tabbedPane.addTab("Properties", propertyPanel);
		
		super.getEditorPanel().setLayout(new BorderLayout());
		super.getEditorPanel().add(tabbedPane, BorderLayout.CENTER);
		
		super.setSize(480, 450);
		super.setResizable(false);
	}// end __set
	private void initPropertyPanel(){
		propertyPanel = new JPanel();
		propertyPanel.setLayout(null);
		
		nameField = new JTextField();
		JLabel nameLabel = new JLabel("Component name : ");
		nameLabel.setBounds(20, 20, 100, 25);
		nameField.setBounds(130, 20, 200, 25);
		
		widthField = new JTextField();
		radiusField = new JTextField();
		thicknessField = new JTextField();
		weightField = new JTextField();
		JLabel widthLabel = new JLabel("Width(cm) : ");
		JLabel radiusLabel = new JLabel("Radius(cm) : ");
		JLabel thicknessLabel = new JLabel("Thickness(cm) : ");
		JLabel weightLabel = new JLabel("Weight(g) : ");
		
		thicknessLabel.setHorizontalAlignment(JLabel.RIGHT);
		widthLabel.setHorizontalAlignment(JLabel.RIGHT);
		radiusLabel.setHorizontalAlignment(JLabel.RIGHT);
		weightLabel.setHorizontalAlignment(JLabel.RIGHT);
		widthLabel.setBounds(20, 55, 100, 25);
		radiusLabel.setBounds(20, 90, 100, 25);
		radiusField.setBounds(130, 90, 70, 25);
		widthField.setBounds(130, 55, 70, 25);
		thicknessLabel.setBounds(210, 55, 80, 25);
		thicknessField.setBounds(300, 55, 50, 25);
		weightLabel.setBounds(20, 125, 80, 25);
		weightField.setBounds(100, 125, 70, 25);
		
		noteEditor = new NoteEditor();
		noteEditor.setBounds(20, 160, 350, 210);
		
		propertyPanel.add(nameLabel);
		propertyPanel.add(nameField);
		
		propertyPanel.add(widthField);
		propertyPanel.add(radiusField);
		propertyPanel.add(widthLabel);
		propertyPanel.add(radiusLabel);
		propertyPanel.add(thicknessLabel);
		propertyPanel.add(thicknessField);
		propertyPanel.add(weightField);
		propertyPanel.add(weightLabel);
		
		propertyPanel.add(noteEditor);
	}// end initPropertyPanel()
	
	
	public void startEditing(SkObject object){
		if(!(object.getRobotComponent() instanceof RoundedRectanglePane))
			return;
		
		this.object = object;

		RoundedRectanglePane rcomp = (RoundedRectanglePane)object.getRobotComponent();
		nameField.setText(rcomp.getName());
		if(rcomp.getNote() == null)
			rcomp.setNote(new Note());
		widthField.setText(String.valueOf(rcomp.getWidth()));
		thicknessField.setText(String.valueOf(rcomp.getThickness()));
		radiusField.setText(String.valueOf(rcomp.getRadius()));
		weightField.setText(String.format("%.5f", rcomp.getWeight()));
		
		noteEditor.open(rcomp.getNote());
	}// end startEditing(SkObject)
	public void finishEditing(){
		RoundedRectanglePane rcomp = (RoundedRectanglePane)object.getRobotComponent();
		rcomp.setNote(noteEditor.getNote());
		rcomp.setName(nameField.getText());
		
		try{
			float width = Float.parseFloat(widthField.getText());
			rcomp.setWidth(width);
		}catch(Exception excp){}
		try{
			float weight = Float.parseFloat(weightField.getText());
			rcomp.setWeight(weight);
		}catch(Exception excp){}
		try{
			float thickness = Float.parseFloat(thicknessField.getText());
			rcomp.setThickness(thickness);
		}catch(Exception excp){}
		try{
			float radius = Float.parseFloat(radiusField.getText());
			rcomp.setRadius(radius);
		}catch(Exception excp){}
		
		SkObject skobj = super.getInterface().getModel().getObject(rcomp);
		try{
			skobj.refresh();
		}catch(Exception excp){
			BugManager.log(excp, true);
		}
		super.getInterface().refreshObject(skobj);
		
		this.object = null;
		this.setVisible(false);
		super.getInterface().setCurrentCompEditor(null);
	}// end finishEditing()
	public SkObject getObject()
	{ return object; }
}