package agdr.robodari.plugin.defcomps;

import java.io.*;

import javax.vecmath.*;

import agdr.library.math.MathUtility;
import agdr.robodari.comp.*;
import agdr.robodari.comp.info.*;
import agdr.robodari.comp.model.*;
import agdr.robodari.plugin.rme.rlztion.*;
import agdr.robodari.store.*;

import static java.lang.Math.*;



public class RoundedRectanglePaneModelGenerator implements ModelGenerator<RoundedRectanglePane>{
	public RobotCompModel generate(RoundedRectanglePane pane) throws Exception{
		StringBuffer uppercode = new StringBuffer();
		StringBuffer lowercode = new StringBuffer();
		StringBuffer totalcode = new StringBuffer();
		
		double seg = Math.PI / 14.0d;
		double x = 0, y = 0;
		for(int i = -7; i <= 7; i++){
			x = pane.getRadius() * sin(seg * (double)i);
			y = pane.getRadius() * cos(seg * (double)i);
			uppercode.append("v " + x + " " + y + " 0\n");
			lowercode.append("v " + x + " " + y + " " + -pane.getThickness()
					+ "\n");
		}
		for(int i = 7; i >= -7; i--){
			x = pane.getRadius() * sin(seg * (double)i);
			y = -pane.getRadius() * cos(seg * (double)i) - pane.getWidth();
			uppercode.append("v " + x + " " + y + " 0\n");
			lowercode.append("v " + x + " " + y + " " + -pane.getThickness()
					+ "\n");
		}
		totalcode.append(uppercode).append(lowercode);
		totalcode.append("f 1 30 29 28 27 26 25 24 23 22 21 20 19" +
				" 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1\n");
		totalcode.append("f 31 32 33 34 35 36 37 38 39 40 41 42 43" +
				" 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60\n");
		totalcode.append("f 1 2 32 31\n");
		totalcode.append("f 2 3 33 32\n");
		totalcode.append("f 3 4 34 33\n");
		totalcode.append("f 4 5 35 34\n");
		totalcode.append("f 5 6 36 35\n");
		totalcode.append("f 6 7 37 36\n");
		totalcode.append("f 7 8 38 37\n");
		totalcode.append("f 8 9 39 38\n");
		totalcode.append("f 9 10 40 39\n");
		totalcode.append("f 10 11 41 40\n");
		totalcode.append("f 11 12 42 41\n");
		totalcode.append("f 12 13 43 42\n");
		totalcode.append("f 13 14 44 43\n");
		totalcode.append("f 14 15 45 44\n");
		totalcode.append("f 15 16 46 45\n");
		totalcode.append("f 16 17 47 46\n");
		totalcode.append("f 17 18 48 47\n");
		totalcode.append("f 18 19 49 48\n");
		totalcode.append("f 19 20 50 49\n");
		totalcode.append("f 20 21 51 50\n");
		totalcode.append("f 21 22 52 51\n");
		totalcode.append("f 22 23 53 52\n");
		totalcode.append("f 23 24 54 53\n");
		totalcode.append("f 24 25 55 54\n");
		totalcode.append("f 25 26 56 55\n");
		totalcode.append("f 26 27 57 56\n");
		totalcode.append("f 27 28 58 57\n");
		totalcode.append("f 28 29 59 58\n");
		totalcode.append("f 29 30 60 59\n");
		totalcode.append("f 30 1 31 60\n");
		
		RobotCompModel str = new RobotCompModel();
		str.load(new StringReader(totalcode.toString()));
		
		PhysicalData pd = str.getPhysicalData();
		
		pd.mass = pane.getWeight();
		pd.volume = (pane.getRadius() * pane.getRadius() * (float)Math.PI) * pane.getThickness();
		
		if(pane.hasBound()){
			BoundObject.CompoundBounds cb = new BoundObject.CompoundBounds();
			pd.bounds = cb;
			{
				BoundObject.Cylinder b1 = new BoundObject.Cylinder(0, 0, -pane.getThickness(), 0, 0, 0, pane.getRadius());
	
				BoundObject.Cylinder b2 = new BoundObject.Cylinder(0, -pane.getWidth(), -pane.getThickness(), 0, -pane.getWidth(), 0, pane.getRadius());
				
				BoundObject.ConvexPolyhedron b3 = new BoundObject.ConvexPolyhedron();
				Point3f[] vertices = new Point3f[]{
						new Point3f(pane.getRadius(), -pane.getWidth(), 0),
						new Point3f(pane.getRadius(), 0, 0),
						new Point3f(-pane.getRadius(), 0, 0),
						new Point3f(-pane.getRadius(), -pane.getWidth(), 0),
						
						new Point3f(pane.getRadius(), -pane.getWidth(), -pane.getThickness()),
						new Point3f(pane.getRadius(), 0, -pane.getThickness()),
						new Point3f(-pane.getRadius(), 0, -pane.getThickness()),
						new Point3f(-pane.getRadius(), -pane.getWidth(), -pane.getThickness()),
				};
				int[][] faces = new int[][]{
						{0, 1, 2, 3},
						{7, 6, 5, 4},
						{0, 3, 7, 4},
						{0, 4, 5, 1},
						{1, 5, 6, 2},
						{2, 6, 7, 3}
				};
				b3.set(vertices, faces);
				
				cb.add(b1);
				cb.add(b2);
				cb.add(b3);
			}
		}else
			pd.bounds = null;
		pd.centerMassPoint = new javax.vecmath.Point3f(
				0, -(pane.getWidth() / 2.0f),
				(pane.getThickness() / -2.0f));
		pd.inertia = new Matrix3f();
		MathUtility.addInertiaTensor(str.getPoints(),
				str.getFaces(),
				pd.mass,
				-pd.centerMassPoint.x,
				-pd.centerMassPoint.y,
				-pd.centerMassPoint.z,
				pd.inertia);
		
		AppearanceData ad = str.getApprData();
		ad.setColor(java.awt.Color.gray);
		
		return str;
	}// end generate(RoundedPlasticPane)
	
	
	
	public static void main(String[] a) throws Throwable
	{ new RoundedRectanglePaneModelGenerator().generate(new RoundedRectanglePane()); }
}

