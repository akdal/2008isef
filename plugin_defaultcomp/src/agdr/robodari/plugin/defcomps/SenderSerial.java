package agdr.robodari.plugin.defcomps;

import java.util.*;

import agdr.robodari.comp.*;

public class SenderSerial implements RobotComponent{
	private ArrayList<Sender> senders;
	private Note note;
	private String name;
	private RobotComponent parent;
	
	
	public SenderSerial(){
		this.senders = new ArrayList<Sender>();
		this.note = new Note();
	}// end Constructor(int, int)
	
	public Note getNote()
	{ return note; }
	public void setNote(Note n)
	{ this.note = n; }
	public String getName()
	{ return name; }
	public void setName(String name)
	{ this.name = name; }
	

	public boolean suppliesPower()
	{ return false; }
	public PowerSupply getPowerSupply()
	{ return null; }
	public boolean hasSubComponent()
	{ return false; }
	public RobotComponent[] getSubComponents()
	{ return null; }

	public RobotComponent getParent()
	{ return parent; }
	public void setParent(RobotComponent p){
		this.parent = p;
	}// end setParent(RobotComponent)
	
	public boolean hasSender()
	{ return /*true*/ false; }
	public int getSendersCount()
	{ return senders.size(); }
	public Sender getSender(int idx)
	{ return senders.get(idx); }
	public void appendSender(Sender sender)
	{ senders.add(sender); }
	public Sender[] getSenders()
	{ return senders.toArray(new Sender[]{}); }
	
	public boolean hasReceiver(){
		/*for(Sender eachs : senders)
			if(eachs instanceof Receiver)
				return true;*/
		return false;
	}// end hasReceiver()
	public Receiver[] getReceivers(){
		ArrayList<Receiver> rcvrs = new ArrayList<Receiver>();
		for(Sender eachs : senders)
			if(eachs instanceof Receiver)
				rcvrs.add((Receiver)eachs);
		return rcvrs.toArray(new Receiver[]{});
	}// end getReceivers()
}
