package agdr.robodari.plugin.defcomps;

import javax.vecmath.*;

import agdr.robodari.comp.*;
import agdr.robodari.comp.info.*;
import agdr.robodari.comp.model.AppearanceData;
import agdr.robodari.comp.model.ModelGenerator;
import agdr.robodari.comp.model.RobotCompModel;
import agdr.robodari.plugin.rme.rlztion.*;
import agdr.robodari.plugin.rme.store.*;
import agdr.robodari.store.*;

import java.io.*;

public class SenderSerialModelGenerator implements ModelGenerator<SenderSerial>{
	public RobotCompModel generate(SenderSerial model) throws Exception{
		RobotCompModel structure = new RobotCompModel();
		structure.setGroup(true);
		
		float startpos = 0;
		//double mass = 0;
		//double volume = 0;
		for(int i = 0; i < model.getSendersCount(); i++){
			RobotCompModel senderstr = RobotModelStore.generateModel(model.getSender(i));
			//mass += senderstr.getPhysicalData().mass;
			
			float minx = 21474836, maxx = -21474836;
			for(Point3f eachp : senderstr.getPoints()){
				if(eachp.x > maxx)
					maxx = eachp.x;
				if(eachp.x < minx)
					minx = eachp.x;
			}
			
			Matrix4f trans = new Matrix4f();
			trans.m00 = trans.m11 = trans.m22 = trans.m33 = 1;
			trans.m03 = startpos - minx;
			
			structure.addSubModel(senderstr, trans);
			startpos += maxx;
		}

		AppearanceData ap = structure.getApprData();
		ap.setColor(java.awt.Color.gray);
		
		//structure.getPhysicalData().mass = mass;
		return structure;
	}// end generate(SenderSerial)
}
