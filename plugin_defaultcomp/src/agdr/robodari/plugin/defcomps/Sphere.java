package agdr.robodari.plugin.defcomps;

import agdr.robodari.comp.*;

public class Sphere extends StructureComponent{
	public final static float DEFAULT_RADIUS = 1.0F;

	private float radius = DEFAULT_RADIUS;
	private float density = 1.5f;
	
	
	public float getDensity()
	{ return density; }
	public void setDensity(float we)
	{ this.density = we; }
	
	public float getRadius() {
		return radius;
	}
	public void setRadius(float radius) {
		this.radius = radius;
	}
	
	public boolean hasSubComponent()
	{ return false; }
	public RobotComponent[] getSubComponents()
	{ return null; }
}
