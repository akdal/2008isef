package agdr.robodari.plugin.defcomps;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Frame;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;


import agdr.robodari.appl.*;
import agdr.robodari.comp.*;
import agdr.robodari.edit.*;
import agdr.robodari.gui.*;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.gui.CompEditor;

public class SphereEditor extends CompEditor{
	private JTabbedPane tabbedPane;
	
	private JPanel propertyPanel;
	private NoteEditor noteEditor;
	
	private SkObject object;
	private JTextField nameField;
	private JTextField radiusField;
	private JTextField densityField;
	

	public SphereEditor(SketchEditorInterface manager, Dialog parent){
		super(manager, parent, "Sphere Editor");
		__set();
	}// end Constructor(EditorManager)
	public SphereEditor(SketchEditorInterface manager, Frame parent){
		super(manager, parent, "Sphere Editor");
		__set();
	}// end Constructor(EditorManager)
	private void __set(){
		initPropertyPanel();
		tabbedPane = new JTabbedPane();
		tabbedPane.setTabPlacement(JTabbedPane.LEFT);
		tabbedPane.addTab("Properties", propertyPanel);
			
		super.getEditorPanel().setLayout(new BorderLayout());
		super.getEditorPanel().add(tabbedPane, BorderLayout.CENTER);
		
		super.setSize(500, 430);
		super.setResizable(false);
	}// end __set
	private void initPropertyPanel(){
		propertyPanel = new JPanel();
		propertyPanel.setLayout(null);
		
		nameField = new JTextField();
		JLabel nameLabel = new JLabel("Component name: ");
		nameLabel.setBounds(20, 20, 100, 25);
		nameField.setBounds(130, 20, 200, 25);
		
		radiusField = new JTextField();
		densityField = new JTextField();
		JLabel radiusLabel = new JLabel("Radius(cm) : ");
		JLabel densityLabel = new JLabel("Density(g/cm^3)  : ");
		
		densityLabel.setHorizontalAlignment(JLabel.RIGHT);
		radiusLabel.setHorizontalAlignment(JLabel.RIGHT);
		
		densityLabel.setBounds(20, 55, 100, 25);
		radiusLabel.setBounds(20, 90, 100, 25);
		densityField.setBounds(130, 55, 70, 25);
		radiusField.setBounds(130, 90, 70, 25);
		
		noteEditor = new NoteEditor();
		noteEditor.setBounds(20, 120, 350, 210);
		
		propertyPanel.add(nameLabel);
		propertyPanel.add(nameField);
		
		propertyPanel.add(densityField);
		propertyPanel.add(radiusField);
		propertyPanel.add(densityLabel);
		propertyPanel.add(radiusLabel);
		
		propertyPanel.add(noteEditor);
	}// end initPropertyPanel()
	
	
	public void startEditing(SkObject object){
		if(!(object.getRobotComponent() instanceof Sphere))
			return;
		
		this.object = object;

		Sphere rcomp = (Sphere)object.getRobotComponent();
		nameField.setText(rcomp.getName());
		if(rcomp.getNote() == null)
			rcomp.setNote(new Note());
		densityField.setText(String.valueOf(rcomp.getDensity()));
		radiusField.setText(String.valueOf(rcomp.getRadius()));
		
		noteEditor.open(rcomp.getNote());
	}// end startEditing(SkObject)
	public void finishEditing(){
		Sphere rcomp = (Sphere)object.getRobotComponent();
		rcomp.setNote(noteEditor.getNote());
		rcomp.setName(nameField.getText());
		
		try{
			float density = Float.parseFloat(densityField.getText());
			rcomp.setDensity(density);
		}catch(Exception excp){}
		try{
			float radius = Float.parseFloat(radiusField.getText());
			rcomp.setRadius(radius);
		}catch(Exception excp){}
		
		SkObject skobj = getInterface().getModel().getObject(rcomp);
		try{
			skobj.refresh();
		}catch(Exception excp){
			BugManager.log(excp, true);
		}
		getInterface().refreshObject(skobj);
		
		this.object = null;
		this.setVisible(false);
		getInterface().setCurrentCompEditor(null);
	}// end finishEditing()
	public SkObject getObject()
	{ return object; }
}
