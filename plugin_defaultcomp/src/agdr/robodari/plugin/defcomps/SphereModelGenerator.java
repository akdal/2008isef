package agdr.robodari.plugin.defcomps;

import java.io.*;

import agdr.robodari.comp.*;
import agdr.robodari.comp.info.*;
import agdr.robodari.comp.model.*;
import agdr.robodari.plugin.rme.rlztion.*;
import agdr.robodari.store.*;

import com.sun.j3d.utils.geometry.*;
import javax.media.j3d.*;
import javax.vecmath.*;



public class SphereModelGenerator implements ModelGenerator<Sphere>{
	public RobotCompModel generate(Sphere sph) throws IOException, Exception{
		StringBuffer source = new StringBuffer();

		com.sun.j3d.utils.geometry.Sphere sp = new
			com.sun.j3d.utils.geometry.Sphere((float)sph.getRadius(),
					com.sun.j3d.utils.geometry.Primitive.GENERATE_NORMALS,
					33);
		Shape3D s3d = sp.getShape(com.sun.j3d.utils.geometry.Sphere.BODY);
		TriangleStripArray geometry = (TriangleStripArray)s3d.getGeometry();
		GeometryInfo gi = new GeometryInfo(geometry);
		
		int[] strip = new int[geometry.getNumStrips()];
		geometry.getStripVertexCounts(strip);
		Point3f[] ps = gi.getCoordinates();
		
		
		for(int i = 0; i < ps.length;){
			source.append("v " + ps[i].x + " " + ps[i].y + " " + ps[i].z)
				.append("\n");
			i++;
			source.append("v " + ps[i].x + " " + ps[i].y + " " + ps[i].z)
				.append("\n");
			i++;
			source.append("v " + ps[i].x + " " + ps[i].y + " " + ps[i].z)
				.append("\n");
			i++;
		}
		int startp = 1, edp = 0;
		boolean cw = true;
		for(int i = 0; i < strip.length; i++){
			edp += strip[i];
			cw = true;
			for(int j = startp; j <= edp - 2; j++){
				if(cw)
					source.append("f " + j + " " + (j + 1) + " " + (j + 2))
						.append("\n");
				else
					source.append("f " + (j + 2) + " " + (j + 1) + " " + j)
						.append("\n");
				cw = !cw;
			}
			startp = edp + 1;
		}
		
		RobotCompModel str = new RobotCompModel();
		str.load(new StringReader(source.toString()));
		
		Info info = RobotInfoStore.getInfo(sph);
		PhysicalData pd = str.getPhysicalData();
		pd.mass = ((Number)info.get(InfoKeys.WEIGHT)).floatValue();
		pd.centerMassPoint = new Point3f(0, 0, 0);
		pd.volume = 4.0f / 3.0f * sph.getRadius() * sph.getRadius() * sph.getRadius() * (float)Math.PI;
		
		BoundObject.Sphere bsph = new BoundObject.Sphere();
		bsph.setCenter(0, 0, 0);
		bsph.setRadius(sph.getRadius());
		pd.bounds = bsph;
		
		pd.inertia = new Matrix3f();
		pd.inertia.m00 = pd.inertia.m11 = pd.inertia.m22 = 0.4f * pd.mass * sph.getRadius() * sph.getRadius();
		
		AppearanceData ad = str.getApprData();
		ad.setColor(java.awt.Color.gray);
		
		return str;
	}// end generate(Sphere)
}
