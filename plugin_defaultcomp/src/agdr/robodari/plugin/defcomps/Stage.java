package agdr.robodari.plugin.defcomps;

import agdr.robodari.comp.*;

public class Stage extends StructureComponent{
	private float[] cylinderX;
	private float[] cylinderY;
	private float[] hlwX;
	private float[] hlwY;
	private float height;
	private float depth;
	private final float weight = Float.MAX_VALUE;
	private String name;
	private Note note;
	private RobotComponent parent;
	
	
	public Stage(){
		cylinderX = new float[]{
				5, -5, -5, 5
		};
		cylinderY = new float[]{
				5, 5, -5, -5
		};
		hlwX = new float[]{
				3, -3, -3, 3
		};
		hlwY = new float[]{
				3, 3, -3, -3
		};
		height = 2;
		depth = 1;
	}// end Constructor()
	
	
	public RobotComponent getParent()
	{ return parent; }
	
	
	public String getName()
	{ return name; }
	public void setName(String n)
	{ this.name = n; }
	
	public String toString()
	{ return  name; }
	
	public Note getNote()
	{ return note; }
	public void setNote(Note note)
	{ this.note = note; }

	
	public boolean hasSubComponent()
	{ return false; }
	public RobotComponent[] getSubComponents()
	{ return null; }

	public float[] getCylinderX(){
		float[] array = new float[cylinderX.length];
		System.arraycopy(cylinderX, 0, array, 0, array.length);
		return array;
	}// end getHollowedX()
	public float[] getCylinderY(){
		float[] array = new float[cylinderY.length];
		System.arraycopy(cylinderY, 0, array, 0, array.length);
		return array;
	}// end getHollowedY()
	public void setCylinderCoords(float[] cx, float[] cy){
		this.cylinderX = new float[cx.length];
		System.arraycopy(cx, 0, cylinderX, 0,cx.length);
		this.cylinderY = new float[cy.length];
		System.arraycopy(cy, 0, cylinderY, 0,cy.length);
	}// end setCylindercoords(float[], float[])
	
	
	public float[] getHollowedX(){
		float[] array = new float[hlwX.length];
		System.arraycopy(hlwX, 0, array, 0, array.length);
		return array;
	}// end getHollowedX()
	public float[] getHollowedY(){
		float[] array = new float[hlwY.length];
		System.arraycopy(hlwY, 0, array, 0, array.length);
		return array;
	}// end getHollowedY()
	public void setHollowedCoords(float[] mx, float[] my){
		this.hlwX = new float[mx.length];
		System.arraycopy(mx, 0, hlwX, 0,mx.length);
		this.hlwY = new float[my.length];
		System.arraycopy(my, 0, hlwY, 0,my.length);
	}// end setHollowedCoords(float[], float[])
	
	public float getHeight() {
		return height;
	}
	public void setHeight(float height) {
		this.height = height;
	}
	public float getDepth() {
		return depth;
	}
	public void setDepth(float depth) {
		this.depth = depth;
	}
	public float getWeight() {
		return weight;
	}
}
