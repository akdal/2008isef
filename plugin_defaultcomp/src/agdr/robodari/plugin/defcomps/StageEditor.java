package agdr.robodari.plugin.defcomps;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.util.*;
import javax.swing.*;


import agdr.robodari.appl.BugManager;
import agdr.robodari.comp.Note;
import agdr.robodari.gui.NoteEditor;
import agdr.robodari.plugin.rme.appl.*;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.gui.*;
import agdr.robodari.plugin.rme.rlztion.*;

public class StageEditor extends CompEditor implements ItemListener, ActionListener{
	private final static Point2D[] NULLPARRAY = new Point2D[0];
	
	private JTabbedPane tabbedPane;
	
	private JPanel propertiesPanel;
		private NoteEditor noteEditor;
		private JTextField nameField;
		private JTextField depthField;
		private JTextField heightField;
		private JTextField weightField;
	
	private JPanel editPanel;
		private JPanel toolsPanel;
			private JComboBox typeComboBox;
			private JTextField scaleField;
			private JButton scaleButton;
		private HCCanvas canvas;
	
	private SkObject object;
	
	
	public StageEditor(SketchEditorInterface manager, Dialog parent){
		super(manager, parent, "Hollowed Cylinder Editor");
		__set();
	}// end Constructor
	public StageEditor(SketchEditorInterface i, Frame parent){
		super(i, parent, "Hollowed Cylinder Editor");
		__set();
	}// end Constructor
	private void __set(){
		initPropertiesPanel();
		initEditPanel();
		
		tabbedPane = new JTabbedPane();
		tabbedPane.setTabPlacement(JTabbedPane.LEFT);
		tabbedPane.addTab("Properties", propertiesPanel);
		tabbedPane.addTab("Edit", editPanel);
		
		super.getEditorPanel().setLayout(new BorderLayout());
		super.getEditorPanel().add(tabbedPane, BorderLayout.CENTER);
		
		super.setSize(500, 430);
	}// end -_set
	private void initPropertiesPanel(){
		propertiesPanel = new JPanel();
		propertiesPanel.setLayout(null);
		
		nameField = new JTextField();
		JLabel nameLabel = new JLabel("Component name : ");
		nameLabel.setBounds(20, 20, 100, 25);
		nameField.setBounds(130, 20, 200, 25);
		
		heightField = new JTextField();
		depthField = new JTextField();
		weightField = new JTextField();
		weightField.setEditable(false);
		JLabel heightLabel = new JLabel("Height(cm) : ");
		JLabel depthLabel = new JLabel("Depth(cm) : ");
		JLabel weightLabel = new JLabel("Weight(gram) : ");
		
		depthLabel.setHorizontalAlignment(JLabel.RIGHT);
		heightLabel.setHorizontalAlignment(JLabel.RIGHT);
		weightLabel.setHorizontalAlignment(JLabel.RIGHT);
		
		heightLabel.setBounds(20, 90, 100, 25);
		heightField.setBounds(130, 90, 70, 25);
		weightLabel.setBounds(20, 55, 100, 25);
		weightField.setBounds(130, 55, 70, 25);
		depthLabel.setBounds(210, 55, 80, 25);
		depthField.setBounds(300, 55, 50, 25);
		
		noteEditor = new NoteEditor();
		noteEditor.setBounds(20, 120, 350, 210);
		
		propertiesPanel.add(weightLabel);
		propertiesPanel.add(weightField);
		propertiesPanel.add(nameLabel);
		propertiesPanel.add(nameField);
		propertiesPanel.add(heightField);
		propertiesPanel.add(heightLabel);
		propertiesPanel.add(depthLabel);
		propertiesPanel.add(depthField);
		
		propertiesPanel.add(noteEditor);
	}// end initPropertiesPanel()
	private void initEditPanel(){
		this.editPanel = new JPanel();
		this.editPanel.setLayout(new BorderLayout());
		{
			this.canvas = new HCCanvas();
			this.toolsPanel = new JPanel(new BorderLayout());
			{
				this.typeComboBox = new JComboBox();
				this.typeComboBox.addItem("Cylinder");
				this.typeComboBox.addItem("Hollowed part");
				this.typeComboBox.addItemListener(this);
				JPanel tmpPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
				this.scaleField = new JTextField();
				this.scaleField.setColumns(6);
				this.scaleField.setText(String.valueOf(this.canvas.getScale()));
				this.scaleButton = new JButton("Apply");
				this.scaleButton.addActionListener(this);
				tmpPanel.add(new JLabel("Scale : "));
				tmpPanel.add(this.scaleField);
				tmpPanel.add(this.scaleButton);
				
				toolsPanel.add(this.typeComboBox, BorderLayout.WEST);
				toolsPanel.add(tmpPanel, BorderLayout.EAST);
			}
			
			this.editPanel.add(this.toolsPanel, BorderLayout.NORTH);
			this.editPanel.add(this.canvas, BorderLayout.CENTER);
		}
	}// end intiEditO
	
	
	
	
	
	public void actionPerformed(ActionEvent ae){
		if(ae.getSource() == this.scaleButton){
			try{
				float d = Float.parseFloat(this.scaleField.getText());
				canvas.setScale(d);
				canvas.repaint();
			}catch(Exception excp){
				ExceptionHandler.onlyDecimalAccepted();
			}
		}
	}// end acitonPErforemd
	public void itemStateChanged(ItemEvent ie){
		if(ie.getSource() == this.typeComboBox){
			this.canvas.setType(this.typeComboBox.getSelectedIndex());
		}
	}// end itemStateChanged
	
	
	

	public void startEditing(SkObject object){
		if(!(object.getRobotComponent() instanceof Stage))
			return;
		
		this.object = object;
		
		Stage rcomp = (Stage)object.getRobotComponent();
		if(rcomp.getNote() == null)
			rcomp.setNote(new Note());
		nameField.setText(rcomp.getName());
		heightField.setText(String.format("%.6g", rcomp.getHeight()));
		depthField.setText(String.format("%.6g", rcomp.getDepth()));
		weightField.setText(String.format("%.6g", rcomp.getWeight()));
		
		scaleField.setText(String.valueOf(canvas.getScale()));
		_refreshPoints();
		
		noteEditor.open(rcomp.getNote());
	}// end startEditing(SkObject)
	
	public void finishEditing(){
		Stage rcomp = (Stage)object.getRobotComponent();
		rcomp.setNote(noteEditor.getNote());
		rcomp.setName(nameField.getText());
		
		float depth = 0, /*weight = 0, */h = 0;
		try{
			depth = Float.parseFloat(depthField.getText());
			if(depth < 0)
				depth = rcomp.getDepth();
		}catch(Exception excp){}
		/*
		try{
			weight = Float.parseFloat(weightField.getText());
			if(weight < 0)
				weight = rcomp.getWeight();
		}catch(Exception excp){}
		*/
		try{
			h = Float.parseFloat(heightField.getText());
			if(h < 0)
				h = rcomp.getHeight();
		}catch(Exception excp){}
		
		if(depth >= h){
			depth = rcomp.getDepth();
			h = rcomp.getHeight();
		}
		rcomp.setDepth(depth);
		rcomp.setHeight(h);
		//rcomp.setWeight(weight);
		
		
		// check intersection
		Point2D[] cylpoints = canvas.cylcoords.toArray(NULLPARRAY);
		Point2D[] holpoints = canvas.holcoords.toArray(NULLPARRAY);
		boolean checkis = false;
		for(int i = 0; i < cylpoints.length; i++){
			for(int j = 0; j < holpoints.length; j++){
				if(_intersects(cylpoints[i], cylpoints[(i + 1) % cylpoints.length],
						holpoints[j], holpoints[(j + 1) % holpoints.length])){
					checkis = true;
					i = j = 21474836;
					break;
				}
			}
		}
		// check containing
		boolean checkct = false;
		Polygon holpol = new Polygon();
		for(int i = 0; i < holpoints.length; i++)
			holpol.addPoint(
					(int)(holpoints[i].getX() * canvas.getScale()) + (int)canvas.getAxisStartX(),
					(int)(holpoints[i].getY() * canvas.getScale()) + (int)canvas.getAxisStartY());
		for(int i = 0; i < cylpoints.length; i++){
			int x = (int)(cylpoints[i].getX() * canvas.getScale()) + (int)canvas.getAxisStartX();
			int y = (int)(cylpoints[i].getY() * canvas.getScale()) + (int)canvas.getAxisStartY();
			
			if(holpol.contains(x, y)){
				checkct = true;
				break;
			}
		}
		
		if(!checkct && !checkis){
			float[] cylxcoords = new float[cylpoints.length];
			float[] cylycoords = new float[cylpoints.length];
			for(int i = 0; i < cylpoints.length; i++){
				cylxcoords[i] = (float)cylpoints[i].getX();
				cylycoords[i] = (float)cylpoints[i].getY();
			}
			float[] holxcoords = new float[holpoints.length];
			float[] holycoords = new float[holpoints.length];
			for(int i = 0; i < holpoints.length; i++){
				holxcoords[i] = (float)holpoints[i].getX();
				holycoords[i] = (float)holpoints[i].getY();
			}
			rcomp.setCylinderCoords(cylxcoords, cylycoords);
			rcomp.setHollowedCoords(holxcoords, holycoords);
		}
		
		
		SkObject skobj = getInterface().getModel().getObject(rcomp);
		try{
			skobj.refresh();
		}catch(Exception excp){
			BugManager.log(excp, true);
		}
		getInterface().refreshObject(skobj);
		
		this.object = null;
		this.setVisible(false);
		getInterface().setCurrentCompEditor(null);
	}// end finishEditing()
	
	
	
	private void _refreshPoints(){
		Stage rcomp = (Stage)this.object.getRobotComponent();
		canvas.clear();
		float[] cx = rcomp.getCylinderX();
		float[] cy = rcomp.getCylinderY();
		for(int i = 0; i < cx.length; i++)
			canvas.cylcoords.add(new Point2D.Double(cx[i], cy[i]));
		float[] hx = rcomp.getHollowedX();
		float[] hy = rcomp.getHollowedY();
		for(int i = 0; i < hx.length; i++)
			canvas.holcoords.add(new Point2D.Double(hx[i], hy[i]));
		canvas.repaint();
	}// end _refreshPoints()
	
	
	
	private boolean _intersects(Point2D ast, Point2D aed, Point2D bst, Point2D bed){
		double vecax = aed.getX() - ast.getX();
		double vecay = aed.getY() - ast.getY();
		
		double vecb1x = bst.getX() - ast.getX();
		double vecb1y = bst.getY() - ast.getY();
		double vecb2x = bed.getX() - ast.getX();
		double vecb2y = bed.getY() - ast.getY();
		
		if((vecax * vecb1y - vecb1x * vecay) * (vecax * vecb2y - vecb2x * vecay) < 0)
			return true;
		return false;
	}// end _intersects
	
	
	public SkObject getObject()
	{ return object; }
	
	
	
	
	
	
	
	
	private static class HCCanvas extends JPanel
			implements MouseListener, MouseMotionListener, SketchScreen{
		public final static int TYPE_CYLINDER = 0;
		public final static int TYPE_HOL = 1;
		private final static int RECSIZE = 2;
		
		private int type;
		private float scale = 10;
		private Projector projector;
		private ArrayList<Point2D> cylcoords;
		private ArrayList<Point2D> holcoords;
		private int selpidx;
		private Point currentMousePos;
		
		
		
		public HCCanvas(){
			super.setOpaque(true);
			super.setBackground(Color.white);
			cylcoords = new ArrayList<Point2D>();
			holcoords = new ArrayList<Point2D>();
			super.addMouseListener(this);
			super.addMouseMotionListener(this);
			this.projector = new Projector.OrthographicProjector(0, 0, 0);
		}// end Constructor()
		
		
		
		public Projector getProjector ()
		{ return projector; }
		public float getAxisStartX ()
		{ return super.getWidth() / 2.0f; }
		public float getAxisStartY ()
		{ return super.getHeight() / 2.0f; }
		public float getScale ()
		{ return scale; }
		public void setScale (float d){
			this.scale = d;
		}// end setScale(double)
		public Polygon getBound(SkObject obj)
		{ return null; }
		
		
		public void clear(){
			cylcoords.clear();
			holcoords.clear();
		}// end reset()
		
		public void setType(int type){
			this.type = type;
		}// end setType(int))
		
		public void mousePressed(MouseEvent me){
			this.currentMousePos = me.getPoint();
			float[] tmpprjp;
			if(type == TYPE_CYLINDER){
				int idx = 0;
				Point2D p;
				for(idx = 0; idx < cylcoords.size(); idx++){
					p = cylcoords.get(idx);
					tmpprjp = projector.project((float)p.getX(), (float)p.getY(), 0, null, this.getScale());
					tmpprjp[0] += this.getAxisStartX();
					tmpprjp[1] += this.getAxisStartY();
					if(Math.abs(tmpprjp[0] - me.getX()) <= RECSIZE
							&& Math.abs(tmpprjp[1] - me.getY()) <= RECSIZE)
						break;
				}
				selpidx = idx;
			}else if(type == TYPE_HOL){
				int idx = 0;
				Point2D p;
				for(idx = 0; idx < holcoords.size(); idx++){
					p = holcoords.get(idx);
					tmpprjp = projector.project((float)p.getX(), (float)p.getY(), 0, null, this.getScale());
					tmpprjp[0] += this.getAxisStartX();
					tmpprjp[1] += this.getAxisStartY();
					if(Math.abs(tmpprjp[0] - me.getX()) <= RECSIZE
							&& Math.abs(tmpprjp[1] - me.getY()) <= RECSIZE)
						break;
				}
				selpidx = idx;
			}
			
			if(me.getButton() == MouseEvent.BUTTON1){
				Point2D newp = new Point2D.Double(
						(currentMousePos.x - this.getAxisStartX()) / this.getScale(),
						(currentMousePos.y - this.getAxisStartY()) / this.getScale());
				if(type == TYPE_CYLINDER && selpidx == cylcoords.size())
					cylcoords.add(newp);
				else if(type == TYPE_HOL && selpidx == holcoords.size())
					holcoords.add(newp);
			}else if(me.getButton() == MouseEvent.BUTTON3){
				if(selpidx != cylcoords.size()){
					if(type == TYPE_CYLINDER)
						cylcoords.remove(selpidx);
					else if(type == TYPE_HOL)
						holcoords.remove(selpidx);
					selpidx = -1;
				}
			}
			repaint();
		}// end mousePressed(MouseEvent)
		public void mouseReleased(MouseEvent me){
			selpidx = -1;
		}
		public void mouseEntered(MouseEvent me){}
		public void mouseExited(MouseEvent me){}
		public void mouseMoved(MouseEvent me){
			currentMousePos = me.getPoint();
		}
		public void mouseClicked(MouseEvent me){}
		public void mouseDragged(MouseEvent me){
			currentMousePos = me.getPoint();
			if(selpidx == -1) return;
			
			Point2D p = null;
			if(type == TYPE_CYLINDER){
				p = cylcoords.get(selpidx);
			}else if(type == TYPE_HOL)
				p = holcoords.get(selpidx);
			p.setLocation(((double)me.getX() - this.getAxisStartX()) / this.getScale(),
					((double)me.getY() - this.getAxisStartY()) / this.getScale());
			repaint();
		}// end mouseDragged(MouseEvent)
		
		
		public void paint(Graphics g){
			super.paint(g);
			
			int axislen = this.getWidth() > this.getHeight()
					? this.getWidth() : this.getHeight();
			SketchEditorUtility.drawAxis(this, (Graphics2D)g,
					DefaultMainSketchEditor.getAxisStroke(),
					DefaultMainSketchEditor.getAxisPaint(),
					DefaultMainSketchEditor.getAxisHiddenPaint(),
					DefaultMainSketchEditor.getZAxisPaint(),
					axislen);
			SketchEditorUtility.drawGrid(this, (Graphics2D)g,
					DefaultMainSketchEditor.getAxisStroke(),
					DefaultMainSketchEditor.getAxisPaint(),
					axislen);
			
			g.setColor(Color.GRAY);
			if(this.currentMousePos != null){
				double cmpx = (currentMousePos.x - this.getAxisStartX()) / this.getScale();
				double cmpy = (currentMousePos.y - this.getAxisStartY()) / this.getScale();
				g.drawString("("
						+ String.valueOf(cmpx) + ","
						+ String.valueOf(cmpy) + ")",
					this.currentMousePos.x, this.currentMousePos.y);
			}

			double[] p, befp;
			if(cylcoords.size() != 0){
				g.setColor(Color.RED);
				_paint(g, cylcoords);
			}
			if(holcoords.size() != 0){
				g.setColor(Color.BLUE);
				_paint(g, holcoords);
			}
		}// end aint(Graphics)
		
		
		private void _paint(Graphics g, ArrayList<Point2D> coords){
			float[] befp, p;
			
			befp = this.projector.project((float)coords.get(0).getX(), (float)coords.get(0).getY(),
					0, null, this.getScale());
			befp[0] += this.getAxisStartX();
			befp[1] += this.getAxisStartY();
			
			for(int i = 1; i < coords.size(); i++){
				p = this.projector.project((float)coords.get(i).getX(), (float)coords.get(i).getY(),
						0, null, this.getScale());
				p[0] += this.getAxisStartX();
				p[1] += this.getAxisStartY();

				g.drawRect((int)p[0] - RECSIZE, (int)p[1] - RECSIZE, RECSIZE * 2, RECSIZE * 2);
				g.drawLine((int)befp[0], (int)befp[1], (int)p[0], (int)p[1]);
				befp = p;
			}
			p = this.projector.project((float)coords.get(0).getX(), (float)coords.get(0).getY(),
					0, null, this.getScale());
			p[0] += this.getAxisStartX();
			p[1] += this.getAxisStartY();
			
			g.drawRect((int)p[0] - RECSIZE, (int)p[1] - RECSIZE, RECSIZE * 2, RECSIZE * 2);
			g.drawLine((int)befp[0], (int)befp[1], (int)p[0], (int)p[1]);
		}// end _paint(Graphics, ArrayList<Point2D>)
	}// end class HCCanvas
}
