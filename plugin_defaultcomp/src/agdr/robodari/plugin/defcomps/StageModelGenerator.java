package agdr.robodari.plugin.defcomps;

import java.util.*;
import javax.vecmath.*;


import agdr.library.math.MathUtility;
import agdr.robodari.comp.model.AppearanceData;
import agdr.robodari.comp.model.BoundObject;
import agdr.robodari.comp.model.ModelGenerator;
import agdr.robodari.comp.model.PhysicalData;
import agdr.robodari.comp.model.RobotCompModel;
import agdr.robodari.plugin.rme.rlztion.*;

public class StageModelGenerator implements ModelGenerator<Stage>{
	private final static char _br = ((char)Character.LINE_SEPARATOR);
	
	public RobotCompModel generate(Stage hc) throws Exception{
		StringBuffer buf = new StringBuffer();
		
		float[] cx = hc.getCylinderX(), cy = hc.getCylinderY();
		float[] hx = hc.getHollowedX(), hy = hc.getHollowedY();
		float hei = hc.getHeight(), dep = hc.getDepth();
		rearrange(cx, cy);
		rearrange(hx, hy);
		
		// 0 ~ cx : bottom edge
		for(int i = 0; i < cx.length; i++)
			buf.append("v ").append(cx[i]).append((char)' ').append(cy[i])
					.append((char)' ').append((char)'0').append((char)_br);
		// cx ~ 2 cx : top edge
		for(int i = 0; i < cx.length; i++)
			buf.append("v ").append(cx[i]).append((char)' ').append(cy[i])
					.append((char)' ').append(hei).append((char)_br);
		// 2 cx ~ 2 cx + hx : top inner edge
		for(int i = 0; i < hx.length; i++)
			buf.append("v ").append(hx[i]).append((char)' ').append(hy[i])
				.append((char)' ').append(hei).append((char)_br);
		// 2 cx + hx ~ 2 cx + 2 hx : bottom inner edge
		for(int i = 0; i < hx.length; i++)
			buf.append("v ").append(hx[i]).append((char)' ').append(hy[i])
				.append((char)' ').append(hei - dep).append((char)_br);
		
		buf.append("f");
		for(int i = cx.length; i >= 1; i--)
			buf.append((char)' ').append(i);
		buf.append(_br);
		
		// top plane(holed)
		buf.append("f");
		for(int i = 1; i <= cx.length; i++)
			buf.append((char)' ').append(i + cx.length);
		buf.append((char)' ').append(1 + cx.length);
		for(int i = hx.length; i >= 1; i--)
			buf.append((char)' ').append(2 * cx.length + i);
		buf.append((char)' ').append(2 * cx.length + hx.length);
		buf.append((char)' ').append(cx.length + 1);
		buf.append(_br);
		
		// bottom plane(in the hole)
		buf.append("f");
		for(int i = 1; i <= hx.length; i++)
			buf.append((char)' ').append(2 * cx.length + hx.length + i);
		buf.append(_br);
		// side
		for(int i = 1; i < cx.length; i++)
			buf.append("f ").append(i).append((char)' ').append(i + 1)
					.append((char)' ').append(cx.length + i + 1)
					.append((char)' ').append(cx.length + i).append(_br);
		buf.append("f ").append(cx.length).append((char)' ').append(1)
				.append((char)' ').append(cx.length + 1)
				.append((char)' ').append(2 * cx.length).append(_br);
		
		for(int i = 1; i < hx.length; i++)
			buf.append("f ").append(2 * cx.length + i)
					.append((char)' ').append(2 * cx.length + i + 1)
					.append((char)' ').append(2 * cx.length + hx.length + i + 1)
					.append((char)' ').append(2 * cx.length + hx.length + i)
					.append(_br);
		buf.append("f ").append(2 * cx.length + hx.length)
				.append((char)' ').append(2 * cx.length + 1)
				.append((char)' ').append(2 * cx.length + hx.length + 1)
				.append((char)' ').append(2 * cx.length + hx.length * 2).append(_br);
		
		RobotCompModel str = new RobotCompModel();
		str.load(new java.io.StringReader(buf.toString()));
		
		
		PhysicalData pd = str.getPhysicalData();
		pd.volume = _getarea(cx, cy) * hei - _getarea(hx, hy) * dep;
		pd.mass = hc.getWeight();
		pd.centerMassPoint = new Point3f();
		_getcm(cx, cy, hx, hy, hei, dep, pd.centerMassPoint);
		pd.inertia = new Matrix3f();
		MathUtility.addInertiaTensor(str.getPoints(), str.getFaces(),
				pd.mass,
				-pd.centerMassPoint.x,
				-pd.centerMassPoint.y,
				-pd.centerMassPoint.z,
				pd.inertia);

		BoundObject.CompoundBounds cb = new BoundObject.CompoundBounds();
		_genBound(hx, hy, hei, dep, cb);
		pd.bounds = cb;

		AppearanceData ap = str.getApprData();
		ap.setColor(java.awt.Color.gray);
		
		
		return str;
	}// end generate
	
	private void rearrange(float[] xcoords, float[] ycoords){
		float val = 0;
		float tmp;
		float vec1x, vec1y, vec2x, vec2y;
		
		for(int i = 1; i < xcoords.length; i++){
			vec1x = xcoords[i - 1];
			vec1y = ycoords[i - 1];
			vec2x = xcoords[i] - xcoords[i - 1];
			vec2y = ycoords[i] - ycoords[i - 1];
			val += vec1x * vec2y - vec2x * vec1y;
		}
		vec1x = xcoords[xcoords.length - 1];
		vec1y = ycoords[xcoords.length - 1];
		vec2x = xcoords[0] - xcoords[xcoords.length - 1];
		vec2y = ycoords[0] - ycoords[xcoords.length - 1];
		val += vec1x * vec2y - vec2x * vec1y;
		
		if(val < 0){
			for(int i = 0; i < xcoords.length; i++){
				tmp = xcoords[i];
				xcoords[i] = xcoords[xcoords.length - i - 1];
				xcoords[xcoords.length - i - 1] = tmp;
			}
			for(int i = 0; i < ycoords.length; i++){
				tmp = ycoords[i];
				ycoords[i] = ycoords[ycoords.length - i - 1];
				ycoords[ycoords.length - i - 1] = tmp;
			}
		}
	}// end rearrange
	private float _getarea(float[] xcoords, float[] ycoords){
		float val = 0;
		float tmp;
		float vec1x, vec1y, vec2x, vec2y;
		
		for(int i = 1; i < xcoords.length; i++){
			vec1x = xcoords[i - 1];
			vec1y = ycoords[i - 1];
			vec2x = xcoords[i] - xcoords[i - 1];
			vec2y = ycoords[i] - ycoords[i - 1];
			val += vec1x * vec2y - vec2x * vec1y;
		}
		vec1x = xcoords[xcoords.length - 1];
		vec1y = ycoords[xcoords.length - 1];
		vec2x = xcoords[0] - xcoords[xcoords.length - 1];
		vec2y = ycoords[0] - ycoords[xcoords.length - 1];
		val += vec1x * vec2y - vec2x * vec1y;
		return Math.abs(val);
	}// end _getarea(float[], float[])
	private void _getcm(float[] cx, float[] cy, float[] hx, float[] hy, float hei, float dep, Point3f cm){
		Point3f ccm = new Point3f(), hcm = new Point3f();
		
		for(int i = 0; i < cx.length; i++){
			ccm.x += cx[i];
			ccm.y += cy[i];
		}
		ccm.x /= cx.length;
		ccm.y /= cx.length;
		ccm.z = hei / 2.0f;
		
		for(int i = 0; i < hx.length; i++){
			hcm.x += hx[i];
			hcm.y += hy[i];
		}
		hcm.x /= hx.length;
		hcm.y /= hy.length;
		hcm.z = hei - dep / 2.0f;
		
		float cvol = _getarea(cx, cy) * hei;
		float hvol = _getarea(hx, hy) * dep;
		cm.x = (cvol * ccm.x - hvol * hcm.x) / (cvol - hvol);
		cm.y = (cvol * ccm.y - hvol * hcm.y) / (cvol - hvol);
		cm.z = (cvol * ccm.z - hvol * hcm.z) / (cvol - hvol);
	}
	private void _genBound(float[] hx, float[] hy, float hei, float dep, BoundObject.CompoundBounds cb){
		float nx = 0, ny = 0, nz = 0;
		for(int i = 1; i <= hx.length; i++){
			BoundObject.Plane bp = new BoundObject.Plane();
			nz = 0;
			nx = -(hy[i % hx.length] - hy[i - 1]);
			ny = (hx[i % hx.length] - hx[i - 1]);
			float normallen = (float)Math.sqrt(nx * nx + ny * ny);
			nx /= normallen;
			ny /= normallen;
			bp.set(nx, ny, nz, new Point3f(hx[i - 1], hy[i - 1], hei));
			
			cb.add(bp);
		}
		
		BoundObject.Plane bottom_bp = new BoundObject.Plane();
		bottom_bp.set(0, 0, 1, new Point3f(hx[0], hy[0], (hei - dep)));
		cb.add(bottom_bp);
	}
	
	
	
	public static void main(String[] argv) throws Exception{
		new StageModelGenerator().generate(new Stage());
	}// end main()
}
