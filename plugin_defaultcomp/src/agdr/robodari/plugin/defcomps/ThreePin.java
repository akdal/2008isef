package agdr.robodari.plugin.defcomps;

import java.awt.*;

import javax.swing.Icon;

import agdr.robodari.comp.*;

public abstract class ThreePin implements Sender, Receiver{
	public final static float DEFAULT_PINLENGTH = 0.3F;
	
	private Note note = new Note();
	private float pinLength = DEFAULT_PINLENGTH;
	private boolean enabled = true;
	private Icon simpleIcon = new ThreePinIcon();
	private String name;
	
	
	
	public String getName()
	{ return name; }
	public void setName(String name)
	{ this.name = name; }
	
	public Icon getSimpleIcon()
	{ return simpleIcon; }
	
	public boolean isEnabled()
	{ return enabled; }
	public void setEnabled(boolean enabled)
	{ this.enabled = enabled; }
		
	public Note getNote()
	{ return note; }
	public void setNote(Note n)
	{ this.note = n; }
	public float getPinLength()
	{ return pinLength; }
	public void setPinLength(float f)
	{ this.pinLength = f; }

	public boolean hasReceiver()
	{ return false; }
	public boolean hasSender()
	{ return false; }
	public boolean hasSubComponent()
	{ return false; }
	public Receiver[] getReceivers()
	{ return null; }
	public Sender[] getSenders()
	{ return null; }
	public RobotComponent[] getSubComponents()
	{ return null; }
	
	public final boolean equals(Object o)
	{ return o == this; }
	

	private static class ThreePinIcon implements Icon, java.io.Serializable{
		public int getIconWidth()
		{ return 32; }
		public int getIconHeight()
		{ return 11; }
		public void paintIcon(Component c, Graphics g, int x, int y){
			if(g == null) return;
			
			Color original = g.getColor();
			g.setColor(Color.white);
			g.fillRect(x + 1, y + 1, 30, 9);
			g.setColor(original);
			g.drawRect(x + 1, y + 1, 30, 9);
			g.drawRect(x + 5, y + 4, 3, 3);
			g.drawRect(x + 14, y + 4, 3, 3);
			g.drawRect(x + 23, y + 4, 3, 3);
		}// end paintIcon(Component, Graphics, int, int)
	}// end class OnePinIcon
}
