package agdr.robodari.plugin.defcomps;

import java.io.StringReader;

import javax.vecmath.*;


import agdr.library.math.MathUtility;
import agdr.robodari.comp.model.*;


public class ThreePinModelGenerator implements ModelGenerator<ThreePin>{
	private final static int PINCOUNT = 3;
	
	private StringBuffer[] facecodes = new StringBuffer[PINCOUNT];
	
	public ThreePinModelGenerator(){
		for(int i = 0; i < PINCOUNT; i++){
			facecodes[i] = new StringBuffer();
			append(facecodes[i], i, 17, 1, 2, 3, 4, 1, 17, 20, 19, 18, 17);
			append(facecodes[i], i, 6, 7, 3, 2);
			append(facecodes[i], i, 7, 8, 4, 3);
			append(facecodes[i], i, 5, 6, 2, 1);
			append(facecodes[i], i, 4, 8, 5, 1);
			append(facecodes[i], i, 5, 8, 7, 6, 5, 12, 9, 10, 11, 12, 5);
			append(facecodes[i], i, 16, 15, 14, 13);
			append(facecodes[i], i, 14, 15, 11, 10);
			append(facecodes[i], i, 15, 16, 12, 11);
			append(facecodes[i], i, 13, 14, 10, 9);
			append(facecodes[i], i, 12, 16, 13, 9);
			append(facecodes[i], i, 21, 22, 23, 24);
			append(facecodes[i], i, 18, 19, 23, 22);
			append(facecodes[i], i, 19, 20, 24, 23);
			append(facecodes[i], i, 17, 18, 22, 21);
			append(facecodes[i], i, 20, 17, 21, 24);
		}
	}// end Constructor
	
	private void append(StringBuffer buf, int i,
			int... coords){
		buf.append("f");
		int plus = 24 * (i);
		for(Integer eachi : coords)
			buf.append(" ").append(eachi + plus);
		buf.append("\n");
	}// edn append(StringBuffer, int, int, int...)
	
	
	
	public RobotCompModel generate(ThreePin model) throws Exception{
		double left, top;
		StringBuffer code = new StringBuffer();
		
		String prfx = "v ", bl = " ";
		for(int i = 0; i < PINCOUNT; i++){
			left = (double)i * 2.5 / 10.0;
			top = 0;
			
			code.append(prfx).append(top).append(bl).append(left).
					append(" 0\n")
				.append(prfx).append(top).append(bl).append(left + 0.246).
					append(" 0\n")
				.append(prfx).append(top + 0.246).append(bl).append(left + 0.246).
					append(" 0\n")
				.append(prfx).append(top + 0.246).append(bl).append(left).
					append(" 0\n")
				
				.append(prfx).append(top).append(bl).append(left).
					append(" 0.25\n")
				.append(prfx).append(top).append(bl).append(left + 0.246).
					append(" 0.25\n")
				.append(prfx).append(top + 0.246).append(bl).append(left + 0.246).
					append(" 0.25\n")
				.append(prfx).append(top + 0.246).append(bl).append(left).
					append(" 0.25\n")
				
				.append(prfx).append(top + 0.10).append(bl).append(left + 0.10).
					append(" 0.25\n")
				.append(prfx).append(top + 0.15).append(bl).append(left + 0.10).
					append(" 0.25\n")
				.append(prfx).append(top + 0.15).append(bl).append(left + 0.15).
					append(" 0.25\n")
				.append(prfx).append(top + 0.10).append(bl).append(left + 0.15).
					append(" 0.25\n")
					
				.append(prfx).append(top + 0.10).append(bl).append(left + 0.10).
					append(" 0.85\n")
				.append(prfx).append(top + 0.15).append(bl).append(left + 0.10).
					append(" 0.85\n")
				.append(prfx).append(top + 0.15).append(bl).append(left + 0.15).
					append(" 0.85\n")
				.append(prfx).append(top + 0.10).append(bl).append(left + 0.15).
					append(" 0.85\n")
						
				.append(prfx).append(top + 0.10).append(bl).append(left + 0.10).
					append(" 0\n")
				.append(prfx).append(top + 0.15).append(bl).append(left + 0.10).
					append(" 0\n")
				.append(prfx).append(top + 0.15).append(bl).append(left + 0.15).
					append(" 0\n")
				.append(prfx).append(top + 0.10).append(bl).append(left + 0.15).
					append(" 0\n")
				
				.append(prfx).append(top + 0.10).append(bl).append(left + 0.10).
					append(bl).append(-1.0 * model.getPinLength())
					.append("\n")
				.append(prfx).append(top + 0.15).append(bl).append(left + 0.10).
					append(bl).append(-1.0 * model.getPinLength())
					.append("\n")
				.append(prfx).append(top + 0.15).append(bl).append(left + 0.15).
					append(bl).append(-1.0 * model.getPinLength())
					.append("\n")
				.append(prfx).append(top + 0.10).append(bl).append(left + 0.15).
					append(bl).append(-1.0 * model.getPinLength())
					.append("\n")
				.append("\n");
			
			code.append(facecodes[i]);
			code.append("\n");
		}
		StringReader sr = new StringReader(code.toString());
		RobotCompModel str = new RobotCompModel();
		str.load(sr);
		
		PhysicalData pd = str.getPhysicalData();
		Point3f[] ps = str.getPoints();
		int[][] faces = str.getFaces();
		pd.mass = 0.3f;
		pd.volume = MathUtility.getVolume(ps, faces);
		pd.bounds = null;
		pd.centerMassPoint = MathUtility.getCenterMassPoint(ps, faces, pd.centerMassPoint);
		pd.inertia = new Matrix3f();
		MathUtility.addInertiaTensor(ps, faces,
				pd.mass, pd.centerMassPoint.x, pd.centerMassPoint.y,
				pd.centerMassPoint.z, pd.inertia);
		
		AppearanceData ad = str.getApprData();
		ad.setColor(java.awt.Color.black);
		
		return str;
	}// end generate(ThreePin)
}
