package agdr.robodari.plugin.defcomps;

import agdr.robodari.comp.*;

public class Transistor1 implements RobotComponent{
	public final static double LEGLENGTH_DEFAULT = 0.3;
	
	private double leglength;
	private Note note;
	private String name;
	private RobotComponent parent;
	
	public Transistor1()
	{ this.leglength = LEGLENGTH_DEFAULT; 
		this.note = new Note();
	}// end Constructor()
	
	public Note getNote()
	{ return note; }
	public void setNote(Note n)
	{ this.note = n; }
	public String getName()
	{ return name; }
	public void setName(String name)
	{ this.name = name; }

	public boolean hasSubComponent()
	{ return false; }
	public boolean hasSender()
	{ return false; }
	public boolean hasReceiver()
	{ return false; }
	public RobotComponent[] getSubComponents()
	{ return null; }
	public Sender[] getSenders()
	{ return null; }
	public Receiver[] getReceivers()
	{ return null; }

	
	public RobotComponent getParent()
	{ return parent; }
	public void setParent(RobotComponent p){
		this.parent = p;
	}// end setParent(RobotComponent)
	
	public boolean suppliesPower()
	{ return false; }
	public PowerSupply getPowerSupply()
	{ return null; }
	
	public double getLegLength()
	{ return leglength; }
	public void setLegLength(double leglen)
	{ this.leglength = leglen; }
}

