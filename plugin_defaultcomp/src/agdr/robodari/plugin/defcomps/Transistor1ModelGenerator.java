package agdr.robodari.plugin.defcomps;

import javax.vecmath.*;


import agdr.library.math.MathUtility;
import agdr.robodari.comp.model.*;
import agdr.robodari.plugin.rme.rlztion.*;

import com.sun.j3d.loaders.objectfile.ObjectFile;
import java.io.StringWriter;
import java.io.StringReader;


public class Transistor1ModelGenerator implements ModelGenerator<Transistor1>{
	public final static String GENPROP_LENGTH = "leglength";
	private final static StringBuffer uppercode, lowercode;
	static{
		uppercode = new StringBuffer();
		lowercode = new StringBuffer();

		uppercode.append("g transistor\n");
		uppercode.append("\n");
		uppercode.append("v 0.50 0.0 0\n");
		uppercode.append("v 0.47524223 0.10847094 0\n");
		uppercode.append("v 0.40587244 0.19545788 0\n");
		uppercode.append("v 0.30563023 0.24373198 0\n");
		uppercode.append("v 0.19436977 0.24373198 0\n");
		uppercode.append("v 0.09412755 0.19545788 0\n");
		uppercode.append("v 0.024757783 0.10847094 0\n");
		uppercode.append("v 0.00 0 0\n");
		uppercode.append("v 0.1 -0.15 0\n");
		uppercode.append("v 0.4 -0.15 0\n");
		uppercode.append("# 10\n");
		uppercode.append("\n");
		uppercode.append("v 0.50 0.0 0.5\n");
		uppercode.append("v 0.47524223 0.10847094 0.5\n");
		uppercode.append("v 0.40587244 0.19545788 0.5\n");
		uppercode.append("v 0.30563023 0.24373198 0.5\n");
		uppercode.append("v 0.19436977 0.24373198 0.5\n");
		uppercode.append("v 0.09412755 0.19545788 0.5\n");
		uppercode.append("v 0.024757783 0.10847094 0.5\n");
		uppercode.append("v 0.0 0.0 0.5\n");
		uppercode.append("v 0.1 -0.15 0.5\n");
		uppercode.append("v 0.4 -0.15 0.5\n");
		uppercode.append("# 20\n");
		uppercode.append("\n");
		uppercode.append("# legs\n");
		uppercode.append("\n");
		uppercode.append("v 0.1 0.025 0\n");
		uppercode.append("v 0.1 -0.025 0\n");
		uppercode.append("v 0.15 -0.025 0\n");
		uppercode.append("v 0.15 0.025 0\n");
		uppercode.append("v 0.0 0.025 -0.2\n");
		uppercode.append("v 0.0 -0.025 -0.2\n");
		uppercode.append("v 0.05 -0.025 -0.2\n");
		uppercode.append("v 0.05 0.025 -0.2\n");

		lowercode.append("# 56\n");
		lowercode.append("\n");
		lowercode.append("\n");
		lowercode.append("# bottom\n");
		lowercode.append("f 48 45 36 33 24 21 8 7 6 5 4 3 2 1\n");
		lowercode.append("f 21 22 23 24 33 34 35 36 45 46 47 48 1 10 9 8\n");
		lowercode.append("# top\n");
		lowercode.append("f 11 12 13 14 15 16 17 18 19 20\n");
		lowercode.append("# side\n");
		lowercode.append("f 19 9 10 20\n");
		lowercode.append("f 18 8 9 19\n");
		lowercode.append("f 17 7 8 18\n");
		lowercode.append("f 16 6 7 17\n");
		lowercode.append("f 15 5 6 16\n");
		lowercode.append("f 14 4 5 15\n");
		lowercode.append("f 13 3 4 14\n");
		lowercode.append("f 12 2 3 13\n");
		lowercode.append("f 11 1 2 12\n");
		lowercode.append("f 20 10 1 11\n");
		lowercode.append("\n");
		lowercode.append("# legs\n");
		lowercode.append("f	25	26	22	21\n");
		lowercode.append("f	26	27	23	22\n");
		lowercode.append("f	27	28	24	23\n");
		lowercode.append("f	28	25	21	24\n");
		lowercode.append("f	29	30	26	25\n");
		lowercode.append("f	32	29	25	28\n");
		lowercode.append("f	31	32	28	27\n");
		lowercode.append("f	30	31	27	26\n");
		lowercode.append("f	32	31	30	29\n");
		lowercode.append("\n");
		lowercode.append("f	37	38	34	33\n");
		lowercode.append("f	38	39	35	34\n");
		lowercode.append("f	39	40	36	35\n");
		lowercode.append("f	40	37	33	36\n");
		lowercode.append("f	41	42	38	37\n");
		lowercode.append("f	44	41	37	40\n");
		lowercode.append("f	43	44	40	39\n");
		lowercode.append("f	42	43	39	38\n");
		lowercode.append("f	44	43	42	41\n");
		lowercode.append("\n");
		lowercode.append("f	49	50	46	45\n");
		lowercode.append("f	50	51	47	46\n");
		lowercode.append("f	51	52	48	47\n");
		lowercode.append("f	52	49	45	48\n");
		lowercode.append("f	53	54	50	49\n");
		lowercode.append("f	56	53	49	52\n");
		lowercode.append("f	55	56	52	51\n");
		lowercode.append("f	54	55	51	50\n");
		lowercode.append("f	56	55	54	53\n");
	}// end static block
	
	public RobotCompModel generate(Transistor1 model) throws Exception{
		double length = model.getLegLength();
		
		StringBuffer buf = new StringBuffer();

		buf.append(uppercode);
		buf.append("v 0.0 0.025 " + (float)(-0.2 - length) + "\n");
		buf.append("v 0.0 -0.025 " + (float)(-0.2 - length) + "\n");
		buf.append("v 0.05 -0.025 " + (float)(-0.2 - length) + "\n");
		buf.append("v 0.05 0.025 " + (float)(-0.2 - length) + "\n");
		buf.append("# 32\n");
		buf.append("\n");
		buf.append("v 0.225 0.025 0\n");
		buf.append("v 0.225 -0.025 0\n");
		buf.append("v 0.275 -0.025 0\n");
		buf.append("v 0.275 0.025 0\n");
		buf.append("v 0.225 0.025 -0.2\n");
		buf.append("v 0.225 -0.025 -0.2\n");
		buf.append("v 0.275 -0.025 -0.2\n");
		buf.append("v 0.275 0.025 -0.2\n");
		buf.append("v 0.225 0.025 " + (float)(-0.2 - length) + "\n");
		buf.append("v 0.225 -0.025 " + (float)(-0.2 - length) + "\n");
		buf.append("v 0.275 -0.025 " + (float)(-0.2 - length) + "\n");
		buf.append("v 0.275 0.025 " + (float)(-0.2 - length) + "\n");
		buf.append("# 44\n");
		buf.append("\n");
		buf.append("v 0.35 0.025 0\n");
		buf.append("v 0.35 -0.025 0\n");
		buf.append("v 0.4 -0.025 0\n");
		buf.append("v 0.4 0.025 0\n");
		buf.append("v 0.45 0.025 -0.2\n");
		buf.append("v 0.45 -0.025 -0.2\n");
		buf.append("v 0.5 -0.025 -0.2\n");
		buf.append("v 0.5 0.025 -0.2\n");
		buf.append("v 0.45 0.025 " + (float)(-0.2 - length) + "\n");
		buf.append("v 0.45 -0.025 " + (float)(-0.2 - length) + "\n");
		buf.append("v 0.5 -0.025 " + (float)(-0.2 - length) + "\n");
		buf.append("v 0.5 0.025 " + (float)(-0.2 - length) + "\n");
		buf.append(lowercode);
		
		StringReader sr = new StringReader(buf.toString());
		RobotCompModel str = new RobotCompModel();
		str.load(sr);
		
		PhysicalData pd = str.getPhysicalData();
		Point3f[] ps = str.getPoints();
		int[][] faces = str.getFaces();
		pd.mass = 0.3f;
		pd.volume = MathUtility.getVolume(ps, faces);
		pd.bounds = null;
		pd.centerMassPoint = MathUtility.getCenterMassPoint(ps, faces, pd.centerMassPoint);
		pd.inertia = new Matrix3f();
		MathUtility.addInertiaTensor(ps, faces,
				pd.mass, pd.centerMassPoint.x, pd.centerMassPoint.y,
				pd.centerMassPoint.z, pd.inertia);
		
		AppearanceData ad = str.getApprData();
		ad.setColor(java.awt.Color.black);
		
		return str;
	}// end generate(String[][] params)
}
