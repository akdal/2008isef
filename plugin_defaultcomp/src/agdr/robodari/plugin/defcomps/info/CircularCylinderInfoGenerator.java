package agdr.robodari.plugin.defcomps.info;

import static agdr.robodari.comp.info.InfoKeys.*;

import agdr.robodari.comp.info.*;
import agdr.robodari.plugin.defcomps.*;

public class CircularCylinderInfoGenerator implements InfoGenerator<CircularCylinder>{
	public Info generateInfo(CircularCylinder comp){
		Info info = new Info(comp);
		info.put(NAME, "Cylinder");
		info.put(BUSINESS, "");
		info.put(ISOUTEROBJECT, false);
		info.put(WEIGHT, comp.getMass());
		
		info.put(CircularCylinderInfoKeys.HEIGHT, comp.getHeight());
		info.put(CircularCylinderInfoKeys.RADIUS, comp.getRadius());
		
		return info;
	}// end generateInfo
}
