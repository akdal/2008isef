package agdr.robodari.plugin.defcomps.info;


import agdr.robodari.comp.info.*;
import agdr.robodari.plugin.defcomps.*;
import static agdr.robodari.comp.info.InfoKeys.*;

public class ElectricPanelInfoGenerator implements InfoGenerator<ElectricPanel>{
	public Info generateInfo(ElectricPanel panel){
		Info info = new Info(panel);
		info.put(NAME, "Panel");
		info.put(BUSINESS, "");
		info.put(WEIGHT, new Float(area(panel) * panel.getThickness() * 1.5f));
		info.put(ISOUTEROBJECT, false);
		return info;
	}// end generateInfo(ElectricPanel)
	float area(ElectricPanel ep){
		float[] xs = ep.getXCoords();
		float[] ys = ep.getYCoords();
		
		float area = 0;
		for(int i = 0; i < xs.length - 1; i++){
			float x1 = xs[i], y1 = ys[i];
			float x2 = xs[i + 1] - xs[i], y2 = ys[i + 1] - ys[i];
			area += x1*y2 - x2*y1;
		}
		return Math.abs(area);
	}// end area(ElectricPanel)
}
