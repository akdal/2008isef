package agdr.robodari.plugin.defcomps.info;

import static agdr.robodari.comp.info.InfoKeys.*;
import agdr.robodari.plugin.defcomps.*;
import agdr.robodari.comp.info.*;

public class GenericControllerInfoGenerator implements InfoGenerator<GenericController>{
	public Info generateInfo(GenericController ctrler){
		Info info = new Info(ctrler);
		info.put(NAME, "Generic-Controller");
		info.put(BUSINESS, "");
		info.put(ISOUTEROBJECT, false);
		info.put(WEIGHT, new Float(ctrler.getMass()));
		return info;
	}// end generateInfo
}
