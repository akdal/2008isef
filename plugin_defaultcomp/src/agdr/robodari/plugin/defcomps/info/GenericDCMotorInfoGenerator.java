package agdr.robodari.plugin.defcomps.info;

import agdr.robodari.comp.info.*;
import agdr.robodari.plugin.defcomps.*;
import static agdr.robodari.comp.info.InfoKeys.*;

public class GenericDCMotorInfoGenerator implements InfoGenerator<GenericDCMotor>{
	public Info generateInfo(GenericDCMotor motor){
		Info info = new Info(motor);
		info.put(NAME, "GenericDCMotor");
		info.put(BUSINESS, "");
		info.put(ISOUTEROBJECT, false);
		info.put(WEIGHT, motor.getWeight());
		
		info.put(GenericDCMotorInfoKeys.TORQUEFUNC, motor.getTorqueFunc().getClass().getSimpleName());
		
		return info;
	}// end generateInfo
}
