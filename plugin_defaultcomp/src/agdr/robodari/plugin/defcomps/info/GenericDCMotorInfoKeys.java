package agdr.robodari.plugin.defcomps.info;

import agdr.robodari.comp.info.*;

public enum GenericDCMotorInfoKeys implements InfoKey{
	TORQUEFUNC("TorqueFunction", InfoKey.TYPE_STRING, null);
	
	public final String name;
	public final int type;
	public final Unit unit;
	GenericDCMotorInfoKeys(String v, int type, Unit unit){
		this.name = v;
		this.type = type;
		this.unit = unit;
	}// end Constructor(String, int, Unit)
	
	public String getName()
	{ return name; }
	public int getType()
	{ return type; }
	public Unit getUnit()
	{ return unit; }
}
