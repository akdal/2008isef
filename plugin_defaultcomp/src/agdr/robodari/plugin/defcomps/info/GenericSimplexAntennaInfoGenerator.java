package agdr.robodari.plugin.defcomps.info;

import static agdr.robodari.comp.info.InfoKeys.*;
import agdr.robodari.comp.info.*;
import agdr.robodari.plugin.defcomps.*;

public class GenericSimplexAntennaInfoGenerator implements InfoGenerator<GenericSimplexAntenna>{
	public Info generateInfo(GenericSimplexAntenna gsa){
		Info info = new Info(gsa);
		info.put(NAME, "Generic-Simplex Antenna");
		info.put(BUSINESS, "");
		info.put(ISOUTEROBJECT, false);
		info.put(WEIGHT, new Float(gsa.getMass()));
		return info;
	}// end generateInfo
}
