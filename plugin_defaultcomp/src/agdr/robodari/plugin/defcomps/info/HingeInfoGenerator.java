package agdr.robodari.plugin.defcomps.info;


import agdr.robodari.comp.info.*;
import agdr.robodari.comp.model.*;
import agdr.robodari.plugin.defcomps.*;
import static agdr.robodari.comp.info.InfoKeys.*;

public class HingeInfoGenerator implements InfoGenerator<Hinge>{
	public Info generateInfo(Hinge hinge){
		Info info = new Info(hinge);
		info.put(NAME, "Hinge");
		info.put(BUSINESS, "");
		info.put(ISOUTEROBJECT, false);
		
		float weight = 0;
		if(hinge.getFixedObject() != null){
			if(hinge.getFixedObject().isGroup()){
				RobotCompModel model = hinge.getFixedObject().getIntegratedModel();
				
				if(model.getGroupPhysData() == null)
					weight += model.calcGroupPhysData().mass;
				else
					weight += model.getGroupPhysData().mass;
			}else{
				RobotCompModel model = hinge.getFixedObject().getModel();
				
				if(model.getPhysicalData() != null)
					weight += model.getPhysicalData().mass;
			}
		}
		if(hinge.getHangingObject() != null){
			if(hinge.getHangingObject().isGroup()){
				RobotCompModel model = hinge.getHangingObject().getIntegratedModel();
				
				if(model.getGroupPhysData() == null)
					weight += model.calcGroupPhysData().mass;
				else
					weight += model.getGroupPhysData().mass;
			}else{
				RobotCompModel model = hinge.getHangingObject().getModel();
				
				if(model.getPhysicalData() != null)
					weight += model.getPhysicalData().mass;
			}
		}
		info.put(WEIGHT, weight);
		
		return info;
	}// endgenerateInfo(Hinge)
}
