package agdr.robodari.plugin.defcomps.info;

//import org.agdr.robodari..BasicUnit;

import agdr.robodari.comp.info.*;
import agdr.robodari.plugin.defcomps.*;
import static agdr.robodari.comp.info.InfoKeys.*;
import static agdr.robodari.comp.info.LEDInfoKeys.*;

public class LED1InfoGenerator implements InfoGenerator<LED1>{
	public Info generateInfo(LED1 model){
		Info info = new Info(model);
		info.put(NAME, "LED");
		info.put(BUSINESS, "");
		info.put(ISOUTEROBJECT, false);
		info.put(WEIGHT, new Float(0.5));
		
		info.put(RADIUS, model.getRadius());
		info.put(PLUSRODLENGTH, model.getPlusRodLength());
		info.put(MINUSRODLENGTH, model.getMinusRodLength());
		info.put(COLOR, model.getColor());
		
		return info;
	}// end generateInfo(LED1)
}
