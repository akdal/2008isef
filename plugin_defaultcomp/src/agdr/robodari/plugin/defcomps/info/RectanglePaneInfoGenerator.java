package agdr.robodari.plugin.defcomps.info;

import static agdr.robodari.comp.info.InfoKeys.*;


import agdr.robodari.comp.info.*;
import agdr.robodari.plugin.defcomps.*;

public class RectanglePaneInfoGenerator implements InfoGenerator<RectanglePane>{
	public Info generateInfo(RectanglePane model){
		Info info = new Info(model);
		info.put(NAME, "Rectangle Pane");
		info.put(BUSINESS, "");
		info.put(ISOUTEROBJECT, false);
		info.put(WEIGHT, model.getWeight());
		
		return info;
	}// end generateInfo(LED1)
}
