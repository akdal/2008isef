package agdr.robodari.plugin.defcomps.info;


import agdr.robodari.comp.info.*;
import agdr.robodari.plugin.defcomps.*;
import static agdr.robodari.comp.info.InfoKeys.*;

public class RoundedPlasticPaneInfoGenerator implements
		InfoGenerator<RoundedRectanglePane>{
	public Info generateInfo(RoundedRectanglePane pane){
		Info info = new Info(pane);
		info.put(BUSINESS, "");
		info.put(NAME, "Rounded Panel");
		info.put(ISOUTEROBJECT, new Boolean(false));
		info.put(WEIGHT, new Float(pane.getWeight()));
		return info;
	}// end generateInfo(RoundedPlasticPane)
}
