package agdr.robodari.plugin.defcomps.info;

import agdr.robodari.comp.info.*;
import agdr.robodari.plugin.defcomps.*;
import static agdr.robodari.comp.info.InfoKeys.*;

public class SphereInfoGenerator implements InfoGenerator<Sphere>{
	public Info generateInfo(Sphere model){
		Info info = new Info(model);
		info.put(NAME, "SPHERE");
		info.put(BUSINESS, "");
		info.put(ISOUTEROBJECT, false);
		info.put(WEIGHT, new Float(model.getDensity() * 4.0f *
				model.getRadius() * model.getRadius() * model.getRadius() * Math.PI
				/ 3.0f));
		
		info.put(SphereInfoKeys.RADIUS, model.getRadius());
		
		return info;
	}// end generateInfo(LED1)
}
