package agdr.robodari.plugin.defcomps.info;

import agdr.robodari.comp.info.*;

public enum SphereInfoKeys implements InfoKey{
	RADIUS("radius", TYPE_QUANTITY, new BasicUnit.CentiMeter());

	public final String name;
	public final int type;
	public final Unit unit;
	SphereInfoKeys(String v, int type, Unit unit){
		this.name = v;
		this.type = type;
		this.unit = unit;
	}// end Constructor(String, int, Unit)
	
	public String getName()
	{ return name; }
	public int getType()
	{ return type; }
	public Unit getUnit()
	{ return unit; }
}
