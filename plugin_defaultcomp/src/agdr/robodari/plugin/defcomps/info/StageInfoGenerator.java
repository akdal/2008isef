package agdr.robodari.plugin.defcomps.info;

import static agdr.robodari.comp.info.InfoKeys.*;
import static agdr.robodari.plugin.defcomps.info.StageInfoKeys.*;


import agdr.robodari.comp.info.*;
import agdr.robodari.plugin.defcomps.*;
import agdr.robodari.plugin.rme.rlztion.*;

public class StageInfoGenerator implements InfoGenerator<Stage>{
	public Info generateInfo(Stage comp){
		Info info = new Info(comp);
		info.put(NAME, "Stage");
		info.put(BUSINESS, "");
		info.put(ISOUTEROBJECT, false);
		info.put(WEIGHT, comp.getWeight());
		info.put(HEIGHT, comp.getHeight());
		
		return info;
	}// end generateInfo
}
