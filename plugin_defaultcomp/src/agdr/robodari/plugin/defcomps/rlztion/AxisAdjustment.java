package agdr.robodari.plugin.defcomps.rlztion;

import java.util.*;
import javax.vecmath.*;

import agdr.library.math.*;
import agdr.robodari.appl.*;
import agdr.robodari.plugin.defcomps.rlztion.AxisHolder.Order;
import agdr.robodari.plugin.rme.rlztion.*;


public class AxisAdjustment implements ObjectAdjustment{
	public final static int ADJTYPE_ATOB = 0;
	public final static int ADJTYPE_BTOA = 1;
	
	private World world;
	private ArrayList<Order> orders = new ArrayList<Order>();
	
	public AxisAdjustment(){}

	public void add(int adjustmentType, RealizationMap.Key aobjKey, RealizationMap.Key bobjKey,
			double axis1AbsPosX, double axis1AbsPosY, double axis1AbsPosZ,
			double axis2AbsPosX, double axis2AbsPosY, double axis2AbsPosZ){
		BugManager.printf("AxisAdj : addHinge : \n  axis1 : %g %g %g\n  axis2 : %g %g %g\n",
				axis1AbsPosX, axis1AbsPosY, axis1AbsPosZ,
				axis2AbsPosX, axis2AbsPosY, axis2AbsPosZ);
		Order o = new Order();
		o.aobjKey = aobjKey;
		o.bobjKey = bobjKey;
		o.type = adjustmentType;
		
		o.axis1AbsPosX = axis1AbsPosX;
		o.axis1AbsPosY = axis1AbsPosY;
		o.axis1AbsPosZ = axis1AbsPosZ;
		o.axis2AbsPosX = axis2AbsPosX;
		o.axis2AbsPosY = axis2AbsPosY;
		o.axis2AbsPosZ = axis2AbsPosZ;
		
		this.orders.add(o); 
	}// end addHinge
	
	public boolean effective(RealizationModel model, RealizationMap map, World world)
	{ return orders.size() != 0; }
	public void clear()
	{ this.orders.clear(); }

	public void init(RealizationModel model, RealizationMap rmap, World world){
		this.world = world;
		Matrix4d transf = new Matrix4d();
		Matrix4d transfInv = new Matrix4d();
		Matrix4f tmptrans = new Matrix4f();
		
		for(int i = 0; i < orders.size(); i++){
			Order o = orders.get(i);
			o.aobjIdx = rmap.getIndex(o.aobjKey);
			o.bobjIdx = rmap.getIndex(o.bobjKey);
			
			rmap.getInitialTransform(o.aobjKey, tmptrans);
			transf.set(tmptrans);
			MathUtility.fastInvert(transf, transfInv);
			// 상대좌표계로 변환 : 절대좌표계일 경우, 축이 움직일 때 계산 불가.
			o.axis1ARelPosX = o.axis1AbsPosX * transfInv.m00 + o.axis1AbsPosY * transfInv.m01 + o.axis1AbsPosZ * transfInv.m02 + transfInv.m03;
			o.axis1ARelPosY = o.axis1AbsPosX * transfInv.m10 + o.axis1AbsPosY * transfInv.m11 + o.axis1AbsPosZ * transfInv.m12 + transfInv.m13;
			o.axis1ARelPosZ = o.axis1AbsPosX * transfInv.m20 + o.axis1AbsPosY * transfInv.m21 + o.axis1AbsPosZ * transfInv.m22 + transfInv.m23;
			
			o.axis2ARelPosX = o.axis2AbsPosX * transfInv.m00 + o.axis2AbsPosY * transfInv.m01 + o.axis2AbsPosZ * transfInv.m02 + transfInv.m03;
			o.axis2ARelPosY = o.axis2AbsPosX * transfInv.m10 + o.axis2AbsPosY * transfInv.m11 + o.axis2AbsPosZ * transfInv.m12 + transfInv.m13;
			o.axis2ARelPosZ = o.axis2AbsPosX * transfInv.m20 + o.axis2AbsPosY * transfInv.m21 + o.axis2AbsPosZ * transfInv.m22 + transfInv.m23;

			rmap.getInitialTransform(o.bobjKey, tmptrans);
			transf.set(tmptrans);
			MathUtility.fastInvert(transf, transfInv);
			o.axis1BRelPosX = o.axis1AbsPosX * transfInv.m00 + o.axis1AbsPosY * transfInv.m01 + o.axis1AbsPosZ * transfInv.m02 + transfInv.m03;
			o.axis1BRelPosY = o.axis1AbsPosX * transfInv.m10 + o.axis1AbsPosY * transfInv.m11 + o.axis1AbsPosZ * transfInv.m12 + transfInv.m13;
			o.axis1BRelPosZ = o.axis1AbsPosX * transfInv.m20 + o.axis1AbsPosY * transfInv.m21 + o.axis1AbsPosZ * transfInv.m22 + transfInv.m23;
			
			o.axis2BRelPosX = o.axis2AbsPosX * transfInv.m00 + o.axis2AbsPosY * transfInv.m01 + o.axis2AbsPosZ * transfInv.m02 + transfInv.m03;
			o.axis2BRelPosY = o.axis2AbsPosX * transfInv.m10 + o.axis2AbsPosY * transfInv.m11 + o.axis2AbsPosZ * transfInv.m12 + transfInv.m13;
			o.axis2BRelPosZ = o.axis2AbsPosX * transfInv.m20 + o.axis2AbsPosY * transfInv.m21 + o.axis2AbsPosZ * transfInv.m22 + transfInv.m23;
		}
	}// end init

	
	Matrix4d aobjtrans = new Matrix4d(),
				bobjtrans = new Matrix4d();
	public void adjust(){
		double absa1px, absa1py, absa1pz, absb1px, absb1py, absb1pz,
				absa2px, absa2py, absa2pz, absb2px, absb2py, absb2pz;
		Vector3d aloc = new Vector3d(), bloc = new Vector3d();
		
		for(int i = 0; i < orders.size(); i++){
			Order io = orders.get(i);
			world.getTransform(io.aobjIdx, aobjtrans);
			world.getTransform(io.bobjIdx, bobjtrans);
			world.getTranslation(io.aobjIdx, aloc);
			world.getTranslation(io.bobjIdx, bloc);
			
			absa1px = (io.axis1ARelPosX * aobjtrans.m00 + io.axis1ARelPosY * aobjtrans.m01 + io.axis1ARelPosZ * aobjtrans.m02 + aobjtrans.m03);
			absa1py = (io.axis1ARelPosX * aobjtrans.m10 + io.axis1ARelPosY * aobjtrans.m11 + io.axis1ARelPosZ * aobjtrans.m12 + aobjtrans.m13);
			absa1pz = (io.axis1ARelPosX * aobjtrans.m20 + io.axis1ARelPosY * aobjtrans.m21 + io.axis1ARelPosZ * aobjtrans.m22 + aobjtrans.m23);

			absb1px = (io.axis1BRelPosX * bobjtrans.m00 + io.axis1BRelPosY * bobjtrans.m01 + io.axis1BRelPosZ * bobjtrans.m02 + bobjtrans.m03);
			absb1py = (io.axis1BRelPosX * bobjtrans.m10 + io.axis1BRelPosY * bobjtrans.m11 + io.axis1BRelPosZ * bobjtrans.m12 + bobjtrans.m13);
			absb1pz = (io.axis1BRelPosX * bobjtrans.m20 + io.axis1BRelPosY * bobjtrans.m21 + io.axis1BRelPosZ * bobjtrans.m22 + bobjtrans.m23);
			
			absa2px = (io.axis1ARelPosX * aobjtrans.m00 + io.axis1ARelPosY * aobjtrans.m01 + io.axis1ARelPosZ * aobjtrans.m02 + aobjtrans.m03);
			absa2py = (io.axis1ARelPosX * aobjtrans.m10 + io.axis1ARelPosY * aobjtrans.m11 + io.axis1ARelPosZ * aobjtrans.m12 + aobjtrans.m13);
			absa2pz = (io.axis1ARelPosX * aobjtrans.m20 + io.axis1ARelPosY * aobjtrans.m21 + io.axis1ARelPosZ * aobjtrans.m22 + aobjtrans.m23);

			absb2px = (io.axis1BRelPosX * bobjtrans.m00 + io.axis1BRelPosY * bobjtrans.m01 + io.axis1BRelPosZ * bobjtrans.m02 + bobjtrans.m03);
			absb2py = (io.axis1BRelPosX * bobjtrans.m10 + io.axis1BRelPosY * bobjtrans.m11 + io.axis1BRelPosZ * bobjtrans.m12 + bobjtrans.m13);
			absb2pz = (io.axis1BRelPosX * bobjtrans.m20 + io.axis1BRelPosY * bobjtrans.m21 + io.axis1BRelPosZ * bobjtrans.m22 + bobjtrans.m23);
			
			if(io.type == ADJTYPE_ATOB){
				// A를 움직인다.
				aloc.x += absb1px - absa1px;
				aloc.y += absb1py - absa1py;
				aloc.z += absb1pz - absa1pz;
			}else if(io.type == ADJTYPE_BTOA){
				bloc.x += absa1px - absb1px;
				bloc.y += absa1py - absb1py;
				bloc.z += absa1pz - absb1pz;
			}
			world.setTranslation(io.aobjIdx, aloc);
			world.setTranslation(io.bobjIdx, bloc);
		}
	}// end adjust()
	
	
	

	public static class Order{
		RealizationMap.Key aobjKey;
		int aobjIdx;
		RealizationMap.Key bobjKey;
		int bobjIdx;
		
		int type;
		
		double axis1AbsPosX, axis1AbsPosY, axis1AbsPosZ;
		double axis2AbsPosX, axis2AbsPosY, axis2AbsPosZ;

		double axis1ARelPosX, axis1ARelPosY, axis1ARelPosZ;
		double axis2ARelPosX, axis2ARelPosY, axis2ARelPosZ;
		double axis1BRelPosX, axis1BRelPosY, axis1BRelPosZ;
		double axis2BRelPosX, axis2BRelPosY, axis2BRelPosZ;
	}// ed class Order
}
