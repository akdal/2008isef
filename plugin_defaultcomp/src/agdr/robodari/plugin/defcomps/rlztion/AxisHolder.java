package agdr.robodari.plugin.defcomps.rlztion;

import java.util.*;
import javax.vecmath.*;

import agdr.library.math.*;
import agdr.robodari.appl.*;
import agdr.robodari.plugin.defcomps.*;
import agdr.robodari.plugin.rme.rlztion.*;

public class AxisHolder implements ForceGenerator{
	private final static Order[] NULLOARRAY = new Order[]{};
	private ArrayList<Order> orders;
	
	public AxisHolder()
	{ this.orders = new ArrayList<Order>(); }
	
	public void add(RealizationMap.Key aobjKey, RealizationMap.Key bobjKey,
			double axis1AbsPosX, double axis1AbsPosY, double axis1AbsPosZ,
			double axis2AbsPosX, double axis2AbsPosY, double axis2AbsPosZ){
		BugManager.printf("AxisHolder : addHinge : \n  axis1 : %g %g %g\n  axis2 : %g %g %g\n   aobjKey : %s, bobjKey : %s\n",
				axis1AbsPosX, axis1AbsPosY, axis1AbsPosZ,
				axis2AbsPosX, axis2AbsPosY, axis2AbsPosZ,
				aobjKey, bobjKey);
		Order o = new Order();
		o.aobjKey = aobjKey;
		o.bobjKey = bobjKey;
		
		o.axis1AbsPosX = axis1AbsPosX;
		o.axis1AbsPosY = axis1AbsPosY;
		o.axis1AbsPosZ = axis1AbsPosZ;
		o.axis2AbsPosX = axis2AbsPosX;
		o.axis2AbsPosY = axis2AbsPosY;
		o.axis2AbsPosZ = axis2AbsPosZ;
		
		this.orders.add(o); 
	}// end addHinge
	public void clear()
	{ this.orders.clear(); }
	public Order[] getorders()
	{ return orders.toArray(NULLOARRAY); }
	
	
	private World world;
	
	public int getGeneratorType()
	{ return ForceGenerator.LCP_NEEDED | ForceGenerator.DEPENDSON_OBJECT | ForceGenerator.DEPENDSON_POSITION; }
	
	public boolean effective(RealizationModel rmodel, RealizationMap rmap, World world){
		return orders.size() != 0;
	}// end effective
	
	public void init(RealizationModel model, RealizationMap rmap, World world){
		this.world = world;
		Matrix4d transf = new Matrix4d();
		Matrix4d transfInv = new Matrix4d();
		Matrix4f tmptrans = new Matrix4f();
		
		for(int i = 0; i < orders.size(); i++){
			Order o = orders.get(i);
			o.aobjIdx = rmap.getIndex(o.aobjKey);
			o.bobjIdx = rmap.getIndex(o.bobjKey);
			
			rmap.getInitialTransform(o.aobjKey, tmptrans);
			transf.set(tmptrans);
			MathUtility.fastInvert(transf, transfInv);
			// 상대좌표계로 변환 : 절대좌표계일 경우, 축이 움직일 때 계산 불가.
			o.axis1ARelPosX = o.axis1AbsPosX * transfInv.m00 + o.axis1AbsPosY * transfInv.m01 + o.axis1AbsPosZ * transfInv.m02 + transfInv.m03;
			o.axis1ARelPosY = o.axis1AbsPosX * transfInv.m10 + o.axis1AbsPosY * transfInv.m11 + o.axis1AbsPosZ * transfInv.m12 + transfInv.m13;
			o.axis1ARelPosZ = o.axis1AbsPosX * transfInv.m20 + o.axis1AbsPosY * transfInv.m21 + o.axis1AbsPosZ * transfInv.m22 + transfInv.m23;
			
			o.axis2ARelPosX = o.axis2AbsPosX * transfInv.m00 + o.axis2AbsPosY * transfInv.m01 + o.axis2AbsPosZ * transfInv.m02 + transfInv.m03;
			o.axis2ARelPosY = o.axis2AbsPosX * transfInv.m10 + o.axis2AbsPosY * transfInv.m11 + o.axis2AbsPosZ * transfInv.m12 + transfInv.m13;
			o.axis2ARelPosZ = o.axis2AbsPosX * transfInv.m20 + o.axis2AbsPosY * transfInv.m21 + o.axis2AbsPosZ * transfInv.m22 + transfInv.m23;

			rmap.getInitialTransform(o.bobjKey, tmptrans);
			transf.set(tmptrans);
			MathUtility.fastInvert(transf, transfInv);
			o.axis1BRelPosX = o.axis1AbsPosX * transfInv.m00 + o.axis1AbsPosY * transfInv.m01 + o.axis1AbsPosZ * transfInv.m02 + transfInv.m03;
			o.axis1BRelPosY = o.axis1AbsPosX * transfInv.m10 + o.axis1AbsPosY * transfInv.m11 + o.axis1AbsPosZ * transfInv.m12 + transfInv.m13;
			o.axis1BRelPosZ = o.axis1AbsPosX * transfInv.m20 + o.axis1AbsPosY * transfInv.m21 + o.axis1AbsPosZ * transfInv.m22 + transfInv.m23;
			
			o.axis2BRelPosX = o.axis2AbsPosX * transfInv.m00 + o.axis2AbsPosY * transfInv.m01 + o.axis2AbsPosZ * transfInv.m02 + transfInv.m03;
			o.axis2BRelPosY = o.axis2AbsPosX * transfInv.m10 + o.axis2AbsPosY * transfInv.m11 + o.axis2AbsPosZ * transfInv.m12 + transfInv.m13;
			o.axis2BRelPosZ = o.axis2AbsPosX * transfInv.m20 + o.axis2AbsPosY * transfInv.m21 + o.axis2AbsPosZ * transfInv.m22 + transfInv.m23;
		}
	}// end init

	Matrix4d atrans = new Matrix4d(),
				btrans = new Matrix4d();
	Matrix3d ainrtInv = new Matrix3d(),
			binrtInv = new Matrix3d();
	Vector3d avel = new Vector3d(),
			bvel = new Vector3d(),
			aangvel = new Vector3d(),
			bangvel = new Vector3d(),
			
			aaccl = new Vector3d(),
			baccl = new Vector3d(),
			aangaccl = new Vector3d(),
			bangaccl = new Vector3d(),
			
			aangmom = new Vector3d(),
			bangmom = new Vector3d();
	Vector3d ap_norm1 = new Vector3d(),
			ap_norm2 = new Vector3d(),
			ap_norm3 = new Vector3d();
	
	public void generateForce(){
		if(BugManager.DEBUG && World.W_DEBUG && World.CONTACT_DEBUG)
			BugManager.printf("HingeAxisHolder.genForce() start!!\n");
		
		ContactModel cmodel = world.getContactModel();

		double axis1x, axis1y, axis1z, axis2x, axis2y, axis2z;
		double relb_apx, relb_apy, relb_apz,
				rela_apx, rela_apy, rela_apz;
		
		for(int i = 0; i < orders.size(); i++){
			Order o = orders.get(i);
			if(BugManager.DEBUG && World.W_DEBUG && World.CONTACT_DEBUG)
				BugManager.printf("HingeAxisHolder : %d : \n", i);

			world.getTransform(o.aobjIdx, atrans);
			world.getTransform(o.bobjIdx, btrans);
			
			// normal방향 계산
			{
				//ap_norm1.set(atrans.m00, atrans.m10, atrans.m20);
				//ap_norm2.set(atrans.m01, atrans.m11, atrans.m21);
				//ap_norm3.set(atrans.m02, atrans.m12, atrans.m22);
				ap_norm1.set(1, 0, 0);
				ap_norm2.set(0, 1, 0);
				ap_norm3.set(0, 0, -1);
			}
			{
				world.getVelocity_bef(o.aobjIdx, avel);
				world.getAngularVelocity_bef(o.aobjIdx, aangvel);
				world.getVelocity_bef(o.bobjIdx, bvel);
				world.getAngularVelocity_bef(o.bobjIdx, bangvel);
				
				world.getAcceleration(o.aobjIdx, aaccl);
				world.getAngularAccl(o.aobjIdx, aangaccl);
				world.getAcceleration(o.bobjIdx, baccl);
				world.getAngularAccl(o.bobjIdx, bangaccl);

				world.getAngularMomentum_bef(o.aobjIdx, aangmom);
				world.getAngularMomentum_bef(o.bobjIdx, bangmom);
				world.getInrtTensor_Inv(o.aobjIdx, ainrtInv);
				world.getInrtTensor_Inv(o.bobjIdx, binrtInv);
			}
			
			// axis point 1에서
			{
				rela_apx = o.axis1ARelPosX * atrans.m00 + o.axis1ARelPosY * atrans.m01 + o.axis1ARelPosZ * atrans.m02;
				rela_apy = o.axis1ARelPosX * atrans.m10 + o.axis1ARelPosY * atrans.m11 + o.axis1ARelPosZ * atrans.m12;
				rela_apz = o.axis1ARelPosX * atrans.m20 + o.axis1ARelPosY * atrans.m21 + o.axis1ARelPosZ * atrans.m22;

				relb_apx = o.axis1BRelPosX * btrans.m00 + o.axis1BRelPosY * btrans.m01 + o.axis1BRelPosZ * btrans.m02;
				relb_apy = o.axis1BRelPosX * btrans.m10 + o.axis1BRelPosY * btrans.m11 + o.axis1BRelPosZ * btrans.m12;
				relb_apz = o.axis1BRelPosX * btrans.m20 + o.axis1BRelPosY * btrans.m21 + o.axis1BRelPosZ * btrans.m22;
					
				_addContact(cmodel, o,
						rela_apx, rela_apy, rela_apz,
						relb_apx, relb_apy, relb_apz);
			}
			
			// axis point 2에서 
			{
				rela_apx = o.axis2ARelPosX * atrans.m00 + o.axis2ARelPosY * atrans.m01 + o.axis2ARelPosZ * atrans.m02;
				rela_apy = o.axis2ARelPosX * atrans.m10 + o.axis2ARelPosY * atrans.m11 + o.axis2ARelPosZ * atrans.m12;
				rela_apz = o.axis2ARelPosX * atrans.m20 + o.axis2ARelPosY * atrans.m21 + o.axis2ARelPosZ * atrans.m22;
				
				relb_apx = o.axis2BRelPosX * btrans.m00 + o.axis2BRelPosY * btrans.m01 + o.axis2BRelPosZ * btrans.m02;
				relb_apy = o.axis2BRelPosX * btrans.m10 + o.axis2BRelPosY * btrans.m11 + o.axis2BRelPosZ * btrans.m12;
				relb_apz = o.axis2BRelPosX * btrans.m20 + o.axis2BRelPosY * btrans.m21 + o.axis2BRelPosZ * btrans.m22;
				
				_addContact(cmodel, o,
						rela_apx, rela_apy, rela_apz,
						relb_apx, relb_apy, relb_apz);
			}
		}
	}// end generateForce()
	
	
	void _addContact(ContactModel cmodel, Order o,
			double rela_apx, double rela_apy, double rela_apz,
			double relb_apx, double relb_apy, double relb_apz){
		double relvelx, relvely, relvelz,
				relacclx, relaccly, relacclz;
		// ap_norm : axis point - normal
		double val1x, val1y, val1z;
		double jinvlwx, jinvlwy, jinvlwz;
		double jlwrx, jlwry, jlwrz;
		double tmp1, tmp2, tmp3;
		double ndotx, ndoty, ndotz;
		ContactModel.Contact contact;
		double relvel1, relaccl1,
				relvel2, relaccl2,
				relvel3, relaccl3;
		
		
		// relative velocity 계산
		{
			relvelx = (avel.x + (aangvel.y * rela_apz - aangvel.z * rela_apy)) - (bvel.x + (bangvel.y * relb_apz - bangvel.z * relb_apy));
			relvely = (avel.y - (aangvel.x * rela_apz - aangvel.z * rela_apx)) - (bvel.y - (bangvel.x * relb_apz - bangvel.z * relb_apx));
			relvelz = (avel.z + (aangvel.x * rela_apy - aangvel.y * rela_apx)) - (bvel.z + (bangvel.x * relb_apy - bangvel.y * relb_apx));
		}
		if(BugManager.DEBUG && World.W_DEBUG && World.CONTACT_DEBUG)
			BugManager.printf("  relvel : %g %g %g\n", relvelx, relvely, relvelz);
		// relative acceleration
		{
			relacclx = (aaccl.x + (aangaccl.y * rela_apz - aangaccl.z * rela_apy)) - (baccl.x + (bangaccl.y * relb_apz - bangaccl.z * relb_apy));
			relaccly = (aaccl.y - (aangaccl.x * rela_apz - aangaccl.z * rela_apx)) - (baccl.y - (bangaccl.x * relb_apz - bangaccl.z * relb_apx));
			relacclz = (aaccl.z + (aangaccl.x * rela_apy - aangaccl.y * rela_apx)) - (baccl.z + (bangaccl.x * relb_apy - bangaccl.y * relb_apx));

			if(BugManager.DEBUG && World.W_DEBUG && World.CONTACT_DEBUG)
				BugManager.printf("  relaccl : %g %g %g\n", relacclx, relaccly, relacclz);
			/*
			// aid : fixed, bid : hang
			
			// page 271 참조.
			// 먼저 e를 계산.
			// A X (B X C) = B(A dot C) - C(A dot B)식으로 변환,
			// A dot C => tmp1, A dot B => tmp2에 넣어둔다.
			// val1 : l cross w_a
			val1x =   aangmom.y * aangvel.z - aangmom.z * aangvel.y;
			val1y = -(aangmom.x * aangvel.z - aangmom.z * aangvel.x);
			val1z =   aangmom.x * aangvel.y - aangmom.y * aangvel.x;
			jinvlwx = ainrtInv.m00 * val1x + ainrtInv.m01 * val1y + ainrtInv.m02 * val1z;
			jinvlwy = ainrtInv.m10 * val1x + ainrtInv.m11 * val1y + ainrtInv.m12 * val1z;
			jinvlwz = ainrtInv.m20 * val1x + ainrtInv.m21 * val1y + ainrtInv.m22 * val1z;
			// J^-1 (L cross w) cross r_a
			jlwrx =   jinvlwy * rela_apz - jinvlwz * rela_apy;
			jlwry = -(jinvlwx * rela_apz - jinvlwz * rela_apx);
			jlwrz =   jinvlwx * rela_apy - jinvlwy * rela_apx;
			
			// w dot r
			tmp1 = aangvel.x * rela_apx + aangvel.y * rela_apy + aangvel.z * rela_apz;
			// w^2
			tmp2 = aangvel.x * aangvel.x 
					+ aangvel.y * aangvel.y
					+ aangvel.z * aangvel.z;
			
			relacclx += (jlwrx + aangvel.x * tmp1 - rela_apx * tmp2);
			relaccly += (jlwry + aangvel.y * tmp1 - rela_apy * tmp2);
			relacclz += (jlwrz + aangvel.z * tmp1 - rela_apz * tmp2);

			val1x = val1y = val1z = tmp1 = tmp2 = tmp3 = 0;

			val1x =   bangmom.y * bangvel.z - bangmom.z * bangvel.y;
			val1y = -(bangmom.x * bangvel.z - bangmom.z * bangvel.x);
			val1z =   bangmom.x * bangvel.y - bangmom.y * bangvel.x;
			jinvlwx = binrtInv.m00 * val1x + binrtInv.m01 * val1y + binrtInv.m02 * val1z;
			jinvlwy = binrtInv.m10 * val1x + binrtInv.m11 * val1y + binrtInv.m12 * val1z;
			jinvlwz = binrtInv.m20 * val1x + binrtInv.m21 * val1y + binrtInv.m22 * val1z;
			jlwrx =   jinvlwy * relb_apz - jinvlwz * relb_apy;
			jlwry = -(jinvlwx * relb_apz - jinvlwz * relb_apx);
			jlwrz =   jinvlwx * relb_apy - jinvlwy * relb_apx;
			tmp1 = bangvel.x * relb_apx + bangvel.y * relb_apy + bangvel.z * relb_apz;
			tmp2 = bangvel.x * bangvel.x 
					+ bangvel.y * bangvel.y
					+ bangvel.z * bangvel.z;

			relacclx -= (jlwrx + bangvel.x * tmp1 - relb_apx * tmp2);
			relaccly -= (jlwry + bangvel.y * tmp1 - relb_apy * tmp2);
			relacclz -= (jlwrz + bangvel.z * tmp1 - relb_apz * tmp2);
			
			val1x = val1y = val1z = tmp1 = tmp2 = tmp3 = 0;*/
		}
		

		// ndot = w cross n
		//ndotx =   bangvel.y * ap_norm1.z - bangvel.z * ap_norm1.y;
		//ndoty = -(bangvel.x * ap_norm1.z - bangvel.z * ap_norm1.x);
		//ndotz =   bangvel.x * ap_norm1.y - bangvel.y * ap_norm1.x;
		relvel1 = ap_norm1.x * relvelx + ap_norm1.y * relvely + ap_norm1.z * relvelz;
		relaccl1 = ap_norm1.x * relacclx + ap_norm1.y * relaccly + ap_norm1.z * relacclz;
		//			+ 2.0 * (ndotx * relvelx + ndoty * relvely + ndotz * relvelz);
		relvel2 = ap_norm2.x * relvelx + ap_norm2.y * relvely + ap_norm2.z * relvelz;
		relaccl2 = ap_norm2.x * relacclx + ap_norm2.y * relaccly + ap_norm2.z * relacclz;
		//			+ 2.0 * (ndotx * relvelx + ndoty * relvely + ndotz * relvelz);
		relvel3 = ap_norm3.x * relvelx + ap_norm3.y * relvely + ap_norm3.z * relvelz;
		relaccl3 = ap_norm3.x * relacclx + ap_norm3.y * relaccly + ap_norm3.z * relacclz;
		//			+ 2.0 * (ndotx * relvelx + ndoty * relvely + ndotz * relvelz);
		int type = ContactModel.Contact.TYPE_BOTH;
		
		
		
	
		// ap_norm1
		{
			//if(Math.abs(relvel) > World.VARIANT_EPSILON || Math.abs(relaccl) > World.VARIANT_EPSILON){
				if(BugManager.DEBUG && World.W_DEBUG && World.CONTACT_DEBUG)
					BugManager.printf("AxisHolder : norm1(%g, %g, %g) works\n", ap_norm1.x, ap_norm1.y, ap_norm1.z);
				contact = cmodel.append();
				
				contact.type = type;
				contact.aid = o.aobjIdx;
				contact.bid = o.bobjIdx;
				contact.apx = rela_apx;
				contact.apy = rela_apy;
				contact.apz = rela_apz;
				contact.bpx = relb_apx;
				contact.bpy = relb_apy;
				contact.bpz = relb_apz;
				contact.coefOfRestt = 0.0; // 반드시
				contact.nx = ap_norm1.x;
				contact.ny = ap_norm1.y;
				contact.nz = ap_norm1.z;
				contact.onEdge = false;
				contact.currRelVelX = relvelx;
				contact.currRelVelY = relvely;
				contact.currRelVelZ = relvelz;
				contact.currDDot = relvel1;
				contact.currDDotDot = relaccl1;
			//}
		}

		// ap_norm2
		{
			//if(Math.abs(relvel) > World.VARIANT_EPSILON || Math.abs(relaccl) > World.VARIANT_EPSILON){
				if(BugManager.DEBUG && World.W_DEBUG && World.CONTACT_DEBUG)
					BugManager.printf("AxisHolder : norm2(%g, %g, %g) works\n", ap_norm2.x, ap_norm2.y, ap_norm2.z);
				contact = cmodel.append();
				
				contact.type = type;
				contact.aid = o.aobjIdx;
				contact.bid = o.bobjIdx;
				contact.apx = rela_apx;
				contact.apy = rela_apy;
				contact.apz = rela_apz;
				contact.bpx = relb_apx;
				contact.bpy = relb_apy;
				contact.bpz = relb_apz;
				contact.coefOfRestt = 0.0; // 반드시
				contact.nx = ap_norm2.x;
				contact.ny = ap_norm2.y;
				contact.nz = ap_norm2.z;
				contact.onEdge = false;
				contact.currRelVelX = relvelx;
				contact.currRelVelY = relvely;
				contact.currRelVelZ = relvelz;
				contact.currDDot = relvel2;
				contact.currDDotDot = relaccl2;
			//}
		}

		// ap_norm3
		{
			//if(Math.abs(relvel) > World.VARIANT_EPSILON || Math.abs(relaccl) > World.VARIANT_EPSILON){
				if(BugManager.DEBUG && World.W_DEBUG && World.CONTACT_DEBUG)
					BugManager.printf("AxisHolder : norm3(%g, %g, %g) works\n\tap : %g %g %g, bp : %g %g %g\n", ap_norm3.x, ap_norm3.y, ap_norm3.z,
							rela_apx, rela_apy, rela_apz, relb_apx, relb_apy, relb_apz);
				contact = cmodel.append();
				
				contact.type = type;
				contact.aid = o.aobjIdx;
				contact.bid = o.bobjIdx;
				contact.apx = rela_apx;
				contact.apy = rela_apy;
				contact.apz = rela_apz;
				contact.bpx = relb_apx;
				contact.bpy = relb_apy;
				contact.bpz = relb_apz;
				contact.coefOfRestt = 0.0; // 반드시
				contact.nx = ap_norm3.x;
				contact.ny = ap_norm3.y;
				contact.nz = ap_norm3.z;
				contact.onEdge = false;
				contact.currRelVelX = relvelx;
				contact.currRelVelY = relvely;
				contact.currRelVelZ = relvelz;
				contact.currDDot = relvel3;
				contact.currDDotDot = relaccl3;
			//}
		}
	}
	
	
	public static class Order{
		RealizationMap.Key aobjKey;
		int aobjIdx;
		RealizationMap.Key bobjKey;
		int bobjIdx;
		
		double axis1AbsPosX, axis1AbsPosY, axis1AbsPosZ;
		double axis2AbsPosX, axis2AbsPosY, axis2AbsPosZ;

		double axis1ARelPosX, axis1ARelPosY, axis1ARelPosZ;
		double axis2ARelPosX, axis2ARelPosY, axis2ARelPosZ;
		double axis1BRelPosX, axis1BRelPosY, axis1BRelPosZ;
		double axis2BRelPosX, axis2BRelPosY, axis2BRelPosZ;
	}// ed class Order
}
