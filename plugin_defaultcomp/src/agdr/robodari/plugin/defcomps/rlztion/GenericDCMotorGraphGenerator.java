package agdr.robodari.plugin.defcomps.rlztion;

import javax.vecmath.*;

import agdr.library.math.MathUtility;
import agdr.robodari.comp.model.*;
import agdr.robodari.plugin.defcomps.*;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.rlztion.*;
import agdr.robodari.plugin.rme.store.RealizationCompStore;

public class GenericDCMotorGraphGenerator implements GraphGenerator{
	public final static GenericDCMotorGraphGenerator INSTANCE = new GenericDCMotorGraphGenerator();
	
	public int getGeneratorType()
	{ return GraphGenerator.SPECFC_OBJECT; }

	
	public boolean editable(RobotCompModel mainStructure,
			SkObject skObject, RealizationMap map, World world){
		if(skObject == null)
			return false;
		if(skObject.getRobotComponent() == null)
			return false;
		return skObject.getRobotComponent() instanceof GenericDCMotor;
	}// end edtiable
	
	public RealizationMap.Key generateGraph(SkObject skobj, Matrix4f trans, RealizationMap map, World world){
		GenericDCMotor model = (GenericDCMotor)skobj.getRobotComponent();
		Matrix4f orgtrans = new Matrix4f();
		MathUtility.fastMul(trans, skobj.getTransform(), orgtrans);

		
		AxisHolder axisHolder = null;
		AxisAdjustment axisAdj = null;
		TorqueGenerator tqGen = null;
		
		int cnt = map.getForceGeneratorCount();
		for(int i = 0; i < cnt; i++){
			ForceGenerator fg = map.getForceGenerator(i);
			if(fg instanceof AxisHolder)
				axisHolder = (AxisHolder)fg;
			else if(fg instanceof TorqueGenerator)
				tqGen = (TorqueGenerator)fg;
		}
		if(axisHolder == null){
			axisHolder = new AxisHolder();
			map.addForceGenerator(axisHolder);
		}
		if(tqGen == null){
			tqGen = new TorqueGenerator();
			map.addForceGenerator(tqGen);
		}
		
		cnt = map.getObjAdjustmentCount();
		for(int i = 0; i < cnt; i++){
			ObjectAdjustment oa = map.getObjectAdjustment(i);
			if(oa instanceof AxisAdjustment)
				axisAdj = (AxisAdjustment)oa;
		}
		if(axisAdj == null){
			axisAdj = new AxisAdjustment();
			map.addObjectAdjustment(axisAdj);
		}
		
		
		RealizationMap.Key bodyKey = map.addElement(skobj.getModel().getSubModel(0), orgtrans, skobj, tqGen, true);
		double absap1x, absap1y, absap1z, absap2x, absap2y, absap2z;

		if(model.getAttachedObject() != null){
			Matrix4f aotrans = new Matrix4f();
			SkObject aobj = model.getAttachedObject();
			aotrans.set(orgtrans);
			RealizationMap.Key aokey;
			//System.out.println("HS_311GGen : " + aoid + " : " + rcomp.getName() + " aobj");
			
			if(aobj.isGroup())
				aokey = RealizationCompStore.generateGraph(
						aobj.getIntegratedModel(),
						aobj, 
						aotrans, map, world);
			else
				aokey = RealizationCompStore.generateGraph(
						aobj.getModel(),
						aobj,
						aotrans, map, world);
			
			map.setParent(aobj, skobj);
			
			// axis위치 구하기..
			// 여기서 orgtrans는 cmp 조절을 하지 않은 변환임.
			tqGen.addOrder(model, model.getTorqueFunc(), bodyKey, aokey);
			
			Point3d axispos1 = new Point3d(), axispos2 = new Point3d();
			
			axispos1.x = 0;
			axispos1.y = 0;
			axispos1.z = 0;
			
			axispos2.x = 5;
			axispos2.y = 0;
			axispos2.z = 0;
			
			Matrix4d orgtr_d = new Matrix4d(orgtrans);

			absap1x = orgtr_d.m00 * axispos1.x + orgtr_d.m01 * axispos1.y + orgtr_d.m02 * axispos1.z + orgtr_d.m03;
			absap1y = orgtr_d.m10 * axispos1.x + orgtr_d.m11 * axispos1.y + orgtr_d.m12 * axispos1.z + orgtr_d.m13;
			absap1z = orgtr_d.m20 * axispos1.x + orgtr_d.m21 * axispos1.y + orgtr_d.m22 * axispos1.z + orgtr_d.m23;
			
			absap2x = orgtr_d.m00 * axispos2.x + orgtr_d.m01 * axispos2.y + orgtr_d.m02 * axispos2.z + orgtr_d.m03;
			absap2y = orgtr_d.m10 * axispos2.x + orgtr_d.m11 * axispos2.y + orgtr_d.m12 * axispos2.z + orgtr_d.m13;
			absap2z = orgtr_d.m20 * axispos2.x + orgtr_d.m21 * axispos2.y + orgtr_d.m22 * axispos2.z + orgtr_d.m23;
			
			axisHolder.add(bodyKey, aokey,
					absap1x, absap1y, absap1z,
					absap2x, absap2y, absap2z);
			axisAdj.add(AxisAdjustment.ADJTYPE_ATOB, bodyKey, aokey,
					absap1x, absap1y, absap1z,
					absap2x, absap2y, absap2z);
		}
		return bodyKey;
	}// end generateGraph
}
