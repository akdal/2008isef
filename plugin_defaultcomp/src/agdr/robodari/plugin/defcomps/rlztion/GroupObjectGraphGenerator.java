package agdr.robodari.plugin.defcomps.rlztion;

import java.util.*;
import javax.vecmath.*;

import agdr.robodari.comp.*;
import agdr.robodari.comp.model.RobotCompModel;
import agdr.robodari.edit.*;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.rlztion.*;
import agdr.robodari.plugin.rme.store.*;
import agdr.robodari.store.*;

public class GroupObjectGraphGenerator implements GraphGenerator{
	public final static GroupObjectGraphGenerator INSTANCE =
		new GroupObjectGraphGenerator();
	
	public int getGeneratorType ()
	{ return GraphGenerator.SPECFC_OBJECT; }
	
	public boolean editable (RobotCompModel structure,
			SkObject obj, RealizationMap map, World world)
	{ return obj != null && obj.isGroup(); }
	
	
	public RealizationMap.Key generateGraph (
			SkObject gobj,
			Matrix4f trans,
			RealizationMap map, World world){
		if(gobj.getChildCount() == 0){
			return null;
		}
		
		SkObject[] children = gobj.getChildren();
		RealizationMap.Key[] mrgngitms = new RealizationMap.Key[children.length];
		Matrix4f gobjtrans = new Matrix4f();
		
		if(trans != null)
			gobjtrans.set(trans);
		else
			gobjtrans.m00 = gobjtrans.m11 =
				gobjtrans.m22 = gobjtrans.m33 = 1;
		gobjtrans.mul(gobj.getTransform());

		
		for(int i = 0; i < children.length; i++){
			Matrix4f itrans = new Matrix4f();
			itrans.set(gobjtrans);
			
			if(children[i].isGroup())
				mrgngitms[i] = RealizationCompStore.generateGraph(
						children[i].getIntegratedModel(),
						children[i], 
						itrans,
						map, world);
			else
				mrgngitms[i] = RealizationCompStore.generateGraph(
					children[i].getModel(),
					children[i], 
					itrans,
					map, world);
			
			map.setParent(map.getOwner(mrgngitms[i]), gobj);
		}
		map.mergeElements(mrgngitms);
		return mrgngitms[0];
	}// end generateGraph
}
