package agdr.robodari.plugin.defcomps.rlztion;

import javax.vecmath.*;

import agdr.library.math.*;
import agdr.robodari.appl.*;
import agdr.robodari.comp.model.*;
import agdr.robodari.plugin.defcomps.*;
import agdr.robodari.plugin.rme.edit.SkObject;
import agdr.robodari.plugin.rme.rlztion.*;
import agdr.robodari.plugin.rme.store.RealizationCompStore;

public class HingeGraphGenerator implements GraphGenerator{
	public final static HingeGraphGenerator INSTANCE = new HingeGraphGenerator();
	
	public int getGeneratorType()
	{ return GraphGenerator.SPECFC_OBJECT; }
	
	/**
	 * 주어진 개체에 대한 편집기인지 여부를 반환합니다.
	 * @param mainStructure
	 * @param model
	 * @param skObject
	 * @return
	 */
	public boolean editable(RobotCompModel mainStructure,
			SkObject skObject, RealizationMap map, World world){
		if(skObject == null)
			return false;
		if(skObject.getRobotComponent() == null)
			return false;
		return skObject.getRobotComponent() instanceof Hinge;
	}// end edtiable

	public RealizationMap.Key generateGraph(
			SkObject skObj, Matrix4f trans,
			RealizationMap map, World world){
		BugManager.printf("HingeGraphGenerator : generateGraph\n");
		Hinge hinge = (Hinge)skObj.getRobotComponent();
		RobotCompModel rcmodel = skObj.getModel();
		
		Matrix4f orgtrans = new Matrix4f();
		MathUtility.fastMul(trans, skObj.getTransform(), orgtrans);
		
		// fixed obj..
		Matrix4f fixedtrans = new Matrix4f(orgtrans);
		SkObject fixedobj = hinge.getFixedObject();
		RealizationMap.Key fixedobjkey = null;
		
		if(fixedobj.isGroup())
			fixedobjkey = RealizationCompStore.generateGraph(
					fixedobj.getIntegratedModel(),
					fixedobj, 
					fixedtrans, map, world);
		else
			fixedobjkey = RealizationCompStore.generateGraph(
					fixedobj.getModel(),
					fixedobj,
					fixedtrans, map, world);
		// axis
		Matrix4f axistrans = new Matrix4f();
		RobotCompModel axismodel = rcmodel.getSubModel(2);
		MathUtility.fastMul(orgtrans, rcmodel.getTransform(axismodel), axistrans);
		// center mass point adjustment <- false : there's no physical data
		RealizationMap.Key axiskey = map.addElement(axismodel, axistrans, fixedobj, false);
		
		map.mergeElements(axiskey, fixedobjkey);
		
		// hanging obj..
		Matrix4f hangingtrans = new Matrix4f(orgtrans);
		SkObject hangingobj = hinge.getHangingObject();
		RealizationMap.Key hangingObjKey = null;
		
		if(hangingobj.isGroup())
			hangingObjKey = RealizationCompStore.generateGraph(
					hangingobj.getIntegratedModel(),
					hangingobj, 
					hangingtrans, map, world);
		else
			hangingObjKey = RealizationCompStore.generateGraph(
					hangingobj.getModel(),
					hangingobj,
					hangingtrans, map, world);
		
		map.setParent(hangingobj, map.getOwner(axiskey));
		
		// registering..
		int cnt = map.getForceGeneratorCount();
		AxisHolder axisHolder = null;
		AxisAdjustment axisAdj = null;
		for(int i = 0; i < cnt; i++){
			ForceGenerator fg = map.getForceGenerator(i);
			if(fg instanceof AxisHolder){
				axisHolder = (AxisHolder)fg;
				break;
			}
		}
		if(axisHolder == null){
			axisHolder = new AxisHolder();
			map.addForceGenerator(axisHolder);
		}
		cnt = map.getObjAdjustmentCount();
		for(int i = 0; i < cnt; i++){
			ObjectAdjustment oa = map.getObjectAdjustment(i);
			if(oa instanceof AxisAdjustment)
				axisAdj = (AxisAdjustment)oa;
		}
		if(axisAdj == null){
			axisAdj = new AxisAdjustment();
			map.addObjectAdjustment(axisAdj);
		}
		
		{
			// axis위치는 두 점으로 나타낸다
			// position of each point는, 무게중심을 adjust하지 않은 orgtrans좌표계를 M이라고 두었을 때,
			// axis 1 = M (hingegap / 2.0,  0, hinge height)
			// axis 2 = M (-hingegap / 2.0, 0, hinge height)
			
			Point3d axispos1 = new Point3d(), axispos2 = new Point3d();
			double absap1x, absap1y, absap1z, absap2x, absap2y, absap2z;
			
			axispos1.x = 0;
			axispos1.y = hinge.getHingeGap() * 2.0;
			axispos1.z = hinge.getHingeHeight();
			
			axispos2.x = 0;
			axispos2.y = -hinge.getHingeGap() * 2.0;
			axispos2.z = hinge.getHingeHeight();
			
			Matrix4d orgtr_d = new Matrix4d(orgtrans);

			absap1x = orgtr_d.m00 * axispos1.x + orgtr_d.m01 * axispos1.y + orgtr_d.m02 * axispos1.z + orgtr_d.m03;
			absap1y = orgtr_d.m10 * axispos1.x + orgtr_d.m11 * axispos1.y + orgtr_d.m12 * axispos1.z + orgtr_d.m13;
			absap1z = orgtr_d.m20 * axispos1.x + orgtr_d.m21 * axispos1.y + orgtr_d.m22 * axispos1.z + orgtr_d.m23;
			
			absap2x = orgtr_d.m00 * axispos2.x + orgtr_d.m01 * axispos2.y + orgtr_d.m02 * axispos2.z + orgtr_d.m03;
			absap2y = orgtr_d.m10 * axispos2.x + orgtr_d.m11 * axispos2.y + orgtr_d.m12 * axispos2.z + orgtr_d.m13;
			absap2z = orgtr_d.m20 * axispos2.x + orgtr_d.m21 * axispos2.y + orgtr_d.m22 * axispos2.z + orgtr_d.m23;
			
			
			axisHolder.add(axiskey, hangingObjKey,
					absap1x, absap1y, absap1z,
					absap2x, absap2y, absap2z);
			axisAdj.add(AxisAdjustment.ADJTYPE_BTOA, axiskey, hangingObjKey,
					absap1x, absap1y, absap1z,
					absap2x, absap2y, absap2z);
		}
		return axiskey;
	}// end generateGrah
}
