package agdr.robodari.plugin.defcomps.rlztion;

import javax.vecmath.Matrix4f;
import agdr.library.math.*;
import agdr.robodari.appl.*;
import agdr.robodari.comp.RobotComponent;
import agdr.robodari.comp.model.RobotCompModel;
import agdr.robodari.plugin.defcomps.*;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.rlztion.*;

public class OneItemGraphGenerator implements GraphGenerator{
	public final static OneItemGraphGenerator INSTANCE = 
		new OneItemGraphGenerator();
	
	public int getGeneratorType()
	{ return GraphGenerator.SPECFC_RCOMP_OR_OBJ; }
	public boolean editable (RobotCompModel str, SkObject skobj,
			RealizationMap smap, World world){
		if(skobj == null)
			return false;
		RobotComponent rc = skobj.getRobotComponent();
		return rc != null
		&& (rc instanceof ThreePin
				|| rc instanceof Sphere
				|| rc instanceof Transistor1
				|| rc instanceof RoundedRectanglePane
				|| rc instanceof PowerConnector1
				|| rc instanceof OnePin
				|| rc instanceof LED1
				|| rc instanceof ElectricPanel
				|| rc instanceof RectanglePane
				|| rc instanceof Stage
				|| rc instanceof CircularCylinder
				|| rc instanceof GenericSimplexAntenna
				|| rc instanceof GenericController);
	}// end editable

	public RealizationMap.Key generateGraph(
			SkObject skobj, Matrix4f orgtrans,
			RealizationMap smap, World world){
		Matrix4f trans = new Matrix4f();
		if(orgtrans != null){
			MathUtility.fastMul(orgtrans, skobj.getTransform(), trans);
		}else
			trans.set(orgtrans);
		return smap.addElement(skobj.getModel(), trans, skobj, true);
	}// end generateGraph(TemporaryGraph, int, RobotComponent,
	//                    StructureMap, World)
}
