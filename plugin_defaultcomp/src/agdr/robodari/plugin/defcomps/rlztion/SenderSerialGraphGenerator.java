package agdr.robodari.plugin.defcomps.rlztion;

import javax.vecmath.*;

import agdr.robodari.comp.*;
import agdr.robodari.comp.model.RobotCompModel;
import agdr.robodari.edit.*;
import agdr.robodari.plugin.defcomps.*;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.rlztion.*;

public class SenderSerialGraphGenerator implements GraphGenerator{
	public final static SenderSerialGraphGenerator INSTANCE
		 = new SenderSerialGraphGenerator();
	
	public int getGeneratorType()
	{ return GraphGenerator.SPECFC_RCOMP; }
	public boolean editable(RobotCompModel st, SkObject obj,
			RealizationMap smap, World world){
		if(obj == null)
			return false;
		RobotComponent rc = obj.getRobotComponent();
		return rc != null && rc instanceof SenderSerial && st != null;
	}// end editable(Structure, RobotComponent, SkObject, ..)
	
	public RealizationMap.Key generateGraph(
			SkObject owner,
			Matrix4f trans,
			RealizationMap smap, World world){
		RobotCompModel str = owner.getModel();
		
		if(str.getSubModelCount() == 0)
			return null;
		
		RobotCompModel str0 = str.getSubModel(0);
		Matrix4f trans0 = new Matrix4f(trans);
		trans0.mul(str.getTransform(str0));

		int len = str.getSubModelCount();
		RealizationMap.Key[] mrgngitms = new RealizationMap.Key[len];
		mrgngitms[0] = smap.addElement(str0, trans0, owner, true);
		
		int newidx = 0;
		
		for(int i = 1; i < len; i++){
			RobotCompModel stri = str.getSubModel(i);
			Matrix4f transi = new Matrix4f(trans);
			transi.mul(str.getTransform(stri));
			
			mrgngitms[i] = smap.addElement(stri, transi, owner, true);
		}
		smap.mergeElements(mrgngitms);
		return mrgngitms[0];
	}// end generateGraph(TemporaryGraph, int, StructureMap, World)
}
