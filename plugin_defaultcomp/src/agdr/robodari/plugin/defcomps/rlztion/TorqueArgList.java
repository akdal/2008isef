package agdr.robodari.plugin.defcomps.rlztion;

import agdr.library.math.*;

public class TorqueArgList implements ArgList{
	private final static String[] NAMES = new String[]{
		"relAngularVelocity",
		"direction"
	};
	public final static int DIR_CW = 1;
	public final static int DIR_STOPPED = 0;
	public final static int DIR_CCW = -1;
	
	public Double relAngularVelocity;
	public Integer direction;
	
	public int argCount()
	{ return 2; }
	public Object argValue(int idx){
		if(idx == 0)
			return this.relAngularVelocity;
		else if(idx == 1)
			return this.direction;
		return null;
	}// end argValue
	public String argName(int idx){
		if(idx >= NAMES.length)
			return null;
		return NAMES[idx];
	}// end argName(int)
}
