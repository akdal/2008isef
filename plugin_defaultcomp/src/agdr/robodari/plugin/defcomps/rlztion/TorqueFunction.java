package agdr.robodari.plugin.defcomps.rlztion;

import agdr.library.math.*;

public interface TorqueFunction extends Function<TorqueArgList, Double>, java.io.Serializable{
	public String[] propertyKeys();
	public Object propertyValue(String key);
	public boolean setProperty(String key, Object val);
}
