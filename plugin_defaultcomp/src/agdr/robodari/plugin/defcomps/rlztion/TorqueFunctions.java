package agdr.robodari.plugin.defcomps.rlztion;

import agdr.robodari.appl.*;

public class TorqueFunctions {
	public final static class ProportionalTorque implements TorqueFunction{
		private final static String[] KEYS = new String[]{
			"speed", "constant"
		};
		
		private double speed = (double)(Math.PI / 3.0);
		private double constant = 10f;
		
		public String[] propertyKeys()
		{ return KEYS; }
		public Object propertyValue(String key){
			if(KEYS[0].equals(key)){
				return new Double(speed / Math.PI * 180.0);
			}else if(KEYS[1].equals(key)){
				return new Double(constant);
			}
			return null;
		}// end propertyValue
		public boolean setProperty(String k, Object o){
			try{
				if(KEYS[0].equals(k)){
					double val = 0;
					if(o instanceof Double)
						val = (Double)o;
					else
						val = Double.parseDouble(o.toString());
					speed = val * Math.PI * 2.0 / 360.0;
					return true;
				}else if(KEYS[1].equals(k)){
					double val = 0;
					if(o instanceof Double)
						val = (Double)o;
					else
						val = Double.parseDouble(o.toString());
					constant = val;
					return true;
				}
			}catch(Exception e){
				BugManager.log(e, true);
			}
			return false;
		}// end setPropertYvalue
		
		public boolean domain(TorqueArgList tal)
		{ return true; }
		public boolean range(Double d)
		{ return true; }
		
		public Double f(TorqueArgList tal){
			if(tal.direction == TorqueArgList.DIR_STOPPED){
				return 0.0;
			}
			double val = tal.direction * constant * (speed - tal.relAngularVelocity);
			agdr.robodari.appl.BugManager.printf("TorqFunc : Prop : const : %g, dir : %d(cw : %d), val : %g\n", constant, tal.direction, TorqueArgList.DIR_CW, val);
			return val;
		}// end f(TorqueArgList)
	}// end class ExponentialTorque
	
	
	
	

	
	public final static class LinearTorque implements TorqueFunction{
		private final static String[] KEYS = new String[]{
		//	"maxspeed", "maxtorque"
		};
		
		private double maxspeed;
		private double maxtorque;
		
		public LinearTorque(){
			maxspeed = Math.PI;
			maxtorque = 2;
		}// end Constructor
		
		public String[] propertyKeys()
		{ return KEYS; }
		public Object propertyValue(String key){
			/*if(KEYS[0].equals(key)){
				return new Double(maxspeed / Math.PI * 180.0);
			}else if(KEYS[1].equals(key)){
				return new Double(maxtorque);
			}*/
			return null;
		}// end propertyValue
		public boolean setProperty(String k, Object o){
			try{
				/*if(KEYS[0].equals(k)){
					double val = 0;
					if(o instanceof Double)
						val = (Double)o;
					else
						val = Double.parseDouble(o.toString());
					maxspeed = val * Math.PI * 2.0 / 360.0;
					return true;
				}else if(KEYS[1].equals(k)){
					double val = 0;
					if(o instanceof Double)
						val = (Double)o;
					else
						val = Double.parseDouble(o.toString());
					maxtorque = val;
					return true;
				}*/
			}catch(Exception e){
				BugManager.log(e, true);
			}
			return false;
		}// end setPropertYvalue
		
		public boolean domain(TorqueArgList tal)
		{ return true; }
		public boolean range(Double d)
		{ return true; }
		
		public Double f(TorqueArgList tal){
			double val = 0;
			maxspeed = 3 * Math.PI;
			maxtorque = 15;
			
			if(tal.direction == TorqueArgList.DIR_CW)
				val = -maxtorque / maxspeed * tal.relAngularVelocity + maxtorque;
			else if(tal.direction == TorqueArgList.DIR_CCW)
				val = -maxtorque / maxspeed * tal.relAngularVelocity - maxtorque;
			else
				val = -maxtorque / maxspeed * tal.relAngularVelocity;
			
			if(val > maxtorque)
				val = maxtorque;
			else if(val < -maxtorque)
				val = -maxtorque;
			return val;
		}// end f(TorqueArgList)
	}// end class LinearTorque
}
