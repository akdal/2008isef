package agdr.robodari.plugin.defcomps.rlztion;

import java.util.*;
import javax.vecmath.*;

import agdr.robodari.appl.*;
import agdr.robodari.plugin.defcomps.*;
import agdr.robodari.plugin.rme.rdpi.*;
import agdr.robodari.plugin.rme.rlztion.*;

/**
 * Class for GenericDCMotor
 * @author LeeJuneYoung
 *
 */

public class TorqueGenerator implements ForceGenerator, MergingObserver{
	private Hashtable<GenericDCMotor, Order> orders;
	private World world;
	private RealizationModel model;
	
	public TorqueGenerator(){
		orders = new Hashtable<GenericDCMotor, Order>();
	}// end Constructor
	
	public void addOrder(GenericDCMotor motor, TorqueFunction torque, RealizationMap.Key bodyKey, RealizationMap.Key motorKey){
		Order o = new Order();
		o.bodyKey = bodyKey;
		o.motorKey = motorKey;
		o.torqueFunc = torque;
		o.dirx = GenericDCMotor.AXIS_X;
		o.diry = GenericDCMotor.AXIS_Y;
		o.dirz = GenericDCMotor.AXIS_Z;
		orders.put(motor, o);
	}// end addOrder
	public void clear()
	{ orders.clear(); }
	
	
	public int getGeneratorType()
	{ return ForceGenerator.DEPENDSON_VELOCITY | ForceGenerator.DEPENDSON_SITUATION; }
	public boolean effective(RealizationModel model, RealizationMap rmap, World world)
	{ return orders.size() != 0; }
	public void init(RealizationModel model, RealizationMap rmap, World world){
		this.world = world;
		this.model = model;
		
		Enumeration<GenericDCMotor> motors = orders.keys();
		while(motors.hasMoreElements()){
			GenericDCMotor key = motors.nextElement();
			
			Order o = orders.get(key);
			o.bodyid = rmap.getIndex(o.bodyKey);
			o.motorid = rmap.getIndex(o.motorKey);
		}
	}// end init
	public void merged(RealizationMap.Key key, Matrix4d transf){
		Enumeration<GenericDCMotor> keys = orders.keys();
		Order o = null;
		GenericDCMotor motor = null;
		
		for(o = null; keys.hasMoreElements();){
			motor = keys.nextElement();
			o = orders.get(motor);
			
			if(o.bodyKey != key) continue;

			BugManager.printf("TqGen : merged : bef : o.dir : %g %g %g\n", o.dirx, o.diry, o.dirz);
			double newdirx = o.dirx * transf.m00 + o.diry * transf.m01 + o.dirz * transf.m02;
			double newdiry = o.dirx * transf.m10 + o.diry * transf.m11 + o.dirz * transf.m12;
			double newdirz = o.dirx * transf.m20 + o.diry * transf.m21 + o.dirz * transf.m22;
			o.dirx = newdirx;
			o.diry = newdiry;
			o.dirz = newdirz;
			break;
		}
		orders.put(motor, o);
		BugManager.printf("TqGen : merged : o.dir : %g %g %g\n", o.dirx, o.diry, o.dirz);
	}// end merged
	
	Vector3d bodyangvel = new Vector3d(),
			motorangvel = new Vector3d();
	Vector3d torque = new Vector3d();
	Matrix3d bodyrot = new Matrix3d();
	
	
	public void generateForce(){
		Enumeration<GenericDCMotor> keys = orders.keys();
		GenericDCMotor eachMotor;
		Order order;
		TorqueArgList talist = new TorqueArgList();
		double dirx, diry, dirz;
		double relavelx, relavely, relavelz;

		try{
			while(keys.hasMoreElements()){
				eachMotor = keys.nextElement();
				order = orders.get(eachMotor);
				
				// world에서  body와 motor간의 각속도차이 비교
				world.getAngularVelocity_bef(order.bodyid, bodyangvel);
				world.getAngularVelocity_bef(order.motorid, motorangvel);
				world.getRotMatrix(order.bodyid, bodyrot);
				
				dirx = order.dirx * bodyrot.m00 + order.diry * bodyrot.m01 + order.dirz * bodyrot.m02;
				diry = order.dirx * bodyrot.m10 + order.diry * bodyrot.m11 + order.dirz * bodyrot.m12;
				dirz = order.dirx * bodyrot.m20 + order.diry * bodyrot.m21 + order.dirz * bodyrot.m22;

				relavelx = motorangvel.x - bodyangvel.x;
				relavely = motorangvel.y - bodyangvel.y;
				relavelz = motorangvel.z - bodyangvel.z;
				
				talist.direction = (Integer)eachMotor.callRDMethod(this.model, "getDir", null);
				if(talist.direction == null)
					talist.direction = new Integer(TorqueArgList.DIR_STOPPED);
				talist.relAngularVelocity = relavelx * dirx + relavely * diry + relavelz * dirz;
				
				double tq = order.torqueFunc.f(talist);
				
				world.getTorque(order.motorid, torque);
				torque.x += tq * dirx;
				torque.y += tq * diry;
				torque.z += tq * dirz;
				world.setTorque(order.motorid, torque);
				world.getTorque(order.bodyid, torque);
				torque.x -= tq * dirx;
				torque.y -= tq * diry;
				torque.z -= tq * dirz;
				world.setTorque(order.bodyid, torque);
			}
		}catch(RealizationNotReadyException rnre){
		}catch(RDMethodNotExistException rnee){
			BugManager.log(rnee, true);
		}
	}// end generateForce()
	
	
	
	static class Order{
		TorqueFunction torqueFunc;
		RealizationMap.Key bodyKey, motorKey;
		double dirx, diry, dirz;
		int bodyid, motorid;
	}// end class Order
}
