package agdr.robodari.plugin.miniROBOT.comp;


import agdr.robodari.comp.*;

public class ATMEL_AT89C4051 implements CPU{
	private RobotComponent parent;
	private Note note = new Note();
	private String name;
	
	
	public RobotComponent getParent()
	{ return parent; }
	public void setParent(RobotComponent p)
	{ this.parent = p; }
	public Note getNote()
	{ return note; }
	public void setNote(Note note)
	{ this.note = note; }
	
	public String getName()
	{ return name; }
	public void setName(String name)
	{ this.name = name; }
	
	public boolean hasSubComponent()
	{ return false; }
	public boolean hasReceiver()
	{ return false; }
	public boolean hasSender()
	{ return false; }
	public RobotComponent[] getSubComponents()
	{ return null; }
	public Receiver[] getReceivers()
	{ return null; }
	public Sender[] getSenders()
	{ return null; }
	
	public boolean suppliesPower()
	{ return false; }
	public PowerSupply getPowerSupply()
	{ return null; }
}
