package agdr.robodari.plugin.miniROBOT.comp;

import java.awt.*;
import javax.swing.*;

import agdr.robodari.comp.*;

public class Computer_MRCConnector implements RobotComponent, Receiver{
	public final static String CONDITION = "컴퓨터와 연결되는 부분입니다." +
			"(현재 연결 가능한 부품이 리스트에 없음)";
	public final static Icon ICON = new Computer_MRCConnectorIcon();
	
	
	private Note note;
	private String name;
	private MR_C2000S parent;
	
	
	public Note getNote()
	{ return note; }
	public void setNote(Note note)
	{ this.note = note; }
	public String getName()
	{ return name; }
	public void setName(String name)
	{ this.name = name; }
	
	public Icon getSimpleIcon()
	{ return ICON; }
	
	public boolean isCompatible(Sender sd)
	{ return sd == null; }
	public boolean isEnabled()
	{ return true; }
	public String getCondition()
	{ return CONDITION; }

	public void setParent(RobotComponent rc){}
	public MR_C2000S getParent()
	{ return parent; }
	public void setParent(MR_C2000S p)
	{ this.parent = p; }

	public boolean hasSubComponent()
	{ return false; }
	public boolean hasReceiver()
	{ return false; }
	public boolean hasSender()
	{ return false; }
	public RobotComponent[] getSubComponents()
	{ return null; }
	public Receiver[] getReceivers()
	{ return null; }
	public Sender[] getSenders()
	{ return null; }
	
	public boolean suppliesPower()
	{ return false; }
	public PowerSupply getPowerSupply()
	{ return null; }
	
	
	static class Computer_MRCConnectorIcon implements Icon{
		private final static byte[] data = new byte[]
		    {71, 73, 70, 56, 57, 97, 25, 0, 21, 0, -9, 0, 0, 0, 0, 0, -128
			, 0, 0, 0, -128, 0, -128, -128, 0, 0, 0, -128, -128, 0, -128, 0, -128
			, -128, -128, -128, -128, -64, -64, -64, -1, 0, 0, 0, -1, 0, -1, -1, 0
			, 0, 0, -1, -1, 0, -1, 0, -1, -1, -1, -1, -1, 0, 0, 0, 0
			, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
			, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
			, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
			, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
			, 0, 0, 0, 0, 0, 0, 0, 0, 0, 51, 0, 0, 102, 0, 0, -103
			, 0, 0, -52, 0, 0, -1, 0, 51, 0, 0, 51, 51, 0, 51, 102, 0
			, 51, -103, 0, 51, -52, 0, 51, -1, 0, 102, 0, 0, 102, 51, 0, 102
			, 102, 0, 102, -103, 0, 102, -52, 0, 102, -1, 0, -103, 0, 0, -103, 51
			, 0, -103, 102, 0, -103, -103, 0, -103, -52, 0, -103, -1, 0, -52, 0, 0
			, -52, 51, 0, -52, 102, 0, -52, -103, 0, -52, -52, 0, -52, -1, 0, -1
			, 0, 0, -1, 51, 0, -1, 102, 0, -1, -103, 0, -1, -52, 0, -1, -1
			, 51, 0, 0, 51, 0, 51, 51, 0, 102, 51, 0, -103, 51, 0, -52, 51
			, 0, -1, 51, 51, 0, 51, 51, 51, 51, 51, 102, 51, 51, -103, 51, 51
			, -52, 51, 51, -1, 51, 102, 0, 51, 102, 51, 51, 102, 102, 51, 102, -103
			, 51, 102, -52, 51, 102, -1, 51, -103, 0, 51, -103, 51, 51, -103, 102, 51
			, -103, -103, 51, -103, -52, 51, -103, -1, 51, -52, 0, 51, -52, 51, 51, -52
			, 102, 51, -52, -103, 51, -52, -52, 51, -52, -1, 51, -1, 0, 51, -1, 51
			, 51, -1, 102, 51, -1, -103, 51, -1, -52, 51, -1, -1, 102, 0, 0, 102
			, 0, 51, 102, 0, 102, 102, 0, -103, 102, 0, -52, 102, 0, -1, 102, 51
			, 0, 102, 51, 51, 102, 51, 102, 102, 51, -103, 102, 51, -52, 102, 51, -1
			, 102, 102, 0, 102, 102, 51, 102, 102, 102, 102, 102, -103, 102, 102, -52, 102
			, 102, -1, 102, -103, 0, 102, -103, 51, 102, -103, 102, 102, -103, -103, 102, -103
			, -52, 102, -103, -1, 102, -52, 0, 102, -52, 51, 102, -52, 102, 102, -52, -103
			, 102, -52, -52, 102, -52, -1, 102, -1, 0, 102, -1, 51, 102, -1, 102, 102
			, -1, -103, 102, -1, -52, 102, -1, -1, -103, 0, 0, -103, 0, 51, -103, 0
			, 102, -103, 0, -103, -103, 0, -52, -103, 0, -1, -103, 51, 0, -103, 51, 51
			, -103, 51, 102, -103, 51, -103, -103, 51, -52, -103, 51, -1, -103, 102, 0, -103
			, 102, 51, -103, 102, 102, -103, 102, -103, -103, 102, -52, -103, 102, -1, -103, -103
			, 0, -103, -103, 51, -103, -103, 102, -103, -103, -103, -103, -103, -52, -103, -103, -1
			, -103, -52, 0, -103, -52, 51, -103, -52, 102, -103, -52, -103, -103, -52, -52, -103
			, -52, -1, -103, -1, 0, -103, -1, 51, -103, -1, 102, -103, -1, -103, -103, -1
			, -52, -103, -1, -1, -52, 0, 0, -52, 0, 51, -52, 0, 102, -52, 0, -103
			, -52, 0, -52, -52, 0, -1, -52, 51, 0, -52, 51, 51, -52, 51, 102, -52
			, 51, -103, -52, 51, -52, -52, 51, -1, -52, 102, 0, -52, 102, 51, -52, 102
			, 102, -52, 102, -103, -52, 102, -52, -52, 102, -1, -52, -103, 0, -52, -103, 51
			, -52, -103, 102, -52, -103, -103, -52, -103, -52, -52, -103, -1, -52, -52, 0, -52
			, -52, 51, -52, -52, 102, -52, -52, -103, -52, -52, -52, -52, -52, -1, -52, -1
			, 0, -52, -1, 51, -52, -1, 102, -52, -1, -103, -52, -1, -52, -52, -1, -1
			, -1, 0, 0, -1, 0, 51, -1, 0, 102, -1, 0, -103, -1, 0, -52, -1
			, 0, -1, -1, 51, 0, -1, 51, 51, -1, 51, 102, -1, 51, -103, -1, 51
			, -52, -1, 51, -1, -1, 102, 0, -1, 102, 51, -1, 102, 102, -1, 102, -103
			, -1, 102, -52, -1, 102, -1, -1, -103, 0, -1, -103, 51, -1, -103, 102, -1
			, -103, -103, -1, -103, -52, -1, -103, -1, -1, -52, 0, -1, -52, 51, -1, -52
			, 102, -1, -52, -103, -1, -52, -52, -1, -52, -1, -1, -1, 0, -1, -1, 51
			, -1, -1, 102, -1, -1, -103, -1, -1, -52, -1, -1, -1, 44, 0, 0, 0
			, 0, 25, 0, 21, 0, 0, 8, 115, 0, 31, 8, 28, 72, -80, -96, -63
			, -126, 0, 18, 42, 92, -56, 80, -31, 65, 0, 7, 35, 62, -128, 104, -112
			, -94, 68, -124, 15, 9, 54, 76, -88, 49, -93, 64, -117, 24, 63, 50, -4
			, 56, 16, 100, -57, -113, 8, 82, 34, -96, 104, -47, 100, -55, -110, 42, 87
			, -110, -100, 121, 17, 64, 76, -106, 47, 47, 78, -68, 73, -45, 101, 69, -98
			, 19, 115, -42, 4, -38, 82, -25, 78, -107, 56, 105, -122, 60, -102, 50, 105
			, 80, -113, 76, 101, 62, -99, -70, -76, -95, -46, -115, 32, 125, -6, -108, -88
			, -43, -24, 73, -124, 88, -61, 122, 29, 75, 48, 32, 0, 59};
		private final static Image img = Toolkit.
				getDefaultToolkit().createImage(data);
		private final static Component c = new Component(){};
		
		public Computer_MRCConnectorIcon(){
			MediaTracker mt = new MediaTracker(c);
			mt.addImage(img, 0);
			try{
			mt.waitForID(0);
			}catch(Exception ie){ ie.printStackTrace(); }
		}// end Constructor()
		
		public int getIconWidth()
		{ return 25; }
		public int getIconHeight()
		{ return 21; }
		
		public void paintIcon(Component c, Graphics g, int x, int y){
			g.drawImage(img, x, y, c);
		}// end paintIcon(Component, Graphics, int, int)
	}// end class Computer_MRCConnectorIcon
}
