package agdr.robodari.plugin.miniROBOT.comp;

import java.io.*;

import javax.vecmath.*;


import agdr.library.math.MathUtility;
import agdr.robodari.comp.model.AppearanceData;
import agdr.robodari.comp.model.ModelGenerator;
import agdr.robodari.comp.model.PhysicalData;
import agdr.robodari.comp.model.RobotCompModel;
import agdr.robodari.plugin.rme.rlztion.*;

public class Computer_MRCConnectorModelGenerator
		implements ModelGenerator<Computer_MRCConnector>{
	private StringBuffer buf;
			
	public Computer_MRCConnectorModelGenerator(){
		buf = new StringBuffer();
		buf
			.append("v 0.75 0.9 0.15\n")
			.append("v 0.7252422 0.9 0.25847092\n")
			.append("v 0.65587246 0.9 0.34545788\n")
			.append("v 0.5556302 0.9 0.39373198\n")
			.append("v 0.44436976 0.9 0.39373198\n")
			.append("v 0.34412754 0.9 0.34545788\n")
			.append("v 0.27475777 0.9 0.25847092\n")
			.append("v 0.25 0.9 0.15\n")
			.append("v 0.27475777 0.9 0.041529067\n")
			.append("v 0.34412754 0.9 -0.04545787\n")
			.append("v 0.44436976 0.9 -0.09373198\n")
			.append("v 0.5556302 0.9 -0.09373198\n")
			.append("v 0.65587246 0.9 -0.04545787\n")
			.append("v 0.7252422 0.9 0.041529067\n")
			
			.append("v 0.75 1.1 0.15\n")
			.append("v 0.7252422 1.1 0.25847092\n")
			.append("v 0.65587246 1.1 0.34545788\n")
			.append("v 0.5556302 1.1 0.39373198\n")
			.append("v 0.44436976 1.1 0.39373198\n")
			.append("v 0.34412754 1.1 0.34545788\n")
			.append("v 0.27475777 1.1 0.25847092\n")
			.append("v 0.25 1.1 0.15\n")
			.append("v 0.27475777 1.1 0.041529067\n")
			.append("v 0.34412754 1.1 -0.04545787\n")
			.append("v 0.44436976 1.1 -0.09373198\n")
			.append("v 0.5556302 1.1 -0.09373198\n")
			.append("v 0.65587246 1.1 -0.04545787\n")
			.append("v 0.7252422 1.1 0.041529067\n")
			
			.append("v 0 0.9 0.4\n")
			.append("v 0 0 0.4\n")
			.append("v 1.0 0 0.4\n")
			.append("v 1.0 0.9 0.4\n")
			.append("v 0 0.9 0\n")
			.append("v 0 0 0\n")
			.append("v 1.0 0 0\n")
			.append("v 1.0 0.9 0\n")
			
			
			.append("f 1 2 3 4 5 6 7 8 9 10 11 12\n")
			.append("f 28 27 26 25 24 23 22 21 20 19 18 17 16 15\n")
			.append("f 1 15 16 2\n")
			.append("f 2 16 17 3\n")
			.append("f 3 17 18 4\n")
			.append("f 4 18 19 5\n")
			.append("f 5 19 20 6\n")
			.append("f 6 20 21 7\n")
			.append("f 7 21 22 8\n")
			.append("f 8 22 23 9\n")
			.append("f 9 23 24 10\n")
			.append("f 10 24 25 11\n")
			.append("f 11 25 26 12\n")
			.append("f 12 26 27 13\n")
			.append("f 13 27 28 14\n")
			.append("f 14 28 15 1\n")
			
			.append("f 29 30 31 32\n")
			.append("f 33 36 35 34\n")
			.append("f 30 34 35 31\n")
			.append("f 31 35 36 32\n")
			.append("f 29 32 36 33\n")
			.append("f 30 29 33 34\n");
	}// end Constructor()
			
			
	public RobotCompModel generate(Computer_MRCConnector model) throws Exception{
		RobotCompModel str = new RobotCompModel();
		str.load(new StringReader(buf.toString()));

		PhysicalData pd = str.getPhysicalData();
		Point3f[] ps = str.getPoints();
		int[][] faces = str.getFaces();
		pd.mass = 1.1662f;
		pd.volume = MathUtility.getVolume(ps, faces);
		pd.bounds = null;
		pd.centerMassPoint = MathUtility.getCenterMassPoint(ps, faces, pd.centerMassPoint);
		pd.inertia = new Matrix3f();
		MathUtility.addInertiaTensor(ps, str.getFaces(),
				pd.mass, pd.centerMassPoint.x, pd.centerMassPoint.y,
				pd.centerMassPoint.z, pd.inertia);

		AppearanceData ap = str.getApprData();
		ap.setColor(java.awt.Color.black);
		
		return str;
	}// end generate(Computer_MRCConnector)
}