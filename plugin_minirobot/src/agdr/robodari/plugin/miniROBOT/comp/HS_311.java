package agdr.robodari.plugin.miniROBOT.comp;


import java.util.*;
import javax.vecmath.*;

import agdr.robodari.comp.*;
import agdr.robodari.edit.*;
import agdr.robodari.plugin.defcomps.*;
import agdr.robodari.plugin.miniROBOT.rlztion.*;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.rdpi.*;
import agdr.robodari.plugin.rme.rlztion.*;
import agdr.robodari.plugin.rme.store.*;

public class HS_311 implements ServoMotor, RDObject{
	public final static Vector3d ROT_AXIS = new Vector3d(0, 0, 1);
	public final static Point3d ROT_AXISP = new Point3d(4.2, 1, 0);
	public final static Vector3d ROT_AXIS_AT_DISK = new Vector3d(0, 0, 1);
	public final static Point3d ROT_AXISP_AT_DISK = new Point3d(0, 0, 0);
	public final static Vector3d ROT_AXIS_AT_BODY =
		new Vector3d(0, 0, -1);
	public final static Point3d ROT_AXISP_AT_BODY = 
		new Point3d(/*4.2 - 2.6213458*/0.35, /*1.0000001*/0, 4.0 - 3.5041668);
	
	private final static RobotComponent[] NULLRCARRAY = new RobotComponent[]{};
	
	
	
	private Receiver[] receivers = new Receiver[]{
			new HS_311Receiver(this)
	};
	private SkObject attachedObject;
	private ArrayList<RobotComponent> subComponents;
	private Note note;
	private String name;
	private RobotComponent parent;
	
	
	public HS_311(){
		note = new Note();
		subComponents = new ArrayList<RobotComponent>();
	}// end Constructor()

	
	public String getName()
	{ return name; }
	public void setName(String name)
	{ this.name = name; }
	
	public SkObject getAttachedObject()
	{ return attachedObject; }
	public void setAttachedObject(SkObject obj){
		this.attachedObject = obj;
		subComponents.clear();
		System.out.println("HS_311 : setAttachedObject : " + obj);
		
		if(attachedObject != null){
			if(attachedObject.isGroup())
				Collections.addAll(subComponents,
						attachedObject.getIntegratedRobotComponents());
			else
				subComponents.add(attachedObject.getRobotComponent());
		}
	}// end setAttachedObject(SkObject)
	
	
	public Note getNote()
	{ return note; }
	public void setNote(Note note)
	{ this.note = note; }
	
	public RobotComponent getParent()
	{ return parent; }
	public void setParent(RobotComponent rc)
	{ this.parent = rc; }
	

	/*
	@Realization public void initRealization(RealizationModel model){
		this.angle = 90;
		this.speed = ((Number)RobotInfoStore.getInfo(this)
				.get(ServoMotorInfoKeys.SPEED)).doubleValue()
				/ 15.0;
	}// end initRealization(RealizationModel)
	@Realization public void
			rotate(RealizationModel model, double angle){
		System.out.println("HS_311 : rotate : angle : " + angle + " , ths.spd : " + speed);
		// angle - 90만큼이 dest angle이다.
		torqueGenerator.order(this.diskStr, this.bodyStr,
				Math.toRadians(angle - 90.0), speed);
		this.angle = angle;
	}//end rotate(double)
	@Realization public double getAngle(RealizationModel model){
		if(torqueGenerator.moving(diskStr, bodyStr))
			return torqueGenerator.getAngle(diskStr, bodyStr, model.getWorld());
		return this.angle;
	}// end getAngle()
	@Realization public boolean isMoving(RealizationModel model){
		return torqueGenerator.moving(diskStr, bodyStr);
	}// end isMoving()
	@Realization public double getSpeed(RealizationModel model)
	{ return speed; }
	@Realization public void setSpeed(RealizationModel model, double speed){
		System.out.println("HS_311 : setSpeed : " + speed);
		this.speed = speed;
	}// end setSpeed(RealizationMode, double)
	*/
	
	public boolean hasSubComponent()
	{ return true; }
	public RobotComponent[] getSubComponents()
	{ return subComponents.toArray(NULLRCARRAY); }
	public boolean hasReceiver()
	{ return true; }
	public Receiver[] getReceivers()
	{ return receivers; }
	public boolean hasSender()
	{ return false; }
	public Sender[] getSenders()
	{ return null; }
	
	
	
	/*
	 * RDObj Implementation
	 */
	private final static boolean[] rd_fieldconsts = new boolean[]{
		true,
		true
	};
	private final static RDObject.Type[] rd_fieldtypes = new RDObject.Type[]{
		new RDObject.Type(Object.class, RDObject.Type.NORMAL),
		new RDObject.Type(Object.class, RDObject.Type.NORMAL)
	};
	private final static String[] rd_fieldnames = new String[]{
		"bodyStrKey",
		"diskStrKey"
	};
	private final static String[] rd_fielddescs = new String[]{
		
	};
	private final static String[] rd_methodnames = new String[]{};
	private final static String[] rd_methoddescs = new String[]{};
	private final static String[][] rd_methodparamnames = new String[0][0];
	private final static RDObject.Type[][] rd_methodparamtypes = new RDObject.Type[0][0];
	private final static RDObject.Type[] rd_methodtypes = new RDObject.Type[0];
	
	
	private transient Object bodyStrKey, diskStrKey;
	private transient RealizationModel rmodel;
	
	// constants
	public Object getDiskStructureKey()
	{ return diskStrKey; }
	public Object getBodyStructureKey()
	{ return bodyStrKey; }
	public void setDiskStructureKey(Object key)
	{ this.diskStrKey = key; }
	public void setBodyStructureKey(Object key)
	{ this.bodyStrKey = key; }
	
	
	public String[] getRDMethodNames()
	{ return rd_methodnames; }
	public RDObject.Type[][] getRDMethodParamTypes()
	{ return rd_methodparamtypes; }
	public RDObject.Type[] getRDMethodTypes()
	{ return rd_methodtypes; }
	public String[] getRDMethodDescs()
	{ return rd_methoddescs; }
	public String[][] getRDMethodParamNames()
	{ return rd_methodparamnames; }
	

	public Object getRDFieldValue(RealizationModel model,
			String fname)
			throws RealizationNotReadyException, RDFieldNotExistException{
		if(rmodel == null || rmodel.getWorld().saidHello())
			throw new RealizationNotReadyException();
		
		if(fname.equals(rd_fieldnames[0]))
			return this.bodyStrKey;
		else if(fname.equals(rd_fieldnames[1]))
			return this.diskStrKey;
		
		throw new RDFieldNotExistException("", this, fname);
	}// end getRDFieldVAlue
	public void setRDFieldValue(RealizationModel model,
			String fname, Object val)
			throws RealizationNotReadyException, RDFieldNotExistException{
		if(rmodel == null || rmodel.getWorld().saidHello())
			throw new RealizationNotReadyException();
		
		if(fname.equals(rd_fieldnames[0])) ;
		else if(fname.equals(rd_fieldnames[1])) ;
		
		throw new RDFieldNotExistException("", this, fname);
	}// end setRDFieldValue
	public Object callRDMethod(RealizationModel model,
			String fname, Object[] params)
			throws RealizationNotReadyException, RDMethodNotExistException{
		if(rmodel == null || rmodel.getWorld().saidHello())
			throw new RealizationNotReadyException();
		
		throw new RDMethodNotExistException("", this, fname, params);
	}// end callRDMethod
	
	
	public String[] getRDFieldNames()
	{ return rd_fieldnames; }
	public boolean[] getRDConstants()
	{ return rd_fieldconsts; }
	public RDObject.Type[] getRDFieldTypes()
	{ return rd_fieldtypes; }
	public String[] getRDFieldDescs()
	{ return rd_fielddescs; }
	
	public void initRlztion(RealizationModel rm) throws RealizationStartedException{
		if(rmodel != null && !rmodel.getWorld().saidHello())
			throw new RealizationStartedException();
		rmodel = rm;
		토크펑션 구현 안함!!! 꼭 해줘야함
	}// end initRlztion
	public void disposeRlztion() throws RealizationNotReadyException{
		if(rmodel == null || rmodel.getWorld().saidHello())
			throw new RealizationNotReadyException();
		rmodel = null;
		bodyStrKey = diskStrKey = null;
	}// end disposeRlztion
	
	public java.awt.Component getPropertyViewer(RealizationModel rm)
	{ return null; }
	
	
	
	
	
	
	static class HS_311Receiver extends ThreePin{
		public final static String CONDITION = "Must be connected to controller.";
		
		private HS_311 parent;
		
		public HS_311Receiver(HS_311 p)
		{ this.parent = p; super.setName("rcv"); }
		
		
		public HS_311 getParent()
		{ return parent; }
		
		public void setParent(RobotComponent rc){}
		
		public String getCondition()
		{ return CONDITION; }

		public boolean isCompatible(Receiver rcv){
			if(rcv == this) return false;
			else if(rcv.getParent() == this) return false;
			
			return rcv.getParent() instanceof MR_C2000S;
		}// end isCompatible(Receiver)
		public boolean isCompatible(Sender rcv){
			if(rcv == this) return false;
			else if(rcv.getParent() == this) return false;
			
			return rcv.getParent() instanceof MR_C2000S;
		}// end isCompatible(Receiver)
	}// end class HS_311Receiver
}
