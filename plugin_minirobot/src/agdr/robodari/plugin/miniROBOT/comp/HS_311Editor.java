package agdr.robodari.plugin.miniROBOT.comp;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.vecmath.*;

import agdr.robodari.appl.*;
import agdr.robodari.comp.*;
import agdr.robodari.edit.*;
import agdr.robodari.gui.*;
import agdr.robodari.plugin.rme.appl.*;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.gui.*;

public class HS_311Editor extends CompEditor implements SketchEditorInterface{
	private JTabbedPane tabbedPane;
	
	private JPanel propertyPanel;
		private NoteEditor noteEditor;
		private JTextField nameField;
	
	private JPanel aoEditPanel;
		private JPanel aoViewerPanel;
		private SideSketchViewer[] aoViewers;
		private SketchModel aoSketchModel;
		private SkObjectViewer aoCompViewer;
		
		private JButton rotationButton;
		private RotationEditor rotationEditor;
		
		private DefaultComboBoxModel aoSelectModel;
		private JComboBox aoSelectComboBox;
		private JPanel moveButtonPanel;
		private JPanel aoCompViewerPanel;
		private JButton[] moveButtons;
		private JTextField[] positionFields;
		private JTextField segmentField;
		
		private SkObject originalObject;
		
		private SkObject motorObject;
		private SkObject attachedObject;
		// x, y, z방향으로 움직일 때 움직이는 칸의 양 
		private double segment = 2.0f;
	
	
	
	public HS_311Editor(SketchEditorInterface manager, Frame parent){
		super(manager, parent, "HS_311 editor");
		__set();
	}// end Constructor
	public HS_311Editor(SketchEditorInterface manager, Dialog parent){
		super(manager, parent, "HS_311 editor");
		__set();
	}// end Constructor(EditorManager)
	private void __set(){
		initPropertyPanel();
		initAddonEditPanel();
		tabbedPane = new JTabbedPane();
		tabbedPane.setTabPlacement(JTabbedPane.LEFT);
		tabbedPane.addTab("Property", propertyPanel);
		tabbedPane.addTab("Addon", aoEditPanel);
		
		super.getEditorPanel().setLayout(new BorderLayout());
		super.getEditorPanel().add(tabbedPane, BorderLayout.CENTER);
		
		super.setSize(650, 600);
	}// end __set()
	private void initPropertyPanel(){
		propertyPanel = new JPanel();
		propertyPanel.setLayout(new BorderLayout());
		nameField = new JTextField();
		
		JPanel northPanel = new JPanel();
		northPanel.setLayout(new BorderLayout());
		northPanel.add(new JLabel("Component name : "), BorderLayout.WEST);
		northPanel.add(nameField, BorderLayout.CENTER);

		noteEditor = new NoteEditor();
		
		propertyPanel.add(northPanel, BorderLayout.NORTH);
		propertyPanel.add(noteEditor, BorderLayout.CENTER);
	}// end initPropertyPanel()
	private void initAddonEditPanel(){
		aoEditPanel = new JPanel();
		aoEditPanel.setLayout(new BorderLayout());
		
		// 중앙

		aoViewerPanel = new JPanel();
		aoViewerPanel.setLayout(new GridLayout(2, 2, 3, 3));
		{
			aoViewers = new SideSketchViewer[3];
			aoViewers[0] = new SideSketchViewer(null, SketchEditor.PROJECTORS[0]);
			aoViewers[1] = new SideSketchViewer(null, SketchEditor.PROJECTORS[1]);
			aoViewers[2] = new SideSketchViewer(null, SketchEditor.PROJECTORS[2]);
			
			aoCompViewerPanel = new JPanel(new BorderLayout());
			aoCompViewerPanel.setLayout(new BorderLayout());
			{
				aoSelectModel = new DefaultComboBoxModel();
				aoSelectComboBox = new JComboBox(aoSelectModel);
				aoSelectComboBox.addItemListener(new ChangeAddonAction(this));
				aoCompViewer = new SkObjectViewer();
				
				aoCompViewerPanel.add(aoSelectComboBox, BorderLayout.NORTH);
				aoCompViewerPanel.add(aoCompViewer, BorderLayout.CENTER);
			}
			
			aoViewerPanel.add(aoViewers[0]);
			aoViewerPanel.add(aoCompViewerPanel);
			aoViewerPanel.add(aoViewers[1]);
			aoViewerPanel.add(aoViewers[2]);
		}
		
		aoSketchModel = new SketchModel("HS_311EditorSKModel");
		
		aoViewers[0].loadSketch(aoSketchModel);
		aoViewers[1].loadSketch(aoSketchModel);
		aoViewers[2].loadSketch(aoSketchModel);
		
		// north
		
		moveButtonPanel = new JPanel();
		moveButtonPanel.setLayout(new GridLayout(3, 3, 2, 2));
		{
			moveButtons = new JButton[6];
			moveButtons[0] = new JButton(new MoveAction(MoveAction.X, MoveAction.PLUS, this));
			moveButtons[1] = new JButton(new MoveAction(MoveAction.X, MoveAction.MINUS, this));
			moveButtons[2] = new JButton(new MoveAction(MoveAction.Y, MoveAction.PLUS, this));
			moveButtons[3] = new JButton(new MoveAction(MoveAction.Y, MoveAction.MINUS, this));
			moveButtons[4] = new JButton(new MoveAction(MoveAction.Z, MoveAction.PLUS, this));
			moveButtons[5] = new JButton(new MoveAction(MoveAction.Z, MoveAction.MINUS, this));
			for(int i = 0; i < moveButtons.length; i++){
				moveButtons[i].setMargin(new Insets(1, 1, 1, 1));
				moveButtons[i].setPreferredSize(new Dimension(23, 23));
				moveButtons[i].setSize(new Dimension(23, 23));
			}
			moveButtonPanel.setPreferredSize(new Dimension(73, 73));
			String[] labellist = new String[]{"x:", "y:", "z:"};
			for(int i = 0; i < 3; i++){
				moveButtonPanel.add(new JLabel(labellist[i], JLabel.RIGHT));
				for(int j = 0; j < 2; j++)
					moveButtonPanel.add(moveButtons[2 * i + j]);
			}
		}
		JPanel positionFieldsPanel = new JPanel();
		positionFieldsPanel.setLayout(new GridLayout(3, 0, 2, 2));
		{
			positionFields = new JTextField[3];
			positionFields[0] = new JTextField();
			positionFields[1] = new JTextField();
			positionFields[2] = new JTextField();
			for(int i = 0; i < positionFields.length; i++){
				positionFields[i].setColumns(8);
				positionFields[i].setEditable(false);
				positionFieldsPanel.add(positionFields[i]);
			}
		}
		
		JPanel firstPanel = new JPanel();
		BoxLayout layout = new BoxLayout(firstPanel, BoxLayout.Y_AXIS);
		firstPanel.setLayout(layout);
		{
			segmentField = new JTextField();
			segmentField.setText(String.valueOf(2.0));
			segmentField.setColumns(4);
			segmentField.setHorizontalAlignment(JTextField.RIGHT);
			segmentField.addFocusListener(new SegmentAction(this));
			
			firstPanel.add(new JLabel("Interval : "));
			firstPanel.add(new JLabel("(unit : )"));
			firstPanel.add(segmentField);
		}

		rotationButton = new JButton("rotate..");
		rotationButton.addActionListener(new RotateAddonAction(this));
		rotationEditor = new RotationEditor(this.getInterface(), this);
		
		JPanel northPanel = new JPanel();
		northPanel.setLayout(new FlowLayout());
		northPanel.add(firstPanel);
		northPanel.add(moveButtonPanel);
		northPanel.add(positionFieldsPanel);
		northPanel.add(new JLabel("Rotation : "));
		northPanel.add(rotationButton);
		
		aoEditPanel.add(northPanel, BorderLayout.NORTH);
		aoEditPanel.add(aoViewerPanel, BorderLayout.CENTER);
	}// end initAddonEditPanel()
	
	
	
	
	
	
	
	
	
	
	/* *****************************************************
	 *                       Comp Editor
	 *******************************************************/
	
	public void startEditing(SkObject newobj) throws Exception{
		if(!(newobj.getRobotComponent() instanceof HS_311))
			return;
		
		this.aoSketchModel.clear();
		
		// getting total data..
		this.originalObject = newobj;
		HS_311 comp = (HS_311)this.originalObject.getRobotComponent();
		
		Matrix4f motortrans = new Matrix4f();
		motortrans.m00 = motortrans.m11 = motortrans.m22 = motortrans.m33 = 1;
		
		this.motorObject = new SkObject(new HS_311(), motortrans);
		
		if(comp.getAttachedObject() != null){
			this.attachedObject = (SkObject)(comp.getAttachedObject()).clone();
		}else{
			Matrix4f aotrans = new Matrix4f();
			aotrans.m00 = aotrans.m11 = aotrans.m22 = aotrans.m33 = 1;
			this.attachedObject = new SkObject((RobotComponent)null, new Matrix4f());
			this.attachedObject.setTransform(aotrans);
		}
		
		
		// properties panel..
		_showPosition(this.attachedObject.getTransform().m03,
				this.attachedObject.getTransform().m13,
				this.attachedObject.getTransform().m23); 
		nameField.setText(comp.getName());
		if(comp.getNote() == null)
			comp.setNote(new Note());
		noteEditor.open(comp.getNote());

		// selection model setting..		
		aoCompViewer.setObject(attachedObject);

		aoSketchModel.addObject(attachedObject);
		aoSketchModel.addObject(motorObject);
		aoSelectModel.removeAllElements();
		SketchModel model = super.getInterface().getModel();
		
		aoSelectModel.addElement("(empty)");
		SkObject[] objs = model.getObjects();
		for(int i = 0; i < objs.length; i++){
			if(objs[i] != newobj){
				aoSelectModel.addElement(objs[i]);
			}
		}
		if(comp.getAttachedObject() != null){
			aoSelectModel.addElement(comp.getAttachedObject());
			aoSelectModel.setSelectedItem(comp.getAttachedObject());
		}
		aoSelectComboBox.setModel(aoSelectModel);
		
		repaint();
	}// end startEditing(SkObject)
	public void finishEditing() throws Exception{
		SketchModel model = super.getInterface().getModel();
		
		HS_311 rcomp = (HS_311)originalObject.getRobotComponent();
		if(rcomp.getAttachedObject() != null){
			SkObject aoobj = rcomp.getAttachedObject();
			Matrix4f originalTrans = new Matrix4f(originalObject.getTransform());
			originalTrans.mul(aoobj.getTransform());
			aoobj.setTransform(originalTrans);
			
			model.addObject(aoobj);
			rcomp.setAttachedObject(null);
		}
		rcomp.setNote(noteEditor.getNote());
		rcomp.setName(nameField.getText());
		if(attachedObject.getRobotComponent() != null || attachedObject.isGroup())
			rcomp.setAttachedObject(attachedObject);
		
		if(!(aoSelectComboBox.getSelectedItem() instanceof String))
			model.removeObject((SkObject)aoSelectComboBox.getSelectedItem());
		originalObject.refresh();
		if(getInterface() != null)
			getInterface().refreshObject(originalObject);
			
		aoCompViewer.setObject(null);
		
		this.originalObject = null;
		this.setVisible(false);
		getInterface().setCurrentCompEditor(null);
	}// end finishEditing()
	
	public SkObject getObject()
	{ return originalObject; }
	
	public SkObject getAttachedObject()
	{ return attachedObject; }
	
	public SkObjectViewer getAddonCompViewer()
	{ return aoCompViewer; }
	public JTextField[] getPositionFields()
	{ return positionFields; }
	
	
	private void _showPosition(double fx, double fy, double fz){
		positionFields[0].setText(String.format("%.5f", fx).toString());
		positionFields[1].setText(String.format("%.5f", fy).toString());
		positionFields[2].setText(String.format("%.5f", fz).toString());
	}// end _showPosition(double, double, flaot)
	
	
	
	/* **************************************************
	 *                SketchEditorInterface
	 ***************************************************/
	
	public int getSideSketchViewerCount()
	{ return aoViewers.length; }
	public SideSketchViewer getSideSketchViewer(int idx)
	{ return aoViewers[idx]; }
	public int getLayoutMode()
	{ return -1; }
	public void setLayoutMode(int mode){}
	public MainSketchEditor getMainEditor()
	{ return null; }
	public SketchModel getModel()
	{ return aoSketchModel; }
	

	public void addObject(RobotComponent obj, Matrix4f trans)
	{ System.err.println("HS_311Editor : addObject : ignored!"); }
	public void refreshObject(SkObject object){
		if(object == null) return;
		if(attachedObject != object)
			return;
		
		try{
			object.refresh();
			aoCompViewer.setObject(attachedObject);
			refreshSideSketchViewers();
			this.repaint();
		}catch(Exception excp){
			agdr.robodari.appl.BugManager.log(excp, true);
		}
	}// end refresh()
	private void refreshSideSketchViewers(){
		for(int i = 0; i < getSideSketchViewerCount(); i++)
			getSideSketchViewer(i).refreshObject(attachedObject);
	}// end refreshSideSketchViewers()
	
	
	
	public GroupEditor getGroupEditor()
	{ return null; }
	public RotationEditor getRotationEditor()
	{ return this.rotationEditor; }
	public CompEditor getCurrentCompEditor()
	{ return this.rotationEditor.isVisible() ? this.rotationEditor : null; }
	public CompEditor getStoredCompEditor(Class<? extends RobotComponent> c)
	{ return getInterface().getStoredCompEditor(c); }
	public void setCurrentCompEditor(CompEditor ce){
		if(this.rotationEditor == ce && this.rotationEditor.isVisible()) return;
		if(ce != null)
			ce.setVisible(true);
	}// end showCompEditor(CompEditor)
	
	
	
	
	
	
	
	static class ChangeAddonAction implements ItemListener{
		private HS_311Editor editor;
		
		public ChangeAddonAction(HS_311Editor editor)
		{ this.editor = editor; }
		
		public void itemStateChanged(ItemEvent ie){
			try{
				JComboBox jcb = (JComboBox)ie.getSource();
				if(jcb.getSelectedItem() instanceof String){
					if(editor.attachedObject.isGroup())
						editor.attachedObject.setGroup(false);
					editor.attachedObject.setRobotComponent(null);
					editor.refreshObject(editor.attachedObject);
					return;
				}
				SkObject obj = (SkObject)jcb.getSelectedItem();
				if(obj == null) return;
				
				if(editor.getObject() == null) return;
				
				if(!obj.isGroup()){
					editor.attachedObject.setGroup(false);
					editor.attachedObject.setRobotComponent(obj.getRobotComponent());
				}else{
					editor.attachedObject.setGroup(true);
					editor.attachedObject.setGroupName(obj.getGroupName());
					editor.attachedObject.removeChildren();
					SkObject[] children = obj.getChildren();
					for(int i = 0; i < children.length; i++)
						editor.attachedObject.addChild(children[i]);
				}
				editor.attachedObject.refresh();
				editor.refreshObject(editor.attachedObject);
			}catch(Exception excp){
				agdr.robodari.appl.BugManager.log(excp, true);
			}
		}// end itemStateChanged(ItemEvent)
	}// end action ChangeAddonAction
	static class RotateAddonAction implements ActionListener{
		private HS_311Editor editor;
		
		public RotateAddonAction(HS_311Editor e)
		{ this.editor = e; }
		
		public void actionPerformed(ActionEvent ae){
			editor.getRotationEditor().startEditing(editor.attachedObject);
			editor.getRotationEditor().setVisible(true);
		}// end actionPerformed(ActionEvent)
	}// end action RotateAddonAction
	static class MoveAction extends AbstractAction{
		public final static int X = 0, Y = 1, Z = 2;
		public final static int PLUS = 1, MINUS = -1;
		
		private int axis;
		private double dir;
		private HS_311Editor editor;
		public MoveAction(int axis, int dir, HS_311Editor editor){
			super(dir == PLUS ? "+" : "-");
			this.axis = axis;
			this.dir = dir;
			this.editor = editor;
		}// end Constructor(int, int, HS_311Editor)
		
		public void actionPerformed(ActionEvent ae){
			SkObject obj = editor.getObject();
			if(obj == null) return;
			
			obj = editor.attachedObject;
			Matrix4f trans = obj.getTransform();
			if(axis == X)
				trans.m03 += dir * editor.segment;
			else if(axis == Y)
				trans.m13 += dir * editor.segment;
			else if(axis == Z)
				trans.m23 += dir * editor.segment;
			
			editor._showPosition(trans.m03, trans.m13, trans.m23);
			
			editor.refreshObject(obj);
		}// end actionPerformed(ActionEvent)
	}// end action MoveAction
	static class SegmentAction implements FocusListener{
		private HS_311Editor editor;
		
		public SegmentAction(HS_311Editor he)
		{ this.editor = he; }
		
		public void focusGained(FocusEvent Fe){}
		public void focusLost(FocusEvent fe){
			String szseg = editor.segmentField.getText();
			try{
				editor.segment = Float.parseFloat(szseg);
			}catch(Exception excp){
				agdr.robodari.plugin.rme.appl.ExceptionHandler.onlyDecimalAccepted();
				editor.segmentField.setText(String.valueOf(editor.segment));
			}
		}// end focusLost(FocusEvent)
	}// end action SegmentAction
}
