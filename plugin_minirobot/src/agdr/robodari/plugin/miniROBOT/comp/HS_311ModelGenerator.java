package agdr.robodari.plugin.miniROBOT.comp;

import java.io.*;
import javax.vecmath.*;

import agdr.library.math.*;
import agdr.robodari.comp.model.*;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.store.*;

public class HS_311ModelGenerator implements ModelGenerator<HS_311>{
	private StringBuffer code = new StringBuffer();
	private StringBuffer disk = new StringBuffer();
	
	public HS_311ModelGenerator(){
		code.append("v 0 2 3.3\n")
			.append("v 0 0 3.3\n")
			.append("v 4 0 3.3\n")
			.append("v 4 2 3.3\n")
			.append("v 0 2 0\n")
			.append("v 0 0 0\n")
			.append("v 4 0 0\n")
			.append("v 4 2 0\n")
			// 8
			.append("\n")
			.append("v 0.4 1.9 3.6\n")
			.append("v 0.4 1.9 3.3\n")
			.append("v 0.4 1.4 3.6\n")
			.append("v 0.4 1.4 3.3\n")
			.append("v 0.25 1.2 3.6\n")
			.append("v 0.25 1.2 3.3\n")
			.append("v 0.2 1 3.6\n")
			.append("v 0.2 1 3.3\n")
			.append("v 0.25 0.8 3.6\n")
			.append("v 0.25 0.8 3.3\n")
			.append("v 0.4 0.6 3.6\n")
			.append("v 0.4 0.6 3.3\n")
			.append("v 0.4 0.1 3.6\n")
			.append("v 0.4 0.1 3.3\n")
			// 22
			.append("\n")
			.append("# B\n")
			.append("# 23\n")
			.append("v 3.0 0.1 3.3\n")
			.append("v 3.0 0.1 3.6\n")
			.append("v 3.3444152 0.16850843 3.3\n")
			.append("v 3.3444152 0.16850843 3.6\n")
			.append("v 3.6363962 0.3636039 3.3\n")
			.append("v 3.6363962 0.3636039 3.6\n")
			.append("v 3.8314915 0.65558493 3.3\n")
			.append("v 3.8314915 0.65558493 3.6\n")
			.append("v 3.9 1.0 3.3\n")
			.append("v 3.9 1.0 3.6\n")
			.append("v 3.8314915 1.3444151 3.3\n")
			.append("v 3.8314915 1.3444151 3.6\n")
			.append("v 3.6363962 1.636396 3.3\n")
			.append("v 3.6363962 1.636396 3.6\n")
			.append("v 3.3444152 1.8314916 3.3\n")
			.append("v 3.3444152 1.8314916 3.6\n")
			.append("v 3.0 1.9 3.3\n")
			.append("v 3.0 1.9 3.6\n")
			.append("# 40\n")
			.append("\n")
			.append("# A\n")
			.append("v 3.6 1.0 3.6\n")
			.append("v 3.6 1.0 3.8\n")
			.append("v 3.5405812 1.2603302 3.6\n")
			.append("v 3.5405812 1.2603302 3.8\n")
			.append("v 3.3740938 1.4690989 3.6\n")
			.append("v 3.3740938 1.4690989 3.8\n")
			.append("v 3.1335125 1.5849568 3.6\n")
			.append("v 3.1335125 1.5849568 3.8\n")
			.append("v 2.8664875 1.5849568 3.6\n")
			.append("v 2.8664875 1.5849568 3.8\n")
			.append("v 2.6259062 1.4690989 3.6\n")
			.append("v 2.6259062 1.4690989 3.8\n")
			.append("v 2.4594188 1.2603302 3.6\n")
			.append("v 2.4594188 1.2603302 3.8\n")
			.append("v 2.4 1.0 3.6\n")
			.append("v 2.4 1.0 3.8\n")
			.append("v 2.4594188 0.73966974 3.6\n")
			.append("v 2.4594188 0.73966974 3.8\n")
			.append("v 2.6259062 0.53090113 3.6\n")
			.append("v 2.6259062 0.53090113 3.8\n")
			.append("v 2.8664875 0.41504326 3.6\n")
			.append("v 2.8664875 0.41504326 3.8\n")
			.append("v 3.1335125 0.41504326 3.6\n")
			.append("v 3.1335125 0.41504326 3.8\n")
			.append("v 3.3740938 0.53090113 3.6\n")
			.append("v 3.3740938 0.53090113 3.8\n")
			.append("v 3.5405812 0.73966974 3.6\n")
			.append("v 3.5405812 0.73966974 3.8\n")
			.append("# 68\n")
			.append("\n")
			.append("# A'\n")
			.append("v 3.425 1.0 3.8\n")
			.append("v 3.425 1.0 4\n")
			.append("v 3.3829117 1.1844006 3.8\n")
			.append("v 3.3829117 1.1844006 4\n")
			.append("v 3.2649832 1.3322784 3.8\n")
			.append("v 3.2649832 1.3322784 4\n")
			.append("v 3.0945714 1.4143443 3.8\n")
			.append("v 3.0945714 1.4143443 4\n")
			.append("v 2.9054286 1.4143443 3.8\n")
			.append("v 2.9054286 1.4143443 4\n")
			.append("v 2.7350168 1.3322784 3.8\n")
			.append("v 2.7350168 1.3322784 4\n")
			.append("v 2.6170883 1.1844006 3.8\n")
			.append("v 2.6170883 1.1844006 4\n")
			.append("v 2.575 1.0 3.8\n")
			.append("v 2.575 1.0 4\n")
			.append("v 2.6170883 0.8155994 3.8\n")
			.append("v 2.6170883 0.8155994 4\n")
			.append("v 2.7350168 0.6677216 3.8\n")
			.append("v 2.7350168 0.6677216 4\n")
			.append("v 2.9054286 0.5856556 3.8\n")
			.append("v 2.9054286 0.5856556 4\n")
			.append("v 3.0945714 0.5856556 3.8\n")
			.append("v 3.0945714 0.5856556 4\n")
			.append("v 3.2649832 0.6677216 3.8\n")
			.append("v 3.2649832 0.6677216 4\n")
			.append("v 3.3829117 0.8155994 3.8\n")
			.append("v 3.3829117 0.8155994 4\n")/*
			.append("# 96\n")
			.append("\n")
			.append("# B'\n")
			.append("v 4.2 1.0 4\n")
			.append("v 4.2 1.0 4.2\n")
			.append("v 4.0811625 1.5206605 4\n")
			.append("v 4.0811625 1.5206605 4.2\n")
			.append("v 3.7481878 1.9381977 4\n")
			.append("v 3.7481878 1.9381977 4.2\n")
			.append("v 3.2670252 2.1699135 4\n")
			.append("v 3.2670252 2.1699135 4.2\n")
			.append("v 2.7329748 2.1699135 4\n")
			.append("v 2.7329748 2.1699135 4.2\n")
			.append("v 2.2518122 1.9381977 4\n")
			.append("v 2.2518122 1.9381977 4.2\n")
			.append("v 1.9188373 1.5206605 4\n")
			.append("v 1.9188373 1.5206605 4.2\n")
			.append("v 1.8 1.0 4\n")
			.append("v 1.8 1.0 4.2\n")
			.append("v 1.9188373 0.4793395 4\n")
			.append("v 1.9188373 0.4793395 4.2\n")
			.append("v 2.2518122 0.06180222 4\n")
			.append("v 2.2518122 0.06180222 4.2\n")
			.append("v 2.7329748 -0.1699135 4\n")
			.append("v 2.7329748 -0.1699135 4.2\n")
			.append("v 3.2670252 -0.1699135 4\n")
			.append("v 3.2670252 -0.1699135 4.2\n")
			.append("v 3.7481878 0.06180222 4\n")
			.append("v 3.7481878 0.06180222 4.2\n")
			.append("v 4.0811625 0.4793395 4\n")
			.append("v 4.0811625 0.4793395 4.2\n")
			.append("# 124\n")
			.append("\n")*/
			.append("\n")
			.append("f 1 5 6 2\n")
			.append("f 2 6 7 3\n")
			.append("f 4 3 7 8\n")
			.append("f 1 4 8 5\n")
			.append("f 8 7 6 5\n")
			.append("f 4 1 2 3 22 20 18 16 14 12 10 39 37 35 33 31 29 27 25 23 22 3\n")
			.append("\n")
			.append("f 9 10 12 11\n")
			.append("f 11 12 14 13\n")
			.append("f 13 14 16 15\n")
			.append("f 15 16 18 17\n")
			.append("f 17 18 20 19\n")
			.append("f 19 20 22 21\n")
			.append("f 21 22 23 24\n")
			.append("f 24 23 25 26\n")
			.append("f 26 25 27 28\n")
			.append("f 28 27 29 30\n")
			.append("f 30 29 31 32\n")
			.append("f 32 31 33 34\n")
			.append("f 34 33 35 36\n")
			.append("f 36 35 37 38\n")
			.append("f 38 37 39 40\n")
			.append("f 40 39 10 9\n")
			.append("\n")
			.append("f 9 11 13 15 17 19 21 24 26 28 30 32 34 36 38 40\n")
			.append("\n")
			.append("# 원반 A\n")
			.append("f 68 66 64 62 60 58 56 54 52 50 48 46 44 42\n")
			.append("f 41 43 45 47 49 51 53 55 57 59 61 63 65 67\n")
			.append("\n")
			.append("f 42 41 43 44\n")
			.append("f 44 43 45 46\n")
			.append("f 46 45 47 48\n")
			.append("f 48 47 49 50\n")
			.append("f 50 49 51 52\n")
			.append("f 52 51 53 54\n")
			.append("f 54 53 55 56\n")
			.append("f 56 55 57 58\n")
			.append("f 58 57 59 60\n")
			.append("f 60 59 61 62\n")
			.append("f 62 61 63 64\n")
			.append("f 64 63 65 66\n")
			.append("f 66 65 67 68\n")
			.append("f 68 67 41 42\n")
			.append("\n")
			.append("# 원반 A'\n")
			.append("# 원반 A에서 28씩만 더해주자.\n")
			.append("f 96 94 92 90 88 86 84 82 80 78 76 74 72 70\n")
			.append("f 69 71 73 75 77 79 81 83 85 87 89 91 93 95\n")
			.append("\n")
			.append("f 70 69 71 72\n")
			.append("f 72 71 73 74\n")
			.append("f 74 73 75 76\n")
			.append("f 76 75 77 78\n")
			.append("f 78 77 79 80\n")
			.append("f 80 79 81 82\n")
			.append("f 82 81 83 84\n")
			.append("f 84 83 85 86\n")
			.append("f 86 85 87 88\n")
			.append("f 88 87 89 90\n")
			.append("f 90 89 91 92\n")
			.append("f 92 91 93 94\n")
			.append("f 94 93 95 96\n")
			.append("f 96 95 69 70\n")
			.append("\n");/*
			.append("# 원반 B' \n")
			.append("f 98 100 102 104 106 108 110 112 114 116 118 120 122 124\n")
			.append("f 123 121 119 117 115 113 111 109 107 105 103 101 99 97 \n")
			.append("\n")
			.append("f 98 97 99 100\n")
			.append("f 100 99 101 102\n")
			.append("f 102 101 103 104\n")
			.append("f 104 103 105 106\n")
			.append("f 106 105 107 108\n")
			.append("f 108 107 109 110\n")
			.append("f 110 109 111 112\n")
			.append("f 112 111 113 114\n")
			.append("f 114 113 115 116\n")
			.append("f 116 115 117 118\n")
			.append("f 118 117 119 120\n")
			.append("f 120 119 121 122\n")
			.append("f 122 121 123 124\n")
			.append("f 124 123 97 98 \n");*/
		disk
			.append("v 4.2 1.0 4\n")
			.append("v 4.2 1.0 4.2\n")
			.append("v 4.0811625 1.5206605 4\n")
			.append("v 4.0811625 1.5206605 4.2\n")
			.append("v 3.7481878 1.9381977 4\n")
			.append("v 3.7481878 1.9381977 4.2\n")
			.append("v 3.2670252 2.1699135 4\n")
			.append("v 3.2670252 2.1699135 4.2\n")
			.append("v 2.7329748 2.1699135 4\n")
			.append("v 2.7329748 2.1699135 4.2\n")
			.append("v 2.2518122 1.9381977 4\n")
			.append("v 2.2518122 1.9381977 4.2\n")
			.append("v 1.9188373 1.5206605 4\n")
			.append("v 1.9188373 1.5206605 4.2\n")
			.append("v 1.8 1.0 4\n")
			.append("v 1.8 1.0 4.2\n")
			.append("v 1.9188373 0.4793395 4\n")
			.append("v 1.9188373 0.4793395 4.2\n")
			.append("v 2.2518122 0.06180222 4\n")
			.append("v 2.2518122 0.06180222 4.2\n")
			.append("v 2.7329748 -0.1699135 4\n")
			.append("v 2.7329748 -0.1699135 4.2\n")
			.append("v 3.2670252 -0.1699135 4\n")
			.append("v 3.2670252 -0.1699135 4.2\n")
			.append("v 3.7481878 0.06180222 4\n")
			.append("v 3.7481878 0.06180222 4.2\n")
			.append("v 4.0811625 0.4793395 4\n")
			.append("v 4.0811625 0.4793395 4.2\n")
			.append("# 124\n")
			.append("# 원반 B' \n")
			.append("f 2 4 6 8 10 12 14 16 18 20 22 24 26 28\n")
			.append("f 27 25 23 21 19 17 15 13 11 9 7 5 3 1 \n")
			.append("\n")
			.append("f 2 1 3 4\n")
			.append("f 4 3 5 6\n")
			.append("f 6 5 7 8\n")
			.append("f 8 7 9 10\n")
			.append("f 10 9 11 12\n")
			.append("f 12 11 13 14\n")
			.append("f 14 13 15 16\n")
			.append("f 16 15 17 18\n")
			.append("f 18 17 19 20\n")
			.append("f 20 19 21 22\n")
			.append("f 22 21 23 24\n")
			.append("f 24 23 25 26\n")
			.append("f 26 25 27 28\n")
			.append("f 28 27 1 2 \n");
	}// end Constructor()
	
	
	
	
	public RobotCompModel generate(HS_311 model) throws IOException, Exception{
		RobotCompModel structure = new RobotCompModel();
		structure.setGroup(true);
		
		_genBody(structure);
		_genDisk(structure);
		
		RobotCompModel rcv = RobotModelStore.generateModel(model.getReceivers()[0]);
		Matrix4f rcvtrans = new Matrix4f();
		rcvtrans.m00 = rcvtrans.m11 = rcvtrans.m22 = rcvtrans.m33 = 1;
		rcvtrans.m13 = 3.5f;
		rcvtrans.m23 = 1.75f;
		structure.addSubModel(rcv, rcvtrans);
		
		SkObject attobj = model.getAttachedObject();
		if(attobj != null){
			if(attobj.isGroup()){
				//System.out.println("HS_311ModelGen : attobj is group");
				structure.addSubModel(attobj.getIntegratedModel(),
						attobj.getTransform());
			}else structure.addSubModel(attobj.getModel(),
					attobj.getTransform());
		}
		
		return structure;
	}// end generate(HS_311)
	
	private void _genBody(RobotCompModel structure) throws Exception{
		Matrix4f bodytrans = new Matrix4f();
		bodytrans.m00 = bodytrans.m11 = bodytrans.m22 = bodytrans.m33 = 1;
		
		RobotCompModel body = new RobotCompModel();
		body.load(new StringReader(code.toString()));
		
		PhysicalData pd = body.getPhysicalData();
		pd.mass = 20;
		pd.centerMassPoint = MathUtility.getCenterMassPoint(body.getPoints(), body.getFaces(), pd.centerMassPoint);
		if(pd.inertia == null)
			pd.inertia = new Matrix3f();
		else
			MathUtility.clear(pd.inertia);
		MathUtility.addInertiaTensor(body.getPoints(),
				body.getFaces(),
				pd.mass,
				pd.centerMassPoint.x,
				pd.centerMassPoint.y,
				pd.centerMassPoint.z,
				pd.inertia);
		pd.bounds = null;
		
		AppearanceData ad = body.getApprData();
		ad.setColor(java.awt.Color.cyan);
		
		structure.addSubModel(body, bodytrans);
	}// end _genBody
	private void _genDisk(RobotCompModel structure) throws Exception{
		RobotCompModel disk = new RobotCompModel();
		Matrix4f disktrans = new Matrix4f();
		disktrans.m00 = disktrans.m11 = disktrans.m22 = disktrans.m33 = 1;
		
		disk.load(new StringReader(this.disk.toString()));
		
		PhysicalData pd = disk.getPhysicalData();
		pd.mass = 23;
		pd.centerMassPoint = new Point3f(3, 1, 4.1f);
		if(pd.inertia == null)
			pd.inertia = new Matrix3f();
		else
			MathUtility.clear(pd.inertia);
		MathUtility.addInertiaTensor(disk.getPoints(),
				disk.getFaces(),
				pd.mass,
				pd.centerMassPoint.x,
				pd.centerMassPoint.y,
				pd.centerMassPoint.z,
				pd.inertia);
		
		AppearanceData ad = disk.getApprData();
		ad.setColor(java.awt.Color.blue);
		
		structure.addSubModel(disk, disktrans);
	}// end _genBody
}
