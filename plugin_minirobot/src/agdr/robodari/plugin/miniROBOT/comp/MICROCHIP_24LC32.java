package agdr.robodari.plugin.miniROBOT.comp;

import agdr.robodari.comp.*;


public class MICROCHIP_24LC32 implements Memory{
	private String name;
	private Note note;
	private RobotComponent parent;
	
	
	public RobotComponent getParent()
	{ return parent; }
	public void setParent(RobotComponent rc)
	{ this.parent = rc; }
	
	
	public String getName()
	{ return name; }
	public void setName(String name)
	{ this.name = name; }
	public Note getNote()
	{ return note; }
	public void setNote(Note note)
	{ this.note = note; }
	
	public boolean hasSubComponent()
	{ return false; }
	public boolean hasReceiver()
	{ return false; }
	public boolean hasSender()
	{ return false; }
	public RobotComponent[] getSubComponents()
	{ return null; }
	public Receiver[] getReceivers()
	{ return null; }
	public Sender[] getSenders()
	{ return null; }
}
