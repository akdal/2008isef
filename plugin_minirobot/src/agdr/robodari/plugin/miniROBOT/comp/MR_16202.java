package agdr.robodari.plugin.miniROBOT.comp;


import agdr.robodari.comp.*;
import agdr.robodari.plugin.defcomps.*;

public class MR_16202 implements Display{
	private final static float[][] BIGPANEL_COORDS =
		new float[][]{{0, 8.0f, 8.0f, 0}, {0, 0, 3.5f, 3.5f}};
	private final static float[][] SMALLPNL_COORDS = 
		new float[][]{{0, 4.5f, 4.5f, 0}, {0, 0, 2, 2}};
	
	
	
	private ElectricPanel bigPanel;
	private ElectricPanel smallPanel;
	private SenderSerial firstSSerial;
	private SenderSerial transmitSSerial;
	private ATMEL_AT89C4051 controller;
	private RobotComponent[] subComps;
	
	private RobotComponent parent;
	private Note note;
	private String name;
	
	
	public MR_16202(){
		bigPanel = new ElectricPanel();
		smallPanel = new ElectricPanel();
		firstSSerial = new SenderSerial();
		transmitSSerial = new SenderSerial();
		controller = new ATMEL_AT89C4051();
		
		for(int i = 0; i < 16; i++){
			OnePin op = new _OnePin(this);
			op.setEnabled(false);
			op.setPinLength(ThreePin.DEFAULT_PINLENGTH * 2.0f);
			firstSSerial.appendSender(op);
		}
		
		ThreePin thp = new _ThreePin(this);
		thp.setEnabled(true);
		thp.setName("P1");
		thp.setPinLength(ThreePin.DEFAULT_PINLENGTH * 2.0f);
		transmitSSerial.appendSender(thp);
		
		subComps = new RobotComponent[]{ bigPanel, smallPanel,
				firstSSerial, transmitSSerial, controller };
		
		bigPanel.setThickness(ElectricPanel.THICKNESS_DEFAULT);
		bigPanel.setShape(BIGPANEL_COORDS[0], BIGPANEL_COORDS[1]);
		smallPanel.setThickness(ElectricPanel.THICKNESS_DEFAULT);
		smallPanel.setShape(SMALLPNL_COORDS[0], SMALLPNL_COORDS[1]);
		
		this.note = new Note();
	}// end Constructor
	
	
	public Note getNote()
	{ return note; }
	public void setNote(Note tnoe)
	{ this.note = tnoe; }
	public String getName()
	{ return name; }
	public void setName(String name)
	{ this.name = name; }
	
	public boolean hasSubComponent()
	{ return true; }
	public RobotComponent[] getSubComponents()
	{ return subComps; }
	

	public ElectricPanel getBigPanel()
	{ return bigPanel; }
	public ElectricPanel getSmallPanel()
	{ return smallPanel; }
	public SenderSerial getLCD_CntrllrConnector()
	{ return firstSSerial; }
	public SenderSerial getConnector()
	{ return transmitSSerial; }
	public ATMEL_AT89C4051 getController()
	{ return controller; }
	
	public void setString(String str){}
	public String getString()
	{ return null; }
	
	
	public boolean hasSender()
	{ return true; }
	public boolean hasReceiver()
	{ return true; }
	
	public Sender[] getSenders()
	{ return transmitSSerial.getSenders(); }
	public Receiver[] getReceivers()
	{ return transmitSSerial.getReceivers(); }

	public boolean suppliesPower()
	{ return false; }
	public PowerSupply getPowerSupply()
	{ return null; }
	public RobotComponent getParent()
	{ return parent; }
	public void setParent(RobotComponent p){
		this.parent = p;
	}// end setParent(RobotComponent)
	
	
	
	
	static class _OnePin extends OnePin{
		private MR_16202 parent;
		
		public _OnePin(MR_16202 p)
		{ this.parent = p; }
		
		public boolean isCompatible(Receiver rcv)
		{ return false; }
		public boolean isCompatible(Sender sd)
		{ return false; }
		public RobotComponent getParent()
		{ return parent; }
		public void setParent(RobotComponent rc){}
		public String getCondition()
		{ return null; }
		
		public boolean suppliesPower()
		{ return false; }
		public PowerSupply getPowerSupply()
		{ return null; }
	}// end class _OnePin
	
	static class _ThreePin extends ThreePin{
		private RobotComponent parent;
		private Receiver connection;
		
		public _ThreePin(RobotComponent p)
		{ this.parent = p; }
		
		
		public boolean suppliesPower()
		{ return false; }
		public PowerSupply getPowerSupply()
		{ return null; }
		
		public boolean isCompatible(Receiver rcv){
			if(rcv == this) return false;
			else if(rcv == null) return true;
			
			return true;
		}// end isCompatible(Receiver rcv)
		
		public boolean isCompatible(Sender sd){
			System.out.println("three pin - isCompatible(2) in mr 16202");
			if(sd == this) return false;
			else if(sd == null) return true;
			else if(sd.getParent() instanceof RobotComponent)
				return false; 
			
			return true;
		}// end isCompatible(Receiver rcv)
		
		public String getCondition(){
			System.out.println("three pin - getCondition in mr 16202");
			return null;
		}// end getCondition(
		
		public RobotComponent getParent()
		{ return parent; }
		public void setParent(RobotComponent rc){}
	}// end class _ThreePin
}
