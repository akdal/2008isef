package agdr.robodari.plugin.miniROBOT.comp;

import java.net.*;
import java.io.*;
import javax.media.j3d.*;
import javax.vecmath.*;

import agdr.robodari.comp.info.*;
import agdr.robodari.comp.model.AppearanceData;
import agdr.robodari.comp.model.ModelGenerator;
import agdr.robodari.comp.model.RobotCompModel;
import agdr.robodari.plugin.defcomps.*;
import agdr.robodari.plugin.rme.rlztion.*;
import agdr.robodari.plugin.rme.store.*;
import agdr.robodari.store.*;

import com.sun.j3d.loaders.objectfile.*;

public class MR_16202_ModelGenerator implements ModelGenerator<MR_16202>{
	public RobotCompModel generate(MR_16202 model) throws Exception{
		if(!RobotModelStore.isRegistered(ElectricPanel.class))
			throw new ModelNotRegisteredException(ElectricPanel.class.toString());
		else if(!RobotModelStore.isRegistered(ATMEL_AT89C4051.class))
			throw new ModelNotRegisteredException(ATMEL_AT89C4051.class.toString());
		else if(!RobotModelStore.isRegistered(SenderSerial.class))
			throw new ModelNotRegisteredException(SenderSerial.class.toString());
		
		

		Info info = RobotInfoStore.getInfo(model);
		double weight = ((Number)info.get(InfoKeys.WEIGHT)).doubleValue();
		URL mr_16202_case = RoboBasicInitializer.class.
				getResource("models/MR-16202_case.obj");
		
		RobotCompModel rootst = new RobotCompModel();
		//double[] volumes = new double[6];
		
		// 1.
		
		Matrix4f caseTrans = new Matrix4f();
		caseTrans.m03 = 0.45f;
		caseTrans.m13 = 0.4f;
		caseTrans.m23 = 0.15f;
		caseTrans.m00 = caseTrans.m11 = caseTrans.m22 = caseTrans.m33 = 1;
		RobotCompModel casest = new RobotCompModel();
		casest.load(new InputStreamReader(mr_16202_case.openStream()));
		rootst.addSubModel(casest, caseTrans);
		
		// 2.
		Matrix4f bpTrans = new Matrix4f();
		bpTrans.m00 = bpTrans.m11 = bpTrans.m22 = bpTrans.m33 = 1;
		rootst.addSubModel(RobotModelStore.generateModel(model.
				getBigPanel()), bpTrans);
		
		// 3.
		Matrix4f spTrans = new Matrix4f();
		spTrans.m00 = spTrans.m11 = spTrans.m22 = spTrans.m33 = 1;
		spTrans.m03 = 0.30f;
		spTrans.m13 = 1.5f;
		spTrans.m23 = -0.35f;
		rootst.addSubModel(RobotModelStore.generateModel(model.
				getSmallPanel()), spTrans);
		
		
		// 4.
		Matrix4f ssTrans = new Matrix4f();
		ssTrans.m03 = 0.6f;
		ssTrans.m13 = 3.4f;
		ssTrans.m23 = 0.25f;
		ssTrans.m00 = ssTrans.m33 = 1;
		ssTrans.m11 = ssTrans.m22 = -1;
		rootst.addSubModel(RobotModelStore.generateModel(model.
				getLCD_CntrllrConnector()), ssTrans);
		
		
		// 5. 
		Matrix4f ctTrans = new Matrix4f();
		ctTrans.m03 = 3.8f;
		ctTrans.m13 = 2.9f;
		ctTrans.m23 = -0.9f;
		ctTrans.m01 = -1;
		ctTrans.m10 = -1;
		ctTrans.m22 = -1;
		ctTrans.m33 = 1;
		rootst.addSubModel(RobotModelStore.generateModel(model.getController()), ctTrans);
		
		// 6.
		Matrix4f senderTrans = new Matrix4f();
		senderTrans.m03 = 4;
		senderTrans.m13 = 2.1f;
		senderTrans.m23 = -0.8f;
		
		// x축
		//senderTrans.m00 = 1;
		//senderTrans.m12 = -1;
		//senderTrans.m21 = 1;
		
		// y축
		//senderTrans.m02 = senderTrans.m20 = 1;
		//senderTrans.m11 = 1;
		
		// z축
		
		//senderTrans.m01 = 1;
		//senderTrans.m10 = -1;
		//senderTrans.m22 = 1;
		
		//senderTrans.m00 = senderTrans.m11 = senderTrans.m22 = 1;
		
		//Y - Z
		//senderTrans.m01 = 1;
		//senderTrans.m20 = 1;
		//senderTrans.m12 = -1;
		//senderTrans.m33 = 1;
		
		// x-z
		senderTrans.m02 = -1;
		senderTrans.m10 = -1;
		senderTrans.m21 = 1;
		senderTrans.m33 = 1;
		
		rootst.addSubModel(RobotModelStore.generateModel(model.getConnector()),
				senderTrans);
		

		AppearanceData ap = rootst.getApprData();
		ap.setColor(java.awt.Color.gray);
		
		
		return rootst;
	}// end generate(MR_16202)
}