package agdr.robodari.plugin.miniROBOT.comp;

//import org.agdr.robodari.plugin.miniROBOT.lang.*;

import agdr.robodari.comp.*;
import agdr.robodari.plugin.defcomps.*;
import agdr.robodari.plugin.rme.rdpi.*;
import agdr.robodari.plugin.rme.rlztion.RealizationModel;
import agdr.robodari.plugin.rme.rlztion.RealizationNotReadyException;
import agdr.robodari.plugin.rme.rlztion.RealizationStartedException;

public class MR_C2000S implements Controller, SourceEditable, RDObject{
	private final static float[] EP_XCOORDS = new float[]
		{ 0, 4.5f, 4.5f, 0 };
	private final static float[] EP_YCOORDS = new float[]
	    { 0, 0, 4.1f, 4.1f };
	//public final static Language[] LANGUAGES = new Language[]{
	//RoboBasicLanguages.ROBOBASIC_V2_5_FORC2000
	//};
	
	private Sender[] senders = new Sender[12];
	private Receiver[] receivers = new Receiver[14];
	
	private SenderSerial senderSerial;
	private ATMEL_AT89C4051 cpu;
	private MICROCHIP_24LC32 memory;
	private Computer_MRCConnector cmcnctor;
	private PowerConnector1 powercnctor;
	private ElectricPanel epanel;
	private LED1 led;
	private Transistor1 transistor1;
	private Transistor1 transistor2;
	private RobotComponent[] subComponents = new RobotComponent[9];
	
	private RobotComponent parent;
	private Note note;
	private String name;
	
	
	public MR_C2000S(){
		subComponents[0] = senderSerial = new SenderSerial();
		subComponents[1] = cmcnctor = new Computer_MRCConnector();
		subComponents[2] = powercnctor = new PowerConnector1();
		subComponents[3] = cpu = new ATMEL_AT89C4051();
		subComponents[4] = memory = new MICROCHIP_24LC32();
		subComponents[5] = epanel = new ElectricPanel();
		subComponents[6] = led = new LED1();
		subComponents[7] = transistor1 = new Transistor1();
		subComponents[8] = transistor2 = new Transistor1();

		led.setMinusRodLength(0.1);
		led.setPlusRodLength(0.1);
		led.setName("LED");
		((LED1.LED1Receiver)led.getReceivers()[0]).setEnabled(false);
		
		cmcnctor.setName("C1");
		
		epanel.setShape(EP_XCOORDS, EP_YCOORDS);
		epanel.setThickness(0.15f);

		
		for(int i = 0; i < senders.length; i++){
			if(0 <= i && i <= 5){
				senders[i] = new Pin1(this);
			}else if(i == 6)
				senders[i] = new Pin2(this);
			else if(i == 7)
				// 뭔지 모름.
				senders[i] = new Pin3(this);
			else if(i == 8)
				// piezo
				senders[i] = new Pin_Piezo(this);
			else if(i == 9 || i == 10){
				// RS232 송수신
				if(i == 9)
					senders[i] = new Pin_RX(this);
				else if(i == 10)
					senders[i] = new Pin_TX(this);
			}else if(i == 11)
				// 뭔지 모름.
				senders[i] = new Pin_Freq(this);
			
			((ThreePin)senders[i]).setName(String.valueOf(i));
			
			senderSerial.appendSender(senders[i]);
			receivers[i] = (Receiver)senders[i];
		}
		receivers[12] = cmcnctor;
		receivers[13] = powercnctor;
		
		for(RobotComponent eachrc : subComponents)
			eachrc.setParent(this);
		for(Sender eachsd : senders)
			eachsd.setParent(this);
		for(Receiver eachrc : receivers)
			eachrc.setParent(this);
		
		this.note = new Note();
	}// end Constructor()
	
	public CPU getCPU()
	{ return cpu; }
	public MICROCHIP_24LC32 getMemory()
	{ return memory; }
	public PowerConnector1 getPowerConnector()
	{ return powercnctor; }
	public SenderSerial getSenderSerial()
	{ return senderSerial; }
	public Computer_MRCConnector getComputerConnector()
	{ return cmcnctor; }
	public ElectricPanel getPanel()
	{ return epanel; }
	public LED1 getLED()
	{ return led; }
	
	public Transistor1 getTransistor1()
	{ return transistor1; }
	public Transistor1 getTransistor2()
	{ return transistor2; }
	
	
	public Note getNote()
	{ return note; }
	public void setNote(Note note)
	{ this.note = note; }
	public String getName()
	{ return name; }
	public void setName(String name)
	{ this.name = name; }
	
	public RobotComponent getParent()
	{ return parent; }
	public void setParent(RobotComponent p){
		this.parent = p;
	}// end setParent(RobotComponent)
	
	
	public boolean hasSubComponent()
	{ return true; }
	public RobotComponent[] getSubComponents()
	{ return subComponents; }
	public boolean hasReceiver()
	{ return true; }
	public Receiver[] getReceivers()
	{ return receivers; }
	public boolean hasSender()
	{ return true; }
	public Sender[] getSenders()
	{ return senders; }
	
	
	public String toString()
	{ return this.getName(); }
	

	
	/* **********************************************
	 *               RDObject IMPLEMENTATION
	 ************************************************/
	private transient RealizationModel rmodel;
	
	private final static boolean[] rd_fieldconsts = new boolean[0];
	private final static RDObject.Type[] rd_fieldtypes = new RDObject.Type[0];
	private final static String[] rd_fieldnames = new String[0];
	private final static String[] rd_fielddescs = new String[0];
	
	private final static String[] rd_methodnames = new String[0];
	private final static String[] rd_methoddescs = new String[0];
	private final static String[][] rd_methodparamnames = new String[0][0];
	private final static RDObject.Type[][] rd_methodparamtypes = new RDObject.Type[0][0];
	private final static RDObject.Type[] rd_methodtypes = new RDObject.Type[0];
	
	
	public void initRlztion(RealizationModel rm) throws RealizationStartedException{
		if(rmodel != null && !rmodel.getWorld().saidHello())
			throw new RealizationStartedException();
		rmodel = rm;
	}// end initRlztion
	public void disposeRlztion() throws RealizationNotReadyException{
		if(rmodel == null || rmodel.getWorld().saidHello())
			throw new RealizationNotReadyException();
		rmodel = null;
	}// end disposeRlztion
	public void resetRlztion(){}
	
	public java.awt.Component getPropertyViewer(RealizationModel rmodel)
	{ return null; }
	

	public String[] getRDMethodNames()
	{ return rd_methodnames; }
	public RDObject.Type[][] getRDMethodParamTypes()
	{ return rd_methodparamtypes; }
	public RDObject.Type[] getRDMethodTypes()
	{ return rd_methodtypes; }
	public String[] getRDMethodDescs()
	{ return rd_methoddescs; }
	public String[][] getRDMethodParamNames()
	{ return rd_methodparamnames; }

	
	public String[] getRDFieldNames()
	{ return rd_fieldnames; }
	public boolean[] getRDConstants()
	{ return rd_fieldconsts; }
	public RDObject.Type[] getRDFieldTypes()
	{ return rd_fieldtypes; }
	public String[] getRDFieldDescs()
	{ return rd_fielddescs; }
	

	public Object getRDFieldValue(RealizationModel model,
			String fname)
			throws RealizationNotReadyException, RDFieldNotExistException{
		if(rmodel == null || rmodel.getWorld().saidHello())
			throw new RealizationNotReadyException();
		throw new RDFieldNotExistException("", this, fname);
	}// end getRDFieldVAlue
	public void setRDFieldValue(RealizationModel model,
			String fname, Object val)
			throws RealizationNotReadyException, RDFieldNotExistException{
		if(rmodel == null || rmodel.getWorld().saidHello())
			throw new RealizationNotReadyException();
		throw new RDFieldNotExistException("", this, fname);
	}// end setRDFieldValue
	public Object callRDMethod(RealizationModel model,
			String fname, Object[] params)
			throws RealizationNotReadyException, RDMethodNotExistException{
		if(rmodel == null || rmodel.getWorld().saidHello())
			throw new RealizationNotReadyException();
		throw new RDMethodNotExistException("", this, fname, params);
	}// end callRDMethod
	
	
	
	
	
	
	static class Pin1 extends ThreePin{
		private MR_C2000S parent;
		
		public Pin1(MR_C2000S p)
		{ this.parent = p; }
		
		
		public String getCondition()
		{ return "모터, LED만 올 수 있습니다."; }
		public boolean isCompatible(Receiver rcv){
			System.out.println("isCompatible(MR_C2000S, Pin1) : rcv : " + rcv + " , " + rcv.getParent());
			if(rcv.getParent() == this)
				return false;
			else if(rcv == this)
				return false;
			else if(rcv.getParent() instanceof Motor ||
					rcv.getParent() instanceof LED)
				return true;
			return false;
		}// end isCompatible(Receiver, RobotCompnoent)
		public boolean isCompatible(Sender sd){
			if(sd == null) return true;
			// sender 중에서 여기와 연결 가능한 것은 없다!
			return false;
		}// end isCompatible(Sender sd, RobotComponent rc)
		
		public MR_C2000S getParent()
		{ return parent; }
		public void setParent(RobotComponent rc){}
	}// end class Pin1
	static class Pin2 extends ThreePin{
		public final static String CONDITION = "LCD 혹은 LED, 컨트롤러만이" +
				" 가능합니다.";
		private MR_C2000S parent;
		
		public Pin2(MR_C2000S p)
		{ this.parent = p; }
		
		
		public String getCondition()
		{ return CONDITION; }
		public boolean isCompatible(Sender sd){
			if(sd == null) return true;
			else if(sd == this) return false;
			else if(sd.getParent() == this.getParent()) return false;
			
			RobotComponent rc = sd.getParent();
			if(rc instanceof MR_16202 || (rc instanceof MR_C2000S &&
					sd instanceof Pin2) || rc instanceof LED)
				return true;
			return false;
		}// end isCompatible(Sender)
		public boolean isCompatible(Receiver rcv){
			if(rcv == null) return true;
			else if(rcv == this) return false;
			else if(rcv.getParent() == this.getParent()) return false;
			
			RobotComponent rc = rcv.getParent();
			if(rc instanceof MR_16202 || (rc instanceof MR_C2000S &&
					rcv instanceof Pin2) || rc instanceof LED)
				return true;
			return false;
		}// end isCompatible(Receiver)
		
		
		public MR_C2000S getParent()
		{ return parent; }
		public void setParent(RobotComponent rc){}
	}// end class Pin2
	static class Pin3 extends ThreePin{
		// 지금 포트 7이 뭘 말하는지 전혀 모르므로 그냥 핀 하나 생성염
		public final static String CONDITION = ".";
		private MR_C2000S parent;
		
		public Pin3(MR_C2000S p)
		{ this.parent = p; }
		
		
		public String getCondition()
		{ return CONDITION; }
		public boolean isCompatible(Sender sd){
			if(sd == null) return true;
			else if(sd == this) return false;
			else if(sd.getParent() == this.getParent()) return false;
			return false;
		}// end isCompatible(Sender)
		public boolean isCompatible(Receiver rcv){
			if(rcv == null) return true;
			else if(rcv == this) return false;
			else if(rcv.getParent() == this.getParent()) return false;
			return false;
		}// end isCompatible(Receiver)
		
		public MR_C2000S getParent()
		{ return parent; }
		public void setParent(RobotComponent rc){}
	}// end class Pin3
	
	
	static class Pin_Piezo extends ThreePin{
		// 피에조 클래스가 현재 없으므로 isCompatible에선 항상 false.
		public final static String CONDITION = "피에조가 올 수 있는 부분입니다.";
		private MR_C2000S parent;
		
		public Pin_Piezo(MR_C2000S p)
		{ this.parent = p; }
		
		
		public String getCondition()
		{ return CONDITION; }
		public boolean isCompatible(Sender sd){
			if(sd == null) return true;
			else if(sd == this) return false;
			else if(sd.getParent() == this.getParent()) return false;
			return false;
		}// end isCompatible(Sender)
		public boolean isCompatible(Receiver rcv){
			if(rcv == null) return true;
			else if(rcv == this) return false;
			else if(rcv.getParent() == this.getParent()) return false;
			return false;
		}// end isCompatible(Receiver)
		
		public MR_C2000S getParent()
		{ return parent; }
		public void setParent(RobotComponent rc){}
	}// end class Pin4

	
	static class Pin_RX extends ThreePin{
		// 피에조 클래스가 현재 없으므로 isCompatible에선 항상 false.
		public final static String CONDITION = "RS232의 송신 부분으로," +
				" 다른 컨트롤러가 와야 합니다.";
		private MR_C2000S parent;
		
		public Pin_RX(MR_C2000S p)
		{ this.parent = p; }
		
		
		public String getCondition()
		{ return CONDITION; }
		public boolean isCompatible(Sender sd){
			if(sd == null) return true;
			else if(sd == this) return false;
			else if(sd.getParent() == this.getParent()) return false;

			if(sd.getParent() instanceof MR_C2000S && sd instanceof Pin_TX)
				return true;
			return false;
		}// end isCompatible(Sender)
		public boolean isCompatible(Receiver rcv){
			if(rcv == null) return true;
			else if(rcv == this) return false;
			else if(rcv.getParent() == this.getParent()) return false;

			if(rcv.getParent() instanceof MR_C2000S && rcv instanceof Pin_TX)
				return true;
			return false;
		}// end isCompatible(Receiver)
		
		public MR_C2000S getParent()
		{ return parent; }
		public void setParent(RobotComponent rc){}
	}// end class Pin4
	static class Pin_TX extends ThreePin{
		// 피에조 클래스가 현재 없으므로 isCompatible에선 항상 false.
		public final static String CONDITION = "RS232의 송신 부분으로," +
				" 다른 컨트롤러가 와야 합니다.";
		private MR_C2000S parent;
		
		public Pin_TX(MR_C2000S p)
		{ this.parent = p; }
		
		
		public String getCondition()
		{ return CONDITION; }
		public boolean isCompatible(Sender sd){
			if(sd == null) return true;
			else if(sd == this) return false;
			else if(sd.getParent() == this.getParent()) return false;

			if(sd.getParent() instanceof MR_C2000S && sd instanceof Pin_RX)
				return true;
			return false;
		}// end isCompatible(Sender)
		public boolean isCompatible(Receiver rcv){
			if(rcv == null) return true;
			else if(rcv == this) return false;
			else if(rcv.getParent() == this.getParent()) return false;

			if(rcv.getParent() instanceof MR_C2000S && rcv instanceof Pin_RX)
				return true;
			return false;
		}// end isCompatible(Receiver)
		
		public MR_C2000S getParent()
		{ return parent; }
		public void setParent(RobotComponent rc){}
	}// end class Pin4
	

	static class Pin_Freq extends ThreePin{
		// 지금 포트 7이 뭘 말하는지 전혀 모르므로 그냥 핀 하나 생성염
		public final static String CONDITION = ".";
		private MR_C2000S parent;
		
		public Pin_Freq(MR_C2000S p)
		{ this.parent = p; }
		
		
		public String getCondition()
		{ return CONDITION; }
		public boolean isCompatible(Sender sd){
			if(sd == null) return true;
			else if(sd == this) return false;
			else if(sd.getParent() == this.getParent()) return false;
			return false;
		}// end isCompatible(Sender)
		public boolean isCompatible(Receiver rcv){
			if(rcv == null) return true;
			else if(rcv == this) return false;
			else if(rcv.getParent() == this.getParent()) return false;
			return false;
		}// end isCompatible(Receiver)
		
		public MR_C2000S getParent()
		{ return parent; }

		public void setParent(RobotComponent rc){}
	}// end class Pin3
}
