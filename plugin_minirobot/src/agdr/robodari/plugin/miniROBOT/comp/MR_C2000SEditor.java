package agdr.robodari.plugin.miniROBOT.comp;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import agdr.library.lang.*;
import agdr.robodari.comp.*;
import agdr.robodari.edit.*;
import agdr.robodari.gui.*;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.gui.*;
import agdr.robodari.store.*;

public class MR_C2000SEditor extends CompEditor{
	public final static int TABSTATE_CNCTION = 1;
	public final static int TABSTATE_PROP = 2;
	
	
	private JTabbedPane tabbedPane = new JTabbedPane();
	private JPanel propertyPanel;
	
	private SkObject skObject;
	
	private SkObjectViewer objectViewer;
	private JPanel propPanel;
		private JTextField tgNameField;
		
	private NoteEditor noteEditor;
	private JTextField nameField;
	

	public MR_C2000SEditor(SketchEditorInterface manager, Frame parent){
		super(manager, parent, "MR-C2000S Editor");
		__set();
	}// end Constructor(EditorManager)
	public MR_C2000SEditor(SketchEditorInterface manager, Dialog parent){
		super(manager, parent, "MR-C2000S Editor");
		__set();
	}// end Constructor(EditorManager)
	private void __set(){
		
		initConnectionPanel();
		initPropertyPanel();
		
		tabbedPane.addTab("Properties", propertyPanel);
		tabbedPane.setTabPlacement(JTabbedPane.LEFT);
		
		super.getEditorPanel().setLayout(new BorderLayout());
		super.getEditorPanel().add(tabbedPane, BorderLayout.CENTER);
		super.setSize(600, 600);
	}// end __set()
	
	private void initConnectionPanel(){
		objectViewer = new SkObjectViewer();
		objectViewer.setPreferredSize(new Dimension(200, 365));
		
		propPanel = new JPanel();
		propPanel.setLayout(new FlowLayout());
		
		tgNameField = new JTextField();
		tgNameField.setPreferredSize(new Dimension(80, 25));
		tgNameField.setEditable(false);
		propPanel.add(new JLabel("Name : "));
		propPanel.add(tgNameField);
		propPanel.setPreferredSize(new Dimension(140, 35));
	}// end initConnectionPanel
	private void initPropertyPanel(){
		propertyPanel = new JPanel();
		propertyPanel.setLayout(null);
		
		nameField = new JTextField();
		nameField.setBounds(160, 50, 180, 25);
		JLabel label = new JLabel("Name : ");
		label.setBounds(100, 50, 60, 25);
		
		noteEditor = new NoteEditor();
		noteEditor.setBorder(new EtchedBorder());
		noteEditor.setBounds(100, 100, 300, 300);
		
		propertyPanel.add(label);
		propertyPanel.add(nameField);
		propertyPanel.add(noteEditor);
	}// end initPropertyPanel()
	
	
	
	public void setTabState(int state)
	{ tabbedPane.setSelectedIndex(state); }
	
	
	
	
	private void openConnInfoObject(RobotComponent rc){
		if(rc == null){
			objectViewer.setObject(null);
			tgNameField.setText("");
			return;
		}
		SkObject obj = getInterface().getModel().getObject(rc);
		objectViewer.setObject(obj);
		tgNameField.setText(rc.getName());
	}// end openConnInfoObject(RobotComponent)
	
	public SkObject getObject()
	{ return skObject; }
	public void startEditing(SkObject obj){
		if(!(obj.getRobotComponent() instanceof Controller)) return;
		
		this.skObject = obj;
		
		Controller controller =(Controller)skObject.getRobotComponent();
		
		RobotComponent rcomp = obj.getRobotComponent();
		nameField.setText(rcomp.getName());
		if(rcomp.getNote() == null)
			rcomp.setNote(new Note());
		noteEditor.open(rcomp.getNote());
	}// end startEditing(SkObject)
	public void finishEditing(){
		Controller controller =(Controller)skObject.getRobotComponent();
		
		RobotComponent rcomp = skObject.getRobotComponent();
		rcomp.setNote(noteEditor.getNote());
		rcomp.setName(nameField.getText());
		getInterface().refreshObject(skObject);
		skObject = null;
		
		this.setVisible(false);
		getInterface().setCurrentCompEditor(null);
	}// end finishEditing()
}

