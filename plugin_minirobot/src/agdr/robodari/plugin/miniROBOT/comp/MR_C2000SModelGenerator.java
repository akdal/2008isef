package agdr.robodari.plugin.miniROBOT.comp;

import javax.vecmath.*;

import agdr.robodari.comp.info.*;
import agdr.robodari.comp.model.*;
import agdr.robodari.plugin.defcomps.*;
import agdr.robodari.plugin.rme.store.*;
import agdr.robodari.store.*;

public class MR_C2000SModelGenerator implements ModelGenerator<MR_C2000S>{
	public RobotCompModel generate(MR_C2000S model)
			throws ModelNotRegisteredException, Exception{
		if(!RobotModelStore.isRegistered(SenderSerial.class))
			throw new ModelNotRegisteredException(SenderSerial.class.toString());
		else if(!RobotModelStore.isRegistered(ATMEL_AT89C4051.class))
			throw new ModelNotRegisteredException(ATMEL_AT89C4051.class.toString());
		else if(!RobotModelStore.isRegistered(ElectricPanel.class))
			throw new ModelNotRegisteredException(ElectricPanel.class.toString());
		else if(!RobotModelStore.isRegistered(MICROCHIP_24LC32.class))
			throw new ModelNotRegisteredException(MICROCHIP_24LC32.class.toString());
		else if(!RobotModelStore.isRegistered(Computer_MRCConnector.class))
			throw new ModelNotRegisteredException(Computer_MRCConnector.class.toString());
		else if(!RobotModelStore.isRegistered(PowerConnector1.class))
			throw new ModelNotRegisteredException(PowerConnector1.class.toString());

		//Info info = RobotInfoStore.getInfo(model);
		//double weight = ((Number)info.get(InfoKeys.WEIGHT)).doubleValue();
		RobotCompModel structure = new RobotCompModel();
		structure.setGroup(true);
		AppearanceData ap = structure.getApprData();
		ap.setColor(new java.awt.Color(66, 66, 189));
		
		Matrix4f eptrans = new Matrix4f();
		eptrans.m00 = eptrans.m11 = eptrans.m22 = eptrans.m33 = 1;
		structure.addSubModel(RobotModelStore.generateModel(model.getPanel()), eptrans);
		//volumes[0] = structure.getSubModel(0).getVolume();
		
		Matrix4f cintrans = new Matrix4f();
		cintrans.m01 = -1;
		cintrans.m10 = 1;
		cintrans.m22 = 1;
		cintrans.m33 = 1;
		cintrans.m03 = 0.9f;
		cintrans.m13 = 0f;
		structure.addSubModel(RobotModelStore.generateModel(model.getComputerConnector()), cintrans);
		//volumes[1] = structure.getSubModel(1).getVolume();
		
		Matrix4f serialtrans = new Matrix4f();
		serialtrans.m00 = 1;
		serialtrans.m11 = 1;
		serialtrans.m22 = 1;
		serialtrans.m33 = 1;
		serialtrans.m03 = 0.7f;
		serialtrans.m13 = 4.1f - 0.75f;
		serialtrans.m23 = 0.05f;
		structure.addSubModel(RobotModelStore.generateModel(model.getSenderSerial()), serialtrans);
		//volumes[2] = structure.getSubModel(2).getVolume();
		
		Matrix4f cputrans = new Matrix4f();
		cputrans.m01 = 1;
		cputrans.m10 = 1;
		cputrans.m22 = 1;
		cputrans.m33 = 1;
		cputrans.m03 = 0.4f;
		cputrans.m13 = 2.55f;
		cputrans.m23 = 0.4f;
		structure.addSubModel(RobotModelStore.generateModel(model.getCPU()), cputrans);
		//volumes[3] = structure.getSubModel(3).getVolume();
		
		Matrix4f memorytrans = new Matrix4f();
		memorytrans.m00 = 1;
		memorytrans.m11 = 1;
		memorytrans.m22 = 1;
		memorytrans.m33 = 1;
		memorytrans.m03 = 3.3f;
		memorytrans.m13 = 3.1f;
		memorytrans.m23 = 0.4f;
		structure.addSubModel(RobotModelStore.generateModel(model.getMemory()), memorytrans);
		//volumes[4] = structure.getSubModel(4).getVolume();
		
		Matrix4f ledtrans = new Matrix4f();
		ledtrans.m00 = 1;
		ledtrans.m11 = 1;
		ledtrans.m22 = 1;
		ledtrans.m33 = 1;
		ledtrans.m03 = 3.45f;
		ledtrans.m13 = 1.2f;
		ledtrans.m23 = 0.05f;
		structure.addSubModel(RobotModelStore.generateModel(model.getLED()), ledtrans);
		//volumes[5] = structure.getSubModel(5).getVolume();
		
		Matrix4f pstrans = new Matrix4f();
		pstrans.m00 = 1;
		pstrans.m11 = 1;
		pstrans.m22 = 1;
		pstrans.m33 = 1;
		pstrans.m03 = 3.6f;
		pstrans.m13 = 0.7f;
		pstrans.m23 = 0;
		structure.addSubModel(RobotModelStore.generateModel(model.getPowerConnector()),
				pstrans);
		//volumes[6] = structure.getSubModel(6).getVolume();
		
		Matrix4f ts1trans = new Matrix4f();
		ts1trans.m01 = 1;
		ts1trans.m10 = -1;
		ts1trans.m22 = 1;
		ts1trans.m33 = 1;
		ts1trans.m03 = 1.05f;
		ts1trans.m13 = 0.6f;
		ts1trans.m23 = 0.2f;
		structure.addSubModel(RobotModelStore.generateModel(model.getTransistor1()),
				ts1trans);
		//volumes[7] = structure.getSubModel(7).getVolume();
		
		Matrix4f ts2trans = new Matrix4f();
		ts2trans.m01 = 1;
		ts2trans.m10 = -1;
		ts2trans.m22 = 1;
		ts2trans.m33 = 1;
		ts2trans.m03 = 1.45f;
		ts2trans.m13 = 0.6f;
		ts2trans.m23 = 0.2f;
		structure.addSubModel(RobotModelStore.generateModel(model.getTransistor2()),
				ts2trans);
			
		return structure;
	}// end generate(MR_C2000S)
}
