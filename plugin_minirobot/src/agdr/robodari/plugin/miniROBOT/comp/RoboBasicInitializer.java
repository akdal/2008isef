package agdr.robodari.plugin.miniROBOT.comp;

import java.io.*;
import java.net.*;
import javax.vecmath.*;

import agdr.library.math.MathUtility;
import agdr.robodari.comp.*;
import agdr.robodari.comp.model.FileModelGenerator;
import agdr.robodari.comp.model.ModelGenerator;
import agdr.robodari.comp.model.PhysicalData;
import agdr.robodari.comp.model.RobotCompModel;
import agdr.robodari.plugin.miniROBOT.comp.info.*;
import agdr.robodari.plugin.miniROBOT.rlztion.*;
import agdr.robodari.plugin.rme.store.*;
import agdr.robodari.store.*;

public class RoboBasicInitializer extends Initializer{
	public RoboBasicInitializer(){}
	
	
	
	public void initialize() throws Exception{
		RobotComponentStore.registerComponent(ATMEL_AT89C4051.class);
		RobotComponentStore.registerComponent(MR_16202.class);
		RobotComponentStore.registerComponent(Computer_MRCConnector.class);
		RobotComponentStore.registerComponent(MICROCHIP_24LC32.class);
		RobotComponentStore.registerComponent(MR_C2000S.class);
		RobotComponentStore.registerComponent(HS_311.class);
		
		
		// info
		RobotInfoStore.registerInfo(MR_C2000S.class, new MR_C2000SInfoGenerator());
		RobotInfoStore.registerInfo(ATMEL_AT89C4051.class, new
				ATMEL_AT89C4051InfoGenerator());
		RobotInfoStore.registerInfo(MICROCHIP_24LC32.class,
				new MICROCHIP_24LC32InfoGenerator());
		RobotInfoStore.registerInfo(MR_16202.class, new MR_16202InfoGenerator());
		RobotInfoStore.registerInfo(HS_311.class, new HS_311InfoGenerator());
		
		
		// rlztion
		RealizationCompStore.registerGraphGenerator(HS_311GraphGenerator
				.INSTANCE);
		RealizationCompStore.registerGraphGenerator(
				MICROCHIP_24LC32GraphGenerator.INSTANCE);
		RealizationCompStore.registerGraphGenerator(
				ATMEL_AT89C4051GraphGenerator.INSTANCE);
		RealizationCompStore.registerGraphGenerator(
				MR_C2000SGraphGenerator.INSTANCE);
		RealizationCompStore.registerGraphGenerator(
				MR_16202GraphGenerator.INSTANCE);
		RealizationCompStore.registerGraphGenerator(
				Computer_MRCConnectorGraphGenerator.INSTANCE);
		
		
		// model
		URL prochip_24lc32 = RoboBasicInitializer.class.
				getResource("models/24LC32A.obj");
		URL at89c4051_24 = RoboBasicInitializer.class.
				getResource("models/AT89C4051-24.obj");
		
		final FileModelGenerator _24lc32fmg = new FileModelGenerator(new InputStreamReader(
				prochip_24lc32.openStream()));
		ModelGenerator<MICROCHIP_24LC32> _24lc32mg = new ModelGenerator<MICROCHIP_24LC32>(){
			public RobotCompModel generate(MICROCHIP_24LC32 model) throws Exception{
				RobotCompModel str = _24lc32fmg.generate(model);
				
				PhysicalData pd = str.getPhysicalData();
				Point3f[] ps = str.getPoints();
				int[][] faces = str.getFaces();
				pd.mass = 1.5f;
				pd.bounds = null;
				pd.centerMassPoint = MathUtility.getCenterMassPoint(ps, faces, pd.centerMassPoint);
				pd.volume = MathUtility.getVolume(ps, faces);
				pd.inertia = new Matrix3f();
				MathUtility.addInertiaTensor(ps, faces,
						pd.mass,
						pd.centerMassPoint.x,
						pd.centerMassPoint.y,
						pd.centerMassPoint.z, pd.inertia);
				return str;
			}// end generate(.._)
		};
		
		final FileModelGenerator _at89c4051fmg = new FileModelGenerator(new InputStreamReader(
				at89c4051_24.openStream()));
		ModelGenerator<ATMEL_AT89C4051> _at89c4051mg = new ModelGenerator<ATMEL_AT89C4051>(){
			public RobotCompModel generate(ATMEL_AT89C4051 model) throws Exception{
				RobotCompModel str = _at89c4051fmg.generate(model);
				PhysicalData pd = str.getPhysicalData();
				Point3f[] ps = str.getPoints();
				int[][] faces = str.getFaces();
				pd.mass = 3;
				pd.bounds = null;
				pd.centerMassPoint = MathUtility.getCenterMassPoint(ps, faces, pd.centerMassPoint);
				pd.volume = MathUtility.getVolume(ps, faces);
				pd.inertia = new Matrix3f();
				MathUtility.addInertiaTensor(ps, faces,
						pd.mass,
						pd.centerMassPoint.x,
						pd.centerMassPoint.y,
						pd.centerMassPoint.z, pd.inertia);
				return str;
			}// end generate(.._)
		};
		
		
		RobotModelStore.registerModel(MR_16202.class,
				new MR_16202_ModelGenerator());
		RobotModelStore.registerModel(MICROCHIP_24LC32.class, _24lc32mg);
		RobotModelStore.registerModel(ATMEL_AT89C4051.class, _at89c4051mg);
		RobotModelStore.registerModel(Computer_MRCConnector.class,
				new Computer_MRCConnectorModelGenerator());
		RobotModelStore.registerModel(MR_C2000S.class,
				new MR_C2000SModelGenerator());
		RobotModelStore.registerModel(HS_311.class,
				new HS_311ModelGenerator());
		
		
		// comp editor
		RobotCompEditorStore.register(MR_C2000S.class, MR_C2000SEditor.class);
		RobotCompEditorStore.register(HS_311.class, HS_311Editor.class);
	}// end initInfos()
}
