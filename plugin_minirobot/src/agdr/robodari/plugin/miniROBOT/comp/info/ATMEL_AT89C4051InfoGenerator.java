package agdr.robodari.plugin.miniROBOT.comp.info;

import java.util.*;

import agdr.robodari.comp.info.*;
import agdr.robodari.plugin.miniROBOT.comp.*;
import static agdr.robodari.comp.info.CPUInfoKeys.*;
import static agdr.robodari.comp.info.InfoKeys.*;

public class ATMEL_AT89C4051InfoGenerator implements
		InfoGenerator<ATMEL_AT89C4051>{
	public Info generateInfo(ATMEL_AT89C4051 model){
		Info hash = new Info(model);
		hash.put(NAME, "AT89C4051");
		hash.put(BUSINESS, "(R) ATMEL");
		hash.put(ISOUTEROBJECT, new Boolean("false"));
		hash.put(WEIGHT, new Integer(0));
		hash.put(MINPROPERVOLTAGE, new Float(2.7));
		hash.put(MAXPROPERVOLTAGE, new Float(6.0));
		return hash;
	}// end generate(ATMEL_AT89C4051)
}
