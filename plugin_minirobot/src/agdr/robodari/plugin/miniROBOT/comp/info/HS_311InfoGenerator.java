package agdr.robodari.plugin.miniROBOT.comp.info;

import java.util.*;

import agdr.robodari.comp.info.*;
import agdr.robodari.plugin.miniROBOT.comp.*;
import static agdr.robodari.comp.info.InfoKeys.*;
import static agdr.robodari.comp.info.ServoMotorInfoKeys.*;

public class HS_311InfoGenerator implements InfoGenerator<HS_311>{
	public Info generateInfo(HS_311 model){
		Info infos = new Info(model);
		infos.put(NAME, "HS-311 Standard");
		infos.put(BUSINESS, "HITEC");
		infos.put(ISOUTEROBJECT, new Boolean("false"));
		
		infos.put(WEIGHT, new Float(43));
		infos.put(TORQUE, new Float(0.294));
		infos.put(WIDTH, new Float(4));
		infos.put(MAXANGLE, new Float(180));
		infos.put(MINPROPERVOLTAGE, new Float(4.8));
		infos.put(MAXPROPERVOLTAGE, new Float(6.0));
		infos.put(SPEED, new Float(315.7947));
		
		return infos;
	}// end generateInfo(HS_311)\
}
