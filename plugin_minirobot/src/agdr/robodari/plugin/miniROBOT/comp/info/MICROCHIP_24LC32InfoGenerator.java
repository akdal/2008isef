package agdr.robodari.plugin.miniROBOT.comp.info;

import java.util.*;

import agdr.robodari.comp.info.*;
import agdr.robodari.plugin.miniROBOT.comp.*;
import static agdr.robodari.comp.info.InfoKeys.*;
import static agdr.robodari.comp.info.MemoryInfoKeys.*;

public class MICROCHIP_24LC32InfoGenerator implements InfoGenerator<MICROCHIP_24LC32>{
	public Info generateInfo(MICROCHIP_24LC32 model){
		Info infos = new Info(model);
		infos.put(NAME, "24LC32");
		infos.put(BUSINESS, "MICROCHIP");
		infos.put(WEIGHT, new Float(0));
		infos.put(ISOUTEROBJECT, new Boolean(false));
		infos.put(MINPROPERVOLTAGE, new Float(2.4f));
		infos.put(MAXPROPERVOLTAGE, new Float(6.0f));
		infos.put(CAPACITY, new Integer(32 * 1024));
		return infos;
	}// end generateInfo(MICROCHIP_24LC32)
}
