package agdr.robodari.plugin.miniROBOT.comp.info;

import java.util.*;

import agdr.robodari.comp.info.*;
import agdr.robodari.plugin.miniROBOT.comp.*;
import static agdr.robodari.comp.info.DisplayInfoKeys.*;
import static agdr.robodari.comp.info.InfoKeys.*;

public class MR_16202InfoGenerator implements InfoGenerator<MR_16202>{
	public Info generateInfo(MR_16202 model){
		Info infos = new Info(model);
		infos.put(NAME, "MR-16202");
		infos.put(BUSINESS, "JE-AN Electronics");
		infos.put(ISOUTEROBJECT, new Boolean(false));
		
		infos.put(WEIGHT, new Float(100));
		infos.put(HEIGHT, new Float(15));
		infos.put(WIDTH, new Float(80));
		infos.put(LENGTH, new Float(36));
		infos.put(DOTSIZEWIDTH, new Float(0.55f));
		infos.put(DOTSIZEHEIGHT, new Float(0.5f));
		infos.put(MINPROPERVOLTAGE, new Float(4.0f));
		infos.put(MAXPROPERVOLTAGE, new Float(6));
		return infos;
	}// end generateInfo(MR_16202)
}
