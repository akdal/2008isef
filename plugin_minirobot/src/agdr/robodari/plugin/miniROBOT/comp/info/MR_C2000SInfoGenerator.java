package agdr.robodari.plugin.miniROBOT.comp.info;

import java.util.*;

import agdr.robodari.comp.info.*;
import agdr.robodari.plugin.miniROBOT.comp.*;
import static agdr.robodari.comp.info.ControllerInfoKeys.*;
import static agdr.robodari.comp.info.InfoKeys.*;

public class MR_C2000SInfoGenerator implements InfoGenerator<MR_C2000S>{
	public Info generateInfo(MR_C2000S model){
		Info infos = new Info(model);
		infos.put(NAME, "MR-C2000S");
		infos.put(BUSINESS, "MINIROBOT");
		infos.put(ISOUTEROBJECT, new Boolean(false));
		
		infos.put(MINPROPERVOLTAGE, new Float(3.3f));
		infos.put(MAXPROPERVOLTAGE, new Float(6.0f));
		infos.put(WEIGHT, new Float(16));
		
		infos.put(WIDTH, new Float(40));
		infos.put(LENGTH, new Float(40));
		infos.put(HEIGHT, new Float(13));
		
		return infos;
	}// end generateInfo(MR_C2000S model)
}
