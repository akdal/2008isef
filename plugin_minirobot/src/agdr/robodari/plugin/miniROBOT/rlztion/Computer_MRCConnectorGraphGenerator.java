package agdr.robodari.plugin.miniROBOT.rlztion;

import javax.vecmath.Matrix4f;


import agdr.robodari.comp.RobotComponent;
import agdr.robodari.comp.model.RobotCompModel;
import agdr.robodari.plugin.miniROBOT.comp.*;
import agdr.robodari.plugin.rme.edit.SkObject;
import agdr.robodari.plugin.rme.rlztion.*;

public class Computer_MRCConnectorGraphGenerator implements GraphGenerator{
	public final static Computer_MRCConnectorGraphGenerator INSTANCE = 
		new Computer_MRCConnectorGraphGenerator();
	
	public int getGeneratorType()
	{ return GraphGenerator.SPECFC_OBJECT; }
	public boolean editable(RobotCompModel str, SkObject skobj,
			RealizationMap smap, World world){
		if(skobj == null)
			return false;
		RobotComponent rc = skobj.getRobotComponent();
		return rc != null && rc instanceof Computer_MRCConnector && skobj != null;
	}// end editable
	
	public RealizationMap.Key generateGraph(
			SkObject obj,
			Matrix4f orgtrans,
			RealizationMap smap, World world){
		Matrix4f trans = new Matrix4f(orgtrans);
		trans.mul(obj.getTransform());
		return smap.addElement(obj.getModel(), trans, obj, true);
	}// end generateGraph(TemporaryGraph, int, RobotComponent,
	//                    StructureMap, World)
}
