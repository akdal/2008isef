package agdr.robodari.plugin.miniROBOT.rlztion;

import javax.vecmath.*;

import agdr.library.math.*;
import agdr.robodari.appl.*;
import agdr.robodari.comp.*;
import agdr.robodari.comp.model.RobotCompModel;
import agdr.robodari.plugin.defcomps.rlztion.*;
import agdr.robodari.plugin.miniROBOT.comp.*;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.rlztion.*;
import agdr.robodari.plugin.rme.store.*;

public class HS_311GraphGenerator implements GraphGenerator{
	public final static HS_311GraphGenerator INSTANCE = 
		new HS_311GraphGenerator();
	
	public int getGeneratorType()
	{ return GraphGenerator.SPECFC_OBJECT; }
	public boolean editable(RobotCompModel st, SkObject skobj,
			RealizationMap map, World world){
		if(skobj == null) return false;
		RobotComponent rc = skobj.getRobotComponent();
		return rc != null && rc.getClass() == HS_311.class && skobj != null;
	}// end editable(Structure, RobotComponent,
	//                   SkObject, StructureMap, World)
	
	public RealizationMap.Key generateGraph(
			SkObject obj, 
			Matrix4f orgtrans,
			RealizationMap smap, World world){
		HS_311 model = (HS_311)obj.getRobotComponent();
		RobotCompModel str = obj.getModel();
		
		Matrix4f objtrans = new Matrix4f();
		MathUtility.fastMul(orgtrans, obj.getTransform(), objtrans);
		BugManager.printf("HS_311GG : objtrans : \n%s", objtrans);

		// structure
		// [0] : body
		// [1] : disk
		// [2] : receiver
		// [3] : attached object

		AxisHolder axisHolder = null;
		for(int i = 0; i < smap.getForceGeneratorCount(); i++)
			if(smap.getForceGenerator(i) instanceof AxisHolder){
				axisHolder = (AxisHolder)smap.getForceGenerator(i);
				break;
			}
		if(axisHolder == null)
			smap.addForceGenerator(axisHolder = new AxisHolder());
		
		Matrix4f trans0 = new Matrix4f();
		MathUtility.fastMul(objtrans, str.getTransform(str.getSubModel(0)), trans0);
		RealizationMap.Key bodyStrKey = smap.addElement(str.getSubModel(0), trans0, obj, true);
		//System.out.println("HS_311GGen : " + focus + " : " + rcomp.getName() + " body");
		
		Matrix4f trans1 = new Matrix4f();
		MathUtility.fastMul(objtrans, str.getTransform(str.getSubModel(0)), trans1);
		RealizationMap.Key diskid = smap.addElement(str.getSubModel(1), trans1, obj, null, true);
		//System.out.println("HS_311GGen : " + diskid + " : " + rcomp.getName() + " disk");
		
		if(model.getAttachedObject() != null){
			Matrix4f aotrans = new Matrix4f();
			SkObject aobj = model.getAttachedObject();
			aotrans.set(objtrans);
			RealizationMap.Key aokey;
			//System.out.println("HS_311GGen : " + aoid + " : " + rcomp.getName() + " aobj");
			
			if(aobj.isGroup())
				aokey = RealizationCompStore.generateGraph(
						aobj.getIntegratedModel(),
						aobj, 
						aotrans,smap, world);
			else
				aokey = RealizationCompStore.generateGraph(
						aobj.getModel(),
						aobj,
						aotrans, smap, world);
			
			smap.mergeElements(diskid, aokey);
		}
		
		
		{
			Point3f axispos1 = new Point3f(), axispos2 = new Point3f();
			
			axispos1.x = 3;
			axispos1.y = 1;
			axispos1.z = 0;
			
			axispos2.x = 3;
			axispos2.y = 1;
			axispos2.z = 4;

			axisHolder.add(bodyStrKey, diskid,
					orgtrans.m00 * axispos1.x + orgtrans.m01 * axispos1.y + orgtrans.m02 * axispos1.z + orgtrans.m03,
					orgtrans.m10 * axispos1.x + orgtrans.m11 * axispos1.y + orgtrans.m12 * axispos1.z + orgtrans.m13,
					orgtrans.m20 * axispos1.x + orgtrans.m21 * axispos1.y + orgtrans.m22 * axispos1.z + orgtrans.m23,

					orgtrans.m00 * axispos2.x + orgtrans.m01 * axispos2.y + orgtrans.m02 * axispos2.z + orgtrans.m03,
					orgtrans.m10 * axispos2.x + orgtrans.m11 * axispos2.y + orgtrans.m12 * axispos2.z + orgtrans.m13,
					orgtrans.m20 * axispos2.x + orgtrans.m21 * axispos2.y + orgtrans.m22 * axispos2.z + orgtrans.m23);
		}
		//System.out.println("HS_311GraphGenerator : CONNECT : " + focus + " , " + diskid + " , " + axisconnid);
		
		model.setBodyStructureKey(bodyStrKey);
		model.setDiskStructureKey(diskid);
		return bodyStrKey;
	}// end generateGraph(TemporaryGraph, int, StructureMap, World)
}
