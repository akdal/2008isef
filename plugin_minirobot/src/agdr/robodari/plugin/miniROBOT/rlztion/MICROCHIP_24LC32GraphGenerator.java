package agdr.robodari.plugin.miniROBOT.rlztion;

import javax.vecmath.*;

import agdr.robodari.comp.RobotComponent;
import agdr.robodari.comp.model.RobotCompModel;
import agdr.robodari.plugin.miniROBOT.comp.*;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.rlztion.*;


public class MICROCHIP_24LC32GraphGenerator implements GraphGenerator{
	public final static MICROCHIP_24LC32GraphGenerator INSTANCE = 
		new MICROCHIP_24LC32GraphGenerator();
	
	public int getGeneratorType()
	{ return GraphGenerator.SPECFC_OBJECT; }
	public boolean editable(RobotCompModel str, SkObject skobj,
			RealizationMap smap, World world){
		if(skobj == null)
			return false;
		RobotComponent rc = skobj.getRobotComponent();
		return rc != null && rc instanceof MICROCHIP_24LC32 && skobj != null;
	}// end editable
	public RealizationMap.Key generateGraph (
			SkObject obj,
			Matrix4f orgtrans,
			RealizationMap smap, World world){
		Matrix4f trans = new Matrix4f(orgtrans);
		trans.mul(obj.getTransform());
		return smap.addElement(obj.getModel(), trans, obj, true);
	}// end generateGraph
}
