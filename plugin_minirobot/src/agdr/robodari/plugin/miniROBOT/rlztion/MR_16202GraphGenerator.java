package agdr.robodari.plugin.miniROBOT.rlztion;

import javax.vecmath.Matrix4f;

import agdr.robodari.comp.RobotComponent;
import agdr.robodari.comp.info.*;
import agdr.robodari.comp.model.*;
import agdr.robodari.plugin.miniROBOT.comp.*;
import agdr.robodari.plugin.rme.edit.SkObject;
import agdr.robodari.plugin.rme.rlztion.*;
import agdr.robodari.plugin.rme.store.*;
import agdr.robodari.store.*;

public class MR_16202GraphGenerator implements GraphGenerator{
	public final static MR_16202GraphGenerator INSTANCE = 
		new MR_16202GraphGenerator();
	
	public int getGeneratorType()
	{ return GraphGenerator.SPECFC_OBJECT; }
	public boolean editable(RobotCompModel str, SkObject skobj,
			RealizationMap smap, World world){
		if(skobj == null)
			return false;
		RobotComponent rc = skobj.getRobotComponent();
		return rc != null && rc instanceof MR_16202 && skobj != null;
	}// end editable
	
	public RealizationMap.Key generateGraph(
			SkObject obj,
			Matrix4f orgtrans,
			RealizationMap smap,
			World world){
		
		Matrix4f objtrans = new Matrix4f();
		if(orgtrans != null)
			objtrans.set(orgtrans);
		else
			objtrans.m00 = objtrans.m11 = objtrans.m22 = objtrans.m33 = 1;
		objtrans.mul(obj.getTransform());
		
		MR_16202 model = (MR_16202)obj.getRobotComponent();
		RobotCompModel mainStr = obj.getModel();
		
		// Case
		RobotCompModel str = mainStr.getSubModel(0);
		Matrix4f trans = new Matrix4f(objtrans);
		trans.mul(mainStr.getTransform(str));
		RealizationMap.Key casekey = smap.addElement(str, trans, obj, true);

		// big panel
		str = mainStr.getSubModel(1);
		trans = new Matrix4f(objtrans);
		trans.mul(mainStr.getTransform(str));
		RealizationMap.Key newid1 = smap.addElement(str, trans, obj, true);
		
		// small panel
		str = mainStr.getSubModel(2);
		trans = new Matrix4f(objtrans);
		trans.mul(mainStr.getTransform(str));
		RealizationMap.Key newid2 = smap.addElement(str, trans, obj, true);
		
		// LCD_Controller Connector
		str = mainStr.getSubModel(3);
		trans = new Matrix4f(objtrans);
		trans.mul(mainStr.getTransform(str));
		RealizationMap.Key newid3 = smap.addElement(str, trans, obj, true);

		// Controller
		str = mainStr.getSubModel(4);
		trans = new Matrix4f(objtrans);
		trans.mul(mainStr.getTransform(str));
		RealizationMap.Key newid4 = smap.addElement(str, trans, obj, true);
		
		// Connector
		str = mainStr.getSubModel(5);
		trans = new Matrix4f(objtrans);
		trans.mul(mainStr.getTransform(str));
		RealizationMap.Key newid5 = smap.addElement(str, trans, obj, true);
		
		smap.mergeElements(casekey, newid1, newid2, newid3, newid4, newid5);
		return casekey;
	}// end generateGraph(TemporaryMap, int, RobotComponent,
	//                    RealizationMap, World)
}
