package agdr.robodari.plugin.miniROBOT.rlztion;

import javax.vecmath.*;


import agdr.robodari.comp.*;
import agdr.robodari.comp.model.RobotCompModel;
import agdr.robodari.plugin.miniROBOT.comp.*;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.rlztion.*;

public class MR_C2000SGraphGenerator implements GraphGenerator{
	public final static MR_C2000SGraphGenerator INSTANCE = 
		new MR_C2000SGraphGenerator();
	
	public int getGeneratorType()
	{ return GraphGenerator.SPECFC_OBJECT; }
	public boolean editable(RobotCompModel str, SkObject obj,
			RealizationMap smap, World world){
		if(obj == null)
			return false;
		RobotComponent rc = obj.getRobotComponent();
		return obj != null && rc != null && rc instanceof MR_C2000S;
	}// end editable
	
	public RealizationMap.Key generateGraph(
			SkObject obj,
			Matrix4f orgtrans,
			RealizationMap smap, World world){
		//System.out.println("MR_C2000SGraphGEnerator : generateGraph : focus : " + focus);
		MR_C2000S model = (MR_C2000S)obj.getRobotComponent();
		Matrix4f objtrans = new Matrix4f();
		
		if(orgtrans != null)
			objtrans.set(orgtrans);
		else
			objtrans.m00 = objtrans.m11 = objtrans.m22 = objtrans.m33 = 1;
		objtrans.mul(obj.getTransform());
		//System.out.println("objtrans : " + objtrans);
		
		RobotCompModel mainStr = obj.getModel();

		// 1. panel
		RobotCompModel str = mainStr.getSubModel(0);
		Matrix4f trans = new Matrix4f(objtrans);
		trans.mul(mainStr.getTransform(str));
		RealizationMap.Key panelkey = smap.addElement(str, trans, obj, true);
		
		// 2. computer connector
		str = mainStr.getSubModel(1);
		trans = new Matrix4f(objtrans);
		trans.mul(mainStr.getTransform(str));
		RealizationMap.Key newid1 = smap.addElement(str, trans, obj, true);
		
		// 3. sender serial
		str = mainStr.getSubModel(2);
		trans = new Matrix4f(objtrans);
		trans.mul(mainStr.getTransform(str));
		RealizationMap.Key newid2 = smap.addElement(str, trans, obj, true);
		
		// 4. cpu
		str = mainStr.getSubModel(3);
		trans = new Matrix4f(objtrans);
		trans.mul(mainStr.getTransform(str));
		RealizationMap.Key newid3 = smap.addElement(str, trans, obj, true);
		
		// 5. memory
		str = mainStr.getSubModel(4);
		trans = new Matrix4f(objtrans);
		trans.mul(mainStr.getTransform(str));
		RealizationMap.Key newid4 = smap.addElement(str, trans, obj, true);
		
		// 6. led
		str = mainStr.getSubModel(5);
		trans = new Matrix4f(objtrans);
		trans.mul(mainStr.getTransform(str));
		RealizationMap.Key newid5 = smap.addElement(str, trans, obj, true);
		
		// 7. power connector
		str = mainStr.getSubModel(6);
		trans = new Matrix4f(objtrans);
		trans.mul(mainStr.getTransform(str));
		RealizationMap.Key newid6 = smap.addElement(str, trans, obj, true);
		
		// 8. transistor 1
		str = mainStr.getSubModel(7);
		trans = new Matrix4f(objtrans);
		trans.mul(mainStr.getTransform(str));
		RealizationMap.Key newid7 = smap.addElement(str, trans, obj, true);
		
		// 9. transistor 2
		str = mainStr.getSubModel(8);
		trans = new Matrix4f(objtrans);
		trans.mul(mainStr.getTransform(str));
		RealizationMap.Key newid8 = smap.addElement(str, trans, obj, true);
		
		smap.mergeElements(panelkey, newid1, newid2, newid3, newid4, newid5, newid6, newid7, newid8);
		return panelkey;
	}// end generateGrpah(TemporaryGraph, int, RobotComponent, ...)
}
