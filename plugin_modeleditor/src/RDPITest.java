import java.io.PrintStream;

import agdr.robodari.plugin.rme.rdpi.*;
import agdr.robodari.plugin.rme.rlztion.*;

public class RDPITest implements RDProcessor{
	private boolean running = false;
	private boolean disposed = false;
	
	public void start(RDEntity entity, RDObjectManager manager, RDConnManager cmng, String controllerName,
			Object[] param, Clock clock, PrintStream rdout, PrintStream rderr)
			throws RDMethodNotExistException, RealizationNotReadyException{
		running = true;
		disposed = false;
		rdout.println("Hello, World!");
		
		long start = clock.getMilliSecond();
		
		RDObject controller = manager.getRDObject(entity, controllerName);
		// obj는 모터이다.
		RDObject obj = cmng.getConn(cmng.getReceivers(controller)[0]);
		manager.runMethod(entity, obj, "setDir", new Object[]{ new Integer(1) });
		
		long end = clock.getMilliSecond();
		while(end - start < 3000 && !disposed){
			try{
				Thread.sleep((start + 3000) - end);
			}catch(InterruptedException e){}
			end = clock.getMilliSecond();
		}
			
		running = false;
	}// end processRobodari(...)
	public void resume(RDEntity e, RDObjectManager om, RDConnManager cm, String ctr){
		
	}// end rsume
	public void pause(RDEntity e, RDObjectManager om, RDConnManager cm, String ctr){
		
	}// end rsume
	
	public boolean isRunning(RDEntity ent, RDObjectManager omng, RDConnManager cmng, String ctrlerName)
	{ return running; }
	public void dispose(RDEntity ent, RDObjectManager omng, RDConnManager cmng, String cname){disposed = true;}
}
