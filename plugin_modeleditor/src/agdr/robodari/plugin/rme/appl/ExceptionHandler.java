package agdr.robodari.plugin.rme.appl;

import javax.swing.*;
import java.io.*;

import agdr.robodari.appl.*;
import agdr.robodari.comp.*;
import agdr.robodari.comp.info.*;
import agdr.robodari.store.RobotInfoStore;

public class ExceptionHandler {
	public final static String MODELNOTRGSTRD = "Not registered model : ";
	public final static String CANNOTCREATECOMPONENT = "Cannot create component.";
	public final static String WRONGCOMPONENT = "Wrong component. Cannot generate an appropriate library : ";
	public final static String CANNOTGETINFO = "Cannot get information of this component. The item will be disabled : ";
	public final static String ONLYDECIMALACCEPTED = "Only number allowed";
	public final static String SKETCHNOTOPENED = "Sketch not opened.";
	public final static String NOMATCHINGEDITOR = "No matching editor to the component.";
	public final static String WRONGURL = "Wrong url : ";
	public final static String WRONGVALUE = "Wrong value : ";
	
	public final static String RDPI_INIT_EXCPOCCURED_1 = "Robodari catched exception(s) from RDPI processor" +
			" while initializing Realization.";
	public final static String RDPI_INIT_EXCPOCCURED_2 = "Given processor must be modified.";
	public final static String RDPI_UNKNOWNERROCC_1 = "Unknow error occured while loading RDPI processors.";
	public final static String RDPI_UNKNOWNERROCC_2 = "Check if given processors caused this error.";
	public final static String RDPI_CANNOTOPENSTREAM = "Cannot access to the file : ";
	

	

	public static void onlyDecimalAccepted(){
		JOptionPane.showMessageDialog(null, ONLYDECIMALACCEPTED,
				ApplicationInfo.getFullName(),
				JOptionPane.OK_OPTION);
	}// end wrongAngle(float)

	
	public static void wrongValue(String name, Object value, String cond){
		Object[] messages = new Object[]{
				WRONGVALUE,
				name + " : " + value,
				cond
		};
		JOptionPane.showMessageDialog(null, messages,
				ApplicationInfo.getFullName(),
				JOptionPane.OK_OPTION);
	}// end wrongVAlue(String, Object
	
	
	public static void sketchNotOpened(){
		JOptionPane.showMessageDialog(null, SKETCHNOTOPENED,
				ApplicationInfo.getFullName(),
				JOptionPane.OK_OPTION);
	}// end sketchNotOpened()
	
	
	public static void cannotCreateComponent(Class<?
			extends RobotComponent> rc){
		try{
			Info info = RobotInfoStore.getInfo(rc.newInstance());
			Object[] messages = new Object[]{
					CANNOTCREATECOMPONENT,
					info.get(InfoKeys.BUSINESS) + ", " + info.get(InfoKeys.NAME)
			};
			JOptionPane.showMessageDialog(null, messages,
					ApplicationInfo.getFullName(),
					JOptionPane.OK_OPTION);
		}catch(Exception excp){
			wrongComponent(rc);
		}
	}// end cannotCreateComponet()
	public static void modelNotRegistered(Class<?
			extends RobotComponent> rc){
		try{
			Info info = RobotInfoStore.getInfo(rc.newInstance());
			Object[] messages = new Object[]{
					MODELNOTRGSTRD,
					info.get(InfoKeys.BUSINESS) + ", " + info.get(InfoKeys.NAME)
			};
			JOptionPane.showMessageDialog(null, messages,
					ApplicationInfo.getFullName(),
					JOptionPane.OK_OPTION);
		}catch(Exception excp){
			wrongComponent(rc);
		}
	}// end modelNotRegistered(RobotComponent)
	
	public static void wrongComponent(Class<? extends RobotComponent> rc){
		Object[] messages = new Object[]{
				WRONGCOMPONENT,
				rc.toString()
		};
		JOptionPane.showMessageDialog(null, messages,
				ApplicationInfo.getFullName(),
				JOptionPane.OK_OPTION);
	}// end wrongComponent(CLASS)
	
	
	public static void cannotGetInfo(Class<? extends RobotComponent> rc){
		Object[] messages = new Object[]{
				CANNOTGETINFO,
				rc.toString()
		};
		JOptionPane.showMessageDialog(null, messages,
				ApplicationInfo.getFullName(),
				JOptionPane.OK_OPTION);
	}// end cannotGetInfo
	
	
	public static void noMatchingEditor(Class<? extends RobotComponent> rc){
		Object[] messages = new Object[]{
				NOMATCHINGEDITOR,
				rc.toString()
		};
		JOptionPane.showMessageDialog(null, messages,
				ApplicationInfo.getFullName(),
				JOptionPane.OK_OPTION);
	}// end noMatchingEditor

	public static void wrongURL(String url){
		Object[] messages = new Object[]{
				WRONGURL,
				url
		};
		JOptionPane.showMessageDialog(null, messages,
				ApplicationInfo.getFullName(),
				JOptionPane.OK_OPTION);
	}// end noMatchingEditor
	
	
	
	
	public static void rdpi_init_exceptionOccured(String ctrlerName, Throwable cause){
		StringWriter sw = new StringWriter();
		PrintWriter ps = new PrintWriter(sw);
		cause.printStackTrace(ps);
		
		JTextArea jta = new JTextArea(sw.getBuffer().toString());
		JScrollPane jsp = new JScrollPane(jta);
		jsp.setPreferredSize(new java.awt.Dimension(300, 300));
		
		Object[] msgs = new Object[]{
				RDPI_INIT_EXCPOCCURED_1,
				RDPI_INIT_EXCPOCCURED_2,
				"controller : " + ctrlerName,
				jsp
		};

		JOptionPane.showMessageDialog(null, msgs,
				ApplicationInfo.getFullName(),
				JOptionPane.OK_OPTION);
	}
	
	public static void rdpi_unknownErrorOccured(String ctrlerName, Throwable cause){
		StringWriter sw = new StringWriter();
		PrintWriter ps = new PrintWriter(sw);
		cause.printStackTrace(ps);
		
		Object[] msgs = new Object[]{
				RDPI_UNKNOWNERROCC_1,
				RDPI_UNKNOWNERROCC_2,
				"controller : " + ctrlerName,
				new JScrollPane(new JTextArea(sw.getBuffer().toString()))
		};

		JOptionPane.showMessageDialog(null, msgs,
				ApplicationInfo.getFullName(),
				JOptionPane.OK_OPTION);
	}
	
	public static void rdpi_cannotOpenStream(String ctrlerName, Object src){
		Object[] messages = new Object[]{
				RDPI_CANNOTOPENSTREAM,
				"controller : " + ctrlerName,
				src
		};
		JOptionPane.showMessageDialog(null, messages,
				ApplicationInfo.getFullName(),
				JOptionPane.OK_OPTION);
	}
}
