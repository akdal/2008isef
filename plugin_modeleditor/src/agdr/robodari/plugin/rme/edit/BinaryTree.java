package agdr.robodari.plugin.rme.edit;

public class BinaryTree{
	Node root;
	
	int size(){
		if(root == null) return 1;
		int size = 1;
		if(root.left != null)
			size += root.left.size();
		if(root.right != null)
			size += root.right.size();
		return size;
	}// end size()
	
	public static class Node{
		Node left, right;
		Object data;
		Node(){}
		Node(Node left, Node right, Object data)
		{ this.left = left; this.right = right; this.data = data; }
		
		int size(){
			int size = 1;
			if(left != null)
				size += left.size();
			if(right != null)
				size += right.size();
			return size;
		}// end size()
	}// end Node
}// end BinaryTree
