package agdr.robodari.plugin.rme.edit;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.toRadians;

import java.awt.*;
import java.util.ArrayList;
import javax.vecmath.*;
import javax.media.j3d.Transform3D;

import agdr.robodari.gui.*;
import agdr.robodari.plugin.rme.edit.BinaryTree.Node;
import agdr.robodari.plugin.rme.gui.Projector;

public final class Calculator{
	final static double SIN30 = Math.sin(Math.PI / 6.0);
	final static double COS30 = Math.cos(Math.PI / 6.0);
	final static double SIN45 = Math.sin(Math.PI / 4.0);
	final static double COS45 = Math.cos(Math.PI / 4.0);
	final static double SQRT3 = Math.sqrt(3);
	
	
	
	private Point[] points;
	
	
	public void setPoints(Point[] p)
	{ this.points = p; }
	
	private strictfp double sqr(double d)
	{ return d * d; }
	private int store_right(int[] points, int psize, Node n){
		if(n == null) return psize;

		psize = store_right(points, psize, n.right);
		points[psize++] = (Integer)n.data;
		psize = store_right(points, psize, n.left);
		return psize;
	}// end store(Node)
	private int store_left(int[] points, int psize, Node n){
		if(n == null) return psize;
		
		psize = store_left(points, psize, n.left);
		points[psize++] = (Integer)n.data;
		psize = store_left(points, psize, n.right);
		return psize;
	}// end store(Node)
	private Polygon createPolygon(Point[] pList, 
			BinaryTree tree, int stp, int edp){
		int[] points = new int[tree.size() + 2];
		int psize = 0;
		
		points[psize++] = stp;
		psize = store_left(points, psize, tree.root.left);
		points[psize++] = edp;
		psize = store_right(points, psize, tree.root.right);
		
		Polygon polygon = new Polygon();
		for(int i = 0; i < psize; i++){
			//System.out.println(points[i]);
			polygon.addPoint(pList[points[i]].x, pList[points[i]].y);
		}
		return polygon;
	}// end createPointIndices(BinaryTree)
	
	
	
	
	
	
	public strictfp Polygon convexHull(){
		if(points == null) return null;
		
		// ready
		boolean[] visited = new boolean[points.length];
		
		// Ÿ   յ a; n´.
		int stpoint = 0, edpoint = 0;
		double sqLen = -1.0;
		double tmpval = 0;
		for(int i = 0; i < points.length; i++){
			for(int j = i + 1; j < points.length; j++){
				tmpval = sqr(points[i].x - points[j].x) +
					sqr(points[i].y - points[j].y);
				if(tmpval > sqLen){
					sqLen = tmpval;
					stpoint = i;
					edpoint = j;
				}
			}
		}/* <= ־ ұ?
		if(points[stpoint].x > points[edpoint].x){
			int tmp = stpoint;
			stpoint = edpoint;
			edpoint = tmp;
		}*/
		visited[stpoint] = visited[edpoint] = true;
		
		//System.out.println("st : " + stpoint + " , ed : " + edpoint);
		//System.out.println("stpoint : " + stpoint + " , edp : " + edpoint);
		
		vector centerLine = new vector(points[stpoint],
				points[edpoint]);
		vector cwdir = new vector(centerLine.y, -1.0 * centerLine.x);
		vector ccwdir = new vector(-1.0 * centerLine.y, centerLine.x);
		
		BinaryTree tree = new BinaryTree();
		Node focus = new Node();
		tree.root = focus;
		
		buildConvexHull(points, stpoint, edpoint, cwdir,
				true, tree, focus, visited);
		buildConvexHull(points, stpoint, edpoint, ccwdir,
				false, tree, focus, visited);
		
		//printTree(tree);
		
		return createPolygon(points, tree, stpoint, edpoint);
	}// end convexHull()
	private strictfp void buildConvexHull(Point[] points, int stpoint, int edpoint,
			vector direction, boolean isout, BinaryTree tree,
			Node focus, boolean[] visited){
		//System.out.println("\nbCH : stp : " + stpoint + " , edp : " + edpoint + " , dir : " + direction + " , iscw : " + isout);
		vector pos, projection;
		
		vector max = null;
		int maxidx = -1;
		//double directionSqrdSize = direction.sqrdSize();
		//double posSqrdSize;
		for(int i = 0; i < points.length; i++){
			if(visited[i]) continue;
			
			pos = new vector(points[stpoint], points[i]);
			//posSqrdSize = pos.sqrdSize();
			projection = direction.proj(pos);
			
			if(pos.dot(direction) < 0)
				continue;
			if(max == null){
				max = projection;
				maxidx = i;
			}else if(max.sqrdSize() < projection.sqrdSize()){
				max = projection;
				maxidx = i;
			}
		}
		if(maxidx == -1){
			//System.out.println("END bCH(1) : stp : " + stpoint + " , edp : " + edpoint + "\n");
			return;
		}else if(max.sqrdSize() < 1){
			//System.out.println("END bCH(2) : stp : " + stpoint + " , edp : " + edpoint + "\n");
			return;
		}
		//System.out.println("maxidx : " + maxidx + " , max : " + max);
		
		visited[maxidx] = true;
		
		//  ã: a; 8 ε κ;  θ : ٿ ʺ, 8 ʺ(a; 8)
		vector left = new vector(points[stpoint], points[maxidx]),
			right = new vector(points[maxidx], points[edpoint]);
		//System.out.println("left : " + left + " , right : " + right);
		
		
		Node n = new Node();
		n.data = maxidx;
		if(isout)
			focus.left = n;
		else
			focus.right = n;
		focus = n;

		vector leftcw = new vector(left.y, left.x * -1.0);
		vector rightcw = new vector(right.y, right.x * -1.0);
		
		boolean lcw = true, rcw = true;
		//System.out.println("leftcw : " + leftcw + " , rightcw : " + rightcw);
		if(leftcw.dot(max) < 0)
			lcw = false;
		if(rightcw.dot(max) < 0)
			rcw = false;
		
		
		//System.out.println("lcw : " + lcw + " , rcw : " + rcw);
		//System.out.println("call rccw from " + stpoint + " , " + edpoint);
		
		if(lcw){
			buildConvexHull(points, stpoint, maxidx, 
					leftcw, true, tree, focus, visited);
		}else{
			buildConvexHull(points, stpoint, maxidx, 
					new vector(left.y * -1.0, left.x),
					true, tree, focus, visited);
		}if(rcw){
			buildConvexHull(points, maxidx, edpoint,
					rightcw, false, tree, focus, visited);
		}else{;
			buildConvexHull(points, maxidx, edpoint, 
					new vector(right.y * -1.0, right.x),
					false, tree, focus, visited);
		}
		//System.out.println("END bCH : stp : " + stpoint + " , edp : " + edpoint);
		//System.out.println();
	}// end build(Point[], int, int, vector, boolean, BinaryTree, Node)
	
	private void printNode(Node n){
		if(n == null)
			return;
		
		System.out.print("(");
		if(n.left != null)
			printNode(n.left);
		System.out.print(n.data);
		if(n.right != null)
			printNode(n.right);
		System.out.print(")");
	}// end printNode(Node)
	private void printTree(BinaryTree tree)
	{ printNode(tree.root); System.out.println(); }
	
	
	
	
	
	public strictfp Polygon expand(Polygon polygon, float range){
		if(polygon == null) return null;
		range = -1.0f * range;
		
		Polygon newp = new Polygon();
		int cnt = polygon.npoints;
		for(int i = cnt; i < cnt * 2; i++){
			vector precede = new vector(polygon.xpoints[(i - 1) % cnt] - polygon.xpoints[i % cnt],
					polygon.ypoints[(i - 1) % cnt] - polygon.ypoints[i % cnt]);
			vector following = new vector(polygon.xpoints[(i + 1) % cnt] - polygon.xpoints[i % cnt],
					polygon.ypoints[(i + 1) % cnt] - polygon.ypoints[i % cnt]);
			precede = precede.hat();
			following = following.hat(); 
			
			double sin2theta = Math.abs(following.crossSize(precede));
			double cos2theta = following.dot(precede);
			//System.out.println("sin2theta : " + sin2theta + " , cos2theta : " + cos2theta);
			double sintheta = sintheta = Math.sqrt((1.0f + Math.sqrt(1.0 - sqr(sin2theta))) / 2.0);
			
			vector pointer = null;
			if(sintheta == 0){
				pointer = precede.plus(following).hat().mul(range);
			}else
				pointer = precede.plus(following).hat().mul(range / sintheta);
			if(pointer.x > 0)
				pointer.x += 0.0000000000001;
			else
				pointer.x += -0.0000000000001;
			if(pointer.y > 0)
				pointer.y += 0.0000000000001;
			else
				pointer.y += -0.0000000000001;
			//System.out.println(i + " : " + "pointer : " + pointer);
			newp.addPoint((int)pointer.x + polygon.xpoints[i % cnt], (int)pointer.y + polygon.ypoints[i % cnt]);
		}
		return newp;
	}// end expand(Polygon, float)
	
	/*
	private ArrayList<Point[]> paths = new ArrayList<Point[]>();
	private ArrayList<Integer> directions = new ArrayList<Integer>();
	public Point[] findShortestPath(Rectangle[] recs, Rectangle original, Point init_stp, Point init_edp){
		paths.clear();
		paths.add(new Point[]{init_stp});
		directions.clear();
		directions.add(3);
		
		while(paths.size() != 0){
			for(int i = 0; i < paths.size(); i++){
				Point[] points = paths.get(i);
				
				int pos = getpos(points[points.length - 1], init_edp);
				int itsc_hor = -2;
				int itsc_ver = -2;
				if(directions.get(i) == 1 || directions.get(i) == 3)
					itsc_hor = intersect_hor(recs, original,
							points[points.length - 1].x, init_edp.x,
							points[points.length - 1].y);
				if(directions.get(i) == 2 || directions.get(i) == 3)
					itsc_ver = intersect_ver(recs, original,
						points[points.length - 1].x,
						points[points.length - 1].y, init_edp.y);
				
				if(itsc_hor == -1){
					// ΰη ΰ O.K.
					Point[] finalps = new Point[points.length + 2];
					System.arraycopy(points, 0, finalps, 0, points.length);
					finalps[finalps.length - 2] = new Point(init_edp.x, points[points.length - 1].y);
					finalps[finalps.length - 1] = init_edp;
					return finalps;
				}else if(itsc_ver == -1){
					// 鼼η ΰ O.K.
					Point[] finalps = new Point[points.length + 2];
					System.arraycopy(points, 0, finalps, 0, points.length);
					finalps[finalps.length - 2] = new Point(points[points.length - 1].x, init_edp.y);
					finalps[finalps.length - 1] = init_edp;
					return finalps;
				}
				
				if(pos == 1){
					//  a , ص a 1̻и鿡 ִ.
					if(itsc_hor != -2){
						
					}
				}
			}
		}
		// ٿ Ͼ / !
		System.out.println("in Calculator : findShortestPath : wrong algorithm....");
		return null;
	}// end findShortestPath(Rectangle[], Point, Point)
	private int getpos(Point stp, Point edp){
		if(stp.x >= edp.x && stp.y >= edp.y)
			return 1;
		else if(stp.x < edp.x && stp.y >= edp.y)
			return 2;
		else if(stp.x < edp.x && stp.y < edp.y)
			return 3;
		return 4;
	}// end getpos(Point, Point)
	private int intersect_hor(Rectangle[] recs, Rectangle original, int x1, int x2, int y){
		int min = -1;
		for(int i = 0; i < recs.length; i++){
			if(recs[i].contains(x1, y) || recs[i].contains(x2, y)) continue;
			if((recs[i].x + recs[i].width - x1) * (recs[i].x + recs[i].width - x2) < 0){
				// ٰ??.
				if(min == -1){
					min = i;
					continue;
				}
				if(recs[min].x > recs[i].x)
					min = i;
			}
		}
		return min;
	}// end intersect_hor(Rectangle[], Rectangle, int, int, int)
	private int intersect_ver(Rectangle[] recs, Rectangle original, int x, int y1, int y2){
		int min = -1;
		for(int i = 0; i < recs.length; i++){
			if(recs[i].contains(x, y1) || recs[i].contains(x, y2)) continue;
			if((recs[i].y + recs[i].height - y1) * (recs[i].y + recs[i].height - y2) < 0){
				// ٰ??.
				if(min == -1){
					min = i;
					continue;
				}
				if(recs[min].y > recs[i].y)
					min = i;
			}
		}
		return min;
	}// end intersect_hor(Rectangle[], Rectangle, int, int, int)
	*/
	
	
	
	
	public static void main(String[] argv){
		// data1 : 8, 0 Ǽ𼭰 ڹٲ ¸𸣰8, 5, 3, 2 Ǵ
		// 6, 7 ǳ ? ?ٲ
		Point[] data1 = new Point[]{
				new Point(28 , 0),
				new Point(0 , 14),
				new Point(-29 , 0),
				new Point(-15 , -8),
				new Point(0 , 0),
				new Point(14 , -8),
				new Point(28 , 17),
				new Point(0 , 31),
				new Point(-29 , 17),
				new Point(-15 , 10),
				new Point(0 , 17),
				new Point(14 , 10)
		};
		Point[] data2 = new Point[]{
				new Point(2, 4),
				new Point(3, 2),
				new Point(-1, 2),
				new Point(-4, 1),
				new Point(-1, -1),
				new Point(-1, -5),
				new Point(6, 0),
				new Point(2, -2),
				new Point(2, -7),
				new Point(-3, -7)
		};
		Calculator chc = new Calculator();
		chc.setPoints(data2);
		Polygon p = chc.convexHull();
	}// end mian(String[])
	
	
	
	
	class vector{
		double x, y;
		vector(double x, double y)
		{ this.x = x; this.y = y; }
		vector(Point st, Point ed)
		{ this.x = ed.x - st.x; this.y = ed.y - st.y; }
		
		strictfp double size()
		{ return Math.sqrt(sqrdSize()); }
		strictfp double sqrdSize()
		{ return x * x + y * y; }
		
		strictfp double dot(vector v){
			double a = v.x * x, b = v.y * y;
			return a + b;
		}// end dot(vector)
		strictfp double crossSize(vector v){
			double a = x * v.y;
			double b = y * v.x;
			return a + b;
		}// end crossSize(vector)
		strictfp vector mul(double d)
		{ return new vector(x * d, y * d); }
		strictfp vector plus(vector v)
		{ return new vector(x + v.x, y + v.y); }
		strictfp vector proj(vector v){
			return mul(dot(v) / sqrdSize());
		}// end proj(vector)
		strictfp vector hat()
		{ return mul(1.0 / size()); }
		
		public String toString()
		{ return "(" + x + "," + y + ")"; }
	}// end vector
}
