package agdr.robodari.plugin.rme.edit;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.util.*;
import javax.swing.*;

import agdr.robodari.comp.*;

public class ConnObject implements java.io.Serializable{
	//private Point position;
	private Rectangle bound;
	private ConnectionBox connBox;
	
	private int totalorder = 1;
	private ArrayList<ReceiverItem> allReceivers;
	private ArrayList<SenderItem> allSenders;
	
	
	public ConnObject(RobotComponent target){
		this.connBox = new ConnectionBox(target);
		//this.position = new Point();
		
		this.allReceivers = new ArrayList<ReceiverItem>();
		this.allSenders = new ArrayList<SenderItem>();
		
		scrabAllReceiversAndSenders(connBox, new Point());
		this.bound = new Rectangle(this.connBox.getRectangle());
		this.bound.height += totalorder * 3;
		this.bound.width += 3;
		this.bound.x = -3;
		this.bound.y = -3;
	}// end Constructor(RobotComponent)
	
	
	public void refresh(){
		totalorder = 1;
		connBox.refresh();
		
		this.allReceivers = new ArrayList<ReceiverItem>();
		this.allSenders = new ArrayList<SenderItem>();
		
		scrabAllReceiversAndSenders(connBox, new Point());
		
		this.bound = new Rectangle(this.connBox.getRectangle());
		this.bound.height += totalorder * 3;
		this.bound.width += 3;
		this.bound.x = -3;
		this.bound.y = -3;
	}// end refresh()
	
	
	public ConnectionBox getConnectionBox()
	{ return connBox; }
	//public Point getPos()
	//{ return position; }
	//public void setPos(Point p)
	//{ this.position = p; }
	public Rectangle getBound()
	{ return bound; }
	public RobotComponent getRobotComponent()
	{ return connBox.getTarget(); }
	
	
	private void scrabAllReceiversAndSenders(ConnectionBox p, Point initp){
		Rectangle rectangle = connBox.getRectangle();

		for(int i = 0; i < p.getSendersSize(); i++){
			Sender rcv = p.getSender(i);
			Rectangle rec = p.getSenderRectangle(i);
			allSenders.add(new SenderItem(rcv,
					new Rectangle(rec.x + initp.x, rec.y + initp.y,
							rec.width, rec.height),
					new Point(rec.width + rec.x + initp.x,
							rec.y + 2 + initp.y),
					new Point(p.sendersColumnXEnd + 3 * (i + 1) + initp.x,
							rec.y + 2 + initp.y),
					new Point(p.sendersColumnXEnd + 3 * (i + 1) + initp.x,
							rectangle.height + 3 * totalorder)));
			totalorder++;
		}
		for(int i = 0; i < p.getReceiversSize(); i++){
			Receiver rcv = p.getReceiver(i);
			Rectangle rec = p.getReceiverRectangle(i);
			allReceivers.add(new ReceiverItem(rcv,
					new Rectangle(rec.x + initp.x, rec.y + initp.y,
							rec.width, rec.height),
					new Point(rec.width + rec.x + initp.x,
							rec.y + 2 + initp.y),
					new Point(p.receiversColumnXEnd + 3 * (i + 1) + initp.x,
							rec.y + 2 + initp.y),
					new Point(p.receiversColumnXEnd + 3 * (i + 1) + initp.x,
							rectangle.height + 3 * totalorder)));
			totalorder++;
		}
		for(int i = 0; i < p.getSubComponentsSize(); i++){
			Point pos = p.getConnectionBoxPosition(i);
			scrabAllReceiversAndSenders(p.getConnectionBox(i),
					new Point(initp.x + pos.x, initp.y + pos.y));
		}
	}// end scrabAllReceiversAndSenders(ConnectionBox p)
	
	
	
	public void paint(Graphics g, int x, int y){
		if(g == null) return;
		
		//g.setColor(Color.yellow);
		//Rectangle rec = connBox.getRectangle();
		//g.fillRect(this.getPos().x + rec.x, this.getPos().y + rec.y, rec.width, rec.height);
		g.setColor(Color.black);
		this.connBox.paint(g, x, y);
	}// end paint(Graphics)
	

	public Receiver[] getAllReceivers(){
		Receiver[] rcvrs = new Receiver[allReceivers.size()];
		for(int i = 0; i < allReceivers.size(); i++)
			rcvrs[i] = allReceivers.get(i).receiver;
		return rcvrs;
	}// end getAllReceivers()
	public Sender[] getAllSenders(){
		Sender[] sds = new Sender[allSenders.size()];
		for(int i = 0; i < allSenders.size(); i++)
			sds[i] = allSenders.get(i).sender;
		return sds;
	}// end getAllReceivers()
	public int getAllReceiverCount()
	{ return allReceivers.size(); }
	public int getAllSenderCount()
	{ return allSenders.size(); }
	
	public Sender getSender(int idx)
	{ return allSenders.get(idx).sender; }
	public Receiver getReceiver(int idx)
	{ return allReceivers.get(idx).receiver; }
	
	public Rectangle getSenderRectangle(int idx)
	{ return allSenders.get(idx).sdrec; }
	public Rectangle getReceiverRectangle(int idx)
	{ return allReceivers.get(idx).rcvrec; }
	public Rectangle getRectangle(Object obj){
		if(obj instanceof Sender){
			int idx = indexOfSender((Sender)obj);
			if(idx != -1)
				return getSenderRectangle(idx);
		}
		if(!(obj instanceof Receiver))
			return null;
		return getReceiverRectangle(indexOfReceiver((Receiver)obj));
	}// end getRectangle(Object)
	
	
	public Point getSenderStartp(int idx)
	{ return allSenders.get(idx).startp; }
	public Point getSenderJoint1(int idx)
	{ return allSenders.get(idx).joint1; }
	public Point getSenderJoint2(int idx)
	{ return allSenders.get(idx).joint2; }
	public Point getReceiverStartp(int idx)
	{ return allReceivers.get(idx).startp; }
	public Point getReceiverJoint1(int idx)
	{ return allReceivers.get(idx).joint1; }
	public Point getReceiverJoint2(int idx)
	{ return allReceivers.get(idx).joint2; }
	
	
	public int indexOfSender(Sender sd){
		for(int i = 0; i < allSenders.size(); i++)
			if(allSenders.get(i).sender == sd)
				return i;
		return -1;
	}// end indexOfSender(Sender_)
	public int indexOfReceiver(Receiver sd){
		for(int i = 0; i < allReceivers.size(); i++)
			if(allReceivers.get(i).receiver == sd)
				return i;
		return -1;
	}// end indexOfSender(Sender_)
	
	
	
	
	
	static class ReceiverItem implements java.io.Serializable{
		Receiver receiver;
		Rectangle rcvrec;
		Point startp;
		Point joint1;
		Point joint2;
		
		public ReceiverItem(Receiver rcv, Rectangle rec, Point startp, Point joint1, Point joint2){
			this.receiver = rcv;
			this.rcvrec = rec;
			this.startp = startp;
			this.joint2 = joint2;
			this.joint1 = joint1;
		}// end 
	}// end class ReceiverItem
	static class SenderItem implements java.io.Serializable{
		Sender sender;
		Rectangle sdrec;
		Point startp;
		Point joint1;
		Point joint2;
		
		public SenderItem(Sender sd, Rectangle rec, Point startp, Point joint1, Point joint2){
			this.sender = sd;
			this.sdrec = rec;
			this.startp = startp;
			this.joint2 = joint2;
			this.joint1 = joint1;
		}// end 
	}// end class ReceiverItem
}
