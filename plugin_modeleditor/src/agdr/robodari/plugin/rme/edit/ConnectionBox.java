package agdr.robodari.plugin.rme.edit;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.util.*;

import javax.swing.Icon;

import agdr.robodari.comp.*;

public class ConnectionBox implements java.io.Serializable{
	private final static Receiver[] NULL_RARRAY = new Receiver[]{};
	private final static Sender[] NULL_SARRAY = new Sender[]{};
	private final static RobotComponent[] NULL_RCARRAY = new RobotComponent[]{};
	private final static Font NAME_FONT = new Font("굴림체", 0, 9);
	private final static Font TITLE_FONT = new Font("굴림체", 0, 11);
	
	
	private RobotComponent target;
	
	private RobotComponent[] subComponents;
	private Sender[] senders;
	private Receiver[] receivers;
	
	private Rectangle[] receiversRec;
	private Rectangle[] sendersRec;
	
	private Rectangle[] receiverLabelsRec;
	private Rectangle[] senderLabelsRec;
	
	private Rectangle titleRec;
	
	private Rectangle rectangle;
	private ConnectionBox[] subBoxes;
	private Point[] subBoxesPos;
	
	int sendersColumnXStart = 0, sendersColumnXEnd = 0;
	int receiversColumnXStart = 0, receiversColumnXEnd = 0;
	int subconnbxsColumnXStart = 0, subconnbxsColumnXEnd = 0;
	
	
	
	public ConnectionBox(RobotComponent component){
		this.target = component;
		
		initSendersAndReceivers();
		initSubComponents();
		
		initTitleRec();
		initSendersPosition();
		initReceiversPosition();
		initSubConnectionBoxesPosition();
		initRectangle();
		
	//	System.out.println("rectangle : " + rectangle);
	//	System.out.println("title rec : " + titleRec);
	//	System.out.println("sendersColumnXStart : " + sendersColumnXStart + " , sendersColumnXEnd : " + sendersColumnXEnd);
	//	System.out.println("rcvsColumnXStart : " + receiversColumnXStart + " , rcvsColumnXEnd : " + receiversColumnXEnd);
	//	System.out.println("subconnbxsColumnXStart : " + subconnbxsColumnXStart + " , subconnbxsColumnXEnd : " + subconnbxsColumnXEnd);
	}// end Constructor(RobotComponent)
	
	
	public void refresh(){
		rectangle = new Rectangle();
		initSendersAndReceivers();
		initSubComponents();
		
		initTitleRec();
		initSendersPosition();
		initReceiversPosition();
		initSubConnectionBoxesPosition();
		initRectangle();
	}// end refresh(
	
	private void initSendersAndReceivers(){
		Receiver[] tmprcvs = target.getReceivers();
		Sender[] tmpsds = target.getSenders();
		
		if(tmprcvs == null && tmpsds == null){
			receivers = new Receiver[0];
			senders = new Sender[0];
			return;
		}
		
		ArrayList<Sender> availsds = new ArrayList<Sender>();
		ArrayList<Receiver> availrcvs = new ArrayList<Receiver>();
		
		if(tmprcvs == null || !target.hasReceiver()){
			for(int i = 0; i < tmpsds.length; i++)
				if(tmpsds[i].isEnabled())
					availsds.add(tmpsds[i]);
			senders = availsds.toArray(NULL_SARRAY);
			receivers = new Receiver[0];
		}else if(tmpsds == null || !target.hasSender()){
			for(int i = 0; i < tmprcvs.length; i++)
				if(tmprcvs[i].isEnabled())
					availrcvs.add(tmprcvs[i]);
			receivers = availrcvs.toArray(NULL_RARRAY);
			senders = new Sender[0];
		}else{
			if(tmprcvs.length > tmpsds.length){
				for(int i = 0; i < tmpsds.length; i++)
					if(tmpsds[i].isEnabled())
						availsds.add(tmpsds[i]);
				senders = availsds.toArray(NULL_SARRAY);
				
				for(int i = 0; i < tmprcvs.length; i++){
					boolean flag = false;
					for(int j = 0; j < senders.length; j++)
						if(tmprcvs[i] == senders[j]){
							flag = true;
							break;
						}
					if(!flag && tmprcvs[i].isEnabled())
						availrcvs.add(tmprcvs[i]);
				}
				receivers = availrcvs.toArray(NULL_RARRAY);
			}else{
				for(int i = 0; i < tmprcvs.length; i++)
					if(tmprcvs[i].isEnabled())
						availrcvs.add(tmprcvs[i]);
				receivers = availrcvs.toArray(NULL_RARRAY);
				
				for(int i = 0; i < tmpsds.length; i++){
					boolean flag = false;
					for(int j = 0; j < receivers.length; j++)
						if(tmpsds[i] == receivers[j]){
							flag = true;
							break;
						}
					if(!flag && tmpsds[i].isEnabled())
						availsds.add(tmpsds[i]);
				}
				senders = availsds.toArray(NULL_SARRAY);
			}
		}
		//System.out.println("in initSendersAndReceivers : receiver len : " + receivers.length);
		//System.out.println("                           , sender len : " + senders.length);
	}// end initSendersAndReceivers()
	private void initSubComponents(){
		RobotComponent[] tmprcs = target.getSubComponents();
		
		if(!target.hasSubComponent() || tmprcs == null){
			subComponents = new RobotComponent[0];
			return;
		}
		ArrayList<RobotComponent> availrcs = new ArrayList<RobotComponent>();
		for(int i = 0; i < tmprcs.length; i++){
			if(tmprcs[i] == null) continue;
			
			//System.out.println("- " + tmprcs[i].getClass());
			if(tmprcs[i].hasReceiver() || tmprcs[i].hasSender())
				availrcs.add(tmprcs[i]);
		}
		subComponents = availrcs.toArray(NULL_RCARRAY);
	}// end initSubComponents()
	
	private void initTitleRec(){
		titleRec = new Rectangle();
		
		AffineTransform titlef_trans = TITLE_FONT.getTransform();
		FontRenderContext titlef_context = new FontRenderContext(titlef_trans,
				false, false);
		
		Rectangle2D tmprec = TITLE_FONT
				.getStringBounds(target.getName() == null ? " " : target.getName(), titlef_context);
		titleRec.width = (int)tmprec.getWidth();
		titleRec.height = (int)tmprec.getHeight();
		titleRec.x = 20 + (int)tmprec.getX();
	}// end initTitleRec()
	private void initSendersPosition(){
		int sender_initp_y = (int)titleRec.getHeight() + 3;
		this.sendersColumnXStart = this.sendersColumnXEnd = 
			3;
		
		AffineTransform namef_trans = NAME_FONT.getTransform();
		FontRenderContext namef_context = new FontRenderContext(namef_trans,
				false, false);
		
		double heightsum = 0, maxwidth = 0;
		String name;
		Icon icon;
		Rectangle2D bound;
		
		senderLabelsRec = new Rectangle[senders.length];
		sendersRec = new Rectangle[senders.length];
		for(int i = 0; i < senders.length; i++){
			name = senders[i].getName();
			icon = senders[i].getSimpleIcon();
			
			if(name == null || name.length() == 0)
				name = " ";
			bound = NAME_FONT.getStringBounds(name, namef_context);
			
			if(maxwidth < bound.getWidth())
				maxwidth = bound.getWidth();
			
			senderLabelsRec[i] = new Rectangle();
			senderLabelsRec[i].x = (int)maxwidth;
			senderLabelsRec[i].y = (int)heightsum + sender_initp_y;
			senderLabelsRec[i].width = (int)bound.getWidth();
			senderLabelsRec[i].height = (int)bound.getHeight();
			
			sendersRec[i] = new Rectangle();
			sendersRec[i].width = icon.getIconWidth();
			sendersRec[i].height = icon.getIconHeight();
			heightsum += icon.getIconHeight();
		}
		for(int i = 0; i < senders.length; i++){
			sendersRec[i].x = (int)maxwidth + 3 + sendersColumnXStart;
			sendersRec[i].y = senderLabelsRec[i].y;
			senderLabelsRec[i].x = (int)maxwidth - senderLabelsRec[i].x +
			sendersColumnXStart;
			
			if(sendersRec[i].x + sendersRec[i].width > sendersColumnXEnd)
				sendersColumnXEnd = sendersRec[i].x + sendersRec[i].width;
		}
	}// end initSendersPosition()
	private void initReceiversPosition(){
		if(!target.hasReceiver() || receivers.length == 0){
			receiversColumnXStart = receiversColumnXEnd = 
				sendersColumnXEnd;
			return;
		}
		int receiver_initp_y = (int)titleRec.getHeight() + 3;
		this.receiversColumnXStart = sendersColumnXEnd + 3 * senders.length;

		AffineTransform namef_trans = NAME_FONT.getTransform();
		FontRenderContext namef_context = new FontRenderContext(namef_trans,
				false, false);
		
		double heightsum = 0, maxwidth = 0;
		String name;
		Icon icon;
		Rectangle2D bound;
		
		receiverLabelsRec = new Rectangle[receivers.length];
		receiversRec = new Rectangle[receivers.length];
		for(int i = 0; i < receivers.length; i++){
			name = receivers[i].getName();
			icon = receivers[i].getSimpleIcon();
			
			if(name == null || name.length() == 0)
				name = " ";
			bound = NAME_FONT.getStringBounds(name, namef_context);
			
			if(maxwidth < bound.getWidth())
				maxwidth = bound.getWidth();
			
			receiverLabelsRec[i] = new Rectangle();
			receiverLabelsRec[i].x = (int)receiversColumnXStart;
			receiverLabelsRec[i].y = (int)heightsum + receiver_initp_y;
			receiverLabelsRec[i].width = (int)bound.getWidth();
			receiverLabelsRec[i].height = (int)bound.getHeight();
			
			receiversRec[i] = new Rectangle();
			receiversRec[i].width = icon.getIconWidth();
			receiversRec[i].height = icon.getIconHeight();
			heightsum += icon.getIconHeight();
		}
		for(int i = 0; i < receivers.length; i++){
			receiversRec[i].x = (int)maxwidth + 3 + receiversColumnXStart;
			receiversRec[i].y = receiverLabelsRec[i].y;
			
			if(receiversRec[i].x + receiversRec[i].width > receiversColumnXEnd)
				receiversColumnXEnd = receiversRec[i].x + receiversRec[i].width;
		}
	}// end initReceiversPosition()
	private void initSubConnectionBoxesPosition(){
		subconnbxsColumnXStart = subconnbxsColumnXEnd = receiversColumnXEnd
				+ 3 * receivers.length + 3;
		if(!target.hasSubComponent()){
			subBoxes = new ConnectionBox[0];
			subBoxesPos = new Point[subBoxes.length];
			return;
		}

		int subboxes_initp_y = (int)titleRec.getHeight() + 3;
		subBoxes = new ConnectionBox[subComponents.length];
		subBoxesPos = new Point[subBoxes.length];
		
		for(int i = 0; i < subBoxes.length; i++){
			subBoxes[i] = new ConnectionBox(subComponents[i]);
			//System.out.println(i + " : new connection box : " + subComponents[i].getClass() + " , " + subBoxes[i].getRectangle());
			subBoxesPos[i] = new Point();
			subBoxesPos[i].y = subboxes_initp_y;
			subBoxesPos[i].x = subconnbxsColumnXEnd;
			
			subconnbxsColumnXEnd += subBoxes[i].getRectangle().width + 3;
		}
	}// end initSubConnectionBoxesPosition()
	private void initRectangle(){
		this.rectangle = new Rectangle();
		this.rectangle.x = this.rectangle.y = 0;
		
		this.rectangle.width = subconnbxsColumnXEnd + 3;
		if(titleRec.x + titleRec.width + 15 > this.rectangle.width)
			this.rectangle.width = titleRec.x + titleRec.width + 15;

		int initp_y = (int)titleRec.getHeight() + 3;
		
		int rcvhei = initp_y;
		for(int i = 0; i < receivers.length; i++)
			rcvhei += receivers[i].getSimpleIcon().getIconHeight();
		rcvhei += 3;
		if(rcvhei > this.rectangle.height)
			this.rectangle.height = rcvhei;
		
		int sdhei = initp_y;
		for(int i = 0; i < senders.length; i++)
			sdhei += senders[i].getSimpleIcon().getIconHeight();
		sdhei += 3;
		if(sdhei > this.rectangle.height)
			this.rectangle.height = sdhei;
		
		for(int i = 0; i < subBoxes.length; i++){
			//System.out.println("in initRectangle()");
			int hei = (int)subBoxes[i].getRectangle().getHeight() + initp_y + 6;
			if(hei > this.rectangle.height)
				this.rectangle.height = hei;
		}
		
	}// end initRectangle()
	
	
	
	
	public Rectangle getRectangle()
	{ return rectangle; }

	public int indexOfReceiver(Receiver rcv)
	{ return Arrays.binarySearch(receivers, rcv); }
	public int indexOfSenders(Sender sd)
	{ return Arrays.binarySearch(senders, sd); }
	public int indexOfSubComp(RobotComponent rc)
	{ return Arrays.binarySearch(subComponents, rc); }
	
	public int getReceiversSize()
	{ return receivers.length; }
	public int getSendersSize()
	{ return senders.length; }
	public int getSubComponentsSize()
	{ return subComponents.length; }
	
	public RobotComponent getTarget()
	{ return target; }
	
	public Receiver getReceiver(int idx)
	{ return receivers[idx]; }
	public Sender getSender(int idx)
	{ return senders[idx]; }
	public RobotComponent getSubComponent(int idx)
	{ return subComponents[idx]; }
	public ConnectionBox getConnectionBox(int idx)
	{ return subBoxes[idx]; }
	
	public Rectangle getReceiverRectangle(int idx)
	{ return receiversRec[idx]; }
	public Rectangle getSenderRectangle(int idx)
	{ return sendersRec[idx]; }
	public Point getConnectionBoxPosition(int idx)
	{ return subBoxesPos[idx]; }
	
	
	
	public void paint(Graphics g, int x, int y){
		//System.out.println("paint() : x : " + x + " , y : " + y);
		x += rectangle.x;
		y += rectangle.y;

		g.setColor(new Color(223, 245, 148, 50));
		g.fillRect(x, y + 7, rectangle.width, rectangle.height - 7);
		g.setColor(Color.black);
		g.setFont(TITLE_FONT);
		g.drawString(target.getName() == null ? "" : target.getName(), x + titleRec.x, y + titleRec.y + titleRec.height);
		
		
		//g.setColor(debug_border);
		g.drawLine(x, y + 7, x, y + rectangle.height);
		g.drawLine(x, y + 7, x + titleRec.x, y + 7);
		g.drawLine(x + titleRec.x + titleRec.width, y + 7, x + rectangle.width,
				y + 7);
		g.drawLine(x + rectangle.width, y + 7, x + rectangle.width
				, y + rectangle.height);
		
		g.setFont(ConnectionBox.NAME_FONT);
		//g.setColor(debug_sd);
		g.drawLine(x, y + rectangle.height, x + sendersColumnXEnd, y + rectangle.height);
		for(int i = 0; i < senders.length; i++){
			g.drawString(senders[i].getName() == null ? "" : senders[i].getName(),
					x + senderLabelsRec[i].x, y + senderLabelsRec[i].y + senderLabelsRec[i].height);
			senders[i].getSimpleIcon().paintIcon(null, g, x + sendersRec[i].x, y + sendersRec[i].y);
		}
		//g.setColor(Color.red);
		g.drawLine(x + receiversColumnXStart, y + rectangle.height, x + receiversColumnXEnd,
				y + rectangle.height);
		for(int i = 0; i < receivers.length; i++){
			g.drawString(receivers[i].getName() == null ? "" : receivers[i].getName(),
					x + receiverLabelsRec[i].x, y + receiverLabelsRec[i].y + receiverLabelsRec[i].height);
			receivers[i].getSimpleIcon().paintIcon(null, g, x + receiversRec[i].x, y + receiversRec[i].y);
		}
		//g.setColor(Color.black);
		//g.setColor(debug_subbx);
		for(int i = 0; i < subBoxes.length; i++)
			subBoxes[i].paint(g, x + subBoxesPos[i].x, y + subBoxesPos[i].y);
	}// end paint(Graphics, int, int)
}