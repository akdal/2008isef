package agdr.robodari.plugin.rme.edit;

import java.io.*;
import java.awt.*;
import java.util.*;

import java.io.*;
import java.awt.*;
import java.util.*;

import agdr.robodari.comp.*;
import agdr.robodari.gui.*;
import agdr.robodari.plugin.rme.gui.ConnectionEditor;


public class ConnectionModel implements java.io.Externalizable{
	private final static long serialVersionUID = 112051698L;
	private final static Receiver[] NULL_RARRAY = new Receiver[]{};
	private final static Sender[] NULL_SARRAY = new Sender[]{};
	private final static ConnObject[] NULL_COARRAY = new ConnObject[]{};

	
	private ArrayList[] horizontal_items = new ArrayList[5];
	private ArrayList[] vertical_items = new ArrayList[5];
	private ArrayList[] horizontal_conobjs = new ArrayList[5];
	private ArrayList[] vertical_conobjs = new ArrayList[5];
	
	private Vector<ConnObject> objects = new Vector<ConnObject>();
	private Hashtable<Object, Object> connection = new Hashtable<Object, Object>();
	private Hashtable<ConnObject, Point> positions = new Hashtable<ConnObject, Point>();
	private transient ArrayList<ConnectionModelListener> listeners = new
			ArrayList<ConnectionModelListener>();
	private SketchModel model;
	
	public ConnectionModel(){
		for(int i = 0; i < 5; i++){
			horizontal_items[i] = new ArrayList();
			vertical_items[i] = new ArrayList();
			horizontal_conobjs[i] = new ArrayList();
			vertical_conobjs[i] = new ArrayList();
		}
	}// end Con()
	
	
	public void load(SketchModel model){
		objects.clear();
		connection.clear();
		positions.clear();
		
		this.model = model;
		if(this.model != null){
			RobotComponent[] comps = model.getRobotComponents();
			for(RobotComponent eachcomp : comps){
				addObject(eachcomp);
			}
		}
		this.fireModelReloaded();
	}// end load(SketchModel)

	
	
	public ConnObject addObject(RobotComponent rc){
		if(rc == null) return null;
		ConnObject newobj = new ConnObject(rc);
		objects.add(newobj);
		positions.put(newobj, findProperPlace(newobj.getBound()));
		this.resetPositionIndex(newobj);
		return newobj;
	}// end addObject(RobotComponent)
	public ConnObject removeObject(RobotComponent rc){
		ConnObject obj = getObject(rc);
		if(obj == null) return null;

		Receiver[] rcvs = obj.getAllReceivers();
		for(int i = 0; i < rcvs.length; i++){
			Enumeration<Object> keys = connection.keys();
			while(keys.hasMoreElements()){
				Object next = keys.nextElement();
				if(next.equals(rcvs[i])){
					Object val = connection.get(next);
					connection.remove(next);
					connection.remove(val);
					break;
				}
			}
		}
		Sender[] sds = obj.getAllSenders();
		for(int i = 0; i < sds.length; i++){
			Enumeration<Object> keys = connection.keys();
			while(keys.hasMoreElements()){
				Object next = keys.nextElement();
				if(next.equals(sds[i])){
					Object val = connection.get(next);
					connection.remove(next);
					connection.remove(val);
					break;
				}
			}
		}
		
		positions.remove(obj);
		this.resetPositionIndex(obj);
		objects.remove(obj);
		return obj;
	}// end removeObject(RobotComponent)
	
	public int getObjectCount()
	{ return objects.size(); }
	public ConnObject getObject(RobotComponent rc){
		for(ConnObject eachobj : objects)
			if(eachobj.getRobotComponent() == rc)
				return eachobj;
		return null;
	}// end getObject(RobotComponent)
	public ConnObject getObject(int idx)
	{ return objects.get(idx); }
	public void resetPositionIndex(ConnObject obj){
		int wseg = ConnectionEditor.DEFAULT_WIDTH / 5;
		int hseg = ConnectionEditor.DEFAULT_HEIGHT / 5;
		Point pos = this.getPosition(obj);
		
		Receiver rcv;
		Rectangle rec;
		Sender sd;
		for(int i = 0; i < obj.getAllReceiverCount(); i++){
			rcv = obj.getReceiver(i);
			rec = obj.getReceiverRectangle(i);
			
			for(int j = 0; j < horizontal_items.length; j++)
				horizontal_items[j].remove(rcv);
			for(int j = 0; j < vertical_items.length; j++)
				vertical_items[j].remove(rcv);
			
			if(pos == null) continue;
			
			horizontal_items[(pos.x + rec.x) / wseg].add(rcv);
			if((pos.x + rec.x) / wseg != (pos.x + rec.width + rec.x) / wseg)
				horizontal_items[(pos.x + rec.x + rec.width) / wseg].add(rcv);
			vertical_items[(pos.y + rec.y) / hseg].add(rcv);
			if((pos.y + rec.y) / hseg != (pos.y + rec.y + rec.height) / hseg)
				vertical_items[(pos.y + rec.y + rec.height) / hseg].add(rcv);
		}
		for(int i = 0; i < obj.getAllSenderCount(); i++){
			sd = obj.getSender(i);
			rec = obj.getSenderRectangle(i);

			for(int j = 0; j < horizontal_items.length; j++)
				horizontal_items[j].remove(sd);
			for(int j = 0; j < vertical_items.length; j++)
				vertical_items[j].remove(sd);
			
			if(pos == null) continue;
			
			horizontal_items[(pos.x + rec.x) / wseg].add(sd);
			if((pos.x + rec.x) / wseg != (pos.x + rec.width + rec.x) / wseg)
				horizontal_items[(pos.x + rec.x + rec.width) / wseg].add(sd);
			vertical_items[(pos.y + rec.y) / hseg].add(sd);
			if((pos.y + rec.y) / hseg != (pos.y + rec.y + rec.height) / hseg)
				vertical_items[(pos.y + rec.y + rec.height) / hseg].add(sd);
		}
		
		
		
		for(int i = 0; i < horizontal_conobjs.length; i++)
			horizontal_conobjs[i].remove(obj);
		for(int i = 0; i < vertical_conobjs.length; i++)
			vertical_conobjs[i].remove(obj);
		
		Rectangle bound = obj.getBound();
		if(bound == null || pos == null){
			return;
		}
		
		int xpos1 = pos.x / wseg;
		int xpos2 = (pos.x + bound.x + bound.width - 1) / wseg;
		int ypos1 = pos.y / hseg;
		int ypos2 = (pos.y + bound.y + bound.height - 1) / hseg;
		if(xpos2 >= 5)
			xpos2 = 4;
		if(ypos2 >= 5)
			ypos2 = 4;
		for(int i = xpos1; i <= xpos2; i++)
			horizontal_conobjs[i].add(obj);
		for(int i = ypos1; i <= ypos2; i++)
			vertical_conobjs[i].add(obj);
	}// end resetPositionIndex(ConnObject)
	public Object findAt(int x, int y){
		int xidx = x / (ConnectionEditor.DEFAULT_WIDTH / 5);
		int yidx = y / (ConnectionEditor.DEFAULT_HEIGHT / 5);
		
		ArrayList selecteditems = new ArrayList();
		for(int i = 0; i < horizontal_items[xidx].size(); i++){
			for(int j = 0; j < vertical_items[yidx].size(); j++)
				if(horizontal_items[xidx].get(i) == vertical_items[yidx].get(j))
					selecteditems.add(vertical_items[yidx].get(j));
		}
		int xseg = ConnectionEditor.DEFAULT_WIDTH / 5;
		int yseg = ConnectionEditor.DEFAULT_HEIGHT / 5;
		for(int i = 0; i < selecteditems.size(); i++){
			Object obj = selecteditems.get(i);
			ConnObject owner = getOwner(obj);
			Point pos = this.getPosition(owner);
			Rectangle rec = owner.getRectangle(obj);
			
			if(rec.contains((x - pos.x), (y - pos.y))){
				return obj;
			}
		}
		return null;
	}// end findAt(int, int)
	public ConnObject findConnObjectAt(int x, int y){
		int xidx = x / (ConnectionEditor.DEFAULT_WIDTH / 5);
		int yidx = y / (ConnectionEditor.DEFAULT_HEIGHT / 5);
		
		ArrayList<ConnObject> selecteditems = new ArrayList<ConnObject>();
		for(int i = 0; i < horizontal_conobjs[xidx].size(); i++){
			for(int j = 0; j < vertical_conobjs[yidx].size(); j++)
				if(horizontal_conobjs[xidx].get(i) == vertical_conobjs[yidx].get(j))
					selecteditems.add((ConnObject)vertical_conobjs[yidx].get(j));
		}
		int xseg = ConnectionEditor.DEFAULT_WIDTH / 5;
		int yseg = ConnectionEditor.DEFAULT_HEIGHT / 5;
		for(int i = 0; i < selecteditems.size(); i++){
			ConnObject obj = selecteditems.get(i);
			Point pos = this.getPosition(obj);
			Rectangle rec = obj.getBound();
			
			if(rec.contains((x - pos.x), (y - pos.y))){
				return obj;
			}
		}
		return null;
	}// end findConnObjectAt(int, int)
	
	
	
	public void setPosition(ConnObject obj, int x, int y){
		Point p = positions.get(obj);
		p.x = x;
		p.y = y;
	}// end setPosition(ConnObject, int, int)
	
	
	
	
	public Receiver[] getAllReceivers(){
		ArrayList<Receiver> receivers = new ArrayList<Receiver>();
		
		for(ConnObject eachobj : objects){
			Collections.addAll(receivers, eachobj.getAllReceivers());
		}
		return receivers.toArray(NULL_RARRAY);
	}// end getAllReceivers()
	public Sender[] getAllSenders(){
		ArrayList<Sender> senders = new ArrayList<Sender>();
		
		for(ConnObject eachobj : objects){
			Collections.addAll(senders, eachobj.getAllSenders());
		}
		return senders.toArray(NULL_SARRAY);
	}// end getAllReceivers()
	public ConnObject[] getConnObjects()
	{ return objects.toArray(NULL_COARRAY); }

	public ConnObject getOwner(Receiver rcv){
		for(ConnObject eachobj : objects)
			if(eachobj.indexOfReceiver(rcv) != -1)
				return eachobj;
		return null;
	}// end getOwner(Receiver)
	public ConnObject getOwner(Sender sd){
		for(ConnObject eachobj : objects)
			if(eachobj.indexOfSender(sd) != -1)
				return eachobj;
		return null;
	}// end getOwner(Receiver)
	public ConnObject getOwner(Object obj){
		ConnObject owner = null;
		if(obj instanceof Receiver)
			owner = getOwner((Receiver)obj);
		if(owner != null)
			return owner;
		else if(!(obj instanceof Sender))
			return null;
		
		return getOwner((Sender)obj);
	}// end getOwner(Object)
	
	public Point getPosition(ConnObject obj)
	{ return positions.get(obj); }
	
	
	
	
	public SketchModel getSketchModel()
	{ return model; }
	

	public void addConnectionModelListener(ConnectionModelListener cml){
		if(listeners == null)
			listeners = new ArrayList<ConnectionModelListener>();
		listeners.add(cml);
	}// end addConnectionModelListener(ConnectionModelListneer)
	public void removeConnectionModelListener(ConnectionModelListener cml){
		if(listeners == null)
			listeners = new ArrayList<ConnectionModelListener>();
		listeners.remove(cml);
	}// end removeConnectionModelListener(ConnectionModelListener)
	
	protected void fireModelReloaded(){
		for(ConnectionModelListener eachListener : listeners)
			eachListener.modelReloaded(new ConnectionModelEvent(this,
					null, null, null));
	}// end fireModelReloaded()
	protected void fireReceiverConnChanged(ConnectionModelEvent cme){
		for(ConnectionModelListener eachListener : listeners)
			eachListener.receiverConnChanged(cme);
	}// end fireReceiverCOnnChanged(ConnectionModeEvent)
	protected void fireSenderConnChanged(ConnectionModelEvent cme){
		for(ConnectionModelListener eachListener : listeners)
			eachListener.senderConnChanged(cme);
	}// end fireReceiverCOnnChanged(ConnectionModeEvent)
	
	
	
	
	
	
	private Point findProperPlace(Rectangle rec){
		double rdx = Math.random();
		Point p = new Point((int)(Math.random() * (double)(ConnectionEditor.DEFAULT_WIDTH - rec.width)),
				(int)(Math.random() * (double)(ConnectionEditor.DEFAULT_HEIGHT - rec.height)));
		return p;
	}// end findProperPlace(Rectangle)
	
	
	
	
	
	public boolean isConnected(Sender sd)
	{ return connection.get(sd) != null; }
	public boolean isConnected(Receiver rcv)
	{ return connection.get(rcv) != null; }
	public Receiver getReceiver(Sender sd){
		Object obj = connection.get(sd);
		if(obj instanceof Receiver)
			return (Receiver)obj;
		return null; 
	}// end getSender
	public Sender getSender(Receiver rcv){
		Object obj = connection.get(rcv);
		if(obj instanceof Sender)
			return (Sender)obj;
		return null; 
	}// end getSender
	
	public void disconnect(Object obj){
		try{
			if(obj instanceof Sender)
				this.connect((Sender)obj, null);
			else if(obj instanceof Receiver)
				this.connect(null, (Receiver)obj);
		}catch(Exception e)
		{ e.printStackTrace(); }
	}// end disconnect
	public void connect(Sender sender, Receiver receiver) throws InvalidConnectionException{
		if(sender != null && connection.contains(sender)){
			Object rmvobj = connection.get(sender);
			connection.remove(rmvobj);
			connection.remove(sender);
		}
		if(receiver != null && connection.contains(receiver)){
			Object rmvobj = connection.get(receiver);
			connection.remove(rmvobj);
			connection.remove(receiver);
		}
		
		if(sender == null || receiver == null){
			if(sender != null)
				this.fireReceiverConnChanged(new ConnectionModelEvent(this,
						getOwner(sender), null, sender));
			if(receiver != null)
				this.fireReceiverConnChanged(new ConnectionModelEvent(this,
						getOwner(receiver), receiver, null));
			return;
		}
		
		if(!sender.isCompatible(receiver))
			throw new InvalidConnectionException(sender.getCondition());
		if(!receiver.isCompatible(sender))
			throw new InvalidConnectionException(receiver.getCondition());
		
		connection.put(sender, receiver);
		connection.put(receiver, sender);

		if(sender != null)
			this.fireReceiverConnChanged(new ConnectionModelEvent(this,
					getOwner(sender), null, sender));
		if(receiver != null)
			this.fireReceiverConnChanged(new ConnectionModelEvent(this,
					getOwner(receiver), receiver, null));
	}// end connect(ConnObject, Sender, ConnObject, Receiver)
	
	
	
	
	public void writeExternal(ObjectOutput output) throws IOException{
		output.writeObject(this.model);
		output.writeObject(this.objects);
		output.writeObject(this.positions);
		output.writeObject(this.connection);
	}// end writeExternal(ObjectOutput)
	public void readExternal(ObjectInput input) throws IOException, ClassCastException,
			ClassNotFoundException{
		this.model = (SketchModel)input.readObject();
		this.objects = (Vector<ConnObject>)input.readObject();
		this.positions = (Hashtable)input.readObject();
		this.connection = (Hashtable)input.readObject();
		
		this.horizontal_conobjs = new ArrayList[5];
		this.horizontal_items = new ArrayList[5];
		this.vertical_conobjs = new ArrayList[5];
		this.vertical_items = new ArrayList[5];
		this.listeners = new ArrayList<ConnectionModelListener>();
		for(int i = 0; i < 5; i++){
			horizontal_items[i] = new ArrayList();
			vertical_items[i] = new ArrayList();
			horizontal_conobjs[i] = new ArrayList();
			vertical_conobjs[i] = new ArrayList();
		}
		for(int i = 0; i < objects.size(); i++)
			objects.get(i).refresh();
		
		for(ConnObject eachobj : objects)
			this.resetPositionIndex(eachobj);
	}// end readExternal(ObjectInput)
}
