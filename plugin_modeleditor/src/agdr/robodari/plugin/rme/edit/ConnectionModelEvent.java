package agdr.robodari.plugin.rme.edit;

import agdr.robodari.comp.*;

public class ConnectionModelEvent {
	public ConnectionModel source;
	public ConnObject changedObject;
	public Receiver changedReceiver;
	public Sender changedSender;
	
	public ConnectionModelEvent(ConnectionModel source, ConnObject
			changedObject, Receiver rcv, Sender sd){
		this.source = source;
		this.changedObject = changedObject;
		this.changedReceiver = rcv;
		this.changedSender = sd;
	}// end Constructor
}
