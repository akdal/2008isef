package agdr.robodari.plugin.rme.edit;

public interface ConnectionModelListener {
	public void modelReloaded(ConnectionModelEvent cme);
	public void receiverConnChanged(ConnectionModelEvent cme);
	public void senderConnChanged(ConnectionModelEvent cme);
}
