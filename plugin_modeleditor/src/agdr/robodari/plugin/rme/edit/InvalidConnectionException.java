package agdr.robodari.plugin.rme.edit;

public class InvalidConnectionException extends Exception{
	public InvalidConnectionException()
	{ super(); }
	public InvalidConnectionException(String msg)
	{ super(msg); }
	public InvalidConnectionException(String message, Throwable cause) 
	{ super(message, cause); }
	public InvalidConnectionException(Throwable cause)  
	{ super(cause); }
}
