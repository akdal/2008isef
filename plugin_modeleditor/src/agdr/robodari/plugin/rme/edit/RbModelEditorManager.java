package agdr.robodari.plugin.rme.edit;


import agdr.robodari.comp.*;
import agdr.robodari.edit.EditorManager;
import agdr.robodari.plugin.rme.gui.*;
import agdr.robodari.plugin.rme.rlztion.gui.RealizationPanel;
import agdr.robodari.project.*;

public interface RbModelEditorManager extends EditorManager{
	public final static int SKETCH_EDITOR = 2;
	public final static int CONN_EDITOR = 3;
	
	public void setTabState(int state);
	
	public SketchEditorInterface getSketchEditor();
	public ConnectionEditor getConnectionEditor();
	public RealizationPanel getRealizationPanel();
	
	
	public RobotModel getEditingRobotModel();
	public RobotModel getRobotModel(ProjectFile pfile);
	
	
	public void startRealization();

}
