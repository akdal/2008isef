package agdr.robodari.plugin.rme.edit;


import agdr.robodari.comp.*;
import agdr.robodari.project.*;

import java.io.*;
import java.util.*;
import java.net.*;

/**
 * Superset of sketch model, connection model
 * @author LeeJuneYoung
 *
 */
public class RobotModel implements Externalizable{
	public final static int STYPE_NULL = -1;
	public final static int STYPE_URL = 0;
	public final static int STYPE_FILE = 1;
	public final static int STYPE_PRJFILE = 2;
	private final static Integer[] NULL_IARRAY = new Integer[]{};
	
	private ProjectFile target;
	 
	private SketchModel sketchModel;
	private ConnectionModel connModel;
	// project file 
	private Hashtable<SourceEditable, URL> sourceURLTable
			= new Hashtable<SourceEditable, URL>();
	private Hashtable<SourceEditable, File> sourceFileTable
			= new Hashtable<SourceEditable, File>();
	private Hashtable<SourceEditable, String[]> sourcePrjFileTable
			= new Hashtable<SourceEditable, String[]>();
	private Hashtable<SourceEditable, String> sourceNameTable
			= new Hashtable<SourceEditable, String>();
	
	private SourceEditable starter;
	
	public RobotModel(){}
	
	
	public ProjectFile getTarget()
	{ return target; }
	public void setTarget(ProjectFile file)
	{ this.target = file; }
	
	public SketchModel getSketchModel()
	{ return sketchModel; }
	public void setSketchModel(SketchModel sm)
	{ this.sketchModel = sm; }
	
	public ConnectionModel getConnectionModel()
	{ return connModel; }
	public void setConnectionModel(ConnectionModel cm)
	{ this.connModel = cm; }
	
	
	/* ***********************************
	 *     Processors
	 ***********************************/
	public SourceEditable getStarter()
	{ return starter; }
	public void setStarter(SourceEditable se)
	{ this.starter = se; }
	
	
	public int getSourceType(SourceEditable se){
		if(se == null)
			return STYPE_NULL;
		
		if(sourceURLTable.containsKey(se))
			return STYPE_URL;
		else if(sourceFileTable.containsKey(se))
			return STYPE_FILE;
		else if(sourcePrjFileTable.containsKey(se))
			return STYPE_PRJFILE;
		return STYPE_NULL;
	}// end getSourceType(SourceEditable)
	public String getSourceName(SourceEditable se)
	{ return sourceNameTable.get(se); }
	public void setSourceName(SourceEditable se, String name)
	{ this.sourceNameTable.put(se, name); }
	
	public URL getSourceURL(SourceEditable se)
	{ return sourceURLTable.get(se); }
	public void setSourceURL(SourceEditable se, URL url){
		if(url == null)
			this.sourceURLTable.remove(se);
		else
			this.sourceURLTable.put(se, url);
		this.sourceFileTable.remove(se);
		this.sourcePrjFileTable.remove(se);
	}// end setSourceURL(SourceEditable, URL)
	
	public File getSourceFile(SourceEditable se)
	{ return sourceFileTable.get(se); }
	public void setSourceFile(SourceEditable se, File file){
		this.sourceURLTable.remove(se);
		if(file == null)
			this.sourceFileTable.remove(se);
		else{
			this.sourceFileTable.put(se, file);
			System.out.println("RobotModel : setSourceFile : " + se + ", " + file);
		}
		this.sourcePrjFileTable.remove(se);
	}// end setSourceFile(SourceEditable)
	
	public String[] getSourcePrjFilePath(SourceEditable se){
		return sourcePrjFileTable.get(se);
	}// end getSourcePrjFilePath()
	public void setSourcePrjFile(SourceEditable se, ProjectFile file){
		this.sourceURLTable.remove(se);
		this.sourceFileTable.remove(se);
		
		if(file == null)
			this.sourcePrjFileTable.remove(se);
		else{
			ArrayList<String> path = new ArrayList<String>();
			ProjectItem focus = file;
			while(focus.getParent() != null){
				path.add(focus.toString());
				focus = (ProjectItem)focus.getParent();
			}
			String[] sapath = new String[path.size()];
			for(int i = 0; i < path.size(); i++)
				sapath[sapath.length - i - 1] = path.get(i);
			this.sourcePrjFileTable.put(se, sapath);
		}
	}// end setSourcePrjFile(SourceEditable, ProjectFile)
	
	
	
	
	/* **********************************
	 *         Input/Output
	 * (non-Javadoc)
	 * @see java.io.Externalizable#writeExternal(java.io.ObjectOutput)
	 **************************************/
	
	public void writeExternal(ObjectOutput output) throws IOException{
		output.writeObject(sketchModel);
		output.writeObject(connModel);
		output.writeObject(this.getStarter());
		
		output.writeInt(sourceURLTable.size() + sourceFileTable.size() + sourcePrjFileTable.size());
		Enumeration<SourceEditable> keys = sourceURLTable.keys();
		SourceEditable key;
		while(keys.hasMoreElements()){
			key = keys.nextElement();
			output.writeObject(key);
			output.writeInt(STYPE_URL);
			output.writeObject(sourceNameTable.get(key));
			output.writeObject(sourceURLTable.get(key));
		}
		keys = sourceFileTable.keys();
		while(keys.hasMoreElements()){
			key = keys.nextElement();
			output.writeObject(key);
			output.writeInt(STYPE_FILE);
			output.writeObject(sourceNameTable.get(key));
			output.writeObject(sourceFileTable.get(key));
		}
		keys = sourcePrjFileTable.keys();
		while(keys.hasMoreElements()){
			key = keys.nextElement();
			output.writeObject(key);
			output.writeInt(STYPE_PRJFILE);
			output.writeObject(sourceNameTable.get(key));
			output.writeObject(this.getSourcePrjFilePath(key));
		}
	}// end writeExternal(ObjectOutput)
	public void readExternal(ObjectInput input) throws IOException,
			ClassNotFoundException, ClassCastException{
		sourceURLTable.clear();
		sourceFileTable.clear();
		sourcePrjFileTable.clear();
		sourceNameTable.clear();
		
		sketchModel = (SketchModel)input.readObject();
		connModel = (ConnectionModel)input.readObject();
		starter = (SourceEditable)input.readObject();
		
		int size = input.readInt();
		for(int i = 0; i < size; i++){
			SourceEditable se = (SourceEditable)input.readObject();
			int type = input.readInt();

			sourceNameTable.put(se, (String)input.readObject());
			if(type == STYPE_URL)
				sourceURLTable.put(se, (URL)input.readObject());
			else if(type == STYPE_FILE)
				sourceFileTable.put(se, (File)input.readObject());
			else if(type == STYPE_PRJFILE)
				sourcePrjFileTable.put(se, (String[])input.readObject());
		}
	}// end readExternal(OBjectInput)
}
