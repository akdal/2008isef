package agdr.robodari.plugin.rme.edit;

import java.io.*;
import java.awt.Graphics2D;
import java.util.*;

import javax.vecmath.*;


import agdr.library.math.*;
import agdr.robodari.appl.*;
import agdr.robodari.comp.*;
import agdr.robodari.comp.model.RobotCompModel;
import agdr.robodari.plugin.rme.gui.SketchScreen;
import agdr.robodari.plugin.rme.store.*;


public class SkObject implements java.io.Externalizable, Cloneable{
	public final static long serialVersionUID = 101;
	private final static RobotComponent[] NULLRCARRAY = new RobotComponent[]{};
	
	private RobotComponent comp;
	protected transient RobotCompModel[] rcmodels;
	
	private transient boolean driftMode = false;
	private transient Vector3f drift_variant;
	
	private Matrix4f transform;
	
	private transient Vector3f negEnd;
	private transient Vector3f posEnd;
	
	private boolean isGroup;
	private Vector<SkObject> children = new Vector<SkObject>();
	private String groupName;
	private Note groupNote;
	
	
	
	
	/**
	 * For Object IO
	 */
	public SkObject(){}
	public SkObject(String groupName, Matrix4f trans){
		this.isGroup = true;
		this.transform = trans;
		this.groupName = groupName;
		this.groupNote = new Note();
	}// end Constructor(Color)
	public SkObject(RobotComponent rc, Matrix4f trans) throws ModelNotRegisteredException, Exception{
		this.comp = rc;
		// MainSketchEditor와 SketchModel간의 transform 비동기화 현상
		this.transform = trans;
		
		this.isGroup = false;
		refresh();
		calculateEnd();
		//restoreImage();
	}// end Constructor(RobotCompoent, STructure, Transform3D)
	
	
	
	public void refresh() throws ModelNotRegisteredException, Exception{
		this.rcmodels = new RobotCompModel[]{
				RobotModelStore.generateModel(this.comp)
		};
		this.calculateEnd();
	}// end refresh()
	
	public RobotComponent getRobotComponent(){
		if(isGroup())
			return null;
		return comp;
	}// end getRobotComponent()
	public RobotCompModel getModel(){
		if(isGroup())
			return null;
		return rcmodels[0];
	}// end getStructure()
	public RobotCompModel getModel(int idx)
	{ return rcmodels[idx]; }
	public int getModelCount()
	{ return rcmodels.length; }
	public void setRobotComponent(RobotComponent r){
		this.comp = r;
	}// end setRobotComponent(R)
	
	
	public Vector3f getPositiveEnd()
	{ return posEnd; }
	public Vector3f getNegativeEnd()
	{ return negEnd; }
	
	public void setTranslVector(float x, float y, float z){
		transform.m03 = x;
		transform.m13 = y;
		transform.m23 = z;
	}// end setTranslVector(float, float, flato)
	
	public void setTransform(Matrix4f matrix){
		if(this.transform == null)
			this.transform = new Matrix4f(matrix);
		this.transform.set(matrix);
	}// end setTransform(Matrix4f)
	public Matrix4f getTransform()
	{ return transform; }
	
	
	public void transform(Point3f p)
	{ transform.transform(p); }
	public void transform(Vector3f p)
	{ transform.transform(p); }
	
	
	
	
	public void addChild(SkObject obj){
		if(!isGroup())
			return;
		children.add(obj);
	}// end addChild(SkObject)
	public void removeChild(SkObject obj){
		if(!isGroup())
			return;
		children.add(obj);
	}// end removeChild(SkObject)
	public void insertChild(SkObject obj, int idx){
		if(!isGroup())
			return;
		children.insertElementAt(obj, idx);
	}// end insertChild(SkObject, int)
	public int indexOfChild(SkObject obj){
		if(!isGroup())
			return -1;
		return children.indexOf(obj);
	}// end indexOfChild(SkObject)
	public SkObject[] getChildren()
	{ return children.toArray(new SkObject[]{}); }
	public void removeChildren()
	{ if(isGroup()) children.clear(); }
	public int getChildCount(){
		if(!isGroup())
			return 0;
		return children.size();
	}// end getChildCount()

	
	public boolean isGroup()
	{ return isGroup; }
	public void setGroup(boolean isGroup){
		this.isGroup = isGroup;
	}// end setGroup(boolean)
	
	public Note getGroupNote()
	{ return groupNote; }
	public void setGroupNote(Note n)
	{ this.groupNote = n; }
	
	public String getGroupName()
	{ return groupName; }
	public void setGroupName(String name){
		if(!isGroup()) return;
		this.groupName = name; 
	}// end setGroupName(String)
	
	public RobotCompModel getIntegratedModel(){
		if(!isGroup()) return null;
		
		RobotCompModel str = new RobotCompModel();
		str.setGroup(true);
		for(int i = 0; i < children.size(); i++){
			SkObject obj = children.get(i);
			if(obj.isGroup())
				str.addSubModel(obj.getIntegratedModel(),
						obj.getTransform());
			else
				str.addSubModel(obj.getModel(), obj.getTransform());
		}
		return str;
	}// end getIntegratedStructure()
	public RobotComponent[] getIntegratedRobotComponents(){
		if(!isGroup()) return null;
		
		ArrayList<RobotComponent> rcomps = new ArrayList<RobotComponent>();
		for(int i = 0; i < children.size(); i++){
			SkObject obj = children.get(i);
			if(obj.isGroup()){
				RobotComponent[] adds = obj.getIntegratedRobotComponents();
				for(int j = 0; j < adds.length; j++)
					rcomps.add(adds[j]);
			}else
				if(obj.getRobotComponent() != null)
					rcomps.add(obj.getRobotComponent());
		}
		return rcomps.toArray(NULLRCARRAY);
	}// end getIntegratedRobotComponent
	
	
	
	public String toString(){
		if(this.isGroup())
			return "[" + groupName + "]"; 
		if(this.comp == null)
			return null;
		return "[" + this.getRobotComponent().getName() + "]";
	}// end toString()
	
	
	
	
	
	public void writeExternal(ObjectOutput output) throws IOException{
		output.writeBoolean(this.isGroup());
		output.writeObject(this.groupName);
		output.writeObject(this.groupNote);
		output.writeObject(this.children);
		output.writeObject(this.comp);
		output.writeObject(this.transform);
	}// end writeExternal(ObjectOutput)
	public void readExternal(ObjectInput input) throws IOException, ClassNotFoundException{
		this.isGroup = input.readBoolean();
		
		this.groupName = (String)input.readObject();
		this.groupNote = (Note)input.readObject();
		
		this.children = (Vector)input.readObject();
		this.rcmodels = new RobotCompModel[0];
		this.comp = (RobotComponent)input.readObject();
		this.transform = (Matrix4f)input.readObject();
		//this.lineColor = (Color)input.readObject();
		this.driftMode = false;
		
		if(!isGroup()){
			try{
				this.refresh();
			}catch(Exception mnfe){
				BugManager.log(mnfe, true);
			}
		}
	}// end readExternal(ObjectINput)
	
	
	
	
	
	// color : 만약 paint가 null이 아니라면 반드시 이 paint를 써야 함.
	private void paintModel(RobotCompModel str, Graphics2D g2d, Matrix4f transform,
			SketchScreen editor, java.awt.Paint paint){
		if(str == null) return;

		if(paint != null)
			g2d.setPaint(paint);
		else if(str.getApprData().getColor() != null)
			g2d.setColor(str.getApprData().getColor());
		
		if(!str.isGroup()){
			int[][] coords = new int[str.getPointCount()][2];
			float[] tmpprjp;
			float scale = editor.getScale();
			for(int i = 0; i < coords.length; i++){
				Point3f tmpp = str.getPoint(i);
				//System.out.println("skobj : tmpp : " + tmpp);
				tmpprjp = editor.getProjector().project(tmpp.x, tmpp.y, tmpp.z,
						transform, scale);
				coords[i][0] = (int)tmpprjp[0];
				coords[i][1] = (int)tmpprjp[1];
			}
			
			int[][] lines = str.getFaces();
			int ax = (int)editor.getAxisStartX(), ay = (int)editor.getAxisStartY();
			
			if(!this.driftMode){
				//System.out.println("SkObject : paintStructure() : coords : " + coords.length + " , lines : " + lines.length);
				for(int i = 0; i < lines.length; i++){
					for(int j = 0; j < lines[i].length - 1; j++)
						g2d.drawLine(coords[lines[i][j]][0] + ax,
								coords[lines[i][j]][1] + ay,
								coords[lines[i][j + 1]][0] + ax,
								coords[lines[i][j + 1]][1] + ay);
					
					g2d.drawLine(coords[lines[i][lines[i].length - 1]][0] + ax,
							coords[lines[i][lines[i].length - 1]][1] + ay,
							coords[lines[i][0]][0] + ax,
							coords[lines[i][0]][1] + ay);
				}
			}else{
				Point3f transl = new Point3f(0, 0, 0);
				this.transform(transl);
				//System.out.println("in paintStructure : transl : " + transl);
				
				tmpprjp = editor.getProjector().project(
						drift_variant.x - transl.x + transform.m03,
						drift_variant.y - transl.y + transform.m13,
						drift_variant.z - transl.z + transform.m23,
						null, editor.getScale());
				int xv = (int)tmpprjp[0], yv = (int)tmpprjp[1];
				//System.out.println("xv : " + xv + " , yv : " + yv);
				for(int i = 0; i < lines.length; i++){
					for(int j = 0; j < lines[i].length - 1; j++)
						g2d.drawLine(coords[lines[i][j]][0] + xv + ax,
								coords[lines[i][j]][1] + yv + ay,
								coords[lines[i][j + 1]][0] + xv + ax,
								coords[lines[i][j + 1]][1] + yv + ay);
					
					g2d.drawLine(coords[lines[i][lines[i].length - 1]][0] + xv + ax,
							coords[lines[i][lines[i].length - 1]][1] + yv + ay,
							coords[lines[i][0]][0] + xv + ax,
							coords[lines[i][0]][1] + yv + ay);
				}
			}
		}
		
		for(int i = 0; i < str.getSubModelCount(); i++){
			RobotCompModel substr = str.getSubModel(i);
			
			Matrix4f trans = new Matrix4f();
			if(transform == null)
				trans.m00 = trans.m11 = trans.m22 = trans.m33 = 1;
			else
				trans.set(transform);
			
			Matrix4f newtrans = str.getTransform(substr);
			if(newtrans != null)
				trans.mul(newtrans);

			paintModel(str.getSubModel(i), g2d, trans, editor, paint);
		}
	}// end paintStructure_stored(Structure, Graphics2D)
	

	
	public void paint(Graphics2D g2d, SketchScreen editor){
		paint(g2d, this.getTransform(), editor);
	}// end paint(Graphics2D)
	public void paint(Graphics2D g2d, Matrix4f transform, SketchScreen editor){
		paint(g2d, transform, editor, null);
	}// end paint(Graphics2D)
	public void paint(Graphics2D g2d, Matrix4f transform,
			SketchScreen editor, java.awt.Paint paint){
		if(g2d == null) return;
		
		if(!isGroup()){
			boolean isnull = paint == null;
			for(RobotCompModel structure : rcmodels){
				if(structure == null)
					continue;
				if(isnull && structure.getApprData() != null)
					paint = structure.getApprData().getColor();
				paintModel(structure, g2d, transform, editor, paint);
			}
		}else{
			for(int i = 0; i < children.size(); i++){
				SkObject obj = children.get(i);
				Matrix4f trans = MathUtility.fastMul(transform, obj.getTransform(), null);
				obj.paint(g2d, trans, editor, paint);
			}
		}
	}// end paint(Graphics2D, boolean)
	
	
	public Object clone(){
		SkObject obj;
		obj = null;
		if(this.isGroup()){
			System.out.println("SkObject : clone : this.groupName : " +this.groupName);
			try{
				obj = new SkObject(this.groupName,
						new Matrix4f(this.transform));
			}catch(Exception excp){
				BugManager.log(excp, true);
			}
			obj.children.addAll(this.children);
			System.out.println("SkObject : clone : obj : " + obj + " , isnull : " + (obj == null));
		}else{
			try{
				obj = new SkObject(this.comp, (Matrix4f)this.transform.clone());
			}catch(Exception excp){
				BugManager.log(excp, true);
			}
		}
		return obj;
	}// end clone()
	
	/*public void restoreImage(){
		if(this.storedImage == null){
			if(this.editor.getGraphics() == null) return;
			GraphicsConfiguration gc = ((Graphics2D)this.editor.getGraphics()).getDeviceConfiguration();
			this.storedImage = gc.createCompatibleImage(1200, 2000, Transparency.TRANSLUCENT);
		}
		if(this.storedImage != null){
			Graphics2D g2d = this.storedImage.createGraphics();
			System.out.println("appr-restoreImage - start Painting");
			this.paint(g2d, true);
			System.out.println("appr-restoreImage - end Painting");
			g2d.dispose();
		}
	}// end restoreImage()
	*/
	
	
	
	
	public void startDrift(){
		driftMode = true;
		if(drift_variant == null)
			drift_variant = new Vector3f();
		else
			drift_variant.x = drift_variant.y = drift_variant.z = 0;
	}// end startDrift()
	public void drift(Point3f newp){
		if(this.drift_variant == null)
			this.drift_variant = new Vector3f();
		this.drift_variant.set(newp.x, newp.y, newp.z);
	}// end applyMousePosition(Point)
	public void endDrift(){
		try{
			synchronized(drift_variant){
				Point3f originalTrans = new Point3f();
				this.transform(originalTrans);
				this.transform.setTranslation(new Vector3f(drift_variant.x + originalTrans.x,
						drift_variant.y + originalTrans.y, drift_variant.z + originalTrans.z));
			}
		}catch(Exception excp){
			BugManager.log(excp, true, false);
		}
		driftMode = false;
	}// end endDrift(f
	
	
	public Point3f[] getAllPoints(){
		if((rcmodels == null || rcmodels.length == 0) && !isGroup())
			return new Point3f[]{};
		else if(!isGroup()){
			ArrayList<Point3f> p3fs = new ArrayList<Point3f>();
			if(rcmodels == null)
				return new Point3f[]{};
			for(RobotCompModel eachstr : rcmodels)
				if(eachstr != null){
					if(eachstr.isGroup())
						Collections.addAll(p3fs, eachstr.getGroupPoints());
					else
						Collections.addAll(p3fs, eachstr.getPoints());
				}
			return p3fs.toArray(new Point3f[]{});
		}
		
		SkObject[] children = getChildren();
		ArrayList<Point3f> points = new ArrayList<Point3f>();

		
		Point3f newp;
		for(int i = 0; i < children.length; i++){
			Point3f[] ps = children[i].getAllPoints();
			Matrix4f transf = children[i].getTransform();

			for(int j = 0; j < ps.length; j++){
				newp = new Point3f(ps[j]);
				transf.transform(newp);
				points.add(newp);
			}
		}
		return points.toArray(new Point3f[]{});
	}// end getAllPoints()
	
	
	
	
	public void calculateEnd(){
		if(posEnd == null)
			posEnd = new Vector3f(-10000, -10000, -10000);
		else
			posEnd.set(-10000, -10000, -10000);
		if(negEnd == null)
			negEnd = new Vector3f(10000, 10000, 10000);
		else
			negEnd.set(10000, 10000, 10000);
		
		if(this.rcmodels == null || this.rcmodels.length == 0){
			posEnd = new Vector3f();
			negEnd = new Vector3f();
		}else{
			for(RobotCompModel eachstr : this.rcmodels)
				_calculateEnd(eachstr);
		}
	}// end calculateEnd()
	private void _calculateEnd(RobotCompModel str){
		if(str == null)
			return;
		
		int cnt = str.getPointCount();
		for(int i = 0; i < cnt; i++){
			Point3f p3f = str.getPoint(i);
			
			if(p3f.x > posEnd.x)
				posEnd.x = p3f.x;
			if(p3f.x < negEnd.x)
				negEnd.x = p3f.x;
			if(p3f.y > posEnd.y)
				posEnd.y = p3f.y;
			if(p3f.y < negEnd.y)
				negEnd.y = p3f.y;
			if(p3f.z > posEnd.z)
				posEnd.z = p3f.z;
			if(p3f.z < negEnd.z)
				negEnd.z = p3f.z;
		}
		cnt = str.getSubModelCount();
		for(int i = 0; i < cnt; i++)
			_calculateEnd(str.getSubModel(i));
	}// end _calculateEnd(STructure)
}