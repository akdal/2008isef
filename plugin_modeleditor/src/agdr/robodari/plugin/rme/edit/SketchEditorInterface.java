package agdr.robodari.plugin.rme.edit;


import agdr.robodari.comp.*;
import agdr.robodari.gui.*;
import agdr.robodari.plugin.rme.gui.CompEditor;
import agdr.robodari.plugin.rme.gui.GroupEditor;
import agdr.robodari.plugin.rme.gui.MainSketchEditor;
import agdr.robodari.plugin.rme.gui.RotationEditor;
import agdr.robodari.plugin.rme.gui.SideSketchViewer;

import javax.vecmath.*;

public interface SketchEditorInterface {
	public final static int HORIZONTAL_MODE = 0;
	public final static int VERTICAL_MODE = 1;
	
	public void addObject(RobotComponent rc, Matrix4f transf);
	public SideSketchViewer getSideSketchViewer(int idx);
	public int getSideSketchViewerCount();
	public MainSketchEditor getMainEditor();
	public SketchModel getModel();
	public void refreshObject(SkObject obj);

	
	public RotationEditor getRotationEditor();
	public GroupEditor getGroupEditor();
	public CompEditor getCurrentCompEditor();
	public void setCurrentCompEditor(CompEditor ce);
	public CompEditor getStoredCompEditor(Class<? extends RobotComponent> cls);
	
	/**
	 * HORIZONTAL_MODE 또는 VERTICAL_MODE
	 * @return
	 */
	public int getLayoutMode();
	public void setLayoutMode(int mode);
}
