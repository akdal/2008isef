package agdr.robodari.plugin.rme.edit;


import java.util.*;
import java.io.*;
//import javax.media.j3d.*;
import javax.vecmath.*;

import agdr.robodari.appl.*;
import agdr.robodari.comp.*;
import agdr.robodari.comp.model.RobotCompModel;
import agdr.robodari.plugin.rme.gui.*;
import agdr.robodari.plugin.rme.rlztion.*;
import agdr.robodari.plugin.rme.store.*;


public class SketchModel implements java.io.Externalizable{
	private final static long serialVersionUID = 10376036392L;
	private final static SketchModelListener[] NULLLARRAY =
				new SketchModelListener[]{};
	private final static RobotComponent[] NULLRARRAY = 
				new RobotComponent[]{};
	private final static SkObject[] NULLAARRAY = new SkObject[]{};
	
	
	private int groupNumber = 1;
	
	
	private Vector<SkObject> objects = new Vector<SkObject>();
	
	private transient Vector<SketchModelListener> listeners = new
			Vector<SketchModelListener>();
	private String name;
	private SourceEditable starter;
	
	
	public SketchModel(){}
	public SketchModel(String name){
		this.name = name;
	}// end Constructor
	
	
	public SourceEditable getStarter()
	{ return starter; }
	public void setStarter(SourceEditable starter)
	{ this.starter = starter; }
	
	public String getName()
	{ return name; }
	public void setName(String name)
	{ this.name = name; }
	
	
	public void addRobotComponent(RobotComponent rc, Matrix4f trans)
			throws ModelNotRegisteredException, NullPointerException,
			Exception{
		SkObject obj = new SkObject(rc, trans);
		objects.add(obj);
		if(BugManager.DEBUG)
			BugManager.println("sketchModel : addRobotComponent() : " + rc.getName());
		
		fireComponentAdded(new SketchModelEvent(this, obj, rc,
				obj.getModel(),
				SketchModelEvent.ACTION_ADDED));
	}// end addRobotComponent(RobotComponent, Matrix4f)
	public void addObject(SkObject obj){
		BugManager.println("SketchModel(" + this.getName() + ") : addObject : " + obj + " , isnull : " + (obj == null) + " , isGroup : " + obj.isGroup());
		if(obj == null) return;
		objects.add(obj);

		fireComponentAdded(new SketchModelEvent(this, obj, obj.getRobotComponent(),
				obj.getModel(),
				SketchModelEvent.ACTION_ADDED));
	}// end addObject(SkObject)
	
	
	public int requestGroupNumber()
	{ return this.groupNumber++; }
	
	
	public int getObjectCount()
	{ return objects.size(); }
	private SkObject _getObject(RobotComponent rc, SkObject obj){
		if(obj.getRobotComponent() == rc)
			return obj;
		else if(!obj.isGroup())
			return null;

		SkObject[] chldr = obj.getChildren();
		SkObject next;
		for(int i = 0; i < chldr.length; i++)
			if((next = _getObject(rc, chldr[i])) != null)
				return next;
		return null;
	}// end _getObject
	public SkObject getObject(RobotComponent rc){
		for(int i = 0; i < objects.size(); i++){
			SkObject obj = objects.get(i);
			if(obj.isGroup() && _getObject(rc, obj)!= null)
				return obj;
			if(obj.getRobotComponent() == rc)
				return obj;
		}
		return null;
	}// end getobject(RobotComponent)
	public Matrix4f getTransform(RobotComponent rc)
	{ return getObject(rc).getTransform(); }
	public RobotCompModel getModel(RobotComponent rc)
	{ return getObject(rc).getModel(); }
	
	public void removeRobotComponent(RobotComponent rc){
		if(!containsRobotComponent(rc))
			return;
		RobotCompModel str = getModel(rc);
		Matrix4f transform = getTransform(rc);
		SkObject obj = this.getObject(rc);
		objects.remove(obj);
		
		fireComponentRemoved(new SketchModelEvent(this, obj, rc, str,
				SketchModelEvent.ACTION_REMOVED));
	}// end removeRobotComponent(RobotComponent)
	
	private void _getRobotComponents(ArrayList<RobotComponent> comps, SkObject obj){
		if(!obj.isGroup()){
			comps.add(obj.getRobotComponent());
			return;
		}
		SkObject[] chldr = obj.getChildren();
		for(int i = 0; i < chldr.length; i++){
			_getRobotComponents(comps, chldr[i]);
		}
	}// end _getRobotComponentSet
	public RobotComponent[] getRobotComponents(){
		ArrayList<RobotComponent> comps = new ArrayList<RobotComponent>();
		for(int i = 0; i < objects.size(); i++){
			SkObject obj = objects.get(i);
			if(!obj.isGroup() && obj.getRobotComponent() != null)
				comps.add(obj.getRobotComponent());
			else
				_getRobotComponents(comps, obj);
		}
		return comps.toArray(NULLRARRAY);
	}// end getComponents()
	
	
	public int size()
	{ return objects.size(); }
	
	private boolean _containsRobotComponent(SkObject obj, RobotComponent rc){
		if(obj.getRobotComponent() == rc)
			return true;
		else if(!obj.isGroup())
			return false;

		SkObject[] chldr = obj.getChildren();
		for(int i = 0; i < chldr.length; i++)
			if(_containsRobotComponent(chldr[i], rc))
				return true;
		return false;
	}// end _contains
	public boolean containsRobotComponent(RobotComponent rc){
		for(int i = 0; i < objects.size(); i++){
			SkObject obj = objects.get(i);
			if(obj.isGroup())
				return _containsRobotComponent(obj, rc);
			if(obj.getRobotComponent() == rc)
				return true;
		}
		return false;
	}// end containsRobotComponent(RobotComponent)
	
	public void removeObject(SkObject obj){
		BugManager.println("sketchModel(" + this.getName() + ") : removeObject : " + obj);
		objects.remove(obj);
		fireComponentRemoved(new SketchModelEvent(this, obj,
				obj.getRobotComponent(), obj.getModel(),
				SketchModelEvent.ACTION_REMOVED));
	}// end removeObject(SkObject)
	
	public void clear(){
		while(0 != objects.size())
			removeObject(objects.get(0));
	}// end clear()

	public SkObject[] getObjects()
	{ return objects.toArray(NULLAARRAY); }
	public boolean containsObject(SkObject appr)
	{ return objects.contains(appr); }
	
	
	
	public SketchModelListener[] getSketchModelListeners(){
		return listeners.toArray(NULLLARRAY);
	}// end getSketchModelListeners()
	public boolean containsSketchModelListener(SketchModelListener sml){
		return listeners.contains(sml);
	}// end containsSketchModelListener(SketchModelListener)
	public void addSketchModelListener(SketchModelListener sml){
		listeners.add(sml);
	}// end addSketchModelListener
	public void removeSketchModelListener(SketchModelListener sml){
		listeners.remove(sml);
	}// end removeSketchModelListener
	
	protected void fireComponentAdded(SketchModelEvent sme){
		for(int i = 0; i < listeners.size(); i++){
			SketchModelListener eachsml = listeners.get(i);
			eachsml.componentAdded(sme);
		}
	}// end fireComponentAdded(SketchModelEvent)
	protected void fireComponentRemoved(SketchModelEvent sme){
		for(SketchModelListener eachsml : listeners)
			eachsml.componentRemoved(sme);
	}// end fireComponentREmoved(SketchModelEvent)
	
	
	
	
	public void readExternal(ObjectInput input)
			throws IOException, ClassCastException, ClassNotFoundException{
		this.name = (String)input.readObject();
		this.objects = (Vector)input.readObject();
		for(int i = 0; i < objects.size(); i++)
			BugManager.println("SketchModel : readExternal : objs[" + i + "] : " + objects.get(i));
		
		int idx = input.readInt();
		RobotComponent[] rcs = this.getRobotComponents();
		BugManager.println("SketchModel : readExternal : startidx : " + idx);
		if(idx == -1)
			this.starter = null;
		else
			this.starter = (SourceEditable)rcs[idx];
		
		this.listeners = new Vector<SketchModelListener>();
	}// end readExternal(ObjectInput)
	public void writeExternal(ObjectOutput output)
			throws IOException{
		output.writeObject(name);
		output.writeObject(objects);
		
		boolean recorded = false;
		RobotComponent[] rcs = this.getRobotComponents();
		for(int i = 0; i < rcs.length; i++)
			if(rcs[i] == starter){
				output.writeInt(i);
				BugManager.println("SketchModel : writeExternal : starter idx : " + i);
				recorded = true;
				break;
			}
		if(!recorded)
			output.writeInt(-1);
	}// end writeExternal(ObjectOutput)
}
