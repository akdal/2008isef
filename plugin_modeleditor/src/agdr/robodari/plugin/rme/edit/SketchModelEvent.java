package agdr.robodari.plugin.rme.edit;

import javax.vecmath.*;

import agdr.robodari.comp.*;
import agdr.robodari.comp.model.RobotCompModel;
import agdr.robodari.plugin.rme.rlztion.*;

public class SketchModelEvent {
	public final static int ACTION_REMOVED = 1;
	public final static int ACTION_ADDED = 2;
	
	private SketchModel source;
	private SkObject object;
	private RobotComponent changedItem;
	private RobotCompModel model3d;
	private int action;
	
	public SketchModelEvent(SketchModel source, SkObject object,
			RobotComponent changedItem, RobotCompModel model3d, int action){
		this.source = source;
		this.object = object;
		this.changedItem = changedItem;
		this.model3d = model3d;
		this.action = action;
	}// end Constructor
	
	public SketchModel getSource()
	{ return source; }
	public SkObject getObject()
	{ return object; }
	public RobotComponent getChangedItem()
	{ return changedItem; }
	public RobotCompModel getModel3D()
	{ return model3d; }
	public int getAction()
	{ return action; }
}
