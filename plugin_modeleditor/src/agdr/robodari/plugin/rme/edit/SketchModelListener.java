package agdr.robodari.plugin.rme.edit;

public interface SketchModelListener {
	public void componentAdded(SketchModelEvent sme);
	public void componentRemoved(SketchModelEvent sme);
}
