package agdr.robodari.plugin.rme.gui;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.vecmath.*;

import agdr.robodari.appl.BugManager;
import agdr.robodari.comp.*;
import agdr.robodari.comp.info.*;
import agdr.robodari.plugin.rme.appl.*;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.store.*;
import agdr.robodari.store.RobotComponentStore;
import agdr.robodari.store.RobotInfoStore;


public class AvailableItemTree extends JPanel implements MouseListener{
	public final static RootNode createRoot(){
		System.out.println("createRoot!");
		Class[] comps = RobotComponentStore.getComponents();
		
		BigCategory[] bigCategories = new BigCategory[5];
		RootNode root = new RootNode("Items", bigCategories);
		
		Category[] categories_0 = new Category[2];
		bigCategories[0] = new BigCategory("Chipsets", categories_0, root);
		{
			Class<Controller> ctrlcls = Controller.class;
			categories_0[0] = new Category("Controllers", bigCategories[0]);
			Class<SubController> subctrlcls = SubController.class;
			categories_0[1] = new Category("Sub controllers", bigCategories[0]);
			
			for(int i = 0; i < comps.length; i++){
				try{
					if(ctrlcls.isAssignableFrom(comps[i]))
						categories_0[0].addChild(new Item(comps[i]));
					else if(subctrlcls.isAssignableFrom(comps[i]))
						categories_0[1].addChild(new Item(comps[i]));
				}catch(Exception excp){}
			}
		}
		
		Category[] categories_1 = new Category[2];
		bigCategories[1] = new BigCategory("Motors", categories_1, root);
		{
			Class<DCMotor> dcmotorcls = DCMotor.class;
			categories_1[0] = new Category("DC Motors", bigCategories[1]);
			Class<ServoMotor> svmotorcls = ServoMotor.class;
			categories_1[1] = new Category("Servo Motors", bigCategories[1]);
			
			for(int i = 0; i < comps.length; i++){
				try{
					if(dcmotorcls.isAssignableFrom(comps[i]))
						categories_1[0].addChild(new Item(comps[i]));
					else if(svmotorcls.isAssignableFrom(comps[i]))
						categories_1[1].addChild(new Item(comps[i]));
				}catch(Exception excp){}
			}
		}
		
		Category[] categories_2 = new Category[2];
		bigCategories[2] = new BigCategory("Sensors", categories_2, root);
		{
			Class<Camera> svcameracls = Camera.class;
			categories_2[0] = new Category("Cameras", bigCategories[2]);
			Class<Sonar> svsonarcls = Sonar.class;
			categories_2[1] = new Category("Sonars", bigCategories[2]);
			
			for(int i = 0; i < comps.length; i++){
				try{
					if(svcameracls.isAssignableFrom(comps[i]))
						categories_2[0].addChild(new Item(comps[i]));
					else if(svsonarcls.isAssignableFrom(comps[i]))
						categories_2[1].addChild(new Item(comps[i]));
				}catch(Exception excp){}
			}
		}
		
		Category[] categories_3 = new Category[4];
		bigCategories[3] = new BigCategory("Electric Components", categories_3, root);
		{
			// categories_3[0]
			Class<Battery> svbtrycls = Battery.class;
			categories_3[0] = new Category("Batteries", bigCategories[3]);
			Class<LED> svledcls = LED.class;
			categories_3[1] = new Category("LED", bigCategories[3]);
			Class<Display> svdispcls = Display.class;
			categories_3[2] = new Category("Display", bigCategories[3]);
			Class<Antenna> svatnacls = Antenna.class;
			categories_3[3] = new Category("Antenna", bigCategories[3]);
			
			for(int i = 0; i < comps.length; i++){
				try{
					if(svbtrycls.isAssignableFrom(comps[i]))
						categories_3[0].addChild(new Item(comps[i]));
					else if(svledcls.isAssignableFrom(comps[i]))
						categories_3[1].addChild(new Item(comps[i]));
					else if(svdispcls.isAssignableFrom(comps[i]))
						categories_3[2].addChild(new Item(comps[i]));
					else if(svatnacls.isAssignableFrom(comps[i]))
						categories_3[3].addChild(new Item(comps[i]));
				}catch(Exception excp){}
			}
		}
		
		Category[] categories_4 = new Category[2];
		bigCategories[4] = new BigCategory("Else", categories_4, root);
		{
			// categories_4[0]
			Class<StructureComponent> strcls = StructureComponent.class;
			categories_4[0] = new Category("Structure Items", bigCategories[4]);
			Class<VirtualComponent> vccls = VirtualComponent.class;
			categories_4[1] = new Category("Virtual Items", bigCategories[4]);
			
			for(int i = 0; i < comps.length; i++){
				try{
					if(strcls.isAssignableFrom(comps[i])){
						if(!RobotCompEditorStore.hasCompEditor(comps[i]))
							continue;
						categories_4[0].addChild(new Item(comps[i]));
					}else if(vccls.isAssignableFrom(comps[i])){
						categories_4[1].addChild(new Item(comps[i]));
					}
				}catch(Exception excp){}
			}
		}
		
		
		return root;
	}// end createRoot()
	
	
	
	
	
	private JTree tree;
	private DefaultTreeModel model;
	private DefaultTreeCellRenderer renderer;
	private RbModelEditorManager manager;
	
	public AvailableItemTree(RbModelEditorManager manager){
		this.manager = manager;
		
		super.setLayout(new BorderLayout());
		
		tree = new JTree();
		model = new DefaultTreeModel(createRoot());
		tree.setModel(model);
		//tree.setFont(new Font("", 0, 12));
		tree.setEditable(false);
		tree.addMouseListener(this);
		
		renderer = new DefaultTreeCellRenderer();
		renderer.setLeafIcon(new ImageIcon());
		tree.setCellRenderer(renderer);
		
		super.add(tree, BorderLayout.CENTER);
	}// end Constructor(
	
	
	
	
	
	
	public void mouseExited(MouseEvent me){}
	public void mouseEntered(MouseEvent me){}
	public void mouseClicked(MouseEvent me){
		if(me.getClickCount() >= 2){
			TreePath path = tree.getPathForLocation(me.getX(), me.getY());
			if(path == null) return;
			Object obj = path.getLastPathComponent();

			if(obj == null) return;
			if(!(obj instanceof Item))
				return;
			
			
			manager.getInterface().setStatusBarText("Adding....");
			System.out.println("in AvailableItemTree : openedfile : " + manager.getInterface().getEditingFile());
			
			Class<? extends RobotComponent> cls = ((Item)obj).getTarget();
			SketchModel editingmodel = manager.getEditingRobotModel().getSketchModel();
			if(editingmodel == null){
				agdr.robodari.plugin.rme.appl.ExceptionHandler.sketchNotOpened();
				return;
			}
			RobotComponent[] comps = editingmodel.getRobotComponents();
			int count = 1;
			for(int i = 0; i < comps.length; i++)
				if(comps[i].getClass() == cls)
					count++;
			String name = cls.getSimpleName() + count;
			
			try{
				final RobotComponent rc = cls.newInstance();
				final Matrix4f pos = new Matrix4f();
				pos.m00 = pos.m11 = pos.m22 = pos.m33 = 1;
				
				rc.setName(name);
				manager.getSketchEditor().addObject(rc, pos);
			}catch(Exception excp){
				ExceptionHandler.cannotCreateComponent(cls);
			}finally{
				manager.getInterface().setStatusBarText("");
			}
		}
	}// end mouseClicked(MouseEvent)
	public void mousePressed(MouseEvent me){}
	public void mouseReleased(MouseEvent me){
	}// end mouseReleased(MouseEvent me)
	
	
	
	
	
	
	/*
	static class AITTreeCellRenderer implements TreeCellRenderer{
		private DefaultTreeCellRenderer dtcr = new DefaultTreeCellRenderer();
		private AvailableItemTree tree;
		
		public AITTreeCellRenderer(AvailableItemTree tree)
		{ this.tree = tree; }
		
		public Component getTreeCellRendererComponent(JTree tree, Object value,
				boolean selected, boolean expanded,
				boolean leaf, int row, boolean hasFocus){
			if(!leaf)
				return dtcr.getTreeCellRendererComponent(tree, value, selected,
						expanded, leaf, row, hasFocus);
			
			
		}// end getTreeCellRenderer
	}// end class AITTreeCellREnderer
	
	*/
	
	
	
	public static class RootNode implements TreeNode{
		private BigCategory[] categories;
		private String name;
		
		public RootNode(String name, BigCategory[] ctgrs){
			this.name = name;
			this.categories = ctgrs;
		}// end Constructor(String, BigCateogry[])
		
		public boolean isLeaf()
		{ return false; }
		public TreeNode getParent()
		{ return null; }
		
		public BigCategory getChildAt(int idx)
		{ return categories[idx]; }
		public int getChildCount()
		{ return categories.length; }
		public boolean getAllowsChildren()
		{ return true; }
		
		public int getIndex(TreeNode node){
			for(int i = 0; i < categories.length; i++)
				if(categories[i] == node)
					return i;
			return -1;
		}// end getIndex(TreeNode)
		
		public Enumeration<BigCategory> children(){
			return new Enumr<BigCategory>(categories);
		}// end children()
		
		public String toString()
		{ return name; }
	}// end class RootNode
	
	public static class BigCategory implements TreeNode{
		private Category[] categories;
		private RootNode parent;
		private String name;
		
		public BigCategory(String name, Category[] ctgrs, RootNode parent){
			this.name = name;
			this.categories = ctgrs;
			this.parent = parent;
		}// end Constructor(String, Category[], RootNode)
		
		public boolean isLeaf()
		{ return false; }
		public TreeNode getParent()
		{ return parent; }
		
		public Category getChildAt(int idx){
			return categories[idx]; 
		}
		public int getChildCount()
		{ return categories.length; }
		public boolean getAllowsChildren()
		{ return true; }
		
		public int getIndex(TreeNode node){
			for(int i = 0; i < categories.length; i++)
				if(categories[i] == node)
					return i;
			return -1;
		}// end getIndex(TreeNode)
		
		public Enumeration<Category> children(){
			return new Enumr<Category>(categories);
		}// end children()
		
		public String toString()
		{ return name; }
	}// end class BigCategory
	
	public static class Category implements TreeNode{
		private final static Item[] NULLIARRAY = new Item[0];
		
		private Vector<Item> items;
		private BigCategory parent;
		private String name;
		
		public Category(String name, BigCategory parent){
			this.name = name;
			this.items = new Vector<Item>();
			this.parent = parent;
		}// end Constructor(String, Item[], BigCategory)
		
		public boolean isLeaf()
		{ return false; }
		public TreeNode getParent()
		{ return parent; }
		
		public void addChild(Item itm){
			itm.setParent(this);
			items.add(itm);
		}// end addChild
		
		public Item getChildAt(int idx)
		{ return items.get(idx); }
		public int getChildCount()
		{ return items.size(); }
		public boolean getAllowsChildren()
		{ return true; }
		
		public int getIndex(TreeNode node){
			return items.indexOf(node);
		}// end getIndex(TreeNode)
		
		public Enumeration<Item> children(){
			return items.elements();
		}// end children()
		
		public String toString()
		{ return name; }
	}// end class Category
	
	public static class Item implements TreeNode{
		private Class target;
		private Info info;
		private Category parent;
		
		public Item(Class target) throws Exception{
			this.target = target;
			this.info = RobotInfoStore.getInfo((RobotComponent)target.newInstance());
		}// end Constructor(Class, Category)
		
		
		public boolean isLeaf()
		{ return true; }
		public TreeNode getParent()
		{ return parent; }
		public void setParent(Category p)
		{ this.parent = p; }
		
		public Class getTarget()
		{ return target; }
		public Info getInfo()
		{ return info; }
		
		public Item getChildAt(int idx)
		{ return null; }
		public int getChildCount()
		{ return 0; }
		public boolean getAllowsChildren()
		{ return false; }
		
		public String toString(){
			StringBuffer buf = new StringBuffer();
			buf.append(info.get(InfoKeys.NAME));
			
			String val = (String)info.get(InfoKeys.BUSINESS);
			if(!(val == null || val.toString().trim().length() == 0))
				buf.append("  (").append(val).append(")");
			return buf.toString();
		}// end toString() 
		
		public int getIndex(TreeNode node){
			return -1;
		}// end getIndex(TreeNode)
		
		public Enumeration<Item> children(){
			return null;
		}// end children()
	}// end class Item
	
	
	
	static class Enumr<C> implements Enumeration<C>{
		private C[] cs;
		private int pos = 0;
		
		public Enumr(C[] cs)
		{ this.cs = cs; }
		
		public boolean hasMoreElements()
		{ return cs.length <= pos; }
		public C nextElement()
		{ return cs[pos++]; }
	}
}
