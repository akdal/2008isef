package agdr.robodari.plugin.rme.gui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import agdr.robodari.appl.*;
import agdr.robodari.comp.*;
import agdr.robodari.plugin.rme.edit.*;

public abstract class CompEditor extends JDialog{
	private JButton okButton = new JButton("Okay");
	private JButton cancelButton = new JButton("Cancel");
	private JPanel editorPanel = new JPanel();
	private JPanel buttonPanel = new JPanel();
	
	protected ActionListener closingAction;
	protected SketchEditorInterface seInterface;
	
	
	public CompEditor(SketchEditorInterface seInterface, Frame parent, String title){
		super(parent, title);
		__set(seInterface);
	}// end Constructor
	public CompEditor(SketchEditorInterface seInterface, Dialog parent, String title){
		super(parent, title);

		__set(seInterface);
	}// end Constructor
	private void __set(SketchEditorInterface seInterface){
		this.seInterface = seInterface;
		
		CancelAction caction = new CancelAction(this);
		super.setModal(true);
		super.addWindowListener(caction);
		
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		okButton.addActionListener(new OkAction(this));
		cancelButton.addActionListener(caction);
		buttonPanel.add(cancelButton);
		buttonPanel.add(okButton);
		super.getContentPane().setLayout(new BorderLayout());
		super.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
		super.getContentPane().add(editorPanel, BorderLayout.CENTER);
	}// end __set
	

	public abstract SkObject getObject();
	public abstract void startEditing(SkObject comp) throws Exception;
	public abstract void finishEditing() throws Exception;
	
	public void setInterface(SketchEditorInterface mng)
	{ this.seInterface = mng; }
	public SketchEditorInterface getInterface()
	{ return seInterface; }
	
	public JPanel getEditorPanel()
	{ return editorPanel; }
	public ActionListener getClosingAction()
	{ return closingAction; }
	
	

	static class OkAction implements ActionListener{
		private CompEditor ce;
		public OkAction(CompEditor ce)
		{ this.ce = ce; }
		
		public void actionPerformed(ActionEvent ae){
			ce.setVisible(false);
			try{
				ce.finishEditing();
			}catch(Exception e){
				BugManager.log(e, true);
			}
			if(ce.getInterface().getCurrentCompEditor() == ce)
				ce.getInterface().setCurrentCompEditor(null);
			if(ce.getClosingAction() != null)
				ce.getClosingAction().actionPerformed(ae);
		}// end actionPerformed(ACtionEvent)
	}// end class OkAction(ActionListener)
	static class CancelAction extends WindowAdapter implements ActionListener{
		private CompEditor ce;
		public CancelAction(CompEditor ce)
		{ this.ce = ce; }
		
		public void actionPerformed(ActionEvent ae){
			ce.setVisible(false);
			if(ce.getInterface().getCurrentCompEditor() == ce)
				ce.getInterface().setCurrentCompEditor(null);
			if(ce.getClosingAction() != null)
				ce.getClosingAction().actionPerformed(ae);
		}// end actionPerformed(ACtionEvent)
		public void windowClosing(WindowEvent we)
		{ actionPerformed(null); }
	}// end class OkAction(ActionListener)
	/*
	protected static class ColorSetAction implements ActionListener{
		private SkObject target;
		
		public ColorSetAction(){}
		
		public void setTarget(SkObject obj)
		{ this.target = obj; }
		
		public void actionPerformed(ActionEvent ae){
			if(target == null) return;
			
			Color c = JColorChooser.showDialog(null, "choose color", target.getLineColor());
			target.setLineColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), 70));
		}// end actionPerformed(ActionEvent)
	}// end class ColorSetAction
	*/
}
