package agdr.robodari.plugin.rme.gui;

import java.awt.*;
import java.awt.event.*;
import java.awt.font.*;
import java.awt.geom.*;
import java.util.*;

import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;

import agdr.robodari.appl.*;
import agdr.robodari.comp.*;
import agdr.robodari.plugin.rme.edit.*;


public class ComponentTree extends JPanel implements MouseListener,
		SketchModelListener, SketchEditorListener, ActionListener{
	private final static String MESSAGE = "Prj not Opened";
	
	private JTree tree;
	private DefaultTreeModel treeModel;
	private RootNode rootNode;
	private TreeCellRenderer cellRenderer;
	private RbModelEditorManager editorManager;
	private SketchModel loadedSketch;
	
	private JPopupMenu popupMenu;
		private JMenuItem editMenuItem;
	
	
	public ComponentTree(RbModelEditorManager editorManager){
		super.setLayout(new BorderLayout());
		this.editorManager = editorManager;
		
		this.cellRenderer = new ItemCellRenderer("");
		//this.cellRenderer.setIcon(null);
		//this.cellRenderer.setLeafIcon(null);
		//this.cellRenderer.setOpenIcon(null);
		//this.cellRenderer.setClosedIcon(null);
		
		this.rootNode = new RootNode(this);
		this.treeModel = new DefaultTreeModel(this.rootNode);
		this.tree = new JTree(treeModel);
		this.tree.setEditable(false);
		this.tree.setCellRenderer(this.cellRenderer);
		this.tree.addMouseListener(this);
		
		super.add(new JScrollPane(this.tree), BorderLayout.CENTER);
		
		if(this.editorManager != null){
			SketchEditorInterface se = this.editorManager.getSketchEditor();
			if(se != null)
				se.getMainEditor().addSketchEditorListener(this);
		}
		
		this.popupMenu = new JPopupMenu();
		{
			this.editMenuItem = new JMenuItem("Edit");
			this.editMenuItem.addActionListener(this);
			this.popupMenu.add(this.editMenuItem);
		}
	}// end Constructor()
	
	
	public void loadSketch(SketchModel model){
		if(this.loadedSketch != null)
			this.loadedSketch.removeSketchModelListener(this);
		
		this.loadedSketch = model;
		this.rootNode.removeAllChildren();
		
		if(loadedSketch != null){
			if(!this.loadedSketch.containsSketchModelListener(this))
				this.loadedSketch.addSketchModelListener(this);
			SkObject[] rcomps = this.loadedSketch.getObjects();
			for(SkObject eachobj : rcomps){
				rootNode.insert(new ItemNode(this, eachobj), 0);
			}
		}
		this.treeModel.reload();
	}// end loadSketch(SketchModel)
	
	
	public void modelChanged(SketchEditorEvent see){
		this.loadSketch(this.editorManager.getEditingRobotModel().getSketchModel());
	}// end modelChanged(SketchEditorEvent)
	public void componentSelected(SketchEditorEvent see){}
	public void componentDeselected(SketchEditorEvent see){}
	public void componentDrifted(SketchEditorEvent see){}
	public void componentRotated(SketchEditorEvent see){}
	public void componentAdded(SketchModelEvent sme){
		rootNode.insert(new ItemNode(this, sme.getObject()), 0);
		this.treeModel.reload();
	}// end componentAdded(SketchModelEvent)
	public void componentRemoved(SketchModelEvent sme){
		rootNode.remove(sme.getObject());
		this.treeModel.reload();
	}// end componentRemoved(SketchModelEvent)
	
	public void mouseEntered(MouseEvent me){}
	public void mouseExited(MouseEvent me){}
	public void mouseDragged(MouseEvent me){}
	public void mouseReleased(MouseEvent me){}
	public void mousePressed(MouseEvent me){}
	public void mouseClicked(MouseEvent me){
		TreePath path = tree.getPathForLocation(me.getX(), me.getY());
		if(path == null) return;
		
		TreeNode node = (TreeNode)
				path.getLastPathComponent();
		if(!(node instanceof ItemNode)) return;
		ItemNode inode = (ItemNode)node;
		
		if(me.getButton() == MouseEvent.BUTTON3){
			tree.setSelectionPath(path);
			this.popupMenu.show(this, me.getX(), me.getY());
		}else if(me.getClickCount() >= 2){
			MainSketchEditor mse = editorManager.getSketchEditor().getMainEditor();
			mse.disposeAllSelections();
			mse.setSelected(inode.getTarget(), true);
			((Component)mse).requestFocus();
		}
	}// end mouseClicked(MouseEvent)
	
	
	public void actionPerformed(ActionEvent ae){
		if(ae.getSource() == this.editMenuItem){
			TreePath tp = tree.getSelectionPath();
			if(tp == null) return;
			
			MainSketchEditor mse = editorManager.getSketchEditor().getMainEditor();
			mse.startCompEdit(((ItemNode)tp.getLastPathComponent()).getTarget());
		}
	}// end aP
	
	
	public void paint(Graphics g){
		if(g == null) return;
		
		super.paint(g);
		if(loadedSketch == null){
			g.setColor(new Color(235, 235, 235));
			g.fillRect(0, 0, super.getWidth(), super.getHeight());
			
			g.setColor(Color.gray);
			
			Font f = new Font("Monospace", 0, 12);
			AffineTransform trans = f.getTransform();
			FontRenderContext context = new FontRenderContext(trans, false, false);
			Rectangle2D rec = f.getStringBounds(MESSAGE, context);
			
			int posx = 0, posy = 0, wid, hei;
			int scrwid = super.getWidth(), scrhei = super.getHeight();

			if(rec instanceof Rectangle){
				Rectangle r = (Rectangle)rec;
				posx = (scrwid - (r.width - r.x)) / 2;
				posy = (scrhei - (r.height - r.y)) / 2;
			}else if(rec instanceof Rectangle2D.Float){
				Rectangle2D.Float r = (Rectangle2D.Float)rec;
				posx = (int)(scrwid - (r.width - r.x)) / 2;
				posy = (int)(scrhei - (r.height - r.y)) / 2;
			}else if(rec instanceof Rectangle2D.Double){
				Rectangle2D.Double r = (Rectangle2D.Double)rec;
				posx = (int)(scrwid - (r.width - r.x)) / 2;
				posy = (int)(scrhei - (r.height - r.y)) / 2;
			}
			g.drawString(MESSAGE, posx, posy);
		}
	}// end paint(Graphics)
	
	
	
	static class RootNode implements MutableTreeNode{
		private ComponentTree ctree;
		private Vector<MutableTreeNode> items;
		private MutableTreeNode parent;
		
		
		public RootNode(ComponentTree ctree){
			this.ctree = ctree; 
			this.items = new Vector<MutableTreeNode>();
			this.parent = null;
		}// end Constructor(ComponentTree)
		
		public TreeNode getParent()
		{ return parent; }
		public boolean isLeaf()
		{ return false; }
		public boolean getAllowsChildren()
		{ return true; }
		public int getIndex(TreeNode node)
		{ return items.indexOf(node); }
		public int getChildCount()
		{ return items.size(); }
		public TreeNode getChildAt(int idx)
		{ return items.get(idx); }
		
		public void removeAllChildren()
		{ items.clear(); }
		public void remove(int idx)
		{ items.removeElementAt(idx); }
		public void setParent(MutableTreeNode mtn)
		{ this.parent = mtn; }
		public void insert(MutableTreeNode mtn, int idx){
			boolean flag = false;
			for(int i = 0; i < items.size(); i++){
				if(compare(items.get(i).toString(), mtn.toString())){
					items.insertElementAt(mtn, i);
					flag = true;
					break;
				}
			}
			if(!flag)
				items.add(mtn);
		}// end insert
		public void add(MutableTreeNode mtn)
		{ this.insert(mtn, 0); }
		public Enumeration children(){
			return items.elements();
		}// end children()
		public void removeFromParent(){
			if(parent.getIndex(this) != -1)
				parent.remove(this);
		}// end removeFromParent()
		public void remove(MutableTreeNode mtn)
		{ items.remove(mtn); }
		public void remove(SkObject rc){
			for(int i = 0; i < items.size(); i++){
				MutableTreeNode eachmtn = items.get(i);
				if(eachmtn instanceof ItemNode){
					ItemNode eachitm = (ItemNode)eachmtn;
					if(eachitm.getTarget() == rc){
						this.remove(eachitm);
						return;
					}
				}
			}
		}// end remove(RobotComponent)
		
		
		private boolean compare(String a, String b){
			if(a.length() > b.length())
				return !compare(b, a);
			
			int len = a.length();
			for(int i = 0; i < len; i++){
				if(a.charAt(i) > b.charAt(i))
					return true;
			}
			return false;
		}// end compare
		
		
		public void setUserObject(Object obj){
			ctree.loadedSketch.setName(obj.toString());
		}// end setUserObject(Object)
		
		public String toString(){
			if(ctree.loadedSketch == null)
				 return "sketch";
			return ctree.loadedSketch.getName();
		}// end toString()
	}// end class RootNode
	static class ItemNode implements MutableTreeNode{
		private ComponentTree ctree;
		private SkObject target;
		
		public ItemNode(ComponentTree ctree, SkObject target){
			this.ctree = ctree;
			this.target = target;
		}// end COnstructor(ComponentTree, RobotComponent)
		
		
		public TreeNode getParent()
		{ return ctree.rootNode; }
		public boolean getAllowsChildren()
		{ return false; }
		public int getIndex(TreeNode node)
		{ return -1; }
		public java.util.Enumeration children()
		{ return null; }
		public boolean isLeaf()
		{ return true; }
		public int getChildCount()
		{ return 0; }
		public TreeNode getChildAt(int idx)
		{ return null; }
		
		public void remove(int idx){}
		public void remove(MutableTreeNode mtn){}
		public void insert(MutableTreeNode mtn, int idx){}
		public void removeFromParent(){}
		public void setParent(MutableTreeNode mtn){}
		public void setUserObject(Object obj){}
		
		public SkObject getTarget()
		{ return target; }
		
		public String toString(){
			if(target.isGroup())
				return target.getGroupName();
			return target.getRobotComponent().getName();
		}// end toString()
	}// end class ItemNode
	
	
	
	
	static class ItemCellRenderer extends JLabel implements TreeCellRenderer{
		int idx = 0;
		public ItemCellRenderer(String str){
			super(str);
			this.setOpaque(true);
		}
		//public ItemCellRenderer(){ System.out.println("ItenCEllRenderer : Constructor2 "); }
		
		public Component getTreeCellRendererComponent(JTree tree, Object value,
				boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus){
			this.setText(value.toString());
			
			AffineTransform trans = this.getFont().getTransform();
			FontRenderContext context = new FontRenderContext(trans, false, false);
			Rectangle2D rec = this.getFont().getStringBounds(value.toString(), context);
			
			Dimension size = /*new Dimension((int)rec.getWidth() + 4, (int)rec.getHeight() + 2);*/
					new Dimension(150, (int)rec.getHeight() + 3);
			this.setSize(size);
			this.setPreferredSize(size);
			
			if(hasFocus)
				this.setBackground((Color)UIManager.getDefaults().get("Tree.selectionBackground"));
			else
				this.setBackground(Color.white);
			return this;
		}// end getTreeCEllRenderer(JTree, Object, boolean, boolean,
		// boolean, int, boolean)
	}// end class ItemCellRenderer
}
