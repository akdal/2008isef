package agdr.robodari.plugin.rme.gui;

import java.util.*;
import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import javax.swing.*;

import agdr.robodari.appl.*;
import agdr.robodari.comp.*;
import agdr.robodari.edit.*;
import agdr.robodari.plugin.rme.edit.*;

public class ConnectionEditor extends JPanel implements
		ConnectionModelListener, SketchModelListener,
		MouseListener, MouseMotionListener{
	public final static int DEFAULT_WIDTH = 1800;
	public final static int DEFAULT_HEIGHT = 1500;
	
	private static Color gridColor = Color.gray;
	public static Color getGridColor()
	{ return gridColor; }
	
	private static Color roveredColor = Color.red;
	public static Color getROveredColor()
	{ return roveredColor; }
	private static Color roveredFillColor = new Color(255, 0, 0, 50);
	public static Color getROveredFillColor()
	{ return roveredFillColor; }
	
	private static Color pressedColor = new Color(128, 64, 0);
	public static Color getPressedColor()
	{ return pressedColor; }
	private static Color pressedFillColor = new Color(128, 64, 0, 50);
	public static Color getPressedFillColor()
	{ return pressedFillColor; }
	
	private static Color selectedColor = Color.red;
	public static Color getSelectedColor()
	{ return selectedColor; }
	private static Color selectedFillColor = new Color(255, 0, 0, 50);
	public static Color getSelectedFillColor()
	{ return selectedFillColor; }
	
	
	
	
	private Object pressedItem;
	private Object selectedItem;
	private Object roveredItem;
	private ConnObject driftingConnObject;
	private Point driftingConnObject_startp;
	
	private RbModelEditorManager editorManager;
	private ConnectionModel connModel;
	private Hashtable<ConnObject, BufferedImage> storedPictures =
		new Hashtable<ConnObject, BufferedImage>();
	private SketchModel sketchModel;
	
	private JPopupMenu popupMenu;
	
	
	
	public ConnectionEditor(RbModelEditorManager editorManager,
			ConnectionModel model){
		this.editorManager = editorManager;
		
		super.setPreferredSize(new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT));
		
		initPopupMenu();
		
		super.setBorder(new javax.swing.border.EtchedBorder());
		super.setOpaque(true);
		super.setBackground(Color.white);
		
		super.addMouseListener(this);
		super.addMouseMotionListener(this);
		
		loadConnection(model);
	}// end Constructor(EdtorMAnager, SketchModel)
	private void initPopupMenu(){
		popupMenu = new JPopupMenu();
		
		JMenuItem disconnect = new JMenuItem(new DisconnectAction(this));
		popupMenu.add(disconnect);
	}// end initPopupMenu()
	
	
	public EditorManager getEditorManager()
	{ return editorManager; }
	
	
	public ConnectionModel getConnectionModel()
	{ return connModel; }
	public void loadConnection(ConnectionModel model){
		if(model == connModel) return;
		
		if(this.connModel != null){
			this.connModel.removeConnectionModelListener(this);
			this.sketchModel.removeSketchModelListener(this);
			storedPictures.clear();
		}
		this.connModel = model;
		if(connModel != null){
			this.connModel.addConnectionModelListener(this);
			this.sketchModel = this.connModel.getSketchModel();
			this.sketchModel.addSketchModelListener(this);
			for(int i = 0; i < connModel.getObjectCount(); i++)
				this.resetPicture(connModel.getObject(i));
		}

		repaint();
	}// end loadSketch(SketchModel)
	
	
	public void receiverConnChanged(ConnectionModelEvent cme)
	{ repaint(); }
	public void senderConnChanged(ConnectionModelEvent cme)
	{ repaint(); }
	public void modelReloaded(ConnectionModelEvent cme){
		if(this.sketchModel == this.connModel.getSketchModel())
			return;
		
		if(this.sketchModel != null)
			this.sketchModel.removeSketchModelListener(this);
		this.sketchModel = this.connModel.getSketchModel();
		if(this.sketchModel != null)
			this.sketchModel.addSketchModelListener(this);
		repaint();
	}// end modelReloaded(ConnectionModelEvent)
	
	public void componentAdded(SketchModelEvent sme){
		if(sme.getObject().isGroup()){
			SkObject gobj = sme.getObject();
			SkObject[] children = gobj.getChildren();
			for(int i = 0; i < gobj.getChildCount(); i++){
				componentAdded(new SketchModelEvent(sme.getSource(),
						children[i], children[i].getRobotComponent(),
						children[i].getModel(),
						SketchModelEvent.ACTION_ADDED));
			}
		}else{
			System.out.println("ConnectionEditor : componentAdded");
			resetPicture(connModel.addObject(sme.getChangedItem()));
			repaint();
		}
	}// end componentAdded(SketchModelEvent)
	public void componentRemoved(SketchModelEvent sme){
		if(sme.getObject().isGroup()){
			SkObject gobj = sme.getObject();
			SkObject[] children = gobj.getChildren();
			for(int i = 0; i < gobj.getChildCount(); i++){
				componentRemoved(new SketchModelEvent(sme.getSource(),
						children[i], children[i].getRobotComponent(),
						children[i].getModel(),
						SketchModelEvent.ACTION_REMOVED));
			}
		}else{
			resetPicture(connModel.removeObject(sme.getChangedItem()));
			repaint();
		}
	}// end componentRemoveD(SketchModelEvent)
	
	
	
	public void resetPicture(ConnObject obj){
		if(obj == null) return;
		
		if(connModel.getPosition(obj) == null){
			storedPictures.remove(obj);
			return;
		}
		Rectangle bound = obj.getBound();
		BufferedImage image = new BufferedImage(bound.width, bound.height, BufferedImage.TYPE_4BYTE_ABGR);
		storedPictures.put(obj, image);
		obj.paint(image.createGraphics(), 0, 0);
	}// end resetPicture(ConnObject)
	
	
	
	
	public void mouseEntered(MouseEvent me){}
	public void mouseExited(MouseEvent me){}
	public void mouseClicked(MouseEvent me){}
	public void mousePressed(MouseEvent me){
		if(me.getButton() == MouseEvent.BUTTON3){
			popupMenu.show(this, me.getX(), me.getY());
			return;
		}else if(popupMenu.isVisible()) return;
		
		if(roveredItem == null){
			driftingConnObject = connModel.
				findConnObjectAt(me.getX(), me.getY());
			if(driftingConnObject != null){
				Point pt = connModel.getPosition(driftingConnObject);
				driftingConnObject_startp = new Point(me.getX() - pt.x, me.getY() - pt.y);
			}
			return;
		}
		if(selectedItem != null){
			if(selectedItem == roveredItem){
				pressedItem = roveredItem;
				roveredItem = null;
			}
			try{
				int type1 = 0, type2 = 0;
				
				if(selectedItem instanceof Sender)
					type1 = 1;
				if(selectedItem instanceof Receiver){
					if(type1 == 1)
						type1 = 3;
					else type1 = 2;
				}
				if(roveredItem instanceof Sender)
					type2 = 1;
				if(roveredItem instanceof Receiver){
					if(type2 == 1)
						type2 = 3;
					else type2 = 2;
				}
				
				if(type1 != 3 && type1 == type2){
					// 둘다 receiver이거나 sender이다 -> 불가능.
					ExceptionHandler.incompatibleConnection(".");
					return;
				}
				
				if(type1 == 1)
					connModel.connect((Sender)selectedItem, (Receiver)roveredItem);
				else if(type2 == 1)
					connModel.connect((Sender)roveredItem, (Receiver)selectedItem);
				else if(type1 == 3 && type2 == 3){
					Sender sd = (Sender)roveredItem;
					Receiver rcv = (Receiver)selectedItem;
					if(sd.isCompatible(rcv) && rcv.isCompatible(sd))
						connModel.connect(sd, rcv);
					else
						connModel.connect((Sender)selectedItem, (Receiver)roveredItem);
				}else if(type1 == 2)
					connModel.connect((Sender)roveredItem, (Receiver)selectedItem);
				else if(type2 == 2)
					connModel.connect((Sender)selectedItem, (Receiver)roveredItem);
			}catch(InvalidConnectionException icexcp){
				ExceptionHandler.incompatibleConnection(icexcp.getMessage());
			}catch(Exception excp){
				BugManager.log(excp, true);
			}finally{
				selectedItem = null;
				roveredItem = null;
			}
		}
		pressedItem = roveredItem;
		roveredItem = null;
		if(selectedItem != pressedItem)
			selectedItem = null;
		repaint();
	}// end mousePressed(MouseEvent)
	public void mouseReleased(MouseEvent me){
		if(driftingConnObject != null){
			connModel.resetPositionIndex(driftingConnObject);
			driftingConnObject = null;
			driftingConnObject_startp = null;
		}else if(pressedItem == null) return;
		else if(popupMenu.isVisible()) return;
		
		
		if(selectedItem == pressedItem){
			selectedItem = null;
			roveredItem = pressedItem;
		}else 
			selectedItem = roveredItem = pressedItem;
		pressedItem = null;
		repaint();
	}// end mouseReleased(MouseEvent)
	
	private boolean mouseMoved_working = false;
	public void mouseMoved(MouseEvent me){
		if(mouseMoved_working) return;
		else if(connModel == null) return;
		else if(popupMenu.isVisible()) return;
		
		mouseMoved_working = true;
		roveredItem = connModel.findAt(me.getX(), me.getY());
		repaint();
		mouseMoved_working = false;
	}// end mouseMoved(MouseEvent)
	private boolean mouseDragged_working = false;
	public void mouseDragged(MouseEvent me){
		if(driftingConnObject == null) return;
		else if(mouseDragged_working) return;
		else if(popupMenu.isVisible()) return;
		
		mouseDragged_working = true;
		connModel.setPosition(driftingConnObject, me.getX() - driftingConnObject_startp.x,
				me.getY() - driftingConnObject_startp.y);
		repaint();
		mouseDragged_working = false;
	}// end mouseDragged(MouseEvent)
	
	
	
	
	private void drawGrid(Graphics g){
		g.setColor(ConnectionEditor.getGridColor());
		for(int i = 20; i < DEFAULT_WIDTH; i += 20)
			for(int j = 20; j < DEFAULT_HEIGHT; j += 20)
				g.drawLine(i, j, i, j);
		g.setColor(Color.black);
	}// end drawGrid(Graphics)
	
	private boolean painting = false;
	public void paint(Graphics g){
		if(g == null) return;
		else if(painting) return;
		
		painting = true;
		super.paint(g);
		drawGrid(g);
		
		if(connModel == null){
			g.setColor(new Color(200, 200, 200, 70));
			g.fillRect(0, 0, DEFAULT_WIDTH, DEFAULT_HEIGHT);
			return;
		}
		
		
		
		ConnObject[] objects = connModel.getConnObjects();
		ArrayList<Object> cnctedobjs = new ArrayList<Object>();
		Map<Object, Point> points = new Hashtable<Object, Point>();
		Map<Object, ConnObject> owners = new Hashtable<Object, ConnObject>();
		
		for(ConnObject eachobj : objects){
			g.setColor(Color.black);
			
			Point pos = connModel.getPosition(eachobj);
			g.drawImage(storedPictures.get(eachobj), pos.x, pos.y, this);
			
			g.setColor(Color.red);
			
			for(int i = 0; i < eachobj.getAllSenderCount(); i++){
				Sender sd = eachobj.getSender(i);
				if(connModel.isConnected(sd)){
					Point startp = eachobj.getSenderStartp(i);
					Point joint1 = eachobj.getSenderJoint1(i);
					Point joint2 = eachobj.getSenderJoint2(i);
					g.drawLine(startp.x + pos.x, startp.y + pos.y, joint1.x + pos.x,
							joint1.y + pos.y);
					g.drawLine(joint1.x + pos.x, joint1.y + pos.y, joint2.x + pos.x,
							joint2.y + pos.y);
					cnctedobjs.add(sd);
					points.put(sd, new Point(joint2.x + pos.x, joint2.y + pos.y));
					owners.put(sd, eachobj);
				}
			}
			for(int i = 0; i < eachobj.getAllReceiverCount(); i++){
				Receiver rcv  = eachobj.getReceiver(i);
				if(connModel.isConnected(rcv)){
					Point startp = eachobj.getReceiverStartp(i);
					Point joint1 = eachobj.getReceiverJoint1(i);
					Point joint2 = eachobj.getReceiverJoint2(i);
					g.drawLine(startp.x + pos.x, startp.y + pos.y, joint1.x + pos.x,
							joint1.y + pos.y);
					g.drawLine(joint1.x + pos.x, joint1.y + pos.y, joint2.x + pos.x,
							joint2.y + pos.y);
					cnctedobjs.add(rcv);
					points.put(rcv, new Point(joint2.x + pos.x, joint2.y + pos.y));
					owners.put(rcv, eachobj);
				}
			}
		}
		
		for(int i = 0; i < cnctedobjs.size(); i++){
			Object eachobj = cnctedobjs.get(i);
			if(!(eachobj instanceof Receiver))
				continue;
			
			Receiver rcv = (Receiver)eachobj;
			ConnObject rcvobj = owners.get(rcv);
			int idx = rcvobj.indexOfReceiver(rcv);
			Rectangle rcvrec = null;
			
			if(idx != -1)
				rcvrec = rcvobj.getReceiverRectangle(idx);
			if(idx == -1 && (rcv instanceof Sender)){
				idx = rcvobj.indexOfSender((Sender)rcv);
				rcvrec = rcvobj.getSenderRectangle(idx);
			}
			Point rcvpos = connModel.getPosition(rcvobj);
			
			Object connected = connModel.getSender(rcv);
			if(connected == null && rcv instanceof Sender)
				connected = connModel.getReceiver((Sender)rcv);
			ConnObject cnnobj = owners.get(connected);
			Rectangle cnnrec = null;
			int cnnidx = -1;
			
			if(connected instanceof Receiver){
				cnnidx = cnnobj.indexOfReceiver((Receiver)connected);
				if(cnnidx != -1)
					cnnrec = cnnobj.getReceiverRectangle(cnnidx);
			}
			if(cnnidx == -1 && connected instanceof Sender)
				cnnrec = cnnobj.getSenderRectangle(cnnidx = cnnobj.indexOfSender((Sender)connected));
			
			Point cnnpos = connModel.getPosition(cnnobj);
				
			Point startp = points.get(rcv);
			Point endp = points.get(connected);
			
			g.setColor(getSelectedColor());
			g.drawRect(rcvpos.x + rcvrec.x, rcvpos.y + rcvrec.y, rcvrec.width, rcvrec.height);
			g.drawRect(cnnpos.x + cnnrec.x, cnnpos.y + cnnrec.y, cnnrec.width, cnnrec.height);
			g.setColor(getSelectedFillColor());
			g.fillRect(rcvpos.x + rcvrec.x, rcvpos.y + rcvrec.y, rcvrec.width, rcvrec.height);
			g.fillRect(cnnpos.x + cnnrec.x, cnnpos.y + cnnrec.y, cnnrec.width, cnnrec.height);
			
			g.setColor(Color.red);
			if(endp.y < startp.y){
				g.drawLine(startp.x, startp.y, endp.x, startp.y);
				g.drawLine(endp.x, startp.y, endp.x, endp.y);
			}else{
				g.drawLine(startp.x, startp.y, startp.x, endp.y);
				g.drawLine(startp.x, endp.y, endp.x, endp.y);
			}
			cnctedobjs.remove(connected);
		}
		
		if(roveredItem != null){
			ConnObject owner = connModel.getOwner(roveredItem);
			Rectangle rec = owner.getRectangle(roveredItem);
			Point point = connModel.getPosition(owner);
			
			g.setColor(ConnectionEditor.getROveredColor());
			g.drawRect(rec.x + point.x, rec.y + point.y,
					rec.width, rec.height);
			g.setColor(ConnectionEditor.getROveredFillColor());
			g.fillRect(rec.x + point.x, rec.y + point.y,
					rec.width, rec.height);
		}
		if(pressedItem != null){
			ConnObject owner = connModel.getOwner(pressedItem);
			Rectangle rec = owner.getRectangle(pressedItem);
			Point point = connModel.getPosition(owner);
			
			g.setColor(ConnectionEditor.getPressedColor());
			g.drawRect(rec.x + point.x, rec.y + point.y,
					rec.width, rec.height);
			g.setColor(ConnectionEditor.getPressedFillColor());
			g.fillRect(rec.x + point.x, rec.y + point.y,
					rec.width, rec.height);
		}
		if(selectedItem != null){
			ConnObject owner = connModel.getOwner(selectedItem);
			Rectangle rec = owner.getRectangle(selectedItem);
			Point point = connModel.getPosition(owner);
			
			g.setColor(ConnectionEditor.getSelectedColor());
			g.drawRect(rec.x + point.x, rec.y + point.y,
					rec.width, rec.height);
			g.setColor(ConnectionEditor.getSelectedFillColor());
			g.fillRect(rec.x + point.x, rec.y + point.y,
					rec.width, rec.height);
		}
		painting = false;
	}// end paint(Graphics)
	
	
	
	
	public static class DisconnectAction extends AbstractAction{
		private ConnectionEditor editor;
		
		public DisconnectAction(ConnectionEditor editor)
		{ super("Disconnect"); this.editor = editor;}
		
		public void actionPerformed(ActionEvent ae){
			if(editor.roveredItem == null){
				JOptionPane.showMessageDialog(null, "Nothing selected");
				return;
			}
			editor.getConnectionModel().disconnect(editor.roveredItem);
		}// end actionPerformed(ActionEvent)
	}// end class DisconnectAction
}
