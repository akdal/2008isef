package agdr.robodari.plugin.rme.gui;

import java.awt.*;
import java.awt.image.*;
import java.awt.geom.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.vecmath.*;
//import javax.media.j3d.*;

import agdr.robodari.appl.BugManager;
import agdr.robodari.comp.*;
import agdr.robodari.plugin.rme.appl.*;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.rlztion.*;
import agdr.robodari.plugin.rme.store.*;


/*
 * rover, clicked item: MainSketchEditor .
 */
public class DefaultMainSketchEditor extends JPanel implements
		SketchModelListener, KeyListener,
		MouseListener, MouseMotionListener, ComponentListener,
		MainSketchEditor, PopupMenuListener{
	/*
	private final static Color[] LINE_COLORS = new Color[]{
		new Color(0, 0, 128, 40), new Color(96, 0, 128, 40),
		new Color(0, 96, 128, 40), new Color(96, 96, 128, 40),
		new Color(0, 96, 64, 40)
	};
	*/
	private final static Color UNENABLED_BACKGROUND = new Color(245, 245, 245);
	private final static Color ENABLED_BACKGROUND = Color.white;
	
	
	private static Color multiSelection_background;
	private static Color multiSelection_borderColor;
	
	private static Stroke lineStroke;
	//private static Paint linePaint;
	private static Paint lineROverPaint;
	private static Paint lineBtnPressedPaint;
	private static Paint lineSelectedPaint;
	private static boolean isColorRandom;
	
	private static Stroke axisStroke;
	private static Paint axisPaint;
	private static Paint zAxisPaint;
	private static Paint axisHiddenPaint;
	private static Paint gridPaint;
	
	private static Stroke guideLine1Stroke;
	private static Stroke guidePointStroke;
	private static float guidePointSize;
	private static Paint guideLine1XYPaint;
	private static Paint guideLine1ZPaint;
	private static Color guideCoordColor;
	
	//private static Stroke rotationAxisStroke;
	private static Paint rotationAxisXPaint;
	private static Paint rotationAxisYPaint;
	private static Paint rotationAxisZPaint;
	
	private static float senseRange;
	
	private static boolean drawXgrid;
	private static boolean drawYgrid;
	private static double axisLength;
	
	private static float gridGap;
	private static float driftAmount;
	
	
	static{
		loadDesignTools();
	}// end static block
	
	
	private static void loadDesignTools(){
		lineStroke = new BasicStroke(1.5f, BasicStroke.CAP_ROUND,
				BasicStroke.JOIN_ROUND);
		
		multiSelection_background = new Color(50, 28, 255, 30);
		multiSelection_borderColor = new Color(50, 60, 255, 80);
		
		//linePaint = new Color(0, 0, 255, 40);
		lineROverPaint = new Color(184, 106, 11, 50);
		lineBtnPressedPaint = new Color(248, 71, 75, 50);
		lineSelectedPaint = new Color(235, 50, 50, 70);
		isColorRandom = true;
		
		axisStroke = new BasicStroke();
		axisPaint = new Color(0, 0, 255);
		zAxisPaint = new Color(0, 0, 128);
		axisHiddenPaint = new Color(190, 190, 255);
		gridPaint = new Color(230, 230, 255);
		
		guideLine1Stroke = new BasicStroke(1.5f, BasicStroke.CAP_ROUND,
				BasicStroke.JOIN_ROUND);
		guidePointSize = 7;
		guidePointStroke = new BasicStroke(guidePointSize,
				BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		guideLine1XYPaint = new Color(225, 0, 225, 70);
		guideLine1ZPaint = new Color(225, 0, 225, 50);
		guideCoordColor = new Color(200, 0, 255);
		
		rotationAxisXPaint = new Color(128, 0, 0);
		rotationAxisYPaint = new Color(196, 0, 0);
		rotationAxisZPaint = new Color(255, 0, 0);
		
		axisLength = 900;
		drawXgrid = true;
		drawYgrid = true;
		
		gridGap = 5;
		
		driftAmount = 0.01f;
		
		senseRange = 3;
		
	}// end static block
	
	public final static String INPUTMAP_PXDRIFTBYKEY = "mainSketchEditor_pxdriftbykey";
	public final static String INPUTMAP_MXDRIFTBYKEY = "mainSketchEditor_mxdriftbykey";
	public final static String INPUTMAP_PYDRIFTBYKEY = "mainSketchEditor_pydriftbykey";
	public final static String INPUTMAP_MYDRIFTBYKEY = "mainSketchEditor_mydriftbykey";
	public final static String INPUTMAP_PZDRIFTBYKEY = "mainSketchEditor_pzdriftbykey";
	public final static String INPUTMAP_MZDRIFTBYKEY = "mainSketchEditor_mzdriftbykey";
	private static void confInputAndActionMap(InputMap imap, ActionMap amap, DefaultMainSketchEditor mse){
		imap.put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), INPUTMAP_PXDRIFTBYKEY);
		imap.put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, InputEvent.CTRL_MASK),
				INPUTMAP_MXDRIFTBYKEY);
		imap.put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), INPUTMAP_PYDRIFTBYKEY);
		imap.put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, InputEvent.CTRL_MASK),
				INPUTMAP_MYDRIFTBYKEY);
		imap.put(KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0), INPUTMAP_PZDRIFTBYKEY);
		imap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0), INPUTMAP_MZDRIFTBYKEY);

		amap.put(INPUTMAP_PXDRIFTBYKEY, new XMinusDriftByKeyAction(mse));
		amap.put(INPUTMAP_MXDRIFTBYKEY, new XPlusDriftByKeyAction(mse));
		amap.put(INPUTMAP_PYDRIFTBYKEY, new YMinusDriftByKeyAction(mse));
		amap.put(INPUTMAP_MYDRIFTBYKEY, new YPlusDriftByKeyAction(mse));
		amap.put(INPUTMAP_PZDRIFTBYKEY, new ZPlusDriftByKeyAction(mse));
		amap.put(INPUTMAP_MZDRIFTBYKEY, new ZMinusDriftByKeyAction(mse));
	}// end confInputAndActionMap(InputMap, ActionMap)
	
	private final static SkObject[] NULLAARRAY = new SkObject[]{};
	
	
	public static float getSenseRange()
	{ return senseRange; }
	
	public static Color getMultiSelectionBackground()
	{ return multiSelection_background; }
	public static Color getMultiSelectionBorderColor()
	{ return multiSelection_borderColor; }
	
	public static Stroke getLineStroke()
	{ return lineStroke; }
	//public static Paint getLinePaint()
	//{ return linePaint; }
	public static Paint getLineROverPaint()
	{ return lineROverPaint; }
	public static Paint getLineBtnPressedPaint()
	{ return lineBtnPressedPaint; }
	public static Paint getLineSelectedPaint()
	{ return lineSelectedPaint; }
	/*
	public static Color getColor(int idx){
		if(isColorRandom)
			return LINE_COLORS[idx % LINE_COLORS.length];
		return LINE_COLORS[0];
	}// end getColor(int)
	*/
	public static Stroke getAxisStroke()
	{ return axisStroke; }
	public static Paint getAxisPaint()
	{ return axisPaint; }
	public static Paint getZAxisPaint()
	{ return zAxisPaint; }
	public static Paint getAxisHiddenPaint()
	{ return axisHiddenPaint; }
	public static Paint getGridPaint()
	{ return gridPaint; }
	
	//public static Stroke getRotationAxisStroke()
	//{ return rotationAxisStroke; }
	public static Paint getRotationAxisXPaint()
	{ return rotationAxisXPaint; }
	public static Paint getRotationAxisYPaint()
	{ return rotationAxisYPaint; }
	public static Paint getRotationAxisZPaint()
	{ return rotationAxisZPaint; }
	
	public static double getAxisLength()
	{ return axisLength; }
	public static boolean drawXgrid()
	{ return drawXgrid; }
	public static boolean drawYgrid()
	{ return drawYgrid; }
	
	public static Stroke getGuideLine1Stroke()
	{ return guideLine1Stroke; }
	public static Stroke getGuidePointStroke()
	{ return guidePointStroke; }
	public static Paint getGuideLine1XYPaint()
	{ return guideLine1XYPaint; }
	public static Paint getGuideLine1ZPaint()
	{ return guideLine1ZPaint; }
	public static Color getGuideCoordColor()
	{ return guideCoordColor; }
	
	public static float getDriftAmount()
	{ return driftAmount; }
	public static float getGridGap()
	{ return gridGap; }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	final static SketchEditorListener[] NULLSELARRAY = new SketchEditorListener[]{};
	
	
	
	
	
	private JPopupMenu popupMenu;
	
	private Hashtable<SkObject, Polygon> boundPolygons =
		new Hashtable<SkObject, Polygon>();
	private Hashtable<SkObject, Picture> storedPictures = 
		new Hashtable<SkObject, Picture>();
	private Hashtable<SkObject, Integer> boundPriorities = 
		new Hashtable<SkObject, Integer>();
	
	private int boundPriorCnt;
	
	private Vector<SketchEditorListener> listeners = new
			Vector<SketchEditorListener>();
	private SketchModel sketchModel;
	private SketchEditorInterface sketchEditor;
	private MouseEvent currentMouseEvent;
	
	private SkObject mouseCaptured_rOverCaptured = null;
	private ArrayList<SkObject> mouseCaptured_clickCaptured =
		new ArrayList<SkObject>();
	private ArrayList<SkObject> mouseCaptured_multiSelCaptured = 
		new ArrayList<SkObject>();
	private SkObject mouseCaptured_btnPressCaptured = null;
	private boolean mouseCaptured_working = false;
	private boolean mouseCaptured_stopFlag = false;
	
	private Point multiSelection_startp = null;
	private Point multiSelection_endp = null;
	private Rectangle multiSelection_range = new Rectangle();
	
	private JMenuItem[] menuItems = new JMenuItem[7];
	
	
	
	// drift_working: mouse dragged drift calculating
	private boolean drift_working = false;
	private DriftOption driftOption = new DriftOption();
	private float scale = 20.0f;
	
	private double projAxisX_x, projAxisX_y;
	private double projAxisY_x, projAxisY_y;
	private double projAxisZ_x, projAxisZ_y;
	
	private Point axisPoint;
	private Projector projector;
	
	
	
	
	/*public MainSketchEditor()
	{ this(new SketchModel()); }*/
	public DefaultMainSketchEditor(SketchEditorInterface sketchEditor){
		this.sketchEditor = sketchEditor;
		
		axisPoint = new Point();
		popupMenu = generatePopupMenu();
		this.setProjector(new Projector.OrthographicProjector((float)(Math.PI * 5.0 / 4.0), (float)(Math.PI * 3.0 / 4.0), 0));
		
		super.setOpaque(true);
		super.addMouseListener(this);
		super.addMouseMotionListener(this);
		super.addComponentListener(this);
		super.addKeyListener(this);
		
		super.setFocusable(true);
		super.setLayout(null);
		
		DefaultMainSketchEditor.confInputAndActionMap(this.getInputMap(), this.getActionMap(), this);
	}// end Constructor(SketchModel)
	
	private JPopupMenu generatePopupMenu(){
		JPopupMenu popupMenu = new JPopupMenu();
		
		final JMenuItem rotateItem = popupMenu.add(new RotateAction(this));
		//final JMenuItem alterLockItem = popupMenu.add(new AlterLockAction(this));
		final JMenuItem deleteItem = popupMenu.add(new DeleteAction(this));
		final JMenuItem disposeSelItem = popupMenu.add(new DisposeSelAction(this));
		final JMenuItem propertyItem = popupMenu.add(new PropertyAction(this));
		popupMenu.addSeparator();
		final JMenuItem groupItem = popupMenu.add(new GroupAction(this));
		final JMenuItem disposeGroupItem = popupMenu.add(new DisposeGroupAction(this));
		final JMenuItem boundPriorItem = popupMenu.add(new BoundPriorityLowestAction(this));
		menuItems[0] = rotateItem;
		menuItems[1] = deleteItem;
		menuItems[2] = disposeSelItem;
		menuItems[3] = propertyItem;
		menuItems[4] = groupItem;
		menuItems[5] = disposeGroupItem;
		menuItems[6] = boundPriorItem;
		
		popupMenu.addPopupMenuListener(this);
		return popupMenu;
	}// end generatePopupMenu
	
	
	
	
	
	public void loadSketch(SketchModel model){
		if(this.sketchModel != null)
			this.sketchModel.removeSketchModelListener(this);
		
		boundPolygons.clear();
		storedPictures.clear();
		boundPriorities.clear();
		
		boundPriorCnt = 0;
		
		this.sketchModel = model;
		if(this.sketchModel != null){
			super.setBackground(ENABLED_BACKGROUND);
			super.setEnabled(true);
			this.sketchModel.addSketchModelListener(this);
			
			SkObject[] objects = sketchModel.getObjects();
			for(int i = 0; i < objects.length; i++){
				this.addObject(objects[i]);
			}
		}else{
			super.setBackground(UNENABLED_BACKGROUND);
			super.setEnabled(false);
		}
		fireModelChanged();
	}// end loadSketch(SketchModel model)
	
	
	
	
	public Projector getProjector()
	{ return this.projector; }
	public void setProjector(Projector prj){
		this.projector = prj;

		float[] array;
		array = prj.project(1, 0, 0, null, 1);
		projAxisX_x = array[0];
		projAxisX_y = array[1];
		array = prj.project(0, 1, 0, null, 1);
		projAxisY_x = array[0];
		projAxisY_y = array[1];
		array = prj.project(0, 0, 1, null, 1);
		projAxisZ_x = array[0];
		projAxisZ_y = array[1];
		
		//rev_projAxisX_x = xmax - projAxisX_x +1.0;
		//rev_projAxisY_x = xmax - projAxisY_x +1.0;
		//rev_projAxisZ_x = xmax - projAxisZ_x +1.0;
		
		//rev_projAxisX_y = ymax - projAxisX_y +1.0;
		//rev_projAxisY_y = ymax - projAxisY_y +1.0;
		//rev_projAxisZ_y = ymax - projAxisZ_y +1.0;
		
		if(this.sketchModel != null){
			SkObject[] objs = this.sketchModel.getObjects();
			for(SkObject eachobj : objs)
				this.refreshObject(eachobj);
		}
		if(super.getGraphics() != null)
			repaint();
	}// end setProjector(Projector)
	
	
	
	
	/* ********************************************* 
	                       EVENTS
	************************************************/
	
	
	public void keyPressed(KeyEvent ke){}
	public void keyTyped(KeyEvent ke){}
	public void keyReleased(KeyEvent ke){}
	
	public void popupMenuCanceled(PopupMenuEvent pme){
		this.mouseCaptured_rOverCaptured = null;
		this.mouseCaptured_btnPressCaptured = null;
		repaint();
	}
	public void popupMenuWillBecomeVisible(PopupMenuEvent pme){
		if(mouseCaptured_rOverCaptured == null){
			menuItems[0].setEnabled(false);
			menuItems[1].setEnabled(false);
			menuItems[2].setEnabled(false);
			menuItems[3].setEnabled(false);
			menuItems[4].setEnabled(false);
			menuItems[5].setEnabled(false);
		}else if(mouseCaptured_clickCaptured.size() <= 1){
			menuItems[4].setEnabled(false);
		}
	}// end popupMenuWillBecomeVIsible(PopupMenuEvent)
	public void popupMenuWillBecomeInvisible(PopupMenuEvent pme){
		menuItems[0].setEnabled(true);
		menuItems[1].setEnabled(true);
		menuItems[2].setEnabled(true);
		menuItems[3].setEnabled(true);
		menuItems[4].setEnabled(true);
		menuItems[5].setEnabled(true);
	}// end popupMenuWillBecomeINvisible(PopupMenuEvent)
	
	
	
	
	public void componentHidden(ComponentEvent ce){}
	public void componentShown(ComponentEvent ce){}
	public void componentResized(ComponentEvent ce){
		this.axisPoint.x = (int)((float)this.getWidth() * 0.5f);
		this.axisPoint.y = (int)((float)this.getHeight() * 0.5f);
		
		if(this.sketchModel != null){
			SkObject[] objects = sketchModel.getObjects();
			for(SkObject eachobj : objects)
				Picture.refreshPicturePos(eachobj, storedPictures.get(eachobj), this);
		}
		repaint();
	}// end componentResized(ComponentEnveT)
	public void componentMoved(ComponentEvent ce){}
	
	public void componentRemoved(SketchModelEvent sme){
		this.removeObject(sme.getObject());
	}// end componentRemoved(SketchModelEvent)
	public void componentAdded(SketchModelEvent sme){
		this.addObject(sme.getObject());
	}// end componentAdded
	
	
	
	
	
	
	
	public void mouseClicked(MouseEvent me)
	{ this.currentMouseEvent = me; }
	public void mouseEntered(MouseEvent me)
	{ this.currentMouseEvent = me; }
	public void mouseExited(MouseEvent me)
	{ this.currentMouseEvent = me; }
	public void mousePressed(MouseEvent me){
		super.grabFocus();
		BugManager.printf("DMainSkEd : mousePressed : button : %d, btnPressCaptured : %s\n\t mouseCaptured_rOverCaptured : %s\n",
				me.getButton(),
				this.mouseCaptured_btnPressCaptured,
				this.mouseCaptured_rOverCaptured);
		this.currentMouseEvent = me; 
		
		if(me.getButton() == MouseEvent.BUTTON3){
			this.mouseCaptured_btnPressCaptured = mouseCaptured_rOverCaptured;
			return;
		}else if(mouseCaptured_rOverCaptured == null){
			this.multiSelection_startp = me.getPoint();
			return;
		}else if(sketchEditor.getCurrentCompEditor() != null)
			return;
		else if(popupMenu.isVisible())
			return;
		
		this.mouseCaptured_btnPressCaptured = mouseCaptured_rOverCaptured;
		repaint();
	}// end mousePressed(MouseEvent)
	public void mouseReleased(MouseEvent me){
		if(sketchEditor.getCurrentCompEditor() != null){
			this.mouseCaptured_btnPressCaptured = null;
			return;
		}else if(me.getButton() == MouseEvent.BUTTON3){
			this.mouseCaptured_btnPressCaptured = null;
			popupMenu.show(this, me.getX(), me.getY());
			return;
		}else if(multiSelection_startp != null){
			mouseCaptured_multiSelCaptured.clear();
			
			if(this.sketchModel == null) return;
			SkObject[] objects = this.sketchModel.getObjects();
			Polygon orgpol;
			Point pos;
			Matrix4f trans;
			float[] prjpos;
			
			for(int i = 0; i < objects.length; i++){
				orgpol = boundPolygons.get(objects[i]);
				trans = objects[i].getTransform();
				pos = new Point((int)(prjpos = this.getProjector().project(0, 0, 0, trans, this.getScale()))[0], (int)prjpos[1]);
				
				for(int j = 0; j < orgpol.npoints; j++){
					if(multiSelection_range.contains((int)orgpol.xpoints[j] + pos.x + (int)this.getAxisStartX(),
							(int)orgpol.ypoints[j] + pos.y + (int)this.getAxisStartY())){
						mouseCaptured_multiSelCaptured.add(objects[i]);
						j = 21474836;
						continue;
					}
				}
			}
			if((me.getModifiers() & InputEvent.SHIFT_MASK) == 0)
				mouseCaptured_clickCaptured.clear();
			mouseCaptured_clickCaptured.addAll(mouseCaptured_multiSelCaptured);
			
			this.mouseCaptured_rOverCaptured = null;
			this.multiSelection_startp = this.multiSelection_endp = null;
			this.multiSelection_range.x = multiSelection_range.y =
				multiSelection_range.width = multiSelection_range.height = 0;
			repaint();
			return;
		}else if(popupMenu.isVisible()) return;
		
		this.currentMouseEvent = me;

		if(driftOption.driftMode){
			driftOption.reset();
			mouseCaptured_btnPressCaptured.endDrift();
			
			boolean selected = false;
			if(!mouseCaptured_clickCaptured.contains(mouseCaptured_btnPressCaptured)){
				if((me.getModifiers() & InputEvent.SHIFT_MASK) == 0)
					mouseCaptured_clickCaptured.clear();
				mouseCaptured_clickCaptured.add(mouseCaptured_btnPressCaptured);
				selected = true;
			}
			fireComponentDrifted(mouseCaptured_btnPressCaptured);
			
			Picture pic = storedPictures.get(this.mouseCaptured_btnPressCaptured);
			Point3f origin = new Point3f(0, 0, 0);
			float[] tmpprjp = getProjector().project(origin,
					mouseCaptured_btnPressCaptured.getTransform(), this.getScale());
			Point newp = new Point((int)tmpprjp[0], (int)tmpprjp[1]);
			newp.x += this.getAxisStartX();
			newp.y += this.getAxisStartY();
			pic.pos = newp;
			
			if(selected)
				fireComponentSelected(this.mouseCaptured_btnPressCaptured);
		}else{
			if(!mouseCaptured_clickCaptured.contains(mouseCaptured_btnPressCaptured)){
				if(mouseCaptured_clickCaptured.size() != 0){
					for(SkObject eachobj : mouseCaptured_clickCaptured)
						fireComponentDeselected(eachobj);
				}
				
				if((me.getModifiers() & InputEvent.SHIFT_MASK) == 0)
					mouseCaptured_clickCaptured.clear();
				if(mouseCaptured_btnPressCaptured != null){
					mouseCaptured_clickCaptured.add(mouseCaptured_btnPressCaptured);
					//mouseCaptured_rOverCaptured = mouseCaptured_btnPressCaptured; // FIXME
					fireComponentSelected(this.mouseCaptured_btnPressCaptured);
				}
			}else{
				mouseCaptured_clickCaptured.remove(mouseCaptured_btnPressCaptured);
				fireComponentDeselected(this.mouseCaptured_btnPressCaptured);
			}
		}
		mouseCaptured_btnPressCaptured = null;
		
		//System.out.println("---- released ----");
		repaint();
	}// end mouseReleased(MouseEvent)
	public void mouseMoved(MouseEvent me){
		if(mouseCaptured_working) return;
		else if(sketchEditor.getCurrentCompEditor() != null)
			 return;
		else if(sketchModel == null) return;
		else if(popupMenu.isVisible()) return;
		
		mouseCaptured_working = true;
		
		boolean found = false;
		Point mousePos = new Point((int)me.getPoint().x - (int)this.getAxisStartX(),
				me.getPoint().y - (int)this.getAxisStartY());
		Point3f center = new Point3f(0, 0, 0);
		Point prjcenter = new Point(0, 0);
		float[] tmpprjp;
		
		SkObject[] objects = sketchModel.getObjects();
		int maxprior = 2147483647;
		for(SkObject eachobj : objects){
			if(mouseCaptured_stopFlag){
				mouseCaptured_stopFlag = true;
				break;
			}
			center.set(eachobj.getTransform().m03, eachobj.getTransform().m13,
					eachobj.getTransform().m23);
			tmpprjp = this.getProjector().project(center, null, this.getScale());
			prjcenter.x = (int)tmpprjp[0];
			prjcenter.y = (int)tmpprjp[1];
			
			mousePos.x -= prjcenter.x;
			mousePos.y -= prjcenter.y;
			boolean contains = this.getBound(eachobj).contains(mousePos);
			mousePos.x += prjcenter.x;
			mousePos.y += prjcenter.y;
			
			if(contains && maxprior > this.boundPriorities.get(eachobj)){
				found = true;
				mouseCaptured_rOverCaptured = eachobj;
				maxprior = this.boundPriorities.get(eachobj);
			}
		}
		if(!found)
			mouseCaptured_rOverCaptured = null;
		else
			repaint();
			
		
		mouseCaptured_working = false;
	}// end mouseMoved(MouseEvent)
	
	private int beforeDir = -1;
	private int beforeDir2 = -1;
	public void mouseDragged(MouseEvent me){
		if(mouseCaptured_btnPressCaptured == null){
			this.multiSelection_endp = me.getPoint();
			
			if(multiSelection_startp == null)
				multiSelection_startp = me.getPoint();
			
			multiSelection_range.x = multiSelection_startp.x;
			multiSelection_range.y = multiSelection_startp.y;
			if(multiSelection_endp.x < multiSelection_range.x)
				multiSelection_range.x = multiSelection_endp.x;
			if(multiSelection_endp.y < multiSelection_range.y)
				multiSelection_range.y = multiSelection_endp.y;
			int wid = Math.abs(multiSelection_startp.x - multiSelection_endp.x);
			int hei = Math.abs(multiSelection_startp.y - multiSelection_endp.y);
			multiSelection_range.width = wid;
			multiSelection_range.height = hei;
			
			repaint();
			return;
		}else if(sketchEditor.getCurrentCompEditor() != null)
			return;
		else if(drift_working) return;
		else if(popupMenu.isVisible()) return;
		else{
			drift_working = true;
			
			Point beforePoint = this.currentMouseEvent.getPoint();
			
			if(!driftOption.driftMode){
				driftOption.driftMode = true;
				driftOption.node = beforePoint;
				mouseCaptured_btnPressCaptured.startDrift();
				beforeDir = beforeDir2 = 0;
			}
	
			//System.out.println("--- DRAG START ---");
			Point nowPoint = me.getPoint();
			float rghXvar = nowPoint.x - driftOption.node.x;
			float rghYvar = nowPoint.y - driftOption.node.y;
			float prciseXvar = nowPoint.x - beforePoint.x;
			float prciseYvar = nowPoint.y - beforePoint.y;
			
			int newdir = DriftOption.NOTDECIDED;
			beforeDir2 = beforeDir;
			beforeDir = newdir;
			if(Math.abs(prciseXvar) + Math.abs(prciseYvar) <= 2.0f){
				if(driftOption.top() != null){
					beforeDir = newdir;
					newdir = driftOption.top().direction;
				}else{
					newdir = driftOption.getDirection(rghXvar, rghYvar);
					if(beforeDir2 == beforeDir && beforeDir != DriftOption.NOTDECIDED)
						newdir = beforeDir2;
				}
			}else{
				newdir = driftOption.getDirection(prciseXvar, prciseYvar);
				if(beforeDir2 == beforeDir && beforeDir != DriftOption.NOTDECIDED)
					newdir = beforeDir2;
			}
	
			//System.out.println("CONFIRM : p : " + driftOption.getPoint());
				
			prciseXvar = Math.abs(prciseXvar);
			prciseYvar = Math.abs(prciseYvar);
			rghXvar = Math.abs(rghXvar);
			rghYvar = Math.abs(rghYvar);
			/*
			XP = 0;
			XM = 1;
			YP = 2;
			YM = 3;
			ZP = 4;
			ZM = 5;
			 */
			//System.out.println("in mouseDragged : direction : " + newdir + " , rghXvar : " + rghXvar + " , rghYvar : " + rghYvar);
			
			if(driftOption.top() == null){
				driftOption.push(newdir);
				driftOption.node = beforePoint;
				rghXvar = prciseXvar;
				rghYvar = prciseYvar;
			}else if(newdir != driftOption.top().direction){
				//System.out.println("-- direction changed to " + newdir);
				driftOption.push(newdir);
				driftOption.node = beforePoint;
				rghXvar = prciseXvar;
				rghYvar = prciseYvar;
			}
			float len = (float)Math.sqrt(rghXvar * rghXvar + rghYvar * rghYvar);
	
			switch(driftOption.top().direction){
			case DriftOption.XP : 
			case DriftOption.XM : 
				driftOption.top().distance = /*projAxisX_len * */len / this.getScale();
				break;
			case DriftOption.YP : 
			case DriftOption.YM : 
				driftOption.top().distance = /*projAxisY_len * */len / this.getScale();
				break;
			case DriftOption.ZP : 
				//System.out.println("in mouseDragged : z plus move , dis : " + driftOption.top().distance);
			case DriftOption.ZM : 
				//System.out.println("in mouseDragged : z minus move , dis : " + driftOption.top().distance);
				driftOption.top().distance = /*projAxisZ_len * */len / this.getScale();
				break;
			}
			//System.out.println("drift : " + driftOption.xvariant +
			//		" , " + driftOption.yvariant + " , " + driftOption.zvariant);
			
			mouseCaptured_btnPressCaptured.drift(driftOption.getPoint());

			
			Point3f p = new Point3f(0, 0, 0);
			Point3f addp = driftOption.getPoint();
			mouseCaptured_btnPressCaptured.transform(p);
			p.x += addp.x;
			p.y += addp.y;
			p.z += addp.z;
			//System.out.println("paintGuideine : " + p);
			
			float[] tmpprjp = getProjector().project(p.x, p.y, p.z, null,
					this.getScale());
			Point projPoint = new Point((int)tmpprjp[0], (int)tmpprjp[1]);
			
			int asx = (int)this.getAxisStartX();
			int asy = (int)this.getAxisStartY();
			
			Picture picture = storedPictures.get(mouseCaptured_btnPressCaptured);
			picture.pos.x = asx + projPoint.x;
			picture.pos.y = asy + projPoint.y;
			repaint();
			
			this.currentMouseEvent = me;
			drift_working = false;
			//System.out.println();
		}
	}// end mouseDragged(MouseEvent me)
	
	
	
	
	
	
	
	
	
	
	
	
	
	public SketchEditorListener[] getSketchEditorListeners()
	{ return listeners.toArray(NULLSELARRAY); }
	public void addSketchEditorListener(SketchEditorListener sel)
	{ listeners.add(sel); }
	public boolean containsSketchEditorListener(SketchEditorListener sel)
	{ return listeners.contains(sel); }
	public void removeSketchEditorListener(SketchEditorListener sel)
	{ listeners.remove(sel); }
	protected void fireComponentDrifted(SkObject appr){
		SketchEditorEvent event = new SketchEditorEvent(appr, this);
		for(SketchEditorListener eachsel : listeners)
			eachsel.componentDrifted(event);
	}// end fireComponentDrifted(SkObject)
	protected void fireComponentRotated(SkObject appr){
		SketchEditorEvent event = new SketchEditorEvent(appr, this);
		for(SketchEditorListener eachsel : listeners)
			eachsel.componentRotated(event);
	}// edn fireComponentRotated(SkObject appr)
	protected void fireComponentSelected(SkObject appr){
		SketchEditorEvent event = new SketchEditorEvent(appr, this);
		for(SketchEditorListener eachsel : listeners)
			eachsel.componentSelected(event);
	}// end fireComponentSelected(SkObject)
	protected void fireComponentDeselected(SkObject appr){
		SketchEditorEvent event = new SketchEditorEvent(appr, this);
		for(SketchEditorListener eachsel : listeners)
			eachsel.componentDeselected(event);
	}// end fireComponentSelected(SkObject)
	protected void fireModelChanged(){
		SketchEditorEvent event = new SketchEditorEvent(null, this);
		for(SketchEditorListener eachsel : listeners)
			eachsel.modelChanged(event);
	}// end fireComponentSelected(SkObject)
	
	
	
	
	
	
	
	
	
	
	public SkObject[] getObjects()
	{ return sketchModel.getObjects(); }
	public int getObjectsCount(){
		if(sketchModel != null)
			return sketchModel.size();
		return 0;
	}// end getObjectsCount()
	public SkObject getObject(RobotComponent rc){
		return sketchModel.getObject(rc);
	}// end getObject(RobotCompoennt)
	public boolean containsObject(SkObject appr)
	{ return sketchModel.containsObject(appr); }
	public void refreshObject(SkObject appr){
		//if(BugManager.DEBUG)
			System.out.println("DefaultMainSketchEditor : refreshObject() : " + appr);
		this.refreshBound(appr);
		this.createPicture(appr);
		this.repaint();
	}// end refreshObject(SkObject)
	
	
	private void addObject(SkObject obj){
		if(BugManager.DEBUG)
			BugManager.printf("DefautMSE : addObject() : %s\n", obj);
		boundPolygons.put(obj, getBound(obj));
		boundPriorities.put(obj, new Integer(++boundPriorCnt));
		storedPictures.put(obj, createPicture(obj));
		if(BugManager.DEBUG)
			BugManager.printf("DefaultMSE : addObject() end\n");
		repaint();
	}// end addObject(SkObject)
	private void removeObject(SkObject obj){
		removeObject(obj, true);
	}// end removeObject(SkObject)
	private void removeObject(SkObject obj, boolean repaint){
		boundPolygons.remove(obj);
		storedPictures.remove(obj);
		if(repaint)
			repaint();
	}// end removeObject(SkObject, boolean)
	
	
	public float getScale ()
	{ return scale; }
	public void setScale(float scale){
		this.scale = scale;
		Set<SkObject> objects = storedPictures.keySet();
		for(SkObject eachobj : objects){
			this.refreshObject(eachobj);
		}
		repaint();
	}// end setScale()
	
	
	
	public float getAxisStartX()
	{ return axisPoint.x; }
	public float getAxisStartY()
	{ return axisPoint.y; }
	
	
	
	
	
	
	/*
	public void refreshPicturePos(SkObject obj){
		Picture picture = storedPictures.get(obj);
		
		picture.pos = getProjector().project(0, 0, 0,
				obj.getTransform(), this.getScale());
		picture.pos.x += this.getAxisStartX();
		picture.pos.y += this.getAxisStartY();
	}// end refreshPicturePos(SkObject)
	*/
	public Picture getPicture(SkObject obj)
	{ return storedPictures.get(obj); }
	public Picture createPicture(SkObject obj){
		System.out.println("MSE : createPicture(SkObj)");
		Picture picture = storedPictures.get(obj);
		if(picture == null){
			picture = new Picture(this);
			storedPictures.put(obj, picture);
		}
		Polygon p = getBound(obj);
		Point min = new Point(21474836, 21474836);
		Point max = new Point(-21474836, -21474836);
		for(int i = 0; i < p.npoints; i++){
			if(min.x > p.xpoints[i])
				min.x = p.xpoints[i];
			if(min.y > p.ypoints[i])
				min.y = p.ypoints[i];
			
			if(max.x < p.xpoints[i])
				max.x = p.xpoints[i];
			if(max.y < p.ypoints[i])
				max.y = p.ypoints[i];
		}
		if(max.x == min.x)
			max.x++;
		if(max.y == min.y)
			max.y++;
		Picture.resetPicture(obj, picture, this, max, min, false);
		return picture;
	}// end createPicture(SkObject)
	
	public Polygon getBound(SkObject appr){
		if(!boundPolygons.containsKey(appr)){
			refreshBound(appr);
		}
		Polygon p = boundPolygons.get(appr);
		return p;
	}// end getBound(SkObject)
	
	public void setBoundPriorityLowest(SkObject appr){
		boundPriorities.put(appr, new Integer(++boundPriorCnt));
	}// end setBoundPriority
	
	public void refreshBound(SkObject obj){
		Point3f[] points3f = obj.getAllPoints();
		
		System.out.println("MSE : refreshBound : obj.getAllPts().length :" + points3f.length);
		
		Matrix4f matrix = new Matrix4f(obj.getTransform());
		Point[] points = new Point[points3f.length];
		matrix.m03 = matrix.m13 = matrix.m23 = 0;
		float[] tmpprjp;
		
		for(int i = 0; i < points.length; i++){
			tmpprjp = this.getProjector().project(points3f[i], matrix, this.getScale());
			points[i] = new Point((int)tmpprjp[0], (int)tmpprjp[1]);
			//if(i % 100 == 0)
			//	System.out.println(points[i]);
		}
		//for(int i = 0; i < points.length; i++)
		//	System.out.println(i + " : (" + (points[i].x - editor.getCenterX()) + " , " + (points[i].y - editor.getCenterY()) + ")");
		
		Calculator bc = new Calculator();
		bc.setPoints(points);
		System.out.println("MSE : refreshBound : points : " + points.length);
		Polygon projectedBound = bc.convexHull();
		
		projectedBound = bc.expand(projectedBound,
				DefaultMainSketchEditor.getSenseRange());
		
		boundPolygons.put(obj, projectedBound);
	}// end refreshBound()
	
	
	
	
	
	
	
	
	public SketchModel getModel()
	{ return sketchModel; }
	
	
	
	
	
	
	
	
	
	
	public SkObject[] getSelectedItems()
	{ return mouseCaptured_clickCaptured.toArray(NULLAARRAY); }
	public void setSelected(SkObject object, boolean selected){
		if(object == null)
			return;
		
		if(selected){
			if(mouseCaptured_clickCaptured.contains(object))
				return;
			mouseCaptured_clickCaptured.add(object);
		}else{
			if(!mouseCaptured_clickCaptured.contains(object))
				return;
			mouseCaptured_clickCaptured.remove(object);
		}
		repaint();
	}// end setSelected(SkObject, boolean)
	public void disposeSelection(SkObject appr){
		if(mouseCaptured_btnPressCaptured == appr)
			disposeDrift();
		mouseCaptured_clickCaptured.remove(appr);
		repaint();
	}// end disposeSelectedItem(SkObject)
	public void disposeAllSelections(){
		disposeDrift();
		mouseCaptured_clickCaptured.clear();
		repaint();
	}// end disposeAll()
	public SkObject getROvered()
	{ return mouseCaptured_rOverCaptured; }
	

	
	public void startRotation(SkObject obj){
		if(obj == null){
			return;
		}else if(!sketchModel.containsObject(obj)){
			return;
		}

		super.repaint(0, 0, 2000, 1500);
		
		RotationEditor reditor = sketchEditor.getRotationEditor();
		reditor.startEditing(obj);
		sketchEditor.setCurrentCompEditor(reditor);
	}// end startRotation(RobotComponent rc)
	public void startCompEdit(SkObject obj){
		if(obj == null){
			return;
		}
		if(sketchEditor.getCurrentCompEditor() != null)
			sketchEditor.setCurrentCompEditor(null);
		
		if(obj.isGroup()){
			GroupEditor grpEditor = this.sketchEditor.getGroupEditor();

			grpEditor.startEditing(obj);
			sketchEditor.setCurrentCompEditor(grpEditor);
		}else{
			CompEditor currentCompEditor = this.sketchEditor
					.getStoredCompEditor(obj
					.getRobotComponent().getClass());
			if(currentCompEditor == null){
				ExceptionHandler.noMatchingEditor(obj.getRobotComponent().getClass());
				return;
			}
			try{
				currentCompEditor.startEditing(obj);
			}catch(Exception excp){
				BugManager.log(excp, true);
			}
			sketchEditor.setCurrentCompEditor(currentCompEditor);
		}
	}// end startCompEdit(SkObject)
	
	/*
	public void finishRotation(){
		if(this.rotationTarget == null) return;
		
		Matrix4d trans = rotationEditor.getTransform();
		trans.m03 = rotationTarget.getTransform().m03;
		trans.m13 = rotationTarget.getTransform().m13;
		trans.m23 = rotationTarget.getTransform().m23;
		this.rotationTarget.setTransform(trans);
		
		this.refreshObject(rotationTarget);
		System.out.println("rotation finished!");
		
		hideRotationEditPanel();
		fireComponentRotated(rotationTarget);
		this.rotationTarget = null;
	}// end endRotation(SkObject obj)*/
	/*
	private void showRotationEditPanel(){
		System.out.println("showRotationEditPanel()");
		if(!this.rotationEditor.isEnabled())
			this.rotationEditor.setEnabled(true);
		this.rotationEditor.setBounds(this.getWidth() / 2 -
				RotationEditor.getDefaultSize().width / 2,
				this.getHeight() / 2 - RotationEditor.getDefaultSize().height / 2,
				RotationEditor.getDefaultSize().width,
				RotationEditor.getDefaultSize().height);
		repaint();
	}// edn showRotationEditPanel()
	private void hideRotationEditPanel(){
		System.out.println("hideRotationPanel()");
		this.rotationEditor.setEnabled(false);
		this.rotationEditor.setBounds(0, 0, 0, 0);
		repaint();
	}// end hideRotationEditPanel()
	*/
	
	
	public void disposeDrift(){
		if(!(mouseCaptured_btnPressCaptured != null && driftOption.driftMode == true))
			return;
		
		driftOption.reset();
		mouseCaptured_btnPressCaptured.endDrift();
		
		if(!mouseCaptured_clickCaptured.contains(mouseCaptured_btnPressCaptured)){
			mouseCaptured_clickCaptured.add(mouseCaptured_btnPressCaptured);
		}
	}// end disposeDrift()
	/*
	public void disposeRotation(){
		if(rotationTarget == null)
			return;
		
		rotationTarget = null;
		hideRotationEditPanel();
		// rotation finished
		// rotationTarget.setTransform(rotationEditor.getTransform());
		
		if(mouseCaptured_btnPressCaptured != null && 
				!mouseCaptured_clickCaptured.
				contains(mouseCaptured_btnPressCaptured)){
			mouseCaptured_clickCaptured.add(mouseCaptured_btnPressCaptured);
		}
	}// end disposeRotation()
	*/
	
	
	
	
	
	
	private boolean painting = false;
	private AffineTransformOp bufimageop = new AffineTransformOp(new AffineTransform(),
			AffineTransformOp.TYPE_BILINEAR);
	public void paint(Graphics g){
		if(g == null) return;
		else if(painting) return;

		super.paint(g);
		try{
			painting = true;
	
			Graphics2D g2d = (Graphics2D)g;
			
			//g2d.setColor(Color.white);
			//g2d.fillRect(0, 0, 2000, 1200);
			SketchEditorUtility.drawGrid(this, g2d, DefaultMainSketchEditor.getAxisStroke(), DefaultMainSketchEditor.getAxisHiddenPaint(),
					DefaultMainSketchEditor.getAxisLength() * 0.85);
			SketchEditorUtility.drawAxis(this, g2d,
					DefaultMainSketchEditor.getAxisStroke(),
					DefaultMainSketchEditor.getAxisPaint(),
					DefaultMainSketchEditor.getAxisHiddenPaint(),
					DefaultMainSketchEditor.getZAxisPaint(),
					(float)DefaultMainSketchEditor.getAxisLength());
			if(sketchModel == null){
				painting = false;
				return;
			}
			
			g2d.setStroke(DefaultMainSketchEditor.getLineStroke());
			
			SkObject[] objects = sketchModel.getObjects();
			for(int i = 0; i < objects.length; i++){
				SkObject obj = objects[i];
				
				Picture pic = storedPictures.get(obj);
				
				if(this.mouseCaptured_btnPressCaptured == obj){
					//System.out.println("Pressed item exists!");
					//obj.paint(g2d);
					g2d.drawImage(pic.pressed, bufimageop,
							-pic.centerPos.x + pic.pos.x,
							-pic.centerPos.y + pic.pos.y);
	
					if(driftOption.driftMode)
						drawGuideLine(g2d, obj, false);
				}else if(this.mouseCaptured_rOverCaptured == obj){
					//System.out.println("ROvered item exists!");
					g2d.drawImage(pic.rOvered, bufimageop,
							-pic.centerPos.x + pic.pos.x,
							-pic.centerPos.y + pic.pos.y);
					g2d.drawImage(pic.normal, bufimageop,
							-pic.centerPos.x + pic.pos.x,
							-pic.centerPos.y + pic.pos.y);
	
					if(this.mouseCaptured_clickCaptured.contains(obj) &&
							!driftOption.driftMode)
						drawGuideLine(g2d, obj, true);
				}else if(this.mouseCaptured_clickCaptured.contains(obj)){
					//System.out.println("Clicked item exists!");
					g2d.drawImage(pic.selected, bufimageop,
							-pic.centerPos.x + pic.pos.x,
							-pic.centerPos.y + pic.pos.y);
					//obj.paint(g2d);
	
					if(!driftOption.driftMode)
						drawGuideLine(g2d, obj, true);
				}else{
					g2d.drawImage(pic.normal, bufimageop,
							-pic.centerPos.x + pic.pos.x,
							-pic.centerPos.y + pic.pos.y);
				}
			}
			
			if(this.multiSelection_endp != null){
				g2d.setColor(DefaultMainSketchEditor.getMultiSelectionBackground());
				g2d.fillRect(this.multiSelection_range.x, multiSelection_range.y,
						multiSelection_range.width, multiSelection_range.height);
				g2d.setColor(DefaultMainSketchEditor.getMultiSelectionBorderColor());
				g2d.drawRect(this.multiSelection_range.x, multiSelection_range.y,
						multiSelection_range.width, multiSelection_range.height);
			}
			
			if(sketchEditor.getCurrentCompEditor() != null){
				g2d.setColor(new Color(0, 0, 0, 60));
				g2d.fillRect(0, 0, 2000, 1500);
			}
			painting = false;
			//System.out.println("---- end PAINT ---- ");
		}catch(Exception excp){
			BugManager.log(excp, true);
		}
	}// end paint(Graphics)

	
	private void drawGuideLine(Graphics2D g2d, SkObject app, boolean ispoint){
		Stroke defaultStroke = g2d.getStroke();
		Paint defaultPaint = g2d.getPaint();
		
		Point3f p = new Point3f(0, 0, 0);
		Point3f addp = driftOption.getPoint();
		app.transform(p);
		p.x += addp.x;
		p.y += addp.y;
		p.z += addp.z;
		//System.out.println("paintGuideine : " + p);
		
		float[] tmpprjp;
		tmpprjp = getProjector().project(p.x, 0, 0, null,
				this.getScale());
		Point tox00line = new Point((int)tmpprjp[0], (int)tmpprjp[1]);
		
		tmpprjp = getProjector().project(p.x, 0, p.z, null,
				this.getScale());
		Point tox0zline = new Point((int)tmpprjp[0], (int)tmpprjp[1]);
		
		tmpprjp = getProjector().project(0, p.y, 0, null,
				this.getScale());
		Point to0y0line = new Point((int)tmpprjp[0], (int)tmpprjp[1]);
		
		tmpprjp = getProjector().project(p.x, p.y, 0, null,
				this.getScale());
		Point toxy0line = new Point((int)tmpprjp[0], (int)tmpprjp[1]);
		
		tmpprjp = getProjector().project(p.x, p.y, p.z, null,
				this.getScale());
		Point toxyzline = new Point((int)tmpprjp[0], (int)tmpprjp[1]);
		
		int asx = (int)this.getAxisStartX();
		int asy = (int)this.getAxisStartY();

		g2d.setPaint(DefaultMainSketchEditor.getGuideLine1XYPaint());
		g2d.setStroke(DefaultMainSketchEditor.getGuideLine1Stroke());
		g2d.drawLine(asx, asy, asx + tox00line.x, asy + tox00line.y);
		g2d.drawLine(asx + tox00line.x, asy + tox00line.y,
				asx + toxy0line.x, asy + toxy0line.y);
		g2d.drawLine(asx + to0y0line.x, asy + to0y0line.y,
				asx + toxy0line.x, asy + toxy0line.y);
		g2d.drawLine(asx + tox0zline.x, asy + tox0zline.y,
				asx + toxyzline.x, asy + toxyzline.y);
		
		g2d.setPaint(DefaultMainSketchEditor.getGuideLine1ZPaint());
		g2d.setStroke(DefaultMainSketchEditor.getGuideLine1Stroke());
		g2d.drawLine(toxy0line.x + asx, toxy0line.y + asy,
				toxyzline.x + asx, toxyzline.y + asy);
		g2d.drawLine(tox00line.x + asx, tox00line.y + asy,
				tox0zline.x + asx, tox0zline.y + asy);
		
		
		g2d.setColor(DefaultMainSketchEditor.getGuideCoordColor());
		StringBuffer cnt = new StringBuffer("(");

		String szx = String.format("%.5g", p.x);
		String szy = String.format("%.5g", p.y);
		String szz = String.format("%.5g", p.z);
		
		cnt.append(szx).append(" , ").append(szy).append(" , ").append(szz);
		cnt.append(")");
		g2d.drawString(cnt.toString(), asx + toxy0line.x, asy + toxy0line.y);
		
		g2d.setPaint(defaultPaint);
		g2d.setStroke(defaultStroke);
	}// end paintGuideLine(Graphics2D)
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	class DriftOption{
		public final static int NOTDECIDED = -1;
		public final static int XP = 0;
		public final static int XM = 1;
		public final static int YP = 2;
		public final static int YM = 3;
		public final static int ZP = 4;
		public final static int ZM = 5;
		
		public Point node;
		public boolean driftMode;
		public Vector<Movement> moves;
		
		
		public DriftOption(){
			moves = new Vector<Movement>();
			reset();
		}// end Constructor()
		
		public void reset(){
			node = null;
			moves.clear();
			driftMode = false;
		}// ed reset()
		
		public Movement top(){
			if(moves.size() == 0)return null;
			return moves.get(moves.size() - 1);
		}// end top()
		
		public Movement push(int dir)
		{ moves.add(new Movement(dir, 0)); return top(); }
		
		
		public Point3f getPoint(){
			Point3f drift_variant = new Point3f();
			for(DriftOption.Movement eachmov : moves){
				if(eachmov.direction == DriftOption.XP)
					drift_variant.x += eachmov.distance;
				else if(eachmov.direction == DriftOption.XM)
					drift_variant.x -= eachmov.distance;
				else if(eachmov.direction == DriftOption.YP)
					drift_variant.y += eachmov.distance;
				else if(eachmov.direction == DriftOption.YM)
					drift_variant.y -= eachmov.distance;
				else if(eachmov.direction == DriftOption.ZP)
					drift_variant.z += eachmov.distance;
				else if(eachmov.direction == DriftOption.ZM)
					drift_variant.z -= eachmov.distance;
			}
			return drift_variant;
		}// end getPoint()
		
		
		
		public int getDirection(double xvar, double yvar){
			if(xvar == 0.0 && yvar == 0.0)
				return 0; // x
			// vec A = xvar hat x + yvar hat y,
			// vec x' = proj(hat x),
			// vec y' = proj(hat y),
			// vec z' = proj(hat z);
			// min(vec A cross vec x', vec A cross vec y', vec A cross vec z') is direction
			
			// xvar * y
			double xdirip = xvar * projAxisX_x + yvar * projAxisX_y;
			double absxdirip = Math.abs(xdirip);
			double ydirip = xvar * projAxisY_x + yvar * projAxisY_y;
			double absydirip = Math.abs(ydirip);
			double zdirip = xvar * projAxisZ_x + yvar * projAxisZ_y;
			double abszdirip = Math.abs(zdirip);
			
			
			if(absxdirip > absydirip && absxdirip > abszdirip){
				// direction is x
				if(xdirip > 0)
					return XP;
				else
					return XM;
			}else if(absydirip > absxdirip && absydirip > abszdirip){
				if(ydirip > 0)
					return YP;
				else
					return YM;
			}else{
				if(zdirip > 0)
					return ZP;
				else 
					return ZM;
			}
			/*
			 * old version..
			double len = Math.hypot(xvar, yvar);
			xvar = -1.0 * xvar / len; // = cos theta
			
			double theta = Math.acos(xvar);
			if(yvar < 0)
				theta = 2.0 * Math.PI - theta;
			theta += Math.PI;
			if(theta >= Math.PI * 2.0)
				theta -= Math.PI * 2.0;
			double xpdiff = Math.abs(DRIFTCONST_XP - theta);
			double xmdiff = Math.abs(DRIFTCONST_XM - theta);
			double ypdiff = Math.abs(DRIFTCONST_YP - theta);
			double ymdiff = Math.abs(DRIFTCONST_YM - theta);
			double zpdiff = Math.abs(DRIFTCONST_ZP - theta);
			double zmdiff = Math.abs(DRIFTCONST_ZM - theta);
			
			double val = Math.min(xpdiff, Math.min(xmdiff, Math.min(ypdiff,
					Math.min(ymdiff, Math.min(zpdiff, zmdiff)))));
			//System.out.println("in getDirection : angle = " + Math.toDegrees(theta) + " , min : " + val);
			//System.out.println("xm : " + xmdiff + " , xp : " + xpdiff);
					
			if(val == ymdiff){
				//System.out.println("diff = " + Math.toDegrees(ymdiff));
				return YM;
			}else if(val == xmdiff){
				//System.out.println("diff = " + Math.toDegrees(xmdiff));
				return XM;
			}else if(val == xpdiff){
				//System.out.println("diff = " + Math.toDegrees(xpdiff));
				return XP;
			}else if(val == ypdiff){
				//System.out.println("diff = " + Math.toDegrees(ypdiff));
				return YP;
			}else if(val == zpdiff){
				//System.out.println("diff = " + Math.toDegrees(zpdiff));
				return ZP;
			}else if(val == zmdiff){
				//System.out.println("diff = " + Math.toDegrees(zmdiff));
				return ZM;
			}
			return NOTDECIDED;
			*/
		}// end drift_getDirection(float, float)
		
		
		
		
		
		class Movement{
			int direction;
			float distance;
			
			public Movement(int dir, float dis)
			{ this.direction = dir; this.distance = dis; }
		}
	}// end class DriftOption
	
	
	
	
	

	public static class XPlusDriftByKeyAction extends AbstractAction{
		private DefaultMainSketchEditor editor;
		
		public XPlusDriftByKeyAction(DefaultMainSketchEditor editor)
		{ this.editor = editor; }
		
		public void actionPerformed(ActionEvent ae){
			float jumpamt = DefaultMainSketchEditor.getDriftAmount() * (float)editor.getScale();
			
			for(SkObject eachobj : editor.mouseCaptured_clickCaptured){
				Point3f startp = new Point3f(0, 0, 0);
				startp.x = -jumpamt;
				eachobj.startDrift();
				eachobj.drift(startp);
				eachobj.endDrift();
				Picture.refreshPicturePos(eachobj, editor.storedPictures.get(eachobj), editor);
				editor.fireComponentDrifted(eachobj);
			}
			editor.repaint();
		}// end actionPerformed(ActionEvent)
	}// end action XDriftByKeyAction
	public static class XMinusDriftByKeyAction extends AbstractAction{
		private DefaultMainSketchEditor editor;
		
		public XMinusDriftByKeyAction(DefaultMainSketchEditor editor)
		{ this.editor = editor; }
		
		public void actionPerformed(ActionEvent ae){
			float jumpamt = -DefaultMainSketchEditor.getDriftAmount() * (float)editor.getScale();
			
			for(SkObject eachobj : editor.mouseCaptured_clickCaptured){
				Point3f startp = new Point3f(0, 0, 0);
				startp.x = -jumpamt;
				eachobj.startDrift();
				eachobj.drift(startp);
				eachobj.endDrift();
				Picture.refreshPicturePos(eachobj, editor.storedPictures.get(eachobj), editor);
				editor.fireComponentDrifted(eachobj);
			}
			editor.repaint();
		}// end actionPerformed(ActionEvent)
	}// end action XDriftByKeyAction
	public static class YPlusDriftByKeyAction extends AbstractAction{
		private DefaultMainSketchEditor editor;
		
		public YPlusDriftByKeyAction(DefaultMainSketchEditor editor)
		{ this.editor = editor; }
		
		public void actionPerformed(ActionEvent ae){
			float jumpamt = DefaultMainSketchEditor.getDriftAmount() * (float)editor.getScale();
			
			for(SkObject eachobj : editor.mouseCaptured_clickCaptured){
				Point3f startp = new Point3f(0, 0, 0);
				startp.y = -jumpamt;
				eachobj.startDrift();
				eachobj.drift(startp);
				eachobj.endDrift();
				Picture.refreshPicturePos(eachobj, editor.storedPictures.get(eachobj), editor);
				editor.fireComponentDrifted(eachobj);
			}
			editor.repaint();
		}// end actionPerformed(ActionEvent)
	}// end action XDriftByKeyAction
	public static class YMinusDriftByKeyAction extends AbstractAction{
		private DefaultMainSketchEditor editor;
		
		public YMinusDriftByKeyAction(DefaultMainSketchEditor editor)
		{ this.editor = editor; }
		
		public void actionPerformed(ActionEvent ae){
			float jumpamt = -DefaultMainSketchEditor.getDriftAmount() * (float)editor.getScale();
			
			for(SkObject eachobj : editor.mouseCaptured_clickCaptured){
				Point3f startp = new Point3f(0, 0, 0);
				startp.y = -jumpamt;
				eachobj.startDrift();
				eachobj.drift(startp);
				eachobj.endDrift();
				Picture.refreshPicturePos(eachobj, editor.storedPictures.get(eachobj), editor);
				editor.fireComponentDrifted(eachobj);
			}
			editor.repaint();
		}// end actionPerformed(ActionEvent)
	}// end action XDriftByKeyAction
	public static class ZPlusDriftByKeyAction extends AbstractAction{
		private DefaultMainSketchEditor editor;
		
		public ZPlusDriftByKeyAction(DefaultMainSketchEditor editor)
		{ this.editor = editor; }
		
		public void actionPerformed(ActionEvent ae){
			float jumpamt = DefaultMainSketchEditor.getDriftAmount() * (float)editor.getScale();
			
			for(SkObject eachobj : editor.mouseCaptured_clickCaptured){
				Point3f startp = new Point3f(0, 0, 0);
				startp.z = jumpamt;
				eachobj.startDrift();
				eachobj.drift(startp);
				eachobj.endDrift();
				Picture.refreshPicturePos(eachobj, editor.storedPictures.get(eachobj), editor);
				editor.fireComponentDrifted(eachobj);
			}
			editor.repaint();
		}// end actionPerformed(ActionEvent)
	}// end action XDriftByKeyAction
	public static class ZMinusDriftByKeyAction extends AbstractAction{
		private DefaultMainSketchEditor editor;
		
		public ZMinusDriftByKeyAction(DefaultMainSketchEditor editor)
		{ this.editor = editor; }
		
		public void actionPerformed(ActionEvent ae){
			float jumpamt = -DefaultMainSketchEditor.getDriftAmount() * (float)editor.getScale();
			
			for(SkObject eachobj : editor.mouseCaptured_clickCaptured){
				Point3f startp = new Point3f(0, 0, 0);
				startp.z = jumpamt;
				eachobj.startDrift();
				eachobj.drift(startp);
				eachobj.endDrift();
				Picture.refreshPicturePos(eachobj, editor.storedPictures.get(eachobj), editor);
				editor.fireComponentDrifted(eachobj);
			}
			editor.repaint();
		}// end actionPerformed(ActionEvent)
	}// end action XDriftByKeyAction
	
	
	
	public static class DisposeSelAction extends AbstractAction{
		private DefaultMainSketchEditor editor;
		
		public DisposeSelAction(DefaultMainSketchEditor editor){
			super("Dispose selection");
			this.editor = editor;
		}// end Constructor(MainSketchEditor)
		
		public void actionPerformed(ActionEvent ae){
			if(!editor.mouseCaptured_clickCaptured.
					contains(editor.mouseCaptured_rOverCaptured))
				editor.mouseCaptured_clickCaptured.
					add(editor.mouseCaptured_rOverCaptured);
			else
				editor.disposeAllSelections();
		}// end actionPerformed(ActionEvent)
	}// end action DisposeSelAction
	public static class PropertyAction extends AbstractAction{
		private DefaultMainSketchEditor editor;
		
		public PropertyAction(DefaultMainSketchEditor editor){
			super("Properties..");
			this.editor = editor;
		}// end Constructor(MainSketchEditor)
		
		public void actionPerformed(ActionEvent ae){
			this.editor.startCompEdit(this.editor.mouseCaptured_rOverCaptured);
		}// end actionPerformed(ActionEvent)
	}// end action DisposeSelAction
	public static class DeleteAction extends AbstractAction{
		private DefaultMainSketchEditor editor;
		
		public DeleteAction(DefaultMainSketchEditor editor){
			super("Delete..");
			this.editor = editor;
		}// end Constructor(MainSketchEditor)
		
		public void actionPerformed(ActionEvent ae){
			if(this.editor.mouseCaptured_rOverCaptured == null)
				return;
			
			int option = JOptionPane.showConfirmDialog(editor, "Are you sure you want to delete?",
					"", JOptionPane.YES_NO_OPTION);
			if(option == JOptionPane.YES_OPTION)
				this.editor.getModel().removeObject(
						this.editor.mouseCaptured_rOverCaptured);
		}// end actionPerformed(ActionEvent)
	}// end action DeleteAction
	public static class RotateAction extends AbstractAction{
		private DefaultMainSketchEditor editor;
		
		public RotateAction(DefaultMainSketchEditor editor){
			super("Rotation..");
			this.editor = editor;
		}// end Constructor(MainSketchEditor)
		
		public void actionPerformed(ActionEvent ae){
			if(this.editor.mouseCaptured_rOverCaptured == null)
				return;
			this.editor.startRotation(this.editor.mouseCaptured_rOverCaptured);
		}// end actionPerformed(ActionEvent)
	}// end action RotateAction
	public static class GroupAction extends AbstractAction{
		private DefaultMainSketchEditor editor;
		
		public GroupAction(DefaultMainSketchEditor editor){
			super("Group..");
			this.editor = editor;
		}// end Constructor(MainSketchEditor)
		
		public void actionPerformed(ActionEvent ae){
			try{
				SkObject[] objs = editor.getSelectedItems();
				if(objs == null || objs.length <= 1) return;
				
				Matrix4f grouptrans = new Matrix4f();
				Matrix4f centertrans = objs[0].getTransform();
				grouptrans.m03 = centertrans.m03;
				grouptrans.m13 = centertrans.m13;
				grouptrans.m23 = centertrans.m23;
				grouptrans.m00 = grouptrans.m11 = grouptrans.m22 = grouptrans.m33 = 1;
				SkObject groupobj = new SkObject("Group" +
						editor.getModel().requestGroupNumber(), grouptrans);
				
				for(int i = 0; i < objs.length; i++){
					Matrix4f trans = objs[i].getTransform();
					trans.m03 -= grouptrans.m03;
					trans.m13 -= grouptrans.m13;
					trans.m23 -= grouptrans.m23;
					groupobj.addChild(objs[i]);
					editor.getModel().removeObject(objs[i]);
				}
				BugManager.printf("GroupAction : actionPerformed : call model.addObject : %s\n", groupobj);
				SketchModel model = editor.getModel();
				BugManager.printf("GroupAction : actionPf : model : %s, model == null : %b\n", model, model == null);
				model.addObject(groupobj);
				BugManager.printf("GroupAction : end actionPerformed\n");
			}catch(Exception excp){
				BugManager.log(excp, true);
			}
		}// end actionPerformed(ActionEvent)
	}// end action GroupAction
	public static class DisposeGroupAction extends AbstractAction{
		private DefaultMainSketchEditor editor;

		public DisposeGroupAction(DefaultMainSketchEditor editor){
			super("Dispose group");
			this.editor = editor;
		}// end Constructor(MainSketchEditor)
		
		public void actionPerformed(ActionEvent ae){
			try{
				SkObject selected = this.editor.mouseCaptured_rOverCaptured;
				if(!selected.isGroup())
					return;
				editor.getModel().removeObject(selected);
				SkObject[] children = selected.getChildren();
				for(int i = 0; i < children.length; i++){
					Matrix4f matrix = new Matrix4f(selected.getTransform());
					matrix.mul(children[i].getTransform());
					children[i].setTransform(matrix);
					editor.getModel().addObject(children[i]);
				}
			}catch(Exception excp){
				BugManager.log(excp, true);
			}
		}
	}// end class DisposeGroupAction
	public static class BoundPriorityLowestAction extends AbstractAction{
		private DefaultMainSketchEditor dmse;
		
		public BoundPriorityLowestAction(DefaultMainSketchEditor e)
		{ super("Go Bottom"); this.dmse = e; }
		
		public void actionPerformed(ActionEvent ae){
			try{
				SkObject selected = this.dmse.mouseCaptured_rOverCaptured;
				dmse.setBoundPriorityLowest(selected);
			}catch(Exception excp){
				BugManager.log(excp, true);
			}
		}// end actionPerformed
	}// end class BPLA
}