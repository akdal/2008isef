package agdr.robodari.plugin.rme.gui;

import java.awt.*;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;


import agdr.robodari.comp.Note;
import agdr.robodari.comp.RobotComponent;
import agdr.robodari.gui.*;
import agdr.robodari.plugin.rme.edit.*;

public class GroupEditor extends CompEditor{
	private JTabbedPane tabbedPane;
	
	private JPanel propertyPanel;
	private NoteEditor noteEditor;
	private JTextField nameField;
	
	private SkObject object;
	
	
	public GroupEditor(SketchEditorInterface manager, Frame parent){
		super(manager, parent, "Group Editor");
		__set();
	}// end Constructor(EditorManager)
	public GroupEditor(SketchEditorInterface manager, Dialog dialog){
		super(manager, dialog, "Group Editor");
		__set();
	}// end Constructor
	private void __set(){
		initPropertyPanel();
		tabbedPane = new JTabbedPane();
		tabbedPane.setTabPlacement(JTabbedPane.LEFT);
		tabbedPane.addTab("Properties", propertyPanel);
		
		super.getEditorPanel().setLayout(new BorderLayout());
		super.getEditorPanel().add(tabbedPane, BorderLayout.CENTER);
		
		super.setSize(450, 400);
	}// end __set()
	private void initPropertyPanel(){
		propertyPanel = new JPanel();
		propertyPanel.setLayout(new BorderLayout());
		nameField = new JTextField();
		
		JPanel northPanel = new JPanel();
		northPanel.setLayout(new BorderLayout());
		northPanel.add(new JLabel("Ʈ ̸ : "), BorderLayout.WEST);
		northPanel.add(nameField, BorderLayout.CENTER);

		noteEditor = new NoteEditor();
		
		propertyPanel.add(northPanel, BorderLayout.NORTH);
		propertyPanel.add(noteEditor, BorderLayout.CENTER);
	}// end initPropertyPanel()
	
	
	public void startEditing(SkObject object){
		if(!object.isGroup())
			return;
		
		this.object = object;

		nameField.setText(object.getGroupName());
		if(object.getGroupNote() == null)
			object.setGroupNote(new Note());
		noteEditor.open(object.getGroupNote());
	}// end startEditing(SkObject)
	public void finishEditing(){
		object.setGroupNote(noteEditor.getNote());
		object.setGroupName(nameField.getText());
		
		getInterface().refreshObject(object);
		
		this.object = null;
		this.setVisible(false);
		getInterface().setCurrentCompEditor(null);
	}// end finishEditing()
	public SkObject getObject()
	{ return object; }
}
