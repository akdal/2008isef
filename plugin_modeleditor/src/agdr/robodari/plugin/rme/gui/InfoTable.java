package agdr.robodari.plugin.rme.gui;

import java.util.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;

import agdr.robodari.comp.info.*;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.gui.SketchEditorEvent;
import agdr.robodari.plugin.rme.store.*;
import agdr.robodari.store.RobotInfoStore;

public class InfoTable extends JPanel implements SketchEditorListener{
	public final static Object[] COLUMN = new Object[]{ "̸", "" };
	private JTable table;
	private DefaultTableModel tableModel;
	private DefaultCellEditor cellEditor;
	private RbModelEditorManager manager;
	
	
	public InfoTable(RbModelEditorManager manager){
		this.manager = manager;
		
		this.tableModel = new DefaultTableModel(new Object[][]{{}}, COLUMN);
		this.table = new JTable(tableModel);
		JTextField editorField = new JTextField();
		editorField.setEditable(false);
		this.cellEditor = new DefaultCellEditor(editorField);
		//this.cellEditor.setClickCountToStart(21474836);
		this.table.getColumn(COLUMN[0]).setCellEditor(cellEditor);
		this.table.getColumn(COLUMN[1]).setCellEditor(cellEditor);
		
		super.setLayout(new BorderLayout());
		JScrollPane jsp = new JScrollPane(this.table);
		jsp.setPreferredSize(new Dimension(100, 200));
		super.add(jsp, BorderLayout.CENTER);
		
		table.setEnabled(false);
		manager.getSketchEditor().getMainEditor().addSketchEditorListener(this);
	}// end Constructor()
	
	public void loadInfo(Info info){
		if(info == null){
			table.setEnabled(false);
			return;
		}
		Enumeration<InfoKey> keys = info.keys();

		while(tableModel.getRowCount() != 0)
			tableModel.removeRow(0);
		
		while(keys.hasMoreElements()){
			Object[] data = new Object[2];
			InfoKey key = keys.nextElement();
			
			data[0] = key.getName();
			String val = String.valueOf(info.get(key));
			if(key.getType() == InfoKey.TYPE_QUANTITY)
				val = val + "(" + key.getUnit().expression() + ")";
			data[1] = val;
			tableModel.addRow(data);
		}
	}// end loadInfo(Info)
	
	public void modelChanged(SketchEditorEvent see)
	{ table.setEnabled(false); }
	public void componentDeselected(SketchEditorEvent see)
	{ table.setEnabled(false); }
	public void componentRotated(SketchEditorEvent see){}
	public void componentDrifted(SketchEditorEvent see){}
	public void componentSelected(SketchEditorEvent see){
		SkObject obj = see.getAppearance();
		if(obj == null){
			table.setEnabled(false);
			return;
		}
		if(!table.isEnabled())
			table.setEnabled(true);
		this.loadInfo(RobotInfoStore.getInfo(obj.getRobotComponent()));
	}// end componentSelected
}
