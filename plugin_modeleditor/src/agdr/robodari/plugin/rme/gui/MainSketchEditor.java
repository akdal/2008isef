package agdr.robodari.plugin.rme.gui;

import agdr.robodari.plugin.rme.edit.*;

public interface MainSketchEditor extends SketchScreen{
	public SketchEditorListener[] getSketchEditorListeners();
	public void addSketchEditorListener(SketchEditorListener sel);
	public boolean containsSketchEditorListener(SketchEditorListener sel);
	public void removeSketchEditorListener(SketchEditorListener sel);
	
	public SketchModel getModel();
	
	public void disposeAllSelections();
	public void setSelected(SkObject obj, boolean selected);
	
	public void startCompEdit(SkObject obj);
	
	public void repaint();
}
