package agdr.robodari.plugin.rme.gui;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.image.BufferedImage;

import javax.vecmath.*;


import agdr.robodari.appl.*;
import agdr.robodari.plugin.rme.edit.Calculator;
import agdr.robodari.plugin.rme.edit.SkObject;


public class Picture implements SketchScreen{
	public BufferedImage normal;
	public BufferedImage pressed;
	public BufferedImage rOvered;
	public BufferedImage selected;
	public Point pos = new Point();
	public Point centerPos = new Point();
	
	public SketchScreen editor;
	public Picture(SketchScreen editor)
	{ this.editor = editor; }
	
	public float getAxisStartX()
	{ return centerPos.x; }
	public float getAxisStartY()
	{ return centerPos.y; }
	public float getScale()
	{ return editor.getScale(); }
	public Projector getProjector()
	{ return editor.getProjector(); }
	public Polygon getBound(SkObject obj)
	{ return editor.getBound(obj); }
	
	
	
	public static void refreshPicturePos(SkObject obj, Picture picture,
			SketchScreen editor){
		if(BugManager.DEBUG)
			BugManager.println("Picture : refreshPicturePos()");
		float[] tmpprjp = editor.getProjector().project(0, 0, 0,
				obj.getTransform(), picture.getScale());
		picture.pos.x = (int)tmpprjp[0] + (int)editor.getAxisStartX();
		picture.pos.y = (int)tmpprjp[1] + (int)editor.getAxisStartY();
		if(BugManager.DEBUG)
			BugManager.printf("\tproj center : %f %f, pic pos : %s\n", tmpprjp[0], tmpprjp[1],
					picture.pos);
	}// end refreshPicturePos(SkObject)
	public static Picture resetPicture(SkObject obj, Picture picture,
			SketchScreen editor, Point maxPoint, Point minPoint, boolean makeNormalOnly){
		refreshPicturePos(obj, picture, editor);
		
		picture.centerPos.x = - minPoint.x;
		picture.centerPos.y = - minPoint.y;
		if(obj.isGroup()){
			picture.centerPos.x += 2;
			picture.centerPos.y += 2;
		}
			
		
		Matrix4f trans = new Matrix4f(obj.getTransform());
		trans.m03 = trans.m13 = trans.m23 = 0;
		
		int width = maxPoint.x - minPoint.x;
		int height = maxPoint.y - minPoint.y;
		if(obj.isGroup()){
			width += 7;
			height += 7;
		}
		
		picture.normal = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = picture.normal.createGraphics();
		//g2d.clearRect(0, 0, 2000, 1400);
		obj.paint(g2d, trans, picture);
		if(obj.isGroup())
			drawBorder(picture, obj, g2d);
		
		if(!makeNormalOnly){
			picture.rOvered = new BufferedImage(width, height,
					BufferedImage.TYPE_INT_ARGB);
			g2d = picture.rOvered.createGraphics();
			//g2d.clearRect(0, 0, 2000, 1400);
			obj.paint(g2d, trans, picture, DefaultMainSketchEditor.getLineROverPaint());
			if(obj.isGroup())
				drawBorder(picture, obj, g2d);
			
			picture.pressed = new BufferedImage(width, height,
					BufferedImage.TYPE_INT_ARGB);
			g2d = picture.pressed.createGraphics();
			//g2d.clearRect(0, 0, 2000, 1400);
			obj.paint(g2d, trans, picture, DefaultMainSketchEditor.getLineBtnPressedPaint());
			if(obj.isGroup())
				drawBorder(picture, obj, g2d);
			
			picture.selected = new BufferedImage(width, height,
					BufferedImage.TYPE_INT_ARGB);
			g2d = picture.selected.createGraphics();
			//g2d.clearRect(0, 0, 2000, 1400);
			obj.paint(g2d, trans, picture, DefaultMainSketchEditor.getLineSelectedPaint());
			if(obj.isGroup())
				drawBorder(picture, obj, g2d);
		}
		
		return picture;
	}// end createPicture(SkObject)
	
	private static void drawBorder(SketchScreen scr, SkObject obj, Graphics2D g2d){
		Polygon pol = scr.getBound(obj);
		if(pol == null) return;
		
		for(int i = 1; i < pol.npoints; i++){
			g2d.drawLine((int)(pol.xpoints[i - 1] + scr.getAxisStartX()),
					(int)(pol.ypoints[i - 1] + scr.getAxisStartY()),
					(int)(pol.xpoints[i] + scr.getAxisStartX()),
					(int)(pol.ypoints[i] + scr.getAxisStartY()));
			//System.out.println("Picture : drawBorder : " + (pol.xpoints[i] + scr.getAxisStartX()));
		}
		if(pol.npoints > 1)
			g2d.drawLine((int)(pol.xpoints[0] + scr.getAxisStartX()),
					(int)(pol.ypoints[0] + scr.getAxisStartY()),
					(int)(pol.xpoints[pol.npoints - 1] + scr.getAxisStartX()),
					(int)(pol.ypoints[pol.npoints - 1] + scr.getAxisStartY()));
	}// end drawBorder(SkObject, Graphics2D)
}
