package agdr.robodari.plugin.rme.gui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import agdr.robodari.plugin.rme.edit.*;


public abstract class ProjectionEditor extends JDialog implements ActionListener{
	private float angleFromZ, angleFromX, rotation;
	private Projector.OrthographicProjector projector;
	private Projector.OrthographicProjector original;
	
	private JTextField zangleField;
	private JTextField xangleField;
	private JTextField rotField;
	private JButton applyButton;
	private JButton resetButton;
	
	private JButton okButton;
	private JButton cancelButton;
	
	private PrjCanvas canvas;
	
	
	public ProjectionEditor (Frame parent){
		super(parent, "Projection Editor");
		__set();
	}// end 
	public ProjectionEditor (JDialog parent){
		super(parent, "Projection Editor");
		__set();
	}// end Con
	private void __set(){
		angleFromZ = angleFromX = rotation = 0;
		projector = new Projector.OrthographicProjector(0, 0, 0);
		
		JPanel centerPanel = new JPanel();
		{
			
			JPanel rightp = new JPanel();
			rightp.setLayout(new BorderLayout());
			JPanel listp = new JPanel();
			BoxLayout listp_boxl = new BoxLayout(listp, BoxLayout.PAGE_AXIS);
			listp.setLayout(listp_boxl);
			{
				JLabel zalabel = new JLabel("Z-angle : ");
				JLabel xalabel = new JLabel("X-angle : ");
				JLabel rotLabel = new JLabel("Rotation : ");
				zangleField = new JTextField(8);
				xangleField = new JTextField(8);
				rotField = new JTextField(8);
				applyButton = new JButton("Apply");
				resetButton = new JButton("Reset");

				xangleField.setHorizontalAlignment(JTextField.RIGHT);
				zangleField.setHorizontalAlignment(JTextField.RIGHT);
				rotField.addActionListener(this);
				
				zangleField.addActionListener(this);
				xangleField.addActionListener(this);
				rotField.addActionListener(this);
				applyButton.addActionListener(this);
				resetButton.addActionListener(this);
				
				listp.add(zalabel);
				listp.add(zangleField);
				listp.add(xalabel);
				listp.add(xangleField);
				listp.add(rotLabel);
				listp.add(rotField);
				listp.add(applyButton);
				listp.add(resetButton);
			}
			rightp.add(listp, BorderLayout.NORTH);
			canvas = new PrjCanvas(this.projector);
			
			centerPanel.setLayout(new BorderLayout());
			centerPanel.add(canvas, BorderLayout.CENTER);
			centerPanel.add(rightp, BorderLayout.EAST);
			centerPanel.setBorder(new MatteBorder(0, 0, 1, 0, Color.black));
		}
		
		JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		{
			okButton = new JButton("Ok");
			okButton.addActionListener(this);
			cancelButton = new JButton("Cancel");
			cancelButton.addActionListener(this);
			buttonPanel.add(okButton);
			buttonPanel.add(cancelButton);
		}
		super.getContentPane().setLayout(new BorderLayout());
		super.getContentPane().add(centerPanel, BorderLayout.CENTER);
		super.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
		super.setSize(350, 350);
		super.setResizable(false);
	}// end __set()
	
	
	public void start (Projector.OrthographicProjector proj){
		this.projector = proj;
		this.zangleField.setText(String.format("%.5f", Math.toDegrees(proj.getTheta())).toString());
		this.xangleField.setText(String.format("%.5f", Math.toDegrees(proj.getPhi())).toString());
		this.rotField.setText(String.format("%.5f", Math.toDegrees(proj.getRho())).toString());
		canvas.setProjector(projector);
		this.original = new Projector.OrthographicProjector(proj);
		super.setVisible(true);
	}// end start
	public abstract void finish ();
	
	
	public void actionPerformed (ActionEvent ae){
		if(ae.getSource() == okButton){
			finish();
		}else if(ae.getSource() == cancelButton){
			super.setVisible(false);
		}else if(ae.getSource() == resetButton){
			this.zangleField.setText(String.format("%.5f", Math.toDegrees(original.getTheta())).toString());
			this.xangleField.setText(String.format("%.5f", Math.toDegrees(original.getPhi())).toString());
			this.rotField.setText(String.format("%.5f", Math.toDegrees(original.getRho())).toString());
			this.apply();
		}else if(ae.getSource() == applyButton || ae.getSource() == zangleField
				|| ae.getSource() == xangleField || ae.getSource() == rotField){
			apply();
		}
	}// end actionPerofrmed
	private void apply (){
		try{
			float f = Float.parseFloat(zangleField.getText());
			this.angleFromZ = (float)Math.toRadians(f);
		}catch(Exception e){
			zangleField.setText(String.valueOf(this.angleFromZ));
		}
		
		try{
			float f = Float.parseFloat(xangleField.getText());
			this.angleFromX = (float)Math.toRadians(f);
		}catch(Exception e){
			xangleField.setText(String.valueOf(this.angleFromX));
		}
		
		try{
			float f = Float.parseFloat(rotField.getText());
			this.rotation = (float)Math.toRadians(f);
		}catch(Exception e){
			rotField.setText(String.valueOf(this.rotation));
		}
		this.projector = new Projector.OrthographicProjector(this.angleFromX, this.angleFromZ, this.rotation);
		this.canvas.setProjector(this.projector);
	}// end apply
	
	
	
	public Projector getProjector(){
		return this.projector;
	}// end getProjector()
	
	
	
	
	
	static class PrjCanvas extends JPanel implements SketchScreen{
		private Projector prj;
		private float scale = 5.0f;
		
		public PrjCanvas (Projector defaultProjector){
			this.prj = defaultProjector;
			super.setOpaque(true);
			super.setBackground(Color.white);
			super.setPreferredSize(new Dimension(270, 300));
		}// end constructor()
		
		public float getScale ()
		{ return scale; }
		public Projector getProjector ()
		{ return prj ;}
		public void setProjector (Projector prj)
		{ this.prj = prj; repaint(); }
		public float getAxisStartX()
		{ return super.getWidth() / 2; }
		public float getAxisStartY()
		{ return super.getHeight() / 2; }
		public Polygon getBound (SkObject o)
		{ return null; }
		
		public void paint (Graphics g){
			super.paint(g);
			if(g == null) return;
			SketchEditorUtility.drawAxis(this,
					(Graphics2D)g,
					DefaultMainSketchEditor.getAxisStroke(),
					DefaultMainSketchEditor.getAxisPaint(),
					DefaultMainSketchEditor.getAxisHiddenPaint(),
					DefaultMainSketchEditor.getZAxisPaint(),
					super.getWidth() - 40);
		}// end paint(G)
	}// end class PrjCanvas
}
