package agdr.robodari.plugin.rme.gui;

import java.awt.Point;
import javax.vecmath.*;
import static java.lang.Math.*;

public interface Projector {
	public float[] project(Point3f p, Matrix4f trans, float scale);
	public float[] project(float x, float y, float z, Matrix4f trans, float scale);
	
	public static class OrthographicProjector implements Projector{
		private float coscth;// cos c_theta : angle from z-axis
		private float sincth;
		private float coscph;// cos c_phi : angle from x-axis
		private float sincph;
		private float coscrh;// cos c_rho : rotation from {hat theta}
		private float sincrh;
		
		private float phi, theta, rho;
		
		/**
		 * Returns angle(radians) from x-axis
		 * @return
		 */
		public float getPhi()
		{ return phi; }
		/**
		 * Returns angle(radians) from z-axis
		 * @return
		 */
		public float getTheta()
		{ return theta; }
		/**
		 * Returns camera-rotation angle
		 * @return
		 */
		public float getRho()
		{ return rho; }
		
		
		public OrthographicProjector(float phi, float theta, float rho){
			this.phi = phi;
			this.theta = theta;
			this.rho = rho;
			
			coscth = (float)cos(theta);
			sincth = (float)sin(theta);
			coscph = (float)cos(phi);
			sincph = (float)sin(phi);
			coscrh = (float)cos(rho);
			sincrh = (float)sin(rho);
		}// end Constructor
		public OrthographicProjector(OrthographicProjector org)
		{ this(org.phi, org.theta, org.rho); }
		
		public float[] project(Point3f p, Matrix4f trans, float scale)
		{ return project(p.x, p.y, p.z, trans, scale); }
		public float[] project(float x, float y, float z, Matrix4f trans, float scale){
			float newx, newy, newz = 0;
			// first, transform given point
			if(trans != null){
				newx = trans.m00 * x + trans.m01 * y + trans.m02 * z + trans.m03;
				newy = trans.m10 * x + trans.m11 * y + trans.m12 * z + trans.m13;
				newz = trans.m20 * x + trans.m21 * y + trans.m22 * z + trans.m23;
				x = newx;
				y = newy;
				z = newz;
			}
			
			// on spherical coordinates.. a_r, a_theta, a_phi

			// 1. z : 
			newx = x * coscph + y * (-sincph);
			newy = x * sincph + y * coscph;
			newz = z;
			x = newx; y = newy; z = newz;
			
			// 2. y : 
			float ypx = 1;//-sincph;
			float ypy = 0;//coscph;
			float ypz = 0;
			//System.out.println("transformed y-axis :" + ypx + " , " + ypy + " , " + ypz);
			
			newx = ((coscth + (1 - coscth) * (ypx * ypx))) * x
					+ ((1-coscth) * ypx * ypy - sincth * ypz) * y
					+ ((1 - coscth) * ypx * ypz + sincth * ypy) * z;
			newy = ((1 - coscth) * ypx * ypy + sincth * ypz) * x
					+ (coscth + (1 - coscth) * ypy * ypy) * y
					+ ((1 - coscth)* ypy * ypz - sincth * ypx) * z;
			newz = ((1 - coscth) * ypx * ypz - sincth * ypy) * x
					+ ((1 - coscth) * ypy * ypz + sincth * ypx) * y
					+ (coscth + (1 - coscth) * ypx * ypx) * z;
			x = newx; y = newy; z = newz;
			
			//System.out.println("bef rot : " + newx + ", " + newy + ", " + newz);
			//System.out.println("sin(rth - cth): " + (sinrth * coscth - sincth * cosrth) + " , sin(rph - cph): " + (sinrph * coscph - sincph * cosrph));
			
			// x, y rot : 
			float[] parray = new float[2];
			parray[0] = coscrh * newx + sincrh * newy;
			parray[1] = -sincrh * newx + coscrh * newy;
			parray[0] *= scale;
			parray[1] *= scale;
			return parray;
		}// end project(float, float, float, Matrix4d, float)
		
		
		
		
		public static void main(String[] argv){
			OrthographicProjector op = new OrthographicProjector(0, 0, 0);
			float[] d = op.project(0, 0, 1, null, 1);
			System.out.println(d[0] + " , " + d[1]);
		}
	}// end class OrthographicProjector
}
