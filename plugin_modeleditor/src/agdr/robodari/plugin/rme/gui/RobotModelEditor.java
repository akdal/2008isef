package agdr.robodari.plugin.rme.gui;

import java.awt.*;
import java.awt.event.*;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Hashtable;

import javax.swing.*;

import agdr.robodari.appl.BugManager;
import agdr.robodari.appl.ExceptionHandler;
import agdr.robodari.edit.RobodariInterface;
import agdr.robodari.gui.dialog.*;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.gui.dialog.*;
import agdr.robodari.plugin.rme.rlztion.*;
import agdr.robodari.plugin.rme.rlztion.gui.*;
import agdr.robodari.project.ProjectFile;


public final class RobotModelEditor implements RbModelEditorManager{
	public final static String EXT_RBMODEL = ".rbmodel";
	public final static String EXTDESC = "Robot Model";
	public final static Icon FILEICON = new Icon(){
		public int getIconWidth()
		{ return 16; }
		public int getIconHeight()
		{ return 16; }
		public void paintIcon(Component c, Graphics g, int x, int y){}
	};
	
	public final static String[] RIGHTCOMPS_TITLES = new String[]{ "Comps" };
	public final static String[] LEFTCOMPS_TITLES = new String[]{ "ItemView" };
	
	
	
	// center
	private SketchEditor sketchEditor;
	private ConnectionEditor connEditor;
	private JTabbedPane centerTabbedPane;

	// left
	private ComponentTree componentTree;
	private JComponent[] leftComps;
	
	// right
	private JPanel rightPanel;
	private AvailableItemTree avitemTree;
	private InfoTable infoTable;
	private JComponent[] rightComps;


	private JDialog rlztionDialog;
		private RMDebuggerSet ddebuggerSet;
		private RealizationPanel rlzPanel;
	private OptionDialog fileOptDlg;
	
	private RobodariInterface rbdrInterface;
	private Frame frame_parent;
	private Hashtable<ProjectFile, RobotModel> cachedRobotModels
			= new Hashtable<ProjectFile, RobotModel>();

	private JMenu viewMenu;
		private JMenuItem changeLayoutModeMenu;
		private JMenuItem projectorEditMenu;
	private JMenu realizationMenu;
		private JMenuItem runMenu;
	
	
	
	
	
	public RobotModelEditor(RobodariInterface rbdritfc, Frame parent) throws Exception{
		this.frame_parent = parent;
		this.rbdrInterface = rbdritfc;
		
		initCenter();
		initLeft();
		initRight();
		initMenuItems();
		
		this.fileOptDlg = new SketchOptionDialog(frame_parent, this);
		this.ddebuggerSet = new RMDebuggerSet(this.frame_parent);
		this.rlzPanel = new RealizationPanel(parent, ddebuggerSet);
		this.rlztionDialog = new JDialog(parent, "Realization");

		Dimension scrsize = Toolkit.getDefaultToolkit().getScreenSize();
		Insets is = Toolkit.getDefaultToolkit().getScreenInsets(
				frame_parent.getGraphicsConfiguration());
		this.rlztionDialog.setSize(scrsize.width - is.left - is.right, scrsize.height
				- is.top - is.bottom);
		this.rlztionDialog.setLocation(0, 0);
		this.rlztionDialog.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent we){
				rlzPanel.finish();
				rlztionDialog.setVisible(false);
			}// end windowCosing
		});
		this.rlztionDialog.setContentPane(this.rlzPanel);
	}// end Constructor()
	private void initCenter() throws Exception{
		centerTabbedPane = new JTabbedPane();
		this.sketchEditor = new SketchEditor(this, frame_parent, null);
		this.connEditor = new ConnectionEditor(this, null);
		
		JPanel sketchPanel = new JPanel();
		sketchPanel.setLayout(new BorderLayout());
		sketchPanel.add(sketchEditor, BorderLayout.CENTER);
		sketchPanel.add(sketchEditor.getControlPanel(), BorderLayout.SOUTH);
		
		centerTabbedPane.add("Sketch", sketchPanel);
		centerTabbedPane.add("Connection", new JScrollPane(connEditor));
	}// end intCenter()
	private void initLeft(){
		componentTree = new ComponentTree(this);
		leftComps = new JComponent[]{ componentTree };
	}// end initLeft()
	private void initRight(){
		avitemTree = new AvailableItemTree(this);
		infoTable = new InfoTable(this);
		rightPanel = new JPanel(new BorderLayout());
		rightPanel.add(new JScrollPane(avitemTree), BorderLayout.CENTER);
		rightPanel.add(infoTable, BorderLayout.SOUTH);
		
		rightComps = new JComponent[]{ rightPanel };
	}// end initRight()
	private void initMenuItems(){
		/*
		 * 
	private JMenu viewMenu;
		private JMenuItem changeLayoutModeMenu;
	private JMenu realizationMenu;
		private JMenuItem runMenu;
		 */
		viewMenu = new JMenu("View");
		{
			changeLayoutModeMenu = new JMenuItem(new ChangeLayoutModeAction(this, "Change layout mode", null));
			projectorEditMenu = new JMenuItem("Edit Projector..");
			projectorEditMenu.addActionListener(new SketchEditor.ProjEditAction(this.sketchEditor));
			
			viewMenu.add(changeLayoutModeMenu);
			viewMenu.add(projectorEditMenu);
		}
		realizationMenu = new JMenu("Realization");
		{
			runMenu = new JMenuItem(new RunAction(this, "Run", null));
			realizationMenu.add(runMenu);
			runMenu.setEnabled(false);
		}
	}// end void initMenuItems()
	
	
	
	public RobodariInterface getInterface()
	{ return rbdrInterface; }
	public void setInterface(RobodariInterface i)
	{ this.rbdrInterface = i; }
	
	public RobotModel getRobotModel(ProjectFile file)
	{ return file == null ? null : cachedRobotModels.get(file); }
	public RobotModel getEditingRobotModel(){
		if(this.rbdrInterface.getEditingFile() != null)
			return cachedRobotModels.get(this.rbdrInterface.getEditingFile());
		return null;
	}// end getEditingRobotModel()
	
	
	public OptionDialog getFileOptionDialog()
	{ return this.fileOptDlg; }
	
	
	
	
	
	
	public String getFileExtension()
	{ return EXT_RBMODEL; }
	public String getFileDescription()
	{ return EXTDESC; }
	public static boolean isEditableFile(String szfile){
		if(szfile.indexOf('.') == -1) return false;
		return szfile.substring(szfile.lastIndexOf('.'), szfile.length()).toLowerCase()
		.equals(RobotModelEditor.EXT_RBMODEL);
	}// end isEditableFile
	public boolean isEditable(String szfile)
	{ return isEditableFile(szfile); }
	public Icon getFileIcon()
	{ return FILEICON; }
	
	
	
	
	public void customizeMenuBar(JMenuBar mb){
		if(BugManager.DEBUG)
			BugManager.println("RME : customizeMenuBar()");
		mb.add(viewMenu);
		mb.add(realizationMenu);
		mb.revalidate();
		mb.repaint();
	}// end customizeMenuBar)
	public void undoMenuBar(JMenuBar mb){
		mb.remove(viewMenu);
		mb.remove(realizationMenu);
		mb.revalidate();
		mb.repaint();
		if(BugManager.DEBUG)
			BugManager.println("RME : undoMenuBar : menuCnt :" + mb.getMenuCount());
	}// end undoMenuBar
	
	

	public void newFile(ProjectFile pfile){
		SketchModel smodel = new SketchModel(pfile.getFileName());
		ConnectionModel cmodel = new ConnectionModel();
		cmodel.load(smodel);
		
		RobotModel rmodel = new RobotModel();
		rmodel.setSketchModel(smodel);
		rmodel.setConnectionModel(cmodel);
		
		cachedRobotModels.put(pfile, rmodel);
		saveFile(pfile, new File(pfile.getTotalPath()));
		cachedRobotModels.remove(pfile);
	}// end newFile(ProjectFile)
	public void cacheFile(ProjectFile pfile) throws IOException{
		if(!cachedRobotModels.containsKey(pfile)){
			if(BugManager.DEBUG)
				BugManager.println("RME : cacheFile");
			
			ByteArrayInputStream bais = null;
			FileInputStream fis = null;
			ObjectInputStream ois = null;
			File file = new File(pfile.getTotalPath());
			try{
				fis = new FileInputStream(file);
				byte[] data = new byte[fis.available()];
				fis.read(data, 0, data.length);
				fis.close();
				
				bais = new ByteArrayInputStream(data);
				ois = new ObjectInputStream(bais);
				RobotModel openRobotModel =
					(RobotModel)ois.readObject();
				
				cachedRobotModels.put(pfile, openRobotModel);
				
				ois.close();
				bais.close();
			}catch(ClassNotFoundException excp){
				BugManager.log(excp, true);
			}
			if(BugManager.DEBUG)
				BugManager.println("RME : cacheFile : end");
		}else{
			// nothing to do
		}
	}// end cacheFile(ProjectFile)
	public boolean isCached(ProjectFile file)
	{ return cachedRobotModels.containsKey(file); }
	public ProjectFile[] getCachedFiles()
	{ return cachedRobotModels.keySet().toArray(new ProjectFile[]{}); }
	public void openFile(ProjectFile file, boolean reload){
		BugManager.println("RME : openFile()");
		if(!this.isCached(file) || reload){
			try{
				cacheFile(file);
			}catch(IOException excp){
				ExceptionHandler.wrongFile(file.getTotalPath());
				excp.printStackTrace();
				return;
			}
		}
		
		RobotModel model = this.getRobotModel(file);
		
		sketchEditor.loadSketch(model.getSketchModel());
		connEditor.loadConnection(model.getConnectionModel());
		this.runMenu.setEnabled(true);
		BugManager.println("RME : end openFile()");
	}// end openFile(ProjectFile)
	public void saveFile(ProjectFile pfile, File file){
		FileOutputStream fos = null;
		ObjectOutputStream oos = null;
		try{
			if(!file.exists())
				file.createNewFile();
			fos = new FileOutputStream(file);
			oos = new ObjectOutputStream(fos);
			
			oos.writeObject(this.getRobotModel(pfile));
		}catch(IOException ioe){
			BugManager.log(ioe, true);
		}finally{
			try{
				oos.close();
				fos.close();
			}catch(Exception excp)
			{ excp.printStackTrace(); }
		}
		BugManager.println("MainFrame : saveFile : end");
	}// end saveFile(ProjectFile, File)
	public void closeFile(ProjectFile file){
		if(this.getEditingRobotModel() == this.cachedRobotModels.get(file)){
			this.connEditor.loadConnection(null);
			this.sketchEditor.loadSketch(null);
			this.runMenu.setEnabled(false);
		}
		this.cachedRobotModels.remove(file);
	}// end closeFile(ProjectFile)
	
	
	
	
	
	
	public void setTabState(int state){
		if(state == RbModelEditorManager.SKETCH_EDITOR)
			centerTabbedPane.setSelectedIndex(0);
		else if(state == RbModelEditorManager.CONN_EDITOR)
			centerTabbedPane.setSelectedIndex(1);
	}// end setTabState(int)
	
	
	
	

	
	
	
	
	
	boolean startRealzflag = false;
	public void startRealization(){
		if(this.getEditingRobotModel().getStarter() == null){
			ExceptionHandler.starterNotDecided();
			return;
		}
		startRealzflag = true;
		Runnable runnable = new Runnable(){
			public void run(){
				try{
					int state = rlzPanel.getState();//realizationViewer.getState();
					while(startRealzflag || state != RealizationPanel.ST_UNEMPLOYED){
						if(state == RealizationPanel.ST_DBG_PAUSE || state == RealizationPanel.ST_DBG_ONPROC){
							rbdrInterface.setStatusBarText("Finished.");
							break;
						}else if(state == RealizationPanel.ST_INIT_RPANEL)
							rbdrInterface.setStatusBarText("canvas initializing..");
						else if(state == RealizationPanel.ST_INIT_COMPS)
							rbdrInterface.setStatusBarText("initializing components..");
						else if(state == RealizationPanel.ST_INIT_RDPLOAD)
							rbdrInterface.setStatusBarText("initializing RDPI modules..");
						Thread.sleep(100);
						state = rlzPanel.getState();
					}
					rbdrInterface.setStatusBarText("");
				}catch(Exception excp){
					BugManager.log(excp, true);
				}
			}// end run()
		};
		Thread thrd = new Thread(runnable);
		thrd.start();
		Runnable run2 = new Runnable(){
			public void run(){
				RMRealizationModel model = new RMRealizationModel();
				try{
					model.init(getEditingRobotModel(), rbdrInterface.getProject());
					rlzPanel.initialize(model);
					rlztionDialog.setVisible(true);
				}catch(RDPLoadException excp){
					if(excp.errorCase == RDPLoadException.CASE_INSTANTIATING)
						agdr.robodari.plugin.rme.appl.ExceptionHandler.rdpi_init_exceptionOccured(excp.ctrlerName, excp.getCause());
					else if(excp.errorCase == RDPLoadException.CASE_STREAMING)
						agdr.robodari.plugin.rme.appl.ExceptionHandler.rdpi_cannotOpenStream(excp.ctrlerName, excp.getCause());
				}catch(Exception excp){
					BugManager.log(excp, true);
				}
				startRealzflag = false;
			}// end run()
		};// end run2()
		this.saveFile(rbdrInterface.getEditingFile(), new File(rbdrInterface.getEditingFile().getTotalPath()));
		Thread thrd2 = new Thread(run2);
		thrd2.start();
	}// end startRealization()
	public RealizationPanel getRealizationPanel()
	{ return rlzPanel; }
	
	
	
	
	
	public SketchEditor getSketchEditor()
	{ return sketchEditor; }
	public ConnectionEditor getConnectionEditor()
	{ return connEditor; }
	
	public JComponent getEditorComp()
	{ return centerTabbedPane; }
	public JComponent getLeftComponent(int idx)
	{ return leftComps[idx]; }
	public int getLeftComponentCount()
	{ return leftComps.length; }
	public String getLeftCompTitle(int idx)
	{ return LEFTCOMPS_TITLES[idx]; }
	public JComponent getRightComponent(int idx)
	{ return rightComps[idx]; }
	public int getRightComponentCount()
	{ return rightComps.length; }
	public String getRightCompTitle(int idx)
	{ return RIGHTCOMPS_TITLES[idx]; } 
	
	
	public void repaint(){
		if(this.sketchEditor != null)
			this.sketchEditor.repaint();
		if(this.connEditor != null)
			this.connEditor.repaint();
	}// end repaint()
	
	
	
	
	

	public static class RunAction extends AbstractAction{
		private RbModelEditorManager manager;
		
		public RunAction(RbModelEditorManager manager, String label, Icon icon){
			super(label, icon);
			this.manager = manager;
		}// end Constructor(RobodariInterface, String, Icon)
		
		public void actionPerformed(ActionEvent ae){
			manager.startRealization();
		}// end actionPerformed(ActionEvent)
	}// end action RunAction
	public static class ChangeLayoutModeAction extends AbstractAction{
		private RbModelEditorManager manager;
		
		public ChangeLayoutModeAction(RbModelEditorManager manager, String label, Icon icon){
			super(label, icon);
			this.manager = manager;
		}// end Constructor(RobodariInterface, String, Icon)
		
		public void actionPerformed(ActionEvent ae){
			int mode = manager.getSketchEditor().getLayoutMode();
			if(mode == SketchEditor.HORIZONTAL_MODE)
				manager.getSketchEditor().setLayoutMode(SketchEditor.VERTICAL_MODE);
			else
				manager.getSketchEditor().setLayoutMode(SketchEditor.HORIZONTAL_MODE);
		}// end actionPerformed(ActionEvent)
	}// end action ChangeLayoutModeAction
}
