package agdr.robodari.plugin.rme.gui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.vecmath.*;


import agdr.library.math.*;
import agdr.robodari.appl.*;
import agdr.robodari.plugin.rme.edit.*;
import static java.lang.Math.*;

public class RotationEditor extends CompEditor{
	private static Color originalColor = new Color(210, 210, 210, 50);
	public static Color getOriginalColor()
	{ return originalColor; }
	
	public static Dimension getDefaultSize()
	{ return new Dimension(400, 300); }
	
	
	
	
	private SkObject object;
	
	private REPCanvas canvas;
	
	private JTextField angleThtField;
	private JTextField anglePhiField;
	private JTextField angleAlphaField;
	
	private JTextField scaleField;
	
	//private SketchModel model = new SketchModel();
	private double thetaDeg;
	private double phiDeg;
	private double alphaDeg;
	private Matrix3f  editedtrans;
	
	
	public RotationEditor(SketchEditorInterface manager, Frame parent){
		super(manager, parent, "Rotation Edit");
		__set(manager);
	}// end Constructor
	public RotationEditor(SketchEditorInterface manager, Dialog parent){
		super(manager, parent, "Rotation Edit");
		__set(manager);
	}// end Constructor(MainSKetchEditor)
	private void __set(SketchEditorInterface manager){
		this.editedtrans = new Matrix3f();
		
		super.closingAction = new ClosingAction(this);
		super.getEditorPanel().setBorder(new EtchedBorder());
		super.getEditorPanel().setOpaque(true);
		super.getEditorPanel().setBackground(Color.white);
		super.getEditorPanel().setPreferredSize(new Dimension(410, 260));
		super.pack();
		
		Dimension scrsize = Toolkit.getDefaultToolkit().getScreenSize();
		super.setLocation((scrsize.width - 410) / 2, (scrsize.height - 260) / 2);
		initComponents();
	}// end __set
	
	private void initComponents(){
		angleThtField = new JTextField();
		anglePhiField = new JTextField();
		angleAlphaField = new JTextField();
		
		AngleChangeAction acaction = new AngleChangeAction(this);
		angleThtField.addActionListener(acaction);
		angleThtField.addFocusListener(acaction);
		anglePhiField.addActionListener(acaction);
		anglePhiField.addFocusListener(acaction);
		angleAlphaField.addActionListener(acaction);
		angleAlphaField.addFocusListener(acaction);
		
		angleThtField.setBounds(330, 30, 70, 25);
		anglePhiField.setBounds(330, 80, 70, 25);
		angleAlphaField.setBounds(330, 130, 70, 25);

		canvas = new REPCanvas(this);
		canvas.setBounds(10, 10, 310, 240);
		
		ScaleChangeAction scaction = new ScaleChangeAction(this);
		scaleField = new JTextField();
		scaleField.addActionListener(scaction);
		scaleField.addFocusListener(scaction);
		scaleField.setBounds(330, 300 - 70, 70, 22);
		
		JSeparator smallSep = new JSeparator();
		smallSep.setBounds(330, 205, 60, 2);
		
		this.getEditorPanel().setLayout(null);
		this.getEditorPanel().add(anglePhiField);
		this.getEditorPanel().add(angleThtField);
		this.getEditorPanel().add(angleAlphaField);
		this.getEditorPanel().add(scaleField);
		this.getEditorPanel().add(smallSep);
		this.getEditorPanel().add(canvas);
		this.getEditorPanel().add(createLabel("Theta : ", 330, 10, 70, 20));
		this.getEditorPanel().add(createLabel("Phi : ", 330, 60, 70, 20));
		this.getEditorPanel().add(createLabel("Alpha : ", 330, 110, 70, 20));
		this.getEditorPanel().add(createLabel("scale : ", 410 - 10 - 60, 300 - 10 - 30 - 5 - 5 - 20 - 5 - 20, 
				60, 20));
	}// end initComponents()
	
	private JLabel createLabel(String lbl, int x, int y, int w, int h){
		JLabel label = new JLabel(lbl);
		label.setHorizontalAlignment(JLabel.RIGHT);
		label.setBounds(x, y, w, h);
		return label;
	}// end createLabel(String, int, int, int, itn)
	
	
	
	public SkObject getObject()
	{ return object; }
	public void startEditing(SkObject object){
		BugManager.printf("RotationEditor : startEditing()\n");
		this.object = object;
		this.thetaDeg = this.phiDeg = this.alphaDeg = 0;
		
		Matrix4f tr = this.object.getTransform();
		
		editedtrans.m00 = tr.m00;
		editedtrans.m01 = tr.m01;
		editedtrans.m02 = tr.m02;
		editedtrans.m10 = tr.m10;
		editedtrans.m11 = tr.m11;
		editedtrans.m12 = tr.m12;
		editedtrans.m20 = tr.m20;
		editedtrans.m21 = tr.m21;
		editedtrans.m22 = tr.m22;
		
		BugManager.printf("RotEditro : startEdt : det(editedtrans) : %f\n", editedtrans.determinant());
		// Fucking bug..
		Quat4f q = new Quat4f();
		q.set(editedtrans);
		AxisAngle4f aaf = new AxisAngle4f();
		aaf.set(q);
		
		BugManager.printf("RotEditor : startEdt : axis : %f %f %f, angle : %f\n", aaf.x, aaf.y, aaf.z, aaf.angle);
		BugManager.printf("RotEditor : startEdt : editedtrans : \n%s", editedtrans);
		
		Matrix3f tmp_replc = new Matrix3f();
		tmp_replc.set(aaf);
		BugManager.printf("RotEditor : startEdt : replc again : \n%s", tmp_replc);
		
		this.alphaDeg = Math.toDegrees(aaf.angle);
		this.thetaDeg = Math.toDegrees(Math.acos(aaf.z));
		
		double sintheta = Math.sqrt(1 - aaf.z * aaf.z);
		if(sintheta < MathUtility.FEPSILON){
			this.phiDeg = 0;
		}else{
			double cosphi = aaf.x / sintheta;
			double sinphi = aaf.y / sintheta;
			this.phiDeg = Math.toDegrees(Math.acos(cosphi));
			if(sinphi < 0.0)
				this.phiDeg += 180.0f;
		}

		this.scaleField.setText(String.format("%.4f", this.canvas.getScale()));
		this.angleThtField.setText(String.format("%.4f", this.thetaDeg).toString());
		this.anglePhiField.setText(String.format("%.4f", this.phiDeg).toString());
		this.angleAlphaField.setText(String.format("%.4f", this.alphaDeg).toString());
		this.scaleField.setText(String.format("%.4f", this.canvas.getScale()));
		
		this.refresh();
	}// end setAppearance(SkObject)
	public void finishEditing(){
		BugManager.printf("RotationEditor : finishEditing : this.getTransform.getTransform() : \n%s", this.getTransform());
		
		Vector3f pos = new Vector3f(object.getTransform().m03,
				object.getTransform().m13,
				object.getTransform().m23);
		object.setTransform(this.getTransform());
		object.getTransform().m03 = pos.x;
		object.getTransform().m13 = pos.y;
		object.getTransform().m23 = pos.z;
		
		this.setVisible(false);
		if(getInterface() != null)
			getInterface().refreshObject(object);
	}// end finishEditing()
	
	
	public Matrix4f getTransform(){
		Matrix4f original = new Matrix4f();
		original.m00 = editedtrans.m00;
		original.m01 = editedtrans.m01;
		original.m02 = editedtrans.m02;
		original.m10 = editedtrans.m10;
		original.m11 = editedtrans.m11;
		original.m12 = editedtrans.m12;
		original.m20 = editedtrans.m20;
		original.m21 = editedtrans.m21;
		original.m22 = editedtrans.m22;
		original.m33 = 1;
		return original;
	}// end getTransform()
	
	
	
	public void refresh(){
		double phi = toRadians(this.phiDeg);
		double theta = toRadians(this.thetaDeg);
		double alpha = toRadians(this.alphaDeg);
		
		double sinph = sin(phi), cosph = cos(phi);
		double sinth = sin(theta), costh = cos(theta);
		
		double x = sinth * cosph;
		double y = sinth * sinph;
		double z = costh;
		
		double s = sin(alpha), c = cos(alpha);
		editedtrans.m00 = (float)(c + (1.0 - c) * x * x);
		editedtrans.m01 = (float)((1.0 - c) * x * y - s * z);
		editedtrans.m02 = (float)((1.0 - c) * x * z + s * y);
		editedtrans.m10 = (float)((1.0 - c) * x * y + s * z);
		editedtrans.m11 = (float)(c + (1.0 - c) * y * y);
		editedtrans.m12 = (float)((1.0 - c) * y * z - s * x);
		editedtrans.m20 = (float)((1.0 - c) * x * z - s * y);
		editedtrans.m21 = (float)((1.0 - c) * y * z + s * x);
		editedtrans.m22 = (float)(c + (1.0 - c) * z * z);
		
		repaint();
	}// end refresh()
	
	
	
	static class REPCanvas extends JPanel implements SketchScreen{
		private float scale;
		private RotationEditor editPanel;
		private Projector projector;
		
		public REPCanvas(RotationEditor editPanel){
			super.setBorder(new LineBorder(Color.gray));
			super.setBackground(Color.white);
			super.setOpaque(true);
			this.editPanel = editPanel;
			this.projector = new Projector.OrthographicProjector((float)(Math.PI * 5.0 / 4.0), (float)(Math.PI * 3.0 / 4.0), 0);
			
			if(editPanel.getInterface().getMainEditor() != null)
				this.scale = editPanel.getInterface().getMainEditor().getScale();
			else
				this.scale = 20.0f;
		}// end Constructor()

		
		public void paint(Graphics g){
			super.paint(g);
			if(g == null) return;
			
			Graphics2D g2d = (Graphics2D)g;
			SketchEditorUtility.drawAxis(this, g2d,
					DefaultMainSketchEditor.getAxisStroke(),
					DefaultMainSketchEditor.getAxisPaint(),
					DefaultMainSketchEditor.getAxisHiddenPaint(),
					DefaultMainSketchEditor.getZAxisPaint(),
					200.0f);
			
			SkObject object = editPanel.getObject();
			
			Matrix4f totaltrans = editPanel.getTransform();
			object.paint(g2d, totaltrans, this);
			
			double sinth = Math.sin(Math.toRadians(editPanel.thetaDeg)),
					costh = Math.cos(Math.toRadians(editPanel.thetaDeg));
			double sinph = Math.sin(Math.toRadians(editPanel.phiDeg)),
					cosph = Math.cos(Math.toRadians(editPanel.phiDeg));
			
			float[] prjpnts = this.projector.project((float)(sinth * cosph * 10.0),
							(float)(sinth * sinph * 10.0),
							(float)(costh * 10.0), null, scale);
			float[] prjorg = this.projector.project(0, 0, 0, null, scale);
			g2d.setColor(Color.red);
			g2d.drawLine((int)prjorg[0] + (int)getAxisStartX(),
					(int)prjorg[1] + (int)getAxisStartY(),
					(int)prjpnts[0] + (int)getAxisStartX(),
					(int)prjpnts[1] + (int)getAxisStartY());
		}// end paint(Graphics)
		
		
		public Projector getProjector()
		{ return this.projector; }
		
		public Polygon getBound(SkObject obj)
		{ return null; }
		
		public float getAxisStartX()
		{ return super.getWidth() / 2.0f; }
		public float getAxisStartY()
		{ return super.getHeight() / 2.0f; }
		
		public float getScale()
		{ return scale; }
		public void setScale(float sc)
		{ this.scale = sc; repaint(); }
	}// end class REPCanvas
	
	
	static class AngleChangeAction implements ActionListener, FocusListener{
		private RotationEditor editPanel;
		public AngleChangeAction(RotationEditor rep)
		{ this.editPanel = rep; }
		
		public void focusGained(FocusEvent fe){}
		public void focusLost(FocusEvent fe){
			JTextField source = (JTextField)fe.getSource();
			changeTheta(source);
		}// end focusLost(FocusEvent)
		public void actionPerformed(ActionEvent ae){
			JTextField source = (JTextField)ae.getSource();
			changeTheta(source);
		}// end actionPerformed(ActionEvent)
		
		private void changeTheta(JTextField source){
			String strtheta = source.getText();
			
			try{
				float theta = Float.parseFloat(strtheta);
				if(source == editPanel.anglePhiField)
					editPanel.phiDeg = theta;
				else if(source == editPanel.angleThtField)
					editPanel.thetaDeg = theta;
				else if(source == editPanel.angleAlphaField)
					editPanel.alphaDeg = theta;
				editPanel.refresh();
			}catch(Exception excp){
				agdr.robodari.plugin.rme.appl.ExceptionHandler.onlyDecimalAccepted();
				
			}
		}// end changeTheta(JtextField)
	}// end action AngleFromXZCHangeAction\
	static class ScaleChangeAction implements ActionListener, FocusListener{
		private RotationEditor editor;
		public ScaleChangeAction(RotationEditor editor)
		{ this.editor = editor; }
		
		public void focusGained(FocusEvent fe){}
		public void focusLost(FocusEvent fe){
			scaleChanged((JTextField)fe.getSource());
		}// end focus:Lost(FocusEvent)
		
		public void actionPerformed(ActionEvent ae){
			scaleChanged((JTextField)ae.getSource());
		}// end actionPerformed(ActionEvent)
		
		public void scaleChanged(JTextField source){
			String strtheta = source.getText();
			
			try{
				float theta = Float.parseFloat(strtheta);
				editor.canvas.setScale(theta);
			}catch(Exception excp){
				agdr.robodari.plugin.rme.appl.ExceptionHandler.onlyDecimalAccepted();
				
			}
		}// end scaleChanged(JTextField)
	}// end action ScaleChangeAction
	
	
	
	static class ClosingAction implements ActionListener{
		RotationEditor re;
		ClosingAction(RotationEditor re)
		{ this.re = re; }
		
		public void actionPerformed(ActionEvent ae){
			if(re.getInterface().getMainEditor() != null)
				re.getInterface().getMainEditor().repaint();
			else{
				SideSketchViewer ssv;
				for(int i = 0; i < re.getInterface().getSideSketchViewerCount(); i++){
					ssv = re.getInterface().getSideSketchViewer(i);
					if(ssv != null)
						ssv.repaint();
				}
			}
		}// end actionPerformed(ActionEvent)
	}// end action ClosingAction
}
