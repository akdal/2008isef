package agdr.robodari.plugin.rme.gui;

import java.util.*;
import java.awt.*;
import java.awt.geom.*;
import java.awt.image.AffineTransformOp;
import java.awt.event.*;
import javax.swing.*;
import javax.vecmath.*;
//import javax.media.j3d.*;

import agdr.library.math.MathUtility;
import agdr.robodari.appl.*;
import agdr.robodari.comp.*;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.gui.*;
import agdr.robodari.plugin.rme.rlztion.*;

public class SideSketchViewer extends JPanel implements
		MouseListener, MouseMotionListener,
		SketchModelListener, SketchEditorListener,
		SketchScreen, ComponentListener{
	private static Stroke axisStroke;
	private static Paint axisPaint;
	private static Paint axisHiddenPaint;
	private static Paint gridPaint;
	
	private static Stroke lineStroke;
	
	static{
		axisStroke = new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		axisPaint = new Color(0, 0, 0, 70);
		axisHiddenPaint = new Color(0, 0, 0, 20);
		gridPaint = new Color(128, 128, 128, 70);
		
		lineStroke = new BasicStroke(0.5f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
	}// end static block
	
	public static Stroke getAxisStroke()
	{ return axisStroke; }
	public static Paint getAxisPaint()
	{ return axisPaint; }
	public static Paint getAxisHiddenPaint()
	{ return axisHiddenPaint; }
	public static Paint getGridPaint()
	{ return gridPaint; }
	
	public static Stroke getLineStroke()
	{ return lineStroke; }
	
	
	
	private Projector projector;
	private DefaultMainSketchEditor mainEditor;
	private SketchModel model;
	
	private Vector<SkObject> objects = new Vector<SkObject>();
	private Hashtable<SkObject, Polygon> boundPolygons = new Hashtable<SkObject, Polygon>();
	private Hashtable<SkObject, Picture> storedPictures = new Hashtable<SkObject, Picture>();
	
	private Point axismov_startp;
	private Point axismov_org;
	
	private float scale = 6.0f;
	private float axisStartXProp = 0.5f;
	private float axisStartYProp = 0.5f;
	
	
	public SideSketchViewer(DefaultMainSketchEditor mainEditor, Projector projector){
		this.mainEditor = mainEditor;
		this.projector = projector;
		
		this.setBackground(Color.white);
		this.setOpaque(true);
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.setBorder(new javax.swing.border.EtchedBorder());
		this.addComponentListener(this);
		
		if(mainEditor != null){
			mainEditor.addSketchEditorListener(this);
			loadSketch(mainEditor.getModel());
		}
	}// end Constructor(SketchModel)
	
	
	public SketchModel getModel()
	{ return model; }
	public DefaultMainSketchEditor getMainEditor()
	{ return mainEditor; }
	public Projector getProjector()
	{ return projector; }
	
	public float getScale()
	{ return scale; }
	public void setScale(float scale){
		this.scale = scale;
		Set<SkObject> objects = storedPictures.keySet();
		for(SkObject eachobj : objects){
			this.refreshBound(eachobj);
			this.createPicture(eachobj);
		}
		repaint();
	}// end setScale(double)
	
	public float getAxisStartX()
	{ return (float)super.getWidth() * axisStartXProp; }
	public float getAxisStartY()
	{ return (float)super.getHeight() * axisStartYProp; }

	public float getAxisStartXProp()
	{ return axisStartXProp; }
	public void setAxisStartXProp(float p){
		this.axisStartXProp = p;
		repaint();
	}// end setAxisStartXProp(float)
	public float getAxisStartYProp()
	{ return axisStartYProp; }
	public void setAxisStartYProp(float p){
		this.axisStartYProp = p;
		repaint();
	}// end setAxisStartXProp(float)
	
	
	
	public void mouseClicked(MouseEvent me){}
	public void mouseEntered(MouseEvent me){}
	public void mouseExited(MouseEvent me){}
	public void mouseMoved(MouseEvent me){}
	
	public void mousePressed(MouseEvent me){
		this.axismov_startp = me.getPoint();
		if(this.axismov_org == null)
			this.axismov_org = new Point((int)this.getAxisStartX(), (int)this.getAxisStartY());
		else
			this.axismov_org.setLocation(this.getAxisStartX(), this.getAxisStartY());
	}// end mousePressed(MouseEvent)
	public void mouseReleased(MouseEvent me){
		this.axismov_startp = null;
	}// end mouseReleased(MouseEvent)
	public void mouseDragged(MouseEvent me){
		this.axisStartXProp = (float)(axismov_org.x + me.getX() - axismov_startp.x) / (float)this.getWidth();
		this.axisStartYProp = (float)(axismov_org.y + me.getY() - axismov_startp.y) / (float)this.getHeight();
		
		Collection<SkObject> objects = storedPictures.keySet();
		for(SkObject eachobj : objects)
			Picture.refreshPicturePos(eachobj, storedPictures.get(eachobj), this);
		repaint();
	}// end mouseDragged(MouseEvent)
	
	
	
	public void loadSketch(SketchModel model){
		if(this.model != null)
			this.model.removeSketchModelListener(this);
		
		boundPolygons.clear();
		storedPictures.clear();
		objects.clear();
		
		this.model = model;
		if(this.model != null){
			this.model.addSketchModelListener(this);
			
			for(SkObject eachobj : model.getObjects()){
				this.addObject(eachobj);
			}
		}
	}// end loadSketch(SkethcModel)
	
	
	
	
	protected void addObject(SkObject eachobj){
		objects.add(eachobj);
		Polygon p = getBound(eachobj);
		boundPolygons.put(eachobj, p);
		storedPictures.put(eachobj, createPicture(eachobj));
	}// end addObject(PhOBject)
	protected void removeObject(SkObject obj){
		boundPolygons.remove(obj);
		objects.remove(obj);
		storedPictures.remove(obj);
	}// end rmeoveObject(PhOBject)
	public void refreshObject(SkObject appr){
		this.refreshBound(appr);
		this.createPicture(appr);
		this.repaint();
	}// end refreshObject(SkObject)
	
	
	

	
	public Picture getPicture(SkObject obj)
	{ return storedPictures.get(obj); }
	public Picture createPicture(SkObject obj){
		Picture picture = storedPictures.get(obj);
		if(picture == null){
			picture = new Picture(this);
			storedPictures.put(obj, picture);
		}
		Polygon p = getBound(obj);
		Point min = null, max = null;
		
		if(p.npoints != 0){
			min = new Point(21474836, 21474836);
			max = new Point(-21474836, -21474836);
			for(int i = 0; i < p.npoints; i++){
				if(min.x > p.xpoints[i])
					min.x = p.xpoints[i];
				if(min.y > p.ypoints[i])
					min.y = p.ypoints[i];
				
				if(max.x < p.xpoints[i])
					max.x = p.xpoints[i];
				if(max.y < p.ypoints[i])
					max.y = p.ypoints[i];
			}
	
			if(max.x == min.x)
				max.x++;
			if(max.y == min.y)
				max.y++;
		}else{
			max = new Point(1, 1);
			min = new Point(0, 0);
		}
		
		Picture.resetPicture(obj, picture, this, max, min, true);
		return picture;
	}// end createPicture(SkObject)
	
	public Polygon getBound(SkObject appr){
		if(model == null) return null;
		else if(appr == null) return null;
		if(!boundPolygons.containsKey(appr)){
			refreshBound(appr);
		}
		return boundPolygons.get(appr);
	}// end getBound(SkObject)
	
	public void refreshBound(SkObject appr){
		Point3f[] points3d = appr.getAllPoints();
		Matrix4f matrix = new Matrix4f(appr.getTransform());
		Point[] points = new Point[points3d.length];
		matrix.m03 = matrix.m13 = matrix.m23 = 0;
		
		float[] tmpprjp;
		for(int i = 0; i < points.length; i++){
			tmpprjp = this.getProjector().project(points3d[i], matrix, this.getScale());
			points[i] = new Point();
			points[i].x = (int)tmpprjp[0];
			points[i].y = (int)tmpprjp[1];
		}
			
		//for(int i = 0; i < points.length; i++)
		//	System.out.println(i + " : (" + (points[i].x - editor.getCenterX()) + " , " + (points[i].y - editor.getCenterY()) + ")");
		
		Polygon projectedBound = MathUtility.convexHull(points);
		
		// 이제 이 바운드를 어느 정도 넓혀 주어야 한다.
		projectedBound = MathUtility.expand(projectedBound,
				DefaultMainSketchEditor.getSenseRange());
		
		boundPolygons.put(appr, projectedBound);
	}// end refreshBound()
	
	
	
	
	
	
	

	private AffineTransformOp bufimageop = new AffineTransformOp(new AffineTransform(),
			AffineTransformOp.TYPE_BILINEAR);
	public void paint(Graphics g){
		try{
			super.paint(g);
			if(g == null) return;
			
			Graphics2D g2d = (Graphics2D)g;
			//g2d.setColor(Color.white);
			//g2d.fillRect(0, 0, 2000, 1200);
			int wid = this.getWidth(), hei = this.getHeight();
			int asx = (int)this.getAxisStartX(), asy = (int)this.getAxisStartY();
			int lim = Math.max(Math.max(wid - asx, wid + asx), Math.max(hei - asy, hei + asy));
			SketchEditorUtility.drawAxis(this, g2d,
					SideSketchViewer.getAxisStroke(),
					SideSketchViewer.getAxisPaint(),
					SideSketchViewer.getAxisHiddenPaint(),
					SideSketchViewer.getAxisPaint(),
					lim);
			SketchEditorUtility.drawGrid(this, g2d, SideSketchViewer.getAxisStroke(), SideSketchViewer.getAxisPaint(), lim);
			if(model == null) return;
			
			SkObject[] objects = model.getObjects();
			for(int i = 0; i < objects.length; i++){
				SkObject obj = objects[i];
				//obj.paint(g2d, this);
				
				Picture pic = storedPictures.get(obj);
				if(pic != null)
					// 아직 picture가 덜 만들어진 상황일수도
					g2d.drawImage(pic.normal, bufimageop,
						-pic.centerPos.x + pic.pos.x,
						-pic.centerPos.y + pic.pos.y);
			}
		}catch(Exception excp){
			BugManager.log(excp, true);
		}
	}// end paint(Graphics g)
	
	
	/* ********************************************************
	 *                   SketchModelListener
	 *8********************************************************/
	
	public void componentAdded(SketchModelEvent sme){
		addObject(sme.getObject());
		repaint();
	}// end componentAdded(SketchModelEvent sme)
	public void componentRemoved(SketchModelEvent sme){
		for(SkObject eachobj : objects)
			if(eachobj == sme.getChangedItem()){
				removeObject(eachobj);
				break;
			}
		repaint();
	}// end componentRemoved(SketchModelEvent)
	
	/* *********************************************************
	 *                  SketchEditorListener
	 *******************************************************/
	public void modelChanged(SketchEditorEvent see){
		SketchModel newModel = see.getModel();
		if(newModel != this.getModel())
			loadSketch(newModel);
	}// end modelChanged(SketchEditorEvent)
	public void componentDrifted(SketchEditorEvent see){
		Picture.refreshPicturePos(see.getAppearance(), storedPictures.get(see.getAppearance()), this);
		repaint();
	}// edn modelDrifted(SketchEditorEvent)
	public void componentRotated(SketchEditorEvent see){
		BugManager.printf("SideSV : componentRotated()\n");
		storedPictures.put(see.getAppearance(), this.createPicture(see.getAppearance()));
		repaint();
	}// end modelRotated(SketchEditorEnvet)
	public void componentSelected(SketchEditorEvent see){
		repaint();
	}// end componentSEkected(SketchEditorEvent)
	public void componentDeselected(SketchEditorEvent see){
		repaint();
	}// end componentDeselected(SketchEditorEvent)
	
	
	public void componentHidden(ComponentEvent ce){}
	public void componentMoved(ComponentEvent ce){}
	public void componentResized(ComponentEvent ce){
		Iterable<SkObject> enumr = storedPictures.keySet();
		for(SkObject eachobj : enumr)
			Picture.refreshPicturePos(eachobj, storedPictures.get(eachobj), this);
	}// end componentREsized(ComponentEvent)
	public void componentShown(ComponentEvent ce){}
}
