package agdr.robodari.plugin.rme.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.vecmath.Matrix4f;

import agdr.robodari.plugin.rme.edit.*;


public class SkObjectViewer extends JPanel implements SketchScreen{
	private float scale;
	private SkObject object;
	private Projector projector;
	
	public SkObjectViewer(){
		super.setBorder(new LineBorder(Color.gray));
		super.setBackground(Color.white);
		super.setOpaque(true);
		this.scale = 20;
		this.projector = new Projector.OrthographicProjector((float)(Math.PI * 5.0 / 4.0), (float)(Math.PI * 3.0 / 4.0), 0);
	}// end Constructor()

	
	
	public SkObject getObject()
	{ return object; }
	public void setObject(SkObject object)
	{ this.object = object; repaint(); }
	
	
	public void paint(Graphics g){
		super.paint(g);
		if(g == null) return;
		
		Graphics2D g2d = (Graphics2D)g;
		SketchEditorUtility.drawAxis(this, g2d,
				DefaultMainSketchEditor.getAxisStroke(),
				DefaultMainSketchEditor.getAxisPaint(),
				DefaultMainSketchEditor.getAxisHiddenPaint(),
				DefaultMainSketchEditor.getZAxisPaint(),
				100);
		SketchEditorUtility.drawGrid(this, g2d,
				DefaultMainSketchEditor.getAxisStroke(),
				DefaultMainSketchEditor.getAxisPaint(),
				80);
		g2d.setColor(Color.gray);
		
		if(object == null){
			g2d.drawString("개체가 열리지 않았습니다",
					this.getWidth() / 2 - 70, this.getHeight() / 2 - 10);
			return;
		}
		//SkObject object = editPanel.getObject();
		
		Matrix4f original = new Matrix4f(object.getTransform());
		original.m03 = original.m13 = original.m23 = 0;
		object.paint(g2d, original, this);
	}// end paint(Graphics)

	
	public Projector getProjector()
	{ return this.projector; }
	
	public float getAxisStartX()
	{ return super.getWidth() / 2.0f; }
	public float getAxisStartY()
	{ return super.getHeight() / 2.0f; }
	
	public java.awt.Polygon getBound(SkObject obj)
	{ return null; }
	
	public float getScale()
	{ return scale; }
	public void setScale(float sc)
	{ this.scale = sc; repaint(); }
}// end class REPCanvas
