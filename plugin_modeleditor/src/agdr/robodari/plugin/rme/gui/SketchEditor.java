package agdr.robodari.plugin.rme.gui;

import java.awt.*;
import java.awt.event.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Hashtable;

import javax.vecmath.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
//import javax.media.j3d.*;


import agdr.robodari.appl.BugManager;
import agdr.robodari.comp.*;
import agdr.robodari.plugin.rme.appl.*;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.store.*;


public class SketchEditor extends JPanel implements SketchEditorInterface{
	public static final Projector[] PROJECTORS =
		new Projector[3];
	public final static String[] PROJECTOR_NAMES = new String[3];
	public final static Dimension PROJECTOR_SIZE = new Dimension(300, 230);
	public final static Dimension SVIEWERFRAME_SIZE = new Dimension(300, 250);
	
	public final static int HORIZONTAL_MODE = 0;
	public final static int VERTICAL_MODE = 1;
	
	
	static{
		PROJECTORS[0] = new Projector(){
			public float[] project(Point3f p, Matrix4f trans, float scale){
				Point3f point = new Point3f(p.x, p.y, p.z);
				if(trans != null)
					trans.transform(point);
				
				return new float[]{scale * point.x, -scale * point.y};
			}// end project
			public float[] project(float x, float y, float z, Matrix4f trans, float scale){
				return project(new Point3f(x, y, z), trans, scale);
			}// end project
		};
		PROJECTORS[1] = new Projector(){
			public float[] project(Point3f p, Matrix4f trans, float scale){
				Point3f point = new Point3f(p.x, p.y, p.z);
				if(trans != null)
					trans.transform(point);
				
				return new float[]{scale * point.y, -scale * point.z};
			}// end project
			public float[] project(float x, float y, float z, Matrix4f trans, float scale){
				return project(new Point3f(x, y, z), trans, scale);
			}// end project
		};
		PROJECTORS[2] = new Projector(){
			public float[] project(Point3f p, Matrix4f trans, float scale){
				Point3f point = new Point3f(p.x, p.y, p.z);
				if(trans != null)
					trans.transform(point);
				
				return new float[]{scale * point.x, -scale * point.z};
			}// end project
			public float[] project(float x, float y, float z, Matrix4f trans, float scale){
				return project(new Point3f(x, y, z), trans, scale);
			}// end project
		};
		PROJECTOR_NAMES[0] = "Front";
		PROJECTOR_NAMES[1] = "Right";
		PROJECTOR_NAMES[2] = "Up";
	}// end static block
	
	
	
	
	private DefaultMainSketchEditor mainEditor;
	private SideSketchViewer[] sideViewers = new SideSketchViewer[3];
	private JPanel southPanel;
	private ControlPanel controlPanel;
	
	private JScrollPane ssvsScrollPane;
	private JPanel ssvsPanel;
	private JInternalFrame sviewerFrame;
	private RbModelEditorManager editorManager;
	private SketchModel model;
	private int layoutMode = SketchEditor.HORIZONTAL_MODE;
	

	private Hashtable<Class<? extends RobotComponent>, CompEditor> compEditors
			= new Hashtable<Class<? extends RobotComponent>, CompEditor>();
	private RotationEditor rotationEditor;
	private CompEditor currentEditor;
	private GroupEditor groupEditor;
	private ProjectionEditor projEditor;
	
	
	
	
	public SketchEditor(RbModelEditorManager manager, Frame frame_parent, SketchModel model) throws Exception{
		this.editorManager = manager;
		this.model = model;

		initEditorComponents(frame_parent);
		mainEditor = new DefaultMainSketchEditor(this);
		mainEditor.setPreferredSize(new Dimension(1300, 700));

		for(int i = 0; i < sideViewers.length; i++){
			if(i == 0){
				sideViewers[i] = new SideSketchViewer(mainEditor, PROJECTORS[i]);
				sideViewers[i].setAxisStartXProp(0.3f);
				sideViewers[i].setAxisStartYProp(0.7f);
			}else
				sideViewers[i] = new SideSketchViewer(mainEditor, PROJECTORS[i]);
		}
		this.projEditor = new ProjectionEditor(frame_parent){
			public void finish(){
				mainEditor.setProjector(projEditor.getProjector());
				projEditor.setVisible(false);
			}// end finish()
		};

		
		southPanel = createSouthPanel();
		JScrollPane scrollPane = new JScrollPane(mainEditor,
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		
		super.setLayout(new BorderLayout());
		super.add(southPanel, BorderLayout.SOUTH);
		super.add(scrollPane, BorderLayout.CENTER);
	}// end Constructor()
	private void initEditorComponents(Frame frame_parent){
		Class<? extends RobotComponent>[] compclses = RobotCompEditorStore.getRegisteredComps();
		Object[] con_args = new Object[]{ this, frame_parent };
		Constructor<CompEditor> cons = null;
		
		for(Class<? extends RobotComponent> eachcls : compclses){
			try{
				Class instcls = RobotCompEditorStore.getCompEditorClass(eachcls);
				
				cons = instcls.getConstructor(new Class[]{ SketchEditorInterface.class, Frame.class });
				compEditors.put(eachcls, cons.newInstance(con_args));
			}catch(NoSuchMethodException nsme){
				BugManager.log(nsme, true);
			}catch(InstantiationException ie){
				BugManager.log(ie, true);
			}catch(IllegalAccessException iae){
				BugManager.log(iae, true);
			}catch(InvocationTargetException ite){
				BugManager.log(ite, true);
			}
		}
		
		rotationEditor = new RotationEditor(this, frame_parent);
		groupEditor = new GroupEditor(this, frame_parent);
	}// end initEditorComponents()
	
	private JPanel createSouthPanel(){
		southPanel = new JPanel();
		southPanel.setLayout(new BorderLayout());
		
		JDesktopPane desktopPane = new JDesktopPane();
		desktopPane.setLayout(new BorderLayout());
		sviewerFrame = createInternalFrame();
		desktopPane.add(sviewerFrame, BorderLayout.CENTER);

		controlPanel = new ControlPanel(this);
		//southPanel.add(controlPanel, BorderLayout.NORTH);
		southPanel.add(desktopPane, BorderLayout.CENTER);
		return southPanel;
	}// end createSouthPanel()
	private JInternalFrame createInternalFrame(){
		ssvsPanel = new JPanel();
		ssvsPanel.setLayout(new GridLayout(0, 3));
		ssvsPanel.setPreferredSize(new Dimension(PROJECTOR_SIZE.width * PROJECTORS.length,
				PROJECTOR_SIZE.height));
		ssvsScrollPane = new JScrollPane(ssvsPanel);
		
		sviewerFrame = new JInternalFrame("Side Viewers");
		sviewerFrame.getComponent(1).removeMouseMotionListener(sviewerFrame
				.getComponent(1).getMouseMotionListeners()[0]);
		
		for(int i = 0; i < sideViewers.length; i++)
			ssvsPanel.add(sideViewers[i]);
		sviewerFrame.setPreferredSize(SVIEWERFRAME_SIZE);
		sviewerFrame.setContentPane(ssvsScrollPane);
		sviewerFrame.setResizable(false);
		sviewerFrame.setIconifiable(true);
		sviewerFrame.setVisible(true);
		sviewerFrame.setFrameIcon(null);
		sviewerFrame.setBorder(new javax.swing.border.EtchedBorder());
		
		return sviewerFrame;
	}// end createInternalFrame()
	
	
	
	
	
	
	public int getLayoutMode()
	{ return layoutMode; }
	public void setLayoutMode(int layoutMode){
		if(this.layoutMode == layoutMode)
			return;
		this.layoutMode = layoutMode;
		
		if(this.layoutMode == HORIZONTAL_MODE){
			ssvsPanel.setPreferredSize(new Dimension(PROJECTOR_SIZE.width * PROJECTORS.length,
					PROJECTOR_SIZE.height));
			super.add(southPanel, BorderLayout.SOUTH);
			ssvsPanel.setLayout(new GridLayout(0, 3));
		}else{
			ssvsPanel.setPreferredSize(new Dimension(PROJECTOR_SIZE.width,
					PROJECTOR_SIZE.height * PROJECTORS.length));
			super.add(southPanel, BorderLayout.EAST);
			ssvsPanel.setLayout(new GridLayout(3, 0));
		}
	}// end setLAyoutMode(int)

	/*
	private float[][] msrmentRates = new float[sideViewers.length][4];
	private Dimension beforeSize;
	public void componentResized(ComponentEvent ce){}
	public void componentHidden(ComponentEvent ce){}
	public void componentMoved(ComponentEvent ce){}
	public void componentShown(ComponentEvent ce){}
	*/
	
	
	public ControlPanel getControlPanel()
	{ return controlPanel; }
	
	public RbModelEditorManager getRMEditorManager()
	{ return editorManager; }
	
	
	public void loadSketch(SketchModel sk){
		System.out.println("SketchEditor.loadSketch");
		this.model = sk;
		mainEditor.loadSketch(sk);
	}// end loadSketch(SketchModel)
	public SketchModel getModel()
	{ return model; }
	public DefaultMainSketchEditor getMainEditor()
	{ return mainEditor; }
	
	

	public ProjectionEditor getProjEditor()
	{ return projEditor; }
	
	
	public RotationEditor getRotationEditor()
	{ return rotationEditor; }
	public GroupEditor getGroupEditor()
	{ return groupEditor; }
	public CompEditor getCurrentCompEditor()
	{ return currentEditor; }
	public void setCurrentCompEditor(CompEditor ce){
		if(this.currentEditor != null && this.currentEditor.isVisible())
			this.currentEditor.setVisible(false);
		this.currentEditor = ce;
		if(ce != null)
			ce.setVisible(true);
	}// end showCompEditor(CompEditor)
	public CompEditor getStoredCompEditor(Class<? extends RobotComponent> cls)
	{ return compEditors.get(cls); }
	
	
	
	
	public SideSketchViewer getSideSketchViewer(int idx)
	{ return sideViewers[idx]; }
	public int getSideSketchViewerCount()
	{ return sideViewers.length; }
	
	//public JInternalFrame getSideSketchViewerFrame()
	//{ return sviewerFrame; }
	
	
	public void addObject(RobotComponent rc, Matrix4f trans){
		if(model != null){
			try{
				System.out.println("SketchEditor : addObject(rc, trans)");
				model.addRobotComponent(rc, trans);
			}catch(ModelNotRegisteredException excp){
				ExceptionHandler.modelNotRegistered(rc.getClass());
			}catch(Exception excp){
				agdr.robodari.appl.BugManager.log(excp, true);
			}
		}else
			ExceptionHandler.sketchNotOpened();
	}// end addComponent(RobotComponent rc)
	public SkObject getObject(RobotComponent rc){
		return mainEditor.getObject(rc);
	}// end getObject(RobotCompoent)
	
	
	public void refreshObject(SkObject obj){
		mainEditor.refreshObject(obj);
		for(SideSketchViewer eachssv : sideViewers)
			eachssv.refreshObject(obj);
		if(obj.getRobotComponent() != null){
			ConnObject cnobj = this.getRMEditorManager().getEditingRobotModel().getConnectionModel().getObject(obj.getRobotComponent());
			cnobj.refresh();
			this.getRMEditorManager().getConnectionEditor().resetPicture(cnobj);
			this.getRMEditorManager().getEditingRobotModel().getConnectionModel().resetPositionIndex(cnobj);
		}
	}// end refreshObject(SkObject)
	public void translateObject(RobotComponent obj, float x, float y, float z){
		SkObject skobj = this.getObject(obj);
		
		float[] tmpprjp;
		Projector projector = getMainEditor().getProjector();
		
		tmpprjp = projector.project(x, y, z, null, getMainEditor().getScale());
		Point mprjp = new Point((int)tmpprjp[0], (int)tmpprjp[1]);
		
		Matrix4f trans = skobj.getTransform();
		tmpprjp = projector.project(trans.m03, trans.m13, trans.m23,
				null, getMainEditor().getScale());
		Point oprjp = new Point((int)tmpprjp[0], (int)tmpprjp[1]);
		trans.m03 = x;
		trans.m13 = y;
		trans.m23 = z;
		
		Picture pic = getMainEditor().getPicture(skobj);
		pic.pos.x += mprjp.x - oprjp.x;
		pic.pos.y += mprjp.y - oprjp.y;
		
		this.mainEditor.repaint();
		for(SideSketchViewer eachssv : sideViewers)
			eachssv.repaint();
		
		mainEditor.fireComponentDrifted(skobj);
	}// end translateObject(SkObject, float, float)
	/*
	private void translateObject_inMainEditor(SkObject obj, float x, float y, float z){
		float[] tmpprjp;
		Projector projector = getMainEditor().getProjector();
		
		tmpprjp = projector.project(x, y, z, null, getMainEditor().getScale());
		Point mprjp = new Point((int)tmpprjp[0], (int)tmpprjp[1]);
		System.out.println("SketchEditor:translateObjec_inMainEditor : " + x + " , " + y + " , " + z);
		Matrix4f trans = obj.getTransform();
		tmpprjp = projector.project(trans.m03, trans.m13, trans.m23,
				null, getMainEditor().getScale());
		Point oprjp = new Point((int)tmpprjp[0], (int)tmpprjp[1]);
		trans.m03 = x;
		trans.m13 = y;
		trans.m23 = z;
		
		Picture pic = getMainEditor().getPicture(obj);
		pic.pos.x += mprjp.x - oprjp.x;
		pic.pos.y += mprjp.y - oprjp.y;
	}// end translateObject_inMainEditor(RobotComponent, float, float, float)
	private void translateObject_inSSV(SkObject obj, float x, float y, float z){
		Matrix4f trans = obj.getTransform();
		for(int i = 0; i < this.getSideSketchViewerCount(); i++){
			SideSketchViewer ssv = this.getSideSketchViewer(i);
			Point prjp = ssv.getProjector().project(x, y, z, null, ssv.getScale());
			
			Point oprjp = ssv.getProjector().project(trans.m03, trans.m13, trans.m23, null, ssv.getScale());
			
			Picture pic = ssv.getPicture(obj);
			System.out.println("prjp : " + prjp + " , oprjp : " + oprjp);
			pic.pos.x += prjp.x - oprjp.x;
			pic.pos.y += prjp.y - oprjp.y;
		}
	}// end translateObject_inSSV(RobotComponent, float, float, float)
	*/
	
	
	
	
	
	
	public static class ControlPanel extends JPanel{
		private SketchEditor sketchEditor;
		
		public ControlPanel(SketchEditor sketchEditor){
			this.sketchEditor = sketchEditor;

			setLayout(new BorderLayout());
			setPreferredSize(new Dimension(0, 25));
			setOpaque(true);
			
			JPanel left = new JPanel();
			FlowLayout layout = new FlowLayout();
			layout.setVgap(2);
			left.setLayout(layout);

			MESChangeAction mesaction = new SketchEditor.MESChangeAction(sketchEditor);
			
			JTextField mainEditorScaleField = new JTextField();
			mainEditorScaleField.setPreferredSize(new Dimension(40, 21));
			mainEditorScaleField.setHorizontalAlignment(JTextField.RIGHT);
			mainEditorScaleField.addActionListener(mesaction);
			mainEditorScaleField.addFocusListener(mesaction);
			mainEditorScaleField.setText(String.valueOf(sketchEditor.mainEditor.getScale()));

			
			JPanel center = new JPanel();
			center.setLayout(layout);
			JTextField xfield = new JTextField();
			JTextField yfield = new JTextField();
			JTextField zfield = new JTextField();
			
			TranslateAction action = new SketchEditor.TranslateAction(xfield, yfield, zfield,
					sketchEditor);
			sketchEditor.mainEditor.addSketchEditorListener(action);
			Dimension xyzsize = new Dimension(58, 21);
			xfield.setPreferredSize(xyzsize);
			xfield.setHorizontalAlignment(JTextField.RIGHT);
			yfield.setPreferredSize(xyzsize);
			yfield.setHorizontalAlignment(JTextField.RIGHT);
			zfield.setPreferredSize(xyzsize);
			zfield.setHorizontalAlignment(JTextField.RIGHT);
			
			center.add(new JLabel("X:"));
			center.add(xfield);
			center.add(new JLabel("cm "));
			center.add(new JLabel("Y:"));
			center.add(yfield);
			center.add(new JLabel("cm "));
			center.add(new JLabel("Z:"));
			center.add(zfield);
			center.add(new JLabel("cm "));
			
			
			JPanel right = new JPanel();
			right.setLayout(layout);

			SVSChangeAction svsaction = new SketchEditor.SVSChangeAction(sketchEditor);
			
			JTextField sideViewerScaleField = new JTextField();
			sideViewerScaleField.setPreferredSize(new Dimension(40, 21));
			sideViewerScaleField.setHorizontalAlignment(JTextField.RIGHT);
			sideViewerScaleField.addActionListener(svsaction);
			sideViewerScaleField.addFocusListener(svsaction);
			sideViewerScaleField.setText(String.valueOf(sketchEditor.sideViewers[0].getScale()));
			
			right.add(new JLabel("Dynamic projector scale : "));
			right.add(mainEditorScaleField);
			left.add(new JLabel("3-dim projector scale : "));
			left.add(sideViewerScaleField);
			
			add(left, BorderLayout.EAST);
			add(center, BorderLayout.CENTER);
			add(right, BorderLayout.WEST);
		}// end Constructor(SketchEditor)
	}// end class ControlPanel
	
	
	
	// main editor scale
	static class MESChangeAction implements ActionListener, FocusListener{
		private SketchEditor editor;
		public MESChangeAction(SketchEditor editor)
		{ this.editor = editor; }
		
		public void actionPerformed(ActionEvent ae){
			change((JTextField)ae.getSource());
		}// end actionPerformed(ActionEvent)
		public void focusGained(FocusEvent fe){
		}// end focusGained(FocusEvent)
		public void focusLost(FocusEvent fe){
			change((JTextField)fe.getSource());
		}// end focusLost(FocusEvent)
		
		void change(JTextField field){
			try{
				String szval = field.getText();
				float val = Float.parseFloat(szval);
				editor.getMainEditor().setScale(val);
			}catch(Exception excp){
				ExceptionHandler.onlyDecimalAccepted();
				field.setText(String.valueOf(editor.getMainEditor().getScale()));
			}
		}// end change(JtextField)
	}// end action MESChangeAction
	public static class ProjEditAction implements ActionListener{
		private SketchEditor editor;
		public ProjEditAction(SketchEditor editor)
		{ this.editor = editor; }
		
		public void actionPerformed(ActionEvent ae){
			editor.getProjEditor().start((Projector.OrthographicProjector)editor.getMainEditor().getProjector());
		}// end aPf
	}// end class ProjEditAction
	static class SVSChangeAction implements ActionListener, FocusListener{
		private SketchEditor editor;
		public SVSChangeAction(SketchEditor editor)
		{ this.editor = editor; }
		
		public void actionPerformed(ActionEvent ae){
			change((JTextField)ae.getSource());
		}// end actionPerformed(ActionEvent)
		public void focusGained(FocusEvent fe){
		}// end focusGained(FocusEvent)
		public void focusLost(FocusEvent fe){
			change((JTextField)fe.getSource());
		}// end focusLost(FocusEvent)
		
		void change(JTextField field){
			try{
				String szval = field.getText();
				float val = Float.parseFloat(szval);
				for(int i = 0; i < SketchEditor.PROJECTORS.length; i++)
					editor.getSideSketchViewer(i).setScale(val);
			}catch(Exception excp){
				ExceptionHandler.onlyDecimalAccepted();
				field.setText(String.valueOf(editor.getMainEditor().getScale()));
			}
		}// end change(JtextField)
	}// end action SVSChangeAction
	
	
	
	public static class TranslateAction implements SketchEditorListener, FocusListener, ActionListener{
		private JTextField xfield;
		private JTextField yfield;
		private JTextField zfield;
		private SketchEditor editor;
		
		public TranslateAction(JTextField xfield, JTextField yfield, JTextField zfield, SketchEditor editor){
			this.xfield = xfield;
			this.yfield = yfield;
			this.zfield = zfield;
			this.editor = editor;
			
			xfield.setEditable(false);
			yfield.setEditable(false);
			zfield.setEditable(false);
			
			xfield.addFocusListener(this);
			xfield.addActionListener(this);
			yfield.addFocusListener(this);
			yfield.addActionListener(this);
			zfield.addFocusListener(this);
			zfield.addActionListener(this);
		}// end Constructor(JtextField, JtextField, JtextField, SKetchEditor)
		
		
		void reset(){
			SkObject[] objects = editor.getMainEditor().getSelectedItems();
			
			if(objects.length == 0){
				xfield.setEditable(false);
				yfield.setEditable(false);
				zfield.setEditable(false);
			}else{
				xfield.setEditable(true);
				yfield.setEditable(true);
				zfield.setEditable(true);
				
				Matrix4f trans = objects[0].getTransform();
				
				String xval = String.valueOf(trans.m03);
				if(xval.length() > 8)
					xval = xval.substring(0, 8);
				String yval = String.valueOf(trans.m13);
				if(yval.length() > 8)
					yval = yval.substring(0, 8);
				String zval = String.valueOf(trans.m23);
				if(zval.length() > 8)
					zval = zval.substring(0, 8);
				
				xfield.setText(xval);
				yfield.setText(yval);
				zfield.setText(zval);
			}
		}// end reset()
		
		
		public void componentRotated(SketchEditorEvent see){}
		public void componentSelected(SketchEditorEvent see){
			reset();
		}// end componentSelecteD(SketchEditorEvent)
		public void componentDeselected(SketchEditorEvent see){
			xfield.setEditable(false);
			yfield.setEditable(false);
			zfield.setEditable(false);
		}// end componentDeselected(SketchEditorEvent)
		public void modelChanged(SketchEditorEvent see){
			reset();
		}// end modelChanged(SketchEditorEVent)
		public void componentDrifted(SketchEditorEvent see){
			reset();
		}// end componentDrifted(SketchEditorEvent)
		
		public void actionPerformed(ActionEvent ae){
			translate();
		}// end actionPerformed(ActionEvent)
		public void focusGained(FocusEvent fe){}
		public void focusLost(FocusEvent fe)
		{ translate(); }
		
		private void translate(){
			SkObject[] selectedItems = editor.getMainEditor().getSelectedItems();
			if(selectedItems.length == 0)
				return;
			Matrix4f orgtrans = selectedItems[0].getTransform();
			float x = 0, y = 0, z = 0;
			try{
				x = Float.parseFloat(xfield.getText());
				y = Float.parseFloat(yfield.getText());
				z = Float.parseFloat(zfield.getText());
			}catch(Exception excp){
				ExceptionHandler.onlyDecimalAccepted();
				reset();
			}
			
			float ox = orgtrans.m03, oy = orgtrans.m13, oz = orgtrans.m23;
			Matrix4f trans;
			for(SkObject eachsk : selectedItems){
				trans = eachsk.getTransform();
				editor.translateObject(eachsk.getRobotComponent(), x - (ox - trans.m03),
						y - (oy - trans.m13),
						z - (oz - trans.m23));
			}
		}// end translate(JTextField)
	}// end action TranslateAction
}
