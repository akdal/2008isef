package agdr.robodari.plugin.rme.gui;

import agdr.robodari.plugin.rme.edit.*;

public class SketchEditorEvent {
	private SkObject appr;
	private MainSketchEditor editor;
	private SketchModel model;
	
	public SketchEditorEvent(SkObject appr, MainSketchEditor editor){
		this.appr = appr;
		this.editor = editor;
		if(editor != null)
			this.model = editor.getModel();
	}// end Constructor(MAinSketchEditor.Appearance, MainSketchEditor)
	
	public SkObject getAppearance()
	{ return appr; }
	public MainSketchEditor getEditor()
	{ return editor; }
	public SketchModel getModel()
	{ return model; }
	
	public boolean equals(Object obj){
		if(obj == null) return false;
		else if(!(obj instanceof SketchEditorEvent))
			return false;
		
		SketchEditorEvent see = (SketchEditorEvent)obj;
		return see.appr.equals(this.appr);
	}// end equals(Object)
}
