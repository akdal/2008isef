package agdr.robodari.plugin.rme.gui;

public interface SketchEditorListener {
	public void modelChanged(SketchEditorEvent see);
	public void componentSelected(SketchEditorEvent see);
	public void componentDeselected(SketchEditorEvent see);
	public void componentDrifted(SketchEditorEvent see);
	public void componentRotated(SketchEditorEvent see);
}