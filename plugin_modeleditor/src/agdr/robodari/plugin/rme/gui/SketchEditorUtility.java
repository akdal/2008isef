package agdr.robodari.plugin.rme.gui;

import java.awt.*;


public class SketchEditorUtility {
	public static void drawAxis(SketchScreen screen, 
			Graphics2D g2d, Stroke axisStroke, Paint axisPaint,
			Paint axisHiddenPaint, Paint zAxisPaint, float axisLength){
		int cx = (int)screen.getAxisStartX();
		int cy = (int)screen.getAxisStartY();
		float scale = screen.getScale();
		Projector projector = screen.getProjector();
		
		g2d.setStroke(axisStroke);
		
		float[] tmpprjp;
		tmpprjp = projector.project(0, 0, axisLength, null, 1);
		Point zplus = new Point((int)tmpprjp[0], (int)tmpprjp[1]);
		tmpprjp = projector.project(axisLength, 0, 0, null, 1);
		Point xplus = new Point((int)tmpprjp[0], (int)tmpprjp[1]);
		tmpprjp = projector.project(0, axisLength, 0, null, 1);
		Point yplus = new Point((int)tmpprjp[0], (int)tmpprjp[1]);
		
		g2d.setPaint(axisHiddenPaint);
		// x minus
		g2d.drawLine(cx, cy, cx - xplus.x, cy - xplus.y);
		g2d.drawString("-x", cx - xplus.x, cy - xplus.y);
		// y minus
		g2d.drawLine(cx, cy, cx - yplus.x, cy - yplus.y);
		g2d.drawString("-y", cx - yplus.x, cy - yplus.y);

		g2d.setPaint(zAxisPaint);
		// z
		g2d.drawLine(cx, cy, cx + zplus.x, cy + zplus.y);
		g2d.drawString("+z", cx + zplus.x, cy + zplus.y);
		g2d.setPaint(axisPaint);
		// x
		g2d.drawLine(cx, cy, cx + xplus.x, cy + xplus.y);
		g2d.drawString("+x", cx + xplus.x, cy + xplus.y);
		g2d.drawLine(cx, cy, cx + yplus.x, cy + yplus.y);
		g2d.drawString("+y", cx + yplus.x, cy + yplus.y);
	}// end drawAxis(float, float, Graphics2D)
	

	public static void drawGrid(SketchScreen ss, Graphics2D g2d, Stroke stroke, Paint paint, double limit){
		g2d.setStroke(stroke);
		g2d.setPaint(paint);
		Projector prj = ss.getProjector();
		float scale = ss.getScale();
		int cx = (int)ss.getAxisStartX();
		int cy = (int)ss.getAxisStartY();
		limit = limit / scale;
		// x : (x1, -0.1, 0) ~ (x1, 0.1, 0)
		// y : (-0.1, y1, 0) ~ (0.1, y1, 0)
		// z : (0.1, -0.1, z1) ~ (-0.1, 0.1, z1)
		
		float[] start, end;
		int seg = 5;
		
		// 1. x
		int pos = seg * 1;
		while(pos < limit){
			start = prj.project(pos, -0.5f, 0, null, scale);
			end = prj.project(pos, 0.5f, 0, null, scale);
			g2d.drawLine((int)start[0] + cx, (int)start[1] + cy, (int)end[0] + cx, (int)end[1] + cy);
			g2d.drawString(String.valueOf(pos), (int)end[0] + cx, (int)end[1] + cy);
			pos += seg;
		}
		pos = -seg;
		while(pos > -limit){
			start = prj.project(pos, -0.5f, 0, null, scale);
			end = prj.project(pos, 0.5f, 0, null, scale);
			g2d.drawLine((int)start[0] + cx, (int)start[1] + cy, (int)end[0] + cx, (int)end[1] + cy);
			g2d.drawString(String.valueOf(pos), (int)end[0] + cx, (int)end[1] + cy);
			pos -= seg;
		}
		// 2. y
		pos = seg * 1;
		while(pos < limit){
			start = prj.project(-0.5f, pos, 0, null, scale);
			end = prj.project(0.5f, pos, 0, null, scale);
			g2d.drawLine((int)start[0] + cx, (int)start[1] + cy, (int)end[0] + cx, (int)end[1] + cy);
			g2d.drawString(String.valueOf(pos), (int)end[0] + cx, (int)end[1] + cy);
			pos += seg;
		}
		pos = -seg;
		while(pos > -limit){
			start = prj.project(-0.5f, pos, 0, null, scale);
			end = prj.project(0.5f, pos, 0, null, scale);
			g2d.drawLine((int)start[0] + cx, (int)start[1] + cy, (int)end[0] + cx, (int)end[1] + cy);
			g2d.drawString(String.valueOf(pos), (int)end[0] + cx, (int)end[1] + cy);
			pos -= seg;
		}
		// 3. z
		pos = seg * 1;
		while(pos < limit){
			start = prj.project(0.3f, -0.3f, pos, null, scale);
			end = prj.project(-0.3f, 0.3f, pos, null, scale);
			g2d.drawLine((int)start[0] + cx, (int)start[1] + cy, (int)end[0] + cx, (int)end[1] + cy);
			g2d.drawString(String.valueOf(pos), (int)end[0] + cx, (int)end[1] + cy);
			pos += seg;
		}/*
		pos = -seg;
		while(pos > -zlimit){
			start = prj.project(0.3, -0.3, pos, null, scale);
			end = prj.project(-0.3, 0.3, pos, null, scale);
			g2d.drawLine((int)start[0] + cx, (int)start[1] + cy, (int)end[0] + cx, (int)end[1] + cy);
			g2d.drawString(String.valueOf(pos), (int)end[0] + cx, (int)end[1] + cy);
			pos -= seg;
		}*/
	}// end drawGrid(Graphics2D)
}
