package agdr.robodari.plugin.rme.gui;


public interface SketchScreen {
	public float getScale();
	public float getAxisStartX();
	public float getAxisStartY();
	public Projector getProjector();
	public java.awt.Polygon getBound(agdr.robodari.plugin.rme.edit.SkObject obj);
}
