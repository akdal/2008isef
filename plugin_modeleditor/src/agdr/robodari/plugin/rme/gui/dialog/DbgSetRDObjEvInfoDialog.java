package agdr.robodari.plugin.rme.gui.dialog;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import agdr.robodari.gui.*;
import agdr.robodari.plugin.rme.rdpi.*;
import agdr.robodari.plugin.rme.rlztion.gui.RealizationPanel;

public class DbgSetRDObjEvInfoDialog extends JDialog implements ActionListener, ListSelectionListener{
	private final static Dimension CENTERPANEL_SIZE = new Dimension(460, 315);
	
	
	private Object[] paramValues;
	private Class[] paramValueTypes;
	
	private JPanel centerPanel;
		private JTextField positionField;
		private JTextArea methodArea;
		private JList paramsList;
		private DefaultListModel paramsListModel;
		private JButton detailButton;
	private JPanel bottomPanel;
		private JButton okButton;
	
	public DbgSetRDObjEvInfoDialog(Dialog parent){
		super(parent, "RDObj Event Info..", true);
		
		_initComps();
		Dimension scrsize = Toolkit.getDefaultToolkit().getScreenSize();
		super.pack();
		super.setResizable(false);
		super.setLocation((scrsize.width - super.getWidth()) / 2, (scrsize.height - super.getHeight()) / 2);
		finish();
	}// end Constructor(DebuggerSet)
	public DbgSetRDObjEvInfoDialog(Frame parent){
		super(parent, "RDObj Event Info..", true);
		
		_initComps();
		Dimension scrsize = Toolkit.getDefaultToolkit().getScreenSize();
		super.pack();
		super.setResizable(false);
		super.setLocation((scrsize.width - super.getWidth()) / 2, (scrsize.height - super.getHeight()) / 2);
		finish();
	}// end Constructor(JFrame)
	
	private void _initComps(){
		centerPanel = new JPanel();
		centerPanel.setLayout(null);
		centerPanel.setPreferredSize(CENTERPANEL_SIZE);
		
		JLabel posFieldLabel = new JLabel("Pos : ", JLabel.RIGHT);
		posFieldLabel.setBounds(20, 30, 80, 25);
		positionField = new JTextField();
		positionField.setEditable(false);
		positionField.setBounds(110, 30, 300, 25);
		centerPanel.add(posFieldLabel);
		centerPanel.add(positionField);
		
		JLabel mthdLabel = new JLabel("޼Method : ", JLabel.RIGHT);
		mthdLabel.setBounds(20, 65, 80, 25);
		methodArea = new JTextArea();
		methodArea.setLineWrap(true);
		methodArea.setEditable(false);
		JScrollPane mthdscrpane = new JScrollPane(methodArea);
		mthdscrpane.setBounds(110, 65, 300, 80);
		centerPanel.add(mthdLabel);
		centerPanel.add(mthdscrpane);
		
		JLabel prmLabel = new JLabel("params : ", JLabel.RIGHT);
		prmLabel.setBounds(20, 155, 80, 25);
		paramsListModel = new DefaultListModel();
		paramsList = new JList(paramsListModel);
		paramsList.addListSelectionListener(this);
		JScrollPane plscrpane = new JScrollPane(paramsList);
		plscrpane.setBounds(110, 155, 300, 120);
		centerPanel.add(prmLabel);
		centerPanel.add(plscrpane);
		
		detailButton = new JButton("detail..");
		detailButton.addActionListener(this);
		detailButton.setBounds(310, 285, 100, 30);
		centerPanel.add(detailButton);
		
		
		bottomPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		okButton = new JButton("Okay");
		okButton.addActionListener(this);
		bottomPanel.add(okButton);
		
		super.setLayout(new BorderLayout());
		super.add(centerPanel, BorderLayout.CENTER);
		super.add(bottomPanel, BorderLayout.SOUTH);
	}// end _initComps()
	
	
	
	public void finish(){
		super.setVisible(false);
		detailButton.setEnabled(false);
		paramsListModel.clear();
		positionField.setText("");
		methodArea.setText("");
	}// end disableComps()
	public void showMethod(RDObjectEvent event){
		this.paramValues = event.values;
		this.paramValueTypes = event.types;
		
		positionField.setText(event.target.getName());
		
		if(event.type == RDObjectEvent.TYPE_FIELD_ACCESS)
			return;
		
		String methodName = event.target.getRDMethodNames()[event.index];
		RDObject.Type returnType = event.target.getRDMethodTypes()[event.index];
		RDObject.Type[] paramTypes = event.target.getRDMethodParamTypes()[event.index];
		String[] paramNames = event.target.getRDMethodParamNames()[event.index];
		
		StringBuffer buf = new StringBuffer();
		buf.append(returnType).append(" ").append(methodName).append("(");
		if(paramTypes.length != 0){
			buf.append(paramNames[0]).append(" : ").append(paramTypes[0]);
			for(int i = 1; i < paramTypes.length; i++)
				buf.append(", ").append(paramNames[i])
					.append(" : ").append(paramTypes[i]);
		}
		buf.append(")");
		methodArea.setText(buf.toString());
		
		for(int i = 0; i < paramValueTypes.length; i++){
			String tostr = null;
			if(paramValueTypes[i].isPrimitive()){
				tostr = paramValues[i] + " : " + paramValueTypes[i].getCanonicalName();
			}else
				tostr = "[" + paramValueTypes[i].getCanonicalName() + "]";
			paramsListModel.addElement(tostr);
		}
		super.setVisible(true);
	}// end showMethod(String, RDObject.Type, RDObject.Type[], String[], OBject[], Class[])
	
	
	public void actionPerformed(ActionEvent ae){
		Object src = ae.getSource();
		if(src == this.okButton){
			if(!this.isVisible())
				return;
			finish();
		}else if(src == this.detailButton){
			int idx = paramsList.getSelectedIndex();
			if(idx == -1) return;
			
			DbgSetValInfoDialog dsoid = new DbgSetValInfoDialog(this);
			dsoid.showObject(paramValues[idx], paramValueTypes[idx]);
		}
	}// end actionPerformed(ActionEvent_)
	public void valueChanged(ListSelectionEvent lse){
		if(lse.getSource() == this.paramsList){
			if(this.paramsList.getSelectedIndex() == -1)
				this.detailButton.setEnabled(false);
			else
				this.detailButton.setEnabled(true);
		}
	}// end valueChangeD(ListSelectionEvent)
	
	
	
	
	public static void main(String[] argv){
		new DbgSetRDObjEvInfoDialog((JDialog)null).setVisible(true);
	}
}
