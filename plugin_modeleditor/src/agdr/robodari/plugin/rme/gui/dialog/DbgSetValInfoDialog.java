package agdr.robodari.plugin.rme.gui.dialog;

import java.lang.reflect.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;

import agdr.robodari.gui.*;

public class DbgSetValInfoDialog extends JDialog
		implements ListSelectionListener, ActionListener{
	private final static Dimension CPSIZE = new Dimension(300, 250);
	
	private JPanel centerPanel;
		private JTextField typeField;
		private JPanel viewPanel;
			// for Primitive, Object
			private JScrollPane view_poScrPane;
			private JTextArea view_poTextArea;
			// for array, vector, ...
			private JPanel view_aPanel;
				private JScrollPane view_aScrPane;
				private JTable view_aArrayTable;
				private ArrayTableModel aArrayTableModel;
				private JPanel view_ap_bottomPanel;
					private JButton view_aDetailButton;
	private JPanel bottomPanel;
	private JButton okButton;
	
	private Object target;
	
	
	public DbgSetValInfoDialog(Dialog parent){
		super(parent, "Object Info", true);
		_initialize();
		Dimension scrsize = Toolkit.getDefaultToolkit().getScreenSize();
		super.pack();
		super.setResizable(false);
		super.setLocation((scrsize.width - super.getWidth()) / 2, (scrsize.height - super.getHeight()) / 2);
		finish();
	}// end Constructor(JDialog)
	public DbgSetValInfoDialog(Frame parent){
		super(parent, "Object Info", true);
		_initialize();
		Dimension scrsize = Toolkit.getDefaultToolkit().getScreenSize();
		super.pack();
		super.setResizable(false);
		super.setLocation((scrsize.width - super.getWidth()) / 2, (scrsize.height - super.getHeight()) / 2);
		finish();
	}// end Constructor(JFrame)
	
	private void _initialize(){
		centerPanel = new JPanel(null);
		centerPanel.setPreferredSize(CPSIZE);
		
		JLabel typeLabel = new JLabel("type : ", JLabel.RIGHT);
		typeLabel.setBounds(30, 20, 40, 25);
		typeField = new JTextField();
		typeField.setEditable(false);
		typeField.setBounds(70, 20, 200, 25);
		JLabel valueLabel = new JLabel(" : ", JLabel.RIGHT);
		valueLabel.setBounds(30, 55, 40, 25);
		viewPanel = new JPanel(new BorderLayout());
		viewPanel.setBounds(70, 55, 200, 175);
		
		centerPanel.add(typeLabel);
		centerPanel.add(typeField);
		centerPanel.add(valueLabel);
		centerPanel.add(viewPanel);
		
		{
			view_poTextArea = new JTextArea();
			view_poTextArea.setLineWrap(true);
			view_poTextArea.setEditable(false);
			view_poScrPane = new JScrollPane(view_poTextArea);
			
			view_aPanel = new JPanel(new BorderLayout());
			aArrayTableModel = new ArrayTableModel(null);
			view_aArrayTable = new JTable(aArrayTableModel);
			view_aArrayTable.getSelectionModel().addListSelectionListener(this);
			view_aScrPane = new JScrollPane(this.view_aArrayTable);
			
			view_ap_bottomPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
			{
				view_aDetailButton = new JButton("detail..");
				view_aDetailButton.addActionListener(this);
				view_aDetailButton.setEnabled(false);
				view_ap_bottomPanel.add(view_aDetailButton);
			}
			view_aPanel.add(view_aScrPane, BorderLayout.CENTER);
			view_aPanel.add(view_ap_bottomPanel, BorderLayout.SOUTH);
		}
		
		bottomPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		okButton = new JButton("Okay");
		okButton.addActionListener(this);
		bottomPanel.add(okButton);
		
		super.setLayout(new BorderLayout());
		super.add(centerPanel, BorderLayout.CENTER);
		super.add(bottomPanel, BorderLayout.SOUTH);
	}// end _initialize()
	
	
	
	public void actionPerformed(ActionEvent ae){
		Object src = ae.getSource();
		if(src == this.okButton){
			this.finish();
		}else if(src == this.view_aDetailButton){
			int row = view_aArrayTable.getSelectedRow();
			if(row == -1) return;
			
			DbgSetValInfoDialog dialog = new DbgSetValInfoDialog(this);
			dialog.showObject(aArrayTableModel.getValueAt(row, 1),
					target.getClass().getComponentType());
		}
	}// end actionPerformed(ActionEvent)
	public void valueChanged(ListSelectionEvent lse){
		if(lse.getSource() == this.view_aArrayTable.getSelectionModel()){
			if(this.view_aArrayTable.getSelectedColumn() == -1)
				this.view_aDetailButton.setEnabled(false);
			else
				this.view_aDetailButton.setEnabled(true);
		}
	}// end valueChanged(ListSelectionEvent)
	
	
	public void finish(){
		super.setVisible(false);
		if(viewPanel.getComponentCount() != 0)
			viewPanel.remove(0);
	}// end finish()
	public void showObject(Object obj, Class type){
		this.target = obj;
		
		if(type != null && type.isArray()){
			this.aArrayTableModel.setArrayObject(obj);
			viewPanel.add(this.view_aPanel);
		}else{
			if(obj == null){
				this.view_poTextArea.setText("null");
				this.view_poTextArea.setForeground(Color.red);
			}else{
				this.view_poTextArea.setText(obj.toString());
				this.view_poTextArea.setForeground(Color.black);
			}
			viewPanel.add(this.view_poScrPane);
		}
		typeField.setText(type.getCanonicalName());
		super.setVisible(true);
	}// end showObject(Object, Class)
	
	
	
	
	/*
	public void dispose(){
		super.dispose();
		System.exit(0);
	}*/
	public static void main(String[] argv){
		DbgSetValInfoDialog dsvid = new DbgSetValInfoDialog((JFrame)null);
		dsvid.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		String[] array = new String[100];
		array[0] = "1sdf";
		array[1] = "3";
		array[2] = "5";
		array[3] = "2";
		dsvid.showObject(array, array.getClass());
	}// end main(String[])
	
	
	
	
	static class ArrayTableModel extends AbstractTableModel{
		private final static String[] COLUMNS = new String[]{
			"index", "value"
		};
		private Object arrayObject;
		
		public ArrayTableModel(Object arrayobj){
			this.setArrayObject(arrayobj);
		}// end Constructor(Object)
		
		public void setArrayObject(Object arrayobj){
			this.arrayObject = arrayobj;
		}// end setArrayObject(Object)
		
		public boolean isCellEditable(int row, int col)
		{ return col == 1; }
		
		public String getColumnName(int idx){
			return COLUMNS[idx];
		}// end getColumn()
		public int getRowCount(){
			if(arrayObject == null)
				return 0;
			return Array.getLength(arrayObject);
		}// end getrowCount()
		public int getColumnCount()
		{ return 2; }
		
		public void setValueAt(Object value, int row, int col)
		{}
		public Object getValueAt(int row, int col){
			if(col == 0)
				return String.valueOf(row);
			else if(arrayObject == null)
				return "";
			return Array.get(arrayObject, row);
		}// end getValueAt(int, int)
	}// end class ArrayTableModel
}
