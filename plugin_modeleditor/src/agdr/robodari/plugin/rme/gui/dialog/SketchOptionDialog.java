package agdr.robodari.plugin.rme.gui.dialog;

import java.net.*;
import java.util.*;
import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.tree.*;

import agdr.robodari.comp.*;
import agdr.robodari.edit.*;
import agdr.robodari.gui.*;
import agdr.robodari.gui.dialog.OptionDialog;
import agdr.robodari.plugin.rme.appl.*;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.gui.*;
import agdr.robodari.project.*;

public class SketchOptionDialog extends OptionDialog<ProjectFile>
		implements ActionListener, ItemListener{
	private final static String MNU_INFO = "Info";
	private final static String MNU_CTRLERS = "Controllers";
	private final static Object[] SRCTYPE_ITEMS = new Object[]{ "File", "URL", "ProjectFile", "None" };
	
	private SourceEditable oldCtrlSelection;
	private TreeModel treeModel;
	private DefaultTreeCellRenderer treeCellRenderer;
	private RbModelEditorManager editorManager;
	private RobotModel robotModel;
	private Hashtable<SourceEditable, Integer> cachedSrcTypeTable
			= new Hashtable<SourceEditable, Integer>();
	private Hashtable<SourceEditable, Object> cachedSrcTable
			= new Hashtable<SourceEditable, Object>();
	private Hashtable<SourceEditable, String> cachedSrcNameTable
			= new Hashtable<SourceEditable, String>();
	private SourceEditable starterObj;
	
	private JPanel infoPanel;
		private JTextField pathField;
	private JPanel ctrlerPanel;
		private DefaultComboBoxModel ctrlerComboBoxModel;
		private JComboBox ctrlerComboBox;
		private JCheckBox mainCtrlButton;
		private SkObjectViewer ctrlViewer;
		private JComboBox srcTypeComboBox;
		private JTextField classNameField;
		private JPanel srcTypeViewPanel;
		private CardLayout srcTypeViewCard;
			private JPanel stvp_filePanel;
				private JTextField filePosField;
				private JButton fileChButton;
			private JPanel stvp_urlPanel;
				private JTextField urlField;
			private ProjectTree projTree; 
			private JScrollPane prjtrScrPane;
		
	
			
			
			
	public SketchOptionDialog(Frame parent, RbModelEditorManager manager){
		super(parent, "Sketch options");
		this.editorManager = manager;
		_init();
	}// end Constructor
	public SketchOptionDialog(Dialog parent, RbModelEditorManager manager){
		super(parent, "Sketch options"); 
		this.editorManager = manager;
		_init();
	}// end Constructor
	private void _init(){
		_initComps();
	}// end _init()
	private void _initComps(){
		// 1.
		infoPanel = new JPanel(new FlowLayout());
		{
			pathField = new JTextField();
			pathField.setColumns(20);
			pathField.setEditable(false);
			infoPanel.add(new JLabel("file path : "));
			infoPanel.add(pathField);
		}
		
		// 2.
		ctrlerPanel = new JPanel();
		{
			ctrlerPanel.setLayout(new GridBagLayout());
			GridBagConstraints con = new GridBagConstraints();

			this.ctrlViewer = new SkObjectViewer();
			this.ctrlViewer.setPreferredSize(new Dimension(170, 200));
			this.srcTypeComboBox = new JComboBox(SRCTYPE_ITEMS);
			this.srcTypeComboBox.addItemListener(this);
			this.classNameField = new JTextField();
			
			this.ctrlerComboBox = new JComboBox();
			this.ctrlerComboBoxModel = new DefaultComboBoxModel();
			this.ctrlerComboBox.setModel(this.ctrlerComboBoxModel);
			this.ctrlerComboBox.setPreferredSize(new Dimension(370, 25));
			this.mainCtrlButton = new JCheckBox("Main controller");
			this.mainCtrlButton.addActionListener(this);

			this.srcTypeViewPanel = new JPanel();
			{
				this.srcTypeViewCard = new CardLayout();
				this.srcTypeViewPanel.setLayout(this.srcTypeViewCard);
				stvp_filePanel = new JPanel();
				{
					stvp_filePanel.setLayout(new FlowLayout());
					this.filePosField = new JTextField();
					this.filePosField.setColumns(16);
					this.fileChButton = new JButton("..");
					this.fileChButton.setPreferredSize(new Dimension(30, 25));
					this.fileChButton.addActionListener(this);
					JPanel tmppnl = new JPanel(new FlowLayout());
					tmppnl.add(new JLabel("path : "));
					tmppnl.add(this.filePosField);
					tmppnl.add(this.fileChButton);
					stvp_filePanel.add(tmppnl, BorderLayout.NORTH);
				}
				this.srcTypeViewPanel.add(this.stvp_filePanel, SRCTYPE_ITEMS[0]);
				stvp_urlPanel = new JPanel();
				{
					stvp_urlPanel.setLayout(new FlowLayout());
					this.urlField = new JTextField();
					this.urlField.setColumns(18);
					JPanel tmppnl = new JPanel();
					tmppnl.add(new JLabel("url : "));
					tmppnl.add(urlField);
					stvp_urlPanel.add(tmppnl, BorderLayout.NORTH);
				}
				this.srcTypeViewPanel.add(this.stvp_urlPanel, SRCTYPE_ITEMS[1]);
				projTree = new ProjectTree(this.editorManager.getInterface(), null, false);
				prjtrScrPane = new JScrollPane(projTree);
				this.srcTypeViewPanel.add(prjtrScrPane, SRCTYPE_ITEMS[2]);
			}
			
			
			con.fill = GridBagConstraints.BOTH;
			con.insets = new Insets(2, 2, 2, 2);
			
			con.gridx = 0; con.gridy = 0;
			con.gridwidth = 3; con.gridheight = 1;
			con.weightx = 1; con.weighty = 0;
			this.ctrlerPanel.add(this.ctrlerComboBox, con);
			con.gridx = 0; con.gridy = 1;
			con.gridwidth = 1; con.gridheight = 3;
			con.weightx = 1; con.weighty = 1;
			this.ctrlerPanel.add(this.ctrlViewer, con);
			con.gridx = 1; con.gridy = 1;
			con.gridwidth = 1; con.gridheight = 1;
			con.weightx = 0; con.weighty = 0;
			
			this.ctrlerPanel.add(new JLabel("src type : ", JLabel.RIGHT), con);
			con.gridx = 2; con.gridy = 1;
			con.gridwidth = 1; con.gridheight = 1;
			con.weightx = 0.7; con.weighty = 0;
			this.ctrlerPanel.add(this.srcTypeComboBox, con);
			con.gridx = 1; con.gridy = 2;
			con.gridwidth = 1; con.gridheight = 1;
			con.weightx = 0; con.weighty = 0;
			this.ctrlerPanel.add(new JLabel("class name : ", JLabel.RIGHT), con);
			con.gridx = 2; con.gridy = 2;
			con.gridwidth = 1; con.gridheight = 1;
			con.weightx = 0.7; con.weighty = 0;
			this.ctrlerPanel.add(this.classNameField, con);
			con.gridx = 1; con.gridy = 3;
			con.gridwidth = 2; con.gridheight = 1;
			con.weightx = 0.7; con.weighty = 1;
			this.ctrlerPanel.add(this.srcTypeViewPanel, con);
			
			con.gridx = 0; con.gridy = 4;
			con.gridwidth = 3; con.gridheight = 1;
			con.weightx = 1; con.weighty = 0;
			this.ctrlerPanel.add(this.mainCtrlButton, con);
		}
	}// end _initComps()
	
	
	
	public void actionPerformed(ActionEvent ae){
		super.actionPerformed(ae);
		Object src = ae.getSource();
		if(src == this.fileChButton){
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setFileFilter(new javax.swing.filechooser.FileFilter(){
				public boolean accept(File file)
				{ return file.isDirectory() || file.getName().endsWith(".class"); }
				public String getDescription()
				{ return "directory or class file"; }
			});
			fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
			fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			int res = fileChooser.showOpenDialog(this);
			if(res == JFileChooser.APPROVE_OPTION){
				File file = fileChooser.getSelectedFile();
				this.filePosField.setText(file.getAbsolutePath());
			}
		}
	}// end actionPerformed(ActionEvent)
	public void itemStateChanged(ItemEvent ie){
		if(!this.ctrlerComboBox.isEnabled())
			return;
		
		//System.out.println("SkOptDig : itemStateChanged : ie.getsrc : " + ((JComboBox)ie.getSource()).getItemCount());
		if(ie.getSource() == this.srcTypeComboBox){
			//System.out.println("SketchOptionDialog : itemSTateChanged : srcTypEcomboBox");
			SourceEditable se = (SourceEditable)this.ctrlerComboBox.getSelectedItem();

			int selidx = this.srcTypeComboBox.getSelectedIndex();
			if(selidx == 0)
				this.showEditorPane(se, RobotModel.STYPE_FILE);
			else if(selidx == 1)
				this.showEditorPane(se, RobotModel.STYPE_URL);
			else if(selidx == 2)
				this.showEditorPane(se, RobotModel.STYPE_PRJFILE);
			else if(selidx == 3)
				this.showEditorPane(se, RobotModel.STYPE_NULL);
		}else if(ie.getSource() == this.ctrlerComboBox){
			if(this.oldCtrlSelection != null && this.oldCtrlSelection
					!= this.ctrlerComboBox.getSelectedItem()){
				this.cacheCurrent(this.oldCtrlSelection);
			}
			readyEditorPane((SourceEditable)this.ctrlerComboBox.getSelectedItem());
		}
	}// end itemStateChanged(ItemEvent)
	
	private void readyEditorPane(SourceEditable ctrler){
		this.ctrlViewer.setObject(this.robotModel.getSketchModel().getObject(ctrler));
		
		int srctype = robotModel.getSourceType(ctrler);
		if(this.cachedSrcTypeTable.containsKey(ctrler)){
			srctype = (Integer)this.cachedSrcTypeTable.get(ctrler);
		}
		
		//System.out.println("readyEditorPane : ctrler : " + ctrler + " , srctype : " + srctype);
		if(srctype == RobotModel.STYPE_FILE){
			this.srcTypeComboBox.setSelectedIndex(0);
		}else if(srctype == RobotModel.STYPE_NULL){
			this.srcTypeComboBox.setSelectedIndex(3);
		}else if(srctype == RobotModel.STYPE_PRJFILE){
			this.srcTypeComboBox.setSelectedIndex(2);
		}else if(srctype == RobotModel.STYPE_URL){
			this.srcTypeComboBox.setSelectedIndex(1);
		}
		String className = this.robotModel.getSourceName(ctrler);
		if(this.cachedSrcNameTable.containsKey(ctrler))
			className = this.cachedSrcNameTable.get(ctrler);
		this.classNameField.setText(className);
		if(this.starterObj == ctrler)
			this.mainCtrlButton.setSelected(true);
		else
			this.mainCtrlButton.setSelected(false);
		
		this.oldCtrlSelection = (SourceEditable)this.ctrlerComboBox.getSelectedItem();
	}// end showEditorPAne(SourceEditalbe)
	private void showEditorPane(SourceEditable se, int srcType){
		if(srcType == RobotModel.STYPE_NULL){
			this.classNameField.setText("");
			this.classNameField.setEditable(false);
			this.filePosField.setText("");
			this.filePosField.setEditable(false);
			this.fileChButton.setEnabled(false);
			this.pathField.setText("");
			this.pathField.setEditable(false);
			this.urlField.setText("");
			this.urlField.setEditable(false);
			this.projTree.setEnabled(false);
			this.mainCtrlButton.setSelected(false);
			this.mainCtrlButton.setEnabled(false);
			return;
		}else{
			//System.out.println("readyEditorPane : " + this.srcTypeViewPanel.getComponents().length);
			this.srcTypeViewCard.show(this.srcTypeViewPanel,
				this.srcTypeComboBox.getSelectedItem().toString());
		}
		this.filePosField.setEditable(true);
		this.urlField.setEditable(true);
		this.classNameField.setEditable(true);
		this.fileChButton.setEnabled(true);
		this.mainCtrlButton.setEnabled(true);
		this.projTree.setEnabled(true);
		
		if(srcType == RobotModel.STYPE_FILE){
			// 
			File file = null;
			if(this.cachedSrcTable.get(se) != null
					&& this.cachedSrcTypeTable.get(se) == RobotModel.STYPE_FILE){
				file = (File)this.cachedSrcTable.get(se);
			}else{
				file = this.robotModel.getSourceFile(se);
				//System.out.println("SkOptDialog : itemStateChanged : file : " + file);
			}
			if(file == null)
				this.filePosField.setText("");
			else
				this.filePosField.setText(file.getAbsolutePath());
		}else if(srcType == RobotModel.STYPE_URL){
			// URL
			URL url = null;
			if(this.cachedSrcTable.get(se) != null
					&& this.cachedSrcTypeTable.get(se) == RobotModel.STYPE_URL)
				url = ((URL)this.cachedSrcTable.get(se));
			else
				url = this.robotModel.getSourceURL(se);
			
			if(url == null)
				this.urlField.setText("");
			else
				this.urlField.setText(url.toString());
		}else if(srcType == RobotModel.STYPE_PRJFILE){
			// Project File
			ProjectFile pfile = null;
			if(this.cachedSrcTable.get(se) != null
					&& this.cachedSrcTypeTable.get(se) == RobotModel.STYPE_PRJFILE){
				pfile = (ProjectFile)this.cachedSrcTable.get(se);
			}else{
				Project prj = this.projTree.getProject();
				ProjectItem focus = prj;
				String[] szpath = this.robotModel.getSourcePrjFilePath(se);
				//System.out.println("SketchOptionDlg : showEditorPane : prjFile : ");
				
				if(szpath == null)
					pfile= null;
				else{
					for(int i = 0; i < szpath.length; i++){
						//System.out.print(" -> " + szpath[i]);
						ProjectItem next = null;
						for(int j = 0; j < focus.getChildCount(); j++){
							if(focus.getChildAt(j).toString().equals(szpath[i])){
								next = (ProjectItem)focus.getChildAt(j);
								j = 2147483;
								continue;
							}
						}
						focus = next;
						if(next == null){
							i = 999999;
							continue;
						}
					}
					pfile = (ProjectFile)focus;
				}
			}
			if(pfile == null)
				this.projTree.getTree().setSelectionPath(null);
			else
				this.projTree.getTree().setSelectionPath(pfile.getTreePath());
		}
	}// end showEditorPane(SourceEditable, int)
	public void cacheCurrent(SourceEditable se){
		int idx = this.srcTypeComboBox.getSelectedIndex();
		//System.out.println("SketchOptionDig : itemStChnged : cacheCurrnt : idx : "
		//		+ idx + " , oldctrl : " + oldCtrlSelection);
		
		if(idx == 0){
			//System.out.println("filePosField.getTExt() : " + filePosField.getText());
			this.cachedSrcTypeTable.put(this.oldCtrlSelection, RobotModel.STYPE_FILE);
			this.cachedSrcTable.put(this.oldCtrlSelection, new File(this.filePosField.getText()));
		}else if(idx == 1){
			this.cachedSrcTypeTable.put(this.oldCtrlSelection, RobotModel.STYPE_URL);
			String szurl = this.urlField.getText();
			Object oldval = this.cachedSrcTable.get(this.oldCtrlSelection);
			
			try{
				this.cachedSrcTable.put(this.oldCtrlSelection, new URL(this.urlField.getText()));
			}catch(Exception excp){
				ExceptionHandler.wrongURL(szurl);
				this.cachedSrcTable.put(this.oldCtrlSelection, oldval);
			}
		}else if(idx == 2){
			this.cachedSrcTypeTable.put(this.oldCtrlSelection, RobotModel.STYPE_PRJFILE);
			TreePath tp = this.projTree.getTree().getSelectionPath();
			
			if(tp == null){
				this.cachedSrcTypeTable.put(this.oldCtrlSelection, RobotModel.STYPE_NULL);
				this.cachedSrcTable.remove(this.oldCtrlSelection);
			}else{
				if(!(tp.getLastPathComponent() instanceof ProjectFile))
					agdr.robodari.appl.ExceptionHandler.onlyFileAllowed();
				else
					this.cachedSrcTable.put(this.oldCtrlSelection, tp.getLastPathComponent());
			}
		}else if(idx == 3){
			this.cachedSrcTypeTable.put(this.oldCtrlSelection, RobotModel.STYPE_NULL);
			this.cachedSrcTable.remove(this.oldCtrlSelection);
		}
		String clsname = this.classNameField.getText();
		this.cachedSrcNameTable.put(this.oldCtrlSelection, clsname);
		if(this.mainCtrlButton.isSelected())
			this.starterObj = se;
	}// end cacheCurrent(SourceEditalbe)
	
	
	
	
	public void resetInputs(ProjectFile file){
		this.cachedSrcTable.clear();
		this.cachedSrcTypeTable.clear();
		this.starterObj = null;
		this.ctrlerComboBox.removeItemListener(this);
		
		this.classNameField.setText("");
		this.classNameField.setEditable(false);
		this.srcTypeComboBox.setEnabled(false);
		this.filePosField.setText("");
		this.filePosField.setEditable(false);
		this.fileChButton.setEnabled(false);
		this.pathField.setText("");
		this.pathField.setEditable(false);
		this.urlField.setText("");
		this.urlField.setEditable(false);
		this.projTree.setEnabled(false);
		this.mainCtrlButton.setSelected(false);
		this.mainCtrlButton.setEnabled(false);
		this.oldCtrlSelection = null;
		
		if(file == null){
			this.ctrlerComboBoxModel.removeAllElements();
		}else{
			this.pathField.setText(file.getTotalPath());
			this.projTree.loadProject(this.editorManager.getInterface().getProject());

			this.ctrlerComboBoxModel.removeAllElements();
			this.robotModel = this.editorManager.getRobotModel(file);
			RobotComponent[] rcomps = robotModel.getSketchModel().getRobotComponents();
			for(int i = 0; i < rcomps.length; i++)
				if(rcomps[i] instanceof SourceEditable)
					this.ctrlerComboBoxModel.addElement((SourceEditable)rcomps[i]);
			this.starterObj = this.robotModel.getStarter();
			
			if(this.ctrlerComboBoxModel.getSize() != 0){
				this.ctrlerComboBox.addItemListener(this);
				this.ctrlerComboBox.setSelectedIndex(0);
				this.srcTypeComboBox.setEnabled(true);
				SourceEditable se = (SourceEditable)this.ctrlerComboBox.getSelectedItem();
				this.readyEditorPane(se);
				this.showEditorPane(se, robotModel
						.getSourceType(se));
			}
		}
	}// end resetInputs(RobotModel)
	public boolean save(){
		this.cacheCurrent((SourceEditable)this.ctrlerComboBox.getSelectedItem());
		Enumeration<SourceEditable> key = cachedSrcTypeTable.keys();
		while(key.hasMoreElements()){
			SourceEditable next = key.nextElement();
			int type = cachedSrcTypeTable.get(next);
			//System.out.println("SkObjDig : type : " + type + " , cachedSrcTable.get : " + cachedSrcTable.get(next));
			if(type == RobotModel.STYPE_FILE)
				this.robotModel.setSourceFile(next, (File)cachedSrcTable.get(next));
			else if(type == RobotModel.STYPE_NULL)
				this.robotModel.setSourceFile(next, null);
			else if(type == RobotModel.STYPE_PRJFILE)
				this.robotModel.setSourcePrjFile(next, (ProjectFile)cachedSrcTable.get(next));
			else if(type == RobotModel.STYPE_URL)
				this.robotModel.setSourceURL(next, (java.net.URL)cachedSrcTable.get(next));
		}
		key = cachedSrcNameTable.keys();
		while(key.hasMoreElements()){
			SourceEditable next = key.nextElement();
			this.robotModel.setSourceName(next, cachedSrcNameTable.get(next));
		}
		this.robotModel.setStarter(this.starterObj);
		
		return true;
	}// end save()
	public JComponent getView(TreePath path){
		if(path.getLastPathComponent().toString() == MNU_INFO)
			return infoPanel;
		else if(path.getLastPathComponent().toString() == MNU_CTRLERS)
			return ctrlerPanel; 
		return null;
	}// end getView(TreePath)
	public TreeCellRenderer getTreeCellRenderer(){
		if(treeCellRenderer == null){
			this.treeCellRenderer = new DefaultTreeCellRenderer();
			this.treeCellRenderer.setLeafIcon(new Icon(){
				public int getIconWidth()
				{ return 0; }
				public int getIconHeight()
				{ return 0; }
				public void paintIcon(Component c, Graphics g, int x, int y){}
			});
		}
		return treeCellRenderer;
	}
	public TreeModel getTreeModel(){
		if(this.treeModel == null){
			DefaultMutableTreeNode root = new DefaultMutableTreeNode("sketchOptions");
			DefaultMutableTreeNode infonode = new DefaultMutableTreeNode(MNU_INFO);
			DefaultMutableTreeNode ctrlersnode = new DefaultMutableTreeNode(MNU_CTRLERS);
			root.add(infonode);
			root.add(ctrlersnode);
			treeModel = new DefaultTreeModel(root);
			
			treeCellRenderer = new DefaultTreeCellRenderer();
			this.treeCellRenderer.setLeafIcon(new Icon(){
				public int getIconWidth()
				{ return 0; }
				public int getIconHeight()
				{ return 0; }
				public void paintIcon(Component c, Graphics g, int x, int y){}
			});
		}
		return treeModel;
	}// end getTreeModel()
}
