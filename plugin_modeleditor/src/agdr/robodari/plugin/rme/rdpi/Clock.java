package agdr.robodari.plugin.rme.rdpi;

public interface Clock {
	public long getMilliSecond();
	public double getTimeRate();
}
