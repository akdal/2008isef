package agdr.robodari.plugin.rme.rdpi;

import agdr.robodari.plugin.rme.rlztion.*;

public interface Dynamic {
	public void initRlztion(RealizationModel rmodel) throws RealizationStartedException;
	public void disposeRlztion() throws RealizationNotReadyException;
	public void resetRlztion() throws RealizationNotReadyException;
}
