package agdr.robodari.plugin.rme.rdpi;

import agdr.robodari.comp.*;

public interface RDConnManager {
	public boolean hasSender(RDObject obj);
	public boolean hasReceiver(RDObject obj);
	
	public Sender[] getSenders(RDObject obj);
	public Receiver[] getReceivers(RDObject obj);
	
	public RDObject getConn(Object obj);
}
