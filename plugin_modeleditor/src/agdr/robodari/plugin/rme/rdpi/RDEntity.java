package agdr.robodari.plugin.rme.rdpi;

public interface RDEntity {
	public String getEntityName();
	
	public RDProcessor getProcessor(String prc);
	public String[] getCtrlerNames();
	public boolean isStarter(String ctrlName);
	
	public void startProcessors();
	public void pauseProcessors();
	public void resumeProcessors();
	public void resetProcessors();
	public void disposeProcessors();
	
	public boolean isRunning();
}
