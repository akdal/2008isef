package agdr.robodari.plugin.rme.rdpi;

public class RDFieldNotExistException extends RDPIException{
	private RDObject object;
	private String fieldName;
	private String totalmsg;
	
	public RDFieldNotExistException(String message, RDObject obj, String fieldName){
		super(message);
		this.object = obj;
		this.fieldName = fieldName;
		this.totalmsg = message + " : " + object + "." + fieldName;
	}// end Constructor(String, String)
	public RDFieldNotExistException(String message, RDObject obj, String fieldName,
			Throwable cause){
		super(message, cause);
		this.object = obj;
		this.fieldName = fieldName;
		this.totalmsg = message + " : " + object + "." + fieldName;
	}// end Constructor(String, Throwable)
	
	public RDObject getobject()
	{ return object; }
	public String getFieldName()
	{ return fieldName; } 
	
	public String toString()
	{ return "RDFieldNotExistException : " + totalmsg; }
}
