package agdr.robodari.plugin.rme.rdpi;

public class RDMethodNotExistException extends RDPIException{
	private RDObject object;
	private String methodName;
	private Object[] args;
	private String totalmsg;
	
	public RDMethodNotExistException(String message, RDObject object,
			String methodName, Object[] args){
		super(message);
		
		this.object = object;
		this.methodName = methodName;
		this.args = args;
		_resettotalmsg();
	}// end Constructor(String, STring, STring, Object[])
	public RDMethodNotExistException(String message, RDObject object,
			String methodName, Object[] args, Throwable cause){
		super(message, cause);
		
		this.object = object;
		this.methodName = methodName;
		this.args = args;
		_resettotalmsg();
	}// end Constructor(String, String, String, Object[], Throwable)
	
	void _resettotalmsg(){
		StringBuffer buf = new StringBuffer();
		buf.append("RDMethodNotExistException : ").append(super.getMessage()).append(" : ").append(object)
				.append(".").append(methodName);
		buf.append("(");
		if(args != null && args.length != 0){
			buf.append(args[0].getClass().getCanonicalName());
			for(int i = 1; i < args.length; i++){
				buf.append(", ").append(args[i].toString());
			}
		}
		buf.append(")");
		this.totalmsg = buf.toString();
	}// end _resettotalmsg()
	
	
	public RDObject getObjectName()
	{ return object; }
	public String getMethodName()
	{ return methodName; }
	public Object[] getArguments()
	{ return args; }
	
	public String toString(){
		return totalmsg;
	}// end toString()
}
