package agdr.robodari.plugin.rme.rdpi;


import agdr.robodari.comp.*;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.rlztion.*;


public interface RDObject extends RobotComponent, Dynamic{
	public String[] getRDFieldNames();
	public Type[] getRDFieldTypes();
	public String[] getRDFieldDescs();
	public boolean[] getRDConstants();
	
	public String[] getRDMethodNames();
	public Type[] getRDMethodTypes();
	public String[][] getRDMethodParamNames();
	public Type[][] getRDMethodParamTypes();
	public String[] getRDMethodDescs();
	
	public String getName();
	
	public Object getRDFieldValue(RealizationModel model,
			String fname)
			throws RealizationNotReadyException, RDFieldNotExistException;
	public void setRDFieldValue(RealizationModel model,
			String fname, Object val)
			throws RealizationNotReadyException, RDFieldNotExistException;
	
	/**
	 * @param model
	 * @param fname
	 * @param params
	 * @return
	 */
	public Object callRDMethod(RealizationModel model,
			String fname, Object[] params)
			throws RealizationNotReadyException, RDMethodNotExistException;
	
	public java.awt.Component getPropertyViewer(RealizationModel model);
	
	
	
	
	public static class Type implements java.io.Serializable{
		public final static int VOID = 0;
		public final static int VARARG = 1;
		public final static int NORMAL = 2;
		private final static String _voidstr = "void";
		private final static String _varargstr = "...";
		
		public final Class clazz;
		public final int type;
		private StringBuffer str;
		
		public Type(Class clazz, int type){
			this.clazz = clazz;
			this.type = type;
			
			str = new StringBuffer();
			if(type == VOID)
				str.append(_voidstr);
			else if(type == VARARG)
				str.append(clazz.getCanonicalName()).append(_varargstr);
			else
				str.append(clazz.getCanonicalName());
		}// end Constructor(Class, int)
		
		public String toString(){
			return str.toString();
		}// end toSTring()
	}// end class Type
}
