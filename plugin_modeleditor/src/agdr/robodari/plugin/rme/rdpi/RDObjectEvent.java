package agdr.robodari.plugin.rme.rdpi;

import agdr.robodari.comp.*;

public class RDObjectEvent {
	public final static int TYPE_FIELD_ACCESS = 0;
	public final static int TYPE_METHOD = 1;
	public final static int TYPE_FIELD_WRITTEN = 3;
	
	public RDEntity source;
	public RDObject target;
	public int type;
	public int index;
	public Object[] values;
	public Class[] types;
	
	public RDObjectEvent(RDEntity source, RDObject target, int type, int index, Object[] values, Class[] types){
		this.source = source;
		this.target = target;
		this.type = type;
		this.index = index;
		this.values = values;
		this.types = types;
	}// end Constructor(int, int, SketchModel, Cmodel)
}
