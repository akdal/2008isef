package agdr.robodari.plugin.rme.rdpi;

public interface RDObjectListener {
	public void fieldAccessed(RDObjectEvent rdoe);
	public void fieldWritten(RDObjectEvent rdoe);
	public void methodCalled(RDObjectEvent rdoe);
}
