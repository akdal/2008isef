package agdr.robodari.plugin.rme.rdpi;

import java.util.*;

import agdr.robodari.plugin.rme.rlztion.*;

public interface RDObjectManager {
	public boolean exists(RDEntity source, String componentName);
	public Object  runMethod(RDEntity source, RDObject obj, String methodName, Object[] args)
			throws  RealizationNotReadyException, RDMethodNotExistException;
	public Object  getFieldValue(RDEntity source, RDObject obj, String fieldName)
			throws  RealizationNotReadyException, RDFieldNotExistException;
	public void    setFieldValue(RDEntity source, RDObject obj, String fieldName,
			Object value)
			throws RealizationNotReadyException, RDFieldNotExistException;
	
	public RDObject getRDObject(RDEntity source, String name);
	public String[] getRDObjectNames(RDEntity source);
}
