package agdr.robodari.plugin.rme.rdpi;

public class RDObjectNotExistException extends RDPIException{
	private String objectName;
	private String totalmsg;
	
	public RDObjectNotExistException(String message, String objName){
		super(message);
		this.objectName = objName;
		this.totalmsg = message + " : " + objectName;
	}// end Constructor(String, String)
	public RDObjectNotExistException(String message, String objName,
			Throwable cause){
		super(message, cause);
		this.objectName = objName;
		this.totalmsg = message + " : " + objectName;
	}// end Constructor(String, Throwable)
	
	public String getObjectName()
	{ return objectName; }
	
	public String toString()
	{ return totalmsg; }
}
