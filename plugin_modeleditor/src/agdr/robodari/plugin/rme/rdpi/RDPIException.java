package agdr.robodari.plugin.rme.rdpi;

public class RDPIException extends Exception{
	public RDPIException(String msg)
	{ super(msg); }
	public RDPIException(String msg, Throwable cause)
	{ super(msg, cause); }
}
