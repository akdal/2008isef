package agdr.robodari.plugin.rme.rdpi;

import java.lang.annotation.*;

@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = ElementType.FIELD)
public @interface RDPIVisible {

}
