package agdr.robodari.plugin.rme.rdpi;

public class RDPUtility {
	public static int getRDMethodIndex(RDObject object, String methodName, Object[] args){
		if(object == null)
			return -1;
		
		String[] methodNames = object.getRDMethodNames();
		RDObject.Type[][] methodParams = object.getRDMethodParamTypes();
		int idx = -1;
		for(int i = 0; i < methodNames.length; i++){
			if(!methodNames[i].equals(methodName))
				continue;
			
			if(args.length == 0 && methodParams[i].length == 0){
				idx = i;
				break;
			}else if(methodParams[i].length == 0 && args.length != 0)
				continue;
			else if((methodParams[i][methodParams[i].length - 1].type
						!= RDObject.Type.VARARG)
					&& methodParams[i].length != args.length)
				continue;
			
			if(methodParams[i][methodParams[i].length - 1].type != RDObject.Type.VARARG){
				boolean flag2 = true;
				for(int j = 0; j < args.length; j++){
					if(args[j] == null) continue;
					if(!methodParams[i][j].clazz.isAssignableFrom(args[j].getClass())){
						flag2 = false;
						j = 214748364;
						continue;
					}
				}
				if(flag2){
					idx = i;
					break;
				}
			}else{
				boolean flag2 = true;
				for(int j = 0; j < methodParams[i].length - 1; j++){
					if(args[j] == null) continue;
					if(!methodParams[i][j].clazz.isAssignableFrom(args[j].getClass())){
						flag2 = false;
						j = 214748364;
						continue;
					}
				}
				for(int j = methodParams[i].length - 1; j < args.length; j++){
					if(args[j] == null) continue;
					if(!methodParams[i][methodParams[i].length - 1].clazz
							.isAssignableFrom(args[j].getClass())){
						flag2 = false;
						j = 214748364;
						continue;
					}
				}
				if(flag2){
					idx = i;
					break;
				}
			}
		}
		return idx;
	}// end getRDMethodIndex(String, Object[])
	public static int getRDFieldIndex(RDObject object, String fieldName){
		String[] names = object.getRDFieldNames();
		int idx = -1;
		for(int i = 0;  i < names.length; i++){
			if(names[i].equals(fieldName)){
				idx = i;
				break;
			}
		}
		return idx;
	}// end getRDFieldIndex
}
