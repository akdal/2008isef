package agdr.robodari.plugin.rme.rdpi;

import java.io.*;
import agdr.robodari.plugin.rme.rlztion.*;

public interface RDProcessor {
	public void start(RDEntity entity, RDObjectManager manager,
			RDConnManager cmng, String controllerName,
			Object[] param, Clock clock, PrintStream rdout, PrintStream rderr)
		throws RealizationNotReadyException, RDPIException;
	public void dispose(RDEntity entity, RDObjectManager manager,
			RDConnManager cmng, String controllerName);
	public void pause(RDEntity ent, RDObjectManager mng, RDConnManager cmng, String ctrlerName);
	public void resume(RDEntity ent, RDObjectManager omng, RDConnManager cmng, String ctrlerName);
	public boolean isRunning(RDEntity entity, RDObjectManager manager,
			RDConnManager cmng, String controllerName);
}
