package agdr.robodari.plugin.rme.rlztion;

import agdr.robodari.comp.model.*;

public class AdjustmentList extends SimpleLinkedList<AdjustmentList.Item>{
	public void setNext(Item a, Item b)
	{ a.next = b; }
	public void init(Item a)
	{}
	public Item getNext(Item cpi)
	{ return cpi.next; }
	protected Item newItem()
	{ return new Item(); }
	
	public static class Item{
		private Item next;
		
		public double apx, apy, apz;
		public double bpx, bpy, bpz;
		/*
		public float relapx, relapy, relapz;
		public float relbpx, relbpy, relbpz;
		*/
		public double nx, ny, nz;
		public int aid, bid;
		
		public double d;
		
		
		public void clear(){
			apx = apy = apz = 0;
			bpx = bpy = bpz = 0;
			d = 0;
			
			//relapx = relapy = relapz = 0;
			//relbpx = relbpy = relbpz = 0;
			nx = ny = nz = 0;
			aid = bid = 0;
		}// end clear()
	}// end class Item
}
