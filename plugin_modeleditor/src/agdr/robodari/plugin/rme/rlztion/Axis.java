package agdr.robodari.plugin.rme.rlztion;

import javax.vecmath.*;
import static java.lang.Math.*;

public class Axis {
	public final static double DEFAULT_EPSILON = 0.0000001;
	
	protected double x;
	protected double y;
	protected double z;
	protected double posX;
	protected double posY;
	protected double posZ;
	
	public Axis(double x, double y, double z, double posX, double posY, double posZ){
		this.x = x;
		this.y = y;
		this.z = z;
		this.posX = posX;
		this.posY = posY;
		this.posZ = posZ;
		
		double len = Math.sqrt(x * x + y * y + z * z);
		this.x = x / len;
		this.y = y / len;
		this.z = z / len;
	}// end Constructor(double, double, double, double, double, double)
	
	
	public boolean equals(Object obj){
		if(obj == null) return false;
		else if(!(obj instanceof Axis))
			return false;
		
		Axis axis = (Axis)obj;
		if(axis.x != x || axis.y != y || axis.z != z)
			return false;
		double dposx = axis.posX - this.posX;
		double dposy = axis.posY - this.posY;
		double dposz = axis.posZ - this.posZ;
		
		double crossx = dposy * z - dposz * y;
		double crossy = dposx * z - dposz * x;
		double crossz = dposx * y - dposy * x;
		if(abs(crossx) <= DEFAULT_EPSILON && abs(crossy) <= DEFAULT_EPSILON
				&& abs(crossz) <= DEFAULT_EPSILON)
			return true;
		return false;
	}// end equals(Object)
	public boolean equalsEpsilon(Object obj, double epsilon){
		if(obj == null) return false;
		else if(!(obj instanceof Axis))
			return false;
		
		Axis axis = (Axis)obj;
		double dx = axis.x - x;
		double dy = axis.y - y;
		double dz = axis.z - z;
		if(abs(dx) > epsilon || abs(dy) > epsilon || abs(dz) > epsilon)
			return false;

		double dposx = axis.posX - this.posX;
		double dposy = axis.posY - this.posY;
		double dposz = axis.posZ - this.posZ;
		
		// ?ؼ 0̸ 鳪ϼ
		double crossx = dposy * z - dposz * y;
		double crossy = dposx * z - dposz * x;
		double crossz = dposx * y - dposy * x;
		if(abs(crossx) <= epsilon && abs(crossy) <= epsilon
				&& abs(crossz) <= epsilon)
			return true;
		return false;
	}// end equalsEpsilon(Object, double)



	public double getPosX()
	{ return posX; }
	public double getPosY()
	{ return posY; }
	public double getPosZ()
	{ return posZ; }

	public double getX()
	{ return x; }
	public double getY()
	{ return y; }
	public double getZ()
	{ return z; }
	
	public Vector3d axisVector()
	{ return new Vector3d(x, y, z); }
	public Vector3d positionVector()
	{ return new Vector3d(posX, posY, posZ); }
}
