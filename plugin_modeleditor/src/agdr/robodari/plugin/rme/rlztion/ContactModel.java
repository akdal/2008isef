package agdr.robodari.plugin.rme.rlztion;

import java.util.*;

import agdr.robodari.appl.*;
import agdr.robodari.comp.model.*;

public class ContactModel extends SimpleLinkedList<ContactModel.Contact>{
	public void setNext(Contact a, Contact b)
	{ a.next = b; }
	public void init(Contact a)
	{}
	public Contact getNext(Contact cpi)
	{ return cpi.next; }
	public Contact newItem()
	{ return new Contact(); }
	
	
	public static class Contact{
		public final static int TYPE_COLLISION = 0;
		public final static int TYPE_NONCOLLISION = 1;
		public final static int TYPE_BOTH = 2;
		public int type;
		
		private Contact next;
		
		// 아래 값은 CFG에서 다룸
		public double coefOfRestt = 1.0;
		public int aid, bid;
		public double apx, apy, apz, bpx, bpy, bpz;
		public boolean onEdge = false;
		// model b에서 나오는 방향!
		public double nx, ny, nz;
		public double edgeax, edgeay, edgeaz;
		public double edgebx, edgeby, edgebz;
		
		// b에서 보았을 때 a의 상대속도.
		public double currDDot;
		public double currRelVelX, currRelVelY, currRelVelZ;
		// b에서 보았을 때 a의 상대가속도
		public double currDDotDot;
		//public double currRelAcclX, currRelAcclY, currRelAcclZ;
		
		// result : will be filled in 
		public double result_force, result_impulse;
		
		
		public String toString(){
			return "[Contact aid : " + aid + ", bid : " + bid + "]";
		}// end toString()
		public void set(Contact c){
			coefOfRestt = c.coefOfRestt;
			aid = c.aid;
			bid = c.bid;
			
			apx = c.apx;
			apy = c.apy;
			apz = c.apz;
			bpx = c.bpx;
			bpy = c.bpy;
			bpz = c.bpz;
			
			onEdge = c.onEdge;
			nx = c.nx;
			ny = c.ny;
			nz = c.nz;
			
			edgeax = c.edgeax;
			edgeay = c.edgeay;
			edgeaz = c.edgeaz;
			edgebx = c.edgebx;
			edgeby = c.edgeby;
			edgebz = c.edgebz;
			
			currDDot = c.currDDot;
			currRelVelX = c.currRelVelX;
			currRelVelY = c.currRelVelY;
			currRelVelZ = c.currRelVelZ;
			
			currDDotDot = c.currDDotDot;
			//currRelAcclX = c.currRelAcclX;
			//currRelAcclY = c.currRelAcclY;
			//currRelAcclZ = c.currRelAcclZ;
			
			result_force = c.result_force;
			result_impulse = c.result_impulse;
		}// end set
	}// end class Contact
}
