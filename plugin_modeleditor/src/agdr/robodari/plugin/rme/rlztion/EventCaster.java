package agdr.robodari.plugin.rme.rlztion;

import java.util.*;

public class EventCaster implements Runnable {
	private Thread thread;
	private boolean activated;
	private LockableLinkedList<Cast> casts;
	private long sleepInterval;

	public EventCaster() {
		this.sleepInterval = 40;
		this.casts = new LockableLinkedList<Cast>();
	}// end Constructor(wrold)

	public void setSleepInterval(long si) {
		this.sleepInterval = si;
	}

	public void start() {
		if(this.thread != null)
			while(this.thread.getState() != Thread.State.TERMINATED);
		
		thread = new Thread(this);
		activated = true;
		casts.clear();
		thread.start();
	}// end start()
	public boolean isStarted()
	{ return this.activated; }
	public void stop(){
		activated = false;
	}// end stop()

	public void fire(Cast cast) {
		this.casts.addLast(cast);
	}// end fire(Cast)

	public void run(){
		try{
			while(activated){
				try{
					if(casts.size() != 0){
						casts.lock();
						while(casts.size() != 0)
							casts.poll().fireEvent();
						casts.unlock();
					}
				}catch(Exception exc){}
				thread.sleep(sleepInterval);
			}
		} catch (Exception exp) {
			agdr.robodari.appl.BugManager.log(exp, true);
		}
	}// end run()

	public static interface Cast {
		public void fireEvent();
	}// end class Cast

	class LockableLinkedList<T> extends LinkedList<T> {
		private boolean lock = false;
		private ArrayList<T> waitingobjs;

		public LockableLinkedList() {
			waitingobjs = new ArrayList<T>();
		}

		public void lock() {
			lock = true;
		}

		public void unlock() {
			lock = false;
			super.addAll(waitingobjs);
		}// end unlock()

		public void clear() {
			super.clear();
			waitingobjs.clear();
		}// end clear()

		public boolean offer(T t) {
			if (lock) {
				waitingobjs.add(t);
				return true;
			}
			return super.offer(t);
		}// end offer(T)
	}// end class LockableLinkedList<T>
}
