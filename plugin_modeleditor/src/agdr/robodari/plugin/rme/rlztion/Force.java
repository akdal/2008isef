package agdr.robodari.plugin.rme.rlztion;

public class Force {
	public int targetStructure;
	// 𷡴 ¾ ϴ ̴.
	// ٱ׷,  ݽùķ̼ǿ   ۿ ʴ´ ٶ ?d8Ƿ
	// ӽ8  ; Ѵ.
	public int standardStructure;
	public double x, y, z;
	
	public double posX, posY, posZ;
	
	public boolean isTorque;
	
	public String toString(){
		StringBuffer buf = new StringBuffer();
		buf.append("[target : ").append(targetStructure)
				.append("\n").append("standard : ").append(standardStructure);
		buf.append("x : ").append(x).append(" , y : ").append(y).append(" , z : ").append(z).append("\n");
		buf.append("posX : ").append(posX).append(" , posY : ").append(posY).append(" , posZ : ").append(posZ).append("\n");
		buf.append("isTorque : ").append(isTorque).append("]");
		return buf.toString();
	}// end toString()
}
