package agdr.robodari.plugin.rme.rlztion;

public interface ForceGenerator {
	public final static int DEPENDSON_ACCEL = 1;
	public final static int DEPENDSON_VELOCITY = 2;
	public final static int DEPENDSON_POSITION = 4;
	public final static int DEPENDSON_OBJECT = 8;
	public final static int DEPENDSON_SITUATION = 16;
	public final static int LCP_NEEDED = 32;
	
	
	public int getGeneratorType();
	public void init(RealizationModel model, RealizationMap rmap, World world);
	public boolean effective(RealizationModel model,
			RealizationMap smap, World world);
	
	public void generateForce();
}
