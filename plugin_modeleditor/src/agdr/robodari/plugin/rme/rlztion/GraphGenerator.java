package agdr.robodari.plugin.rme.rlztion;

import javax.vecmath.*;

import agdr.robodari.comp.*;
import agdr.robodari.comp.model.RobotCompModel;
import agdr.robodari.plugin.rme.edit.*;


public interface GraphGenerator {
	/**
	 * 특정한 RobotComponent에 대해서 그래프를 생성해 줍니다.
	 */
	public final static int SPECFC_RCOMP = 0;
	/**
	 * 특정한 SkObject에 대해서 그래프를 생성해 줍니다.
	 */
	public final static int SPECFC_OBJECT = 1;
	/**
	 * 특정한 SkObject 또는 RobotCompoent에 대해 그래프를 생성해줍니다.
	 */
	public final static int SPECFC_RCOMP_OR_OBJ = 2;
	/**
	 * 특정한 상황에 대해서 그래프를 생성해 줍니다.
	 */
	public final static int SPECFC_SITUATION = 4;

	/**
	 * 주어진 개체에 대한 편집기인지 여부를 반환합니다.
	 * @param mainStructure
	 * @param model
	 * @param skObject
	 * @return
	 */
	public boolean editable(RobotCompModel mainStructure,
			SkObject skObject, RealizationMap map, World world);
	public int getGeneratorType();
	/**
	 */
	public RealizationMap.Key generateGraph(SkObject skObj, Matrix4f trans,
			RealizationMap map, World world);
}
