package agdr.robodari.plugin.rme.rlztion;

public interface MergingObserver {
	public void merged(RealizationMap.Key key, javax.vecmath.Matrix4d transf);
}
