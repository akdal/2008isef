package agdr.robodari.plugin.rme.rlztion;

public interface ObjectAdjustment {
	public void init(RealizationModel model, RealizationMap rmap, World world);
	public boolean effective(RealizationModel model,
			RealizationMap smap, World world);
	
	public void adjust();
	
}
