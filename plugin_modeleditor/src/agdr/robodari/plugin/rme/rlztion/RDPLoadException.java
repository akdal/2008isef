package agdr.robodari.plugin.rme.rlztion;

public class RDPLoadException extends Exception{
	public final static int CASE_STREAMING = 0;
	public final static int CASE_INSTANTIATING = 1;
	
	public String ctrlerName;
	public int errorCase;
	
	public RDPLoadException(String ctrlerName, int errcase, Throwable cause)
	{ super(cause); }
}
