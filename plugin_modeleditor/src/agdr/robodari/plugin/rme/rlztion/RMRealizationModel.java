package agdr.robodari.plugin.rme.rlztion;

import java.util.*;
import java.io.*;

import agdr.robodari.appl.*;
import agdr.robodari.comp.*;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.rdpi.*;
import agdr.robodari.project.*;

public class RMRealizationModel extends RealizationModel implements RDObjectManager, RDConnManager, RDEntity{
	private RDObject[] rdObjects;
	private String[] ctrlerNames;
	private String[] rdobjNames;
	private String starterName;
	private Hashtable<SourceEditable, RDProcessor> processors;
	private Hashtable<String, SourceEditable> controllers;
	
	private Hashtable<RDObject, Receiver[]> receivers;
	private Hashtable<RDObject, Sender[]> senders;
	private ConnectionModel connModel;
	private RDPClassLoader rdpLoader = new RDPClassLoader();
	
	private boolean isRlzStarted = false;

	private EventCaster eventCaster = new EventCaster();
	private ArrayList<RDObjectListener> eventListeners = new
			ArrayList<RDObjectListener>();
	
	
	public void init(RobotModel rmodel, Project project) throws RDPLoadException{
		SkObject[] skObjects = rmodel.getSketchModel().getObjects();
		
		// clearing...
		if(this.controllers == null)
			this.controllers = new Hashtable<String, SourceEditable>();
		else
			this.controllers.clear();
		if(this.processors == null)
			this.processors = new Hashtable<SourceEditable, RDProcessor>();
		else
			this.processors.clear();
		if(this.receivers == null)
			this.receivers = new Hashtable<RDObject, Receiver[]>();
		else
			this.receivers.clear();
		if(this.senders == null)
			this.senders = new Hashtable<RDObject, Sender[]>();
		else
			this.senders.clear();
		
		this.starterName = rmodel.getStarter().getName();
		
		// object manager part..
		RobotComponent[] rcomps = rmodel.getSketchModel().getRobotComponents();
		this.connModel = rmodel.getConnectionModel();
		ArrayList<RDObject> rdobjList = new ArrayList<RDObject>();
		ArrayList<String> rdobjNameList = new ArrayList<String>();
		
		for(RobotComponent eachrc : rcomps){
			if(eachrc instanceof SourceEditable)
				controllers.put(eachrc.getName(), (SourceEditable)eachrc);
			if(eachrc instanceof RDObject){
				RDObject rdo = (RDObject)eachrc;
				rdobjList.add(rdo);
				rdobjNameList.add(eachrc.getName());
				
				if(eachrc.hasSender())
					senders.put(rdo, eachrc.getSenders());
				if(eachrc.hasReceiver())
					receivers.put(rdo, eachrc.getReceivers());
			}
		}
		this.ctrlerNames = controllers.keySet().toArray(new String[]{});
		this.rdobjNames = rdobjNameList.toArray(new String[]{});
		this.rdObjects = rdobjList.toArray(new RDObject[]{});
		

		// processor manager part..
		int idx = 0;
		SourceEditable ctrler = null;
		InputStream is = null;
		
		for(idx = 0; idx < ctrlerNames.length; idx++){
			try{
				ctrler = controllers.get(ctrlerNames[idx]);
				int type = rmodel.getSourceType(ctrler);
				
				if(type == RobotModel.STYPE_NULL)
					continue;
				else if(type == RobotModel.STYPE_FILE){
					is = new FileInputStream(rmodel.getSourceFile(ctrler));
				}else if(type == RobotModel.STYPE_PRJFILE){
					String[] path = rmodel.getSourcePrjFilePath(ctrler);
					ProjectItem focus = project;
					for(int j = 0; j < path.length; j++){
						for(int k = 0; k < focus.getChildCount(); k++){
							if(focus.getChildAt(k).toString().equals(path[j])){
								focus = (ProjectItem)focus.getChildAt(k);
								k = 2147483647;
								continue;
							}
						}
					}
					if(!(focus instanceof ProjectFile))
						continue;

					is = new FileInputStream(((ProjectFile)focus).getTotalPath());
				}else if(type == RobotModel.STYPE_URL){
					is = rmodel.getSourceURL(controllers.get(ctrlerNames[idx])).openStream();
				}
			}catch(IOException fnfe){
				throw new RDPLoadException(ctrlerNames[idx], RDPLoadException.CASE_STREAMING, fnfe);
			}
			try{
				processors.put(ctrler, rdpLoader.loadRDProcessor(is, rmodel.getSourceName(ctrler)));
			}catch(InstantiationException ie){
				throw new RDPLoadException(ctrlerNames[idx], RDPLoadException.CASE_INSTANTIATING, ie);
			}catch(IllegalAccessException iae){
				throw new RDPLoadException(ctrlerNames[idx], RDPLoadException.CASE_INSTANTIATING, iae);
			}catch(IOException ioe){
				throw new RDPLoadException(ctrlerNames[idx], RDPLoadException.CASE_STREAMING, ioe);
			}catch(ClassFormatError cfe){
				throw new RDPLoadException(ctrlerNames[idx], RDPLoadException.CASE_INSTANTIATING, cfe);
			}catch(NoClassDefFoundError ncdfe){
				ncdfe.printStackTrace();
				throw new RDPLoadException(ctrlerNames[idx], RDPLoadException.CASE_INSTANTIATING, ncdfe);
			}
			
			try{
				is.close();
			}catch(Exception e){}
		}
		
		
		RealizationMap smap = new RealizationMap();
		smap.load(skObjects);
		super.init(smap);
	}// end load(RobotModel)
	
	
	
	
	
	
	/* ************************************************
	 *                Overrides
	 **************************************************/
	
	public void startRealization() throws RealizationStartedException, RealizationNotReadyException{
		this.isRlzStarted = true;
		this.eventCaster.start();
		this.startProcessors();
		super.startRealization();
	}// end startRealization()
	
	public void resumeRealization() throws RealizationNotReadyException{
		super.resumeRealization();
		this.resumeProcessors();
	}// end resumeRealization
	
	public void pauseRealization() throws RealizationNotReadyException{
		super.pauseRealization();
		this.pauseProcessors();
	}// end pauseRlz
	
	
	public void disposeRealization() throws RealizationNotReadyException{
		agdr.robodari.appl.BugManager.printf("RMRlzModel : disposeRlztion() start\n");
		this.eventCaster.stop();
		this.disposeProcessors();
		
		while(this.isRunning()){
			BugManager.printf("RMRlzModel : disposeRlztion() : entity is running...\n");
			try{
				Thread.sleep(50);
			}catch(Exception excp){}
		}
		super.disposeRealization();
		this.isRlzStarted = false;
		BugManager.printf("RMRlzModel : disposeRlztion() end\n");
	}// end finishRealization
	
	public boolean isRlzStarted()
	{ return isRlzStarted; }
	
	
	
	/* *********************************************************
	 *      RDProcessManager Implementation
	 **********************************************************/
	
	public String getEntityName()
	{ return null; }
	
	public RDProcessor getProcessor(String prc)
	{ return processors.get(controllers.get(prc)); }
	public String[] getCtrlerNames()
	{ return this.ctrlerNames; }
	public boolean isStarter(String ctrlName)
	{ return this.starterName.equals(ctrlName); }
	
	public void initProcessors(){
		// look at load()
	}// end initProcessors
	public void startProcessors(){
		try{
			for(RDObject eachobj : this.rdObjects)
				eachobj.initRlztion(this);
		}catch(Exception e){
			agdr.robodari.appl.BugManager.log(e, true);
		}
		final RMRealizationModel _rmrmptr = this;
		
		for(int i = 0; i < ctrlerNames.length; i++) {
			final String name = ctrlerNames[i];
			Thread thr = new Thread(new Runnable() {
				public void run() {
					try{
						getProcessor(name).start(_rmrmptr, _rmrmptr, _rmrmptr, name,
										null, _rmrmptr, getRDOut(), getRDErr());
					}catch (Exception excp){
						agdr.robodari.plugin.rme.appl.ExceptionHandler
								.rdpi_unknownErrorOccured(name, excp);
					}
				}
			});
			thr.start();
		}
	}// end startProcessors()
	public void disposeProcessors(){
		for(int i = 0; i < ctrlerNames.length; i++){
			RDProcessor rdp = this.processors.get(this.controllers.get(ctrlerNames[i]));
			if(rdp == null) continue;
			try{
				rdp.dispose(this, this, this, ctrlerNames[i]);
			}catch(Exception excp){
				agdr.robodari.appl.BugManager.log(excp, true, false);
			}
		}

		try{
			for(RDObject eachobj : this.rdObjects)
				eachobj.disposeRlztion();
		}catch(Exception e){
			agdr.robodari.appl.BugManager.log(e, true);
		}
	}// end finishProcessors()
	
	public boolean isRunning(){
		for(int i = 0; i < ctrlerNames.length; i++){
			RDProcessor rdp = this.processors.get(this.controllers.get(ctrlerNames[i]));
			BugManager.printf("RMrlzModel : isRunning() : %s : %b\n", ctrlerNames[i], rdp.isRunning(this, this, this, ctrlerNames[i]));
			if(rdp == null) continue;
			if(rdp.isRunning(this, this, this, ctrlerNames[i]))
				return true;
		}
		return false;
	}// end isRunning()
	public void resetProcessors(){
		this.disposeProcessors();
		while(this.isRunning());
		this.startProcessors();
	}// end resetProcessors()

	public void pauseProcessors(){
		for(int i = 0; i < ctrlerNames.length; i++){
			RDProcessor rdp = this.processors.get(this.controllers.get(ctrlerNames[i]));
			if(rdp == null) continue;
			rdp.pause(this, this, this, ctrlerNames[i]);
		}
	}// end finishProcessors
	public void resumeProcessors(){
		for(int i = 0; i < ctrlerNames.length; i++){
			RDProcessor rdp = this.processors.get(ctrlerNames[i]);
			if(rdp == null) continue;
			rdp.resume(this, this, this, ctrlerNames[i]);
		}
	}// end finishProcessors
	
	
	
	/* *************************************************
	 *       RDConnManager Impl
	 *****************************************************/
	
	public boolean hasSender(RDObject rdo)
	{ return senders.containsKey(rdo); }
	public boolean hasReceiver(RDObject rdo)
	{ return receivers.containsKey(rdo); }
	
	public Sender[] getSenders(RDObject rdo)
	{ return senders.get(rdo); }
	public Receiver[] getReceivers(RDObject rdo)
	{ return receivers.get(rdo); }

	
	public RDObject getConn(Object obj){
		RobotComponent rc = null;
		if(obj instanceof Sender){
			Receiver rcv = connModel.getReceiver((Sender)obj);
			if(rcv != null)
				rc = connModel.getOwner(rcv).getRobotComponent();
		}
		if(rc == null && obj instanceof Receiver){
			Sender sd = connModel.getSender((Receiver)obj);
			if(sd != null)
				rc = connModel.getOwner(sd).getRobotComponent();
		}
		
		if(rc instanceof RDObject)
			return (RDObject)rc;
		return null;
	}// end getConn
	
	
	
	/* *********************************************************
	 *      RDObjectManager Implementation
	 **********************************************************/
	
	
	
	public boolean exists(RDEntity proc, String componentName){
		for(RDObject eachobj : rdObjects)
			if(eachobj.getName().equals(componentName))
				return true;
		return false;
	}// end exists(String[])


	public Object runMethod(RDEntity source,
			RDObject object, String methodName, Object[] args)
			throws RealizationNotReadyException, RDMethodNotExistException{
		if(!isRlzStarted)
			return null;
		
		int idx = RDPUtility.getRDMethodIndex(object, methodName, args);

		if(idx == -1){
			throw new RDMethodNotExistException("",
					object, methodName, args);
		}

		// event .
		Class[] argclasses = new Class[args.length];
		for(int i = 0; i < args.length; i++)
			argclasses[i] = (args[i] == null) ? null : args[i].getClass();
		
		Object value = object.callRDMethod(this, methodName, args);
		fireMethodCalled(new RDObjectEvent(source, object, RDObjectEvent.TYPE_METHOD, idx,
				args, argclasses));
		//rdobjevListModels.get(this.ctrlerComboBox.getSelectedIndex()).addElement(event);
		
		return value;
	}// end getMethodValue(String, Object[])
	
	public Object getFieldValue(RDEntity source, RDObject obj, String fieldName)
			throws RealizationNotReadyException, RDFieldNotExistException{
		if(!isRlzStarted)
			return null;

		int idx = RDPUtility.getRDFieldIndex(obj, fieldName);
		if(idx == -1)
			throw new RDFieldNotExistException("Field not found", obj, fieldName);
		
		Object fieldval = obj.getRDFieldValue(this, fieldName);
		fireFieldAccessed(new RDObjectEvent(source, obj,
				RDObjectEvent.TYPE_FIELD_ACCESS,
				idx,
				new Object[]{ fieldval },
				new Class[]{ fieldval == null ? null : fieldval.getClass() }));
		
		return fieldval;
	}// end getFieldValue(String, String)
	
	public void setFieldValue(RDEntity source, RDObject obj, String fieldName,
			Object value) throws RealizationNotReadyException, RDFieldNotExistException{
		if(!isRlzStarted)
			return;
		
		String[] names = obj.getRDFieldNames();
		RDObject.Type[] types = obj.getRDFieldTypes();
		int idx = RDPUtility.getRDFieldIndex(obj, fieldName);
		if(idx == -1)
			throw new RDFieldNotExistException("Field not found", obj, fieldName);
		
		// event .
		Object[] values = new Object[]{ value };
		Class[] valclasses = new Class[]{ null };
		if(value != null)
			valclasses[0] = value.getClass();
		RDObjectEvent event = new RDObjectEvent(source, obj, RDObjectEvent.TYPE_FIELD_WRITTEN,
				idx, values, valclasses);
		
		obj.setRDFieldValue(this, fieldName, value);
		fireFieldWritten(event);
	}// end setFieldValue(RoboDariProcessor, String, String, Object)
	
	public RDObject getRDObject(RDEntity source, String name){
		for(RDObject eachobj : rdObjects)
			if(eachobj.getName().equals(name))
				return eachobj;
		return null;
	}// end getRDObject
	
	public String[] getRDObjectNames(RDEntity src)
	{ return this.rdobjNames; }
	
	
	
	
	
	
	
	


	
	
	

	/*
	 * Event Listeners
	 */
	
	public void addRDObjectListener(RDObjectListener rdol)
	{ eventListeners.add(rdol); }
	public void removeRDObjectListener(RDObjectListener l)
	{ eventListeners.add(l); }
	public EventCaster getEventCaster()
	{ return this.eventCaster; }
	
	protected void fireMethodCalled(RDObjectEvent rdoe)
	{ eventCaster.fire(new RDCast(rdoe, eventListeners, RDCast.METHOD_CALLED)); }
	protected void fireFieldAccessed(RDObjectEvent rdoe)
	{ eventCaster.fire(new RDCast(rdoe, eventListeners, RDCast.FIELD_ACCESSED)); }
	protected void fireFieldWritten(RDObjectEvent rdoe)
	{ eventCaster.fire(new RDCast(rdoe, eventListeners, RDCast.FIELD_WRITTEN)); }
	
	
	
	
	
	public static class RDCast implements EventCaster.Cast{
		public final static int FIELD_ACCESSED = 1;
		public final static int FIELD_WRITTEN = 3;
		public final static int METHOD_CALLED = 2;
		
		RDObjectEvent rdoe;
		private ArrayList<RDObjectListener> listeners;
		int type;
		
		public RDCast(RDObjectEvent rdoe, ArrayList<RDObjectListener> listeners, int type)
		{ this.rdoe = rdoe; this.type = type; this.listeners = listeners; }
		
		public void fireEvent(){
			if(type == FIELD_ACCESSED)
				for(RDObjectListener eachel : listeners)
					eachel.fieldAccessed(rdoe);
			else if(type == METHOD_CALLED)
				for(RDObjectListener eachel : listeners)
					eachel.methodCalled(rdoe);
		}// end fireEvent()
	}// end class RDCast
	
	
	
	public static class RDPClassLoader extends ClassLoader{
		public RDProcessor loadRDProcessor(InputStream is, String name)
				throws IOException, ClassFormatError, InstantiationException, IllegalAccessException{
			byte[] data = new byte[is.available()];
			is.read(data, 0, data.length);
			Class cls = null;
			
			if((cls = super.findLoadedClass(name)) == null){
				cls = super.defineClass(name, data, 0, data.length);
			}
			
			RDProcessor rdpm = (RDProcessor)cls.newInstance();
			return rdpm;
		}// end loadRDProcessor(File)
	}// end class RDPClassLoader
}
