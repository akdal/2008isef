package agdr.robodari.plugin.rme.rlztion;

import java.util.*;

import javax.vecmath.*;


import agdr.library.math.MathUtility;
import agdr.robodari.appl.BugManager;
import agdr.robodari.comp.model.*;
import agdr.robodari.plugin.rme.edit.SkObject;
import agdr.robodari.plugin.rme.store.*;

public class RealizationMap{
	private final static boolean RM_DEBUG = true;
	
	private final static ForceGenerator[] NULLFGARRAY = new ForceGenerator[0];
	private final static ObjectAdjustment[] NULLOAARRAY = new ObjectAdjustment[0];
	private final static Key[] NULLKARRAY = new Key[]{};
	
	private Hashtable<Object, DataPackage> dataTable = new Hashtable<Object, DataPackage>();
	private Vector<Key> keys = new Vector<Key>();
	private Hashtable<SkObject, SkObject> parentTable = new Hashtable<SkObject, SkObject>();
	
	private ArrayList<ForceGenerator> forceGenerators = new ArrayList<ForceGenerator>();
	private ArrayList<ObjectAdjustment> objAdjustments = new ArrayList<ObjectAdjustment>();
	
	private Hashtable<Object, Integer> indexTable = new Hashtable<Object, Integer>();
	
	
	public RealizationMap(){
	}// end Constructor()
	
	public synchronized void load(SkObject[] objs){
		if(RM_DEBUG)
			BugManager.printf("-- RMap : LOAD : objs.len : %d\n", objs.length);
		//clear();
		for(int i = 0; i < objs.length; i++){
			Matrix4f itrans = new Matrix4f();
			itrans.m00 = itrans.m11 = itrans.m22 = itrans.m33 = 1;
			RealizationCompStore.generateGraph(
							objs[i].getModel(),
							objs[i],
							itrans,
							this, null);
		}
		Key[] keys = this.getKeys();
		
		int idx = 0;
		for(int i = 0; i < keys.length; i++){
			if(!indexTable.containsKey(keys[i].ref))
				indexTable.put(keys[i].ref, idx++);
		}
	}// end load(SkObject[])
	public void clear(){
		this.dataTable.clear();
		this.forceGenerators.clear();
		objAdjustments.clear();
		this.keys.clear();
		this.indexTable.clear();
	}// end clear()
	
	
	
	
	public Key addElement(RobotCompModel rcm, Matrix4f initialTrans, SkObject owner, boolean cmpAdjustmentNeeded)
	{ return addElement(rcm, initialTrans, owner, null, cmpAdjustmentNeeded); }
	public Key addElement(RobotCompModel rcm, Matrix4f initialTrans, SkObject owner, MergingObserver obsv, boolean cmpAdjustmentNeeded){
		Key key = new Key();
		key.ref = new Object();
		setElement(key, rcm, initialTrans, owner, obsv, cmpAdjustmentNeeded);
		return key;
	}/// end addEl

	public void setElement(Key key, RobotCompModel rcm, Matrix4f initialTrans, SkObject owner, boolean cmpAdjustmentNeeded)
	{ setElement(key, rcm, initialTrans, owner, null, cmpAdjustmentNeeded); }
	public void setElement(Key key, RobotCompModel rcm, Matrix4f initialTrans, SkObject owner, MergingObserver obsv, boolean cmpAdjustmentNeeded){
		DataPackage dp = dataTable.get(key.ref);
		if(!dataTable.containsKey(key.ref)){
			dp = new DataPackage();
			dataTable.put(key.ref, dp);
		}
		if(RM_DEBUG)
			BugManager.printf("RlzMap : setElement : key : %s, owner : %s\n", key, owner);
		
		dp.observer = obsv;
		dp.rcompModel = rcm;
		if(dp.initTransf == null)
			dp.initTransf = new Matrix4f();
		dp.initTransf.set(initialTrans);
		dp.owner = owner;
		this.keys.add(key);
		
		if(rcm != null){
			if(rcm.isGroup()){
				dp.physData = rcm.getGroupPhysData();
				if(dp.physData == null)
					dp.physData = rcm.calcGroupPhysData();
			}else{
				dp.physData = rcm.getPhysicalData();
			}
		}

		if(cmpAdjustmentNeeded){
			Point3f cmp = dp.physData.centerMassPoint;
			
			initialTrans = dp.initTransf;
			initialTrans.m03 = initialTrans.m03 + initialTrans.m00*cmp.x + initialTrans.m01*cmp.y + initialTrans.m02*cmp.z;
			initialTrans.m13 = initialTrans.m13 + initialTrans.m10*cmp.x + initialTrans.m11*cmp.y + initialTrans.m12*cmp.z;
			initialTrans.m23 = initialTrans.m23 + initialTrans.m20*cmp.x + initialTrans.m21*cmp.y + initialTrans.m22*cmp.z;
		}
	}// end setElement(int, RobotCompModel, Matrix4f, boolean)
	
	public void mergeElements(Key ... keys){
		RobotCompModel res = new RobotCompModel();
		res.setGroup(true);
		Matrix4f orgtrans = new Matrix4f(),
				orgtrans_inv = new Matrix4f();
		Point3f cmp;

		if(RM_DEBUG){
			BugManager.printf("RMap : mergeElements()\n");
			BugManager.printf("RMap : each key : \n");
			for(int i = 0; i < keys.length; i++){
				BugManager.printf("%s(%s)\n", this.getOwner(keys[i]), keys[i]);
			}
		}
		
		DataPackage dp0 = dataTable.get(keys[0].ref);
		// cmp adjustment를 풀어주어야 한다!!
		if(dp0.physData == null || dp0.physData.centerMassPoint == null)
			cmp = new Point3f(0, 0, 0);
		else
			cmp = dp0.physData.centerMassPoint;
		
		orgtrans.set(dp0.initTransf);
		//BugManager.printf("RMap : standard transf : \n%s", orgtrans);
		orgtrans.m03 -= orgtrans.m00*cmp.x + orgtrans.m01*cmp.y + orgtrans.m02*cmp.z;
		orgtrans.m13 -= orgtrans.m10*cmp.x + orgtrans.m11*cmp.y + orgtrans.m12*cmp.z;
		orgtrans.m23 -= orgtrans.m20*cmp.x + orgtrans.m21*cmp.y + orgtrans.m22*cmp.z;
		//BugManager.printf("->\n%s", orgtrans);
		MathUtility.fastInvert(orgtrans, orgtrans_inv);
		
		Matrix4f tmptrans = new Matrix4f();
		GroupMergingObserver gmo = new GroupMergingObserver();
		
		for(int i = 0; i < keys.length; i++){
			Matrix4f trans = new Matrix4f();
			DataPackage dpi = dataTable.get(keys[i].ref);
			//BugManager.printf("RlzMap : %s removal!\n", keys[i]);
			if(i == 0){
				trans.m00 = trans.m11 = trans.m22 = trans.m33 = 1;
				if(dpi.observer != null){
					gmo.addObserver(dpi.observer);
					dpi.observer.merged(keys[i], new Matrix4d(trans));
				}
			}else{
				cmp = dpi.physData.centerMassPoint;
				tmptrans.set(dpi.initTransf);
				tmptrans.m03 -= tmptrans.m00*cmp.x + tmptrans.m01*cmp.y + tmptrans.m02*cmp.z;
				tmptrans.m13 -= tmptrans.m10*cmp.x + tmptrans.m11*cmp.y + tmptrans.m12*cmp.z;
				tmptrans.m23 -= tmptrans.m20*cmp.x + tmptrans.m21*cmp.y + tmptrans.m22*cmp.z;
				
				MathUtility.fastMul(orgtrans_inv, tmptrans, trans);
				
				dataTable.remove(keys[i].ref);
				if(dpi.observer != null){
					gmo.addObserver(dpi.observer);
					dpi.observer.merged(keys[i], new Matrix4d(trans));
				}
				this.keys.remove(keys[i]);
				keys[i].ref = keys[0].ref;
			}
			
			//BugManager.printf("RMap : current keys : \n");
			//Key[] aakeys = this.getKeys();
			//for(Key eachKey : aakeys)
			//	BugManager.printf("%s , \n", eachKey);
			
			res.addSubModel(dpi.rcompModel, trans);
		}
		
		dp0.rcompModel = res;
		dp0.observer = gmo;
		
		//owners[idxs[0]] = no change
		PhysicalData newdata = res.calcGroupPhysData();
		// 다시 cmp adjustment를 넣어준다.
		float varx = newdata.centerMassPoint.x - dp0.physData.centerMassPoint.x,
				vary = newdata.centerMassPoint.y - dp0.physData.centerMassPoint.y,
				varz = newdata.centerMassPoint.z - dp0.physData.centerMassPoint.z;
		dp0.initTransf.m03 = varx * dp0.initTransf.m00 + vary * dp0.initTransf.m01 + varz * dp0.initTransf.m02 + dp0.initTransf.m03;
		dp0.initTransf.m13 = varx * dp0.initTransf.m10 + vary * dp0.initTransf.m11 + varz * dp0.initTransf.m12 + dp0.initTransf.m13;
		dp0.initTransf.m23 = varx * dp0.initTransf.m20 + vary * dp0.initTransf.m21 + varz * dp0.initTransf.m22 + dp0.initTransf.m23;
		dp0.physData = newdata;
		
		//BugManager.printf("RMap : mergeElem : new phdata : \n  cmp : %s\n  inertia : %s  mass : %g\n   initTransf : \n%s",
		//		newdata.centerMassPoint, newdata.inertia, newdata.mass,
		//		dp0.initTransf);

		if(RM_DEBUG){
			BugManager.printf("RMap : mergeElem ; merging finished! remained keys : \n");
			Key[] aakeys = this.getKeys();
			for(Key eachKey : aakeys)
				BugManager.printf("%s , \n", eachKey);
		}
	}// end mergeELement
	
	public void setParent(SkObject child, SkObject p)
	{ this.parentTable.put(child, p); }
	public SkObject getParent(SkObject so)
	{ return parentTable.get(so); }
	
	public void removeElement(Key key){

		if(RM_DEBUG)
			BugManager.printf("RlzMap : rmvElem : %s\n", key);
		if(!dataTable.containsKey(key.ref))
			return;
		keys.remove(key);
		dataTable.remove(key.ref);
	}// end removeElement(int)
	public void removeElements(Key... keys){
		for(int i = 0; i < keys.length; i++)
			removeElement(keys[i]);
	}// end removeElements
	
	public int getModelCount()
	{ return dataTable.size(); }
	public Key[] getKeys()
	{ return keys.toArray(NULLKARRAY); }
	public Key getKey(int idx)
	{ return keys.get(idx); }
	public boolean cotainsKey(Key key)
	{ return Arrays.binarySearch(this.getKeys(), key) != -1; } 
	public int getIndex(Key key)
	{ return indexTable.get(key.ref); }
	
	public RobotCompModel getModel(Key key)
	{ return dataTable.get(key.ref).rcompModel; }
	public SkObject getOwner(Key key)
	{ return dataTable.get(key.ref).owner; }
	//public Key getKey(RobotCompModel rcm)
	//{ return keyTable.get(rcm); }
	
	public PhysicalData getPhysicalData(Key key)
	{ return dataTable.get(key.ref).physData; }
	public boolean hasTransform(Key key){
		DataPackage dp = dataTable.get(key.ref);
		return dp.initTransf != null;
	}//end hasTransform
	public void getInitialTransform(Key key, Matrix4f m){
		if(m != null)
			m.set(this.dataTable.get(key.ref).initTransf);
	}// end getTransform
	
	public void addMergingObserver(Key key, MergingObserver obsv){
		DataPackage dp = dataTable.get(key);
		if(dp.observer == null)
			dp.observer = obsv;
		else if(dp.observer instanceof GroupMergingObserver)
			((GroupMergingObserver)dp.observer).addObserver(obsv);
		else{
			GroupMergingObserver gmo = new GroupMergingObserver();
			gmo.addObserver(obsv);
			gmo.addObserver(dp.observer);
			dp.observer = gmo;
		}
	}// end addMergingObserver
	
	
	
	
	public void addObjectAdjustment(ObjectAdjustment oa)
	{ objAdjustments.add(oa); }
	public int getObjAdjustmentCount()
	{ return objAdjustments.size(); }
	public ObjectAdjustment getObjectAdjustment(int idx)
	{ return objAdjustments.get(idx); }
	public ObjectAdjustment[] getObjectAdjustments()
	{ return objAdjustments.toArray(NULLOAARRAY); }
	
	public void addForceGenerator(ForceGenerator fc)
	{ forceGenerators.add(fc); }
	public int getForceGeneratorCount()
	{ return forceGenerators.size(); }
	public ForceGenerator getForceGenerator(int idx)
	{ return forceGenerators.get(idx); }
	public ForceGenerator[] getForceGenerators()
	{ return forceGenerators.toArray(NULLFGARRAY); }
	/*
	public Matrix3d addInertiaTensorOnCM(int idx, Matrix3d tensor){
		tensor.add(inertiaTensorsOnCM[idx]);
		return tensor;
	}// end addInertiaTensorOnCM
	*/
	
	
	
	static class DataPackage{
		RobotCompModel rcompModel;
		Matrix4f initTransf;
		PhysicalData physData;
		SkObject owner;
		MergingObserver observer;
	}// end class DataPackage
	static class GroupMergingObserver implements MergingObserver{
		private ArrayList<MergingObserver> observers = new ArrayList<MergingObserver>();
		
		public void addObserver(MergingObserver ob)
		{ this.observers.add(ob); }
		
		public void merged(RealizationMap.Key key, Matrix4d m4f){
			for(MergingObserver eachobsv : observers)
				eachobsv.merged(key, m4f);
		}
	}// end class GroupMergingObserver
	
	public static class Key{
		protected Object ref;
		
		public boolean equals(Object o){
			if(this == o) return true;
			else if(o == null) return false;
			else if(!(o instanceof Key)) return false;
			return ((Key)o).ref == ref;
		}// end eq
		public String toString()
		{ return ref == null ? "[null]" : "[" + ref.hashCode() + "]"; }
	}// end class Key
}
