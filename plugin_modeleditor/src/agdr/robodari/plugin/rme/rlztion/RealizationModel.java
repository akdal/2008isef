package agdr.robodari.plugin.rme.rlztion;

import java.util.*;
import java.io.*;

import javax.vecmath.*;
import javax.media.j3d.*;
import javax.swing.*;

import agdr.library.math.MathUtility;
import agdr.robodari.appl.*;
import agdr.robodari.comp.model.*;
import agdr.robodari.plugin.rme.rdpi.*;

import com.sun.j3d.utils.geometry.*;



public abstract class RealizationModel implements GeometryUpdater, Clock{
	private final static GeometryArray[] NULLGAARRAY = new GeometryArray[]{};
	
	private Hashtable<GeometryArray, Integer> garrayTable
			= new Hashtable<GeometryArray, Integer>(1, 0.3f);
	private Hashtable<GeometryArray, Appearance> apprTable
			= new Hashtable<GeometryArray, Appearance>(1, 0.3f);
	private GeometryArray[] geoms;
	private float[][] originalPoints;
	private float[][] normals;
	
	private RealizationMap rlzMap;
	private World world;
	private boolean update3d = true;
	
	private ArrayList<RlzModelListener> rlzModelListeners = new ArrayList<RlzModelListener>();
	protected PrintStream outstream = System.out;
	protected PrintStream errstream = System.err;
	
	
	
	
	protected void init(RealizationMap smap){
		this.rlzMap = smap;
		//BugManager.printf("RModel : reset(smap) : smap.getModelCount() : %d\n", smap.getModelCount());
		
		world = new World(this);
		
		geoms = new GeometryArray[smap.getModelCount()];
		originalPoints = new float[smap.getModelCount()][];
		normals = new float[smap.getModelCount()][];
		garrayTable.clear();
		apprTable.clear();
		
		RealizationMap.Key[] keys = smap.getKeys();
		BugManager.printf("RlzModel : init() : keys.len : %d, rmap.getModelCnt() : %d\n", keys.length, smap.getModelCount());
		for(int i = 0; i < keys.length; i++){
			BugManager.printf("RlzModel : init() : keys[%d] : %s, keys.ref : %s\n", i, keys[i], keys[i].ref);
			resetGeometryArray(keys[i]);
		}
		for(int i = 0; i < rlzModelListeners.size(); i++)
			rlzModelListeners.get(i).rlzInit(this);
	}// end load(SketchModel, ConnectionModel)
	
	
	public void addRlzModelListener(RlzModelListener rml)
	{ rlzModelListeners.add(rml); }
	public void removeRlzModelListener(RlzModelListener rml)
	{ rlzModelListeners.remove(rml); }
	
	
	
	public void startRealization() throws RealizationStartedException, RealizationNotReadyException{
		for(int i = 0; i < rlzModelListeners.size(); i++)
			rlzModelListeners.get(i).rlzStarted(this);
		world.startWorld();
	}// end hello()
	
	public void resumeRealization() throws RealizationNotReadyException{
		for(int i = 0; i < rlzModelListeners.size(); i++)
			rlzModelListeners.get(i).rlzResumed(this);
		world.resumeWorld();
	}// end resumeRlz
	
	public void pauseRealization() throws RealizationNotReadyException{
		for(int i = 0; i < rlzModelListeners.size(); i++)
			rlzModelListeners.get(i).rlzPaused(this);
		world.pauseWorld();
	}// end pause()
	
	public void disposeRealization() throws RealizationNotReadyException{
		for(int i = 0; i < rlzModelListeners.size(); i++)
			rlzModelListeners.get(i).rlzDisposed(this);
		world.byeWorld();
	}// end bye()
	
	public boolean isRlzStarted()
	{ return world.saidHello(); }
	
	
	public abstract EventCaster getEventCaster();
	
	public RealizationMap getRealizationMap()
	{ return rlzMap; }
	public World getWorld()
	{ return world; }
	
	//public GeometryArray getGeometry(RobotCompModel str)
	//{ return geoms[this.rlzMap.getIndex(rlzMap.getKey(str))]; }
	public GeometryArray[] getGeometries()
	{ return geoms; }
	public Appearance getAppearance(GeometryArray ga)
	{ return apprTable.get(ga); }
	
	public PrintStream getRDOut()
	{ return outstream; }
	public void setRDOut(PrintStream os)
	{ this.outstream = os; }
	public PrintStream getRDErr()
	{ return this.errstream; }
	public void setRDErr(PrintStream os)
	{ this.errstream = os; }

	public boolean updates3D()
	{ return update3d; }
	public void setUpdate3D(boolean b)
	{ this.update3d = b; }
	
	
	public long getMilliSecond()
	{ return this.world.getElapsedTime(); }
	public double getTimeRate()
	{ return this.world.getTimeRate(); }
	
	
	/* ****************************************
	 * GeometryUpdator
	 ******************************************/
	
	
	
	public void resetGeometryArray(RealizationMap.Key key){
		try{
			RobotCompModel str = rlzMap.getModel(key);
			int currentPos = rlzMap.getIndex(key);
			
			if(this.geoms[currentPos] == null){
				//currentPos에 data쓰기 작업 실시
				//BugManager.println("RModel : resetGeomArray : -- create new geom");
				Point3f[] orgpoints = null;
				Integer[] facepos = null;
				int[][] faces = null;
				AppearanceData[] ads = null;
				PhysicalData pd = rlzMap.getPhysicalData(key);
				
				if(str.isGroup()){
					ArrayList<Point3f> plist = new ArrayList<Point3f>();
					ArrayList<Integer> faceposlist = new ArrayList<Integer>();
					ArrayList<int[]> facelist = new ArrayList<int[]>();
					ArrayList<AppearanceData> adlist = new ArrayList<AppearanceData>();
					
					Matrix4f itrans = new Matrix4f();
					itrans.m00 = itrans.m11 = itrans.m22 = itrans.m33 = 1;
					_fetchInfos(str, plist, faceposlist, facelist, adlist, itrans);
					
					orgpoints = plist.toArray(new Point3f[]{});
					facepos = faceposlist.toArray(new Integer[]{});
					faces = facelist.toArray(new int[][]{{}});
					ads = adlist.toArray(new AppearanceData[]{});
				}else{
					orgpoints = str.getPoints();
					facepos = new Integer[]{
							0
					};
					faces = str.getFaces();
					ads = new AppearanceData[]{
							str.getApprData()
					};
				}
				
				//BugManager.println("RModel : resetGeomArr : facepos :");
				//for(int i = 0; i < facepos.length; i++)
				//	BugManager.printf(" %d", facepos[i]);
				//BugManager.printf("\nRModel : resetGA : faces.len : %d\n", faces.length);
				
				float[] tri_orgpoints;
				float[] tri_transfpoints;
				float[] tri_newpoints;
				int[] tri_orgppos = new int[facepos.length];
				int tri_orgpposidx = 0;
				// original points copy
				Point3f[] orgpoints_cmpadj;
				float[] tri_orgpntdraft;
				Point3f cmp = new Point3f();
				cmp.x = pd.centerMassPoint.x;
				cmp.y = pd.centerMassPoint.y;
				cmp.z = pd.centerMassPoint.z;
				
				//BugManager.printf("Rmodel : resetGeomArray : cmp : %s\n", cmp);

				tri_orgpoints = new float[0];
				
				// cmp adjustment..
				orgpoints_cmpadj = new Point3f[orgpoints.length];
				for(int i = 0; i < orgpoints_cmpadj.length; i++){
					orgpoints_cmpadj[i] = new Point3f();
					orgpoints_cmpadj[i].x = orgpoints[i].x - cmp.x;
					orgpoints_cmpadj[i].y = orgpoints[i].y - cmp.y;
					orgpoints_cmpadj[i].z = orgpoints[i].z - cmp.z;
				}
				for(int i = 0; i < faces.length; i++){
					if(tri_orgpposidx < facepos.length && i == facepos[tri_orgpposidx]){
						tri_orgppos[tri_orgpposidx++] = tri_orgpoints.length / 3;
					}
					tri_orgpntdraft = new float[faces[i].length * 3];
					
					for(int j = 0; j < faces[i].length; j++){
						tri_orgpntdraft[j * 3] = orgpoints_cmpadj[faces[i][j]].x;
						tri_orgpntdraft[j * 3 + 1] = orgpoints_cmpadj[faces[i][j]].y;
						tri_orgpntdraft[j * 3 + 2] = orgpoints_cmpadj[faces[i][j]].z;
					}

					int[] triangleids = MathUtility.triangulate3d(tri_orgpntdraft);
					tri_newpoints = new float[triangleids.length * 3
					                           + tri_orgpoints.length];
					System.arraycopy(tri_orgpoints, 0, tri_newpoints, 0, tri_orgpoints.length);
					for(int j = 0; j < triangleids.length; j++){
						tri_newpoints[tri_orgpoints.length + j * 3] = tri_orgpntdraft[triangleids[j] * 3];
						tri_newpoints[tri_orgpoints.length + j * 3 + 1]
						              = tri_orgpntdraft[triangleids[j] * 3 + 1];
						tri_newpoints[tri_orgpoints.length + j * 3 + 2]
						              = tri_orgpntdraft[triangleids[j] * 3 + 2];
					}
					tri_orgpoints = tri_newpoints;
				}
				//for(int i = 0; i < tri_orgpoints.length / 3; i++){
					//BugManager.printf("RModel : resetGA : tri_orgpoints[%d] : %f %f %f\n", i, tri_orgpoints[i * 3], tri_orgpoints[i * 3 + 1], tri_orgpoints[i * 3 + 2]);
				//}

				Matrix4f trans = new Matrix4f();
				// world.getTransform
				rlzMap.getInitialTransform(key, trans);
				//BugManager.printf("RModel : resetGeomArray : rlzmap.getTrans : %s", trans);
				
				// unit of trans : cm
				tri_transfpoints = new float[tri_orgpoints.length];
				for(int i = 0; i < tri_orgpoints.length / 3; i++){
					tri_transfpoints[i * 3] =
							tri_orgpoints[i * 3] * trans.m00 + 
							tri_orgpoints[i * 3 + 1] * trans.m01 + 
							tri_orgpoints[i * 3 + 2] * trans.m02 + 
							trans.m03;
					tri_transfpoints[i * 3 + 1] =
							tri_orgpoints[i * 3] * trans.m10 + 
							tri_orgpoints[i * 3 + 1] * trans.m11 + 
							tri_orgpoints[i * 3 + 2] * trans.m12 + 
							trans.m13;
					tri_transfpoints[i * 3 + 2] =
							tri_orgpoints[i * 3] * trans.m20 + 
							tri_orgpoints[i * 3 + 1] * trans.m21 + 
							tri_orgpoints[i * 3 + 2] * trans.m22 + 
							trans.m23;
					//System.out.println(tri_transfpoints[i * 3] + " , " + tri_transfpoints[i * 3 + 1] + " , " + tri_transfpoints[i * 3 + 2]);
					//System.out.println("(" + tri_orgpoints[i * 3] + " , " + tri_orgpoints[i * 3 + 1] + " , " + tri_orgpoints[i * 3 + 2] + ")");
				}

				
				// normals
				float[] orgnormals = new float[tri_orgpoints.length];
				float[] normals = new float[orgnormals.length];
				Vector3f vn = null;
				
				for(int i = 0; i < orgnormals.length / 9; i++){
					float[] triangle = new float[]{
							tri_orgpoints[i * 9], tri_orgpoints[i*9+1], tri_orgpoints[i*9+2],
							tri_orgpoints[i*9+3], tri_orgpoints[i*9+4], tri_orgpoints[i*9+5],
							tri_orgpoints[i*9+6], tri_orgpoints[i*9+7], tri_orgpoints[i*9+8]
					};
					vn = MathUtility.generateNormal(triangle, vn);
					orgnormals[i * 9] = orgnormals[i*9+3] = orgnormals[i*9+6] = (float)vn.x;
					orgnormals[i*9+1] = orgnormals[i*9+4] = orgnormals[i*9+7] = (float)vn.y;
					orgnormals[i*9+2] = orgnormals[i*9+5] = orgnormals[i*9+8] = (float)vn.z;
					
					normals[i * 9] = normals[i*9+3] = normals[i*9+6] =
						(float)(vn.x * trans.m00 + vn.y * trans.m01 + vn.z * trans.m02);
					normals[i*9+1] = normals[i*9+4] = normals[i*9+7] =
						(float)(vn.x * trans.m10 + vn.y * trans.m11 + vn.z * trans.m12);
					normals[i*9+2] = normals[i*9+5] = normals[i*9+8] =
						(float)(vn.x * trans.m20 + vn.y * trans.m21 + vn.z * trans.m22);
				}
				
				
				float[] colordata = new float[tri_transfpoints.length];
				int lim = 0;
				
				java.awt.Color col = java.awt.Color.white;
				for(int i = 0; i < ads.length; i++){
					if(ads[i].getColor() != null)
						col = ads[i].getColor();
					
					if(i != ads.length - 1)
						lim = tri_orgppos[i + 1];
					else
						lim = tri_transfpoints.length / 3;

					for(int j = tri_orgppos[i]; j < lim; j++){
						colordata[j * 3] = (float)col.getRed() / 255.0f;
						colordata[j * 3 + 1] = (float)col.getGreen() / 255.0f;
						colordata[j * 3 + 2] = (float)col.getBlue() / 255.0f;
					}
				}
				
				GeometryArray garray = new TriangleArray(tri_transfpoints.length / 3,
						TriangleArray.COORDINATES
						| TriangleArray.NORMALS
						| TriangleArray.BY_REFERENCE
						| TriangleArray.COLOR_3);
				garray.setCoordRefFloat(tri_transfpoints);
				garray.setColorRefFloat(colordata);
				garray.setCapability(GeometryArray.ALLOW_COORDINATE_WRITE);
				garray.setCapability(GeometryArray.ALLOW_REF_DATA_WRITE);
				garray.setCapability(GeometryArray.ALLOW_COUNT_WRITE);
				garray.setCapability(GeometryArray.ALLOW_NORMAL_READ);
				garray.setCapability(GeometryArray.ALLOW_NORMAL_WRITE);
				garray.setNormalRefFloat(normals);
				
				Appearance appr = new Appearance();
				Material m = new Material();
				m.setLightingEnable(true);
				m.setShininess(10f);
				appr.setMaterial(m);

				this.garrayTable.put(garray, currentPos);
				this.apprTable.put(garray, appr);
				this.geoms[currentPos] = garray;
				this.normals[currentPos] = orgnormals;
				this.originalPoints[currentPos] = tri_orgpoints;
			}else{
				if(this.update3d)
					geoms[currentPos].updateData(this);
			}
		}catch(Exception excp){
			BugManager.log(excp, true);
		}
	}// end generateGEometryARray(STructure)
	private void _fetchInfos(RobotCompModel rcm, ArrayList<Point3f> plist,
			ArrayList<Integer> faceposlist,
			ArrayList<int[]> facelist,
			ArrayList<AppearanceData> adlist,
			Matrix4f transf){
		if(rcm.isGroup()){
			int lim = rcm.getSubModelCount();
			for(int i = 0; i < lim; i++){
				RobotCompModel smodel = rcm.getSubModel(i);
				
				Matrix4f subtransf = MathUtility.fastMul(transf, rcm.getTransform(smodel), null);
				_fetchInfos(smodel, plist, faceposlist, facelist, adlist, subtransf);
			}
		}else{
			int f = plist.size();
			adlist.add(rcm.getApprData());
			
			Point3f[] ps = rcm.getPoints();
			int[][] faces = rcm.getFaces();
			
			for(int i = 0; i < ps.length; i++){
				Point3f p = new Point3f();
				p.x = ps[i].x * transf.m00 + ps[i].y * transf.m01 + ps[i].z * transf.m02 + transf.m03;
				p.y = ps[i].x * transf.m10 + ps[i].y * transf.m11 + ps[i].z * transf.m12 + transf.m13;
				p.z = ps[i].x * transf.m20 + ps[i].y * transf.m21 + ps[i].z * transf.m22 + transf.m23;
				//BugManager.printf("p[%d] : %s\n", i, p);
				plist.add(p);
			}
			faceposlist.add(facelist.size());
			for(int i = 0; i < faces.length; i++){
				int[] array = faces[i].clone();
				for(int j = 0; j < array.length; j++)
					array[j] += f;
				facelist.add(array);
			}
		}
	}// end _fetchInfos
	
	
	
	Matrix4f _ud_trans = new Matrix4f();
	Matrix4d _ud_trans_d = new Matrix4d();
	public void updateData(Geometry geom){
		// unit of location : m
		int strid = garrayTable.get(geom);
		
		world.getTransform(strid, _ud_trans_d);
		_ud_trans.set(_ud_trans_d);
		
		float[] orgpoints = originalPoints[strid];
		float[] points = geoms[strid].getCoordRefFloat();
		float[] orgnormals = normals[strid];
		float[] normals = geoms[strid].getNormalRefFloat();
		int j = 0;
		
		
		for(int i = 0; i < orgpoints.length / 3; i++){
			j = i * 3;
			
			points[j] =     _ud_trans.m00 * orgpoints[j] + _ud_trans.m01 * orgpoints[j+1] + _ud_trans.m02 * orgpoints[j+2] + _ud_trans.m03;
			points[j + 1] = _ud_trans.m10 * orgpoints[j] + _ud_trans.m11 * orgpoints[j+1] + _ud_trans.m12 * orgpoints[j+2] + _ud_trans.m13;
			points[j + 2] = _ud_trans.m20 * orgpoints[j] + _ud_trans.m21 * orgpoints[j+1] + _ud_trans.m22 * orgpoints[j+2] + _ud_trans.m23;
		}
		for(int i = 0; i < normals.length / 3; i++){
			j = i * 3;
			
			normals[i * 3] =     _ud_trans.m00 * orgnormals[j] + _ud_trans.m01 * orgnormals[j+1] + _ud_trans.m02 * orgnormals[j+2];
			normals[i * 3 + 1] = _ud_trans.m10 * orgnormals[j] + _ud_trans.m11 * orgnormals[j+1] + _ud_trans.m12 * orgnormals[j+2];
			normals[i * 3 + 2] = _ud_trans.m20 * orgnormals[j] + _ud_trans.m21 * orgnormals[j+1] + _ud_trans.m22 * orgnormals[j+2];
		}
		//MathUtility.generateNormals(points, normals);
	}// end updateData(Geometry)
}
