package agdr.robodari.plugin.rme.rlztion;

public class RealizationNotReadyException extends Exception{
	public RealizationNotReadyException(){}
	public RealizationNotReadyException(String message)
	{ super(message); }
	public RealizationNotReadyException(String message, Throwable cause)
	{ super(message, cause); }
	public RealizationNotReadyException(Throwable cause)
	{ super(cause); }
}
