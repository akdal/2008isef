package agdr.robodari.plugin.rme.rlztion;

public class RealizationStartedException extends Exception{
	public RealizationStartedException(){}
	public RealizationStartedException(String message)
	{ super(message); }
	public RealizationStartedException(String message, Throwable cause)
	{ super(message, cause); }
	public RealizationStartedException(Throwable cause)
	{ super(cause); }
}
