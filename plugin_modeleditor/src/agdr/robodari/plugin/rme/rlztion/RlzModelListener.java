package agdr.robodari.plugin.rme.rlztion;

public interface RlzModelListener {
	public void rlzInit(RealizationModel rm);
	public void rlzStarted(RealizationModel rm);
	public void rlzResumed(RealizationModel rm);
	public void rlzPaused(RealizationModel rm);
	public void rlzDisposed(RealizationModel rm);
}
