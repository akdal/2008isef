package agdr.robodari.plugin.rme.rlztion;

public class TypeNotMatchException extends Exception{
	public TypeNotMatchException(){}
	public TypeNotMatchException(String message) 
	{ super(message); }
	public TypeNotMatchException(String message, Throwable cause)
	{ super(message, cause); }
	public TypeNotMatchException(Throwable cause) 
	{ super(cause); }
}
