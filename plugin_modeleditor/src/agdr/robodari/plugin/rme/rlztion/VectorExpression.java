package agdr.robodari.plugin.rme.rlztion;

public class VectorExpression {
	public double x_const;
	public double x_xfactor;
	public double x_yfactor;
	public double x_zfactor;
	public double x_xyfactor;
	public double x_yzfactor;
	public double x_xzfactor;
	
	public double y_const;
	public double y_xfactor;
	public double y_yfactor;
	public double y_zfactor;
	public double y_xyfactor;
	public double y_yzfactor;
	public double y_xzfactor;

	public double z_const;
	public double z_xfactor;
	public double z_yfactor;
	public double z_zfactor;
	public double z_xyfactor;
	public double z_yzfactor;
	public double z_xzfactor;
	
	
	// must be implemented
	public void solve(VectorExpression ve2, double[] result){
		
	}// end solve(VectorExpression, double[])
	
	public void clear(){
		x_const = x_xfactor = x_yfactor = x_zfactor =
				x_xyfactor = x_xzfactor = x_yzfactor = 
			y_const = y_xfactor = y_yfactor = y_zfactor = 
				y_xyfactor = y_xzfactor = y_yzfactor = 
			z_const = z_xfactor = z_yfactor = z_zfactor = 
				z_xyfactor = z_xzfactor = z_yzfactor = 0;
	}// end clear()
	
	public boolean isEmpty(){
		return x_const == 0
			&& x_xfactor == 0
			&& x_yfactor == 0
			&& x_zfactor == 0
			&& x_xyfactor == 0
			&& x_xzfactor == 0
			&& x_yzfactor == 0
			&& y_const == 0
			&& y_xfactor == 0
			&& y_yfactor == 0
			&& y_zfactor == 0 
			&& y_xyfactor == 0
			&& y_xzfactor == 0
			&& y_yzfactor == 0
			&& z_const == 0
			&& z_xfactor == 0
			&& z_yfactor == 0
			&& z_zfactor == 0 
			&& z_xyfactor == 0
			&& z_xzfactor == 0
			&& z_yzfactor == 0;
	}// end empty()
	public boolean equals(Object obj){
		if(obj == null) return false;
		else if(!(obj instanceof VectorExpression))
			return false;
		
		VectorExpression ve = (VectorExpression)obj;
		return x_const == ve.x_const && 
				x_xfactor == ve.x_xfactor && 
				x_yfactor == ve.x_yfactor && 
				x_zfactor == ve.x_zfactor && 
				x_xyfactor == ve.x_xzfactor && 
				x_xzfactor == ve.x_xzfactor && 
				x_yzfactor == ve.x_yzfactor && 
			y_const == ve.y_const && 
				y_xfactor == ve.y_xfactor && 
				y_yfactor == ve.y_yfactor && 
				y_zfactor == ve.y_zfactor && 
				y_xyfactor == ve.y_xyfactor && 
				y_xzfactor == ve.y_xzfactor && 
				y_yzfactor == ve.y_yzfactor && 
			z_const == ve.z_const && 
				z_xfactor == ve.z_xfactor && 
				z_yfactor == ve.z_yfactor && 
				z_zfactor == ve.z_zfactor && 
				z_xyfactor == ve.z_xyfactor && 
				z_xzfactor == ve.z_xzfactor && 
				z_yzfactor == ve.z_yzfactor; 
	}// end equals(Object)
}
