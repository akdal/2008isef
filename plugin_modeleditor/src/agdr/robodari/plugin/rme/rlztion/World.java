package agdr.robodari.plugin.rme.rlztion;


import java.util.*;
import javax.vecmath.*;


import agdr.library.math.MathUtility;
import agdr.robodari.appl.*;
import agdr.robodari.comp.model.*;
import agdr.robodari.plugin.rme.store.*;
import agdr.robodari.store.*;
import static java.lang.Math.*;


/**
 * This world uses cm, g
 * @author root
 *
 */
public final class World implements java.io.Serializable, Runnable{
	public final static boolean W_DEBUG = BugManager.DEBUG && true;
	
	public final static double VARIANT_EPSILON = 4.0E-5;
	public final static int DEFAULT_TIMEINTERVAL = 9;
	public final static double MASS_CONVERSION = 0.001;
	private final static ForceGenerator[] NULLFGARRAY = new ForceGenerator[]{};
	private final static ObjectAdjustment[] NULLOAARRAY = new ObjectAdjustment[]{};
	private final static double[] ZERO3x3 = new double[]{
		0, 0, 0,
		0, 0, 0,
		0, 0, 0
	};
	private final static double[] INF3x3 = new double[]{
		Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY,
		Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY,
		Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY
	};
	
	
	private boolean isActivated = false;
	private boolean pause = false;
	// is run() working?
	private boolean running = false;
	private int timeInterval;
	private long elapsedTime;
	// timeRate = 2.0 : 1 sec elapsed in the real world -> 2 sec elapsed in the virtual world
	private double timeRate = 1.0;
	private boolean[] changed;
	
	private RealizationModel realizationModel;

	private Vector3d[] vels_nxt;
	private Vector3d[] vels_bef;
	private Vector3d[] ang_vels_nxt;
	private Vector3d[] ang_vels_bef;
	private Vector3d[] moms_nxt;
	private Vector3d[] moms_bef;
	private Vector3d[] ang_moms_nxt;
	private Vector3d[] ang_moms_bef;
	// Location of CM
	private Vector3d[] loc;
	private Vector3d[] loc_nxt;
	private Quat4d[] rot;
	private double[] rot_lenSqr;
	private Matrix3d[] rot_matrix;
	private Quat4d[] rot_nxt;
	private Matrix3d[] rot_nxt_matrix;
	private double[] rot_nxt_lenSqr;
	
	private Vector3d[] forces;
	private Vector3d[] accls;
	private Vector3d[] torques;
	private Vector3d[] angularAccls;

	private BoundObject[] bounds;
	
	private double[] masses;
	private double[] massesInv;
	private Matrix3d[] inertias_org;
	private Matrix3d[] inertias;
	private Matrix3d[] inrts_nxt;
	private Matrix3d[] inrtsInv;
	private Matrix3d[] inrtsInv_nxt;
	private boolean[] isInrtInfinite;
	private boolean[] isMassZero;
	
	private ForceGenerator[] forcegens;
	private ForceGenerator[] forcegens_lcp;
	private ObjectAdjustment[] objadjusts;
	private ContactModel contactModel;
	
	private Thread p_thread;
	private WorldListener worldListener;
	private EventCaster eventCaster;
	private EventCaster.Cast worldEventCaster = new EventCaster.Cast(){
		public void fireEvent(){
			worldListener.timeElapsed(realizationModel);
		}// end fireEvent()
	};
	
	
	
	
	private World(){}
	public World(RealizationModel rmodel)
	{ this(DEFAULT_TIMEINTERVAL, rmodel); }
	public World(int timeInterval, RealizationModel rmodel){
		this.timeInterval = timeInterval;
		this.realizationModel = rmodel;
		this.eventCaster = rmodel.getEventCaster();

		RealizationMap smap = realizationModel.getRealizationMap();
		
		loc = new Vector3d[smap.getModelCount()];
		loc_nxt = new Vector3d[loc.length];
		rot = new Quat4d[loc.length];
		rot_matrix = new Matrix3d[loc.length];
		rot_lenSqr = new double[loc.length];
		rot_nxt = new Quat4d[loc.length];
		rot_nxt_matrix = new Matrix3d[loc.length];
		rot_nxt_lenSqr = new double[loc.length];

		moms_bef = new Vector3d[loc.length];
		moms_nxt = new Vector3d[loc.length];
		vels_bef = new Vector3d[loc.length];
		vels_nxt = new Vector3d[loc.length];
		ang_moms_bef = new Vector3d[loc.length];
		ang_moms_nxt = new Vector3d[loc.length];
		ang_vels_bef = new Vector3d[loc.length];
		ang_vels_nxt = new Vector3d[loc.length];
		
		masses = new double[loc.length];
		massesInv = new double[loc.length];
		inertias_org = new Matrix3d[loc.length];
		inertias = new Matrix3d[loc.length];
		inrtsInv = new Matrix3d[loc.length];
		inrts_nxt = new Matrix3d[loc.length];
		inrtsInv_nxt = new Matrix3d[loc.length];
		isInrtInfinite = new boolean[loc.length];
		isMassZero = new boolean[loc.length];
		
		forces = new Vector3d[loc.length];
		accls = new Vector3d[loc.length];
		torques = new Vector3d[loc.length];
		angularAccls = new Vector3d[loc.length];
		
		bounds = new BoundObject[loc.length];
		
		for(int i = 0; i < loc.length; i++){
			moms_bef[i] = new Vector3d();
			moms_nxt[i] = new Vector3d();

			vels_bef[i] = new Vector3d();
			vels_nxt[i] = new Vector3d();
			
			ang_moms_bef[i] = new Vector3d();
			ang_moms_nxt[i] = new Vector3d();
			
			ang_vels_bef[i] = new Vector3d();
			ang_vels_nxt[i] = new Vector3d();
			
			torques[i] = new Vector3d();
			accls[i] = new Vector3d();
			forces[i] = new Vector3d();
			angularAccls[i] = new Vector3d();

			loc[i] = new Vector3d();
			rot[i] = new Quat4d();
			rot_matrix[i] = new Matrix3d();
			loc_nxt[i] = new Vector3d();
			rot_nxt[i] = new Quat4d();
			rot_nxt_matrix[i] = new Matrix3d();
			
			inertias_org[i] = new Matrix3d();
			inertias[i] = new Matrix3d();
			inrtsInv[i] = new Matrix3d();
			inrts_nxt[i] = new Matrix3d();
			inrtsInv_nxt[i] = new Matrix3d();
		}

		contactModel = new ContactModel();
		initData();
	}// end Constructor(int, float)
	
	
	
	
	
	public WorldListener getWorldListener()
	{ return worldListener; }
	public void setWorldListener(WorldListener wl)
	{ this.worldListener = wl; }
	
	
	public RealizationModel getRealizationModel()
	{ return realizationModel; }
	public int getTimeInterval()
	{ return timeInterval; }
	public void setTimeInterval(int ti){
		this.timeInterval = ti;
	}// end setTimeInterval(ti)
	
	public double getTimeRate()
	{ return this.timeRate; }
	public void setTimeRate(double tr)
	{ this.timeRate = tr; }
	
	public BoundObject getBound(int idx)
	{ return bounds[idx]; }
	
	public double getMass(int idx)
	{ return masses[idx]; }
	public double getMassInv(int idx)
	{ return massesInv[idx]; }
	
	/**
	 * returns mass(gram) * velocity(cm / s)
	 * @param idx
	 * @param vec
	 * @return
	 */
	public Vector3d getMomentum_nxt(int idx, Vector3d vec)
	{ vec.set(moms_nxt[idx]); return vec; }
	public Vector3d getMomentum_bef(int idx, Vector3d vec)
	{ vec.set(moms_bef[idx]); return vec; }
	public void setMomentum_bef(int idx, Vector3d vec)
	{ moms_bef[idx].set(vec); }
	
	public Vector3d getVelocity_nxt(int idx, Vector3d vec)
	{ vec.set(vels_nxt[idx]); return vec; }
	public Vector3d getVelocity_bef(int idx, Vector3d vec)
	{ vec.set(vels_bef[idx]); return vec; }
	
	public Vector3d getAngularMomentum_nxt(int idx, Vector3d vec)
	{ vec.set(ang_moms_nxt[idx]); return vec; }
	public Vector3d getAngularMomentum_bef(int idx, Vector3d vec)
	{ vec.set(ang_moms_bef[idx]); return vec; }
	public void setAngularMomentum_bef(int idx, Vector3d vec)
	{ ang_moms_bef[idx].set(vec); }
	
	
	public Vector3d getAngularVelocity_nxt(int idx, Vector3d vec)
	{ vec.set(ang_vels_nxt[idx]); return vec; }
	public Vector3d getAngularVelocity_bef(int idx, Vector3d vec)
	{ vec.set(ang_vels_bef[idx]); return vec; }
	

	public Matrix3d getInertiaTensor(int idx, Matrix3d m3f)
	{ m3f.set(inertias[idx]); return m3f; }
	public Matrix3d getInrtTensor_Inv(int idx, Matrix3d m3f)
	{ m3f.set(inrtsInv[idx]); return m3f; }
	
	
	public void getTransform_nxt(int structure, Matrix4d tr){
		tr.set(this.rot_nxt_matrix[structure]);
		tr.m03 = loc_nxt[structure].x;
		tr.m13 = loc_nxt[structure].y;
		tr.m23 = loc_nxt[structure].z;
		tr.m33 = 1;
	}// end getTransform_nxt
	
	public double getRotLengthSqr(int str)
	{ return rot_lenSqr[str]; }
	/*double _____len(Quat4d q){
		return q.w * q.w + q.x * q.x + q.y * q.y + q.z * q.z;
	}// end*/
	public void getTransformInverted(int str, Matrix4d tr){
		Matrix4d m = new Matrix4d();
		getTransform(str, m);
		MathUtility.fastInvert(m, tr);
	}

	public void getTransform(int structure, Matrix4d tr){
		tr.set(this.rot_matrix[structure]);
		tr.m03 = loc[structure].x;
		tr.m13 = loc[structure].y;
		tr.m23 = loc[structure].z;
		tr.m33 = 1;
	}// end getTransform
	
	public Vector3d getTranslation(int str, Vector3d tr)
	{ tr.set(this.loc[str]); return tr; }
	public void setTranslation(int str, Vector3d tr)
	{ this.loc[str].set(tr); }
	public Quat4d getRotation(int str, Quat4d q)
	{ q.set(this.rot[str]); return q; }
	public Matrix3d getRotMatrix(int str, Matrix3d matrix)
	{ matrix.set(this.rot_matrix[str]); return matrix; }
	
	public Vector3d getForce(int idx, Vector3d vec)
	{ vec.set(forces[idx]); return vec; }
	public Vector3d setForce(int idx, Vector3d vec)
	{ forces[idx].set(vec); return vec; }
	
	public Vector3d getTorque(int idx, Vector3d vec)
	{ vec.set(torques[idx]); return vec; }
	public Vector3d setTorque(int idx, Vector3d vec)
	{ torques[idx].set(vec); return vec; }
	
	public Vector3d getAcceleration(int idx, Vector3d vec)
	{ vec.set(accls[idx]); return vec; }
	public Vector3d getAngularAccl(int idx, Vector3d vec)
	{
		vec.set(angularAccls[idx]); return vec; }
	
	public long getElapsedTime()
	{ return elapsedTime; }
	
	public boolean isChanged(int idx)
	{ return changed[idx]; }
	
	
	public ContactModel getContactModel()
	{ return this.contactModel; }
	
	
	
	
	
	

	public boolean saidHello()
	{ return isActivated; }
	
	public void byeWorld() throws RealizationNotReadyException{
		if(!isActivated)
			throw new RealizationNotReadyException("Realization is" +
					" not started!");
		
		if(this.eventCaster.isStarted())
			this.eventCaster.stop();
		pause = true;
		isActivated = false;
		while(running){
			isActivated = false;
		}
	}// end stopWorld()
	
	public void pauseWorld() throws RealizationNotReadyException{
		if(!isActivated)
			throw new RealizationNotReadyException("Realization is" +
					" not started!");
		
		pause = true;
	}// end pauseWorld()
	
	public void resumeWorld() throws RealizationNotReadyException{
		if(!isActivated)
			throw new RealizationNotReadyException("Realization is" +
					" not started!");
		
		pause = false;
	}// end startWorld()
	
	public boolean isPaused()
	{ return pause; }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// START!
	public void startWorld()
			throws RealizationStartedException{
		if(isActivated)
			throw new RealizationStartedException("Realization" +
					" is already started!");
		BugManager.printf("World : startWOrld()\n");
		
		initData();
		isActivated = true;
		pause = false;
		
		_listForceGenerators();
		_listObjectAdjustments();
		changed = new boolean[realizationModel.getRealizationMap().getModelCount()];
		//momentums[0].set(30, 0, 0);
		
		p_thread = new Thread(this);
		p_thread.start();
	}// end helloWorld()
	
	public void resetWorld() throws RealizationStartedException{
		// correspond to this.byeWorld();
		BugManager.printf("World : resetWorld()\n");
		if(this.eventCaster.isStarted())
			this.eventCaster.stop();
		BugManager.printf("World : resetWorld : eventCaster stopped.\n");
		isActivated = false;
		// waiting...
		while(running);
		BugManager.printf("World : resetWorld : running stopped.\n");
		while(p_thread.getState() != Thread.State.TERMINATED);
		BugManager.printf("Wrd : resetW : restart!!!\n");
		pause = true;
		
		startWorld();
	}// end resetWorld()
	
	private void initData(){
		RealizationMap smap = realizationModel.getRealizationMap();
		
		this.elapsedTime = 0;
		RealizationMap.Key[] keys = smap.getKeys();
		if(W_DEBUG)
			BugManager.printf("World : initData : keys totalcnt : %d, model totalcnt : %d\n", keys.length, loc.length);
		for(RealizationMap.Key eachKey : keys){
			int idx = smap.getIndex(eachKey);
			if((BugManager.DEBUG && W_DEBUG)){
				BugManager.printf("World : initDat : keys[%d] : %s, owner[key] : \n", idx, eachKey);
				BugManager.printf("World : initDat :     %s\n", smap.getOwner(eachKey));
			}
				
			
			if(!smap.hasTransform(eachKey)){
				rot[idx].w = 1;
				rot[idx].x = rot[idx].y = rot[idx].z = 0;
			}else{
				Matrix4f tmptrans = new Matrix4f();
				smap.getInitialTransform(eachKey, tmptrans);
				loc[idx].x = tmptrans.m03;
				loc[idx].y = tmptrans.m13;
				loc[idx].z = tmptrans.m23;
				loc_nxt[idx].set(loc[idx]);
				
				rot_matrix[idx].set(new double[]{
						tmptrans.m00, tmptrans.m01, tmptrans.m02, 
						tmptrans.m10, tmptrans.m11, tmptrans.m12, 
						tmptrans.m20, tmptrans.m21, tmptrans.m22
				});
				rot_nxt_matrix[idx].set(rot_matrix[idx]);
				rot[idx].set(rot_matrix[idx]);
				rot_nxt[idx].set(rot[idx]);
				rot_lenSqr[idx] = rot[idx].x * rot[idx].x + rot[idx].y * rot[idx].y + rot[idx].z * rot[idx].z;
				rot_nxt_lenSqr[idx] = rot_lenSqr[idx];
			}
			
			PhysicalData pd = smap.getPhysicalData(eachKey);
			
			
			vels_bef[idx].set(0, 0, 0);
			vels_nxt[idx].set(0, 0, 0);
			ang_vels_bef[idx].set(0, 0, 0);
			ang_vels_nxt[idx].set(0, 0, 0);
			forces[idx].set(0, 0, 0);
			torques[idx].set(0, 0, 0);
			
			moms_bef[idx].set(0, 0, 0);
			moms_nxt[idx].set(0, 0, 0);
			ang_moms_bef[idx].set(0, 0, 0);
			ang_moms_nxt[idx].set(0, 0, 0);
			
			if(W_DEBUG)
				BugManager.printf("\tmass : %g\n", pd.mass);
			
			masses[idx] = pd.mass * MASS_CONVERSION;
			massesInv[idx] = 1.0 / masses[idx];
			
			if(pd.inertia != null){
				inertias_org[idx].set(pd.inertia);
				double checkdet = pd.inertia.determinant();
				
				if(Double.isInfinite(checkdet) || Double.isNaN(checkdet)){
					BugManager.printf("\t\t**INRT INF CHECKED!\n");
					inrtsInv[idx].set(ZERO3x3);
					inertias[idx].set(inertias_org[idx]);
					isInrtInfinite[idx] = true;
					isMassZero[idx] = false;
				}else if(masses[idx] == 0.0){
					isInrtInfinite[idx] = false;
					isMassZero[idx] = true;
					inrtsInv[idx].set(INF3x3);
					bounds[idx] = null;
				}else{
					MathUtility.trim(inertias[idx]);
					
					inertias[idx].set(rot_matrix[idx]);
					inertias[idx].mul(inertias_org[idx]);
					inertias[idx].mul(MASS_CONVERSION);
					this._empty_matrix3f_for_transposing_rot_nxt_matrix.set(rot_matrix[idx]);
					this._empty_matrix3f_for_transposing_rot_nxt_matrix.transpose();
					inertias[idx].mul(this._empty_matrix3f_for_transposing_rot_nxt_matrix);
					inrts_nxt[idx].set(inertias[idx]);
					
					inrtsInv[idx].set(inertias[idx]);
					inrtsInv[idx].invert();
					inrtsInv_nxt[idx].set(inrtsInv[idx]);

					isInrtInfinite[idx] = isMassZero[idx] = false;
				}
			}
			
			if(pd.bounds != null){
				bounds[idx] = pd.bounds.clone();
				bounds[idx].translate(-pd.centerMassPoint.x, -pd.centerMassPoint.y, -pd.centerMassPoint.z);
			}
		}
	}// end initData()
	
	


	private void _listObjectAdjustments(){
		ArrayList<ObjectAdjustment> oalist = new ArrayList<ObjectAdjustment>();
		ObjectAdjustment[] alloas = this.realizationModel.getRealizationMap().getObjectAdjustments();
		
		for(int i = 0; i < alloas.length; i++){
			if(!alloas[i].effective(realizationModel, realizationModel.getRealizationMap(), this))
				continue;
			alloas[i].init(this.realizationModel, realizationModel.getRealizationMap(), this);
			oalist.add(alloas[i]);
		}
		this.objadjusts = oalist.toArray(NULLOAARRAY);
	}// end _listObjectAdjustments()
	private void _listForceGenerators(){
		ArrayList<ForceGenerator> fglist = new ArrayList<ForceGenerator>();
		ArrayList<ForceGenerator> fglist_lcp = new ArrayList<ForceGenerator>();
		
		ForceGenerator[] allFGens = this.realizationModel.getRealizationMap().getForceGenerators();
		for(int i = 0; i < allFGens.length; i++){
			if(!allFGens[i].effective(realizationModel, realizationModel.getRealizationMap(), this))
				continue;
			
			allFGens[i].init(this.realizationModel, this.realizationModel.getRealizationMap(), this);
			
			if((allFGens[i].getGeneratorType() & ForceGenerator.LCP_NEEDED) == 0)
				fglist.add(allFGens[i]);
			else
				fglist_lcp.add(allFGens[i]);
		}
		
		forcegens = fglist.toArray(NULLFGARRAY);
		forcegens_lcp = fglist_lcp.toArray(NULLFGARRAY);
	}// end _listForceGenerators
	
	
	
	
	
	
	
	
	
	
	public strictfp void run(){
		try{
			System.out.println("------------- HELLO WORLD! -----------");
			RealizationMap smap = realizationModel.getRealizationMap();
			//byte[][] map = smap.getMap();
			long startTime = 0, endTime = 0;
			double deltaTime;
			
			if(!eventCaster.isStarted())
				eventCaster.start();
			
			running = true;
			this._printInfo();
			while(isActivated){
				deltaTime = timeInterval * 0.001;
				
				while(pause && isActivated){
					try{
						Thread.sleep(10);
					}catch(Exception e){ e.printStackTrace(); }
				}
				if(World.W_DEBUG)
					System.out.printf("run() : activatd : %b, pause : %b\n", isActivated, pause);
				if(!isActivated){
					running = false;
					break;
				}
				
				startTime = System.currentTimeMillis();
				BugManager.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ " + elapsedTime + "ms ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
				/*
				 * *** Generated forces from ForceGenerator is absolute-coordinates.
				 * *** Velocity is absolute-coordinates.
				 */
				

				RealizationCompStore.generateGraph(
						null, null, null, smap, this);
				
				int trial = 0;
				// flag가 true이면 계속 돈다
				boolean flag = true;
				//do{
					trial++;
					if(trial > 100) break;

					_clearForces();
					_clearChangedMarks();
					
					// 1. GENERAL CALCULATION
					
					_calcForces(smap);
					{
						_calcAccelerations();
						// calcMomentums : 
						// depends on : momentums_bef, ang_moms_bef, forces, torques
						// output : momentums_nxt
						_calcMomentums(deltaTime);
						// calcVelocities : 
						// depends on : moms_nxt, ang_moms_nxt
						// output : vels_nxt, ang_vels_nxt
						_calcVelocities();
						// _trimData : 
						// depends on, output : vels_nxt, ang_vels_nxt
						//_trimData(deltaTime);
						// calcLocation : 
						// depends on : velocities_nxt, ang_vels_nxt, forces, torques
						// output : loc_nxt, rot_nxt
						_calcLocation(smap, deltaTime);
					}
					_printInfo();

					// 2. CONTACT CALCULATION

					// clearcontact가 여기있는 이유 : force generator에서 contact를 사용할 수도 있다.
					_clearContact();
					// calcForces_LCP : 
					// depends on : velocities_bef, momentums_bef, forces, torques, transforms, trasforms_nxt
					// output : contacts
					_calcForces_LCP(smap);
					// calcContact : 
					// output : momentums_bef <= IMPORTANT!
					// momentums, ang_moms인 상황일 때 어느 정도의 반동을 줘야하는지 계산.
					// This updated momentums은 현재 관점에서 loc_nxt에 적용되어야 한다.
					flag = _calcContact();

					
					if(flag){
						_calcAccelerations();
						// calcMomentums : 
						// depends on : momentums_bef, ang_moms_bef, forces, torques
						// output : momentums_nxt
						_calcMomentums(deltaTime);
						// calcVelocities : 
						// depends on : moms_nxt, ang_moms_nxt
						// output : vels_nxt, ang_vels_nxt
						_calcVelocities();
						// _trimData : 
						// depends on, output : vels_nxt, ang_vels_nxt
						//_trimData(deltaTime);
						// calcLocation : 
						// depends on : velocities_nxt, ang_vels_nxt, forces, torques
						// output : loc_nxt, rot_nxt
						_calcLocation(smap, deltaTime);
					}
					
					_printInfo();
					// vels <- vels_nxt
					_cpyVelocities();
					// moms <- moms_nxt
					_cpyMomentums();
					// loc <- loc_nxt
					_cpyLocation();
				//}while(flag);

				// 3. OBJECT ADJUSTMENT
				
				_calcAdjustments(smap);
				
				
				elapsedTime += (int)(deltaTime * 1000.0);
				eventCaster.fire(this.worldEventCaster);
				endTime = System.currentTimeMillis();
				int tmpti = (int)(timeInterval / timeRate);
				if(tmpti == 0){
					if((BugManager.DEBUG && W_DEBUG))
						BugManager.printf("World : run() : elapedTime : %d, tmpti : %d\n", elapsedTime, 0);
				}else{
					if((BugManager.DEBUG && W_DEBUG))
						BugManager.printf("World : run() : elapedTime : %d, tmpti : %d, sleep time : %d\n", elapsedTime, tmpti, (tmpti - (endTime - startTime) % tmpti));
					try{
						Thread.sleep(tmpti - (endTime - startTime) % tmpti);
					}catch(Exception excp){}
				}
			}
		}catch(Exception excp){
			running = false;
			BugManager.log(excp, true);
		}
		running = false;
	}// end run()
	
	
	
	
	private void _printInfo(){
		if(!(BugManager.DEBUG && W_DEBUG))
			return;
		BugManager.println("------ _printInfo : ");
		
		for(int i = 0; i < loc.length; i++){
			BugManager.printf("%d : \n", i);
			BugManager.printf("  inertia : \n%s", this.inertias[i]);
			BugManager.printf("  inrtInv : \n%s", this.inrtsInv[i]);
			BugManager.printf("  mass : %s\n", this.masses[i]);
			BugManager.printf(
					"  boundobj : %s\n" + 
					"  torque : %s\n" + 
					"  accl : %s\n" +
					"  angularaccl : %s\n" +
					"  vel_bef : %s\n" + 
					"  vel_nxt : %s\n" + 
					"  ang_vel_bef : %s\n" +
					"  ang_vel_nxt : %s\n" +
					"  loc : %s\n" +
					"  loc_nxt : %s\n" + 
					"  rot : %s\n" +
					"  rot_nxt : %s\n", this.bounds[i], torques[i], accls[i], angularAccls[i],
					vels_bef[i], vels_nxt[i],
					ang_vels_bef[i], ang_vels_nxt[i],
					loc[i], loc_nxt[i],
					rot[i], rot_nxt[i]);
		}
	}// end _printInfo()
	
	
	private strictfp void _trimData(double deltaTime){
		double vel_adjepsilon = World.VARIANT_EPSILON / deltaTime,
				accl_adjepsilon = World.VARIANT_EPSILON / (deltaTime * deltaTime);
		
		for(int i = 0; i < vels_nxt.length; i++){
			if(-vel_adjepsilon < vels_nxt[i].x && vels_nxt[i].x < vel_adjepsilon)
				vels_nxt[i].x = 0;
			if(-vel_adjepsilon < vels_nxt[i].y && vels_nxt[i].y < vel_adjepsilon)
				vels_nxt[i].y = 0;
			if(-vel_adjepsilon < vels_nxt[i].z && vels_nxt[i].z < vel_adjepsilon)
				vels_nxt[i].z = 0;
			
			if(-vel_adjepsilon < ang_vels_nxt[i].x && ang_vels_nxt[i].x < vel_adjepsilon)
				ang_vels_nxt[i].x = 0;
			if(-vel_adjepsilon < ang_vels_nxt[i].y && ang_vels_nxt[i].y < vel_adjepsilon)
				ang_vels_nxt[i].y = 0;
			if(-vel_adjepsilon < ang_vels_nxt[i].z && ang_vels_nxt[i].z < vel_adjepsilon)
				ang_vels_nxt[i].z = 0;

			
			if(-accl_adjepsilon < accls[i].x && accls[i].x < accl_adjepsilon)
				accls[i].x = 0;
			if(-accl_adjepsilon < accls[i].y && accls[i].y < accl_adjepsilon)
				accls[i].y = 0;
			if(-accl_adjepsilon < accls[i].z && accls[i].z < accl_adjepsilon)
				accls[i].z = 0;
			
			if(-accl_adjepsilon < angularAccls[i].x && angularAccls[i].x < accl_adjepsilon)
				angularAccls[i].x = 0;
			if(-accl_adjepsilon < angularAccls[i].y && angularAccls[i].y < accl_adjepsilon)
				angularAccls[i].y = 0;
			if(-accl_adjepsilon < angularAccls[i].z && angularAccls[i].z < accl_adjepsilon)
				angularAccls[i].z = 0;
		}
	}// end _trimData()
	
	
	
	
	
	
	
	
	
	
	
	/* ********************************************
	 * 	               CONTACT
	 ****************************************/
	
	
	private void _calcForces_LCP(RealizationMap smap){
		if((BugManager.DEBUG && W_DEBUG))
			BugManager.println("------ World : FORCE GENERATION(LCP)");

		for(int i = 0; i < forcegens_lcp.length; i++)
			forcegens_lcp[i].generateForce();
	}// end _calcForces(Rea
	
	public final static boolean CONTACT_DEBUG = W_DEBUG && false;
	private strictfp boolean _calcContact(){
		if((BugManager.DEBUG && W_DEBUG))
			BugManager.printf("------ World : CONTACT CALC\n");
		if(contactModel.size() == 0)
			return false;
		
		ContactModel.Contact[] _cc_contacts = new ContactModel.Contact[contactModel.size()];
		double[][] A_col, A_noncol;
		int[] colidxs, noncolidxs,
				cncmap; // cncmap[i] = j that satisfies colcontacts[i] == noncolcontacts[j] or -1
		double[][] ran_col, rbn_col,
				ran_noncol, rbn_noncol;
		boolean flag = false;
		
		contactModel.resetPos();
		int i = 0;
		int colidxscnt = 0, noncolidxscnt = 0;
		
		// contacts의 초기화, collision contact갯수와 noncollision contact갯수 count
		while(contactModel.hasMoreElements()){
			_cc_contacts[i] = contactModel.nextElement();
			
			//if((BugManager.DEBUG && W_DEBUG) && CONTACT_DEBUG){
			//	BugManager.printf("calc ran, rbn_col : %d : type : %d, aid : %d, bid : %d\n", i, _cc_contacts[i].type, _cc_contacts[i].aid, _cc_contacts[i].bid);
			//	BugManager.printf("    contact : ap : %g %g %g\n", _cc_contacts[i].apx, _cc_contacts[i].apy, _cc_contacts[i].apz);
			//	BugManager.printf("    bp : %g %g %g\n", _cc_contacts[i].bpx, _cc_contacts[i].bpy, _cc_contacts[i].bpz);
			//	BugManager.printf("    n : %g %g %g\n", _cc_contacts[i].nx, _cc_contacts[i].ny, _cc_contacts[i].nz);
			//}
			
			if(_cc_contacts[i].type == ContactModel.Contact.TYPE_BOTH){
				colidxscnt++;
				noncolidxscnt++;
			}else if(_cc_contacts[i].type == ContactModel.Contact.TYPE_COLLISION)
				colidxscnt++;
			else
				noncolidxscnt++;
			
			i++;
		}
		
		// collision contact와 noncollision contact 대입, cncmap대입
		colidxs = new int[colidxscnt];
		noncolidxs = new int[noncolidxscnt];
		cncmap = new int[colidxscnt];
		colidxscnt = noncolidxscnt = 0;
		
		for(i = 0; i < _cc_contacts.length; i++){
			if(_cc_contacts[i].type == ContactModel.Contact.TYPE_BOTH){
				colidxs[colidxscnt] = i;
				noncolidxs[noncolidxscnt] = i;
				cncmap[colidxscnt] = noncolidxscnt;
				
				colidxscnt++;
				noncolidxscnt++;
			}else if(_cc_contacts[i].type == ContactModel.Contact.TYPE_COLLISION){
				cncmap[colidxscnt] = -1;
				colidxs[colidxscnt++] = i;
			}else{
				noncolidxs[noncolidxscnt++] = i;
			}
		}
		
		// ran_col rbn_col, ran_noncol, rbn_noncol 초기화
		ran_col = new double[colidxs.length][3];
		rbn_col = new double[colidxs.length][3];
		ran_noncol = new double[noncolidxs.length][3];
		rbn_noncol = new double[noncolidxs.length][3];
		
		for(i = 0; i < colidxs.length; i++){
			ran_col[i][0] =   _cc_contacts[colidxs[i]].apy * _cc_contacts[colidxs[i]].nz - _cc_contacts[colidxs[i]].apz * _cc_contacts[colidxs[i]].ny;
			ran_col[i][1] = -(_cc_contacts[colidxs[i]].apx * _cc_contacts[colidxs[i]].nz - _cc_contacts[colidxs[i]].apz * _cc_contacts[colidxs[i]].nx);
			ran_col[i][2] =   _cc_contacts[colidxs[i]].apx * _cc_contacts[colidxs[i]].ny - _cc_contacts[colidxs[i]].apy * _cc_contacts[colidxs[i]].nx;
			
			rbn_col[i][0] =   _cc_contacts[colidxs[i]].bpy * _cc_contacts[colidxs[i]].nz - _cc_contacts[colidxs[i]].bpz * _cc_contacts[colidxs[i]].ny;
			rbn_col[i][1] = -(_cc_contacts[colidxs[i]].bpx * _cc_contacts[colidxs[i]].nz - _cc_contacts[colidxs[i]].bpz * _cc_contacts[colidxs[i]].nx);
			rbn_col[i][2] =   _cc_contacts[colidxs[i]].bpx * _cc_contacts[colidxs[i]].ny - _cc_contacts[colidxs[i]].bpy * _cc_contacts[colidxs[i]].nx;
			
			if((BugManager.DEBUG && W_DEBUG) && CONTACT_DEBUG){
				BugManager.printf("    ran_col[%d] : %g %g %g\n", i, ran_col[i][0], ran_col[i][1], ran_col[i][2]);
				BugManager.printf("    rbn_col[%d] : %g %g %g\n", i, rbn_col[i][0], rbn_col[i][1], rbn_col[i][2]);
			}
			
			if(cncmap[i] != -1){
				ran_noncol[cncmap[i]] = ran_col[i];
				rbn_noncol[cncmap[i]] = rbn_col[i];
			}
		}
		for(i = 0; i < noncolidxs.length; i++){
			// 위에서 미리 계싼해 놓은 경우 취소
			if(ran_noncol[i][0] != 0.0 || ran_noncol[i][1] != 0.0 || ran_noncol[i][2] != 0.0 ||
					rbn_noncol[i][0] != 0.0 || rbn_noncol[i][1] != 0.0 || rbn_noncol[i][2] != 0.0){
				if((BugManager.DEBUG && W_DEBUG) && CONTACT_DEBUG)
					BugManager.printf("calc ran, rbn_noncol : %dth noncol contact is already calced\n", i);
				continue;
			}
			ran_noncol[i][0] =   _cc_contacts[noncolidxs[i]].apy * _cc_contacts[noncolidxs[i]].nz - _cc_contacts[noncolidxs[i]].apz * _cc_contacts[noncolidxs[i]].ny;
			ran_noncol[i][1] = -(_cc_contacts[noncolidxs[i]].apx * _cc_contacts[noncolidxs[i]].nz - _cc_contacts[noncolidxs[i]].apz * _cc_contacts[noncolidxs[i]].nx);
			ran_noncol[i][2] =   _cc_contacts[noncolidxs[i]].apx * _cc_contacts[noncolidxs[i]].ny - _cc_contacts[noncolidxs[i]].apy * _cc_contacts[noncolidxs[i]].nx;
			
			rbn_noncol[i][0] =   _cc_contacts[noncolidxs[i]].bpy * _cc_contacts[noncolidxs[i]].nz - _cc_contacts[noncolidxs[i]].bpz * _cc_contacts[noncolidxs[i]].ny;
			rbn_noncol[i][1] = -(_cc_contacts[noncolidxs[i]].bpx * _cc_contacts[noncolidxs[i]].nz - _cc_contacts[noncolidxs[i]].bpz * _cc_contacts[noncolidxs[i]].nx);
			rbn_noncol[i][2] =   _cc_contacts[noncolidxs[i]].bpx * _cc_contacts[noncolidxs[i]].ny - _cc_contacts[noncolidxs[i]].bpy * _cc_contacts[noncolidxs[i]].nx;
		}
		// A_col, A_noncol초기;
		A_col = new double[colidxs.length][colidxs.length];
		A_noncol = new double[noncolidxs.length][noncolidxs.length];
		_contact_computeMatrix(A_col, A_noncol, colidxs, noncolidxs, cncmap, _cc_contacts, 
				ran_col, rbn_col, ran_noncol, rbn_noncol);
		
		
		
		/* ***************** BENEATH HERE is STABLE. ***************************/
		
		///////////////// START CALCULATION!
		
		if(noncolidxs.length != 0){	
			BugManager.printf("-- noncol cont exists!! : %d\n", noncolidxs.length);
			// 비충돌 접촉
			// d dbldot = Ag + b
			// M <- A,
			// q <- b,n
			// w <- d dbldot,
			// z <- g
			double[] b = new double[noncolidxs.length];
			// answer
			double[] x = new double[noncolidxs.length];
			
			_contact_computeNoncolMatrix(b, noncolidxs, _cc_contacts);
			//int res = MathUtility.LCPSolve(g.length, A_noncol, b, g, ddotdot);
			
			if((BugManager.DEBUG && W_DEBUG) && CONTACT_DEBUG){
				BugManager.printf("  A : \n");
				for(i = 0; i < A_noncol.length; i++){
					for(int j = 0; j < A_noncol[i].length; j++)
						BugManager.printf("%g ", A_noncol[i][j]);
					BugManager.println();
				}
				BugManager.printf("  b : \n");
				for(i = 0; i < b.length; i++)
					BugManager.printf("%g\n", b[i]);
				//for(i = 0; i < ddotdot.length; i++)
				//	BugManager.printf("%g\n", ddotdot[i]);
				
				//BugManager.printf("result of lcp solv : %d\n", res);
			}
			
			_solve(A_noncol, x, b);
			
			ContactModel.Contact ci;
			for(i = 0; i < x.length; i++){
				ci = _cc_contacts[noncolidxs[i]];
				if((BugManager.DEBUG && W_DEBUG) && CONTACT_DEBUG){
					BugManager.printf("%d : normal : %g %g %g, size : %g\n", i, ci.nx, ci.ny, ci.nz, x[i]);
					BugManager.printf("   ap : %g %g %g, bp : %g %g %g\n", ci.apx, ci.apy, ci.apz, ci.bpx, ci.bpy, ci.bpz);
					BugManager.printf("   before forces[%d] : %s\n", ci.aid, forces[ci.aid]);
					BugManager.printf("   before forces[%d] : %s\n", ci.bid, forces[ci.bid]);
				}
				
				ci.result_force = x[i];
				
				forces[ci.aid].x += ci.nx * x[i];
				forces[ci.aid].y += ci.ny * x[i];
				forces[ci.aid].z += ci.nz * x[i];
				
				torques[ci.aid].x += x[i] * (ci.apy * ci.nz - ci.apz * ci.ny);
				torques[ci.aid].y -= x[i] * (ci.apx * ci.nz - ci.apz * ci.nx);
				torques[ci.aid].z += x[i] * (ci.apx * ci.ny - ci.apy * ci.nx);
				
				forces[ci.bid].x += -ci.nx * x[i];
				forces[ci.bid].y += -ci.ny * x[i];
				forces[ci.bid].z += -ci.nz * x[i];
				
				torques[ci.bid].x += -x[i] * (ci.bpy * ci.nz - ci.bpz * ci.ny);
				torques[ci.bid].y -= -x[i] * (ci.bpx * ci.nz - ci.bpz * ci.nx);
				torques[ci.bid].z += -x[i] * (ci.bpx * ci.ny - ci.bpy * ci.nx);
				
				if((BugManager.DEBUG && W_DEBUG) && CONTACT_DEBUG){
					BugManager.printf("   after torques[%d] : %s\n", ci.aid, torques[ci.aid]);
					BugManager.printf("   after torques[%d] : %s\n", ci.bid, torques[ci.bid]);
					BugManager.printf("   after forces[%d] : %s\n", ci.aid, forces[ci.aid]);
					BugManager.printf("   after forces[%d] : %s\n", ci.bid, forces[ci.bid]);
				}
			}
			flag = true;
		}
		
		
		if(colidxs.length != 0){
			if((BugManager.DEBUG && W_DEBUG))
				BugManager.printf("-- col cont exists!!:%d\n", colidxs.length);
			if((BugManager.DEBUG && W_DEBUG) && CONTACT_DEBUG){
				
				BugManager.printf("  A : \n");
				for(i = 0; i < A_col.length; i++){
					for(int j = 0; j < A_col[i].length; j++)
						BugManager.printf("%g ", A_col[i][j]);
					BugManager.println();
				}
			}
			// 충돌 접촉
			// d dot plus = Af + d dot minus,
			// delta d dot = Af
			// f = (A^(-1)) (delta d dot)
			
			double[] x = new double[A_col.length];
			double[] deltaddot = new double[colidxs.length];
			_contact_computeColMatrix(deltaddot, colidxs, _cc_contacts);

			_solve(A_col, x, deltaddot);
			
			ContactModel.Contact ci;
			for(i = 0; i < x.length; i++){
				if((BugManager.DEBUG && W_DEBUG) && CONTACT_DEBUG)
					BugManager.printf("%d : impulse : %g\n", i, x[i]);
				
				ci = _cc_contacts[colidxs[i]];
				ci.result_impulse = x[i];
				
				// OUTPUT is _bef!
				moms_bef[ci.aid].x += ci.nx * x[i];
				moms_bef[ci.aid].y += ci.ny * x[i];
				moms_bef[ci.aid].z += ci.nz * x[i];
				
				// r cross P
				ang_moms_bef[ci.aid].x += x[i] * (ci.apy * ci.nz - ci.apz * ci.ny);
				ang_moms_bef[ci.aid].y -= x[i] * (ci.apx * ci.nz - ci.apz * ci.nx);
				ang_moms_bef[ci.aid].z += x[i] * (ci.apx * ci.ny - ci.apy * ci.nx);
				
				moms_bef[ci.bid].x += -ci.nx * x[i];
				moms_bef[ci.bid].y += -ci.ny * x[i];
				moms_bef[ci.bid].z += -ci.nz * x[i];
				
				// r cross P
				ang_moms_bef[ci.bid].x += -x[i] * (ci.bpy * ci.nz - ci.bpz * ci.ny);
				ang_moms_bef[ci.bid].y -= -x[i] * (ci.bpx * ci.nz - ci.bpz * ci.nx);
				ang_moms_bef[ci.bid].z += -x[i] * (ci.bpx * ci.ny - ci.bpy * ci.nx);
				
				if((BugManager.DEBUG && W_DEBUG) && CONTACT_DEBUG){
					BugManager.printf("%d : after col : normal : %g %g %g\n", i, ci.nx, ci.ny, ci.nz);
					BugManager.printf("  %d : momentum : %s\n", ci.aid, moms_bef[ci.aid]);
					BugManager.printf("  %d : momentum : %s\n", ci.bid, moms_bef[ci.bid]);
					BugManager.printf("  %d : ang_mom : %s\n", ci.aid, ang_moms_bef[ci.aid]);
					BugManager.printf("  %d : ang_mom : %s\n", ci.bid, ang_moms_bef[ci.bid]);
				}
			}
			flag = true;
			deltaddot = null;
		}
		
		_cc_contacts = null;
		colidxs = null;
		noncolidxs = null;
		
		ran_col = null;
		rbn_col = null;
		ran_noncol = null;
		rbn_noncol = null;
		
		return flag;
	}// end
		// STABLE CODE
		private strictfp void _contact_computeColMatrix(double[] deltaddot, int[] colidxs, ContactModel.Contact[] contacts){
			for(int i = 0; i < colidxs.length; i++){
				// contact 하나당 점 하나를 담당한다.
				if((BugManager.DEBUG && W_DEBUG)  && CONTACT_DEBUG)
					BugManager.printf("_compColMtrx : contacts[%d] : \n", i);
				
				deltaddot[i] = -contacts[colidxs[i]].currDDot * (1.0 + contacts[colidxs[i]].coefOfRestt);
				if((BugManager.DEBUG && W_DEBUG) && CONTACT_DEBUG)
					BugManager.printf("  ci.currDDot : %g\n", contacts[colidxs[i]].currDDot);
			}
		}// end _computeColMatrix
		// STABLE CODE
		private strictfp void _contact_computeNoncolMatrix(double[] b, int[] noncolidxs, ContactModel.Contact[] contacts){
			ContactModel.Contact ci;
			
			for(int i = 0; i < noncolidxs.length; i++){
				if((BugManager.DEBUG && W_DEBUG) && CONTACT_DEBUG)
					BugManager.printf("_computeNoncolMatrix : %d : \n", i);
				// contact 하나당 점 하나를 담당한다.
				ci = contacts[noncolidxs[i]];
				
				// b += relAccl dot n.
				b[i] += -ci.currDDotDot;
			}
			ci = null;
			if((BugManager.DEBUG && W_DEBUG) && CONTACT_DEBUG)
				BugManager.printf("_computeNoncolMatrix end\n");
		}// end _computeNoncolMatrix
		
		
		// STABLE CODE
		private strictfp void _contact_computeMatrix(double[][] A_col, double[][] A_noncol,
				int[] colidxs, int[] noncolidxs, int[] cncmap,
				ContactModel.Contact[] contacts,
				double[][] ran_col, double[][] rbn_col, double[][] ran_noncol, double[][] rbn_noncol){
			/*
			 * 
				BugManager.printf("_computeMtrx : %s, %s : \n" +
						"  ci : aid, bid : %d, %d, cj : aid, bid : %d, %d\n" +
						"  ci.n : %g %g %g, cj.n : %g %g %g\n" + 
						"  ci.ap : %g %g %g, ci.bp : %g %g %g\n" + 
						"  i_r-an : %g %g %g, i_r-bn : %g %g %g\n" +
						"  cj.ap : %g %g %g, cj.bp : %g %g %g\n" +
						"  j_r-an : %g %g %g, j_r-bn : %g %g %g\n",
						ci, cj, 
						ci.aid, ci.bid, cj.aid, cj.bid,
						ci.nx, ci.ny, ci.nz, cj.nx, cj.ny, cj.nz,
						ci.apx, ci.apy, ci.apz, ci.bpx, ci.bpy, ci.bpz,
						ranix, raniy, raniz, rbnix, rbniy, rbniz,
						cj.apx, cj.apy, cj.apz, cj.bpx, cj.bpy, cj.bpz,
						ranjx, ranjy, ranjz, rbnjx, rbnjy, rbnjz);
			 */
			ContactModel.Contact ci, cj;
			
			for(int i = 0; i < colidxs.length; i++){
				ci = contacts[colidxs[i]];
				for(int j = 0; j <= i; j++){
					cj = contacts[colidxs[j]];
					
					A_col[i][j] = 0;
					
					_contact_computeMatrixElement(A_col, i, j, ran_col, rbn_col, ci, cj);
					if(cncmap[i] != -1 && cncmap[j] != -1){
						A_noncol[cncmap[i]][cncmap[j]] = A_col[i][j];
					}
				}
			}
			for(int i = 0; i < colidxs.length; i++)
				for(int j = i + 1; j < colidxs.length; j++)
					A_col[i][j] = A_col[j][i];
			
			for(int i = 0; i < noncolidxs.length; i++){
				ci = contacts[noncolidxs[i]];
				for(int j = 0; j <= i; j++){
					cj = contacts[noncolidxs[j]];
					
					if(A_noncol[i][j] != 0.0)
						continue;
					_contact_computeMatrixElement(A_noncol, i, j, ran_noncol, rbn_noncol, ci, cj);
				}
			}
			for(int i = 0; i < noncolidxs.length; i++)
				for(int j = i + 1; j < noncolidxs.length; j++)
					A_noncol[i][j] = A_noncol[j][i];
		}// end _computeMatrix
		// STABLE CODE
		private strictfp void _contact_computeMatrixElement(double[][] A, int i, int j,
				double[][] ran, double[][] rbn,
				ContactModel.Contact ci, ContactModel.Contact cj){
			if((BugManager.DEBUG && W_DEBUG) && CONTACT_DEBUG)
				BugManager.printf("_computeMatrixElem : A[%d, %d] : ci.aid : %d, bid : %d, cj.aid : %d, bid : %d\n", i, j, ci.aid, ci.bid, cj.aid, cj.bid);
			if(ci.aid == cj.aid){
				A[i][j] += massesInv[ci.aid] * (ci.nx * cj.nx + ci.ny * cj.ny + ci.nz * cj.nz)
						+ (  ran[i][0] * 
									(inrtsInv[ci.aid].m00 * ran[j][0] + inrtsInv[ci.aid].m01 * ran[j][1] + inrtsInv[ci.aid].m02 * ran[j][2])
							+ ran[i][1] * 
									(inrtsInv[ci.aid].m10 * ran[j][0] + inrtsInv[ci.aid].m11 * ran[j][1] + inrtsInv[ci.aid].m12 * ran[j][2])
							+ ran[i][2] * 
									(inrtsInv[ci.aid].m20 * ran[j][0] + inrtsInv[ci.aid].m21 * ran[j][1] + inrtsInv[ci.aid].m22 * ran[j][2]));
			}else if(ci.aid == cj.bid){
				A[i][j] -= massesInv[ci.aid] * (ci.nx * cj.nx + ci.ny * cj.ny + ci.nz * cj.nz)
						+ (  ran[i][0] * 
									(inrtsInv[ci.aid].m00 * ran[j][0] + inrtsInv[ci.aid].m01 * ran[j][1] + inrtsInv[ci.aid].m02 * ran[j][2])
							+ ran[i][1] * 
									(inrtsInv[ci.aid].m10 * ran[j][0] + inrtsInv[ci.aid].m11 * ran[j][1] + inrtsInv[ci.aid].m12 * ran[j][2])
							+ ran[i][2] * 
									(inrtsInv[ci.aid].m20 * ran[j][0] + inrtsInv[ci.aid].m21 * ran[j][1] + inrtsInv[ci.aid].m22 * ran[j][2]));
			}
			if((BugManager.DEBUG && W_DEBUG)  && CONTACT_DEBUG)
				BugManager.printf("  mid-way val : %g\n", A[i][j]);
			
			if(ci.bid == cj.aid){
				A[i][j] -= massesInv[ci.bid] * (ci.nx * cj.nx + ci.ny * cj.ny + ci.nz * cj.nz)
						+ (  rbn[i][0] *
									(inrtsInv[ci.bid].m00 * rbn[j][0] + inrtsInv[ci.bid].m01 * rbn[j][1] + inrtsInv[ci.bid].m02 * rbn[j][2])
							+ rbn[i][1] *
									(inrtsInv[ci.bid].m10 * rbn[j][0] + inrtsInv[ci.bid].m11 * rbn[j][1] + inrtsInv[ci.bid].m12 * rbn[j][2])
							+ rbn[i][2] *
									(inrtsInv[ci.bid].m20 * rbn[j][0] + inrtsInv[ci.bid].m21 * rbn[j][1] + inrtsInv[ci.bid].m22 * rbn[j][2]));
			}else if(ci.bid == cj.bid){
				A[i][j] += massesInv[ci.bid] * (ci.nx * cj.nx + ci.ny * cj.ny + ci.nz * cj.nz)
						+ (  rbn[i][0] * 
									(inrtsInv[ci.bid].m00 * rbn[j][0] + inrtsInv[ci.bid].m01 * rbn[j][1] + inrtsInv[ci.bid].m02 * rbn[j][2])
							+ rbn[i][1] *
									(inrtsInv[ci.bid].m10 * rbn[j][0] + inrtsInv[ci.bid].m11 * rbn[j][1] + inrtsInv[ci.bid].m12 * rbn[j][2])
							+ rbn[i][2] *
									(inrtsInv[ci.bid].m20 * rbn[j][0] + inrtsInv[ci.bid].m21 * rbn[j][1] + inrtsInv[ci.bid].m22 * rbn[j][2]));
			}
			if((BugManager.DEBUG && W_DEBUG) && CONTACT_DEBUG)
				BugManager.printf("  value : %g\n", A[i][j]);
		}// end _computeMatrixElement

	
		
	/* *****************************************
	 *             CLEAR FUNCTIONS
	 ***************************************/
		

	private void _clearForces(){
		if((BugManager.DEBUG && W_DEBUG))
			BugManager.println("------ World : clear Force");
		for(int i = 0; i < forces.length; i++){
			forces[i].set(0, 0, 0);
			torques[i].set(0, 0, 0);
		}
	}// end clearForce()
	private void _clearChangedMarks(){
		for(int i = 0; i < changed.length; i++)
			changed[i] = false;
	}// end _clearChanges()
	private void _clearContact(){
		this.contactModel.clear();
	}// end _clearContact
		
		
	
	
	
	
	/* ********************************************
	 *            OBJECT ADJUSTMENT
	 *********************************************/ 
	
	
	private strictfp void _calcAdjustments(RealizationMap rmap){
		if((BugManager.DEBUG && W_DEBUG))
			BugManager.println("------ World : CALC ADJUSTMENTS");
		for(int i = 0; i < this.objadjusts.length; i++)
			this.objadjusts[i].adjust();
	}// end _calcAdjustmenets
	
		
		
		
		
	
	
	
	
	
	/* **********************************************
	 *           BASIC COMPONENTS
	 * ******************************************/
	
	private void _calcForces(RealizationMap smap){
		if((BugManager.DEBUG && W_DEBUG))
			BugManager.println("------ World : FORCE GENERATION");

		for(int i = 0; i < forcegens.length; i++)
			forcegens[i].generateForce();
	}// end _calcForce
	private strictfp void _calcVelocities(){
		if(BugManager.DEBUG && W_DEBUG)
			BugManager.println("------ World : VELOCITY CALC");
		
		for(int i = 0; i < ang_vels_nxt.length; i++){
			if(isMassZero[i]){
				vels_nxt[i].set(0, 0, 0);
				ang_vels_nxt[i].set(0, 0, 0);
			}else{
				vels_nxt[i].x = massesInv[i] * moms_nxt[i].x;
				vels_nxt[i].y = massesInv[i] * moms_nxt[i].y;
				vels_nxt[i].z = massesInv[i] * moms_nxt[i].z;

				ang_vels_nxt[i].x = inrtsInv[i].m00 * ang_moms_nxt[i].x + inrtsInv[i].m01 * ang_moms_nxt[i].y + inrtsInv[i].m02 * ang_moms_nxt[i].z;
				ang_vels_nxt[i].y = inrtsInv[i].m10 * ang_moms_nxt[i].x + inrtsInv[i].m11 * ang_moms_nxt[i].y + inrtsInv[i].m12 * ang_moms_nxt[i].z;
				ang_vels_nxt[i].z = inrtsInv[i].m20 * ang_moms_nxt[i].x + inrtsInv[i].m21 * ang_moms_nxt[i].y + inrtsInv[i].m22 * ang_moms_nxt[i].z;
			}
			
			if(BugManager.DEBUG && W_DEBUG)
			BugManager.printf("vel_bef[%d] : %s, velocities_nxt[%d] : %s, angvel_bef[%d] : %s, ang_vels[%d] : %s\n",
					i, vels_bef[i], i, vels_nxt[i],
					i, ang_vels_bef[i], i, ang_vels_nxt[i]);
		}
	}// end _calcVels
	private strictfp void _calcMomentums(double deltaTime){
		if(BugManager.DEBUG && W_DEBUG)
			BugManager.println("------ World : MOMENTUM UPDATE");
		
		for(int i = 0; i < forces.length; i++){
			moms_nxt[i].x = moms_bef[i].x + forces[i].x * deltaTime;
			moms_nxt[i].y = moms_bef[i].y + forces[i].y * deltaTime;
			moms_nxt[i].z = moms_bef[i].z + forces[i].z * deltaTime;

			ang_moms_nxt[i].x = ang_moms_bef[i].x + torques[i].x * deltaTime;
			ang_moms_nxt[i].y = ang_moms_bef[i].y + torques[i].y * deltaTime;
			ang_moms_nxt[i].z = ang_moms_bef[i].z + torques[i].z * deltaTime;
			
			/*
			BugManager.printf("moms_nxt[%d] : %s, forces[%d] : %s\n", i, moms_nxt[i], i, forces[i]);
			BugManager.printf("ang_moms_nxt[%d] : %s, torques[%d] : %s\n", i, ang_moms_nxt[i], i, torques[i]);
			*/
		}
	}// end _calcVel
	private strictfp void _calcAccelerations(){
		if(BugManager.DEBUG && W_DEBUG)
			BugManager.println("------ World : ACCELERATION CALC");
		
		for(int i = 0; i < forces.length; i++){
			if(isMassZero[i]){
				accls[i].set(0, 0, 0);
				angularAccls[i].set(0, 0, 0);
			}else{
				accls[i].x = massesInv[i] * forces[i].x;
				accls[i].y = massesInv[i] * forces[i].y;
				accls[i].z = massesInv[i] * forces[i].z;
				
				angularAccls[i].x = inrtsInv[i].m00 * torques[i].x
									+ inrtsInv[i].m01 * torques[i].y
									+ inrtsInv[i].m02 * torques[i].z;
				angularAccls[i].y = inrtsInv[i].m10 * torques[i].x
									+ inrtsInv[i].m11 * torques[i].y
									+ inrtsInv[i].m12 * torques[i].z;
				angularAccls[i].z = inrtsInv[i].m20 * torques[i].x
									+ inrtsInv[i].m21 * torques[i].y
									+ inrtsInv[i].m22 * torques[i].z;
			}
			if((BugManager.DEBUG && W_DEBUG))
				BugManager.printf("angularAccls[%d] : %s\n", i, angularAccls[i]);
		}
	}// end _calcAcceleration
	/* 
	 * 다음 장면의 좌표를 *_nxt 배열에 넣는다.
	 */
	Matrix3d _empty_matrix3f_for_transposing_rot_nxt_matrix = new Matrix3d();
	private strictfp void _calcLocation (RealizationMap rmap, double deltaTime){
		if(BugManager.DEBUG && W_DEBUG)
			BugManager.println("------ World : LOCATION CALC");
		double s = 0, c = 0;
		double acclrate = -deltaTime / 2.0,
				aarate = -deltaTime / 2.0;
		double dsx, dsy, dsz, dax, day, daz, dalensqr, dalen;
		
		for(int i = 0; i < loc.length; i++){
			if((-VARIANT_EPSILON < vels_nxt[i].x && vels_nxt[i].x < VARIANT_EPSILON)
					&& (-VARIANT_EPSILON < vels_nxt[i].y && vels_nxt[i].y < VARIANT_EPSILON)
					&& (-VARIANT_EPSILON < vels_nxt[i].z && vels_nxt[i].z < VARIANT_EPSILON)
					&& (-VARIANT_EPSILON < ang_vels_nxt[i].x && ang_vels_nxt[i].x < VARIANT_EPSILON)
					&& (-VARIANT_EPSILON < ang_vels_nxt[i].y && ang_vels_nxt[i].y < VARIANT_EPSILON)
					&& (-VARIANT_EPSILON < ang_vels_nxt[i].z && ang_vels_nxt[i].z < VARIANT_EPSILON)
					&& (-VARIANT_EPSILON < forces[i].x && forces[i].x < VARIANT_EPSILON)
					&& (-VARIANT_EPSILON < forces[i].y && forces[i].y < VARIANT_EPSILON)
					&& (-VARIANT_EPSILON < forces[i].z && forces[i].z < VARIANT_EPSILON)
					&& (-VARIANT_EPSILON < torques[i].x && torques[i].x < VARIANT_EPSILON)
					&& (-VARIANT_EPSILON < torques[i].y && torques[i].y < VARIANT_EPSILON)
					&& (-VARIANT_EPSILON < torques[i].z && torques[i].z < VARIANT_EPSILON)){
				loc_nxt[i].set(loc[i]);
				rot_nxt[i].set(rot[i]);
				rot_nxt_lenSqr[i] = rot_lenSqr[i];
				rot_nxt_matrix[i].set(rot_matrix[i]);
				inrts_nxt[i].set(inertias[i]);
				inrtsInv_nxt[i].set(inrtsInv[i]);
				changed[i] = false;
				continue;
			}
			
			changed[i] = true;
			dsx = vels_nxt[i].x + acclrate * accls[i].x;
			dsy = vels_nxt[i].y + acclrate * accls[i].y;
			dsz = vels_nxt[i].z + acclrate * accls[i].z;
			loc_nxt[i].x = loc[i].x + deltaTime * dsx;
			loc_nxt[i].y = loc[i].y + deltaTime * dsy;
			loc_nxt[i].z = loc[i].z + deltaTime * dsz;
			
			dax = ang_vels_nxt[i].x + angularAccls[i].x * aarate;
			day = ang_vels_nxt[i].y + angularAccls[i].y * aarate;
			daz = ang_vels_nxt[i].z + angularAccls[i].z * aarate;

			dalensqr = dax * dax + day * day + daz * daz;
			
			if(-World.VARIANT_EPSILON * World.VARIANT_EPSILON <= dalensqr
					&& dalensqr <= World.VARIANT_EPSILON * World.VARIANT_EPSILON){
				rot_nxt[i].set(rot[i]);
				rot_nxt_lenSqr[i] = rot_lenSqr[i];
				rot_nxt_matrix[i].set(rot_matrix[i]);
				inrts_nxt[i].set(inertias[i]);
				inrtsInv_nxt[i].set(inrtsInv[i]);
			}else{
				dalen = Math.sqrt(dalensqr);
				dax /= dalen;
				day /= dalen;
				daz /= dalen;
				
				s = sin(dalen * deltaTime);
				c = cos(dalen * deltaTime);
				
				if((BugManager.DEBUG && W_DEBUG))
					BugManager.printf("World : _calcLoc : \n\taxis : " + dax + " , " + day + " , " + daz + " , \n\taxislen : %g\n",
							dalen);
				
				// rot_nxt = q rot(q = c + vec s)
				rot_nxt[i].w = c*rot[i].w - dax*rot[i].x*s - day*rot[i].y*s - daz*rot[i].z*s;
				rot_nxt[i].x = c*rot[i].x + dax*rot[i].w*s + day*rot[i].z*s - daz*rot[i].y*s;
				rot_nxt[i].y = c*rot[i].y - dax*rot[i].z*s + day*rot[i].w*s + daz*rot[i].x*s;
				rot_nxt[i].z = c*rot[i].z + dax*rot[i].y*s - day*rot[i].x*s + daz*rot[i].w*s;
				
				rot_nxt_lenSqr[i] = rot_nxt[i].x * rot_nxt[i].x + rot_nxt[i].y * rot_nxt[i].y + rot_nxt[i].z * rot_nxt[i].z;
				rot_nxt_matrix[i].set(rot_nxt[i]);
				
				if(W_DEBUG)
					BugManager.printf("%d : inertias : \n%s, inrtsInv : \n%s", i, inertias[i], inrtsInv[i]);
				
				if(isInrtInfinite[i] || isMassZero[i]){
					inrts_nxt[i].set(inertias[i]);
					inrtsInv_nxt[i].set(inrtsInv[i]);
				}else{
					inrts_nxt[i].set(rot_nxt_matrix[i]);
					inrts_nxt[i].mul(inertias_org[i]);
					inrts_nxt[i].mul(MASS_CONVERSION);
					_empty_matrix3f_for_transposing_rot_nxt_matrix.set(rot_nxt_matrix[i]);
					_empty_matrix3f_for_transposing_rot_nxt_matrix.transpose();
					inrts_nxt[i].mul(_empty_matrix3f_for_transposing_rot_nxt_matrix);
					inrtsInv_nxt[i].set(inrts_nxt[i]);
					inrtsInv_nxt[i].invert();
				}
			}
			/*
			BugManager.printf("%d : rot : %g %g %g %g\n",
					i, rot[i].x, rot[i].y, rot[i].z, rot[i].w);
			BugManager.printf("  rot_nxt : %g %g %g %g\n" +
					"  rot_nxt_matrix : \n   loc : %s\n  loc_nxt : %s\n",
					  rot_nxt[i].x, rot_nxt[i].y, rot_nxt[i].z, rot_nxt[i].w,
					  loc[i], loc_nxt[i]);
			BugManager.printf("  velocities[%d] : %s, forces[%d] : %s\n", i, vels_nxt[i], i, forces[i]);
			*/
		}
	}// end _calcLocation
	
	

	
	
	private void _cpyLocation (){
		if(BugManager.DEBUG && W_DEBUG)
			BugManager.println("------ World : LOCATION CPY");
		Vector3d[] tmptrn = this.loc;
		Quat4d[] tmprot = this.rot;
		double[] tmprotlenSqr = this.rot_lenSqr;
		Matrix3d[] tmprotmatrix = this.rot_matrix;
		
		this.loc = this.loc_nxt;
		this.rot = this.rot_nxt;
		this.rot_lenSqr = this.rot_nxt_lenSqr;
		this.loc_nxt = tmptrn;
		this.rot_nxt = tmprot;
		this.rot_nxt_lenSqr = tmprotlenSqr;
		this.rot_matrix = this.rot_nxt_matrix;
		this.rot_nxt_matrix = tmprotmatrix;
	}// end _cpyLoc
	private void _cpyMomentums(){
		if(BugManager.DEBUG && W_DEBUG)
			BugManager.println("------ World : CPY MOMENTUMS");
		
		Vector3d[] tmp = this.moms_bef;
		Vector3d[] tmpang = this.ang_moms_bef;
		Matrix3d[] tmpinrts = this.inrts_nxt;
		Matrix3d[] tmpinrtsinv = this.inrtsInv_nxt;
		
		this.moms_bef = this.moms_nxt;
		this.moms_nxt = tmp;
		
		this.ang_moms_bef = this.ang_moms_nxt;
		this.ang_moms_nxt = tmpang;
		
		this.inrts_nxt = this.inertias;
		this.inertias = tmpinrts;
		
		this.inrtsInv_nxt = this.inrtsInv;
		this.inrtsInv = tmpinrtsinv;
	}// end _cpyMomentums
	private void _cpyVelocities(){
		BugManager.println("------ World : CPY VELOCITIES");
		Vector3d[] tmpv = this.vels_bef;
		Vector3d[] tmpav = this.ang_vels_bef;
		
		this.vels_bef = this.vels_nxt;
		this.vels_nxt = tmpv;
		
		this.ang_vels_bef = this.ang_vels_nxt;
		this.ang_vels_nxt = tmpav;
	}// end _cpyVel
	
	
	
	
	
	/* *******************************************************************
	 *                 MATHEMATIC TOOLS
	 ***********************************************************************/
	final static boolean SOLVE_DEBUG = false && W_DEBUG;
	
	@StableCode private strictfp void _solve(double[][] A, double[] x, double[] b){
		if(A.length == 0)
			return;
		
		if((BugManager.DEBUG && W_DEBUG) && SOLVE_DEBUG)
			BugManager.printf("START World : _solve()\n");
		// Ax = b,
		// M A N N^-1 x = M b
		// N^-1 x = M b
		// x = N M b
		
		int i = 0, j = 0;
		for(i = 0; i < A.length; i++){
			for(j = 0; j < A[i].length; j++)
				A[i][j] *= 1000.0;
			b[i] *= 1000.0;
		}
		double epsilon = 10e-11;
		double epsilon2 = 10e-1;
		
		double[][] M = new double[A.length][A.length],
				N = new double[A.length][A.length];
		// q[i] : i번째 변수를 0으로 처리하는가?
		boolean[] q = new boolean[A.length];
		for(i = 0; i < M.length; i++)
			M[i][i] = 1;
		for(i = 0; i < N.length; i++)
			N[i][i] = 1;
		
		_solve_elimination(A, M, N, q, epsilon,epsilon2);
		
		if((BugManager.DEBUG && W_DEBUG) && SOLVE_DEBUG){
			BugManager.printf("q(after modified-GaussElim) : \n");
			for(i = 0; i < q.length; i++)
				BugManager.printf("%b ", q[i]);
			BugManager.println();
			BugManager.printf("A_inv : \n");
			for(i = 0; i < M.length; i++){
				for(j = 0; j < M.length; j++){
					BugManager.printf("%g ", M[i][j]);
				}
				BugManager.println();
			}
		}
		
		// X = N g(Mb) = N b', b'_i (i >= rowcnt) = 0
		double[] newb = new double[A.length];
		for(i = 0; i < A.length; i++){
			if(q[i])
				newb[i] = 0;
			else
				for(j = 0; j < A.length; j++){
					if(!q[i])
						newb[i] += M[i][j] * b[j];
				}
		}
		
		if((BugManager.DEBUG && W_DEBUG)){
			BugManager.println("res(before N op) : ");
			for(i = 0; i < A.length; i++)
				BugManager.printf("%g ", newb[i]);
			BugManager.println();
		}
		for(i = 0; i < A.length; i++)
			for(j = 0; j < A.length; j++){
				x[i] += N[i][j] * newb[j];
			}
		if((BugManager.DEBUG && W_DEBUG) && SOLVE_DEBUG)
			BugManager.printf("World : end _solve()\n");
	}// end _solve
		
		/**
		 * M 연산 : 열 간의 연산, M : stack, rowcnt ^ 2크기<br>
		 * N 연산 : 행 간의 교환 연산, N : stack, matrix[0].length ^ 2크기
		 */
		@StableCode private strictfp void _solve_elimination(double[][] matrix,
				double[][] M, double[][] N, boolean[] q, double epsilon, double epsilon2){
			if((BugManager.DEBUG && W_DEBUG) && SOLVE_DEBUG)
				BugManager.printf("-- modified-GaussElim start!\n");
			
			int i, j, k;
			int rowcnt = matrix.length;
			
			for(i = 0; i < rowcnt; i++){
				if(i >= matrix[i].length)
					break;
				// matrix[i][i] = 1이 되어야 한다.
				// if abs(matrix[i][i]) < 10E-5 : 
				//    j (j > i)에서 abs(matrix[j][i]) > 10E-5인 matrix[j]를 matrix[i]에다가 더해준다.(M 연산)
				//    if abs(matrix[i][i]) < 10E-5 : 
				//       j(j > i)에서 abs(matrix[i][j]) > 10E-5인 matrix[*][j]를 matrix[*][i]에 더한다.(N연산)
				// if matrix[i][i] != 1 : 
				//    divide matrix[i] by matrix[i][i]
				// 이제 j (j > i)인 matrix[j][k]에 matrix[j][i] * matrix[i][k]를 빼준다.
				
				if(abs(matrix[i][i]) < epsilon2){
					if(BugManager.DEBUG && W_DEBUG && SOLVE_DEBUG)
						BugManager.printf("Trying to switch..\n");
					// 기본적으로 M연산 실시
					double maxval = abs(matrix[i][i]);
					int maxj = -1, maxk = -1;
					for(j = i + 1; j < rowcnt; j++){
						for(k = i + 1; k < rowcnt; k++){
							if(abs(matrix[j][k]) > maxval){
								maxj = j;
								maxk = k;
								maxval = abs(matrix[j][k]);
								if(maxval > 3){
									j = k = 21474836;
									continue;
								}
							}
						}
					}
					if((BugManager.DEBUG && W_DEBUG) && SOLVE_DEBUG)
						BugManager.printf("M,N operation : %d, %d\n", maxj, maxk);
					
					if(maxj != -1 && maxk != -1){
						double tmp = 0;
						for(k = 0; k < rowcnt; k++){
							tmp = matrix[k][i];
							matrix[k][i] = matrix[k][maxk];
							matrix[k][maxk] = tmp;
						}
						for(k = 0; k < matrix[i].length; k++){
							tmp = N[k][maxk];
							N[k][maxk] = N[k][i];
							N[k][i] = tmp;
						}
						
						for(k = 0; k < rowcnt; k++){
							tmp = matrix[i][k];
							matrix[i][k] = matrix[maxj][k];
							matrix[maxj][k] = tmp;
						}
						for(k = 0; k < matrix[i].length; k++){
							tmp = M[i][k];
							M[i][k] = M[maxj][k];
							M[maxj][k] = tmp;
						}
					}
				}
				if(abs(matrix[i][i]) < epsilon){
					// N, M operation후에도 0.0이다 -> q[i] = true
					q[i] = true;
					continue;
				}
				
				if(matrix[i][i] != 1.0){
					double rate = matrix[i][i];
					for(j = 0; j < rowcnt; j++)
						M[i][j] /= rate;
					for(j = 0; j < matrix.length; j++)
						matrix[i][j] /= rate;
					matrix[i][i] = 1;
				}
				
				// j : 각 열마다
				for(j = 0; j < rowcnt; j++){
					if(j == i) continue;
					
					double rate = matrix[j][i];
					matrix[j][i] = 0;
					for(k = i + 1; k < matrix[j].length; k++){
						// k < i에서 matrix[i][k]는 0이므로 계산하지 않아도 됨.
						matrix[j][k] -= rate * matrix[i][k];
					}
					for(k = 0; k < rowcnt; k++)
						M[j][k] -= rate * M[i][k];
				}
				
				if((BugManager.DEBUG && W_DEBUG) && SOLVE_DEBUG){
					BugManager.printf("A : \n");
					for(j = 0; j < matrix.length; j++){
						for(k = 0; k < matrix.length; k++){
							BugManager.printf("%g ", matrix[j][k]);
						}
						BugManager.println();
					}
					BugManager.printf("A_inv : \n");
					for(j = 0; j < M.length; j++){
						for(k = 0; k < M.length; k++){
							BugManager.printf("%g ", M[j][k]);
						}
						BugManager.println();
					}
				}
			}
			if((BugManager.DEBUG && W_DEBUG) && SOLVE_DEBUG)
				BugManager.printf("-- modified-GaussElim end!\n");
			return;
		}// end gaussElimination
		
		
		
		
	public static void main(String[] argv){
		double[][] A = new double[][]{
				{0.0411116, 0.00245543, 2.86092e-05, 0.0229069, 0.00245543, 2.86092e-05, 0.00648509, -5.51013e-20, 0.00000, 0.000128884, -3.71577e-19, 0.00000 ,0.00000, 0.00000},
				{0.00245543, 0.0314119 ,-5.72339e-05, -0.00982171, 0.0314119, 0.000228935, -5.51013e-20, 0.0125000, 0.00000, -1.13329e-19, 0.0125000, 0.00000, 0.00000, 0.00000}, 
				{2.86092e-05, -5.72339e-05, 0.0423384, 2.86079e-05, -5.72313e-05, 0.0241336, 0.00000, 0.00000, 0.00648509, 0.00000, 0.00000, 0.000128884, 0.0176839, 0.00000},
				{0.0229069, -0.00982171, 2.86079e-05, 0.134224, -0.00982171, 2.86079e-05, 0.000128883, -1.13329e-19, 0.00000, -0.0129442, -7.64237e-19, 0.00000, 0.00000, 0.00000},
				{0.00245543, 0.0314119, -5.72313e-05, -0.00982171, 0.0314119, 0.000228925, 4.75240e-19, 0.0125000, 0.00000, 9.77446e-19 ,0.0125000, 0.00000, 0.00000, 0.00000}, 
				{2.86092e-05, 0.000228935, 0.0241336, 2.86079e-05, 0.000228925, 0.135450, 0.00000, 0.00000, 0.000128883, 0.00000, 0.00000, -0.0129442, 0.0176839, 0.00000}, 
				{0.00648509, -5.51013e-20, 0.00000, 0.000128883, 4.75240e-19, 0.00000, 0.0411117, -0.00245543, 2.86120e-05, 0.0229069, -0.00245543, 2.86120e-05, 0.00000, 0.0000},
				{-5.51013e-20, 0.0125000, 0.00000, -1.13329e-19, 0.0125000, 0.00000, -0.00245543, 0.0314119, 5.72394e-05, 0.00982171, 0.0314119, -0.000228958, 0.00000, 0.0000},
				{0.00000, 0.00000, 0.00648509, 0.00000, 0.00000, 0.000128883, 2.86120e-05, 5.72394e-05, 0.0423384, 2.86132e-05, 5.72420e-05, 0.0241336, 0.00000, 0.017683},
				{0.000128884, -1.13329e-19, 0.00000, -0.0129442, 9.77446e-19, 0.00000, 0.0229069, 0.00982171, 2.86132e-05, 0.134224, 0.00982171, 2.86132e-05, 0.00000, 0.00000}, 
				{-3.71577e-19, 0.0125000, 0.00000, -7.64237e-19, 0.0125000, 0.00000, -0.00245543, 0.0314119, 5.72420e-05, 0.00982171, 0.0314119, -0.000228968, 0.00000, 0.00000},
				{0.00000, 0.00000, 0.000128884, 0.00000, 0.00000, -0.0129442, 2.86120e-05, -0.000228958, 0.0241336, 2.86132e-05, -0.000228968, 0.135450, 0.00000, 0.0176839},
				{0.00000, 0.00000 ,0.0176839, 0.00000, 0.00000, 0.0176839, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.0176839, 0.00000},
				{0.00000, 0.00000, 0.00000, 0.00000 ,0.00000, 0.00000, 0.00000, 0.00000, 0.0176839, 0.00000, 0.00000, 0.0176839, 0.00000, 0.0176839}
		};
		double[][] A_clone = new double[A.length][A.length];
		for(int i = 0; i < A.length; i++)
			for(int j = 0; j < A.length; j++)
				A_clone[i][j] = A[i][j];
		
		double[] b = new double[]{
				165.169,
				3.71440e-06,
				68.4129,
				165.169,
				3.71439e-06,
				68.4129,
				165.169,
				3.71440e-06,
				68.4129,
				165.169,
				3.71440e-06,
				68.4129,
				980.000,
				980.000
		};
		// answer ; 37.8899 -37.8899 37.8899 -37.8899 31.0504 409.950 31.0505 409.950 

		double[] x = new double[b.length];
		new World()._solve(A_clone, x, b);
		BugManager.println("answer : ");
		for(int i = 0; i < x.length; i++)
			BugManager.printf("%g ", x[i]);
		
		BugManager.printf("\ncheck : \n");
		for(int i = 0; i < A.length; i++){
			double f = 0;
			for(int j = 0; j < A.length; j++)
				f += A[i][j] * x[j];
			BugManager.printf("%g = %g\n", f, b[i]);
		}
	}// end main
}
