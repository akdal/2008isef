package agdr.robodari.plugin.rme.rlztion.gui;

import javax.swing.*;
import agdr.robodari.plugin.rme.rlztion.*;

public abstract class DebuggerSet extends JPanel{
	public abstract void initDebugger(RealizationModel rmodel);
	public abstract void finishDebugger();
}
