package agdr.robodari.plugin.rme.rlztion.gui;

import java.awt.Component;
import java.awt.Toolkit;
import java.util.Vector;

import javax.swing.AbstractListModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;

import agdr.robodari.plugin.rme.rdpi.*;


public class RDObjEvListSet {

	public static class RDObjEvListModel extends AbstractListModel{
		public final static int MAX_COUNT = 100;
		private Vector<RDObjectEvent> events;
		
		public RDObjEvListModel(){
			this.events = new Vector<RDObjectEvent>();
		}// end Constructor(ArrayList<RDObjecTEvent>)
		
		public void addElement(RDObjectEvent event){
			if(events.size() > MAX_COUNT){
				events.remove(0);
				super.fireIntervalRemoved(this, 1, 0);
			}
			events.add(event);
			super.fireIntervalAdded(this, events.size() - 1, events.size());
		}// end addElement(RDobjectEvent)
		public void removeElementAt(int idx){
			this.events.remove(idx); 
			super.fireIntervalRemoved(this, 0, 1);
		}// end removeElemAt
		
		public int getSize()
		{ return events.size(); }
		
		public Object getElementAt(int idx){
			return events.get(idx);
		}// end getElementAt(int)
	}// end class RDObjEvListModel
	public static class RDObjEvListCellRenderer extends DefaultListCellRenderer{
		private final static ImageIcon _mthdicon
				= new ImageIcon(Toolkit.getDefaultToolkit()
						.createImage(RDObjEvListCellRenderer.class.getResource("icons/dbgset_method.jpg")));
		private final static ImageIcon _fieldicon
				= new ImageIcon(Toolkit.getDefaultToolkit()
						.createImage(RDObjEvListCellRenderer.class.getResource("icons/dbgset_field.jpg")));
		
		public Component getListCellRendererComponent(JList list,
				Object value, int index, boolean isSelected, boolean cellHasFocus){
			JLabel label = (JLabel)super.getListCellRendererComponent(list,
					value, index, isSelected, cellHasFocus);
			if(!(value instanceof RDObjectEvent))
				return label;
			
			label.setHorizontalTextPosition(JLabel.RIGHT);
			label.setHorizontalAlignment(JLabel.LEFT);
			RDObjectEvent rdoe = (RDObjectEvent)value;
			
			StringBuffer text = new StringBuffer();
			
			if(rdoe.type == RDObjectEvent.TYPE_FIELD_ACCESS){
				label.setIcon(_fieldicon);
				text.append(rdoe.target.getRDFieldNames()[rdoe.index]).append(" : ").append(rdoe.target.getRDFieldTypes()[rdoe.index]);
			}else if(rdoe.type == RDObjectEvent.TYPE_FIELD_WRITTEN){
				label.setIcon(_fieldicon);
				text.append(rdoe.target.getRDFieldNames()[rdoe.index]).append(" = ").append(rdoe.values[0]);
			}else{
				label.setIcon(_mthdicon);
				text.append(rdoe.target.getRDMethodNames()[rdoe.index])
						.append("(");
				
				RDObject.Type[] prtypes = rdoe.target.getRDMethodParamTypes()[rdoe.index];
				if(prtypes.length != 0){
					text.append(prtypes[0]);
					for(int i = 1; i < prtypes.length; i++)
						text.append(", ").append(prtypes[i]);
				}
				text.append(")");
				RDObject.Type rettype = rdoe.target.getRDMethodTypes()[rdoe.index];
				text.append(" : ").append(rettype);
			}
			label.setText(text.toString());
			return label;
		}// end getListCellRendererComponent(JList, Object, int, boolean, boolean)
	}// end class RDObjEvListCellRenderer
}
