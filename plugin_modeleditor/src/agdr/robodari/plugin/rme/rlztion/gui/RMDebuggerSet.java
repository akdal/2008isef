package agdr.robodari.plugin.rme.rlztion.gui;

import java.awt.*;
import java.awt.event.*;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;
import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.event.*;
import javax.swing.table.*;

import agdr.robodari.appl.*;
import agdr.robodari.plugin.rme.gui.dialog.DbgSetRDObjEvInfoDialog;
import agdr.robodari.plugin.rme.gui.dialog.DbgSetValInfoDialog;
import agdr.robodari.plugin.rme.rdpi.*;
import agdr.robodari.plugin.rme.rlztion.*;
import agdr.robodari.plugin.rme.rlztion.gui.RDObjEvListSet.RDObjEvListModel;

public class RMDebuggerSet extends DebuggerSet implements
		ItemListener, ActionListener, ListSelectionListener, RDObjectListener{
	private JPanel dsp_northPanel;
		private DefaultComboBoxModel ctrlerComboBoxModel;
		private JComboBox ctrlerComboBox;
		private JTextField ctrlerSrcField;

	private JPanel dsp_centerPanel;
		private JPanel dsp_cp_rdobjevPanel;
			private JPanel dsp_cp_roep_southPanel;
				private JButton rdobjevIntcpButton;
				private JButton rdobjevInfoButton;
			private Vector<RDObjEvListModel> rdobjevListModels;
			private JScrollPane rdobjevScrPane;
			private JList rdobjevList;
		private JPanel dsp_cp_watchPanel;
			private JPanel dsp_cp_wp_southPanel;
			private JButton watchDetailButton;
			private Vector<WatchTableModel> watchTableModels;
			private JTable watchTable;
			private WatchTableCellRenderer cellRenderer;
		
			
	private RMRealizationModel realizationModel;
	private Frame parent;
	
		
	public RMDebuggerSet(Frame _parent){
		this.parent = _parent;
		_init();
	}// end Constructor
	
	private void _init(){
		super.setLayout(new BorderLayout());
		
		dsp_northPanel = new JPanel(new GridLayout(0, 2));
		{
			ctrlerComboBoxModel = new DefaultComboBoxModel();
			ctrlerComboBox = new JComboBox();
			ctrlerComboBox.setModel(ctrlerComboBoxModel);
			ctrlerComboBox.addItemListener(this);
			ctrlerSrcField = new JTextField();
			ctrlerSrcField.setEditable(false);

			dsp_northPanel.add(ctrlerComboBox);
			dsp_northPanel.add(ctrlerSrcField);
		}
		dsp_centerPanel = new JPanel(new GridLayout(2, 0));
		{
			dsp_cp_rdobjevPanel = new JPanel(new BorderLayout());
			{
				dsp_cp_roep_southPanel = new JPanel(new FlowLayout(
						FlowLayout.RIGHT));
				{
					rdobjevIntcpButton = new JButton("..");
					rdobjevInfoButton = new JButton("Info..");

					rdobjevIntcpButton.addActionListener(this);
					rdobjevInfoButton.addActionListener(this);
					dsp_cp_roep_southPanel.add(rdobjevIntcpButton);
					dsp_cp_roep_southPanel.add(rdobjevInfoButton);
				}
				rdobjevListModels = new Vector<RDObjEvListModel>();
				rdobjevList = new JList();
				rdobjevList.setCellRenderer(new RDObjEvListSet.RDObjEvListCellRenderer());
				rdobjevList.getSelectionModel().addListSelectionListener(this);
				rdobjevList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

				dsp_cp_rdobjevPanel.add(new JLabel("RDObj events",
						JLabel.CENTER), BorderLayout.NORTH);
				dsp_cp_rdobjevPanel.add(
						rdobjevScrPane = new JScrollPane(rdobjevList),
						BorderLayout.CENTER);
				dsp_cp_rdobjevPanel.add(dsp_cp_roep_southPanel,
						BorderLayout.SOUTH);
			}
			dsp_cp_watchPanel = new JPanel(new BorderLayout());
			{
				dsp_cp_wp_southPanel = new JPanel(new FlowLayout(
						FlowLayout.RIGHT));
				{
					watchDetailButton = new JButton("Detail..");
					dsp_cp_wp_southPanel.add(watchDetailButton);
				}
				watchDetailButton.addActionListener(this);
				
				watchTableModels = new Vector<WatchTableModel>();
				watchTable = new JTable();
				cellRenderer = new WatchTableCellRenderer();
				watchTable.setDefaultRenderer(Object.class, cellRenderer);
				watchTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

				dsp_cp_watchPanel.add(new JLabel("Watch",
						JLabel.CENTER), BorderLayout.NORTH);
				dsp_cp_watchPanel.add(new JScrollPane(watchTable),
						BorderLayout.CENTER);
				dsp_cp_watchPanel.add(dsp_cp_wp_southPanel,
						BorderLayout.SOUTH);
			}
			
			dsp_centerPanel.add(dsp_cp_rdobjevPanel);
			dsp_centerPanel.add(dsp_cp_watchPanel);
		}
		
		add(dsp_northPanel, BorderLayout.NORTH);
		add(dsp_centerPanel, BorderLayout.CENTER);
	}// end _init
	
	
	
	
	
	
	
	public void initDebugger(RealizationModel rmodel){
		BugManager.printf("RMDbgSet : initDebugger()\n");
		this.realizationModel = (RMRealizationModel)rmodel;
		
		watchDetailButton.setEnabled(true);
		rdobjevInfoButton.setEnabled(false);
		
		RDObjectManager oMng = this.realizationModel;
		String[] names = oMng.getRDObjectNames(this.realizationModel);

		this.rdobjevListModels.clear();
		this.watchTableModels.clear();
		for (int i = 0; i < names.length; i++) {
			RDObjEvListModel roelModel = new RDObjEvListModel();
			WatchTableModel wtModel = null;
			RDObject rdobj = oMng.getRDObject(this.realizationModel, names[i]);

			wtModel = new WatchTableModel(this.realizationModel, rdobj);

			this.rdobjevListModels.add(roelModel);
			this.watchTableModels.add(wtModel);
		}

		// addihng ctrler indices to ctrlerComoboBoxModel
		for (int i = 0; i < names.length; i++)
			this.ctrlerComboBoxModel.addElement(names[i]);
		
		this.realizationModel.addRDObjectListener(this);
	}// end initDebugger
	
	public void finishDebugger(){
		BugManager.printf("RMDbgSet : finishDebugger()\n");
		this.realizationModel.removeRDObjectListener(this);
		this.watchTableModels.clear();
		this.rdobjevListModels.clear();
		this.ctrlerComboBoxModel.removeAllElements();
		this.realizationModel = null;
	}// end finishDebugger
	

	public void openDebugTool(String ctrler) {
		BugManager.printf("RMDbgSet : openDebugTool()\n");
		int idx = ctrlerComboBoxModel.getIndexOf(ctrler);
		if (idx == -1)
			return;
		
		this.rdobjevList.setModel(rdobjevListModels.get(idx));
		this.rdobjevScrPane.getVerticalScrollBar().setValue(
				this.rdobjevScrPane.getVerticalScrollBar().getMaximum());
		this.watchTable.setModel(this.watchTableModels.get(idx));
	}// end openDebugTools(SourceEdtiable)
	

	public void fieldAccessed(RDObjectEvent rdoe)
	{ addRDObjectEvent(rdoe); }
	public void methodCalled(RDObjectEvent rdoe)
	{ addRDObjectEvent(rdoe); }
	public void fieldWritten(RDObjectEvent rdoe)
	{ addRDObjectEvent(rdoe); }
	public void addRDObjectEvent(RDObjectEvent event){
		BugManager.printf("RMDbgSet : addRDObjectEv\n");
		RDObjEvListModel model = rdobjevListModels.get(this.ctrlerComboBoxModel.getIndexOf(event.target.getName()));
		if(model.getSize() > 40)
			model.removeElementAt(0);
		model.addElement(event);
	}// end addRDObjectEvent(SourceEditable, RDObjectEvent)
	
	
	
	/* *********************************************
	 *                EVENT LISTENERS
	 *******************************************/


	public void itemStateChanged(ItemEvent ie) {
		BugManager.printf("RMDbgSet : itemStateChanged()\n");
		if (ie.getSource() == this.ctrlerComboBox) {
			this.openDebugTool((String) this.ctrlerComboBox.getSelectedItem());
		}
	}// end itemStateChanged(ItemEvent)
	
	
	public void actionPerformed(ActionEvent ae){
		Object source = ae.getSource();
		
		if(source == this.rdobjevIntcpButton){
			// FIXME
		}else if(source == this.rdobjevInfoButton){
			int selidx = rdobjevList.getSelectedIndex();
			if(selidx == -1) return;
			
			RDObjectEvent obj = (RDObjectEvent)rdobjevListModels
					.get(ctrlerComboBox.getSelectedIndex()).getElementAt(selidx);
			
			if(obj.type == RDObjectEvent.TYPE_FIELD_ACCESS || obj.type == RDObjectEvent.TYPE_FIELD_WRITTEN){
				DbgSetValInfoDialog dialog = new DbgSetValInfoDialog(parent);
				dialog.showObject(obj.values[0], obj.types[0]);
			}else{
				DbgSetRDObjEvInfoDialog dialog = new DbgSetRDObjEvInfoDialog(parent);
				dialog.showMethod(obj);
			}
		}
		
		
		else if(source == this.watchDetailButton){
			int selidx = watchTable.getSelectedRow();
			if(selidx == -1) return;
			
			WatchTableModel model = watchTableModels.get(this.ctrlerComboBox.getSelectedIndex());
			DbgSetValInfoDialog dsvid = new DbgSetValInfoDialog(parent);
			Object value = model.getFieldValue(selidx);
			if(value == null){
				JOptionPane.showMessageDialog(null, "Value is null.");
				return;
			}
			Class type = value.getClass();
			dsvid.showObject(value, type);
		}
	}// end actionPerformed


	public void valueChanged(ListSelectionEvent lse) {
		Object src = lse.getSource();
		if (src == rdobjevList.getSelectionModel()) {
			if (lse.getFirstIndex() == -1)
				rdobjevInfoButton.setEnabled(false);
			else
				rdobjevInfoButton.setEnabled(true);
		}
	}// end valueChanged(ListSelectionEvent)
	
	
	
	
	
	

	public static class WatchTableModel extends AbstractTableModel {
		private final static Field[] NULLFARRAY = new Field[0];
		private final static String[] WATCHTABLE_TITLE = new String[] {
				"Field", "Value" };

		private RealizationModel rmodel;
		private RDObject targetobj;
		private ArrayList<String> fields;
		private Hashtable<String, RDObject.Type> fieldTypes;

		public WatchTableModel(RealizationModel rmodel, RDObject targetobj){
			this.targetobj = targetobj;
			this.rmodel = rmodel;

			fields = new ArrayList<String>();
			fieldTypes = new Hashtable<String, RDObject.Type>();
			String[] fnames = targetobj.getRDFieldNames();
			RDObject.Type[] ftypes = targetobj.getRDFieldTypes();
			for(int i = 0; i < fnames.length; i++)
				fieldTypes.put(fnames[i], ftypes[i]);
			
			fields.add("");
		}// end WatchTableModel(Class)

		public void addRow() {
			fields.add("");
			super.fireTableStructureChanged();
		}// end addRow()

		public int findColumn(String name) {
			if (name.equals(WATCHTABLE_TITLE[0]))
				return 0;
			else if (name.equals(WATCHTABLE_TITLE[1]))
				return 1;
			return -1;
		}// end findCOlumn(String)

		public int getColumnCount() {
			return 2;
		}

		public int getRowCount() {
			return fields.size();
		}

		public boolean isCellEditable(int row, int column) {
			return column == 0;
		}// end isCEllEditable(int, int)

		public String getColumnName(int idx) {
			return WATCHTABLE_TITLE[idx];
		}

		public boolean fieldExists(int idx){
			if(fields.size() <= idx)
				return false;
			return fieldTypes.containsKey(fields.get(idx));
		}// end fieldExists
		public Object getFieldValue(int idx) {
			try{
				return this.targetobj.getRDFieldValue(rmodel, this.fields.get(idx));
			}catch (Exception e){}
			return null;
		}// end getFieldValue(int)

		public Class getFieldClass(int idx) {
			String fname = fields.get(idx);
			if(!this.fieldTypes.containsKey(fname))
				return null;
			RDObject.Type type = this.fieldTypes.get(fname);
			if(type == null)
				return null;
			return type.clazz;
		}

		public Object getValueAt(int row, int column) {
			if (column == 0)
				return fields.get(row);
			else if (column == 1) {
				String idf = fields.get(row);
				if (idf == null || idf.equals(""))
					return "";
				else if (fieldTypes.containsKey(idf)){
					return this.getFieldValue(row);
				}
				return "[wrong field]";
			}
			return null;
		}// end getValueAt(int, int)

		public void setValueAt(Object value, int row, int column) {
			if (column == 1)
				return;

			fields.set(row, (String) value);
			super.fireTableRowsUpdated(row, row);
			super.fireTableCellUpdated(row, 0);
			super.fireTableCellUpdated(row, 1);
		}// end setValueAt(Object, int, int)
	}// end class WatchTableMOdel

	public static class WatchTableCellRenderer extends DefaultTableCellRenderer {
		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {
			Component comp = super.getTableCellRendererComponent(table, value,
					isSelected, hasFocus, row, column);
			comp.setForeground(Color.black);
			if(column == 0){
				return comp;
			}
			
			TableModel tmodel = table.getModel();
			if (!(tmodel instanceof WatchTableModel))
				return comp;

			WatchTableModel wtmodel = (WatchTableModel) tmodel;
			if (!wtmodel.fieldExists(row))
				comp.setForeground(Color.red);
			return comp;
		}// end getTableCellRenderer(JTable, Object, boolena, boolean, int,
			// int)
	}// end clas WatchTableFCellREnderer
}
