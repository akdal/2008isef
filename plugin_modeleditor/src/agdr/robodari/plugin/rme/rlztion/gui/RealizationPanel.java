package agdr.robodari.plugin.rme.rlztion.gui;

import java.io.*;
import java.util.*;
import java.lang.reflect.*;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.vecmath.*;

import static agdr.robodari.plugin.rme.rlztion.gui.RDObjEvListSet.*;

import agdr.robodari.appl.*;
import agdr.robodari.comp.*;
import agdr.robodari.plugin.rme.gui.dialog.*;
import agdr.robodari.plugin.rme.rdpi.*;
import agdr.robodari.plugin.rme.rlztion.*;


public class RealizationPanel extends JPanel implements ActionListener, WorldListener, RlzModelListener{
	public final static int ST_INIT_RPANEL = 0;
	public final static int ST_INIT_COMPS = 1;
	public final static int ST_INIT_RDPLOAD = 2;
	public final static int ST_DBG_ONPROC = 10;
	public final static int ST_DBG_PAUSE = 11;
	public final static int ST_UNEMPLOYED = -1;

	private final static SourceEditable[] NULLSEARRAY = new SourceEditable[] {};
	private final static Comparator<String> SECOMPR = new Comparator<String>() {
		public int compare(String a, String b) {
			if (a == null)
				return -1;
			else if (b == null)
				return 1;
			return String.CASE_INSENSITIVE_ORDER.compare(a, b);
		}
	};

	private JSplitPane splitPane;

	private JPanel rviewerPanel;
		private JPanel rvp_centerPanel;
			private JPanel rvp_cp_northPanel;
				private JButton startWorldButton;
				private JButton pauseWorldButton;
				private JButton resumeWorldButton;
				//private JButton resetWorldButton;
				private JLabel timeLabel;
			private SimulatorPanel simPanel;
			private JPanel rvp_cp_southPanel;
				private JTextField deltaTimeField;
				private JTextField timeRateField;
				private JButton applyButton;
				private JCheckBox dupdateMdlBox;
			private JTextArea logTextArea;
	
	private DebuggerSet debuggerSet;

	private RealizationModel realizationModel;
	// private Project project;
	// private SourceEditable[] controllers;
	// private RoboDariProcessor[] rdProcessors;
	// private ArrayList<Integer> starterIdcs;
	private int state;
	
	
	
	public RealizationPanel(/* RbModelEditorManager manager, */Frame parent, DebuggerSet dset) {
		this.debuggerSet = dset;
		__initGUIs();

		this.state = RealizationPanel.ST_UNEMPLOYED;
	}// end Constructor(EditorMAnager, Jframe)

	private void __initGUIs() {
		super.setLayout(new BorderLayout());

		splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);

		rviewerPanel = new JPanel(new BorderLayout());
		{
			rvp_centerPanel = new JPanel(new BorderLayout());
			{
				rvp_cp_northPanel = new JPanel(new FlowLayout());
				{
					startWorldButton = new JButton("Start");
					pauseWorldButton = new JButton("Pause");
					resumeWorldButton = new JButton("Resume");
					timeLabel = new JLabel("0.0");
					startWorldButton.addActionListener(this);
					pauseWorldButton.addActionListener(this);
					resumeWorldButton.addActionListener(this);

					rvp_cp_northPanel.add(startWorldButton);
					rvp_cp_northPanel.add(pauseWorldButton);
					rvp_cp_northPanel.add(resumeWorldButton);
					rvp_cp_northPanel.add(timeLabel);
				}
				rvp_cp_southPanel = new JPanel();
				{
					deltaTimeField = new JTextField();
					deltaTimeField.setText(String
							.valueOf(World.DEFAULT_TIMEINTERVAL));
					deltaTimeField.setHorizontalAlignment(JTextField.RIGHT);
					deltaTimeField.setColumns(6);
					timeRateField = new JTextField();
					timeRateField.setColumns(6);
					timeRateField.setHorizontalAlignment(JTextField.RIGHT);
					
					applyButton = new JButton("Apply");
					applyButton.addActionListener(this);
					
					dupdateMdlBox = new JCheckBox("Don't update 3D Model");
					dupdateMdlBox.addActionListener(this);

					rvp_cp_southPanel.add(new JLabel("deltaT :"));
					rvp_cp_southPanel.add(deltaTimeField);
					rvp_cp_southPanel.add(new JLabel("ms"));
					rvp_cp_southPanel.add(new JLabel("timeRate :"));
					rvp_cp_southPanel.add(timeRateField);
					rvp_cp_southPanel.add(applyButton);
					rvp_cp_southPanel.add(dupdateMdlBox);
				}
				rvp_centerPanel.add(rvp_cp_northPanel, BorderLayout.NORTH);
				rvp_centerPanel.add(rvp_cp_southPanel, BorderLayout.SOUTH);
			}
			logTextArea = new JTextArea();
			JScrollPane jspLta = new JScrollPane(logTextArea);
			jspLta.setPreferredSize(new Dimension(400, 200));
			rviewerPanel.add(this.rvp_centerPanel, BorderLayout.CENTER);
			rviewerPanel.add(jspLta, BorderLayout.SOUTH);
		}
		splitPane.setLeftComponent(rviewerPanel);
		splitPane.setRightComponent(debuggerSet);
		splitPane.setDividerLocation(0.7);
		
		super.add(splitPane, BorderLayout.CENTER);
	}// end __init()

	public void setVisible(boolean b) {
		if(!b){
			this.finish();
		}
	}// end dispose()

	/***************************************************************************
	 * EVENT LISTENER IMPLEMENTATION
	 **************************************************************************/

	public void actionPerformed(ActionEvent ae){
		Object source = ae.getSource();
		if(source == this.applyButton){
			RealizationModel model = simPanel.getRealizationModel();
			World world = null;
			if(model != null)
				world = model.getWorld();
			
			if(world == null || model == null){
				Toolkit.getDefaultToolkit().beep();
				this.deltaTimeField.setText(String.valueOf(World
						.DEFAULT_TIMEINTERVAL));
				this.timeRateField.setText("1.0");
				return;
			}
			
			try{
				int tint = Integer.parseInt(this.deltaTimeField.getText());
				if(world != null)
					world.setTimeInterval(tint);
			}catch(Exception excp){
				Toolkit.getDefaultToolkit().beep();
				this.deltaTimeField.setText(String.valueOf(
						world.getTimeInterval()));
			}
			try{
				double tint = Double.parseDouble(this.timeRateField.getText());
				if(world != null)
					world.setTimeRate(tint);
			}catch(Exception excp){
				Toolkit.getDefaultToolkit().beep();
				this.timeRateField.setText(String.valueOf(
						world.getTimeRate()));
			}
		}else if(source == this.startWorldButton){
				// System.out.println(simPanel.getRealizationModel());
			this.start();
		}else if(source == this.pauseWorldButton){
			this.pause();
		}else if(source == this.resumeWorldButton){
			this.resume();
		}else if(source == this.dupdateMdlBox){
			this.realizationModel.setUpdate3D(!this.dupdateMdlBox.isSelected());
		}
	}// end actionPerformed(ActionEvent)

	public strictfp void timeElapsed(RealizationModel rmodel) {
		long et = rmodel.getWorld().getElapsedTime();
		double det = (double)et / 1000.0;
		timeLabel.setText(String.format("%.3f", det).toString());

		RealizationMap smap = rmodel.getRealizationMap();
		World world = rmodel.getWorld();
		for (int i = 0; i < smap.getModelCount(); i++) {
			if (!world.isChanged(i))
				continue;
			rmodel.resetGeometryArray(rmodel.getRealizationMap().getKey(i));
		}
	}// end timeElapsed(RealizationModel)

	/***************************************************************************
	 * debug
	 **************************************************************************/

	public void initialize(RealizationModel rmodel) {
		if(this.state != ST_UNEMPLOYED)
			throw new IllegalStateException("Realization already started");
		if(this.realizationModel != null)
			this.realizationModel.removeRlzModelListener(this);
		
		BugManager.printf("--- RmzPanel : initDebugger\n");
		this.realizationModel = rmodel;
		this.realizationModel.addRlzModelListener(this);
		
		this.state = ST_INIT_RPANEL;

		this.simPanel = new SimulatorPanel();
		simPanel.load(this.realizationModel);
		this.rvp_centerPanel.add(this.simPanel, BorderLayout.CENTER);
		this.realizationModel.getWorld().setWorldListener(this);

		this.state = ST_INIT_COMPS;

		timeLabel.setText("0.000");
		this.deltaTimeField.setText(String.valueOf(rmodel.getWorld().getTimeInterval()));
		this.timeRateField.setText(String.valueOf(rmodel.getWorld().getTimeRate()));
		this.logTextArea.setText("");
		startWorldButton.setEnabled(true);
		resumeWorldButton.setEnabled(false);
		pauseWorldButton.setEnabled(false);
		try {
			this.realizationModel.getWorld().setTimeInterval(
					Integer.parseInt(this.deltaTimeField.getText()));
		} catch (Exception e) {
			this.deltaTimeField.setText(String.valueOf(this.realizationModel
					.getWorld().getTimeInterval()));
		}

		this.state = ST_INIT_RDPLOAD;
		this.debuggerSet.initDebugger(this.realizationModel);

		this.state = ST_DBG_PAUSE;

		super.setVisible(true);
		BugManager.printf("--- DebuggerSEt : end initDebugger\n");
	}// end initDebugger(RealizationModel)

	public void start(){
		System.out.println("Debugger : startDebug : startWorld");
		try {
			this.startWorldButton.setEnabled(false);
			this.resumeWorldButton.setEnabled(false);
			this.pauseWorldButton.setEnabled(true);
			OutputStream os = new OutputStream() {
				public void write(int i){
					if(logTextArea.getLineCount() > 100){
						String cnt = logTextArea.getText();
						cnt = cnt.substring(cnt.indexOf("\n") + 1, cnt.length());
						logTextArea.setText(cnt + String.valueOf((char)i));
					}else
						logTextArea.append(String.valueOf((char) i));
				}
			};
			final PrintStream ps = new PrintStream(os);
			this.realizationModel.setRDOut(ps);
			this.realizationModel.setRDErr(ps);
			this.realizationModel.startRealization();
		} catch (RealizationStartedException rse) {
			BugManager.log(rse, true);
		}catch(RealizationNotReadyException rnre)
		{ BugManager.log(rnre, true); }
		this.state = RealizationPanel.ST_DBG_ONPROC;
	}// end hello
	
	public void resume() {
		try {
			this.realizationModel.resumeRealization();
		} catch (RealizationNotReadyException rnre) {
			BugManager.log(rnre, true);
		}
		this.state = RealizationPanel.ST_DBG_ONPROC;
	}// end startDebug()

	public void pause(){
		if(realizationModel.isRlzStarted()){
			try{
				this.realizationModel.pauseRealization();
			}catch(RealizationNotReadyException rnre){
				rnre.printStackTrace();
				//BugManager.log(rnre, true);
			}
		}
	}// end pauseDebug()

	public void finish() {
		super.setVisible(false);
		
		if (this.simPanel != null)
			this.rvp_centerPanel.remove(this.simPanel);

		this.debuggerSet.finishDebugger();

		if(this.realizationModel.getWorld().saidHello()){
			try {
				this.realizationModel.disposeRealization();
			} catch (Exception excp) {
				BugManager.log(excp, true);
			}
		}
		this.state = ST_UNEMPLOYED;
	}// end finishDebug()

	public int getState() {
		return this.state;
	}


	public SimulatorPanel getSimulatorPanel() {
		return simPanel;
	}

	public void rlzInit(RealizationModel rm){}
	public void rlzStarted(RealizationModel rm){}
	public void rlzResumed(RealizationModel rm){
		this.resumeWorldButton.setEnabled(false);
		this.pauseWorldButton.setEnabled(true);
	}// end rlzResumed
	public void rlzPaused(RealizationModel rm){
		this.resumeWorldButton.setEnabled(true);
		this.pauseWorldButton.setEnabled(false);
	}// end rlzPaused
	public void rlzDisposed(RealizationModel rm){}
}
