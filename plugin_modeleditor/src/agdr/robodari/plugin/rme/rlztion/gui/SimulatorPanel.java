package agdr.robodari.plugin.rme.rlztion.gui;

import java.util.*;
import java.awt.*;
import javax.swing.*;
import javax.vecmath.*;
import javax.media.j3d.*;


import agdr.robodari.plugin.rme.rlztion.RealizationModel;

import com.sun.j3d.utils.universe.*;
import com.sun.j3d.utils.behaviors.keyboard.*;
import com.sun.j3d.utils.behaviors.mouse.*;
import com.sun.j3d.utils.geometry.*;

//import org.agdr.robodari.comp.*;
//import org.agdr.robodari.edit.*;


public class SimulatorPanel extends JPanel implements GeometryUpdater{
	private final static BoundingSphere BOUND = new BoundingSphere(new Point3d(0, 0, 0),
			1000.0f);
	
	
	private double scale = 1;
	
	private VirtualUniverse universe;
		private javax.media.j3d.Locale locale;
			private BranchGroup rootBrGroup;
				private BranchGroup rlztionGroup;
					private TransformGroup rlztionTransGroup;
					private Transform3D rlztionTransform;
				private TransformGroup viewTransGroup;
					private Transform3D viewTransform;
					private ViewPlatform viewPlatf;
						private View view;
				private KeyNavigatorBehavior viewKeyBehavior;
				private MouseRotate viewMsBehv_rotate;
					

	private GeometryArray[] forceIndications;
	private GeometryArray[] velocityIndications;
	private boolean showForces = true;
	private boolean showVelocities = true;
	
	private Canvas3D canvas;
	private RealizationModel realizationModel;
	
	
	public SimulatorPanel(){
		canvas = new Canvas3D(SimpleUniverse.getPreferredConfiguration());
		canvas.setSize(600, 600);
		super.setSize(600, 600);
		super.setPreferredSize(new Dimension(600, 600));
		super.setLayout(new BorderLayout());
		super.add(canvas, BorderLayout.CENTER);

		genUniverse();
	}// end Constructor()
	
	
	
	private void genUniverse(){
		this.universe = new VirtualUniverse();
		{
			int[] xlocale = new int[8];
			int[] ylocale = new int[8];
			int[] zlocale = new int[8];
			this.locale = new javax.media.j3d.Locale(this.universe, new HiResCoord(xlocale, ylocale, zlocale));
			{
				this.rootBrGroup = new BranchGroup();
				{
					this.rlztionGroup = new BranchGroup();
					{
						this.rlztionTransGroup = new TransformGroup();
						this.rlztionTransGroup.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
						this.rlztionTransGroup.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
						this.rlztionTransGroup.setCapability(Group.ALLOW_CHILDREN_EXTEND);
						this.rlztionTransGroup.setCapability(Group.ALLOW_CHILDREN_WRITE);
						this.rlztionTransform = new Transform3D();
						this.rlztionTransGroup.setTransform(this.rlztionTransform);
						this.rlztionGroup.addChild(this.rlztionTransGroup);
					}
					this.viewTransGroup = new TransformGroup();
					{
						this.viewTransGroup.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
						this.viewTransGroup.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
						this.viewTransform = new Transform3D();
						this.viewTransform.setTranslation(new Vector3f(5, 0, 0));
						this.viewTransGroup.setTransform(this.viewTransform);
						
						this.viewPlatf = new ViewPlatform();
						this.viewPlatf.setViewAttachPolicy(View.RELATIVE_TO_FIELD_OF_VIEW);
						
						this.view = new View();
						PhysicalBody pb = new PhysicalBody();
						PhysicalEnvironment pe = new PhysicalEnvironment();
						view.setPhysicalBody(pb);
						view.setPhysicalEnvironment(pe);
						view.attachViewPlatform(this.viewPlatf);
						view.setFrontClipDistance(0.001);
						view.setBackClipDistance(1000);
						view.addCanvas3D(this.canvas);
						this.viewTransGroup.addChild(this.viewPlatf);
					}
					this.viewKeyBehavior = new KeyNavigatorBehavior(this.viewTransGroup);
					this.viewKeyBehavior.setSchedulingBounds(this.BOUND);
					this.viewKeyBehavior.setEnable(true);

					this.viewMsBehv_rotate = new MouseRotate(this.rlztionTransGroup);
					this.viewMsBehv_rotate.setEnable(true);
					this.viewMsBehv_rotate.setSchedulingBounds(this.BOUND);
					
					this.rootBrGroup.addChild(this.viewMsBehv_rotate);
					this.rootBrGroup.addChild(this.viewKeyBehavior);
					this.rootBrGroup.addChild(this.rlztionGroup);
					this.rootBrGroup.addChild(this.viewTransGroup);
				}
				this.locale.addBranchGraph(this.rootBrGroup);
			}
		}
	}// end genUniverse()
	
	/*
	
	
	private void resetScale(){
		GeometryArray[] objects = realizationModel.getGeometries();
		double xmax = -214748, xmin = 214748, ymax = -214748, ymin = 214748,
				zmax = -214748, zmin = 214748;
		
		for(GeometryArray eachobj : objects){
			GeometryInfo info = new GeometryInfo(eachobj);
			Point3f[] coords = info.getCoordinates();
			for(Point3f eachcoord : coords){
				if(eachcoord.x > xmax)
					xmax = eachcoord.x;
				if(eachcoord.x < xmin)
					xmin = eachcoord.x;
				if(eachcoord.y > ymax)
					ymax = eachcoord.y;
				if(eachcoord.y < ymin)
					ymin = eachcoord.y;
				if(eachcoord.z > zmax)
					zmax = eachcoord.z;
				if(eachcoord.z < zmin)
					zmin = eachcoord.z;
			}
		}
		double xscale = xmax - xmin, yscale = ymax - ymin, zscale = zmax - zmin;
		double max = xscale;
		if(max < yscale)
			max = yscale;
		if(max < zscale)
			max = zscale;
		
		if(max == 0) return;
		this.scale = 1.0 / max;
		System.out.println("RlzP : resetScale() : scale : " + this.scale + " , max : " + max + " , zmax : " + zscale);
	}// end resetScale()
	*/
	
	
	
	public void load(RealizationModel realizationModel){
		this.realizationModel = realizationModel;
		//resetScale();
		
		this.rlztionTransform.setScale(this.scale);
		this.rlztionTransGroup.setTransform(this.rlztionTransform);
		
		
		GeometryArray[] objects = realizationModel.getGeometries();
		BranchGroup bgroup = new BranchGroup();
		
		for(GeometryArray eachobj : objects){
			Shape3D s3d = new Shape3D();
			Appearance appr = realizationModel.getAppearance(eachobj);
			s3d.setAppearance(appr);
			s3d.addGeometry(eachobj);
			bgroup.addChild(s3d);
		}
		bgroup.addChild(createAxisObj(new Color3f(1, 0, 0), 50, 0, 0));
		bgroup.addChild(createAxisObj(new Color3f(0, 1, 0), 0, 50, 0));
		bgroup.addChild(createAxisObj(new Color3f(0, 0, 1), 0, 0, 50));
		
		addLight(bgroup);
		addForceIndications(bgroup);
		addVelocityIndications(bgroup);
		rlztionTransGroup.addChild(bgroup);
		
		super.repaint();
	}// end load(SketchModel, ConnectionModel, Controller)

	private Shape3D createAxisObj(Color3f col, double x, double y, double z){
		Shape3D s3d = new Shape3D();
		
		LineArray la = new LineArray(2, GeometryArray.COORDINATES
				| GeometryArray.COLOR_3);
		la.setCoordinate(0, new double[]{ x, y, z });
		la.setCoordinate(1, new double[]{ 0, 0, 0 });
		la.setColor(0, col);
		la.setColor(1, col);
		s3d.addGeometry(la);
		
		Appearance appr = new Appearance();
		ColoringAttributes cattr = new ColoringAttributes();
		LineAttributes lattr = new LineAttributes();
		PolygonAttributes pattr = new PolygonAttributes(PolygonAttributes.POLYGON_LINE, PolygonAttributes.CULL_NONE, 0);
		
		lattr.setLineWidth(1);
		cattr.setColor(col);
		
		appr.setPolygonAttributes(pattr);
		appr.setColoringAttributes(cattr);
		appr.setLineAttributes(lattr);
		s3d.setAppearance(appr);
		
		return s3d;
	}// end addChild(Color3f, double, double, double)
	
	
	private void addLight(BranchGroup group){
		// Set up the ambient light
		Color3f ambientColor = new Color3f(1.0f, 1.0f, 1.0f);
		AmbientLight ambientLightNode = new AmbientLight(ambientColor);
		ambientLightNode.setInfluencingBounds(BOUND);
		
		float sqrt3 = (float)Math.sqrt(3);
		DirectionalLight dlNode = new DirectionalLight(true, ambientColor, new Vector3f(-0.4f, -0.4f, -0.4f));
		dlNode.setInfluencingBounds(BOUND);
		
		group.addChild(dlNode);
		group.addChild(ambientLightNode);
/*
		// Set up the directional lights
		Color3f lightColor = new Color3f(1.0f, 1.0f, 0.0f);
		Vector3f light1Direction  = new Vector3f(1.0f, 1.0f, 1.0f);
		Vector3f light2Direction  = new Vector3f(-1.0f, -1.0f, -1.0f);

		DirectionalLight light1
		    = new DirectionalLight(lightColor, light1Direction);
		light1.setInfluencingBounds(BOUND);
		rootGroup.addChild(light1);
		
		DirectionalLight light2
		    = new DirectionalLight(lightColor, light2Direction);
		light2.setInfluencingBounds(BOUND);
		rootGroup.addChild(light2);*/
	}// end addLight(SimpleUniverse)
	
	private void addForceIndications(BranchGroup bg){
		forceIndications = new GeometryArray[realizationModel.getRealizationMap().getModelCount()];
		for(int i = 0; i < forceIndications.length; i++){
			
		}
	}// end addForceIndications()
	private void addVelocityIndications(BranchGroup bg){}
	
	public void updateData(Geometry g){}
	
	
	
	
	public boolean showsVelocities()
	{ return this.showVelocities; }
	public void showVelocities()
	{ this.showVelocities = true; }
	public void hideVelocities()
	{ this.showVelocities = false; }
	public boolean showsForces()
	{ return this.showForces; }
	public void showForces()
	{ this.showForces = true; }
	public void hideForces()
	{ this.showForces = false; }
	
	
	
	
	public RealizationModel getRealizationModel()
	{ return realizationModel; }
}
