package agdr.robodari.plugin.rme.store;

public class ModelNotRegisteredException extends Exception{
	public ModelNotRegisteredException(){}
	public ModelNotRegisteredException(String message)
	{ super(message); }
	public ModelNotRegisteredException(String message, Throwable cause)
	{ super(message, cause); }
	public ModelNotRegisteredException(Throwable cause)
	{ super(cause); }
}
