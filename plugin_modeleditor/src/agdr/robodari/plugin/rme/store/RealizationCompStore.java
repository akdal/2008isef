package agdr.robodari.plugin.rme.store;

import agdr.robodari.comp.model.RobotCompModel;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.rlztion.*;

import java.util.*;
import javax.vecmath.*;


public class RealizationCompStore {
	private final static Vector<GraphGenerator>
		graphGenerators = new Vector<GraphGenerator>();
	
	
	public final static void registerGraphGenerator(GraphGenerator g){
		graphGenerators.add(g);
	}// end registerGraphGenerator
	
	
	public final static RealizationMap.Key generateGraph(
			RobotCompModel str, SkObject obj, Matrix4f trans,
			RealizationMap map, World world){
		GraphGenerator gg;
		int size = graphGenerators.size();
		
		for(int i = 0; i < size; i++){
			gg = graphGenerators.get(i);
			if((gg.getGeneratorType() & GraphGenerator.SPECFC_OBJECT) != 0)
				if(gg.editable(str, obj, map, world)){
					return gg.generateGraph(obj, trans, map, world);
				}
		}
		for(int i = 0; i < size; i++){
			gg = graphGenerators.get(i);
			if((gg.getGeneratorType() & GraphGenerator.SPECFC_RCOMP) != 0)
				if(gg.editable(str, obj, map, world)){
					return gg.generateGraph(obj, trans, map, world);
				}
		}

		for(int i = 0; i < size; i++){
			gg = graphGenerators.get(i);
			if((gg.getGeneratorType() & GraphGenerator.SPECFC_RCOMP_OR_OBJ) != 0)
				if(gg.editable(str, obj, map, world)){
					return gg.generateGraph(obj, trans, map, world);
				}
		}
		return null;
	}// end findComaptibleGraphGenerator
}
