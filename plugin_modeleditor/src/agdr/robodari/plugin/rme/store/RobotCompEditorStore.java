package agdr.robodari.plugin.rme.store;

import java.util.*;
import java.lang.reflect.*;

import agdr.robodari.appl.*;
import agdr.robodari.comp.*;
import agdr.robodari.gui.*;
import agdr.robodari.plugin.rme.edit.*;
import agdr.robodari.plugin.rme.gui.CompEditor;

public class RobotCompEditorStore {
	private final static Hashtable<Class<? extends RobotComponent>,
			Class<? extends CompEditor>>
		table = new Hashtable<Class<? extends RobotComponent>,
			Class<? extends CompEditor>>();
	
	public static void register(Class<? extends RobotComponent> cls,
			Class<? extends CompEditor> ce)
	{ table.put(cls, ce); }
	public static Class<? extends CompEditor> getCompEditorClass(Class<?
			extends RobotComponent> cls)
	{ return table.get(cls); }
	
	
	public static boolean hasCompEditor(Class<? extends RobotComponent> cls)
	{ return table.get(cls) != null; }
	
	public static Class<? extends RobotComponent>[] getRegisteredComps(){
		Class<? extends RobotComponent>[] array = new Class[table.size()];

		Enumeration<Class<? extends RobotComponent>> edclses = table.keys();
		for(int i = 0; i < array.length; i++)
			array[i] = edclses.nextElement();
		return array;
	}// end getRegisteredComps()
}
