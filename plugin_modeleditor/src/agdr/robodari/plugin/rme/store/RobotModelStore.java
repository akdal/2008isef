package agdr.robodari.plugin.rme.store;

import java.util.*;
import javax.media.j3d.*;

import agdr.robodari.comp.*;
import agdr.robodari.comp.model.ModelGenerator;
import agdr.robodari.comp.model.RobotCompModel;
import agdr.robodari.plugin.rme.rlztion.*;

public class RobotModelStore {
	private final static Hashtable<Class<? extends RobotComponent>, ModelGenerator>
					models = new Hashtable<Class<? extends RobotComponent>, ModelGenerator>();
	
	
	
	public static void registerModel(Class clazz, ModelGenerator modelg){
		models.put(clazz, modelg);
	}// end loadModel(String)
	
	
	public static boolean isRegistered(Class clazz){
		if(models.containsKey(clazz))
			return true;
		
		Enumeration<Class<? extends RobotComponent>> enums = models.keys();
		while(enums.hasMoreElements()){
			Class<? extends RobotComponent> cls = enums.nextElement();
			try{
				if(clazz.asSubclass(cls) != null)
					return true;
			}catch(Exception excp){ continue; }
		}
		return false;
	}// end isRegistered(Class)
	
	public static RobotCompModel generateModel(RobotComponent rc)
			throws ModelNotRegisteredException, NullPointerException,
			Exception{
		if(rc == null)
			return null;
		
		if(models.containsKey(rc.getClass()))
			return models.get(rc.getClass()).generate(rc);
		
		Enumeration<Class<? extends RobotComponent>> enums = models.keys();
		Class clazz = rc.getClass();
		while(enums.hasMoreElements()){
			Class<? extends RobotComponent> cls = enums.nextElement();
			try{
				if(clazz.asSubclass(cls) != null){
					return models.get(cls).generate(rc);
				}
			}catch(Exception excp){ continue; }
		}
		throw new ModelNotRegisteredException();
	}// end generateModel(RobotComponent)
	
	
	
	
	
	
	
	
}
